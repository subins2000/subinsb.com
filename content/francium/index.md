---
title: Francium
author: Subin Siby
type: page
date: 2015-04-18T18:08:55+00:00

---
The support forum for the **[Francium Projects][1]**.

Please follow the [guidelines][2] and ask for support in the corresponding pages.

This page is for the support regarding the **Francium** base class. This doesn&#8217;t mean that you should ask support for a specific Francium project here. You should ask for that in their corresponding Ask pages.

 [1]: //subinsb.com/the-francium-project "The Francium Project"
 [2]: //subinsb.com/ask