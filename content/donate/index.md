---
title: Donate
author: Subin Siby
type: page
date: 2014-02-20T18:13:53+00:00
lastmod: 2020-10-27T18:00:00+05:30
---

To keep my site and [projects](/projects) up and running, there are costs. As a full-time college student, I work on projects in my free time.

So, please consider a donation for my future projects and the maintenance of all my sites & [projects](/projects).

If you would like to mention your website or a message to display on this page, please [send them to me](/contact) after donation. Every donation will be publicly mentioned here, if you'd like not to, please do contact after.

## UPI

You can send Indian UPI payments to `ssiby@fbl` (no transaction fees).

<div class="padlinks">
    <a class="demo href="upi://pay?pa=ssiby@fbl&pn=Subin%20Siby">Donate to ssiby@fbl</a>
</div>

## PayPal

You can send PayPal payments to `subins2000 [at] gmail [dot] com` (PayPal fees applicable) :

<div class="padlinks">
    <a class="demo" href="https://www.paypal.me/SubinSiby">Donate Via PayPal</a>
    <!--
    <br/><br/>
    <div class="download">
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="TXLUHEULFHWYN">
            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
        </form>
    </div>
    -->
</div>

## Liberapay

If you would like to support me with recurring payments, I'm on Liberapay. Liberapay is like Patreon.

<div class="padlinks">
    <a class="demo" href="https://liberapay.com/subins2000">Liberapay</a>
</div>

## Donations

Thank you very much to all the people who have supported me, this site and projects through donations. 🙂 ❤️❤️

<table>
<thead>
<tr>
<td>
<h2><a href="#date"><span id="date">Date</span></a></h2>
</td>
<td>
<h2><a href="#name-btc-address"><span id="name-btc-address">Name / BTC Address</span></a></h2>
</td>
<td>
<h2><a href="#amount"><span id="amount">Amount</span></a></h2>
</td>
</tr>
</thead>
<tbody>
<tr>
<td>2020/08/25</td>
<td>Fletcher Amelioration</td>
<td>10 USD</td>
</tr>
<tr>
<td>2019/03/07</td>
<td>Clyde Summers</td>
<td>50 GBP</td>
</tr>
<tr>
<td>2019/03/07</td>
<td>Christine Mann</td>
<td>10 USD</td>
</tr>
<tr>
<td>2017/02/18</td>
<td>Yves Monsel</td>
<td>5 EUR</td>
</tr>
<tr>
<td>2017/02/14</td>
<td>David Clifford</td>
<td>10 USD</td>
</tr>
<tr>
<td>2017/02/9</td>
<td>José María Montabes</td>
<td>3 USD</td>
</tr>
<tr>
<td>2017/02/01</td>
<td>Guidi Egnon</td>
<td>20 EUR</td>
</tr>
<tr>
<td>2017/01/05</td>
<td>Bruce Robertson</td>
<td>25 USD</td>
</tr>
<tr>
<td>2016/12/31</td>
<td>Alexander Jankuv</td>
<td>5 USD</td>
</tr>
<tr>
<td>2016/09/01</td>
<td>Sang Ho</td>
<td>5 USD</td>
</tr>
<tr>
<td>2016/07/05</td>
<td>Aristide Sere</td>
<td>4 USD</td>
</tr>
<tr>
<td>2016/07/02</td>
<td><a href="https://www.myetv.tv/" target="_blank">Oscar Cosimo</a></td>
<td>10 USD</td>
</tr>
<tr>
<td>2016/06/22</td>
<td><a href="http://lisbonbookings.com/" target="_blank">Joshua Anderson</a></td>
<td>100 USD</td>
</tr>
<tr>
<td>2016/01/10</td>
<td><a href="https://www.myetv.tv/" target="_blank">Oscar Cosimo</a></td>
<td>10 USD</td>
</tr>
<tr>
<td>2015/09/19</td>
<td><a href="http://hawkeye.net/" target="_blank">Thomas Haberland</a></td>
<td>30 USD</td>
</tr>
<tr>
<td>2015/09/12</td>
<td>Templaty</td>
<td>10 USD</td>
</tr>
<tr>
<td>2015/07/12</td>
<td><a href="http://lisbonbookings.com/" target="_blank">Milton Taft</a></td>
<td>50 USD</td>
</tr>
<tr>
<td>2015/06/08</td>
<td>Piotr Mrzygłowski</td>
<td>1 USD</td>
</tr>
<tr>
<td>2015/04/20</td>
<td>Omega4</td>
<td>10 USD</td>
</tr>
<tr>
<td>2015/02/26</td>
<td>Brian Brock</td>
<td>30 USD</td>
</tr>
<tr>
<td>2014/06/14</td>
<td>Brazil365</td>
<td>5 USD</td>
</tr>
<tr>
<td>2014/05/20</td>
<td>David Moore</td>
<td>20 USD</td>
</tr>
<tr>
<td>2014/05/09</td>
<td><a href="http://levlaz.blogspot.com/" target="_blank">Lev Lazinskiy</a></td>
<td>20 USD</td>
</tr></tbody>
</table>

 [1]: #paypal
 [2]: #cryptocurrency
