---
title: How to view or Download Google Drive files on your site
author: Subin Siby
type: post
date: 2013-01-04T14:07:00+00:00
excerpt: 'Many of you wanted to show images, videos etc... on your site which is uploaded on your Google Drive.The link of the file will not be the same at all times due to security reasons.&nbsp;So Google provides another method to show or download a file.For t...'
url: /how-to-view-or-download-google-drive-files-on-your-site
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
  - http://sag-3.blogspot.com/2013/01/how-to-view-or-download-google-drive.html
syndication_item_hash:
  - 4386039d780a01464dece408eb52caa0
  - 4386039d780a01464dece408eb52caa0
  - 4386039d780a01464dece408eb52caa0
  - 4386039d780a01464dece408eb52caa0
  - 4386039d780a01464dece408eb52caa0
  - 4386039d780a01464dece408eb52caa0
categories:
  - HTML
tags:
  - Google
  - Google Drive

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Many of you wanted to show images, videos etc&#8230; on your site which is uploaded on your Google Drive.</p> 
  
  <div>
    The link of the file will not be the same at all times due to security reasons.&nbsp;
  </div>
  
  <div>
    So Google provides another method to show or download a file.
  </div>
  
  <div>
  </div>
  
  <div>
    For this method you need to get the file id. For that open your file.&nbsp;
  </div>
  
  <div>
    Look at the url box of your browser.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-LK-tP7By0gQ/UObaHiNSCnI/AAAAAAAACYY/kiGA8HQbF-s/s1600/drive.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-LK-tP7By0gQ/UObaHiNSCnI/AAAAAAAACYY/kiGA8HQbF-s/s1600/drive.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    As shown in the image you should select the word before <b>/edit</b>&nbsp;and after <b>d/</b>.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    This selected text will be the file id. Copy the file id.
  </div>
  
  <h3 style="text-align: left;">
    <u>To View</u>
  </h3>
  
  <blockquote class="tr_bq">
    <p>
      https://drive.google.com/uc?export=view&id=<b><span style="color: red;">fileid</span></b>
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: left;">
    Replace the red text with the file id. You can use this url to show an image or any other file.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    An Example:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <img src="https://drive.google.com/uc?export=view&id=0B2VjYaTkCpiQT0tRSWQzVHMxR1U">
    </p>
  </blockquote>
  
  <h3 style="text-align: left;">
    <u>To Download</u>
  </h3>
  
  <blockquote class="tr_bq">
    <p>
      https://drive.google.com/uc?export=download&id=<b><span style="color: red;">fileid</span></b>
    </p>
  </blockquote>
  
  <div>
    <b><span style="color: red;"><br /></span></b>
  </div>
  
  <div>
    <div class="separator" style="clear: both;">
      Replace the red text with the file id. You can use this url to download a file.
    </div>
    
    <div class="separator" style="clear: both;">
      An Example:
    </div>
    
    <blockquote class="tr_bq">
      <p>
        <a href="https://drive.google.com/uc?export=download&id=0B2VjYaTkCpiQT0tRSWQzVHMxR1U">Download this file</a>
      </p>
    </blockquote>
    
    <div class="separator" style="clear: both;">
      This method helped me a lot for developing <a href="http://games-subins.hp.af.cm/" >Subins Games</a>.
    </div>
  </div>
</div>