---
title: Make Better WordPress Minify Work On OpenShift
author: Subin Siby
type: post
date: 2014-01-18T06:54:26+00:00
url: /make-bwp-work-on-openshift-wordpress-blog
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - CSS
  - PHP
  - WordPress
tags:
  - Plugin

---
If you run a **WordPress** blog on **OpenShift**, you might not be able to run the Minify plugins. These plugins won&#8217;t work if your OpenShift app is directly installed from the WordPress catridge they provide. This happens because the plugins, themes and other directories of wp-content are linked directories which means they&#8217;re fake. The folders are just shortcuts of the original folder situated in **app-root/data**. This makes the minify plugin hard to find the source of the file. Hence it won&#8217;t work.

This happened to my blog and I looked in to the source files of plugin and found a solution. You can solve this problem with only **5 lines **of code !


# Editing bwp-minify/min/index.php

The changes we are going to make to fix the problem are on the **bwp-minify/min/index.php** file of the plugin. You can use **SFTP** to edit the file or you can use the **Plugin Editor** of WordPress.

## SFTP

See the [tutorial][3] to know how to use File Transfer Protocol (FTP) to access the source code of you app running on OpenShift.

Connect to your site&#8217;s **FTP **server and go to the following location :

<pre class="prettyprint"><code>/var/lib/openshift/your-app-id/app-root/runtime/repo/php/wp-content/plugins/bwp-minify/min</code></pre>

Open the file **index.php** from the directory using your text editor.

## WordPress Plugin Editor

Go to **wp-admin/plugin-editor.php** page. Choose **Better Wordpress Minify** from "plugin to edit" field. Then choose

<pre class="prettyprint"><code>bwp-minify/min/index.php</code></pre>

from the files section shown on right side of window.

# Solution

Search the file for the following line :

<pre class="prettyprint"><code>$min_serveOptions['minifierOptions']['text/css']['symlinks']</code></pre>

Add the following code just above the line you just searched :

<pre class="prettyprint"><code>if(preg_match("/wp-content/",$_GET['f'])){
 $_SERVER['DOCUMENT_ROOT'] = "/var/lib/openshift/app-id/app-root";
 $_GET['f']=str_replace("wp-content/","data/",$_GET['f']);
 $_GET['f']=str_replace("wp-includes/","runtime/repo/php/wp-includes/",$_GET['f']);
}</code></pre>

Make sure that you have replaced the **app-id** with your application&#8217;s id. You can get your app id from the Web URL of your app on Openshift Console. An example URL :

<pre class="prettyprint"><code>https://openshift.redhat.com/app/console/application/527de613e0b8cdef8d000212-blog</code></pre>

From the above URL, the id of the app is **527de613e0b8cdef8d000212** found just after the "**application/"** and end at the "-" sign.

And the whole code of that area would be :

<pre class="prettyprint"><code>if(preg_match("/wp-content/",$_GET['f'])){
 $_SERVER['DOCUMENT_ROOT'] = "/var/lib/openshift/app-id/app-root";
 $_GET['f']=str_replace("wp-content/","data/",$_GET['f']);
 $_GET['f']=str_replace("wp-includes/","runtime/repo/php/wp-includes/",$_GET['f']);
}
$min_serveOptions['minifierOptions']['text/css']['symlinks'] = $min_symlinks;</code></pre>

Save the file. Now, Enable the plugin if it&#8217;s deactivated. Open your blog and you will see the magic. If there are any problems, then please comment it on the comments section below. I will be here to help you out.

 [1]: #editing-bwp-minifyminindexphp
 [2]: #sftp
 [3]: //subinsb.com/2013/10/how-to-use-filezillasftp-to-update-site-in-openshift
 [4]: #wordpress-plugin-editor
 [5]: #solution
