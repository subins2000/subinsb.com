---
title: 'How To Check If Internet Connection Exists In jQuery & Javascript'
author: Subin Siby
type: post
date: 2013-10-18T13:58:00+00:00
excerpt: "If you are running an app on the web that requires Internet Connection&nbsp;all the time, sometimes the internet connection will fail on the client side and your app won't properly work. The best example is the New Tab&nbsp;Chrome Apps&nbsp;that will d..."
url: /how-to-check-if-internet-connection-exists-in-jquery-javascript
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
  - http://sag-3.blogspot.com/2013/10/check-if-internet-connection-exist-jquery-javascript.html
dsq_thread_id:
  - 1958296506
  - 1958296506
  - 1958296506
  - 1958296506
  - 1958296506
  - 1958296506
syndication_item_hash:
  - e53f205d118f48bdb17cd39ac5aad9a3
  - e53f205d118f48bdb17cd39ac5aad9a3
  - e53f205d118f48bdb17cd39ac5aad9a3
  - e53f205d118f48bdb17cd39ac5aad9a3
  - e53f205d118f48bdb17cd39ac5aad9a3
  - e53f205d118f48bdb17cd39ac5aad9a3
categories:
  - JavaScript
  - jQuery
tags:
  - Chrome
  - featured
  - URL

---
<div dir="ltr" style="text-align: left;">
  If you are running an app on the web that requires <b>Internet Connection</b> all the time, sometimes the <b>internet connection</b> will fail on the client side and your app won&#8217;t properly work. The best example is the New Tab <b>Chrome Apps</b> that will display the <b>URL</b> icons <b>grayscale</b> when offline and will make the icons back to color when online. To check whether the client&#8217;s <b>internet connection</b> is online, we use <b>jQuery</b>. You can also use <b>Javascript</b> to check. I will tell you how to do the checking in both <b>jQuery</b> and <b>Javascript</b>.<br /> The checking is simple. We will send a request to one of the site&#8217;s image. If the request was successful,<b> </b>the <b>Internet Connection</b> is active and if it&#8217;s not successful, there isn&#8217;t any active <b>Internet Connection</b>. It&#8217;s as simple as that. The following modal will make you understand the concept easily :</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-LtDtdVE1roA/UmAavs_T_iI/AAAAAAAADNY/g0L-HAPlkTY/s1600/0060.png"><img alt="" src="//1.bp.blogspot.com/-LtDtdVE1roA/UmAavs_T_iI/AAAAAAAADNY/g0L-HAPlkTY/s1600/0060.png" border="0" /></a>
  </div>
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=rqzu9k13h8hdm9d&class=11">Download</a> <a class="demo" href="http://demos.subinsb.com/check-internet-connection-js-jquery">Demo</a>
  </div>
  
  <p>
    I have wrapped the checking into a function named <b>checkNetConnection</b>.
  </p>
  
  <h2 style="text-align: left;">
    <span style="font-size: large;">Javascript</span>
  </h2>
  
  <pre class="prettyprint"><code>function checkNetConnection(){
 var xhr = new XMLHttpRequest();
 var file = "http://yoursite.com/somefile.png";
 var r = Math.round(Math.random() * 10000);
 xhr.open('HEAD', file + "?subins=" + r, false);
 try {
  xhr.send();
  if (xhr.status &gt;= 200 && xhr.status &lt; 304) {
   return true;
  } else {
   return false;
  }
 } catch (e) {
  return false;
 }
}</code></pre>
  
  <h2 style="text-align: left;">
    <span style="font-size: large;">jQuery</span>
  </h2>
  
  <pre class="prettyprint"><code>function checkNetConnection(){
 jQuery.ajaxSetup({async:false});
 re="";
 r=Math.round(Math.random() * 10000);
 $.get("http://yoursite.com/somefile.png",{subins:r},function(d){
  re=true;
 }).error(function(){
  re=false;
 });
 return re;
}</code></pre>
  
  <p>
    In Both Codes above you have to change the <b>URL</b>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      http://yoursite.com/somefile.png
    </p>
  </blockquote>
  
  <p>
    to a small <b>Image</b> file location of your site. If you want a small size image (<b>137</b> bytes) go to <b><a href="https://demos.subinsb.com/cdn/dot.png">https://demos.subinsb.com/cdn/dot.png</a>.</b><br /> If you have any suggestions / problems report it to me via comments. I am here 10/7.
  </p>
</div>