---
title: 'Make Image Black & White (grayscale) using CSS In Webkit'
author: Subin Siby
type: post
date: 2012-11-02T04:28:00+00:00
excerpt: "Note that this will only work on browsers that supports webkit&nbsp;which is used by browsers like Google Chrome, Safari.DEMOHere's the CSS code which will make all img tags grayscale and will return to normal when mouse hover &nbsp;the image.img{-webk..."
url: /make-an-image-black-white-grayscale-using-css-in-webkit
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 7cb9914eb4db1a539333efbbe6b2dfd3
  - 7cb9914eb4db1a539333efbbe6b2dfd3
  - 7cb9914eb4db1a539333efbbe6b2dfd3
  - 7cb9914eb4db1a539333efbbe6b2dfd3
  - 7cb9914eb4db1a539333efbbe6b2dfd3
  - 7cb9914eb4db1a539333efbbe6b2dfd3
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
  - http://sag-3.blogspot.com/2012/11/grayscale-using-css.html
dsq_thread_id:
  - 1957389119
  - 1957389119
  - 1957389119
  - 1957389119
  - 1957389119
  - 1957389119
categories:
  - CSS
  - HTML
tags:
  - Chrome
  - Google
  - Safari
  - WebKit

---
<div dir="ltr" style="text-align: left;">
  If you want to make images grayscale ie black & white you can use this tutorial.
</div>

<div dir="ltr" style="text-align: left;">
  Note that this trick will only work on browsers that runs <b>webkit</b> which is used by browsers like <b>Google Chrome</b>, <b>Safari</b>, <b>Midori </b>etc.
</div>

<div dir="ltr" style="text-align: left;">
  Other browsers using different engines do not support the gray scake option. Hence this trick does not support all browsers.
</div>

<div dir="ltr" style="text-align: left;">
  <div class="padlinks">
    <a class="demo" href="http://jsfiddle.net/subins2000/AJcFU/show" target="_blank">DEMO</a>
  </div>
  
  <div style="text-align: left;">
    <figure id="attachment_2440" class="wp-caption aligncenter"><a href="//lab.subinsb.com/projects/blog/uploads/2012/11/0098.png"><img class="size-medium wp-image-2440" alt="Grayscaled Image Using CSS & Original Image" src="//lab.subinsb.com/projects/blog/uploads/2012/11/0098-300x151.png" width="300" height="151" /></a><figcaption class="wp-caption-text">Grayscaled Image Using CSS & Original Image</figcaption></figure>
  </div>
  
  <div style="text-align: left;">
    Here&#8217;s the CSS code which will make all images grayscale.</p> 
    
    <pre class="prettyprint"><code>img{
 -webkit-filter: grayscale(100%);
}</code></pre>
    
    <p>
      If you want to make the image normal when hovered, use the following code :
    </p>
    
    <pre class="prettyprint"><code>img:hover{
 -webkit-filter: grayscale(0%);
}</code></pre>
    
    <p>
      The gray scale effect amount is mentioned as value. "100%" is for full gray scale effect. Making the grayscale value "0%" would remove the grays scale effect on the image. The image will become normal when the value is changed to "0%" or if the value is not present at all.
    </p>
  </div>
</div>