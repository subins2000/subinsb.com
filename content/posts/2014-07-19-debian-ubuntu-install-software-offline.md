---
title: Install Softwares With Dependencies Without Internet
author: Subin Siby
type: post
date: 2014-07-19T06:08:36+00:00
url: /debian-ubuntu-install-software-offline
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Linux
  - Ubuntu
tags:
  - Debian
  - Download
  - Software
  - Software Center
  - Synaptic Package Manager
  - Tutorials

---
Debian Systems like Ubuntu and others&#8217; software packages has the extension **.deb**. With the file, you can install softwares on your system. While you click the "Install" button on Ubuntu Software Center or on Synaptic Package Manger, what the system actually do is downloading the .deb file from the server and installing it.

These .deb files are installed using the **dpkg** software. It&#8217;s not necessarily required for you to have internet to install softwares, but if you don&#8217;t have the dependencies of the software, internet is required for the software to download and install it.


## Introduction

If you&#8217;re a newbie to the Debian, Linux Platform, here&#8217;s what some of the terms mean :

  * Package &#8211; The Software File that is used to install a software. In Windows, it will be a file called "setup.exe"
  * Dependency &#8211; For working of some softwares, other softwares are needed. An Example : Google Chrome needs the WebKit Package for rendering Web pages
  * ".deb" files &#8211; The install Package is of the extension ".deb" as opposed to ".exe" in Windows
  * dpkg &#8211; A command line (Terminal) tool / software to install ".deb" packages.
  * Synaptic Package Manager &#8211; A Graphical (GUI) application for installing softwares in Linux systems. Will take care of everything for the installation of software
  * Command Line &#8211; The Terminal. On some systems, it can be opened using CTRL + ALT + T key combination.

This tutorial will help you to do these following things :

  * Install Softwares in your Debian (Ubuntu) system Without Internet (Offline)
  * Install Dependencies with Original Software Offline
  * Getting the Offline **.deb** Packages

## Obtaining The <strong>.deb</strong> Package

As I said before, Software Center & Synaptic Package Manger downloads the **.deb** files to a folder on your system. The folder is :

<pre class="prettyprint"><code>/var/cache/apt/archives</code></pre>

If the packages are completely downloaded, it&#8217;s file is seen there. If it&#8217;s only partially downloaded, it&#8217;s seen in the **partial** folder.

We only need the fully downloaded packages. On installing a fresh System, this folder will be empty. So, you can install a software via Software Center and see it&#8217;s package  and dependency packages (maybe) in the folder.

If it have dependencies, you have to copy the whole .deb files and paste it in a single folder or if it&#8217;s a single file, you only have to copy that file.

An another way (complicated) is to get the ".deb" file from internet. Though it is risky, because you need to know the architecture of your system and find & download dependencies manually. Here are the site addresses :

  * Debian &#8211; https://www.debian.org/distrib/packages
  * Ubuntu &#8211; http://packages.ubuntu.com/

Debian & Ubuntu have some major differences although they have many similarities. Don&#8217;t install packages of Debian in Ubuntu, because it can cause unexpected Results.

I highly recommend you use Synaptic Package Manger. If your system doesn&#8217;t have it, install it using the following terminal command :

<pre class="prettyprint"><code>sudo apt-get install synaptic</code></pre>

## Install a Single Software Offline

Suppose, the software you&#8217;re going to install is **Firefox**. Gladly, it doesn&#8217;t require any dependencies, because it&#8217;s already in the system.

The Package File name is "firefox-30-lucid-32.deb" and it&#8217;s situated in the "/home/simsu" directory. We will use the following command in Terminal to install this software :

<pre class="prettyprint"><code>sudo dpkg -i /home/simsu/firefox-30-lucid-32.deb</code></pre>

The installation status will be automatically printed. If there&#8217;s any dependency issue, it will also be printed out. So, you have to install the dependency package name mentioned using Synaptic.

## Install Software With Dependencies Offline

We have a folder named "firefox" in the directory "/home/simsu" with dependencies&#8217; packages and the main "firefox-30-lucid-32.deb" package files. We will pint the terminal directory to the folder and use a single command to install software with the dependencies that are in the same folder.

First, point the terminal to the directory :

<pre class="prettyprint"><code>cd /home/simsu/firefox</code></pre>

and tell **dpkg** to install all softwares that are in the directory :

<pre class="prettyprint"><code>dpkg -i *.deb</code></pre>

The softwares will be installed one by one. Though, there may be some dependency issues, because some files won&#8217;t be installed in the right order. In this case, do the command again one or 3 times until everything is OK.

 [1]: #introduction
 [2]: #obtaining-thedebpackage
 [3]: #install-a-single-software-offline
 [4]: #install-software-with-dependencies-offline
