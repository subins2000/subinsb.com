---
title: 'Word Palindrome Check in PHP & Python'
author: Subin Siby
type: post
date: 2014-10-27T03:30:50+00:00
url: /palindrome-check-php-python
categories:
  - PHP
  - Python
  - Short Post
tags:
  - String

---
<a href="http://en.wikipedia.org/wiki/Palindrome" target="_blank">Palindromes</a> are unique words that are read the same forward and backward. We can identify them using out eyes and brain. How about we identify using a simple program in **PHP** & **Python** ?

## PHP

<pre class="prettyprint"><code>&lt;?php
$word = strtolower("Malayalam");
$splitted = str_split($word);
$reversedWord = "";
$length = strlen($word);
for($i = 0; $i &lt; $length; $i++)
	$reversedWord .= $splitted[$length - $i - 1];
echo $word == $reversedWord ? "It's a palindrome !n" : "It's not a palindromen";
?&gt;
</code></pre>

Instead of the loop to reverse the word, you can also use :

<pre class="prettyprint"><code>$reversedWord = strrev($word)
</code></pre>

instead of :

<pre class="prettyprint"><code>$splitted = str_split($word);
$reversedWord = "";
$length = strlen($word);
for($i = 0; $i &lt; $length; $i++)
         $reversedWord .= $splitted[$length - $i - 1];
</code></pre>

A line break is added to the output to make output neat if the program is ran via terminal.

## Python

<pre class="prettyprint"><code>word = "Malayalam"
word = word.lower()
reversedWord = "";
for i in range(1, len(word) + 1, 1):
	reversedWord += word[-i]
if word == reversedWord :
	print "It's a palindrome !"
else:
	print "It's not a palindrome"</code></pre>

The input word should be given to the variable "word" in both the programs and both will output "It&#8217;s a palindrome" if the given word is a palindrome and if not, outputs "It&#8217;s not a palindrome".