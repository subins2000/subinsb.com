---
title: 'Create Live Group Chat With PHP, jQuery & WebSocket'
author: Subin Siby
type: post
date: 2014-09-16T03:30:23+00:00
url: /live-group-chat-with-php-jquery-websocket
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - HTML5
  - JavaScript
  - jQuery
  - PHP
tags:
  - Server
  - Tutorials
  - WebSocket

---
With the introduction of **HTML5**, a new technology was evolved in **2011** called **WebSockets**. This technology enables live connection with the server even after the page finished loading. It&#8217;s a better, replaceable version of **AJAX** for client to server communication in the background.

With WebSocket technology, its possible to have a direct communication between server and client without any interruption and faster data transmission.

So, to demonstrate this to me and for you, We&#8217;re going to create a live group chat with **PHP**, **jQuery** with the help of **WebSockets**.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=s/qkuz7ln0zloqslfvzs67&class=30" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/php/websocketChat" target="_blank">Demo</a>
</div>


## Introduction

Here are the main components of the chat we&#8217;re going to create :

  * <a href="http://code.jquery.com/jquery-2.1.1.min.js" target="_blank">jQuery 2.1.1</a>
  * <a href="http://demos.subinsb.com/php/websocketChat/cdn/ws.js" target="_blank">jQuery WebSocket plugin</a>
  * PHP 5.3.9 or later
  * <a href="http://socketo.me/docs/hello-world" target="_blank">PHP Ratchet Library</a>

You should download the libraries from the hyperlinks of the above components. You should use **Composer** for downloading the Socketo library.

## Directory tree

There are only two directories, "cdn" and "inc". The client side files like **JavaScript** and **CSS** are in the "cdn" directory. The **PHP** files for the **WebSocket** server is in the "inc" directory.

Note that, you should download **Ratchet** library using **Composer** in the "inc" directory. So, there will be a subfolder in the "inc" directory called "vendor".

## Database

We only need one table for storing messages called "wsMessages" :

<pre class="prettyprint"><code>CREATE TABLE IF NOT EXISTS `wsMessages` (
    `name` varchar(20) NOT NULL,
    `msg` text NOT NULL,
    `posted` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;</code></pre>

## The Interface

First, we are going to make the interface ie the **HTML**. This is the **index.php** file :

<pre class="prettyprint"><code>&lt;?php
include "config.php";
?&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;
	&lt;head&gt;
		&lt;script src="cdn/jquery.js"&gt;&lt;/script&gt;
		&lt;script src="cdn/ws.js"&gt;&lt;/script&gt;
		&lt;script src="cdn/chat.js"&gt;&lt;/script&gt;
		&lt;link href="cdn/chat.css" rel="stylesheet"/&gt;
		&lt;title&gt;PHP Group Chat With jQuery & WebSocket&lt;/title&gt;
	&lt;/head&gt;
	&lt;body&gt;
		&lt;div id="content" style="margin-top:10px;height:100%;"&gt;
			&lt;center&gt;&lt;h1&gt;Live Group Chat In PHP&lt;/h1&gt;&lt;/center&gt;
			&lt;div class="chatWindow"&gt;
				&lt;div class="users"&gt;&lt;/div&gt;
				&lt;div class="chatbox"&gt;
					&lt;div class="status"&gt;Offline&lt;/div&gt;
					&lt;div class="chat"&gt;
						&lt;div class="msgs"&gt;&lt;/div&gt;
	 					&lt;form id="msgForm"&gt;
							&lt;input type="text" size="30" /&gt;
							&lt;button&gt;Send&lt;/button&gt;
						&lt;/form&gt;
					&lt;/div&gt;
					&lt;div class="login"&gt;
						&lt;p&gt;Type in your name to start chatting !&lt;/p&gt;
						&lt;form id="loginForm"&gt;
							&lt;input type="text" /&gt;
							&lt;button&gt;Submit&lt;/button&gt;
						&lt;/form&gt;
					&lt;/div&gt;
				&lt;/div&gt;
			&lt;/div&gt;
		&lt;/div&gt;
	&lt;/body&gt;
&lt;/html&gt;</code></pre>

As you can see, there&#8217;s a chat box and a login box with online users&#8217; display box. We load **jQuery**, **ws.js**, **chat.js** and **chat.css** in this page. Also, this is the only page the user needs to see for everything.

## jQuery

jQuery must be of version **2.1.1**. But, I think it will be compatible with versions **1.7** or more.

## ws.js

The **jQuery** websocket plugin we mentioned in the Introduction of this post must be in this file.

## chat.js

This is the file which connect to WebSocket server, login the user, sends messages and all the things that does on the client side.

<pre class="prettyprint"><code>window.scTop = function(){
	$(".chatWindow .chatbox .msgs").animate({
		scrollTop : $(".chatWindow .chatbox .msgs")[0].scrollHeight
	});
};
window.connect = function(){
	window.ws = $.websocket("ws://127.0.0.1:8080/", {
		open: function() {
			$(".chatWindow .chatbox .status").text("Online");
			ws.send("fetch");
		},
		close: function() {
			$(".chatWindow .chatbox .status").text("Offline");
		},
		events: {
			fetch: function(e) {
				$(".chatWindow .chat .msgs").html('');
				$.each(e.data, function(i, elem){
					$(".chatWindow .chat .msgs").append("&lt;div class='msg' title='"+ elem.posted +"'&gt;&lt;span class='name'&gt;"+ elem.name +"&lt;/span&gt; : &lt;span class='msgc'&gt;"+ elem.msg +"&lt;/span&gt;&lt;/div&gt;");
				});
				scTop();
			},
			onliners: function(e){
				$(".chatWindow .users").html('');
				$.each(e.data, function(i, elem){
					$(".chatWindow .users").append("&lt;div class='user'&gt;"+ elem.name +"&lt;/div&gt;");
				});
			},
			single: function(e){
				var elem = e.data;
				$(".chatWindow .chat .msgs").append("&lt;div class='msg' title='"+ elem.posted +"'&gt;&lt;span class='name'&gt;"+ elem.name +"&lt;/span&gt; : &lt;span class='msgc'&gt;"+ elem.msg +"&lt;/span&gt;&lt;/div&gt;");
				scTop();
			}
		}
	});
};
$(document).ready(function(){
	$(".chatWindow .chat #msgForm").on("submit", function(e){
		e.preventDefault();
		var form = $(this);
		var val	 = $(this).find("input[type=text]").val();
		if(val != ""){
			ws.send("send", {"msg": val});
			form[0].reset();
		}
	});
	$(".chatWindow .login #loginForm").on("submit", function(e){
		e.preventDefault();
		var val	 = $(this).find("input[type=text]").val();
		if(val != ""){
			ws.send("register", {"name": val});
			ws.send("fetch");
			$(".chatWindow .login").fadeOut(1000, function(){
				$(".chatWindow .chat").fadeIn(1000, function(){
					scTop();
					$(".chatWindow .chat #msgForm input[type=text]").focus();
				});
			});
		}
	});
	$(".chatWindow .chatbox .status").on("click", function(){
		if($(this).text() == "Offline"){
			connect();
		}
	});
	setInterval(function(){
		ws.send("onliners");
	}, 4000);
	connect();
});</code></pre>

We connect to server using **window.connect()** function which can also be called as **connect()** function and the **WebSocket** plugin object is stored in **window.ws** variable.

The URL of the server is mentioned in the above script as "127.0.0.1:8080". We will get to it later.

There is a function that is called every 4 seconds, to update the user&#8217;s online presence and fetching the online users from the server.

But, there might be some errors in the console, because this interval function is sometimes called before the connection to the server is established. So, don&#8217;t care about those errors.

There is also a small box which shows the status of the connection between server and client. If it&#8217;s "Offline" and the user clicks it, **connect()** function is called to try again to establish a connection.

This file also has code that responds with the messages the server sends to the client like online status of users, messages sent by other users etc.. This can be seen in the value of **events** key of **JSON** array in **$.websocket** function.

## chat.css

Now, let&#8217;s style the chat page :

<pre class="prettyprint"><code>.chatWindow .users, .chatWindow .chatbox{
	display:inline-block;
	vertical-align:top;
	height:330px;
	padding: 0px 15px;
	position:relative;
}
.chatWindow .users{
	background: #6AAEEC;
	color:white;
	width: 100px;
	padding: 10px 15px;
	overflow-y:auto;
}
.chatWindow .chatbox{
	color:black;
	width: 330px;
}
.chatWindow .chatbox .status{
	background: #ABC8F8;
	padding: 5px 15px;
}
.chatWindow .chatbox .chat {
	display: none;
	background:#fff;
}
.chatWindow .chatbox .chat .msgs{
	border-top: 1px solid black;
	border-bottom: 1px solid black;
	overflow-y: auto;
	height: 300px;
}
.chatWindow .chatbox #msgForm{
	padding-top: 1.5px;
}
.chatWindow .msgs .msg, .chat .users .user{
	border-bottom:1px solid black;
	padding:4px 10px;
	white-space:pre-line;
	word-break:break-word;
}</code></pre>

All the element selectors in the stylesheet starts with **.chatWindow** to avoid applying **CSS** rules to other elements.

## config.php

The configuration for connecting to database is in here. Also, this file is used / required by some of the other **PHP** files. Each time, this file is called, it sends a request to start the **WebSocket** server if it&#8217;s not already started.

<pre class="prettyprint"><code>&lt;?php
// ini_set("display_errors","on");
$docRoot	= realpath(dirname(__FILE__));
if( !isset($dbh) ){
	session_start();
	date_default_timezone_set("UTC");
	$musername 	= "root";
	$mpassword 	= "backstreetboys";
	$hostname  	= "localhost";
	$dbname    	= "test";
	$dbh		= new PDO("mysql:dbname={$dbname};host={$hostname};port={$port}",$musername, $mpassword);
	/* Change The Credentials to connect to database. */
	include_once "$docRoot/inc/startServer.php";
}
?&gt;</code></pre>

The files you&#8217;re going to see below should be in the "inc" directory.

## serverStatus.txt

A normal text file. This file is used to determine whether the server was started (1) or it isn&#8217;t running currently (0). As a default value, set the file content to "0". It is required for this file to have the permission of **Read & Write** (0666).

## class.chat.php

The operations done when a user connects to the server and all the actions done between the server and client is in this file :

<pre class="prettyprint"><code>&lt;?php
use RatchetMessageComponentInterface;
use RatchetConnectionInterface;

class ChatServer implements MessageComponentInterface {
	protected $clients;
	private $dbh;
	private $users = array();
	
	public function __construct() {
        global $dbh, $docRoot;
        $this-&gt;clients 	= new SplObjectStorage;
        $this-&gt;dbh 		= $dbh;
        $this-&gt;root 	= $docRoot;
    }
	
	public function onOpen(ConnectionInterface $conn) {
		$this-&gt;clients-&gt;attach($conn);
		$this-&gt;send($conn, "fetch", $this-&gt;fetchMessages());
		$this-&gt;checkOnliners();
		echo "New connection! ({$conn-&gt;resourceId})n";
	}

	public function onMessage(ConnectionInterface $from, $data) {
		$id	  = $from-&gt;resourceId;
		$data = json_decode($data, true);
		if(isset($data['data']) && count($data['data']) != 0){
			$type = $data['type'];
			$user = isset($this-&gt;users[$id]) ? $this-&gt;users[$id]['name'] : false;
			if($type == "register"){
				$name = htmlspecialchars($data['data']['name']);
				$this-&gt;users[$id] = array(
					"name" 	=&gt; $name,
					"seen"	=&gt; time()
				);
			}elseif($type == "send" && $user !== false){
				$msg = htmlspecialchars($data['data']['msg']);
				$sql = $this-&gt;dbh-&gt;prepare("INSERT INTO `wsMessages` (`name`, `msg`, `posted`) VALUES(?, ?, NOW())");
				$sql-&gt;execute(array($user, $msg));
				foreach ($this-&gt;clients as $client) {
					$this-&gt;send($client, "single", array("name" =&gt; $user, "msg" =&gt; $msg, "posted" =&gt; date("Y-m-d H:i:s")));
				}
			}elseif($type == "fetch"){
				$this-&gt;send($from, "fetch", $this-&gt;fetchMessages());
			}
		}
		$this-&gt;checkOnliners($from);
	}

	public function onClose(ConnectionInterface $conn) {
		if( isset($this-&gt;users[$conn-&gt;resourceId]) ){
			unset($this-&gt;users[$conn-&gt;resourceId]);
		}
		$this-&gt;clients-&gt;detach($conn);
	}

	public function onError(ConnectionInterface $conn, Exception $e) {
		$conn-&gt;close();
	}
	
	/* My custom functions */
	public function fetchMessages(){
		$sql = $this-&gt;dbh-&gt;query("SELECT * FROM `wsMessages`");
		$msgs = $sql-&gt;fetchAll();
		return $msgs;
	}
	
	public function checkOnliners($curUser = ""){
		date_default_timezone_set("UTC");
		if( $curUser != "" && isset($this-&gt;users[$curUser-&gt;resourceId]) ){
			$this-&gt;users[$curUser-&gt;resourceId]['seen'] = time();
		}
		
		$curtime 	= strtotime(date("Y-m-d H:i:s", strtotime('-5 seconds', time())));
		foreach($this-&gt;users as $id =&gt; $user){
			$usertime 	= $user['seen'];
			if($usertime &lt; $curtime){
				unset($this-&gt;users[$id]);
			}
		}
		
		/* Send online users to evryone */
		$data = $this-&gt;users;
		foreach ($this-&gt;clients as $client) {
			$this-&gt;send($client, "onliners", $data);
		}
	}
	
	public function send($client, $type, $data){
		$send = array(
			"type" =&gt; $type,
			"data" =&gt; $data
		);
		$send = json_encode($send, true);
		$client-&gt;send($send);
	}
}
?&gt;</code></pre>

Since the server is always running, we store the online users in an array variable inside the **ChatServer** class called "users". Each user that connects to the server has a unique connection code. This is also added within the array of variable. But, a new item is only added when the registration data is sent to server. An example item of **ChatServer::users** array :

<pre class="prettyprint"><code>15 =&gt; array(
 "name" =&gt; "Subin",
 "seen" =&gt; 1410624788
)</code></pre>

The "seen" key is for storing the timestamp of the users&#8217; last online presence. If this time is less than that of 5 seconds of current time, the user is removed as he is offline. See **ChatServer::checkOnliners()** function for seeing this.

The user registration, message sending and message fetching are all taken care by the **ChatServer::onMessage()** function.

The messages sent by the user and the name of user is filtered for **HTML** code to prevent **XSS** injection.

There are no cookie or session in this. If the user closes the browser window, then he/she is eliminated from the online users and have to re register for chatting again.

**ChatServer::checkOnliners()** also updates the "seen" timestamp value of the current user each time it&#8217;s called.

## server.php

This is the script that starts the **WebSocket** server :

<pre class="prettyprint"><code>&lt;?php
use RatchetServerIoServer;
use RatchetHttpHttpServer;
use RatchetWebSocketWsServer;

function shutdown(){
	global $docRoot;
	file_put_contents("$docRoot/inc/serverStatus.txt", "0");
	require_once "$docRoot/inc/startServer.php";
}
register_shutdown_function('shutdown');
if( isset($startNow) ){
	require_once "$docRoot/inc/vendor/autoload.php";
	require_once "$docRoot/inc/class.chat.php";
	$server = IoServer::factory(
          new HttpServer( 
            new WsServer(
              new ChatServer()
            )
          ), 
          8080,
          "127.0.0.1"
        );
	$server-&gt;run();
}
?&gt;</code></pre>

The above script listens requests to the port **8080** of IP **127.0.0.1** or localhost. So, any request to **127.0.0.1:8080** is processed by the above script. If you want to change the port and URL, change it in this file and also in the **cdn/chat.js** file.

In any case the above script was terminated, it automatically starts back due to the callback used in the **register\_shutdown\_function()**. This file can&#8217;t operate on it&#8217;s own, because it needs **config.php** which has been not yet included in this file, because actually this file is used by another one called **bg.php**.

## bg.php

To run the server, this file should be ran :

<pre class="prettyprint"><code>&lt;?php
require_once realpath(__DIR__) . "/../config.php";
$startNow = 1;
include_once "$docRoot/inc/server.php";
?&gt;</code></pre>

You can run this file by the following command in **Linux** :

<pre class="prettyprint"><code>php bg.php</code></pre>

## startServer.php

The server is automatically started when the user visits the chat page if it&#8217;s not already started. This file starts the server if it&#8217;s not started in the background. This file is called by the **config.php** file.

<pre class="prettyprint"><code>&lt;?php
require_once realpath(dirname(__DIR__)) . "/config.php";
$statusFile = "$docRoot/inc/serverStatus.txt";
$status = file_get_contents($statusFile);
if($status == "0"){
	/* This means, the WebSocket server is not started. So we, start it */
	function execInbg($cmd) { 
		if (substr(php_uname(), 0, 7) == "Windows"){ 
			pclose(popen("start /B ". $cmd, "r"));  
		} else { 
			exec($cmd . " &gt; /dev/null &");   
		} 
	}
	execInbg("php $docRoot/inc/bg.php");
	file_put_contents($statusFile, 1);
}
?&gt;</code></pre>

If the content of the "serverStatus.txt" file is "0", then a command is executed by this file to run the **bg.php** file in the background. This means that the server started running in the background. If it started running, then the file&#8217;s contents is changed to "1" and this file won&#8217;t do anything if the value is "1".

The command executed by this script is different for both **Linux** and** Windows**.

Thus the server is started and the client can start communicating with the server and the chatting works. This tutorial is completed. If you encountered any problems, please comment and I&#8217;ll be here (most of the times) to help you. Cheers ! 🙂

 [1]: #introduction
 [2]: #directory-tree
 [3]: #database
 [4]: #the-interface
 [5]: #jquery
 [6]: #wsjs
 [7]: #chatjs
 [8]: #chatcss
 [9]: #configphp
 [10]: #serverstatustxt
 [11]: #classchatphp
 [12]: #serverphp
 [13]: #bgphp
 [14]: #startserverphp
