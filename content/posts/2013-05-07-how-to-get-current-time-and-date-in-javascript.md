---
title: How to get Current Time and date in Javascript
author: Subin Siby
type: post
date: 2013-05-07T05:47:00+00:00
excerpt: 'This tutorial will help you to get current date and time at which the function is executed. Here is the function:function ctime(){var currentdate = new Date();var time=currentdate.getFullYear()+"-"+(currentdate.getMonth()+1)+"-"+currentdate.getDate()+"...'
url: /how-to-get-current-time-and-date-in-javascript
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - ac85fa73488125d3842c935709f89652
  - ac85fa73488125d3842c935709f89652
  - ac85fa73488125d3842c935709f89652
  - ac85fa73488125d3842c935709f89652
  - ac85fa73488125d3842c935709f89652
  - ac85fa73488125d3842c935709f89652
syndication_permalink:
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
  - http://sag-3.blogspot.com/2013/05/get-current-date-time-javascript.html
categories:
  - JavaScript
tags:
  - Date
  - Function
  - Hour
  - Minute
  - Month
  - Second
  - Year

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This tutorial will help you to get current date and time at which the function is executed. Here is the function:</p> 
  
  <blockquote class="tr_bq">
    <p>
      function ctime(){<br />var currentdate = new Date();<br />var time=currentdate.getFullYear()+"-"+(currentdate.getMonth()+1)+"-"+currentdate.getDate()+" "+currentdate.getHours()+":"+currentdate.getMinutes()+":"+currentdate.getSeconds();<br />return time;<br />}
    </p>
  </blockquote>
  
  <div>
    <div>
      Just print out <b>ctime()</b>&nbsp;function to get the date and time in the format <b>YEAR/MONTH/DAY HOUR:MINUTE:SECOND&nbsp;</b>
    </div>
  </div>
  
  <div>
    <b><br /></b>
  </div>
  
  <div>
    <u><b><span style="font-size: x-large;">Explanation :</span></b></u>
  </div>
  
  <div>
    <b><br /></b>
  </div>
  
  <div>
    <b>new Date()</b>&nbsp;function prints out the current date and time in a disorderly way. You can order it in any way as I did in the <b>time()</b>&nbsp;function.
  </div>
  
  <div>
  </div>
  
  <div>
    <b>currentdate.getFullYear()</b> will get the year from <b>new Date()</b>&nbsp;which is in the variable currentdate
  </div>
  
  <div>
  </div>
  
  <div>
    <b>currentdate.getMonth() </b>will get the month from&nbsp;<b>new Date() </b>function. The program <b>adds 1</b> to the month because <b>Javascript</b>&nbsp;always count the month from <b></b>. So we should add a&nbsp;<b>+1</b>.
  </div>
  
  <div>
  </div>
  
  <div>
    <b>currentdate.getDate() </b>will get the day of the month in which the function is executed.
  </div>
  
  <div>
  </div>
  
  <div>
    <b>currentdate.getHours() </b>will give the <b>Hour</b>&nbsp;of the time.
  </div>
  
  <div>
  </div>
  
  <div>
    <b>currentdate.getMinutes() </b>will give the Minute in which the script is executed.
  </div>
  
  <div>
  </div>
  
  <div>
    <b>currentdate.getSeconds() </b>This function will give the seconds of the time at which the script is executed.
  </div>
</div>