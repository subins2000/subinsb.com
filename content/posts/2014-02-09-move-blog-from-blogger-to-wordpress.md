---
title: Move Blog From Blogger To WordPress With Posts Redirection
author: Subin Siby
type: post
date: 2014-02-09T06:09:44+00:00
url: /move-blog-from-blogger-to-wordpress
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Blogger
  - WordPress
tags:
  - Posts
  - Redirect

---
If you are fed up with Blogger and wants to move to WordPress, you took a right decision. When migrating, you have to make sure that the posts  on your Blogger blog should redirect to the post on the new domain. This might get tricky. If it get tricky, you will lose your visitors. You can find the step by step instructions to properly migrate a Blogger Blog To WordPress with a method to redirect old blog posts to your new blog posts.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/WordPress/blogger-2-wordpress.zip">Download</a><a class="demo" href="http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html">Demo</a>
</div>

# Import Blogger Posts

The default Blogger Posts Importer can be buggy. It won&#8217;t work sometimes. So you should look for an alternative.

There is a way to import posts from the RSS feed of Blogger blog. We are going forward with this way. Go to **WordPress -> Plugins -> Add** **New**, search for "FeedWordpress" plugin, install and activate it. Now we should get the feed URL of your blogger blog.

Go to your Blogger blog&#8217;s **Settings** -> **Other** page. Choose "Full" for "Allow Blog Feed". This will make sure your blog have a feed link. Make sure you removed the "Post Feed Redirect URL". If there is a redirection, then we can&#8217;t do our work. So remove redirection if there is.

A blogger blog have a feed URL like this :

<pre class="prettyprint"><code>http://yourblog.blogspot.com/feeds/posts/default?max-results=1000&alt=rss</code></pre>

We will mention this URL in Feed WordPress plugin. For that go to **Syndication -> Syndicated Sites** :<figure id="attachment_2419" class="wp-caption aligncenter">

[<img class="size-full wp-image-2419" title="Syndication Menu" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0092.png" alt="Syndication Menu" width="175" height="295" />][1]<figcaption class="wp-caption-text">Syndication Menu</figcaption></figure> 

Add the blog feed URL we found earlier in to the "New Source" field seen on right side of window and click on "Add" button.

When it asks to choose "Atom 1.0" or "RSS 2.0", choose the "RSS 2.0" option.

<p style="text-align: center;">
  <a href="//lab.subinsb.com/projects/blog/uploads/2014/02/0093.png"><img class=" wp-image-2420 aligncenter" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0093.png" alt="Choose RSS 2.0" /></a>
</p>

Then, go to **Syndication -> Posts & Links** page. Choose "The local copy on this website" option under "Links" section. When we are going to import the posts, the settings on this page will affect the posts. So, be careful while changing settings.

Under "Custom Post Settings" section add a custom field with key :

<pre class="prettyprint"><code>blogger_permalink</code></pre>

and value as :

<pre class="prettyprint"><code>$(item/link)</code></pre>

Also you have to add "blogger_blog" key with any value.

<p style="text-align: center;">
  <a href="//lab.subinsb.com/projects/blog/uploads/2014/02/0094.png"><img class=" wp-image-2421 aligncenter" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0094.png" alt="Syndication Custom Post Settings" /></a>
</p>

Save the changes. Now go back to **Syndication -> Syndicated Sites** page. Click on the "Update Now" button aside to your blog feed item. It will take some time to update and create posts. Wait until all posts are imported. Feed WordPress plugin&#8217;s purpose is done. You can deactivate or remove the plugin. We don&#8217;t need it anymore.

# Redirection

Download the "Blogger 2 WordPress" plugin from [here][2] or from the WordPress Plugin Directory, search for "Blogger 2 WordPress".

The plugin is also available in these languages :

Spanish & Serbian by <a href="http://firstsiteguide.com" target="_blank">Ogi Djuraskovic<br /> </a>

Indonesian by [Jordan Silaen][3]

After activating the plugin, Go to the plugin page from **Plugins -> Blogger 2 WordPress** and follow the instructions of the plugin.

You will get a template code. It&#8217;s better to do a backup of the current template code of your blog in case something goes wrong.

Go to the **Template **tab of your Blogger blog and choose **Edit HTML**. Paste the new code got from the plugin in the textarea.<figure id="attachment_2432" class="wp-caption aligncenter">

[<img class="wp-image-2432" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0095.png" alt="Get Blogger classic Template Code" width="765" height="482" />][4]<figcaption class="wp-caption-text">Don&#8217;t mind this image as it is of an older version of the plugin. But the newer version also look something like this.</figcaption></figure> 

[<img class="aligncenter wp-image-2883 size-full" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0122.png" alt="0122" width="1114" height="253" />][5]

If you want to redirect pages as well, go to the "Blogger 2 WordPress" plugin page and add the pages as JSON data :

<p style="text-align: center;">
  <a href="//lab.subinsb.com/projects/blog/uploads/2014/02/0097.png"><img class=" wp-image-2434 aligncenter" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0097.png" alt="Add Pages For Redirection" /></a>
</p>

A JSON data example :

<pre class="prettyprint"><code>{
 "http://subinsb.com/ask" : "/p/ask.html", 
 "http://subinsb.com/about" : "/p/about.html"
}</code></pre>

Note that the current WordPress page URL should be mentioned first as key and the old URL mentioned second as value. Separate items using comma. Also it&#8217;s better to add just the pathname of the page URL, because there are multiple country domain TLD&#8217;s in a Blogger blog (au, in, com).

Now when you go to your Blogger Post, it will be redirected to your new WordPress Post URL.

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0092.png
 [2]: http://demos.subinsb.com/WordPress/blogger-2-wordpress.zip
 [3]: http://ChameleonJohn.com
 [4]: //lab.subinsb.com/projects/blog/uploads/2014/02/0095.png
 [5]: //lab.subinsb.com/projects/blog/uploads/2014/02/0122.png