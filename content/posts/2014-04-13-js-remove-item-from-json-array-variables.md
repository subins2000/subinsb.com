---
title: Delete Items From JSON or Remove Variables in JavaScript
author: Subin Siby
type: post
date: 2014-04-13T06:20:13+00:00
url: /js-remove-item-from-json-array-variables
authorsure_include_css:
  - 
  - 
  - 
categories:
  - JavaScript

---
In **PHP**, you can directly remove an item from an array using **unset** function. But there is no **unset** function in JavaScript. But there is another function that will remove a variable. It&#8217;s called **delete**. Removing variables isn&#8217;t the only thing it does. It can also remove items from a **JSON** array in **JavaScript**.

It&#8217;s usage is simple. Just pass the array variable with the name of the key. Like this :

<pre class="prettyprint"><code>delete arr[keyname];</code></pre>

## Examples

This is the array we&#8217;ll use for examples :

<pre class="prettyprint"><code>var arr = {
 "Gohan" : "Yipee",
 "Vegeta" : "Goku"
}</code></pre>

We first delete the key "Gohan" :

<pre class="prettyprint"><code>delete arr["Gohan"];</code></pre>

Now, when you output the array **arr**, you will not see the key "Gohan" with the value "Yipee". Here&#8217;s how the array will become after the above code is executed :

<pre class="prettyprint"><code>{
 "Vegeta" : "Goku"
}</code></pre>

Easy as that. You can also use **delete** for removing variables and functions. But you can&#8217;t remove the variable "window". Example :

<pre class="prettyprint"><code>var uid = "abc";
delete uid;</code></pre>

Now when you access the variable "uid", you will get the "undefined" error :

<pre class="prettyprint"><code>console.log(uid);
// ReferenceError: uid is not defined</code></pre>

**delete** returns "true" when the deleting was successful and "false" when not successful. You will get "false" when you try to delete variables like **window**.