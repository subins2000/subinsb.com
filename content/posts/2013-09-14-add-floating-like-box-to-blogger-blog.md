---
title: Add Floating Like Box to Blogger Blog
author: Subin Siby
type: post
date: 2013-09-14T06:29:00+00:00
excerpt: "Every Success Blog was popularized with the social integration such as Like Box, Share Box, Commenting and others. The social sharing widget will help other users to reach your blog. So, In this tutorial I'm going to tell you how to add a Floating&nbsp..."
url: /add-floating-like-box-to-blogger-blog
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - eae1df7b8a6046e20f5c1c7bf0b33400
  - eae1df7b8a6046e20f5c1c7bf0b33400
  - eae1df7b8a6046e20f5c1c7bf0b33400
  - eae1df7b8a6046e20f5c1c7bf0b33400
  - eae1df7b8a6046e20f5c1c7bf0b33400
  - eae1df7b8a6046e20f5c1c7bf0b33400
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
  - http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html
dsq_thread_id:
  - 1971099797
  - 1971099797
  - 1971099797
  - 1971099797
  - 1971099797
  - 1971099797
categories:
  - Facebook
  - Stumbleupon
  - Twitter
tags:
  - Float
  - Google
  - Pinterest
  - Share Box
  - Sharing

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Every Success Blog was popularized with the social integration such as Like Box, Share Box, Commenting and others. The social sharing widget will help other users to reach your blog. So, In this tutorial I&#8217;m going to tell you how to add a <b>Floating</b>&nbsp;<b>Like Box</b>. I made the like box floating, because wherever the user goes the like box will also go with him. Hence the user will notice the like box all the time and he will click sometime during his visit. The liking action will be posted to his <b>Facebook</b>&nbsp;Wall or <b>Google&nbsp;</b><b>+</b>&nbsp;page or his&nbsp;<b>Twitter </b>and his friends who sees this post will also visit. That friends too like your post if it is good and thus your post will pass along a lot of people.</p> 
  
  <p>
    In this <b>Floating Box</b>, there are <b>5</b>&nbsp;like plugins. They are :
  </p>
  
  <ol style="text-align: left;">
    <li>
      Facebook Like
    </li>
    <li>
      Twitter Tweet
    </li>
    <li>
      Google Plus&nbsp;+1
    </li>
    <li>
      Pinterest Pinning
    </li>
    <li>
      Stumbleupon Submission
    </li>
  </ol>
  
  <p>
    Let&#8217;s begin. First of all go to <b>Blogger </b>-> <b>YOUR BLOG</b>&nbsp;-><b>&nbsp;Layout</b>.<br />Add a new <b>HTML/JavaScript</b>&nbsp;widget.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-_gbP2lRSQAU/TzEvXRJvIxI/AAAAAAAABCE/5BGNp6f4yeE/s1600/fb7.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-_gbP2lRSQAU/TzEvXRJvIxI/AAAAAAAABCE/5BGNp6f4yeE/s1600/fb7.png" /></a>
  </div>
  
  <p>
    Leave the title value of the widget blank.<br />Then add the following contents :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <div id="subinsblogshare" title="Like and Share this to your friends"><br />&nbsp;<script><br />&nbsp;/*Floating Share Box [http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html] Copyright Subin Siby 2013*/<br />&nbsp;/*Facebook*/<br />&nbsp;ifr=document.createElement("iframe");<br />&nbsp;ifr.style.height="62px";<br />&nbsp;ifr.style.width="56px";<br />&nbsp;ifr.style.border="none";<br />&nbsp;ifr.src="https://www.facebook.com/plugins/like.php?layout=box_count&width=56&colorscheme=light&show_faces=false&send=true&href="+window.location;<br />&nbsp;document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr);<br />&nbsp;/*Twitter*/<br />&nbsp;ifr=document.createElement("iframe");<br />&nbsp;ifr.style.height="62px";<br />&nbsp;ifr.style.width="56px";<br />&nbsp;ifr.style.border="none";<br />&nbsp;ifr.src="https://platform.twitter.com/widgets/tweet_button.1375828408.html#_=1377268257182&count=vertical&id=twitter-widget-2&lang=en&size=m&url="+window.location;<br />&nbsp;document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr);<br />&nbsp;/*Google +*/<br />&nbsp;ifr=document.createElement("div");<br />&nbsp;ifr.style.height="80px";<br />&nbsp;ifr.style.marginTop="10px";<br />&nbsp;subinsblogp=document.createElement("div");<br />&nbsp;subinsblogp.setAttribute("class","g-plusone");<br />&nbsp;subinsblogp.setAttribute("data-size","tall");<br />&nbsp;ifr.appendChild(subinsblogp);<br />&nbsp;(function() {var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;po.src = "https://apis.google.com/js/plusone.js";var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);})();<br />&nbsp;document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr);<br />&nbsp;/*Pinterest*/<br />&nbsp;ifr=document.createElement("a");<br />&nbsp;subinsblogp=document.createElement("img");<br />&nbsp;subinsblogp.src="//assets.pinterest.com/images/pidgets/pin_it_button.png";<br />&nbsp;ifr.appendChild(subinsblogp);<br />&nbsp;img="http://goo.gl/3i1AG";/*Change this url to your desired image.*/<br />&nbsp;ds="Check out this Blog Post. You Will love it.";/*Change this to your desired description.*/<br />&nbsp;ds=ds.replace(" ","%20");<br />&nbsp;ifr.href="//www.pinterest.com/pin/create/button/?url="+window.location+"&media="+img+"&description="+ds;<br />&nbsp;ifr.setAttribute("data-pin-do","buttonPin");<br />&nbsp;ifr.setAttribute("data-pin-config","above");<br />&nbsp;document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr);<br />&nbsp;(function(d){<br />&nbsp; var f = d.getElementsByTagName(&#8216;SCRIPT&#8217;)[0], p = d.createElement(&#8216;SCRIPT&#8217;);<br />&nbsp; p.type = &#8216;text/javascript&#8217;;<br />&nbsp; p.async = true;<br />&nbsp; p.src = &#8216;//assets.pinterest.com/js/pinit.js&#8217;;<br />&nbsp; f.parentNode.insertBefore(p, f);<br />&nbsp;}(document));<br />&nbsp;/*Stumbleupon*/<br />&nbsp;ifr=document.createElement("iframe");<br />&nbsp;ifr.style.height="62px";<br />&nbsp;ifr.style.width="56px";<br />&nbsp;ifr.style.border="none";<br />&nbsp;ifr.src=&#8217;http://badge.stumbleupon.com/badge/embed/5/?url=&#8217;+window.location;<br />&nbsp;document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr);<br />&nbsp;ifr2=document.createElement("a");<br />&nbsp;ifr2.href="http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html";<br />&nbsp;ifr2.style.color="black";<br />&nbsp;ifr2.style.textDecoration="none";<br />&nbsp;ifr2.style.marginTop="10px";<br />&nbsp;ifr2.target="_blank";<br />&nbsp;ifr2.style.display="block";<br />&nbsp;ifr2.innerHTML="Get This";<br />&nbsp;document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr2);<br />&nbsp;</script><br /></div><br /><style><br />#subinsblogshare{<br />&nbsp;position:fixed;<br />&nbsp;left:40px;<br />&nbsp;height: 380px;<br />&nbsp;top:15%;<br />&nbsp;bottom:25%;<br />&nbsp;background:rgba(255,255,255,.8);<br />&nbsp;opacity:.8;<br />&nbsp;padding:0px 15px 10px 15px;<br />&nbsp;border-radius:10px;<br />&nbsp;border:1.5px solid black;<br />}<br />#subinsblogshare:hover{<br />&nbsp;opacity:1;<br />}<br />#subinsblogshare iframe{<br />&nbsp;display:block;<br />&nbsp;margin-top:10px;<br />}<br /></style>
    </p>
  </blockquote>
  
  <p>
    Yes, It&#8217;s a long code. If you want it less, Here is the compressed version :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <div id="subinsblogshare" title="Like and Share this to your friends"><script>/*Floating Share Box [http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html] Copyright Subin Siby 2013*//*Facebook*/ifr=document.createElement("iframe"); ifr.style.height="62px"; ifr.style.width="56px"; ifr.style.border="none"; ifr.src="https://www.facebook.com/plugins/like.php?layout=box_count&width=56&colorscheme=light&show_faces=false&send=true&href="+window.location; document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr); /*Twitter*/ifr=document.createElement("iframe"); ifr.style.height="62px"; ifr.style.width="56px"; ifr.style.border="none"; ifr.src="https://platform.twitter.com/widgets/tweet_button.1375828408.html#_=1377268257182&count=vertical&id=twitter-widget-2&lang=en&size=m&url="+window.location; document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr); /*Google +*/ifr=document.createElement("div");ifr.style.height="80px";ifr.style.marginTop="10px";subinsblogp=document.createElement("div");subinsblogp.setAttribute("class","g-plusone");subinsblogp.setAttribute("data-size","tall");ifr.appendChild(subinsblogp);(function() {var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;po.src = "https://apis.google.com/js/plusone.js";var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);})();document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr);/*Pinterest*/ifr=document.createElement("a"); subinsblogp=document.createElement("img"); subinsblogp.src="//assets.pinterest.com/images/pidgets/pin_it_button.png"; ifr.appendChild(subinsblogp); img="http://goo.gl/3i1AG";/*Change this url to your desired image.*/ds="Check out this Blog Post. You Will love it.";/*Change this to your desired description.*/ds=ds.replace(" ","%20"); ifr.href="//www.pinterest.com/pin/create/button/?url="+window.location+"&media="+img+"&description="+ds; ifr.setAttribute("data-pin-do","buttonPin");ifr.setAttribute("data-pin-config","above"); document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr); (function(d){var f = d.getElementsByTagName(&#8216;SCRIPT&#8217;)[0], p = d.createElement(&#8216;SCRIPT&#8217;); p.type = &#8216;text/javascript&#8217;; p.async = true; p.src = &#8216;//assets.pinterest.com/js/pinit.js&#8217;; f.parentNode.insertBefore(p, f); }(document)); /*Stumbleupon*/ifr=document.createElement("iframe");ifr.style.height="62px"; ifr.style.width="56px"; ifr.style.border="none"; ifr.src=&#8217;http://badge.stumbleupon.com/badge/embed/5/?url=&#8217;+window.location; document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr); ifr2=document.createElement("a"); ifr2.href="http://sag-3.blogspot.com/2013/09/floating-share-box-to-blogger-blog.html"; ifr2.style.color="black"; ifr2.style.textDecoration="none"; ifr2.style.marginTop="10px"; ifr2.target="_blank"; ifr2.style.display="block"; ifr2.innerHTML="Get This"; document.getElementById(&#8216;subinsblogshare&#8217;).appendChild(ifr2); </script></div><style>#subinsblogshare{position:fixed;left:40px;height:380px;top:15%;bottom:25%;background:rgba(255,255,255,.8);opacity:.8;padding:0px 15px 10px 15px;border-radius:10px;border:1.5px solid black;}#subinsblogshare:hover{opacity:1;}#subinsblogshare iframe{display:block;margin-top:10px;}</style>
    </p>
  </blockquote>
  
  <p>
    Be sure to change the values accordingly. The values to change are :
  </p>
  
  <ol style="text-align: left;">
    <li>
      Pinterest Image URL
    </li>
    <li>
      Pinterest Description
    </li>
  </ol>
  
  <p>
    Please don&#8217;t change variable names or any piece of the code, because it may cause problems in the working of the floating share box.<br />If there&#8217;s any problems/suggestions/feedback just say it out in the comment box below.</div>