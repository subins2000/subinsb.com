---
title: Google Hangouts Is Down
author: Subin Siby
type: post
date: 2014-03-17T17:11:38+00:00
url: /google-hangouts-down
authorsure_include_css:
  - 
  - 
  - 
tags:
  - Google
  - Google Drive

---
Again, one of Google&#8217;s service is down. This time it&#8217;s **Google Hangouts** also known before ad **Google Talk**. Google Sheets are down too.

The downtime of these services started from **17 March 9:45 PM IST**. I noticed the Internal Server Errors when connecting to Google server by Pidgin messenger. I retried again and again, but the problem still poped up. I went to Twitter and found out, I wasn&#8217;t the only one that experienced this problem.

The Apps Status Dashboard of Google states that it&#8217;s a know error with description :

<pre class="prettyprint"><code>We're investigating reports of an issue with Google Talk. We will provide more information shortly.</code></pre>

Let&#8217;s hope it comes back up and doesn&#8217;t happen again.

## Update

Google Hangouts, Google Sheets, Google Talk has been restored at **18 March 12:58 AM IST**.