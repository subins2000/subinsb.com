---
title: How to change/set background image of GRUB bootscreen
author: Subin Siby
type: post
date: 2012-11-02T17:45:00+00:00
excerpt: 'You might have heard of background image in GRUB boot screen. You can change the ugly black background of GRUB&nbsp;using a few little steps.VIDEO TUTORIAL1 - Press ALT + F2. Type this in the input box of the window.gksu gedit /etc/grub.d/05_debian_the...'
url: /how-to-changeset-background-image-of-grub-bootscreen
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 385d0d93f1ebb9bb61700fba68a10c20
  - 385d0d93f1ebb9bb61700fba68a10c20
  - 385d0d93f1ebb9bb61700fba68a10c20
  - 385d0d93f1ebb9bb61700fba68a10c20
  - 385d0d93f1ebb9bb61700fba68a10c20
  - 385d0d93f1ebb9bb61700fba68a10c20
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
  - http://sag-3.blogspot.com/2012/11/how-to-change-background-image-of-grub.html
dsq_thread_id:
  - 1971261027
  - 1971261027
  - 1971261027
  - 1971261027
  - 1971261027
  - 1971261027
categories:
  - Linux
  - Ubuntu
tags:
  - GRUB

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You might have heard of background image in <b>GRUB </b>boot screen. You can change the ugly black background of <b>GRUB</b>&nbsp;using a few little steps.</p> 
  
  <div style="text-align: center;">
    <a href="http://www.youtube.com/watch?v=er8UhdzmLmc&utm_source=subins-blog" ><span style="background-color: #93c47d; color: black; font-size: large;">VIDEO TUTORIAL</span></a>
  </div>
  
  <p>
    1 &#8211; Press <b>ALT + F2</b>. Type this in the input box of the window.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      gksu gedit /etc/grub.d/05_debian_theme
    </p>
  </blockquote>
  
  <p>
    and press <b>Enter Key</b>.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-pvwmtDa3yR8/UJQE9VeqsTI/AAAAAAAACMw/uiPHHHY6Sn8/s1600/Screenshot-Run+Application.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-pvwmtDa3yR8/UJQE9VeqsTI/AAAAAAAACMw/uiPHHHY6Sn8/s1600/Screenshot-Run+Application.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    The program will ask for your password. After typing the password and pressed the <b>Enter Key</b>&nbsp;a&nbsp;<b>gedit</b>&nbsp;window will appear. Press <b>CTRL + F </b>and search for&nbsp;<b>WALLPAPER= </b>after you found the word copy the location where your background image is and pate the location in <b>quotes</b>&nbsp;after the "=" like in the image shown below.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-I5gFN8H16Fc/UJQFvGI1L4I/AAAAAAAACM4/jLTSkHiT_io/s1600/grub-bg.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-I5gFN8H16Fc/UJQFvGI1L4I/AAAAAAAACM4/jLTSkHiT_io/s1600/grub-bg.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    After editing the file press <b>CTRL + S</b>. After Saving the file press <b>ALT + F2</b>&nbsp;again. This time type :&nbsp;
  </div>
  
  <blockquote class="tr_bq">
    <p>
      gksu update-grub
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: left;">
    and press <b>Enter Key</b>. A T<b>erminal window </b>appears and will close after some time. <b>Reboot </b>your computer and you will see your new <b>Background Image</b>.
  </div>
</div>