---
title: In My Name is Linux
author: Subin Siby
type: post
date: 2014-04-02T06:26:38+00:00
url: /in-my-name-is-linux
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Linux
  - Personal
tags:
  - Name

---
As you know already, my full name is Subin Siby. What&#8217;s special about my name is that it&#8217;s connected to Linux. As you may have seen, executable files on a Linux system is in the directory **/usr/bin**, **/usr/sbin** and **/sbin**. If you look at it closely, you can see that **sbin** only misses the character **u** from my name. If you add "u" to the name, it&#8217;ll become **/usr/subin**.

Creepy & Fun, right ? I can assure you one thing. I wasn&#8217;t named after the directory **/sbin**. It was a totally random decision to make my name **Subin** and there are a number of people in Kerala who has the name Subin. So I&#8217;m not alone.

Also, the word **bin** is coming in my name. Some call it **Recycle Bin** as in **Windows** and real life. The other **bin** word in **Linux** is the directory **/usr/bin**. **
  
**

Also, there is a command in **Linux** called **su**. It is used for changing user ID or become superuser. Also, **su** comes in the command **sudo**.

In short words, my name is entirely based on **Linux** except my last name which is of my dad&#8217;s. No wonder why I like **Linux** 🙂

From what I have arrived, It creeps me out and what an incredible coincidence.