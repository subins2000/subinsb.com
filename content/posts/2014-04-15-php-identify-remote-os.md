---
title: Identify Remote OS By IP Address In PHP
author: Subin Siby
type: post
date: 2014-04-15T14:32:34+00:00
url: /php-identify-remote-os
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Linux
  - PHP
tags:
  - JSON
  - Ping
  - Terminal
  - TTL

---
In the <a title="Default TTL (Time To Live) Values of Different OS" href="//subinsb.com/default-device-ttl-values" target="_blank">recent post</a>, I gave the default TTL value of different devices. As you can see, when you ping a host you can roughly identify the operating system the remote server&#8217;s running. I saw a question on Stack Overflow about this. Sadly it was down voted and was removed later. I tested if it is possible and I found that it is possible.

In this post, I will give a function that automatically pings the remote host and find out the possible Operating Systems the remote server is running. For this, we have a **JSON **file that contains the TTL of different devices / OSs.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/php/identify-remote-os" target="_blank">Demo</a>
</div>

Note that the remote server with the **IP Address** should be online the time the checking is done. Otherwise pinging the host will fail.

## Get JSON Data

I made the JSON file and is available at <a href="http://demos.subinsb.com/php/identify-remote-os/ttlValues.txt" target="_blank">this URL</a>. This JSON file is needed for the identification of the device. Here is how an item in the JSON file looks like :

<pre class="prettyprint"><code>{
 "device" : "Linux",
 "version" : "2.0.x kernel",
 "protocol" : "ICMP",
 "ttl" : "64"
}</code></pre>

It contains the **TTL** value, the device name and its version. Also the protocol which is not needed necessarily.

## detectOs($ip)

Now, let&#8217;s get to the function that identifies the OS. It&#8217;s an easy function that needs the following :

  * IP Address
  * JSON file with the name "ttlValues.txt" on the same directory as the file containing the function
  * shell_exec function to execute (PHP Safe Mode should be Off)

We use the **ping** command to ping the remote IP address and get the TTL. This is the function code :

<pre class="prettyprint"><code>function detectOs($ip, $returnArray=true){
 if(filter_var($ip, FILTER_VALIDATE_IP)){
  $cmd="ping -c 2 $ip";
  $shell=shell_exec($cmd);
  preg_match("/ttl=(.*?)stime/", $shell, $m);
  if(isset($m[1])){
   $ttl=$m[1];
   $list=file_get_contents("./ttlValues.txt");
   $list=json_decode($list, true);
   $oses=array();
   if($ttl==64){$oses[]="Mostly A *nix Device (Linux, Unix etc..)";}
   if($ttl==128){$oses[]="Mostly A Windows Device";}
   if($ttl==254){$oses[]="Mostly A Solaris/AIX Device";}
   foreach($list as $vals) {
    if($vals['ttl']==$ttl){
     $oses[]=$vals['device']." ".$vals['version'];
    }
   }
   return $returnArray===true ? $oses:implode(",", $oses);
  }else{
   return "connectionFailed";
  }
 }else{
  return "invalidIP";
 }
}</code></pre>

You can change the location of the JSON file by replacing the file path "./ttlValues.txt". The function will return either one of the following :

<table class="table">
  <tr>
    <td>
      invalidIP
    </td>
    
    <td>
      If the IP address given is not valid
    </td>
  </tr>
  
  <tr>
    <td>
      connectionFailed
    </td>
    
    <td>
      If the pinging failed or TTL value wasn&#8217;t accessbile
    </td>
  </tr>
</table>

If either one of the following didn&#8217;t return, then the return value is an array. You have the option to decide whether the return output should be an array or a string of possible devices separated by ",".

Also, if the hops between the connection of your machine and the remote server is too much, it&#8217;s better to widen the search by, replacing the following code in the function

<pre class="prettyprint"><code>if($vals['ttl']==$ttl)</code></pre>

to :

<pre class="prettyprint"><code>if($vals['ttl']&lt;=$ttl)</code></pre>

## Usage

<pre class="prettyprint"><code>$os=detectOs("127.0.0.1"); // Localhost OS, please
if($os=="invalidIP"){
 echo "Invalid IP address";
}elseif($os=="connectionFailed"){
 echo "Unable to connect to external server";
}else{
 echo "&lt;br/&gt;The Operating System Can Be one of the following : &lt;ul&gt;";
 foreach($os as $v){
  echo "&lt;li&gt;$v&lt;/li&gt;";
 }
 echo "&lt;/ul&gt;";
}</code></pre>

The above code checks all the return values of the function and finally output the array of possible devices.

You can also output string instead of array :

<pre class="prettyprint"><code>echo detectOs("127.0.0.1", false);</code></pre>

The above sample code will output something like this :

<pre class="prettyprint"><code>Linux 2.2.14 kernel,MacOS/MacTCP 2.0.x,NetBSD</code></pre>

Simple usage, right ? Well, use it for your own various purposes.