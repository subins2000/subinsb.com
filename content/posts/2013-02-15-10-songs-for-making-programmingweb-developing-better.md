---
title: 10 Songs for Making Programming Better
author: Subin Siby
type: post
date: 2013-02-15T16:07:00+00:00
excerpt: When I am coding my site Subins, I hear a lot of songs along with coding. It makes me energetic and will help calm me down when an error occurs. The error probably will be a simple mistake but causes a lot of time to solve it. To calm me down I will pl...
url: /10-songs-for-making-programmingweb-developing-better
authorsure_include_css:
  - 
  - 
  - 
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
syndication_item_hash:
  - d3c688027b707befe5167991118668fb
  - d3c688027b707befe5167991118668fb
  - d3c688027b707befe5167991118668fb
  - d3c688027b707befe5167991118668fb
  - d3c688027b707befe5167991118668fb
  - d3c688027b707befe5167991118668fb
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
  - http://sag-3.blogspot.com/2013/02/10-songs-for-making-programmingweb.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
categories:
  - Personal
  - Program
tags:
  - Developing
  - Music
  - WEB

---
When I am coding my site [Open](http://open.subinsb.com) or my blog, I hear a lot of songs along with coding. It makes me energetic and will help calm me down when an error occurs. The error probably will be a simple mistake but causes a lot of time to solve it. To calm me down I will play a song and **BINGO !** I fixed the error.

Here are the songs that make my coding easier and refreshes my mind. The text in brackets are the detail of which the song do to my mind.

1.  **Outasight – Now Or Never**
2.  **Outasight – Tonight Is The Night**
3.  **Outasight – Outasight – I’ll Drink To That**
4.  **Train – 50 ways to say goodbye (Erases my pain of errors)**
5.  **Back Street Boys – show me the meaning of being lonely**
6.  **Back Street Boys – everybody Backstreet’s Back (makes me energetic)**
7.  **Flo Rida-Turn Around (makes me happy)**
8.  **The Wanted – Chasing The Sun**
9.  **3 Days Grace – Never Too Late (makes me energetic)**
10.  **Matt Willis – Crash (makes me energetic)**

Music make **programming/web developing** easier and better to me. I think music are the best in the world. It’s just my opinion. What do you think about it ? Post it in the comments.

Try coding with a song playing. I am sure. It will make coding better. **Try It Out**.