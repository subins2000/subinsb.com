---
title: Disable Right Click On Blogger Blog
author: Subin Siby
type: post
date: 2012-03-17T04:46:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on"><div><span><span>Some websites and blogs have disabled right click. you can also disable this on your blog.</span></span><br><br><span><span>See Demo&nbsp;<a href="http://pfogb.blogspot.com/" target="_blank">here</a></span></span><br><span><br></span></div><div><span><span>Follow these steps</span></span><br><br><span><span><br></span></span></div><span>Step 1 : Go to Dashboard &amp;gt; Layout &amp;gt; Add Gadget &amp;gt; HTML / JavaScript</span><br><br><div><a href="//1.bp.blogspot.com/-AsdUESbUyBY/TzvRPEkugmI/AAAAAAAAAgk/cPBSB6ld5Qw/s1600/fb7.png" imageanchor="1"><img border="0" src="//1.bp.blogspot.com/-AsdUESbUyBY/TzvRPEkugmI/AAAAAAAAAgk/cPBSB6ld5Qw/s1600/fb7.png"></a></div><span><br></span><br><br><span>Step 2 : Copy the code from this file and paste it in that blogger widget.</span><br><br><br><div><blockquote>&lt;script language=JavaScript&gt;<br>&lt;!--<br>//Disable right mouse click Script<br>//By Subin's Blog<br>//For full source code, visit http://www.sag-3.blogspot.com<br>var message="Function Disabled!";<br>///////////////////////////////////<br>function clickIE4(){<br>if (event.button==2){<br>alert(message);<br>return false;<br>}<br>}<br>function clickNS4(e){<br>if (document.layers||document.getElementByI&hellip;<br>if (e.which==2||e.which==3){<br>alert(message);<br>return false;<br>}<br>}<br>}<br>if (document.layers){<br>document.captureEvents(Event.MOUSEDOWN)&hellip;<br>document.onmousedown=clickNS4;<br>}<br>else if (document.all&amp;&amp;!document.getElementById)&hellip;<br>document.onmousedown=clickIE4;<br>}<br>document.oncontextmenu=new Function("alert(message);return false")<br>// --&gt;<br>&lt;/script&gt;</blockquote></div><div><br></div><span>Step 3 : Save the html/javascript and view you blog.</span><br><br><div><a href="//4.bp.blogspot.com/-3D9C-9KMWDY/T1obA7vJUwI/AAAAAAAAAlU/mUDaGAM7XQw/s1600/Screenshot-7.png" imageanchor="1"><img border="0" src="//4.bp.blogspot.com/-3D9C-9KMWDY/T1obA7vJUwI/AAAAAAAAAlU/mUDaGAM7XQw/s1600/Screenshot-7.png"></a></div><br></div>
url: /disable-right-click-blogger-blog
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - d39c7f6a5a759e3226be5a30b5922841
  - d39c7f6a5a759e3226be5a30b5922841
  - d39c7f6a5a759e3226be5a30b5922841
  - d39c7f6a5a759e3226be5a30b5922841
  - d39c7f6a5a759e3226be5a30b5922841
  - d39c7f6a5a759e3226be5a30b5922841
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
  - http://sag-3.blogspot.com/2012/03/disable-right-click-on-bloggerblogspot.html
categories:
  - Blogger
tags:
  - Tutorials

---
For preventing copying of your content by visitors, you can disable the use of Right Click on your blog. Some websites and blogs prevent the right click for preventing copiers. You can also prevent the copiers by disabling the right click.

<div class="padlinks">
  <a class="demo" href="http://subin-demos.blogspot.in/2012/04/disable-right-click-demo.html" target="_blank">Demo</a>
</div>

<span>Go to Dashboard -> Layout -> Add Gadget -> HTML / JavaScript</span><figure id="attachment_2477" class="wp-caption aligncenter">

[<img class="size-full wp-image-2477" alt="Add HTML/Javascript Widget In Blogger" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0102.png" width="1049" height="244" />][1]<figcaption class="wp-caption-text">Add HTML/JavaScript Widget In Blogger</figcaption></figure> 

Copy the following code and paste it in the Widget text area :

<pre class="prettyprint"><code>&lt;script&gt;
var message="Not Allowed";
var shouldAlert=1;
document.oncontextmenu=function(){
 if(shouldAlert==1){
  alert(message);
 }
 return false;
};
&lt;/script&gt;</code></pre>

Save the "HTML/JavaScript" widget.

If you want to change the message, edit the contents of the variable "message".

Now, go to your blog and try right clicking. You will get a message saying "Not Allowed".

Whenever the user right clicks, the message will be displayed. It can be quite annoying to the visitors. So, if you don&#8217;t want the message to appear, change the "shouldAlert" value to "0" instead of "1".

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0102.png