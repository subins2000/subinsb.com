---
title: 'New : Change background of Subins Network.'
author: Subin Siby
type: post
date: 2013-01-28T14:57:00+00:00
excerpt: "You can now change background of this blog and the whole Subins Network. You can only do this if you have a account in Subins. If you don't have an account you can signup&nbsp;@ http://accounts-subins.hp.af.cm/signup.php.&nbsp;To change background you ..."
url: /new-change-background-of-subins-network
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
syndication_item_hash:
  - 41260f92c6eda85eb9eded4edbab0ba2
  - 41260f92c6eda85eb9eded4edbab0ba2
  - 41260f92c6eda85eb9eded4edbab0ba2
  - 41260f92c6eda85eb9eded4edbab0ba2
  - 41260f92c6eda85eb9eded4edbab0ba2
  - 41260f92c6eda85eb9eded4edbab0ba2
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
  - http://sag-3.blogspot.com/2013/01/new-change-background-of-subins-network.html
categories:
  - Blogger
  - Themes
tags:
  - Background
  - Subins

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You can now change background of this blog and the whole Subins Network. You can only do this if you have a account in Subins. If you don&#8217;t have an account you can signup&nbsp;@ <a href="http://accounts-subins.hp.af.cm/signup.php">http://accounts-subins.hp.af.cm/signup.php</a>.&nbsp;</p> 
  
  <div>
  </div>
  
  <div>
    To change background you must go to <a href="http://accounts-subins.hp.af.cm/">My Account</a> in Subins. In the left sidebar click <b>Appearance</b>.
  </div>
  
  <div>
    Click <b>Themes</b>&nbsp;or <b>Background</b>&nbsp;to change the background.
  </div>
  
  <div>
  </div>
  
  <div>
    Hope you like it.
  </div>
</div>