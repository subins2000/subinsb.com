---
title: Fix WordPress Database Error "MySQL server has gone away.."
author: Subin Siby
type: post
date: 2014-01-09T15:00:31+00:00
url: /fix-wordpress-error-mysql-server-has-gone-away
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - WordPress
tags:
  - Trick

---
If you run a WordPress blog and see the error "MySQL server has gone away for query&#8230;.." on your error logs a lot, you are certainly in trouble. When this happens, your blog will be down. It happened to this blog and I started searching for a solution. Finally I got a solution and it works. It&#8217;s a simple solution. All you have to do is add 2 lines after a specific code. Let&#8217;s get right on it.

This trick will work for all WordPress versions. Open your Wordpress directory. The directory will contain the folders :

  1. wp-admin
  2. wp-content
  3. wp-includes

Open **wp-includes**. Search for the **wp-db.php **file. If you found the file, open it using a text editor. Using the text editor&#8217;s search tool, search for :

> $this->ready = true;

Here is a screenshot of the found string in **gEdit** text editor :

<p style="text-align: center;">
  <a href="//lab.subinsb.com/projects/blog/uploads/2014/01/0073.png"><img class="aligncenter size-medium wp-image-2214" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0073-300x135.png" alt="0073" width="300" height="135" /></a>
</p>

Once you found the line, add the following lines just after the found line :

> //WP Query Gone Away Error Fix
  
> $this->query("set session wait_timeout=600");

Save the file. Now, when you check the error log of your site, you won&#8217;t see the error anymore which means that the site won&#8217;t go down as before.

This is just a temporary fix. You can&#8217;t be 100% sure that the error won&#8217;t come anymore. By doing this "fix", MySQL will wait for some time to finish a query. Here are some tips to fix this error permanently :

  1. Try Disabling / Removing Very High Memory Needed Plugins
  2. Disable plugins one by one and find the plugins that cause this error
  3. Use plugins that you only need. More plugins means more C.P.U memory and MySQL server will be overloaded
  4. Use LightWeight Themes that only uses less memory ( <a href="http://www.wpthm.com/wordpress/lightweight" target="_blank">http://www.wpthm.com/wordpress/lightweight</a> )