---
title: How to run/execute SQL Code
author: Subin Siby
type: post
date: 2012-11-13T14:05:00+00:00
excerpt: 'If you have installed phpMyAdmin&nbsp;do as below.Go to localhost/phpmyadmin/server_sql.php.Paste the SQL Code&nbsp;in the text box shown in the page and press GO button at the bottom right of the page.If you want to run code in PHP file do as below.Op...'
url: /how-to-runexecute-sql-code
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 298d975b2c63c9093e13cd25fabdef97
  - 298d975b2c63c9093e13cd25fabdef97
  - 298d975b2c63c9093e13cd25fabdef97
  - 298d975b2c63c9093e13cd25fabdef97
  - 298d975b2c63c9093e13cd25fabdef97
  - 298d975b2c63c9093e13cd25fabdef97
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
  - http://sag-3.blogspot.com/2012/11/run-execute-sql-code.html
categories:
  - PHP
  - phpMyAdmin
  - SQL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you have installed <b><a href="http://sag-3.blogspot.in/2012/10/phpmyadmin-software-to-manage-mysql.html" >phpMyAdmin</a>&nbsp;</b>do as below.<br />Go to <a href="http://localhost/phpmyadmin/server_sql.php" >localhost/phpmyadmin/server_sql.php</a>.<br />Paste the <b>SQL Code</b>&nbsp;in the text box shown in the page and press <b>GO </b>button at the bottom right of the page.</p> 
  
  <p>
    If you want to run code in <b>PHP </b>file do as below.<br />Open your<b>&nbsp;PHP </b>file.<br />Add these lines in the file.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      mysql_query("<span style="color: red;">sql code here</span>");
    </p>
  </blockquote>
  
  <p>
    <b>NOTE</b> &#8211; Replace the red line with the&nbsp;<b>SQL</b>&nbsp;code.</div>