---
title: Blog’s 3rd Anniversary
author: Subin Siby
type: post
date: 2014-01-30T15:43:26+00:00
url: /blog-3rd-aniversary
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Personal
tags:
  - Blog
  - News

---
Today, January 30 marks the 3rd anniversary of this blog. Thanks to everyone who have supported me and this blog. With out your help, I couldn&#8217;t have run this blog for 3 years. Here are the 5 popular posts of the last 3 years :

<a href="//subinsb.com/2012/07/how-to-add-a-header-image-to-blogger-dynamic-views" target="_blank">HOW TO ADD A HEADER IMAGE TO BLOGGER DYNAMIC VIEWS</a>

<a href="//subinsb.com/2012/09/how-to-repairrestorereinstall-grub-2-using-a-ubuntu-live-cd" target="_blank">HOW TO REPAIR/RESTORE/REINSTALL GRUB 2 USING A UBUNTU LIVE CD</a>

<a href="//subinsb.com/2012/02/how-to-add-a-fixed-header-to-blogger-blog" target="_blank">HOW TO ADD A FIXED HEADER TO BLOGGER BLOG</a>

<a href="//subinsb.com/2012/04/how-to-create-a-localhost-web-site-in-ubuntu-using-apache-web-server" target="_blank">HOW TO CREATE A LOCALHOST WEB SITE IN LINUX USING APACHE WEB SERVER</a>

<a href="//subinsb.com/2013/10/how-to-create-a-simple-web-crawler-in-php" target="_blank">HOW TO CREATE A SIMPLE WEB CRAWLER IN PHP</a>

Other Popular Posts

<a href="//subinsb.com/2013/05/create-facebook-like-system-with-jquery-mysql-pdo-in-php" target="_blank">CREATE FACEBOOK LIKE SYSTEM WITH JQUERY, MYSQL, PDO IN PHP</a>

<a href="//subinsb.com/2013/10/how-to-use-filezillasftp-to-update-site-in-openshift" target="_blank">HOW TO USE FILEZILLA/SFTP TO UPDATE SITE IN OPENSHIFT</a>

<a href="//subinsb.com/2013/08/create-mysql-injection-free-secure-login-system-in-php" target="_blank">CREATE MYSQL INJECTION FREE SECURE LOGIN SYSTEM IN PHP</a>

<a href="//subinsb.com/2013/04/how-to-check-if-the-mouse-is-over-an-element-in-jquery" target="_blank">HOW TO CHECK IF THE MOUSE IS OVER AN ELEMENT IN JQUERY ?</a>

<a href="//subinsb.com/2012/08/insert-xml-file-contents-to-mysql-database-table-using-php" target="_blank">INSERT XML FILE CONTENTS TO MYSQL DATABASE TABLE USING PHP</a>

<a href="//subinsb.com/2013/04/spam-urls-in-blogger-traffic-source-stats-what-should-i-do" target="_blank">SPAM URL&#8217;S IN BLOGGER TRAFFIC SOURCE STATS : WHAT SHOULD I DO ?</a>

<a href="//subinsb.com/2013/10/how-to-create-a-simple-username-availability-checker-using-jquery-and-php" target="_blank">HOW TO CREATE A SIMPLE USERNAME AVAILABILITY CHECKER USING JQUERY AND PHP</a>

<a href="//subinsb.com/2013/10/how-to-add-a-simple-birthday-field-in-a-form-using-jquery" target="_blank">HOW TO ADD A SIMPLE BIRTHDAY FIELD IN A FORM USING JQUERY</a>

<a href="//subinsb.com/2012/08/refresh-a-div-container-in-equal-intervals-of-time" target="_blank">REFRESH A DIV CONTAINER IN EQUAL INTERVALS OF TIME</a>

Thank you every one for your support, comments, likes and shares.