---
title: How to add Facebook log in button to blogger blog
author: Subin Siby
type: post
date: 2012-02-07T14:52:00+00:00
excerpt: 'Now you can see a lot of blog has a Facebook login button.You can also add this to your blog.See an example here under Social Login tab.1st step : go to https://developers.facebook.com/apps and create a new app.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;...'
url: /how-to-add-facebook-log-in-button-to-blogger-blog
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - a837ee3330e453ef801fbec0a44eaf87
  - a837ee3330e453ef801fbec0a44eaf87
  - a837ee3330e453ef801fbec0a44eaf87
  - a837ee3330e453ef801fbec0a44eaf87
  - a837ee3330e453ef801fbec0a44eaf87
  - a837ee3330e453ef801fbec0a44eaf87
syndication_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-facebook-log-in-button-to.html
categories:
  - Blogger
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">Now you can see a lot of blog has a <b>Facebook login button</b>.</span><br /><span style="font-family: inherit;">You can also add this to your blog.</span><br /><span style="font-family: inherit;">See an example <a href="http://accounts-subins.hp.af.cm/login.php" >here</a> under <b>Social Login </b>tab.</span><br /><span style="font-family: inherit;"><br /></span><span style="font-family: inherit;">1st step : go to <a href="https://developers.facebook.com/apps" >https://developers.facebook.com/apps</a> and create a new app.</span></p> 
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;"><a href="https://2.bp.blogspot.com/-LiURNxniv_Y/TzEmozSvKZI/AAAAAAAAAZA/urQTL3UoAPs/s1600/fb-1.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-LiURNxniv_Y/TzEmozSvKZI/AAAAAAAAAZA/urQTL3UoAPs/s1600/fb-1.png" /></a></span>
  </div>
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="font-family: inherit;">Type your &nbsp;App name in <b>App Display Name</b>. You don&#8217;t need to type anything on <b>App Namespace.</b></span>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: inherit;">After typing your app name and after clicked on "<span class="Apple-style-span" style="background-color: white; color: #666666; font-size: 11px; font-weight: bold; line-height: 14px;">I agree to the&nbsp;</span><a href="https://developers.facebook.com/policy" style="background-color: white; color: #3b5998; cursor: pointer; font-size: 11px; font-weight: bold; line-height: 14px; text-align: left; text-decoration: none;" >Facebook Platform Policies.</a>" &nbsp;click <b>Continue</b> button.</span>
  </div>
  
  <div>
    <span style="font-family: inherit;"><br /></span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Now you should see a new page like this.</span>
  </div>
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;"><a href="//4.bp.blogspot.com/-d3c-24Qm8L8/TzErEFQs4CI/AAAAAAAAAZY/jBU0l4KDoD0/s1600/fb2.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-d3c-24Qm8L8/TzErEFQs4CI/AAAAAAAAAZY/jBU0l4KDoD0/s1600/fb2.png" /></a></span>
  </div>
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;"><br /></span>
  </div>
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;">Type the required information and click on <img border="0" src="//3.bp.blogspot.com/-ULMIe2OzLm4/TzErcv_ZtgI/AAAAAAAAAZg/Gs8k64IhV3w/s1600/fb3.png" style="width:200px !important;" height="26" width="200" />&nbsp;button</span>
  </div>
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;"><br /></span>
  </div>
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;"><br /></span>
  </div>
  
  <div class="" style="clear: both;">
    <span style="font-family: inherit;">2nd step :&nbsp;<span class="Apple-style-span" style="background-color: white; color: #333333; font-size: 14px; line-height: 19px;">Go to&nbsp;</span><b style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">Template</b><span class="Apple-style-span" style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">/</span><b style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">Design</b><span class="Apple-style-span" style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">&nbsp;>&nbsp;</span><b style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">Edit HTML</b><a href="//3.bp.blogspot.com/-nOZNaCL7tLs/TzEsuTdDPNI/AAAAAAAAAZs/MgoINsJjYEk/s1600/fb4.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em; text-align: center;"><img border="0" src="//3.bp.blogspot.com/-nOZNaCL7tLs/TzEsuTdDPNI/AAAAAAAAAZs/MgoINsJjYEk/s1600/fb4.png" height="18" width="93" /></a></span>
  </div>
  
  <div class="" style="clear: both;">
    <span style="font-family: inherit;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
  </div>
  
  <div class="separator" style="clear: both;">
    <span style="font-family: inherit;">Paste this code below <b><html</b> text:</span>
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        <span style="font-family: inherit;">xmlns:fb="http://ogp.me/ns/fb#"</span>
      </p>
    </blockquote>
    
    <p>
      <span style="font-family: inherit;">Example:</span><br /><span style="font-family: inherit;"><br /></span></div> 
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"><a href="https://2.bp.blogspot.com/-z5_XrYguhlE/TzExTRIgWGI/AAAAAAAAAac/v0t_0Qc2p_M/s1600/fb9.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-z5_XrYguhlE/TzExTRIgWGI/AAAAAAAAAac/v0t_0Qc2p_M/s1600/fb9.png" /></a></span>
      </div>
      
      <div>
        <span style="font-family: inherit;"><br /></span>
      </div>
      
      <div>
        <span style="font-family: inherit;">3rd step : Go to <b style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">Blogger > Layout</b><span class="Apple-style-span" style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">&nbsp;</span></span>
      </div>
      
      <div>
        <span style="font-family: inherit;"><span class="Apple-style-span" style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></span>
      </div>
      
      <p>
        <span style="font-family: inherit;"><span class="Apple-style-span" style="background-color: white; color: #333333; font-size: 14px; line-height: 19px;">Click on</span><a href="//3.bp.blogspot.com/-dgJ1CCw0N3I/TzEvAoOnHGI/AAAAAAAAAaA/4AWaRv9zNwI/s1600/fb6.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-dgJ1CCw0N3I/TzEvAoOnHGI/AAAAAAAAAaA/4AWaRv9zNwI/s1600/fb6.png" height="29" width="230" /></a>button.</span>
      </p>
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"><br /></span>
      </div>
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"></span>
      </div>
      
      <p>
        <span style="font-family: inherit;">Select &nbsp;HTML/JavaScript:</span>
      </p>
      
      <div class="separator" style="clear: both; text-align: center;">
        <a href="//3.bp.blogspot.com/-_gbP2lRSQAU/TzEvXRJvIxI/AAAAAAAAAaI/6OYFfBP_WKI/s1600/fb7.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-_gbP2lRSQAU/TzEvXRJvIxI/AAAAAAAAAaI/6OYFfBP_WKI/s1600/fb7.png" height="102" width="436" /></a>
      </div>
      
      <div class="" style="clear: both;">
        <span style="font-family: inherit;">Paste this in the <b>content</b> area:</span>
      </div>
      
      <blockquote class="tr_bq">
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;"><script>(function(d, s, id) {</span>
        </div>
        
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;">&nbsp; var js, fjs = d.getElementsByTagName(s)[0];</span>
        </div>
        
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;">&nbsp; if (d.getElementById(id)) return;</span>
        </div>
        
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;">&nbsp; js = d.createElement(s); js.id = id;</span>
        </div>
        
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;">&nbsp; js.src = &#8216;http://connect.facebook.net/en_US/all.js?xfbml=1&appId=<span class="Apple-style-span" style="color: red;"><b><i><u>YOUR APP ID</u></i></b></span>&nbsp;&quot;&#39&#8242;;</span>
        </div>
        
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;">&nbsp; fjs.parentNode.insertBefore(js, fjs);</span>
        </div>
        
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;">}(document, &#39;script&#39;, &#39;facebook-jssdk&#39;));</script></span>
        </div>
        
        <div class="separator" style="clear: both;">
          <span style="font-family: inherit;"><div class="fb-login-button" data-show-faces="true" data-width="200&#8243; data-max-rows="1&#8243;></div></span>
        </div>
      </blockquote>
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"><br /></span>
      </div>
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"><a href="//4.bp.blogspot.com/-pccHyyinKZU/TzE9Gq9DHhI/AAAAAAAAAao/xYZ1vmXgPKU/s1600/fb9.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-pccHyyinKZU/TzE9Gq9DHhI/AAAAAAAAAao/xYZ1vmXgPKU/s1600/fb9.png" /></a></span>
      </div>
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"><br /></span>
      </div>
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"><br /></span>
      </div>
      
      <div class="separator" style="clear: both;">
        <span style="font-family: inherit;"><span class="Apple-style-span" style="font-size: large;">Now it&#8217;s finished you will see your Facebook log in button in your blog</span>.</span>
      </div></div>