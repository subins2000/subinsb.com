---
title: 'Compress PHP Site – HTML, CSS & JS'
author: Subin Siby
type: post
date: 2014-06-15T15:13:35+00:00
url: /compress-php-website-html-css-js
authorsure_include_css:
  - 
  - 
  - 
categories:
  - CSS
  - HTML
  - JavaScript
  - jQuery
  - Localhost
  - PHP
tags:
  - Compress
  - Software

---

## UPDATE

Site Compressor is now an app on <a href="//subinsb.com/lobby" target="_blank">Lobby</a>. To use Site Compressor, <a href="https://lobby.subinsb.com/download" target="_blank">download Lobby</a> and install <a href="https://lobby.subinsb.com/apps/site-compressor" target="_blank">Site Compressor app</a>.

Compressing / minifying a site&#8217;s code will improve the data transfer from server to the client. The browser will be easily able to get the content from server. There are a lot of benefits that comes with site compression. I use compression too in my own way. My site <a href="http://open.subinsb.com" target="_blank">Open</a> is heavily compressed.

I compressed sites using **Python** by using loops to iterate over files and replace strings, compress according to my needs. But, every time I had to open a Terminal to give the request and there will be some kind of errors when something new is added to the site.

So, I decided to create a Graphical Compressor. The best thing I thought was, it will be easier to create the application in localhost rather than creating a software. This is because that software creation is not platform independent, but **PHP** and localhost is.

And I created one. It&#8217;s termed as **siteCompressor**. All you have to do is give the location of the local site and the output path, choose some options and you&#8217;re ready to go. With the help of some external libraries like **JSqueeze** and **CssMin**, it was easy.

## Features

  * Compress HTML
  * Minify JavaScript & CSS files
  * Minify Inline JavaScript, jQuery & CSS in HTML pages (<script>, <style>)
  * Replace Multiple Strings like "localsite.dev" to "mydomain.com"
  * Save, Restore & Remove Configurations
  * Choose Site Location & Output folder
  * Real Time Status of site compression
  * Execute \*\*Terminal\*\* / \*\*Shell\*\* commands before or after compression
  * Can Run in any localhost server
  * No Database needed
  * Leaves alone PHP files that doesn&#8217;t have HTML, so that nothing messes up the PHP code

A <a href="https://github.com/subins2000/siteCompressor" target="_blank">GitHub repository</a> is opened for the software under **MIT** license.

## Installation

Download the full source code from GitHub :

<div class="padlinks">
  <a class="download" href="https://github.com/subins2000/siteCompressor/archive/master.zip" target="_blank">Download Software</a>
</div>

Extract the contents of the Zip file to your localhost destination and by your browser visit that location and you will access the software.

## Usage

The siteCompressor window will look like this :<figure id="attachment_2831" class="wp-caption aligncenter">

[<img class="wp-image-2831 size-large" title="The siteCompressor Window" src="//lab.subinsb.com/projects/blog/uploads/2014/06/0119-1024x340.png" alt="0119" width="640" height="212" />][5]<figcaption class="wp-caption-text">The siteCompressor Window</figcaption></figure> 

There are 6 sections in the software which is as follows :

  1. Site Details
  2. Compression Options
  3. Replacer
  4. Before Compression
  5. After Compression
  6. Compression Status

Each section has different options except for the **Compression Status** which is just an output section for printing the compression status.

### Site Details

There are 2 fields in this section. In the **Site Location** field, paste the full location of the local site&#8217;s source code. **No "/" should be placed in the end**.

And in the **Output** field, input the full location of the output directory which also shouldn&#8217;t have "/" at the end.

### Compression Options

You can choose which to compress, what to compress in here. Some of the options are :

  * Minimize HTML
  * Minimize HTML in .php Files
  * Remove Comments
  * Minimize CSS
  * Minimize JS

As the Features said, PHP files that doesn&#8217;t have HTML content won&#8217;t be minimized.

### Replacer

Since the site is being converted from local to the real deal, some strings should be replaced with new ones. This section allows you to do that. To add a new **From & To** field, just click the "Add New Field" button.

All kind of strings is replaced. Note that **RegEx** is not supported.

### Before Compression

In here, you can optionally type in a Shell command that should be executed before the compression starts. It&#8217;s value is according to the Operating System the software is being ran.

**Please** avoid using of "+" character in the command, because it can produce unexpected results.

### After Compression

Just like the **Before Compression**, a Terminal / Shell command can be executed after the compression is finished. Rules of the **Before Compression** is applicable here too.

### Compression Status

When you give the request to compress the site, the status of the compression is outputted real time in here. It will automatically scroll into the last line as each lines are outputted.

## Start Compression

You can find the **Let&#8217;s Start Compressing** button just after **After Compression** section. This button will submit the form and a compression request with the values is sent to the proper file.

Live Output of the compression is seen in the **Compression Status** section. When the compression is finished, the last line of it will be :

<pre class="prettyprint"><code>(Timestamp) - Finished Site Compression. Thank you. Hope everything went OK.</code></pre>

and the time taken to compress will also be outputted just after the above one. Here is an example :<figure id="attachment_2832" class="wp-caption aligncenter">

[<img class="size-large wp-image-2832" src="//lab.subinsb.com/projects/blog/uploads/2014/06/0120-1024x284.png" alt="After site compression is finished" width="640" height="177" />][13]<figcaption class="wp-caption-text">After site compression is finished</figcaption></figure> 

The end source code of the site will be outputted in the Output Directory given.

You can ask for support or discuss at <a href="//subinsb.com/ask/php-sitecompressor" target="_blank">this page</a> or for project related matters such as code, ask it on <a href="https://github.com/subins2000/siteCompressor/issues" target="_blank">GitHub</a>.

 [1]: #update
 [2]: #features
 [3]: #installation
 [4]: #usage
 [5]: //lab.subinsb.com/projects/blog/uploads/2014/06/0119.png
 [6]: #site-details
 [7]: #compression-options
 [8]: #replacer
 [9]: #before-compression
 [10]: #after-compression
 [11]: #compression-status
 [12]: #start-compression
 [13]: //lab.subinsb.com/projects/blog/uploads/2014/06/0120.png
