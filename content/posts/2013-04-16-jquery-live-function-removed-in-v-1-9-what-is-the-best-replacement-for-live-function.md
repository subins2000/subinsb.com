---
title: jQuery .live function removed in V 1.9. What is the best replacement for .live function ?
author: Subin Siby
type: post
date: 2013-04-16T04:09:00+00:00
excerpt: 'As of jQuery&nbsp;version 1.9, the .live&nbsp;function has been removed which means it is no longer available to use. Instead of this function you can use .on&nbsp;function which also has the same function as .live.&nbsp;Both of them attach an event ha...'
url: /jquery-live-function-removed-in-v-1-9-what-is-the-best-replacement-for-live-function
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 1693f709070065f520e595708be25460
  - 1693f709070065f520e595708be25460
  - 1693f709070065f520e595708be25460
  - 1693f709070065f520e595708be25460
  - 1693f709070065f520e595708be25460
  - 1693f709070065f520e595708be25460
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
  - http://sag-3.blogspot.com/2013/04/replacement-for-.live-function-jquery.html
categories:
  - JavaScript
  - jQuery

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div dir="ltr" style="text-align: left;" trbidi="on">
    <span style="font-family: Times, Times New Roman, serif;">As of <b>jQuery</b>&nbsp;version 1.9, the <b>.live</b>&nbsp;function has been removed which means it is no longer available to use. Instead of this function you can use <b>.on</b>&nbsp;function which also has the same function as <b>.live</b>.&nbsp;</span><br /><span style="font-family: Times, Times New Roman, serif;"><br />Both of them a<span style="background-color: white; line-height: 22.5px;">ttach an event handler for all elements which match the current selector, now and in the future.</span></span><br /><span style="font-family: Times, Times New Roman, serif; line-height: 22.5px;">Since <b>.live</b>&nbsp;has been removed you can use <b>.on</b>&nbsp;function. Here&#8217;s how we should do it.</span><br /><span style="font-family: Times, Times New Roman, serif; line-height: 22.5px;">Replace <b>.live</b>&nbsp;function with <b>.on</b>. It&#8217;s that simple.</span><br /><span style="font-family: Times, Times New Roman, serif; font-size: large; line-height: 22.5px;"><u><b>Example:</b></u></span></p> 
    
    <div class="container" style="background-image: none !important; border-bottom-left-radius: 0px !important; border-bottom-right-radius: 0px !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; bottom: auto !important; box-sizing: content-box !important; float: none !important; font-size: 1em !important; height: auto !important; left: auto !important; line-height: 1.1em !important; margin: 0px !important; min-height: auto !important; outline: 0px !important; overflow: visible !important; padding: 0px !important; position: relative !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important;">
      <blockquote class="tr_bq">
        <p>
          <span style="font-family: Times, Times New Roman, serif;">$(<span class="string" style="box-sizing: border-box; color: #dd1144;">"<span style="color: red;">#dataTable tbody tr</span>"</span>).live(<span class="string" style="box-sizing: border-box; color: #dd1144;">"<span style="color: red;">click</span>"</span>, <span class="keyword" style="box-sizing: border-box; font-weight: bold;">function</span>(event){<br /><span style="background-color: white;"> alert($(<span class="keyword" style="box-sizing: border-box; color: red; font-weight: bold;">this</span>).text());</span><br /><span style="background-color: white;">});</span></span>
        </p>
      </blockquote>
    </div>
  </div>
  
  <p>
    <span style="font-family: Times, Times New Roman, serif;"><span style="line-height: 22.5px;">With :</span></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: Times, Times New Roman, serif;"><span style="line-height: 22.5px;"><span style="line-height: normal; white-space: pre;">$(</span><span class="string" style="box-sizing: border-box; color: #dd1144;">"<span style="color: red;">#dataTable tbody tr</span>"</span><span style="line-height: normal; white-space: pre;">).on(</span><span class="string" style="box-sizing: border-box; color: #dd1144;">"<span style="color: red;">click</span>"</span><span style="line-height: normal; white-space: pre;">, </span><span class="keyword" style="box-sizing: border-box; font-weight: bold;">function</span><span style="line-height: normal; white-space: pre;">(event){</span></span><br /><span style="line-height: 22.5px;"><span style="background-color: white;"> alert($(<span class="keyword" style="box-sizing: border-box; color: red; font-weight: bold;">this</span>).text());</span></span><br /><span style="line-height: 22.5px;"><span style="background-color: white;">});</span></span></span>
    </p>
  </blockquote>
  
  <p>
    <span style="line-height: 22.5px;"><span style="background-color: white; font-family: Times, Times New Roman, serif;">Use a software such as <a href="http://adf.ly/J9Rdm" >regexxer</a> to replace all <b>.live(</b> word with <b>.on(</b> word.</span></span></div>