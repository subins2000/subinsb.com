---
title: How to see transit of Venus using Stellarium in Ubuntu
author: Subin Siby
type: post
date: 2012-06-04T08:39:00+00:00
excerpt: 'U can watch&nbsp;transit of Venus&nbsp;in your ComputerEdubuntu operating system using&nbsp;Stellarium.To know more click the link belowSIMMASH&nbsp;Efingers&nbsp;GoogleTo view in Ubuntu&nbsp;Go toApplications --&gt; Ubuntu Software Center --&gt;and se...'
url: /how-to-see-transit-of-venus-using-stellarium-in-ubuntu
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 6ea0ce93ffe30e83c7c8e6a417f6de45
  - 6ea0ce93ffe30e83c7c8e6a417f6de45
  - 6ea0ce93ffe30e83c7c8e6a417f6de45
  - 6ea0ce93ffe30e83c7c8e6a417f6de45
  - 6ea0ce93ffe30e83c7c8e6a417f6de45
  - 6ea0ce93ffe30e83c7c8e6a417f6de45
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
  - http://sag-3.blogspot.com/2012/06/how-to-see-transit-of-venus-using.html
categories:
  - Ubuntu
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <u style="color: blue; font-size: large;">U can watch&nbsp;<b>transit of Venus&nbsp;</b>in your Computer</u></p> 
  
  <div style="margin-bottom: 0cm;">
    Edubuntu operating system using&nbsp;<b>Stellarium</b>.To know more click the link below
  </div>
  
  <div style="margin-bottom: 0cm;">
    <a href="http://simmash.blogspot.com/">SIMMASH</a>&nbsp;<a href="http://efingers.blogspot.com/" >Efingers</a>&nbsp;<a href="http://www.google.co.in/search?sourceid=efingers&ie=UTF-8&q=Transit%20of%20Venus">Google</a>
  </div>
  
  <div style="margin-bottom: 0cm;">
    <b><br /></b></p> 
    
    <p>
      <b>To view in Ubuntu&nbsp;</b><br />Go to<br /><span style="color: blue;"><b>Applications &#8211;> Ubuntu Software Center &#8211;></b></span><br />and search for&nbsp;<b>Stellarium&nbsp;</b>and click install button<br />Then do the steps as shown below</div> 
      
      <div style="margin-bottom: 0cm;">
        <b>Steps</b>
      </div>
      
      <div style="margin-bottom: 0cm;">
        *****
      </div>
      
      <div style="margin-bottom: 0cm;">
        <b><span style="color: blue;">Applications&#8212;>Science&#8212;->Stellarium</span></b>
      </div>
      
      <div style="margin-bottom: 0cm;">
      </div>
      
      <div style="margin-bottom: 0cm;">
        in tool bar select&nbsp;<b><span style="color: blue;">location window</span></b>&nbsp;, then select country name the location u need
      </div>
      
      <div style="margin-bottom: 0cm;">
        here we type India country name and location kunnamkulam
      </div>
      
      <div style="margin-bottom: 0cm;">
        &#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;
      </div>
      
      <div style="margin-bottom: 0cm;">
        in tool bar select<span style="color: blue;">&nbsp;<b>Search window</b></span>
      </div>
      
      <div style="margin-bottom: 0cm;">
        <span style="color: blue;">find objec</span>t position
      </div>
      
      <div style="margin-bottom: 0cm;">
        in here we type&nbsp;<b>venus&nbsp;</b>on the search box
      </div>
      
      <div style="margin-bottom: 0cm;">
        by scrolling mouse you can zoom the selected object
      </div>
      
      <div style="margin-bottom: 0cm;">
        &#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;-
      </div>
      
      <div style="margin-bottom: 0cm;">
        in tool bar select&nbsp;<b><span style="color: blue;">Date And Time</span></b>
      </div>
      
      <div style="margin-bottom: 0cm;">
        adjust the date&nbsp;<b>2012/6/6&nbsp;</b>and time&nbsp;<b>1:00:00</b>
      </div>
      
      <div style="margin-bottom: 0cm;">
        in status bar selec<span style="color: blue;">t&nbsp;<b>inceasing and decreasing the time</b></span>
      </div>
      
      <div style="margin-bottom: 0cm;">
        using the keyes&nbsp;<b>L</b>&nbsp;and<b>&nbsp;J&nbsp;</b>on key- board
      </div>
      
      <div style="margin-bottom: 0cm;">
        The transit also between the time&nbsp;<b>6:00 to 10:15 in kunnamkulam</b><br /><b><br /></b>
      </div>
      
      <div style="margin-bottom: 0cm;">
      </div></div>