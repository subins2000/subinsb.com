---
title: Making Text To Speech In PHP With Google Translate
author: Subin Siby
type: post
date: 2014-01-16T16:30:14+00:00
url: /text-to-speech-in-php-with-google-translate
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
enclosure:
  - |
    |
        http://translate.google.com/translate_tts?ie=UTF-8&amp
        0
        application/vnd.apple.pages
        
categories:
  - PHP
tags:
  - Audio
  - Google

---
Google provides text to speech feature on their translator product which is an awesome feature. If you are creating a web app of your own and need a human speaking voice for your app, you will get lost. Google have a URL for creating these audio files when given a text. I will tell you how to implement this text to speech feature on your **PHP** site without directly compromising the Google URL.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=404x12bqlmmu/s/y15htxpnn21icg2f9hxz&class=17">Download</a><a class="demo" href="http://demos.subinsb.com/php/speech">Demo</a>
</div>

For our task, we will only create a file named **get_sound.php** which sends a **GET** request to Google via **cURL** and returns the audio file. We will play this audio file using the audio element in **HTML5** or you can use SWF players. The audio returned will be of the **mpeg** format.

# get_sound.php

This file sends request to Google and returns the audio file. This file needs the **text** parameter to return the sound file. The audio file type is **mpeg**.

<pre class="prettyprint"><code>&lt;?
if(isset($_GET['text'])){
 $txt=htmlspecialchars($_GET['text']);
 $txt=rawurlencode($txt);
 if($txt!="" || strlen($txt) &lt; 100){
  header("Content-type: audio/mpeg");
  $url="http://translate.google.com/translate_tts?ie=UTF-8&q=$txt&tl=en";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  $audio = curl_exec($ch);
  echo $audio;
 }else{
  echo "No value or String exceeds 100 characters.";
 }
}
?&gt;</code></pre>

As you can see, the URL of the Google text to speech is :

<pre class="prettyprint"><code>http://translate.google.com/translate_tts?ie=UTF-8&q=$txt&tl=en</code></pre>

where **$txt** is a string.

This is applicable only to english. So, pronunciation will be different for other languages. In this case, replace the "en" in the url to the short forms of the language needed to make speech.

You can see short forms in <a href="https://github.com/subins2000/francium-translator/blob/master/class.translator.php" target="_blank">this file</a> from line 52.

# UPDATE &#8211; 14 Dec 2015

This doesn&#8217;t work anymore as Google enhanced the URL making us unable to use it. Now, a 403 status is returned when requests are made to the URL.

So, sorry guys and gals it dosen&#8217;t work anymore