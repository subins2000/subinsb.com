---
title: 'Create A Trending Box In PHP With MySQL & HTML'
author: Subin Siby
type: post
date: 2014-02-02T14:08:56+00:00
url: /trending-box-html-php-mysql
authorsure_include_css:
  - 
  - 
  - 
categories:
  - CSS
  - HTML
  - MySQL
  - PHP
  - SQL

---
Trending boxes can be seen is social networks. Facebook, Twitter, Google + have it. Why not create one of your own. You can create your own using HTML and add styling by CSS. We will use PHP for updating most searched queries in MySQL database.

<div class="padlinks">
  <a class="demo" href="http://open.subinsb.com" target="_blank">Demo</a>
</div>

How the trending box looks like :<figure id="attachment_2387" class="wp-caption aligncenter">

[<img class="size-full wp-image-2387" alt="Trending Box" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0089.png" width="186" height="271" />][1]<figcaption class="wp-caption-text">Trending Box</figcaption></figure> 

We need a table for storing the search queries. For this we are going to create a table named "trends". Execute this SQL code to create the table

<pre class="prettyprint"><code>CREATE TABLE IF NOT EXISTS `trends` (
 `title` varchar(50) NOT NULL,
 `hits` int(11) NOT NULL,
 PRIMARY KEY (`title`),
 UNIQUE KEY `title` (`title`),
 KEY `title_2` (`title`),
 KEY `hits` (`hits`),
 FULLTEXT KEY `title_3` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;</code></pre>

When a user searches for a query in your search page, it should be automatically added to the "trends" table. To automatically add the queries, append the following code at the end or beginning of your search page. The **$q** variable should contain the search query.

<pre class="prettyprint"><code>&lt;?
$q="query";/* The Search Query */
if($q!=''){
 $sql=$dbh-&gt;prepare("UPDATE trends SET hits=hits+1 WHERE title=?");
 $sql-&gt;execute(array($q));
}
/* If There isn't a seacrh query in table, insert it */
if($sql-&gt;rowCount()==0 && $q!=""){
 $sql=$dbh-&gt;prepare("INSERT INTO trends(title,hits) VALUES(?,'1')");
 $sql-&gt;execute(array($q)); 
}
?&gt;</code></pre>

When a user searches for a query, the script updates the value "hits" to +1 ("hits" + 1). If it wasn&#8217;t successfully updated, the query is inserted in to the table with "hist" as "1".

The "hits" field determines the popularity of the query. The query with the most "hits" value is the most trending query.

To display the trending box, we get the first 10 popular queries (highest "hits" value) from the table. Then it is displayed in the descending order of the "hits".

<pre class="prettyprint"><code>&lt;div class="trending"&gt;
 &lt;font size="5" style="color:rgb(0,250,120);"&gt;Trending&lt;/font&gt;
 &lt;div class="trends"&gt;
  &lt;?
   $sql=$db-&gt;query("SELECT * FROM trends ORDER BY hits DESC LIMIT 9");
   foreach($sql as $r){
    $query=$r['title'];
    $wdot=strlen($query)&gt;=14 ? "....":"";
    $sp_t=str_split($query,14);
    $sp_t=$sp_t[0];
    echo '&lt;div style="padding:1px;"&gt;';
     echo '&lt;a href="search.php?query='.urlencode($query).'" title="'.$query.'"&gt;'.$sp_t."$wdot&lt;/a&gt;";
     echo '&lt;div style="margin-top:5px;"&gt;&lt;/div&gt;'; // Adds A Break like &lt;br/&gt;
    echo "&lt;/div&gt;";
   }
  ?&gt;
 &lt;/div&gt;
&lt;/div&gt;</code></pre>

If you want to delete the old non popular queries from table, add the following code where as you like :

<pre class="prettyprint"><code>&lt;?
$db-&gt;query("DELETE FROM trend WHERE hits=(SELECT MIN(hits) FROM (SELECT * FROM trend HAVING COUNT(hits)&gt;150) x);");
?&gt;
</code></pre>

Yes, the "x" should be in the query or you would get the derived table error (#1248).

Add this CSS code to design the trending box :

<pre class="prettyprint"><code>&lt;style&gt;
.trending{
 background: black;
 border-radius: 10px;
 padding:15px;
}
.trending .trends{
 color:#fff;
 padding-left:10px;
 margin-top:10px;
}
&lt;/style&gt;</code></pre>

The finished product will look something like this :<figure id="attachment_2387" class="wp-caption aligncenter">

[<img class="size-full wp-image-2387" alt="Trending Box" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0089.png" width="186" height="271" />][1]<figcaption class="wp-caption-text">Trending Box</figcaption></figure>

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0089.png