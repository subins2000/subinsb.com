---
title: Download Zip Files Dynamically in PHP
author: Subin Siby
type: post
date: 2014-03-18T17:38:16+00:00
url: /php-dynamic-download-zip-file
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - Download
  - header
  - HTTP

---
Zip file downloading is used by websites. The most important and popular of them is **WordPress**. They use Zip files for downloading plugins, themes and even their versions. They do it ofcourse, dynamically. You just provide the location of the file and PHP will download it to the user for you. Actually HTTP headers have the main role in the downloading.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/php/download-zip/" target="_blank">Demo</a>
</div>

We make the headers using **header** function in PHP and the browser will take care of the rest. The file path that you provide must be the absolute path. These are the two variables where you provide information about the Zip file :

<pre class="prettyprint"><code>$filename = "My Zip File Download.zip";
$filepath = "/var/www/subinsbdotcom/download_cdn/file.zip";</code></pre>

The download file name is stored in the **$filename** variable and the file location is in the **$filepath** variable.

Now we&#8217;re going to set the headers for making browser understand that this should be downloaded :

<pre class="prettyprint"><code>header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-type: application/octet-stream");
header('Content-Disposition: attachment; filename="'.$filename.'"');
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize($filepath));
ob_end_flush();
@readfile($filepath);</code></pre>

When the user visits the page, the download will start. The downloaded file will be of the name **My Zip File Download.zip**. You can change the name by changing the value of **$filename**.