---
title: Convert Seconds To Hours, Minutes in PHP
author: Subin Siby
type: post
date: 2016-03-24T09:03:52+00:00
url: /php-convert-seconds-hours-minutes-seconds
categories:
  - PHP
  - Program
tags:
  - Function
  - Second
  - time

---
Say you&#8217;re running a process that runs in the background. It&#8217;s better to not let the user lose their patience and tell them when it will finish. But, you have the time remaining in **seconds**. If we show the user :

<pre class="prettyprint"><code>567 seconds to complete</code></pre>

He/she (user) would have to take a calculator and convert it into minutes or hours. Or if the guy/gal is not a math genius, he would close the UI and say "What the Hell ?"

The primary point in UI is that

<pre class="prettyprint"><code>Don't irritate the user
</code></pre>

Mark my words : "You shouldn&#8217;t make the user angry".

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/php/seconds-to-human-readable/" target="_blank">Demo</a>
</div>

[<img src="//lab.subinsb.com/projects/blog/uploads/2016/03/lobby-downloader-app.png" alt="Seconds is converted to Hours, Minutes" height="207" />][1] 

Here is a simple function to convert seconds to hours, minutes and seconds separately or mixed together :

<pre class="prettyprint"><code>function secToHR($seconds) {
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds / 60) % 60);
  $seconds = $seconds % 60;
  return "$hours:$minutes:$seconds";
}</code></pre>

If you&#8217;re wondering why I chose **secToHR** as the function name, it&#8217;s the abbreviation of **Seconds to Human Readable**.

Or if you wanna display like this :

<pre class="prettyprint"><code>1 hour, 20 minutes remaining
10 minutes, 20 seconds remaining
15 seconds remaining</code></pre>

then, replace the **return** function with this :

<pre class="prettyprint"><code>return $hours &gt; 0 ? "$hours hours, $minutes minutes remaining" : ($minutes &gt; 0 ? "$minutes minutes, $seconds seconds remaining" : "$seconds seconds remaining");</code></pre>

BTW, I would suggest you do it like above ^, because it&#8217;s more user friendly.

## Usage

Simple as it is :

<pre class="prettyprint"><code>/**
 * Function with 'h:m:s' form as return value
 */
echo secToHR(560);
echo secToHR(10950);
/**
 * Function with 'remaining' in return value
 */
echo secToHR();</code></pre>

And the corresponding outputs :

<pre class="prettyprint"><code>0:9:20
3:2:30
9 minutes, 20 seconds remaining
3 hours, 2 minutes remaining
</code></pre>

I have used this in the <a href="https://github.com/subins2000/lobby-downloader" target="_blank">Downloader App</a> as I mentioned in the previous post.

 [1]: //lab.subinsb.com/projects/blog/uploads/2016/03/lobby-downloader-app.png