---
title: Check whether mysqli connection is made or not in php
author: Subin Siby
type: post
date: 2013-05-29T16:37:00+00:00
excerpt: 'This function helps to check whether connection the mysql server has been made or not.Here is the code:if&nbsp;($mysqli-&gt;ping())&nbsp;{echo&nbsp;("Our&nbsp;connection&nbsp;is&nbsp;ok!n");}&nbsp;else&nbsp;{&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;printf&...'
url: /check-whether-mysqli-connection-is-made-or-not-in-php
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - c7f737be29ef85700eb29a418e998baa
  - c7f737be29ef85700eb29a418e998baa
  - c7f737be29ef85700eb29a418e998baa
  - c7f737be29ef85700eb29a418e998baa
  - c7f737be29ef85700eb29a418e998baa
  - c7f737be29ef85700eb29a418e998baa
syndication_permalink:
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
  - http://sag-3.blogspot.com/2013/05/check-whether-mysqli-connection-is-made.html
categories:
  - PHP
tags:
  - Mysqli

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">This function helps to check whether connection the mysql server has been made or not.</span></p> 
  
  <div>
    <span style="font-family: inherit;">Here is the code:</span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><span style="background-color: white;"><span style="color: #007700;">if&nbsp;(</span><span style="color: #0000bb;">$mysqli</span><span style="color: #007700;">-></span><span style="color: #0000bb;">ping</span><span style="color: #007700;">())&nbsp;{</span></span></span><br /><span style="font-family: inherit;"><span style="background-color: white;"><span style="color: #007700;">echo</span><span style="color: #0000bb;">&nbsp;</span><span style="color: #007700;">(</span><span style="color: #dd0000;">"Our&nbsp;connection&nbsp;is&nbsp;ok!n"</span><span style="color: #007700;">);<br />}&nbsp;else&nbsp;{&nbsp;&nbsp;</span></span></span><span style="font-family: inherit;"><span style="background-color: white;"><span style="color: #007700;"><br /></span></span></span><span style="font-family: inherit;"><span style="background-color: white;"><span style="color: #007700;">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000bb;">printf&nbsp;</span><span style="color: #007700;">(</span><span style="color: #dd0000;">"Error:&nbsp;%sn"</span><span style="color: #007700;">,&nbsp;</span><span style="color: #0000bb;">$mysqli</span><span style="color: #007700;">-></span><span style="color: #0000bb;">error</span></span><span style="color: #007700;"><span style="background-color: white;">);<br />}</span></span></span>
    </p>
  </blockquote>
  
  <div>
    The <b>$mysqli</b>&nbsp;variable is a connection made via <b>mysqli</b>&nbsp;function.
  </div>
  
  <div>
    Know more about <b>mysqli ping</b>&nbsp;function <a href="http://in2.php.net/manual/en/mysqli.ping.php" >here</a>.
  </div>
</div>