---
title: Hide or show a div using data sent to a php file
author: Subin Siby
type: post
date: 2012-04-08T05:13:00+00:00
excerpt: 'This tutorial will help you to send data to a php file to hide a div ie sending a string to a php file to hide a div.Follow these steps.1 : Open the php file which contains the div to hide.&nbsp;&nbsp; &nbsp; &nbsp;Paste this code above the &lt;/head&g...'
url: /hide-or-show-a-div-using-data-sent-to-a-php-file
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 1c42b0d170b5ffeae789828ff56c3f62
  - 1c42b0d170b5ffeae789828ff56c3f62
  - 1c42b0d170b5ffeae789828ff56c3f62
  - 1c42b0d170b5ffeae789828ff56c3f62
  - 1c42b0d170b5ffeae789828ff56c3f62
  - 1c42b0d170b5ffeae789828ff56c3f62
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
syndication_permalink:
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
  - http://sag-3.blogspot.com/2012/04/tutorial-hide-or-show-div-using-data.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
categories:
  - PHP
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This tutorial will help you to send data to a php file to hide a div ie sending a string to a php file to hide a div.Follow these steps.</p> 
  
  <div>
  </div>
  
  <div>
    1 : Open the php file which contains the div to hide.&nbsp;
  </div>
  
  <div>
    &nbsp; &nbsp; &nbsp;Paste this code above the <b></head></b>&nbsp;tag</p> 
    
    <blockquote class="tr_bq">
      <p>
        <b><style type="text/css"></b><b><span style="font-family: Times, 'Times New Roman', serif;">#<?php echo $_GET[&#8216;div&#8217;]; ?>{</span></b><b><span style="font-family: Times, 'Times New Roman', serif;"><span style="color: red;">display:none;</span></span></b><b><span style="font-family: Times, 'Times New Roman', serif;">}</span></b><b><span style="font-family: Times, 'Times New Roman', serif;"></style></span></b>
      </p>
    </blockquote>
  </div>
  
  <p>
    <span style="color: red;">If you want to show a div change the line "display:none;" to "display:block;"</span>
  </p>
  
  <div>
    <span style="font-family: Times, 'Times New Roman', serif;">2 : Paste th</span><span style="font-family: Times, 'Times New Roman', serif;">e div code </span><b style="font-family: Times, 'Times New Roman', serif;">with an id </b><span style="font-family: Times, 'Times New Roman', serif;">on the php file.&nbsp;</span>
  </div>
  
  <div>
    <div class="code">
      <blockquote class="tr_bq">
        <p>
          <span style="font-family: Times, 'Times New Roman', serif;">&nbsp; &nbsp; &nbsp;<b><div id="hide">Hello World</div></b></span>
        </p>
      </blockquote>
    </div>
    
    <p>
      <span style="font-family: Times, 'Times New Roman', serif;">3 : If the name of the file you just typed is "<b>main" </b>and the url of the file is <b>"http://example.com/main" </b>then if you want to hide you have to type the url like this. <b>"http://example.com/main?div=hide" </b>. After going to this url the div id <b>"hide"</b></span><b style="font-family: Times, 'Times New Roman', serif;">&nbsp;</b><span style="font-family: Times, 'Times New Roman', serif;">will be hidden.</span></div> </div>