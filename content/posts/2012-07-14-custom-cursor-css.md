---
title: Custom Cursor CSS
author: Subin Siby
type: post
date: 2012-07-14T08:50:00+00:00
excerpt: 'This css code will help you to add custom cursor image.SEE DEMOCopy this css code to your html page.&lt;style&gt;body {cursor: url(https://lh5.googleusercontent.com/-u1rSVKP3Vpc/UAEqLZnKv0I/AAAAAAAABCQ/3alnkvGlnlQ/s32/cursor.png), url(https://lh5.googl...'
url: /custom-cursor-css
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
  - http://sag-3.blogspot.com/2012/07/custom-cursor-css.html
syndication_item_hash:
  - 854f6121f0f12bf93b9a2fce3d5e33f9
  - 854f6121f0f12bf93b9a2fce3d5e33f9
  - 854f6121f0f12bf93b9a2fce3d5e33f9
  - 854f6121f0f12bf93b9a2fce3d5e33f9
  - 854f6121f0f12bf93b9a2fce3d5e33f9
  - 854f6121f0f12bf93b9a2fce3d5e33f9
categories:
  - CSS
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This css code will help you to add custom cursor image.</p> 
  
  <p>
    <b><span style="font-size: large;"><a href="http://g2enjoy.blogspot.in/" >SEE DEMO</a></span></b><br /><b><br /></b>Copy this css code to your html page.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <b><style></b><span style="background-color: white;"><b>body {</b></span><span style="background-color: white;"><b>cursor: url(https://lh5.googleusercontent.com/-u1rSVKP3Vpc/UAEqLZnKv0I/AAAAAAAABCQ/3alnkvGlnlQ/s32/cursor.png), url(</b></span><b>https://lh5.googleusercontent.com/-u1rSVKP3Vpc/UAEqLZnKv0I/AAAAAAAABCQ/3alnkvGlnlQ/s32/cursor.png</b><span style="background-color: white;"><b>), auto; }</b></span><span style="background-color: white;"><b></style></b></span>
    </p>
  </blockquote>
  
  <div style="text-align: center;">
    <span style="font-size: large;">OR</span>
  </div>
  
  <div style="text-align: left;">
    Copy this to your css file
  </div>
  
  <div style="text-align: left;">
    <blockquote class="tr_bq">
      <p>
        <span style="background-color: white;"><b>body {</b></span><span style="background-color: white;"><b>cursor: url(</b></span><b>https://lh5.googleusercontent.com/-u1rSVKP3Vpc/UAEqLZnKv0I/AAAAAAAABCQ/3alnkvGlnlQ/s32/cursor.png</b><span style="background-color: white;"><b>), url(</b></span><b>https://lh5.googleusercontent.com/-u1rSVKP3Vpc/UAEqLZnKv0I/AAAAAAAABCQ/3alnkvGlnlQ/s32/cursor.png</b><span style="background-color: white;"><b>), auto;}</b></span>
      </p>
    </blockquote>
  </div>
</div>