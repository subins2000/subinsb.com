---
title: How To Change Your Country By Using Proxy in Ubuntu ?
author: Subin Siby
type: post
date: 2013-09-30T17:15:00+00:00
excerpt: 'There are certain restrictions on some sites such as Country Restriction. Mostly there are sites just for US&nbsp;citizens. For Example The most 99&nbsp;cent domain offers from GoDaddy&nbsp;is only available to US&nbsp;citizens. They check the country ...'
url: /how-to-change-your-country-by-using-proxy-in-ubuntu
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 0c09307dac0d88915b2ae0e78301ad25
  - 0c09307dac0d88915b2ae0e78301ad25
  - 0c09307dac0d88915b2ae0e78301ad25
  - 0c09307dac0d88915b2ae0e78301ad25
  - 0c09307dac0d88915b2ae0e78301ad25
  - 0c09307dac0d88915b2ae0e78301ad25
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/change-country-using-proxy-ubuntu.html
categories:
  - Linux
  - Ubuntu
tags:
  - GNOME
  - IP
  - Proxy

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  There are certain restrictions on some sites such as <b>Country Restriction</b>. Mostly there are sites just for <b>US</b>&nbsp;citizens. For Example The most <b>99</b>&nbsp;cent domain offers from <b>GoDaddy</b>&nbsp;is only available to <b>US</b>&nbsp;citizens. They check the country by <b>IP</b>&nbsp;Address. I&#8217;m going to tell you how to Change your Country by changing the <b>IP</b>&nbsp;address using a&nbsp;<b>PROXY</b>. This is explained to work on <b>Ubuntu</b>, you can also use this method in <b>Windows</b>&nbsp;& Others but in a different way.<br />OK. First Of All Open Terminal (<b>CTRL&nbsp;+ ALT&nbsp;+ T</b>) and do the following command :</p> 
  
  <blockquote class="tr_bq">
    <p>
      gnome-network-properties
    </p>
  </blockquote>
  
  <p>
    You will get a Window Like Below :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-Nyh1YH6PLk8/UkmtvMXmdeI/AAAAAAAADDQ/5uZ60C9iSko/s1600/0048.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-Nyh1YH6PLk8/UkmtvMXmdeI/AAAAAAAADDQ/5uZ60C9iSko/s1600/0048.png" /></a>
  </div>
  
  <p>
    Now, What you have to do is go to a Free Proxy Site like&nbsp;<a href="http://free-proxy-list.net/">free-proxy-list.net</a>&nbsp;and get a <b>Proxy IP </b>and <b>Port</b>. You can choose the <b>Proxy</b>&nbsp;based on countries or anything else.<br />Click On <b>Manual proxy configuration</b>&#8216;s radio button and choose <b>Use the same</b>&nbsp;proxy for all protocols. Then type in the <b>Proxy IP</b>&nbsp;address and port. Example :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-G_nZkJNBmOM/Ukmv0uoJymI/AAAAAAAADDc/9IbHZSoUU3A/s1600/0049.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-G_nZkJNBmOM/Ukmv0uoJymI/AAAAAAAADDc/9IbHZSoUU3A/s1600/0049.png" /></a>
  </div>
  
  <p>
    Then Click on <b>Close</b>&nbsp;button. You will be asked for using the proxy completely on your system which means that the applications you install via <b>syanptic</b>&nbsp;or <b>software-center</b>&nbsp;use the <b>Proxy</b>&nbsp;mentioned. If you just want to trick a site, then ignore the dialog box. Here is an example of my <b>IP</b>&nbsp;details&nbsp;when my <b>Proxy</b>&nbsp;was changed :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-3HbpUGhfaqM/UkmxW0UI_dI/AAAAAAAADDo/R0ImYhVN61Y/s1600/0050.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-3HbpUGhfaqM/UkmxW0UI_dI/AAAAAAAADDo/R0ImYhVN61Y/s1600/0050.png" /></a>
  </div>
  
  <p>
    If you have any doubts/suggestions/problems/feedback say it out in the comments, I will help you.</div>