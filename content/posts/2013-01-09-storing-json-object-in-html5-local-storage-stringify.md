---
title: 'Storing JSON object in HTML5 Local Storage : Stringify'
author: Subin Siby
type: post
date: 2013-01-09T08:08:00+00:00
excerpt: "This tutorial will help you to store JSON&nbsp;Object in LocalStorage.This method uses a function called&nbsp;stringify.Suppose we have a JSON&nbsp;object like this :var data = {name:'subin',class:'8A'};If we want to store this JSON object in Local Sto..."
url: /storing-json-object-in-html5-local-storage-stringify
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
  - http://sag-3.blogspot.com/2013/01/storing-json-object-in-localstorage.html
syndication_item_hash:
  - 4ff3520161b3b71be81bb00ab0736026
  - 4ff3520161b3b71be81bb00ab0736026
  - 4ff3520161b3b71be81bb00ab0736026
  - 4ff3520161b3b71be81bb00ab0736026
  - 4ff3520161b3b71be81bb00ab0736026
  - 4ff3520161b3b71be81bb00ab0736026
categories:
  - HTML
  - HTML5
  - JavaScript
tags:
  - JSON
  - Local Storage

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This tutorial will help you to store <b>JSON</b>&nbsp;Object in LocalStorage.</p> 
  
  <div>
    This method uses a function called&nbsp;<b>stringify</b>.
  </div>
  
  <div>
    Suppose we have a <b>JSON</b>&nbsp;object like this :
  </div>
  
  <blockquote class="tr_bq">
    <p>
      var data = {name:&#8217;subin&#8217;,class:&#8217;8A&#8217;};
    </p>
  </blockquote>
  
  <div>
    If we want to store this <b>JSON </b>object in <b>Local Storage</b>&nbsp;then use the function below:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      localStorage[&#8216;<span style="color: red;">info</span>&#8216;]=JSON.stringify(<span style="color: red;">data</span>);
    </p>
  </blockquote>
  
  <div>
    That&#8217;s it. <b>JSON</b>&nbsp;Object is now stored in your <b>Local Storage</b>.
  </div>
</div>