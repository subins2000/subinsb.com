---
title: GOD Is Open Source
author: Subin Siby
type: post
date: 2014-03-27T17:03:36+00:00
url: /god-open-source
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Personal
tags:
  - GOD
  - Open Source

---
<pre class="prettyprint"><code>This is just my opinion. Say what you think about this using the comments.</code></pre>

There are many many religions in the world and we are all part of a religion (or not). But, whatever the religion we are in, we all have a single GOD. A GOD who created atoms to the biggest mammal in Earth. All religions are just a path to GOD. Every religion points to GOD and whatever religion we choose, we all reach a single place. So, if GOD created all the things in universe, then he / she may have created the religions.

<pre class="prettyprint"><code>But, if GOD counted everyone as equal, then he won't waste time by creating religions.</code></pre>

Human beings created religion, not GOD. We all have to understand this fact.

<pre class="prettyprint"><code>If all religions have the same GOD, then we can say the religions are various releases of "path to GOD"</code></pre>

GOD created a religion, it&#8217;s called **null** which means It&#8217;s not defined. GOD made his religion Open Source. Humans got the code and made changes as they like. Thus came religions like Christian, Muslim, Hindu and more and more. Humans still make religions by changing the code more. But, ultimately the code belongs to GOD. So, all religions are same. GOD is an Open Source supporter and software maker. His / Her Official Operating System is the Universe and the various components of the OS are the celestial bodies.

If GOD is Open Source, why can&#8217;t you be one ? Use Open Source Software, be Open Source and support Open Source. Here is a technological brief of what I mean :

<pre class="prettyprint"><code>GOD      = A Kernel
Religion = Operating Systems using the "GOD" Kernel
The "GOD Kernel" is Open Source and anyone can use it. It is licensed under GPL Version 2 or more.</code></pre>