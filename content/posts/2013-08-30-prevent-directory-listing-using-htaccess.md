---
title: Prevent Directory Listing using HTACCESS
author: Subin Siby
type: post
date: 2013-08-30T03:30:00+00:00
excerpt: "If you don't want your site users to see your directory listing, you can do a simple trick in&nbsp;.htaccess&nbsp;file.Here is a screenshot of a directory listing on Apache Server&nbsp;:Now, to disable directory listing of all types of files, type in t..."
url: /prevent-directory-listing-using-htaccess
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 73e449f44a2a681ab814c48da5aa21d5
  - 73e449f44a2a681ab814c48da5aa21d5
  - 73e449f44a2a681ab814c48da5aa21d5
  - 73e449f44a2a681ab814c48da5aa21d5
  - 73e449f44a2a681ab814c48da5aa21d5
  - 73e449f44a2a681ab814c48da5aa21d5
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
  - http://sag-3.blogspot.com/2013/08/prevent-directory-list-using-htaccess.html
categories:
  - PHP
tags:
  - .htaccess
  - Apache
  - Directory
  - Listing
  - Prevention

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you don&#8217;t want your site users to see your directory listing, you can do a simple trick in&nbsp;<b>.htaccess</b>&nbsp;file.<br />Here is a screenshot of a directory listing on <b>Apache Server</b>&nbsp;:</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-RtCUJR3z8Bc/Uh9tnU-hDKI/AAAAAAAAC-o/s9CVng4OVGc/s1600/0036.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-RtCUJR3z8Bc/Uh9tnU-hDKI/AAAAAAAAC-o/s9CVng4OVGc/s1600/0036.png" /></a>
  </div>
  
  <p>
    <span id="goog_1988981101"></span><span id="goog_1988981102"></span>Now, to disable directory listing of all types of files, type in the following code in <b>.htaccess</b>&nbsp;:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      IndexIgnore *
    </p>
  </blockquote>
  
  <p>
    This will change the directory listing to this :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-BoM8drKvtWs/Uh9uWwgcCII/AAAAAAAAC-w/4vKYszE8Ifo/s1600/0037.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-BoM8drKvtWs/Uh9uWwgcCII/AAAAAAAAC-w/4vKYszE8Ifo/s1600/0037.png" /></a>
  </div>
  
  <p>
    The empty directory listing looks ugly. To make it beautiful, you can add an index page for all directory listings without adding each index files on each directory. Use the following code on <b>.htaccess</b>&nbsp;for that.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      DirectoryIndex index.php /<span style="color: red;">primary.php</span>
    </p>
  </blockquote>
  
  <p>
    The above code will make all directory&#8217;s index file <b>primary.php</b>&nbsp;which is kept in the root directory of the site <b>if</b>&nbsp;there isn&#8217;t an <b>index.php</b>&nbsp;file in the directory. I have made a <b>primary.php</b>&nbsp;file and added it as the <b>index</b>&nbsp;file of directory and It looks like below :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-1TONH8ftbfo/Uh9wD3tfETI/AAAAAAAAC-8/hOZ9CR_g9eA/s1600/0038.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-1TONH8ftbfo/Uh9wD3tfETI/AAAAAAAAC-8/hOZ9CR_g9eA/s1600/0038.png" /></a>
  </div>
  
  <p>
    </div>