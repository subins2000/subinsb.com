---
title: Stop playback in HTML5 audio using Javascript
author: Subin Siby
type: post
date: 2013-01-13T17:00:00+00:00
excerpt: "You may wanted to stop playback of an audio playing via audio&nbsp;HTML5&nbsp;tag.Here is the code :document.getElementById('audio').currentTime = 0;//By IDdocument.getElementsByTagName('audio').currentTime = 0;//By TagIt's that simple. Just use&nbsp;c..."
url: /stop-playback-in-html5-audio-using-javascript
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - b0d9da7f71c1cadbf48afd118b1666c0
  - b0d9da7f71c1cadbf48afd118b1666c0
  - b0d9da7f71c1cadbf48afd118b1666c0
  - b0d9da7f71c1cadbf48afd118b1666c0
  - b0d9da7f71c1cadbf48afd118b1666c0
  - b0d9da7f71c1cadbf48afd118b1666c0
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
  - http://sag-3.blogspot.com/2013/01/stop-playback-in-html5-audio-using.html
categories:
  - HTML
  - HTML5
  - JavaScript
tags:
  - Audio
  - playback

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You may wanted to stop playback of an audio playing via <b>audio</b>&nbsp;<b>HTML5</b>&nbsp;tag.<br />Here is the code :</p> 
  
  <blockquote class="tr_bq">
    <p>
      document.getElementById(&#8216;audio&#8217;).currentTime = 0;//By ID<br />document.getElementsByTagName(&#8216;audio&#8217;).currentTime = 0;//By Tag
    </p>
  </blockquote>
  
  <p>
    It&#8217;s that simple. Just use&nbsp;<b>currentTime = 0</b>.</div>