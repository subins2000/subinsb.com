---
title: 'Resuming Download In Wget, cURL & aria2c'
author: Subin Siby
type: post
date: 2015-04-22T04:30:32+00:00
url: /resume-download-wget-curl-aria2c
categories:
  - Linux
  - Operating System
  - Ubuntu

---
We all download stuffs and we want it to be perfect. No lagging, just download the fastest way, right ?

I don&#8217;t download via browsers these days, because it is too slow (< 5 KB/s). Google Chrome downloads are the worst. So, I download via command line now.

Not all downloading is easy. Sometimes it will break up. So, we must resume it. Only some servers allow to resume downloads. But, we need client softwares that is able to resume downloads.

There are lots of software that can be used to download in **Linux** and **Windows**. Most of them are GUIs. But, I like using commands.

In this post, I&#8217;m going to say how to resume downloads in **wGet**, **cURL** and **aria2c**. Since, I use **Ubuntu**, you should read the stuff **Ubuntu Linux** &#8211; wise.


# cURL

If you use **PHP**, you can see that it has a default **cURL** extension. It is one of the most popular tools to download. But, it is complicated and not as easy as **wGet** or **aria2c**. **Chrome** uses **cURL** and you can get the **cURL** command of a file using the Developer Tools (F12) in Chrome.

Install it by :

<pre class="prettyprint"><code>sudo apt-get install curl</code></pre>

And here is the command to download

<pre class="prettyprint"><code>curl -L -C - -o "myfile.zip" "http://example.com/file.zip"</code></pre>

Here is what the options mean :

<table class="table">
  <tr>
    <td>
      <h2>
        <a href="#option"><span id="option">Option</span></a>
      </h2>
    </td>
    
    <td>
      <h2>
        <a href="#what-it-does"><span id="what-it-does">What It Does</span></a>
      </h2>
    </td>
  </tr>
  
  <tr>
    <td>
      -L
    </td>
    
    <td>
      Some URLs redirect to some other sites. This option tells cURL to obtain the file from that redirected location.
    </td>
  </tr>
  
  <tr>
    <td>
      -C &#8211;
    </td>
    
    <td>
      This option is for telling cURL to resume download and the dash ("-") that follows it is to automatically detect the size of the file to continue downloading.
    </td>
  </tr>
  
  <tr>
    <td>
      -o "myfile.zip"
    </td>
    
    <td>
      This parameter says the output location of the file being downloaded.
    </td>
  </tr>
</table>

The last quoted value is the URL of the file. This command can be used to download for the first time as well as resuming it later.

# aria2c

What a funny, complicated name, huh ? This command is the easiest one. I use this now to download.

You can install it by manually compiling. For **Linux**, download the plain [.tar.xz][3]{.name} file. Here&#8217;s the manual compiling commands to use in the directory :

<pre class="prettyprint"><code>./configure && sudo make && sudo make install</code></pre>

Now, here&#8217;s the command to download files :

<pre class="prettyprint"><code>aria2c -c -m 0 -o "myfile.zip" "http://example.com/file.zip"</code></pre>

Here is the explanation of each options used in the above command :

<table class="table">
  <tr>
    <td>
      <h2>
        <a href="#option-2"><span id="option-2">Option</span></a>
      </h2>
    </td>
    
    <td>
      <h2>
        <a href="#what-it-does-2"><span id="what-it-does-2">What It Does</span></a>
      </h2>
    </td>
  </tr>
  
  <tr>
    <td>
      -c
    </td>
    
    <td>
      Tells aria2c to resume downloads
    </td>
  </tr>
  
  <tr>
    <td>
      -m 0
    </td>
    
    <td>
      This option will make aria2c retry downloads for unlimited times. Some servers tend to break connection after some time. But, this option makes aria2c to auto retry when such things happen.
    </td>
  </tr>
  
  <tr>
    <td>
      -o "myfile.zip"
    </td>
    
    <td>
      This parameter says the output location of the file being downloaded.
    </td>
  </tr>
</table>

Like before, the last quoted value is the URL of the file.

# wGet

**wGet** is another popular downloading software. It&#8217;s use is similar to that of the other tools. But, one option is different.

Install it by :

<pre class="prettyprint"><code>sudo apt-get install wget</code></pre>

Here is a resumable download command :

<pre class="prettyprint"><code>wget -d -c --tries=0 --read-timeout=30 -O "myfile.zip" "http://example.com/file.zip"</code></pre>

And here is the explanation of the options used :

<table class="table">
  <tr>
    <td>
      <h2>
        <a href="#option-3"><span id="option-3">Option</span></a>
      </h2>
    </td>
    
    <td>
      <h2>
        <a href="#what-it-does-3"><span id="what-it-does-3">What It Does</span></a>
      </h2>
    </td>
  </tr>
  
  <tr>
    <td>
      -d
    </td>
    
    <td>
      Tells wGet to Download the File
    </td>
  </tr>
  
  <tr>
    <td>
      -c
    </td>
    
    <td>
      Tells wGet to resume download
    </td>
  </tr>
  
  <tr>
    <td>
      &#8211;tries=0
    </td>
    
    <td>
      Tells wGet to retry connections unlimitedly when it is interrupted.
    </td>
  </tr>
  
  <tr>
    <td>
      &#8211;read-timeout=30
    </td>
    
    <td>
      If no response is received in 30 seconds, exit and establish a new connection
    </td>
  </tr>
  
  <tr>
    <td>
      -O "myfile.zip"
    </td>
    
    <td>
      Sets the output location of the downloaded file.
    </td>
  </tr>
</table>

Of the above 3, I would certainly recommend using **aria2c**, because it&#8217;s easy to use and is fast. Have fun downloading .. 🙂

 [1]: #curl
 [2]: #aria2c
 [3]: http://sourceforge.net/projects/aria2/files/stable/aria2-1.18.10/aria2-1.18.10.tar.xz/download "Click to download aria2-1.18.10.tar.xz"
 [4]: #wget
