---
title: How to alert user that he is the ___th visitor of blogger/blogspot blog?
author: Subin Siby
type: post
date: 2012-11-22T13:18:00+00:00
excerpt: "If you ever visited my blog on the day it's reaching a big view (24000 , 25000) you will get a javascript alert saying you are the 24000th visitor like this :The above image shows that the blog alerted when the user was the 23000&nbsp;visitor. It will ..."
url: /how-to-alert-user-that-he-is-the-___th-visitor-of-bloggerblogspot-blog
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 312cca338a08a415db901ed94ccb65c8
  - 312cca338a08a415db901ed94ccb65c8
  - 312cca338a08a415db901ed94ccb65c8
  - 312cca338a08a415db901ed94ccb65c8
  - 312cca338a08a415db901ed94ccb65c8
  - 312cca338a08a415db901ed94ccb65c8
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
  - http://sag-3.blogspot.com/2012/11/how-to-alert-user-that-he-is-th-visitor.html
categories:
  - Blogger
  - HTML
  - JavaScript
  - jQuery

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">If you ever visited my blog on the day it&#8217;s reaching a big view (<b>24000</b> , <b>25000</b>) you will get a javascript alert saying you are the <b>24000</b>th visitor like this :</span></p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-R0PFGsxOylg/UK4bf9bsjTI/AAAAAAAACTo/8H9o8VpHUkc/s1600/alert.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><span style="font-family: inherit;"><img border="0" src="//1.bp.blogspot.com/-R0PFGsxOylg/UK4bf9bsjTI/AAAAAAAACTo/8H9o8VpHUkc/s1600/alert.png" /></span></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="font-family: inherit;">The above image shows that the blog alerted when the user was the <b>23000</b>&nbsp;visitor. It will also change the background to <a href="http://themes.googleusercontent.com/image?id=0BwVBOzw_-hbMYWRiZmQ3NDctZGQyZC00MWU0LWE0ZDMtMGFlZWI0MWM4NzQ1" >this</a>. Want to have this feature on your blog ???&nbsp;</span><span style="font-family: inherit;">You are in the right place.</span>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="font-family: inherit;"><br /></span>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <u style="font-family: inherit;"><b><span style="font-size: large;">Requirements</span></b></u>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <u style="font-family: inherit; font-size: x-large;"><br /></u>
  </div>
  
  <div>
    <span style="font-family: inherit;">One Stats Widget (Only one).</span>
  </div>
  
  <div>
    <span style="font-family: inherit;"><br /></span>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="font-family: inherit;"><span style="background-color: white; color: #333333; line-height: 20px;">Add this code before&nbsp;</span><b style="background-color: white; color: #333333; line-height: 20px;"></head></b><span style="background-color: white; color: #333333; line-height: 20px;">&nbsp;if you haven&#8217;t yet added a&nbsp;</span><b style="background-color: white; color: #333333; line-height: 20px;">script src</b><span style="background-color: white; color: #333333; line-height: 20px;">&nbsp;of&nbsp;</span><b style="background-color: white; color: #333333; line-height: 20px;">Jquery</b><span style="background-color: white; color: #333333; line-height: 20px;">&nbsp;</span><b style="background-color: white; color: #333333; line-height: 20px;">Library</b><span style="background-color: white; color: #333333; line-height: 20px;">.</span></span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="font-family: inherit;"><span style="background-color: white; color: #333333; line-height: 18px; white-space: pre; word-spacing: -3px;">Add this Code before </span><b style="background-color: white; color: #333333; line-height: 18px; white-space: pre; word-spacing: -3px;"></head></b><span style="background-color: white; color: #333333; line-height: 18px; white-space: pre; word-spacing: -3px;"> tag.</span></span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><span style="background-color: white;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;">if($(&#39;#Stats1_totalCount&#39;).text()==&#39;</span><span style="color: orange;"><b>25000</b> </span><span style="color: #333333;">&#39;){ $(&#39;body&#39;).css({background: &quot;#111 url(//themes.googleusercontent.com/image?id=0BwVBOzw_-hbMYWRiZmQ3NDctZGQyZC00MWU0LWE0ZDMtMGFlZWI0MWM4NzQ1) repeat-x fixed top center /* Credit: michieldb (http://www.istockphoto.com/googleimages.php?id=4252213&platform=blogger) */&quot;}, 900); if(localStorage[</span></span></span><span style="background-color: white;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;">$(&#39;body&#39;).css({background: &quot;#111 url(//themes.googleusercontent.com/image?id=0BwVBOzw_-hbMYWRiZmQ3NDctZGQyZC00MWU0LWE0ZDMtMGFlZWI0MWM4NzQ1) repeat-x fixed top center /* Credit: michieldb (http://www.istockphoto.com/googleimages.php?id=4252213&platform=blogger) */&quot;}, 900);</span></span></span><span style="background-color: white;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;">if(localStorage[</span><span style="line-height: normal; white-space: normal; word-spacing: 0px;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;">&quot;a</span>&quot;+</span></span><span style="color: #333333;">$(&quot;#Stats1_totalCount&quot;).text()</span><span style="line-height: 17px; white-space: normal; word-spacing: 0px;">.</span><span style="line-height: 17px; white-space: normal; word-spacing: 0px;">substr(</span><span style="line-height: 17px; white-space: normal; word-spacing: 0px;">0, 4</span><span style="line-height: 17px; white-space: normal; word-spacing: 0px;">)</span><span style="color: #333333;">]==&#39;s&#39;){}else{ alert(&quot;Congratulations, You are our &#39;+$(&quot;#Stats1_totalCount&quot;).text()+&#39;Vistor !!!!!&quot;);</span></span></span><span style="background-color: white;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;"><br /></span></span></span><span style="background-color: white;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;">localStorage[&quot;a</span>&quot;+</span></span><span style="background-color: white; color: #333333; line-height: 18px; white-space: pre; word-spacing: -3px;">$(&quot;#Stats1_totalCount&quot;).text()</span><span style="background-color: white; line-height: 17px;">.</span><span style="line-height: 17px;">substr(</span><span style="background-color: white; line-height: 17px;">0, 4</span><span style="line-height: 17px;">)</span><span style="background-color: white; color: #333333; line-height: 18px; white-space: pre; word-spacing: -3px;">] = &#39;s&#39;;</span><span style="background-color: white;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;">}</span></span></span><span style="background-color: white;"><span style="line-height: 18px; white-space: pre; word-spacing: -3px;"><span style="color: #333333;">}</span></span></span></span>
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="font-family: inherit;"><span style="background-color: white;"><span style="color: #333333;"><b>Replace the orange colored number&nbsp;</b></span></span><b style="background-color: white; color: #333333;">the next upcoming milestone stat of your blog</b><span style="background-color: white; color: #333333;">.</span></span>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="background-color: white; font-family: inherit;"><span style="color: red;">Note </span>:- You have to do the steps mentioned above every time when your blog is going to reach the milestone.</span>
  </div>
</div>