---
title: 'Fix: Dynamic Views failed to load properly'
author: Subin Siby
type: post
date: 2013-07-18T03:30:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on"><span>Well, this problem is pretty annoying to everyone who is using <b>Dynamic Views</b>, even to me. It's a <b>Blogger</b>&nbsp;fault as always and there has not been a solution to the problem since it started on the <b>Dynamic Views</b>&nbsp;launch.&nbsp;<span>P&auml;ivi &amp; Santeri of&nbsp;</span><span><b><a href="http://www.2globalnomads.info/" target="_blank">Global Nomads</a></b>&nbsp;founded a solution to this problem. Oh! I didn't said the problem was. The problem is that the whole <b>Blog</b>&nbsp;will fail to load completely/properly. Some of the problems happen when the blog won't properly load are listed below:</span></span><br><ol><li><span>Page List will become blank</span></li><li>Custom <b>CSS</b>&nbsp;won't load.</li><li>Ugly Blog.</li><li>Visitor Disaprovement</li></ol>and many more. Let's get back to the solution which is pretty simple.<br>Go to <b>Blogger</b>&nbsp;-&gt; <b>Template</b>&nbsp;-&gt; <b>Edit HTML</b>.<br>Search for&nbsp;<b>blogger.ui().configure()</b><br>You will get a code like this:<br><blockquote>&lt;script language='javascript' type='text/javascript'&gt;<br>&nbsp;setTimeout(function() {<br>&nbsp;blogger.ui().configure().view();<br>&nbsp;}, 0);<br>&lt;/script&gt;</blockquote>The only thing you should do is to replace the <b>seTimeout</b>&nbsp;value from <b>0</b>&nbsp;to <b>500</b>. And the final code will be:<br><blockquote>&lt;script language='javascript' type='text/javascript'&gt;<br>&nbsp;setTimeout(function() {<br>&nbsp;blogger.ui().configure().view();<br>&nbsp;}, 1000);<br>&lt;/script&gt;</blockquote>If you have any problems/feedback/suggestions then feel free to comment below on <b>Disqus</b>.</div>
url: /fix-dynamic-views-failed-to-load-properly
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
  - http://sag-3.blogspot.com/2013/07/fix-dynamic-views-failed-to-load.html
syndication_item_hash:
  - 508eaf6ce651eb85af20225859d0935c
  - 508eaf6ce651eb85af20225859d0935c
  - 508eaf6ce651eb85af20225859d0935c
  - 508eaf6ce651eb85af20225859d0935c
  - 508eaf6ce651eb85af20225859d0935c
  - 508eaf6ce651eb85af20225859d0935c
dsq_thread_id:
  - 1960979490
  - 1960979490
  - 1960979490
  - 1960979490
  - 1960979490
  - 1960979490
categories:
  - Blogger
  - CSS
  - HTML
  - JavaScript
tags:
  - Dynamic Views

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">Well, this problem is pretty annoying to everyone who is using <b>Dynamic Views</b>, even to me. It&#8217;s a <b>Blogger</b>&nbsp;fault as always and there has not been a solution to the problem since it started on the <b>Dynamic Views</b>&nbsp;launch.&nbsp;<span style="background-color: white;">Päivi & Santeri of&nbsp;</span><span style="background-color: white;"><b><a href="http://www.2globalnomads.info/" >Global Nomads</a></b>&nbsp;founded a solution to this problem. Oh! I didn&#8217;t said the problem was. The problem is that the whole <b>Blog</b>&nbsp;will fail to load completely/properly. Some of the problems happen when the blog won&#8217;t properly load are listed below:</span></span></p> 
  
  <ol style="text-align: left;">
    <li>
      <span style="background-color: white; font-family: inherit;">Page List will become blank</span>
    </li>
    <li>
      Custom <b>CSS</b>&nbsp;won&#8217;t load.
    </li>
    <li>
      Ugly Blog.
    </li>
    <li>
      Visitor Disaprovement
    </li>
  </ol>
  
  <p>
    and many more. Let&#8217;s get back to the solution which is pretty simple.<br />Go to <b>Blogger</b>&nbsp;-> <b>Template</b>&nbsp;-> <b>Edit HTML</b>.<br />Search for&nbsp;<b>blogger.ui().configure()</b><br />You will get a code like this:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <script language=&#8217;javascript&#8217; type=&#8217;text/javascript&#8217;><br />&nbsp;setTimeout(function() {<br />&nbsp;blogger.ui().configure().view();<br />&nbsp;}, 0);<br /></script>
    </p>
  </blockquote>
  
  <p>
    The only thing you should do is to replace the <b>seTimeout</b>&nbsp;value from <b></b>&nbsp;to <b>500</b>. And the final code will be:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <script language=&#8217;javascript&#8217; type=&#8217;text/javascript&#8217;><br />&nbsp;setTimeout(function() {<br />&nbsp;blogger.ui().configure().view();<br />&nbsp;}, 1000);<br /></script>
    </p>
  </blockquote>
  
  <p>
    If you have any problems/feedback/suggestions then feel free to comment below on <b>Disqus</b>.</div>