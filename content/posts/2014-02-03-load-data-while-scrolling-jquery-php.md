---
title: 'Load Data While Scrolling Down With jQuery & PHP'
author: Subin Siby
type: post
date: 2014-02-03T14:11:16+00:00
url: /load-data-while-scrolling-jquery-php
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - JavaScript
  - jQuery
  - PHP
tags:
  - Scroll
  - Tutorials

---
If your site have a lot of data to display on a page, it will make the page laggy. With this tutorial, you can limit the data on the main page and load new data when user reach the bottom of the page. This makes it easy to show huge data in a single page. <figure id="attachment_2393" class="wp-caption aligncenter">[<img class="size-medium wp-image-2393" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0090-300x111.png" alt="Before user reaches the end of page." width="300" height="111" />][1]<figcaption class="wp-caption-text">Before user reaches the end of page.</figcaption></figure>   <figure id="attachment_2399" class="wp-caption aligncenter">[<img class=" wp-image-2399  " src="//lab.subinsb.com/projects/blog/uploads/2014/02/0091-300x121.png" alt="After user reaches the end of page." width="300" height="121" />][2]<figcaption class="wp-caption-text">After user reaches the end of page.</figcaption></figure>

<div class="padlinks">
  <a class="demo" href="http://open.subinsb.com" target="_blank">Demo</a>
</div>

jQuery will trigger a POST request when user reaches the end of the page. The return data of the request contains the data older than the last data at the time of the trigger. In Server Side (PHP), we&#8217;ll use a SQL query that loads data less than the last data Id.


## Database Table

You need a table that contains the data with a unique ID. The unique Id can be accomplished using the Auto Increment option of the field with Primary Key option. Here is a sample which we will use throughout this tutorial :

<pre class="prettyprint"><code>CREATE TABLE data(
 id INT PRIMARY KEY AUTO_INCREMENT,
 data TEXT
);</code></pre>

## home.php

The file where the first latest added 10 datas are shown. When user scrolls to the bottom, the next 10 data are shown.

<pre class="prettyprint"><code>&lt;html&gt;
 &lt;head&gt;
  &lt;script src="http://code.jquery.com/jquery-latest.js"&gt;&lt;/script&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;?
  $sql=$dbh-&gt;prepare("SELECT * FROM data ORDER BY id DESC LIMIT 10");
  $sql-&gt;execute();
  while($r=$sql-&gt;fetch()){
   $id=$r['id'];
   $data=$r['data'];
  ?&gt;
   &lt;div id="&lt;?echo $id;?&gt;" class="data"&gt;
    &lt;?echo $data;?&gt;
   &lt;/div&gt;
  &lt;?
  }
  ?&gt;
  &lt;div class='load_more_data'&gt;
   &lt;div class='normal'&gt;Load More Data&lt;/div&gt;
   &lt;div class='loader'&gt;
    &lt;img src='load.gif' height='32' width='32'/&gt;
    &lt;span&gt;Loading More Data&lt;/span&gt;
   &lt;/div&gt;
  &lt;/div&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

## jQuery

The following jQuery code will call the loadData() function when the "Load More Data" button is clicked or when the user reaches the end of the page. The loadData() function will send the request to the load_data.php file and appends the return HTML data after the last data.

<pre class="prettyprint"><code>localStorage['dataRequested']=0;
function loadData(){
 var lastDataId=$(".data:last").attr("id");
 var params={};
 params["id"]=lastDataId;
 var t=$(".load_more_data");
 t.find(".normal").hide();
 t.find(".loader").show();
 if(localStorage['dataRequested']==0){
  localStorage['dataRequested']=1;
  $.post("load_data.php", params, function(data){
   if(data!=""){
    $(".data:last").after(data);
   }
   t.find(".normal").show();
   t.find(".loader").hide();
   localStorage['dataRequested']=0;
  });
 }
}
$(document).ready(function(){
 $(".load_more_data .normal").on("click", function(){
  loadData();
 });
 $(window).scroll(function(){
  if($(window).scrollTop() + $(window).height() == $(document).height() && $(".data").length!=0){
   loadData();
  }
 });
});</code></pre>

You can include the above JS code inside a script tag or include it externally via <script src>

## load_data.php

This file is where jQuery sents the request to get the data older than the last one.

<pre class="prettyprint"><code>&lt;?
if(isset($_POST['id'])){
 $id=$_POST['id'];
 $limit=10; // The Data Limit
 if($id!=""){
  $sql=$dbh-&gt;prepare("SELECT * FROM data WHERE id &lt; ? ORDER BY id DESC LIMIT ?");
  $sql-&gt;execute(array($id, $limit));
  if($sql-&gt;rowCount()!=0){
   while($r=$sql-&gt;fetch()){
    $id=$r['id'];
    $data=$r['data']
?&gt;
    &lt;div id="&lt;?echo $id;?&gt;" class="data"&gt;
     &lt;?echo $data;?&gt;
    &lt;/div&gt;
&lt;? 
   } // While End
  } // If Row Count!=0 End
 } // If $id!=blank End
} // If $_POST['id'] End
?&gt;</code></pre>

## Styling

Here is some styling to decorate the Load More Data button and data element :

<pre class="prettyprint"><code>.data{ 
 border: dashed 1px #48B1D9; 
 padding: 10px;
 margin: 5px;
}
.load_more_posts{
 background: #FAB359;
 border-radius: 10px;
 padding: 7px 25px;
 margin-top: 5px;
}
.load_more_posts .normal{
 cursor: pointer;
 vertical-align: middle;
}
.load_more_posts .loader img{vertical-align: middle;}
.load_more_posts .loader span{
 vertical-align: middle;
 margin-left: 10px;
}</code></pre>

This was a implementation of the code to load new posts in Open. This has not been tested. But I think it works well. If you found any errors, please comment and I will fix it. If you have any problems, please comment. I will be here to help you.

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0090.png
 [2]: //lab.subinsb.com/projects/blog/uploads/2014/02/0091.png
 [3]: #database-table
 [4]: #homephp
 [5]: #jquery
 [6]: #load-dataphp
 [7]: #styling
