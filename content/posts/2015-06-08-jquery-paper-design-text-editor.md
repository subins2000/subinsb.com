---
title: Notebook Paper Design Text Editor With jQuery
author: Subin Siby
type: post
date: 2015-06-08T03:30:20+00:00
url: /jquery-paper-design-text-editor
categories:
  - CSS
  - HTML
  - HTML5
  - JavaScript
  - jQuery
  - Program

---
Hey, all.. it&#8217;s been a while since I wrote anything on my blog. Sorry guys, but I was working on a project which will be released soon. 🙂

WYSIWYG editors are used by us all on the web and if we are going to create a site, it&#8217;s better if the editor on it is WYSIWYG. There are many text editors, but there are none with a Notebook Paper design. Well, in this post I will guide you in creating a notebook paper with **CSS3** and making that paper design into a text editor.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?class=39" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/css/notebook" target="_blank">Demo</a>
</div>

Yes, this requires **HTML5** and you can easily modify it to suit your needs. Let&#8217;s begin.


# HTML

## index.html

We will be making the layout first. It&#8217;s minimal and there are 5 resources loaded.

<pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
  &lt;head&gt;
    &lt;script src="//lab.subinsb.com/projects/jquery/core/jquery-latest.js"&gt;&lt;/script&gt;
    &lt;script src="js/jquery.notebook.js"&gt;&lt;/script&gt;
    &lt;link href="css/font-awesome.css" rel="stylesheet" /&gt;
    &lt;link href="css/jquery.notebook.css" rel="stylesheet" /&gt;
    &lt;link href="css/style.css" rel="stylesheet" /&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;div id="content"&gt;
      &lt;center&gt;&lt;h1&gt;Paper&lt;/h1&gt;&lt;/center&gt;
      &lt;div class="paper-wrapper"&gt;
        &lt;div class="paper"&gt;
          &lt;h2 style="margin: 0px;"&gt;HEADING&lt;/h2&gt;
          &lt;div class="entry"&gt;&lt;/div&gt;
        &lt;/div&gt;
      &lt;/div&gt;
    &lt;/div&gt;
  &lt;/body&gt;
&lt;/html&gt;
</code></pre>

A file called "font-awesome.css" is loaded. This file requires font files which should be stored in a folder called "fonts" in the root directory of website. Here are the font files to download :

  * <a href="//demos.subinsb.com/css/notebook/fonts/fontawesome-webfont.eot" target="_blank">fontawesome-webfont.eot</a>
  * <a href="//demos.subinsb.com/css/notebook/fonts/fontawesome-webfont.svg" target="_blank">fontawesome-webfont.svg</a>
  * <a href="//demos.subinsb.com/css/notebook/fonts/fontawesome-webfont.ttf" target="_blank">fontawesome-webfont.ttf</a>
  * <a href="//demos.subinsb.com/css/notebook/fonts/fontawesome-webfont.woff" target="_blank">fontawesome-webfont.woff</a>

After you placed these into the "fonts" folder, create folders "css" and "js" in the root directory.

And then, download <a href="//demos.subinsb.com/css/notebook/css/jquery.notebook.css" target="_blank">jquery.notebook.css</a> and <a href="//demos.subinsb.com/css/notebook/css/font-awesome.css" target="_blank">font-awesome.css</a> files and move them into the "css" folder.

Don&#8217;t forget to make sure that the links to the fonts in the "font-awesome.css" file is correct.

# CSS

## css/style.css

We add our custom styles in to this file and also, the CSS for making the notebook paper design is in this file :

<pre class="prettyprint"><code>.paper-wrapper{
  position: relative;
  height: 100%;
  margin: 20px 0 0;
  padding: 0;
	/* gradients - second red border, first red border, top white space, blue lines */
  background-image:  -webkit-linear-gradient(left, rgba(255,85,91, .2), rgba(255,85,91, .2)), -webkit-linear-gradient(left, rgba(255,85,91, .8), rgba(255,85,91, .8)), -webkit-linear-gradient(top, white 0px, white 69px, rgba(255,255,255,0) 70px), -webkit-repeating-linear-gradient(white 0px, white 18px, #A6FFED 19px, white 20px);
  background-image:  -moz-linear-gradient(left, rgba(255,85,91, .2), rgba(255,85,91, .2)), -moz-linear-gradient(left, rgba(255,85,91, .8), rgba(255,85,91, .8)), -moz-linear-gradient(top, white 0px, white 69px, rgba(255,255,255,0) 70px), -moz-repeating-linear-gradient(white 0px, white 18px, #A6FFED 19px, white 20px);
  background-image:  -ms-linear-gradient(left, rgba(255,85,91, .2), rgba(255,85,91, .2)), -ms-linear-gradient(left, rgba(255,85,91, .8), rgba(255,85,91, .8)), -ms-linear-gradient(top, white 0px, white 69px, rgba(255,255,255,0) 70px), -ms-repeating-linear-gradient(white 0px, white 18px, #A6FFED 19px, white 20px);
  background-image:  -o-linear-gradient(left, rgba(255,85,91, .2), rgba(255,85,91, .2)), -o-linear-gradient(left, rgba(255,85,91, .8), rgba(255,85,91, .8)), -o-linear-gradient(top, white 0px, white 69px, rgba(255,255,255,0) 70px), -o-repeating-linear-gradient(white 0px, white 18px, #A6FFED 19px, white 20px);
  background-image:  linear-gradient(left, rgba(255,85,91, .2), rgba(255,85,91, .2)), linear-gradient(left, rgba(255,85,91, .8), rgba(255,85,91, .8)), linear-gradient(top, white 0px, white 69px, rgba(255,255,255,0) 70px), repeating-linear-gradient(white 0px, white 18px, #A6FFED 19px, white 20px);
  background-size: 1px, 1px, auto, auto 30px;
  background-repeat: repeat-y, repeat-y, no-repeat, repeat;
  background-position: 90% 0px, 10% 0px, 0px 0px, 0px 0px;
}
.paper {
  margin: 0;
  display: block;
  min-height: 100%;
  font: 13pt Arial, sans-serif;  
  line-height: 30px;
  padding-top: 50px;
  padding-left: 10.5%;
  padding-right: 10.5%;
  text-align: justify;
}
.paper p{
  position: relative;
  min-height: 30px !important;
  margin: 0;
  z-index: 2;
}
.paper p:first-child{
  text-indent: 80px;
}
.paper:after {  
  content: '';
  display: block;
  width: 20px;
  height: 20px;
  border-radius: 10px;
  position: absolute;
  background: rgba(255,255,255,1);
  top: 40px;
  left: 2%;
  z-index: 10;
  -webkit-box-shadow: 
    inset 2px 2px 5px rgba(125,125,125,1),  
    2px 50.1em 5px rgba(255,255,255,1), 0px 50em rgba(125,125,125,.75),  
    2px 90.1em 5px rgba(255,255,255,1), 0px 90em rgba(125,125,125,.5),
    2px 130.1em 5px rgba(255,255,255,1), 0px 130em rgba(125,125,125,.25);
  -moz-box-shadow: 
    inset 2px 2px 5px rgba(125,125,125,1),  
    2px 50.1em 5px rgba(255,255,255,1), 0px 50em rgba(125,125,125,.75),  
    2px 90.1em 5px rgba(255,255,255,1), 0px 90em rgba(125,125,125,.5),
    2px 130.1em 5px rgba(255,255,255,1), 0px 130em rgba(125,125,125,.25);
  box-shadow: 
    inset 2px 2px 5px rgba(125,125,125,1),  
    2px 50.1em 5px rgba(255,255,255,1), 0px 50em rgba(125,125,125,.75),  
    2px 90.1em 5px rgba(255,255,255,1), 0px 90em rgba(125,125,125,.5),
    2px 130.1em 5px rgba(255,255,255,1), 0px 130em rgba(125,125,125,.25);
}
.paper:focus {
  outline: none;
}
.paper .entry{
  min-height: 300px;
}
</code></pre>

There aren&#8217;t any resources loaded in the above file. So, leave it as it is.

# JavaScript

Download the <a href="//demos.subinsb.com/css/notebook/js/jquery.notebook.js" target="_blank">jquery.notebook.js</a> file and move it into the "js" folder.

Add the following **script** tag inside the **index.html** file before </body> :

<pre class="prettyprint"><code>&lt;script&gt;
  $(document).ready(function(){
    $(".paper .entry").notebook();
  });
&lt;/script&gt;</code></pre>

That&#8217;s all you need. The text editor is powered by **JS**, but the style and most part is because of **CSS**. Thank you for reading and I hope it helped.

 [1]: #html
 [2]: #indexhtml
 [3]: #css
 [4]: #cssstylecss
 [5]: #javascript
