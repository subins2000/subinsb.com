---
title: Password protect folder using HTACCESS
author: Subin Siby
type: post
date: 2013-08-30T15:35:00+00:00
excerpt: 'HTACCESS&nbsp;is able to protect folders with passwords. To password protect folders you should mention the location of&nbsp;.htpasswd&nbsp;- a file that contains the username and password to unlock the folder.First of all create a .htaccess&nbsp;file ...'
url: /password-protect-folder-using-htaccess
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 9286794e24e58fe8c1261661622223f1
  - 9286794e24e58fe8c1261661622223f1
  - 9286794e24e58fe8c1261661622223f1
  - 9286794e24e58fe8c1261661622223f1
  - 9286794e24e58fe8c1261661622223f1
  - 9286794e24e58fe8c1261661622223f1
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
  - http://sag-3.blogspot.com/2013/08/use-htaccess-to-password-protect-folders.html
dsq_thread_id:
  - 1819450002
  - 1819450002
  - 1819450002
  - 1819450002
  - 1819450002
  - 1819450002
tags:
  - .htaccess
  - .htpasswd
  - Passwords
  - Username

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>HTACCESS</b>&nbsp;is able to protect folders with passwords. To password protect folders you should mention the location of&nbsp;<b>.htpasswd</b>&nbsp;&#8211; a file that contains the username and password to unlock the folder.<br />First of all create a <b>.htaccess</b>&nbsp;file if doesn&#8217;t exists and create a <b>.htpasswd</b>&nbsp;file.<br />Now Add username and password to the <b>.htpasswd</b>&nbsp;file which you can get from this <a href="http://demos.subinsb.com/htpasswd-password-generator/" >simple tool</a>.</p> 
  
  <p>
    Add the following text in <b>.htaccess</b>&nbsp;:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      AuthName "This folder requires a login"<br />AuthType Basic<br />AuthUserFile <span style="color: red;">/home/simsu/projects/TestPalace/Blog/Password_Protect/.htpasswd</span>AuthGroupFile /dev/null<br />require valid-user
    </p>
  </blockquote>
  
  <p>
    Replace the red highlighted text above to the location of the <b>.htpasswd</b>&nbsp;file. Now open the folder on the site and you will get a screen like below :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-j4SXoUi90k8/UiC7PfWTkvI/AAAAAAAAC_U/v0tXAadEJnI/s1600/0039.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-j4SXoUi90k8/UiC7PfWTkvI/AAAAAAAAC_U/v0tXAadEJnI/s1600/0039.png" /></a>
  </div>
</div>