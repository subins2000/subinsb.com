---
title: How To Integrate Disqus Into Blogger Dynamic Views
author: Subin Siby
type: post
date: 2013-07-04T03:30:00+00:00
excerpt: "The following trick works only on the following templates:Sidebar (Disqus for the first post on sidebar won't be available when loading the main page.)Classic (Disqus will only be available for the first post, because they won't allow you to put more t..."
url: /how-to-integrate-disqus-into-blogger-dynamic-views
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
  - http://sag-3.blogspot.com/2013/07/disqus-in-blogger-dynamic-views-template.html
dsq_thread_id:
  - 1795387444
  - 1795387444
  - 1795387444
  - 1795387444
  - 1795387444
  - 1795387444
syndication_item_hash:
  - 13d1213b66cb2d772bfa564cdbebc4d4
  - 13d1213b66cb2d772bfa564cdbebc4d4
  - 13d1213b66cb2d772bfa564cdbebc4d4
  - 13d1213b66cb2d772bfa564cdbebc4d4
  - 13d1213b66cb2d772bfa564cdbebc4d4
  - 13d1213b66cb2d772bfa564cdbebc4d4
categories:
  - Blogger
  - HTML
  - JavaScript
tags:
  - Disqus
  - featured
  - Posts

---
**Disqus** works in normal **Blogger** template but won&#8217;t work in **Dynamic Views** because it don&#8217;t support **HTML/Javascript** widget. But you can edit the contents in **head** tag of Template and I found a way to integrate the **Disqus** commenting system in to **Blogger**. I&#8217;m gonna show you how to do that.

<div class="padlinks">
  <a class="demo" href="http://gadgetsyoulike.blogspot.in/">DEMO</a>
</div>


# Works On

  1. Sidebar
  2. Classic
  3. Flipcard
  4. Magazine
  5. Mosaic
  6. Snapshot
  7. Timeslide

# Disadvantages

On Classic Templates **Disqus** will only be available for the first post, because they won&#8217;t allow you to put more than 1 **Disqus** Comment box on a page

# Dependencies

  1. Sharing buttons below posts should be Enabled
  2. Google + Comments should be hidden
  3. Blogger Default Comments should be hidden

# Current Version

Updated on 2017 July 29.

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//4.bp.blogspot.com/-KX5aGPTMgK8/UdQ1klD4sMI/AAAAAAAAC1Q/PHjGEz__13s/s1366/0026.png"><img src="//4.bp.blogspot.com/-KX5aGPTMgK8/UdQ1klD4sMI/AAAAAAAAC1Q/PHjGEz__13s/s1600/0026.png" alt="" border="0" /></a>
</div>

First of all go to **Blogger** ->** Settings** -> **Posts and Comments **and change the **Comment Location **option to **Hide**. If you enabled **Google + Comments**, Please disable that too.

You have two options :

  * Include the code below in "Text/JavaScript" widget of Blogger
  * Include the code below in your Blogger Template

Go to **Blogger **-> **Template **-> **Edit HTML** and Paste the following code before **&lt;/head&gt;** :

<pre class="prettyprint"><code>&lt;script type='text/javascript'&gt;
/**
 * Disqus in Blogger Dynamic views integrated by Subin Siby [http://goo.gl/jPb8TR]
 */
window.disqus_shortname = "<span style="background-color: #ff0;">example</span>"; // required: replace example with your forum shortname
var d = document, o = d.createElement(&quot;script&quot;); o.type = &quot;text/javascript&quot;, o.async = true, o.src = &quot;https://lab.subinsb.com/projects/Francium/dibd/load.min.js&quot;;
d.getElementsByTagName(&#39;head&#39;)[0].appendChild(o);
&lt;/script&gt;</code></pre>

Replace the <span style="background-color: #ff0;">yellow color background</span> text `example` in the above code with your **Disqus** shortname. Example: **subinsdemos** from **subinsdemos.disqus.com**.

It&#8217;s that simple. Your blog posts will now have a **Disqus** comment box. Enjoy !!

* **UPDATE** : "2015-07-14" The Sidebar first post problem has been fixed.
* **UPDATE** : "2015-09-24" An Improved version was released.
* **UPDATE** : "2016-02-05" Fixed bugs. Thanks [Spikeren][5].
* **UPDATE** : "2017-07-29" Changed URL to HTTPS. Thanks [Transmission Radio][6].
* **UPDATE** : "2019-11-07" Fixed bug in code display. Thanks [Amber][7].

If you have any problems/suggestions/feedback just comment in **Disqus** below.

 [1]: #works-on
 [2]: #disadvantages
 [3]: #dependencies
 [4]: #current-version
 [5]: //subinsb.com/how-to-integrate-disqus-into-blogger-dynamic-views/#comment-2497491925
 [6]: //subinsb.com/how-to-integrate-disqus-into-blogger-dynamic-views/#comment-3436277830
 [7]: //subinsb.com/how-to-integrate-disqus-into-blogger-dynamic-views/#comment-4678866719
