---
title: Earth Day Special – 21 KMs of Exploration
author: Subin Siby
type: post
date: 2016-04-22T15:56:45+00:00
url: /earth-day-21-kms-exploration
categories:
  - Personal
tags:
  - bicycle

---
Today is **Earth Day**, a special day dedicated to our mother Earth. I didn&#8217;t knew about this day until I saw the **Google Doodle** today morning at **12:15** (around-ish).

When I looked it up, I saw the ways to participate in this event :

> <a href="http://www.telegraph.co.uk/technology/2016/04/21/earth-day-2016-five-facts-to-inspire-you-and-how-to-get-involved/" target="_blank">Walk to work, cycle or take public transport</a>

Cycle ? That&#8217;s my favorite thing besides coding. It&#8217;s a way I could relax and exercise without losing the fun.

I had this plan long before to go **Westwards** and reach the beach. In my place, going **West** is nearer to reach the seashore than going in any other directions.

I looked up on **Google Maps** (of course) and found the way to reach the place faster with less distance. First, I wanted to go alone. But, going this much distance provoked my parents and said **No!** So, I went with my dad on a motorbike.

**Google** Navigation certainly helped and we reached the place.

^ This happened about one year ago. I can&#8217;t remember the date.

Today, I went alone on my **bicycle**. My Mom asked me not to do it, but I went on. My **GOD**, it was one hell of a trip !

<a href="http://goo.gl/zBz0Fi" target="_blank">This is the path I took.</a>

I left the house at **4 PM** with these :

  * A bottle of Water
  * Purse
  * Phone
  * And of course the cycle !

The starting of the trip did not go as I planned. Going about 20 Meters, I realized the tyres didn&#8217;t have enough air. So, I went back and filled it. How idiotic I was !

It was then alright. I calmly rode the cycle without any problem until I traveled about **8 KMs** (5 Miles). The cycle was moving slowly even though I gave the strength it needed. Then I realized the air inside the front tyre was less.

I then started to walk instead of riding it. I entered into a house in which a kid was in the porch with his cycle. I asked him if there was a cycle pump. He said "No". I only had a little more to go to reach the destination.

I **din&#8217;t give up** and went on walking. To my surprise, a man in the fifties smiled at me. I don&#8217;t know the guy and haven&#8217;t even met him before, yet he smiled. I saw that there were no cycle inside, but still I asked if there was a cycle pump.

He said, "Let me look" and I entered into the porch of the house. He was searching for the pump in the house. To my surprise, he looked for a while and brought me a pump. I didn&#8217;t expected a stranger to be this nice.

The air entered the back tyre finely. But, the air kept coming out in the front one. **Alas !** another problem. I was like "I&#8217;m screwed". A conversation was started :

> The Kind Guy : Did it work fine ?

> Me : No, the air is not getting inside the front tyre.

> The Kind Guy : It's probably because that the wal tube is damaged

> Me : Hmmm...

> The Kind Guy : Where are you from ?

> Me : Kunnamkulam

> The Kind Guy : Kunnamkulam !!! From all the way there ? Why did you come here for ?

> Me : I came here to go to the beach (Smiles)

> The Kind Guy : Oh ! (Smiles back) How will you bring the cycle back to Kunnamkulam ?

> Me : Is there a bicycle shop near ?

> The Kind Guy : Yeah, it's in the junction

You should know, people like me who goes on these "**long ****trips**" on a cycle is **considered** **CRAZY**.

I said **Thanks** and went back to the junction to find the **cycle** shop. I fixed the tyre for **5 Rupees** (0.075 USD). I went ahead.

After a while (about 2 **KMs**), I found the air inside the back tyre was also becoming less. Shit&#8230;Shit&#8230;Shit. I should have went back, fix it and come back. But, I went on as there is also another junction coming up&#8230; That was a really stupid mistake.

The cycle shop on the next junction was **closed**. I&#8217;m screwed again. Meanwhile, the air inside the tyre was going out.

I went on and reached the destination. The feeling I had in my mind cannot be expressed in words !!!<figure class="wp-caption aligncenter">

[<img class="" src="//i.imgur.com/80cNiIR.jpg" alt="A dream come true !" width="480" height="640" />][1]<figcaption class="wp-caption-text">A dream come true !</figcaption></figure>

The confidence and happiness I had at that point was the highest I ever had. I&#8217;m normally a shy person (but not too shy), but there I confidently asked a boy to take a photo of me with the cycle :<figure class="wp-caption aligncenter">

[<img class="" src="//i.imgur.com/MN3Fwe3.jpg" alt="What A Trip !" width="480" height="640" />][2]<figcaption class="wp-caption-text">What A Trip !</figcaption></figure>

I couldn&#8217;t stay there for a long time as I reached at **5:30 PM** and it becomes dark by **7 PM**. It took me **1 and a half hour** to reach here. How the hell am I gonna reach back with this tiredness and fatigues legs ? Also, I need to hurry to get my back tyre fixed.

So, I quickly left after staying for **15 Minutes**. After a while, most of the air inside the tyre had gone and the cycle was jumping when the wal tube part hit the ground.

If I continued this "jumping journey", the tube inside would become flat. I&#8217;m long away from home and my dad isn&#8217;t at home yet from work. I&#8217;m trapped. But, I continued walking and sometimes I rode the cycle with the flat tire even though it can damage the tyre from fixing.

A guy from North Indian said in Hindi :

> ये पन्जर है !

meaning "The tyre is punctured". The vast diversity of people Kerala have !

After some more while at **6:15 PM**, I reached the **same shop** where I had fixed my front tyre. The first time I came, the old man there gave me a balance of **5 Rupees**. Now, to fix the back tyre, I gave back the same coins I once got from him ! Ah, the irony !

It was a miracle that the shop didn&#8217;t close even after **6 PM**. Incidents like this make you believe in **GOD**.

Now, with the two tyres fixed, I was able to reach my house at **6:45 PM**. The return trip took only **1 Hour**.

I&#8217;m glad that I took this trip. But, I&#8217;m **never gonna go this much distance again**. I was very much lucky this time.

Now, I have a story to remember in my life. Guys and gals, make memorable moments in your like. You shouldn&#8217;t have any regrets when you die.

🙂

 [1]: //i.imgur.com/80cNiIR.jpg
 [2]: //i.imgur.com/MN3Fwe3.jpg
