---
title: Create a Search Engine In PHP, MySQL | Part 2
author: Subin Siby
type: post
date: 2014-04-09T07:37:37+00:00
url: /search-engine-in-php-part-2
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - MySQL
  - PHP
  - SQL
tags:
  - Tutorials

---
This is the Part 2 of "How To Create A Search Engine In PHP". In this part, We&#8217;ll add code to files that are being displayed to the users. We make the database and the table in this part.

<div class="padlinks">
  <a class="download" href="https://github.com/subins2000/search/archive/master.zip" target="_blank">Download</a><a class="demo" href="http://search.subinsb.com" target="_blank">Demo</a>
</div>


## index.php

The main page of our Web Search is index.php. It has a simple design and is not very advanced as Yahoo or something else.

<pre class="prettyprint"><code>&lt;?include("inc/functions.php");?&gt;
&lt;html&gt;
 &lt;head&gt;
  &lt;?head("", array("index"));?&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;?headerElem();?&gt;
  &lt;div class="container"&gt;
   &lt;center&gt;
    &lt;h1&gt;Web Search&lt;/h1&gt;
    &lt;form class="searchForm" action="search.php" method="GET"&gt;
     &lt;input type="text" autocomplete="off" name="q" id="query"/&gt;
     &lt;div&gt;
      &lt;button&gt;
       &lt;svg class='shape-search' viewBox="0 0 100 100" class='shape-search'&gt;&lt;use xlink:href='#shape-search'&gt;&lt;/use&gt;&lt;/svg&gt;
      &lt;/button&gt;
     &lt;/div&gt;
     &lt;p&gt;Free, Open Source & Anonymous&lt;/p&gt;
    &lt;/form&gt;
   &lt;/center&gt;
 &lt;/div&gt;
 &lt;?footer();?&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

We use simple functions for every files. **head()** function accepts two parameters. The first parameter is the title of the web page. The main site title will automatically be appended to the title you give in the first parameter when it&#8217;s displayed as **<title></title>**.

We also use **SVG** images to speed up the site. The "#shape-search" path is mentioned in the **functions.php** file which is dynamically added below the footer.**
  
**

## url.php

When a user clicks on a link of search result, the user actually goes to this page. This page will redirect back to the original URL. When the site owner sees the stats log, he will find our Search Engine URL as a referrer link and our site is promoted.

<pre class="prettyprint"><code>&lt;?
$url=isset($_GET['u']) ? urldecode($_GET['u']):"";
if(filter_var($url, FILTER_VALIDATE_URL) === FALSE || $url==""){
 header("Location: http://".$_SERVER['HTTP_HOST'], 302);
 exit;
}else{
 header("Location: ".$url);
 exit;
}
?&gt;</code></pre>

We check if the URL is valid too, because we don&#8217;t want any douche bags messing with our site (No Offence).

## search.php

We show the search results here in this page. Search results are obtained from **functions.php** and it is only displayed here.

<pre class="prettyprint"><code>&lt;?include("inc/functions.php");?&gt;
&lt;html&gt;
 &lt;head&gt;
 &lt;?head($GLOBALS['displayQ'], array("search"));?&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;?headerElem();?&gt;
  &lt;div class="container"&gt;
   &lt;script&gt;document.getElementById('query').focus();&lt;/script&gt;
   &lt;?
   if($GLOBALS['q']==""){
    echo "A query please...";
   }else{
    require "inc/spellcheck.php";
    $SC=new SpellCheck();
    $corSp=$SC-&gt;check($GLOBALS['q']);
    if($corSp!=""){
     echo "&lt;p style='color:red;font-size:15px;margin-bottom:10px'&gt;Did you mean ? &lt;br/&gt;&lt;a href='?q=$corSp'&gt;".$corSp."&lt;/a&gt;&lt;/p&gt;";
    }
    $res=getResults();
    if($res==0){
     echo "&lt;p&gt;Sorry, no results were found&lt;/p&gt;&lt;h3&gt;Search Suggestions&lt;/h3&gt;";
     echo "&lt;ul&gt;";
     echo "&lt;li&gt;Check your spelling&lt;/li&gt;";
     echo "&lt;li&gt;Try more general words&lt;/li&gt;";
     echo "&lt;li&gt;Try different words that mean the same thing&lt;/li&gt;";
     echo "&lt;/ul&gt;";
    }else{
   ?&gt;
    &lt;div class="info"&gt;
     &lt;strong&gt;&lt;?echo $res['count'];?&gt;&lt;/strong&gt;
     &lt;?echo $res['count']==1 ? "result" : "results";?&gt; found in &lt;?echo $res['time'];?&gt; seconds. Page &lt;?echo $GLOBALS['p'];?&gt;
    &lt;/div&gt;
    &lt;div class="results"&gt;
     &lt;?
     foreach($res['results'] as $re){
      $t=htmlFilt($re[0]);
      $u=htmlFilt($re[1]);
      $d=htmlFilt($re[2]);
      if(strlen($GLOBALS['q']) &gt; 2){
       $d=str_replace($GLOBALS['q'], "&lt;strong&gt;{$GLOBALS['q']}&lt;/strong&gt;", $d);
      }
     ?&gt;
      &lt;div class="result"&gt;
       &lt;h3 class="title"&gt;
        &lt;a target="_blank" onmousedown="this.href='&lt;?echo HOST;?&gt;/url.php?u='+encodeURIComponent(this.getAttribute('data-href'));" data-href="&lt;?echo $u;?&gt;" href="&lt;?echo $u;?&gt;"&gt;&lt;?echo strlen($t)&gt;59 ? substr($t, 0, 59)."..":$t;?&gt;&lt;/a&gt;
       &lt;/h3&gt;
       &lt;p class="url" title="&lt;?echo $u;?&gt;"&gt;&lt;?echo $u;?&gt;&lt;/p&gt;
       &lt;p class="description"&gt;&lt;?echo $d;?&gt;&lt;/p&gt;
      &lt;/div&gt;
     &lt;?
     }
     ?&gt;
    &lt;/div&gt;
    &lt;div class="pages"&gt;
     &lt;?
     $count=(ceil($res['count']/10));
     $start=1;
     if($GLOBALS['p'] &gt; 5 && $count &gt; ($GLOBALS['p'] + 4)){
      $start=$GLOBALS['p']-4;
      $count=$count &gt; ($start+8) ? ($start+8):$count;
     }elseif($GLOBALS['p'] &gt; 5){
      if($GLOBALS['p']==$count){
       $start=$GLOBALS['p']-8;
      }elseif($GLOBALS['p']==($count-1)){
       $start=$GLOBALS['p']-7;
      }elseif($GLOBALS['p']==($count-2)){
       $start=$GLOBALS['p']-6;
      }elseif($GLOBALS['p']==($count-3)){
       $start=$GLOBALS['p']-5;
      }elseif($GLOBALS['p']==($count-4)){
       $start=$GLOBALS['p']-4;
      }
     }elseif($GLOBALS['p'] &lt;= 5 && $count &gt; ($GLOBALS['p'] + 5)){
      $count=$start+8;
     }
     for($i=$start;$i&lt;=$count;$i++){
      $isC=$GLOBALS['p']==$i ? 'current':'';
      echo "&lt;a href='?p=$i&q={$GLOBALS['q']}' class='button $isC'&gt;$i&lt;/a&gt;";
     }
     ?&gt;
    &lt;/div&gt;
   &lt;? 
    }
   }
  ?&gt;
  &lt;/div&gt;
  &lt;?footer();?&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

It&#8217;s a tremendous file. As you can see in the **<head>** tag, we use **head()** function. We display the **query** as the title of the page. If the query is empty, main title will be shown and the string "A query please&#8230;" is shown inside **.container**.

## Database & Tables

It&#8217;s time to create the database and tables. You can create the database you like and I&#8217;ll give you the **SQL** query to create the table. BTW, the name of the table is **search**.

<pre class="prettyprint"><code>CREATE TABLE IF NOT EXISTS `search` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `title` varchar(60) NOT NULL,
 `url` text NOT NULL,
 `description` varchar(160) NOT NULL,
 PRIMARY KEY (`id`),
 UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;</code></pre>

We add the **title** of the page, **URL** of the page and **description** of the page to this table.

## Crawler

Now, lets&#8217; build the crawler which runs in the background. In the previous part of the tutorial, we added the PHPCrawl & Simple HTML Dom library to the **crawler** folder. In this part, we add code to some of the files we created before.

### crawlStatus.txt

The status of the crawling (0 / 1) is added inside this file. If it&#8217;s ****, **runCrawl.php** will start calling the **bgCrawl.php** which runs the crawler in the background. For starting the crawler, we&#8217;ll add **** inside this file.

<pre class="prettyprint"><code></code></pre>

### crawl.php

<pre class="prettyprint"><code>&lt;?
if(!isset($crawlToken) || $crawlToken!=798821){
 die("Error");
}
$dir=realpath(dirname(__FILE__));
function shutdown(){ 
 global $dir;
 $a=error_get_last(); 
 if($a==null){
  echo "No errors";
 }else{
  file_put_contents($dir."/crawlStatus.txt", "0");
  include($dir."/runCrawl.php");
 }
}
register_shutdown_function('shutdown'); 
set_time_limit(30);
include($dir."/../inc/config.php");
include($dir."/PHPCrawl/libs/PHPCrawler.class.php");
include($dir."/simple_html_dom.php");
function addURL($t, $u, $d){
 global $dbh;
 if($t!="" && filter_var($u, FILTER_VALIDATE_URL)){
  $check=$dbh-&gt;prepare("SELECT `id` FROM `search` WHERE `url`=?");
  $check-&gt;execute(array($u));
  $t=preg_replace("/s+/", " ", $t);
  $t=substr($t, 0, 1)==" " ? substr_replace($t, "", 0, 1):$t;
  $t=substr($t, -1)==" " ? substr_replace($t, "", -1, 1):$t;
  $t=html_entity_decode($t, ENT_QUOTES);
  $d=html_entity_decode($d, ENT_QUOTES);
  echo $u."n";
  if($check-&gt;rowCount()==0){
   $sql=$dbh-&gt;prepare("INSERT INTO `search` (`title`, `url`, `description`) VALUES (?, ?, ?)");
   $sql-&gt;execute(array(
    $t,
    $u,
    $d
   ));
  }else{
   $sql=$dbh-&gt;prepare("UPDATE `search` SET `description` = ?, `title` = ? WHERE `url`=?");
   $sql-&gt;execute(array(
    $d,
    $t,
    $u
   ));
  }
 }
}
class WSCrawler extends PHPCrawler { 
 function handleDocumentInfo(PHPCrawlerDocumentInfo $p){ 
  $u=$p-&gt;url;
  $c=$p-&gt;http_status_code;
  $s=$p-&gt;source;
  if($c==200 && $s!=""){
   $html = str_get_html($s);
   if(is_object($html)){
    $d="";
    $do=$html-&gt;find("meta[name=description]", 0);
    if($do){
     $d=$do-&gt;content;
    }
    $t=$html-&gt;find("title", 0);
    if($t){
     $t=$t-&gt;innertext;
     addURL($t, $u, $d);
    }
    $html-&gt;clear(); 
    unset($html);
   }
  }
 }
}
function crawl($u){
 $C = new WSCrawler();
 $C-&gt;setURL($u);
 $C-&gt;addContentTypeReceiveRule("#text/html#");
 $C-&gt;addURLFilterRule("#(jpg|gif|png|pdf|jpeg|svg|css|js)$# i");
 $C-&gt;obeyRobotsTxt(true);
 $C-&gt;setUserAgentString("DingoBot (http://search.subinsb.com/about/bot.php)");
 $C-&gt;setFollowMode(0);
 $C-&gt;go();
}
// Get the last indexed URLs (If there isn't, use default URL's) & start Crawling
$last=$dbh-&gt;query("SELECT `url` FROM search");
$count=$last-&gt;rowCount();
if($count &lt; 2){
 crawl("http://subinsb.com"); // The Default URL #1
 crawl("http://demos.subinsb.com"); // The Default URL #2
}else{
 $urls=$last-&gt;fetchAll();
 for($i=0;$i&lt;2;$i++){
  $index=rand(0, $count-1);
  crawl($urls[$index]['url']);
 }
}
?&gt;</code></pre>

The **crawl()** function is the one that initiates the crawling. When **crawl.php** is executed, PHP checks if there are 2 or more rows in the **search** table. If there is, Using MySQL, PHP gets 2 random URL&#8217;s from the table and send them to **crawl()** for crawling. If not, 2 default URL&#8217;s are crawled.

To prevent others from directly going to the URL and initiate the crawl, we only continue the crawl if the variable **$crawlToken** is set and it&#8217;s value is "798821".

When each document is loaded by the crawler, **handleDocumentInfo()** function is called. This function will check if the status code of the document is "200" (OK) and the contents of the document is not null. If the checks are all Ok, then the title tag from the document and the meta description tag is obtained. If title is null or not found, the record is not inserted in to the table. The description field is not mandatory.

The title and description is filtered and the page URL is printed out.

### bgCrawl.php

<pre class="prettyprint"><code>&lt;?
$dir=realpath(dirname(__FILE__));
$GLOBALS['bgFull']="";
$crawlToken=418941;
include($dir."/crawl.php");
?&gt;</code></pre>

This file is called by **runCrawl.php** for background running.

### runCrawl.php

<pre class="prettyprint"><code>&lt;?
$dir=realpath(dirname(__FILE__));
$s="$dir/crawlStatus.txt";
$c=file_get_contents($s);
if($c==0){
 function execInbg($cmd) { 
  if (substr(php_uname(), 0, 7) == "Windows"){ 
   pclose(popen("start /B ". $cmd, "r")); 
  }else{ 
   exec($cmd . " &gt; /dev/null &"); 
  } 
 }
 execInbg("php -q $dir/bgCrawl.php");
 file_put_contents($s, 1);
 echo "Started Running";
}else{
 echo "Currently Running";
}
?&gt;</code></pre>

To start the crawling process in background, you should go to this page by using the browser. The background crawling only starts if the contents of the **crawlStatus.txt** file is "0". When the **bgCrawl.php** is executed using the shell command, the contents of the **crawlStatus.txt** is replaced with "1" indicating that the crawling process in the background has started.

Every time you update your site, you should visit this page to initiate background crawling. Other wise, crawling won&#8217;t be done and the **search** table&#8217;s contents won&#8217;t be increased.

This concludes the 2nd part. The next part will contain additional features and what more you can do with the search engine. The next part will also contain adding **Spell Check** to the search engine.

 [1]: #indexphp
 [2]: #urlphp
 [3]: #searchphp
 [4]: #database-tables
 [5]: #crawler
 [6]: #crawlstatustxt
 [7]: #crawlphp
 [8]: #bgcrawlphp
 [9]: #runcrawlphp
