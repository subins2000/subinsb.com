---
title: Create Facebook Like System With jQuery, MySQL, PDO In PHP
author: Subin Siby
type: post
date: 2013-05-24T08:14:00+00:00
excerpt: "Want to get a Facebook&nbsp;Like system in your site ? You're at the right place. This technic is used in my social network, Friendshood. See the demo there in the social network. But you have to signup.DEMOYou should create a user table with users dat..."
url: /create-facebook-like-system-with-jquery-mysql-pdo-in-php
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - a1e7c37cdfcbdb04597247d557070ad8
  - a1e7c37cdfcbdb04597247d557070ad8
  - a1e7c37cdfcbdb04597247d557070ad8
  - a1e7c37cdfcbdb04597247d557070ad8
  - a1e7c37cdfcbdb04597247d557070ad8
  - a1e7c37cdfcbdb04597247d557070ad8
syndication_permalink:
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
blogger_permalink:
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
  - http://sag-3.blogspot.com/2013/05/create-facebook-like-system-with-jquery.html
dsq_thread_id:
  - 1957624962
  - 1957624962
  - 1957624962
  - 1957624962
  - 1957624962
  - 1957624962
categories:
  - Facebook
  - MySQL
  - PHP
  - phpMyAdmin
  - SQL
tags:
  - Like
  - Unlike

---
<div dir="ltr" style="text-align: left;">
  <p>
    <span style="font-family: inherit;">Facebook have it&#8217;s own like system which pretty good. This post will tell you how to create a "Like system" like that of Facebook. All you have to need is <strong>jQuery</strong>, <strong>MySQL</strong> Database and <strong>PDO</strong> <strong>PHP</strong> extension. This technic is partially used in my social network, <a href="http://open.subinsb.com">Open</a>. See the demo there in the social network. But you have to signup.</span>
  </p>
  
  <div>
    <div class="padlinks">
      <a class="demo" href="http://open.subinsb.com">DEMO</a>
    </div>
  </div>
  
  <div>
    <div>
      <span style="color: red; font-family: inherit;">You should create a user table with users data including <b>user id</b>, <b>username</b>, <b>password</b>, <b>email</b> etc&#8230;</span>
    </div>
    
    <div>
      <span style="color: red; font-family: inherit;">The table should be like this:</span>
    </div>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <span style="font-family: inherit;"><a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-EP5ux4A5LHo/UZ8U5arHNfI/AAAAAAAACnQ/hRNPcrJPA9I/s1600/users.png"><img src="//3.bp.blogspot.com/-EP5ux4A5LHo/UZ8U5arHNfI/AAAAAAAACnQ/hRNPcrJPA9I/s1600/users.png" alt="" border="0" /></a></span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Let&#8217;s start by creating a <b>likes</b> table where we store the like actions by the user.</span>
  </div>
  
  <p>
    <img class="aligncenter" style="line-height: 1.5em;" src="//3.bp.blogspot.com/-jd5tvgcmVlg/UZ8U8a-svNI/AAAAAAAACnc/It5MUN4TyKo/s1600/fdlikes.png" alt="" border="0" />
  </p>
  
  <p>
    Here is the SQL code that create the likes table :
  </p>
  
  <pre class="prettyprint"><code>CREATE TABLE `fdlikes` (
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 `pid` int NOT NULL PRIMARY KEY ,
 `user` varchar(25) NOT NULL UNIQUE
);</code></pre>
  
  <p>
    <span style="font-family: inherit;">The <b>pid</b> field is for the <b>post</b> id. The <b>user</b> field is for the user <b>id</b>.</span>
  </p>
  
  <p>
    Create a <b>post</b> table to insert posts created by users.
  </p>
</div>

<pre class="prettyprint"><code>CREATE TABLE `fdposts` (
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 `user` varchar(25) NOT NULL,
 `message` varchar(200) NOT NULL,
 `likes` int(11) DEFAULT NULL,
 `posted` varchar(20) NOT NULL,
);</code></pre>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-wnAT7G4Rqj4/UZ8U7wYqNnI/AAAAAAAACnY/F8weoNZtPQg/s1600/fdposts.png"><span style="font-family: inherit;"><img src="//1.bp.blogspot.com/-wnAT7G4Rqj4/UZ8U7wYqNnI/AAAAAAAACnY/F8weoNZtPQg/s1600/fdposts.png" alt="" border="0" /></span></a>
</div>

<span style="font-family: inherit;">The <b>id </b>field is an auto increment field which decides the <b>post id</b>. <b>User</b> field is the user&#8217;s id and <b>likes</b> field contains number of users who liked the post. <b>Posted</b> field contains the date and time in which the post was created.</span>
  
<span style="font-family: inherit;">First of all create a <b>config</b> file that will connect to database. We are going to use <b>PDO</b>.</span>


<h1 style="text-align: left;">
  <span id="configphp">config.php</span>
</h1>

This file contains the **$dbh** variable which is the **PDO** object we&#8217;re going to use for executing **SQL** queries.

<pre class="prettyprint"><code>$dbh = new PDO('mysql:dbname=database;host=127.0.0.1', 'username', 'password');</code></pre>

<h1 style="text-align: left;">
  <span id="showing-likeunlike-button"><span style="font-family: inherit;">Showing Like/Unlike Button</span></span>
</h1>

<pre class="prettyprint"><code>&lt;?php
require_once "config.php";
$pid='1'; //Post id - Post is "Hi everybody"
$uid='1'; //User id - User is "Subin Siby"
$sql=$dbh-&gt;prepare("SELECT * FROM fdlikes WHERE pid=? and user=?");
$sql-&gt;execute(array($pid, $uid));
if($sql-&gt;rowCount()==1){
 echo '&lt;a href="#" class="like" id="'.$pid.'" title="Unlike"&gt;Unlike&lt;/a&gt;';
}else{ 
 echo '&lt;a href="#" class="like" id="'.$pid.'" title="Like"&gt;Like&lt;/a&gt;';
}
?&gt;</code></pre>

Place the above code anywhere you like.

<div>
  <h1 style="text-align: left;">
    <span id="jquery-code-to-post-likeunlike-action">jQuery code to post like/unlike action</span>
  </h1>
  
  <div>
    <pre class="prettyprint"><code>&lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">$(document).ready(function(){
 $(document).on('click', '.like', function(){&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">if($(this).attr('title') == 'Like'){&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;"> $that = $(this);
   $.post('action.php', {pid:$(this).attr('id'), action:'like'},function(){&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">  $that.text('Unlike');&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">  $that.attr('title','Unlike');&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;"> });&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">}else{&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;"> if($(this).attr('title') == 'Unlike'){&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">  $that = $(this);
    $.post('action.php', {pid:$(this).attr('id'), action:'unlike'},function(){&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">   $that.text('Like');&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">   $that.attr('title','Like');&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">  });&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;"> }&lt;/span>&lt;/span>
  &lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;">}&lt;/span>&lt;/span>
&lt;span style="line-height: 16px;">&lt;span style="font-family: inherit;"> });
});&lt;/span>&lt;/span></code></pre>
    
    <p>
      The above code can be placed inside <strong>script</strong> element or as an external file.
    </p>
  </div>
  
  <h1 style="text-align: left;">
    <span id="php-code-to-likeunlike-to-which-jquery-sends-request">PHP code to like/unlike to which jQuery sends request</span>
  </h1>
  
  <p>
    <span style="font-family: inherit;"><span style="line-height: 16px;">This file is <b>action.php</b></span></span>
  </p>
  
  <pre class="prettyprint"><code>&lt;?php
require_once "config.php";
$pid=$_POST['pid'];
$user=$_COOKIE['user'];
$action=$_POST['action'];
if ($action=='like'){
 $sql=$dbh-&gt;prepare("SELECT * FROM fdlikes WHERE pid=? and user=?");
 $sql-&gt;execute(array($pid,$user));
 $matches=$sql-&gt;rowCount();
 if($matches==0){
 $sql=$dbh-&gt;prepare("INSERT INTO fdlikes (pid, user) VALUES(?, ?)");
 $sql-&gt;execute(array($pid,$user));
 $sql=$dbh-&gt;prepare("UPDATE fdposts SET likes=likes+1 WHERE id=?");
 $sql-&gt;execute(array($pid));
 }else{
 die("There is No Post With That ID");
 }
}
if ($action=='unlike'){
 $sql = $dbh-&gt;prepare("SELECT 1 FROM `fdlikes` WHERE pid=? and user=?");
 $sql-&gt;execute(array($pid,$user));
 $matches = $sql-&gt;rowCount();
 if ($matches != 0){
 $sql=$dbh-&gt;prepare("DELETE FROM fdlikes WHERE pid=? AND user=?");
 $sql-&gt;execute(array($pid,$user));
 $sql=$dbh-&gt;prepare("UPDATE fdposts SET likes=likes-1 WHERE id=?");
 $sql-&gt;execute(array($pid));
 }
}
?&gt;</code></pre>
  
  <p>
    <span style="font-family: inherit;">That&#8217;s it. Try it out. If there&#8217;s any bugs, problems or questions feel free to comment below on Disqus.</span>
  </p>
</div>
