---
title: How to increase your apt cache limit
author: Subin Siby
type: post
date: 2012-11-13T06:12:00+00:00
excerpt: '<div dir="ltr" trbidi="on"><span>Increase value APT::Cache-Limit</span><br><div><span>When the time I try to install backtrack unfortunately i got message error in the <b>terminal</b> :</span></div><div><span><b><span>Reading package lists&hellip; Error!<br>E: Dynamic MMap ran out of room. Please increase the size of APT::Cache-Limit. Current value: 25165824. (man 5 apt<a href="http://aziest.wordpress.com/2011/01/24/how-to-increase-your-apt-cache-limit/www.aziest.wordpress.com">.</a>conf)<br>E: Error occurred while processing riece (NewVersion1)<br>E: Problem with MergeList /var/lib/apt/lists/archive.offensive-security.com_dists_pwnsauce_universe_binary-i386_Packages<br>W: Unable to munmap<br>E: The package lists or status file could not be parsed or opened.</span></b><br><span></span></span></div><div><span>and solution to fix it is just increase the value <b>APT::Cache-Limit</b> at the <b>/etc/apt/apt.conf.d/70debconf</b></span></div><blockquote><span><code>sudo gedit /etc/apt/apt.conf.d/70debconf</code></span></blockquote><span>Then put this code</span><br><blockquote><span><code>APT::Cache-Limit "100000000";</code></span></blockquote><span>at the below on that file and then save it.</span><br><div><span>You won&rsquo;t find <b>apt cache limit</b> again in the next time.</span></div></div>'
url: /how-to-increase-your-apt-cache-limit
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
  - http://sag-3.blogspot.com/2012/11/increase-apt-cache-limit.html
syndication_item_hash:
  - 43d1f04beb362291b5cc877d2196c95b
  - 43d1f04beb362291b5cc877d2196c95b
  - 43d1f04beb362291b5cc877d2196c95b
  - 43d1f04beb362291b5cc877d2196c95b
  - 43d1f04beb362291b5cc877d2196c95b
  - 43d1f04beb362291b5cc877d2196c95b
categories:
  - Linux
  - Ubuntu
tags:
  - APT
  - Cache
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="background-color: white; font-family: inherit; line-height: 19.200000762939453px;">Increase value APT::Cache-Limit</span></p> 
  
  <div style="line-height: 19.200000762939453px; padding: 0px 0px 18px;">
    <span style="background-color: white; font-family: inherit;">When the time I try to install backtrack unfortunately i got message error in the <b>terminal</b> :</span>
  </div>
  
  <div style="line-height: 19.200000762939453px; padding: 0px 0px 18px;">
    <span style="background-color: white; font-family: inherit;"><b><span style="color: red;">Reading package lists… Error!<br />E: Dynamic MMap ran out of room. Please increase the size of APT::Cache-Limit. Current value: 25165824. (man 5 apt<a href="http://aziest.wordpress.com/2011/01/24/how-to-increase-your-apt-cache-limit/www.aziest.wordpress.com" style="text-decoration: none;">.</a>conf)<br />E: Error occurred while processing riece (NewVersion1)<br />E: Problem with MergeList /var/lib/apt/lists/archive.offensive-security.com_dists_pwnsauce_universe_binary-i386_Packages<br />W: Unable to munmap<br />E: The package lists or status file could not be parsed or opened.</span></b><br /><span id="more-112"></span></span>
  </div>
  
  <div style="line-height: 19.200000762939453px; padding: 0px 0px 18px;">
    <span style="background-color: white; font-family: inherit;">and solution to fix it is just increase the value <b>APT::Cache-Limit</b> at the <b>/etc/apt/apt.conf.d/70debconf</b></span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><code style="background-color: white; padding: 0px 2px;">sudo gedit /etc/apt/apt.conf.d/70debconf</code></span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; font-family: inherit;">Then put this code</span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><code style="background-color: white; padding: 0px 2px;">APT::Cache-Limit "100000000";</code></span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; font-family: inherit;">at the below on that file and then save it.</span>
  </p>
  
  <div style="line-height: 19.200000762939453px; padding: 0px 0px 18px;">
    <span style="background-color: white; font-family: inherit;">You won’t find <b>apt cache limit</b> again in the next time.</span>
  </div>
</div>