---
title: Convert Bytes To KB, MB, GB in PHP
author: Subin Siby
type: post
date: 2016-03-23T09:09:02+00:00
url: /convert-bytes-kb-mb-gb-php
categories:
  - PHP
  - Program
  - Short Post
tags:
  - File
  - Function
  - Memory

---
Say you were displaying the size of a file in PHP. You obviously get the file size in Bytes by using **filesize()**.

You won&#8217;t have any idea what the file size is if you read it in Bytes. Bytes is useful for file transmission in a network, but not for human usage. So, it&#8217;s better to convert it to human readable form.<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/03/php-file-size-converter.png" alt="The circled areas show the converted size from Bytes. App - Lobby Downloader" width="738" height="207" />][1]<figcaption class="wp-caption-text">The circled areas show the converted size from Bytes. App &#8211; Lobby Downloader</figcaption></figure> 

Here is a simple function to convert Bytes to **KB**, **MB**, **GB**, **TB** :

<pre class="prettyprint"><code>function convertToReadableSize($size){
  $base = log($size) / log(1024);
  $suffix = array("", "KB", "MB", "GB", "TB");
  $f_base = floor($base);
  return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
}</code></pre>

Note that KB is for Kibibyte. Normal systems uses Kilobyte (1000). If you want to change it, then replace the parameter passed to **log()**.

Note that upto **TB** is included in the function. You can extend it by adding more into the **$suffix **array.

## Usage

Just call the function :

<pre class="prettyprint"><code>echo convertToReadableSize(1024); // Outputs '1KB'
echo convertToReadableSize(1024 * 1024); // Outputs '1MB'
echo convertToReadableSize(filesize("/home/simsu/good.txt"));
</code></pre>

I had to use it for a download manager app I created for [Lobby][2]. You can see the source code of <a href="https://github.com/subins2000/lobby-downloader" target="_blank">"Downloader" app here</a>.

 [1]: //lab.subinsb.com/projects/blog/uploads/2016/03/php-file-size-converter.png
 [2]: http://lobby.subinsb.com