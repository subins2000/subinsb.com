---
title: Create a free .tk domain website with free hosting by AppFog
author: Subin Siby
type: post
date: 2013-02-24T05:57:00+00:00
excerpt: 'DEMOThis free .tk&nbsp;domain is made available by a company called Dot&nbsp;TK&nbsp;and the free hosting is provided by AppFog.Create free hosting for siteGo to AppFog&nbsp;Signup pageFill up the form and click Signup.Go to Create an appChoose an appl...'
url: /create-a-free-tk-domain-website-with-free-hosting-by-appfog
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
  - http://sag-3.blogspot.com/2013/02/create-free-tk-domain-website-with-free.html
syndication_item_hash:
  - f07642c1a3c968ebac6c5c20cb05076c
  - f07642c1a3c968ebac6c5c20cb05076c
  - f07642c1a3c968ebac6c5c20cb05076c
  - f07642c1a3c968ebac6c5c20cb05076c
  - f07642c1a3c968ebac6c5c20cb05076c
  - f07642c1a3c968ebac6c5c20cb05076c
categories:
  - Uncategorized
tags:
  - AppFog
  - Free
  - My.Dot.Tk
  - WEB
  - Website

---
<div class="padlinks">
  <a class="demo" href="http://msurl.tk/">DEMO</a>
</div>

This free **.tk** domain is made available by a company called **Dot TK** and the free hosting is provided by **AppFog**.

<div style="text-align: center;">
  <div class="separator" style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-oNdKese_CrQ/Ub1UK4o6PiI/AAAAAAAACtU/M0GTKNzJaBs/s1600/0006.png"><img src="//3.bp.blogspot.com/-oNdKese_CrQ/Ub1UK4o6PiI/AAAAAAAACtU/M0GTKNzJaBs/s1600/0006.png" alt="" border="0" /></a>
  </div>
  
  <h1 class="separator" style="clear: both; text-align: left;">
    Create free hosting for site
  </h1>
</div>

<div>
  Go to <b><a href="http://adf.ly/JiFq8">AppFog Signup page</a></b>
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-l3TncKL2Qc0/USmgS8bMIAI/AAAAAAAACbc/tcyx5l9bxDw/s1600/appfogform.png"><img src="//1.bp.blogspot.com/-l3TncKL2Qc0/USmgS8bMIAI/AAAAAAAACbc/tcyx5l9bxDw/s1600/appfogform.png" alt="" border="0" /></a>
</div>

<div>
  Fill up the form and click <b>Signup</b>.
</div>

<div>
  Go to <a href="https://console.appfog.com/apps/new">Create an app</a>
</div>

<div>
</div>

<div>
  Choose an application:
</div>

<div>
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-VpN_T3s6dMg/USmhlf2NN1I/AAAAAAAACbk/aovb3Soc3CU/s1600/chooseapp.png"><img src="//1.bp.blogspot.com/-VpN_T3s6dMg/USmhlf2NN1I/AAAAAAAACbk/aovb3Soc3CU/s640/chooseapp.png" alt="" width="640" height="395" border="0" /></a>
</div>

<div>
</div>

<div>
  Choose a infrastructure and a subdomain (I use <b>HP</b>):
</div>

<div>
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-J0m61xZ0AHg/USmiRs9yCCI/AAAAAAAACbs/DHaoTrxQ3xY/s1600/infra.png"><img src="//3.bp.blogspot.com/-J0m61xZ0AHg/USmiRs9yCCI/AAAAAAAACbs/DHaoTrxQ3xY/s640/infra.png" alt="" width="640" height="392" border="0" /></a>
</div>

<div>
</div>

<div>
  After filling up form, click on <b>Create App</b>.
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-WAeqLdze3BQ/USmi1XPjskI/AAAAAAAACb0/pX2wufKD5Rs/s1600/buliding-appfog.png"><img src="//1.bp.blogspot.com/-WAeqLdze3BQ/USmi1XPjskI/AAAAAAAACb0/pX2wufKD5Rs/s640/buliding-appfog.png" alt="" width="640" height="420" border="0" /></a>
</div>

<div>
</div>

<div>
  You will be automatically redirected to the app panel.
</div>

<div>
  That&#8217;s it. You created your <b>AppFog</b> app.
</div>

<div>
  To learn how to upload files to your app. Here are some pages that will help you understand about <b>AppFog</b>.
</div>

<div>
  <ul style="text-align: left;">
    <li>
      <b><a href="http://adf.ly/JiHkv">Installing AF CLI Tool</a></b>
    </li>
    <li>
      <b><a href="http://adf.ly/JiHsX">Uploading files to app in Windows.</a></b>
    </li>
    <li>
      <b><a href="http://adf.ly/JiHzq">Uploading files to app in Mac OSX & Linux.</a></b>
    </li>
    <li>
      <b><a href="http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html">Uploading files to app in Ubuntu (alternate way).</a></b>
    </li>
  </ul>
</div>

<div>
  The created app domain will be <b>.hp.af.cm</b>. You can link this app to <b>Dot TK</b>. See How to do that below.
</div>

<h1 style="text-align: left;">
  Create Free Website
</h1>

<div>
  Go to <b><a href="http://adf.ly/JiEP2">Dot TK</a></b> website.
</div>

<div>
  Login with a service provider (Google, Facebook etc&#8230;)
</div>

<div>
  Go to <a href="http://adf.ly/JiFCZ">Add Domain</a>
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-PzyisBlI-eA/USmfZrSttBI/AAAAAAAACbU/iylPpAFmKEE/s1600/domainadd.png"><img src="//3.bp.blogspot.com/-PzyisBlI-eA/USmfZrSttBI/AAAAAAAACbU/iylPpAFmKEE/s640/domainadd.png" alt="" width="640" height="276" border="0" /></a>
</div>

<div>
  Fill up the form and click <b>NEXT</b>.
</div>

<div>
  A new page will be visible and in it click the <span style="background-color: white; font-family: Arial, sans-serif; font-size: 15px;"><b>Use DNS for this domain</b></span><span style="font-family: Arial, sans-serif; font-size: 15px;"> button.</span>
</div>

<div>
  <span style="font-family: Arial, sans-serif; font-size: 15px;">Go to <a href="http://adf.ly/JiIxA"><b>AppFog</b> panel</a> in another tab. Click your app and from the app panel click <b>Domain Names.</b></span>
</div>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;"><b>Type </b>the url of site you made in <b>Dot TK </b>in the input box as shown in the picture:</span></span>
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-tAgDHW3_Bcs/USmnu-bZkwI/AAAAAAAACb8/YmvIuHndTN0/s1600/adddomain-dot-tk-appfog.png"><img src="//3.bp.blogspot.com/-tAgDHW3_Bcs/USmnu-bZkwI/AAAAAAAACb8/YmvIuHndTN0/s640/adddomain-dot-tk-appfog.png" alt="" width="640" height="288" border="0" /></a>
</div>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;"> </span></span>
</div>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;">Then Click on <b>Update</b> button.</span></span>
</div>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;"> </span></span>
</div>

<h3 style="text-align: left;">
  <span style="font-family: Arial, sans-serif;"><span style="text-decoration: underline;">Getting the <b>IP</b> address of <b>AppFog</b> app<span style="font-size: 15px;">.</span></span></span>
</h3>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;">Open a new tab and go to <a href="http://www.getip.com/">http://www.getip.com/</a></span></span>
</div>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;">Type the <b>URL</b> of the <b>AppFog </b>app in to the <b>selfseo</b> input box and click <b>Get IP</b>.</span></span>
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-TxyNyLwl0WU/USmpir1VcXI/AAAAAAAACcE/HZFc1_ZN3sQ/s1600/ip-find.png"><img src="//1.bp.blogspot.com/-TxyNyLwl0WU/USmpir1VcXI/AAAAAAAACcE/HZFc1_ZN3sQ/s640/ip-find.png" alt="" width="640" height="248" border="0" /></a>
</div>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;"> </span></span>
</div>

<div>
  <span style="font-family: Arial, sans-serif;"><span style="font-size: 15px;">The page will show the <b>IP address</b> of the site :</span></span>
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-P4uIQeDfQzY/USmpqG4P3QI/AAAAAAAACcM/0obcAYV0uOs/s1600/ip-found.png"><img src="//3.bp.blogspot.com/-P4uIQeDfQzY/USmpqG4P3QI/AAAAAAAACcM/0obcAYV0uOs/s1600/ip-found.png" alt="" border="0" /></a>
</div>

<div class="separator" style="clear: both; text-align: left;">
  Go to the <b>Dot TK</b> tab we already opened. Type the <b>IP </b>address we just found on to the two <b>IP address </b>input box :
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-ldKiljbSxcA/USmqZgVjDTI/AAAAAAAACcU/nrmOz92hYHc/s1600/fillup-form-tk.png"><img src="//3.bp.blogspot.com/-ldKiljbSxcA/USmqZgVjDTI/AAAAAAAACcU/nrmOz92hYHc/s640/fillup-form-tk.png" alt="" width="640" height="462" border="0" /></a>
</div>

<div class="separator" style="clear: both; text-align: left;">
  Change the <b>Registration </b>length to <b>12 months</b>.
</div>

<div class="separator" style="clear: both; text-align: left;">
  Type the verification code in the picture and click on the <b>Next</b> button.
</div>

<div class="separator" style="clear: both; text-align: left;">
</div>

<div class="separator" style="clear: both; text-align: left;">
  Visit the site you created on <b>Dot TK </b>to verify that your site is created<b>.</b>
</div>

<div class="separator" style="clear: both; text-align: left;">
  <b>If you have any doubts or problems, feel free to comment the problem/doubt.</b>
</div>