---
title: How To Set Same Cookie On Different Domains
author: Subin Siby
type: post
date: 2014-06-20T16:59:54+00:00
url: /set-same-cookie-on-different-domains
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - JavaScript
  - PHP
tags:
  - Cookies
  - Hack
  - Trick
  - Tutorials

---
You might have seen sites like Google setting the login status cookie on the various domains of theirs (YouTube, Blogger). As you may know, cookie can&#8217;t be set in a different domain from another domain directly.

If you&#8217;re having multiple sites in where you need to set a cookie from a parent site, you can use basic **HTML **and **JS** to set the cookies. Google is using this same way.


## Domains

For this tutorial, we will refer to three domains :

<pre class="prettyprint"><code>www.example.com
www.mysite.com
www.india.com</code></pre>

We will set cookies on **mysite.com** and **india.com** from **example.com**.

## Other Domains

You should make a dynamic page named "setCookie.php" on your server where you&#8217;re going to create the cookie. If it&#8217;s **PHP**, then add the following code to set the cookie :

<pre class="prettyprint"><code>&lt;?
setcookie("MyCookie", "subinsb.com", time()+3600);
?&gt;</code></pre>

In the above case we&#8217;re not mentioning the path or domain because PHP automatically sets it.

## Main Domain

On the main domain (example.com) where you&#8217;re going to ask the other two domains to set the cookie, create an HTML page with the following content :

<pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
 &lt;head&gt;
  &lt;script&gt;
   function loadComplete(){
    alert("Completed Setting Cookies");
   }
  &lt;/script&gt;
 &lt;/head&gt;
 &lt;body onload="loadComplete()"&gt;
  &lt;p&gt;
  Please Wait...
  &lt;/p&gt;
  &lt;img src="http://www.mysite.com/setCookie.php" style="display:none;" /&gt;
  &lt;img src="http://www.india.com/setCookie.php" style="display:none;" /&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

When the client visits the above page, a page is requested from the **mysite.com** domain as image source, but the page is not an image. This page that is on the other domain will set the cookie on that domain.

We also add an event listener on the document, so that we will know when the cookies are set completely. This is equal to document loaded listener because when the images are loaded, cookies are set.

## Other Things

You can also send data to the other domains as GET parameters so that cookies based on that data can be created. But, when you send passwords or other secure content, be sure to encrypt the string.

The images should be hidden because, since it&#8217;s not valid images, the ugly image icon will appear in the browser.

You can change the content of the event listener callback **loadComplete()** to do something else according to your choice, like redirecting back to the main domain.

## How Google Do It

If you have account on Blogger & YouTube which are on external domains, when you log in via **accounts.google.com** you are redirected to a page that says "Please Wait&#8230;", right ? If you look at the source code of the page, you can see the **<img>** tags of **youtube.com** and **blogger.com** domain. Yes, Google is using the same way to set login information cookie on YouTube and Blogger.

Microsoft does in this same way to set cookies on their services like **Hotmail**, **live.com**, **msn.com** etc.. So many domains, right ?

 [1]: #domains
 [2]: #other-domains
 [3]: #main-domain
 [4]: #other-things
 [5]: #how-google-do-it
