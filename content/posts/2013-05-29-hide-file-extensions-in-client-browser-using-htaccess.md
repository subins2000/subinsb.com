---
title: Hide File Extensions In Browsers Using .htaccess
author: Subin Siby
type: post
date: 2013-05-29T04:12:00+00:00
excerpt: 'Many of us programmers want to hide the file extension of server files such as .php, .jsp, .py&nbsp;etc....You can easily hide these extensions with the help of a .htaccess&nbsp;file. Follow the steps :Create a file named .htaccess&nbsp;if your site ro...'
url: /hide-file-extensions-in-client-browser-using-htaccess
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - ceab532e81668a23c1df96bc6c1a1fab
  - ceab532e81668a23c1df96bc6c1a1fab
  - ceab532e81668a23c1df96bc6c1a1fab
  - ceab532e81668a23c1df96bc6c1a1fab
  - ceab532e81668a23c1df96bc6c1a1fab
  - ceab532e81668a23c1df96bc6c1a1fab
syndication_permalink:
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
  - http://sag-3.blogspot.com/2013/05/hide-file-extensions-in-client-browser.html
categories:
  - HTML
  - PHP
tags:
  - .htaccess
  - ASP
  - JS
  - JSP
  - Rewrite

---
<span style="background-color: white; font-family: inherit;">Many of us programmers want to hide the file extension of server files such as <b>.php</b>, <b>.jsp</b>, <b>.py</b> etc&#8230;.</span>

<div>
  <span style="background-color: white; font-family: inherit;">You can easily hide these extensions with the help of a <b>.htaccess</b> file. This feature is only available to Apache servers.</span>
</div>

<div>
  <span style="background-color: white; font-family: inherit;">Create a file named <b>.htaccess</b> if your root directory of site doesn&#8217;t have one.</span>
</div>

<div>
  <span style="background-color: white; font-family: inherit;">Add these lines to the <b>.htaccess</b> file :</span>
</div>

<div>
  <pre class="prettyprint"><code>&lt;span style="background-color: white; font-family: inherit;">&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteEngine&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> &lt;/span>&lt;span class="kw2" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">on&lt;/span>&lt;/span>
&lt;span style="background-color: white; font-family: inherit;">&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME} !-d&lt;/span>&lt;/span>
&lt;span style="background-color: white; font-family: inherit;">&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME}.php -f&lt;/span>&lt;/span>
&lt;span style="background-color: white; font-family: inherit;">&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteRule&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> .* $0.php&lt;/span>&lt;/span></code></pre>
</div>

<div>
  The above lines wll hide the <b>PHP</b> extension. If you want to hide the <b>HTML</b> and other extension <span style="line-height: 18.200000762939453px; white-space: nowrap;">too add these line after </span>
</div>

<div>
  <pre class="prettyprint"><code>&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME}.php -f&lt;/span></code></pre>
</div>

<div>
  <span style="line-height: 18.200000762939453px; white-space: nowrap;">as you want. You can also replace it if you don&#8217;t want php extensions to hide.</span>
</div>


## HTML

<div>
  <pre class="prettyprint"><code>&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME}.html -f&lt;/span></code></pre>
</div>

## JSP

<div>
  <pre class="prettyprint"><code>&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME}.jsp -f&lt;/span></code></pre>
</div>

## ASP

<div>
  <pre class="prettyprint"><code>&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME}.asp -f&lt;/span></code></pre>
</div>

## PY

<div>
  <pre class="prettyprint"><code>&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME}.py -f&lt;/span></code></pre>
</div>

## JS

<div>
  <pre class="prettyprint"><code>&lt;span class="kw1" style="border: 0px; color: #2060a0; line-height: 18.200000762939453px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; white-space: nowrap;">RewriteCond&lt;/span>&lt;span style="line-height: 18.200000762939453px; white-space: nowrap;"> %{REQUEST_FILENAME}.js -f&lt;/span></code></pre>
</div>

<div>
  <span style="background-color: white; font-family: inherit;">You really should go into each page and delete the extensions, but I believe you can redirect them by adding another rule :</span>
</div>

<div>
  <pre class="prettyprint"><code>&lt;span style="color: #2060a0;">RewriteRule &lt;/span>(.*).&lt;span style="color: red;">html&lt;/span> $1 [R=301,NC]</code></pre>
</div>

<div>
  <span style="background-color: white; line-height: 18.200000762939453px; white-space: nowrap;"><span style="font-family: inherit;">The above rule will redirect all <b>.html</b> pages to non <b>.html</b> extension page.</span></span>
</div>

<div>
  <span style="background-color: white; line-height: 18.200000762939453px; white-space: nowrap;"><span style="font-family: inherit;">Replace the <span style="color: red;">red </span>highlighted text with any extension you want to redirect.</span></span>
</div>

 [1]: #jsp
 [2]: #asp
 [3]: #py
 [4]: #js
