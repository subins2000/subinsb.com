---
title: Microsoft’s New Logo
author: Subin Siby
type: post
date: 2012-08-25T14:17:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on">Microsoft got a new Logo after 25 years. Here is the new logo.<br><br><div><a href="//4.bp.blogspot.com/-UvYcV70J5YI/UDjdDjpGrKI/AAAAAAAABgQ/Ea5LC6IbcAk/s1600/mslogo_new.jpg" imageanchor="1"><img border="0" src="//4.bp.blogspot.com/-UvYcV70J5YI/UDjdDjpGrKI/AAAAAAAABgQ/Ea5LC6IbcAk/s1600/mslogo_new.jpg"></a></div><div><span>This&nbsp;</span><b>Microsoft New logo&nbsp;</b><span>contains the four sqaure and the name of the company. Logo designer Sagi Haviv for the Library of Congress and Armani Exchange, thinks that the logo simply isn&rsquo;t distinctive enough. By opting for a simple array of four colored squares, Haviv says Microsoft missed a big opportunity.</span><span>It kinda looks like Google's Logo.</span></div><div><span>Here's a image I found in <b>Google +.</b></span></div><div><a href="//3.bp.blogspot.com/-y0cM4FC2Uds/UDjdvGw99LI/AAAAAAAABgY/CCdW98X2Uxg/s1600/f_00cd0b" imageanchor="1"><img border="0" src="//3.bp.blogspot.com/-y0cM4FC2Uds/UDjdvGw99LI/AAAAAAAABgY/CCdW98X2Uxg/s1600/f_00cd0b"></a></div><div>The image is right. It looks like the Nazi Symbol. But it's a good logo.</div><div>Hoping Windows 8 would be good.</div></div>
url: /microsofts-new-logo
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - 6e64e77230afd24d8902c0a350ca12f7
  - 6e64e77230afd24d8902c0a350ca12f7
  - 6e64e77230afd24d8902c0a350ca12f7
  - 6e64e77230afd24d8902c0a350ca12f7
  - 6e64e77230afd24d8902c0a350ca12f7
  - 6e64e77230afd24d8902c0a350ca12f7
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
  - http://sag-3.blogspot.com/2012/08/microsofts-new-logo_25.html
categories:
  - Microsoft
  - Windows
tags:
  - Google

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Microsoft got a new Logo after 25 years. Here is the new logo.</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-UvYcV70J5YI/UDjdDjpGrKI/AAAAAAAABgQ/Ea5LC6IbcAk/s1600/mslogo_new.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-UvYcV70J5YI/UDjdDjpGrKI/AAAAAAAABgQ/Ea5LC6IbcAk/s1600/mslogo_new.jpg" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="background-color: white; color: #222222; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 21px;">This&nbsp;</span><b style="background-color: white; color: #222222; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 21px;">Microsoft New logo&nbsp;</b><span style="background-color: white; color: #222222; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 21px;">contains the four sqaure and the name of the company. Logo designer Sagi Haviv for the Library of Congress and Armani Exchange, thinks that the logo simply isn’t distinctive enough. By opting for a simple array of four colored squares, Haviv says Microsoft missed a big opportunity.</span><span style="background-color: white; color: #222222; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 21px;">It kinda looks like Google&#8217;s Logo.</span>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <span style="background-color: white; color: #222222; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 21px;">Here&#8217;s a image I found in <b>Google +.</b></span>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-y0cM4FC2Uds/UDjdvGw99LI/AAAAAAAABgY/CCdW98X2Uxg/s1600/f_00cd0b" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-y0cM4FC2Uds/UDjdvGw99LI/AAAAAAAABgY/CCdW98X2Uxg/s1600/f_00cd0b" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    The image is right. It looks like the Nazi Symbol. But it&#8217;s a good logo.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Hoping Windows 8 would be good.
  </div>
</div>