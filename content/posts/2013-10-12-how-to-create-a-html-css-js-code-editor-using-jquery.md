---
title: How To Create A HTML, CSS, JS Code Editor Using jQuery
author: Subin Siby
type: post
date: 2013-10-12T07:56:00+00:00
excerpt: 'There are a lot of HTML&nbsp;editing services in the Web. Some of the popular services are JsFiddle&nbsp;and&nbsp;JsBin. These sites offer saving function to your account too. If you want to create a site like JSFiddle, then you are at the right place....'
url: /how-to-create-a-html-css-js-code-editor-using-jquery
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-html-css-js-editor-using-jquery.html
dsq_thread_id:
  - 1958141729
  - 1958141729
  - 1958141729
  - 1958141729
  - 1958141729
  - 1958141729
syndication_item_hash:
  - 5ccecaa675506614f2f80ba4e783b119
  - 5ccecaa675506614f2f80ba4e783b119
  - 5ccecaa675506614f2f80ba4e783b119
  - 5ccecaa675506614f2f80ba4e783b119
  - 5ccecaa675506614f2f80ba4e783b119
  - 5ccecaa675506614f2f80ba4e783b119
categories:
  - HTML
  - JavaScript
  - jQuery
tags:
  - Demo
  - Download
  - iframe

---
There are a lot of **HTML** editing services in the **Web**. Some of the popular services are **JsFiddle** and **JsBin**. These sites offer saving function to your account too. If you want to create a site like **JSFiddle**, then you are at the right place. I&#8217;m going to tell you how to create a basic **HTML** editing software using **jQuery**.

The editor we&#8217;re going to create also has a live preview service which means that the user don&#8217;t have to submit the code form every time. The preview is automatically updated using **jQuery**&#8216;s **keyup** trigger except for **JS **code update, because **Javascript** can&#8217;t be added every time when user types something because there will be **syntax** errors.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=txjxy7h69mhznsz&class=7">Download</a> <a class="demo" href="http://demos.subinsb.com/html-css-js-editor">Demo</a>
</div>

First, we create the text editor layout which have a table containing **HTML** codes, **CSS** codes, **JS** codes and **textareas**. Here is the full **HTML** code of layout (We&#8217;re also adding the jQuery library) :

<pre class="prettyprint"><code>&lt;script src="http://code.jquery.com/jquery-latest.min.js"&gt;&lt;/script&gt;
&lt;script src="htmleditor.js"&gt;&lt;/script&gt;
&lt;table&gt;
 &lt;tbody&gt;
  &lt;tr&gt;
   &lt;td&gt;
    &lt;h2&gt;HTML&lt;/h2&gt;
    &lt;textarea class="codes" id="html" placeholder="Your HTML Code HERE"&gt;&lt;/textarea&gt;
   &lt;/td&gt;
   &lt;td&gt;
    &lt;h2&gt;CSS&lt;/h2&gt;
    &lt;textarea class="codes" id="css" placeholder="Your CSS Code HERE"&gt;&lt;/textarea&gt;
   &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td&gt;
    &lt;h2&gt;JS&lt;/h2&gt;
    &lt;textarea class="codes" id="js" placeholder="Your JavaScript Code HERE"&gt;&lt;/textarea&gt;
   &lt;/td&gt;
   &lt;td&gt;
    &lt;h2&gt;Preview&lt;/h2&gt;
    &lt;iframe id="preview" src="javascript:;"&gt;&lt;/iframe&gt;
   &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td&gt;
    &lt;div style="text-align:right;"&gt;&lt;input id="upjs" type="button" value="Update And Run JS"/&gt;&lt;/div&gt;
   &lt;/td&gt;
  &lt;/tr&gt;
 &lt;/tbody&gt;
&lt;/table&gt;
&lt;style&gt;
.codes,#preview{
 width: 320px;
 height: 135px;
 border:2px dashed white;
 background:white;
 color:black;
 overflow:auto;
}
&lt;/style&gt;</code></pre>

<h2 style="text-align: left;">
  <b><span style="font-family: inherit; font-size: large;">htmleditor.js</span></b>
</h2>

<pre class="prettyprint"><code>$(document).ready(function(){
 var p=$("#preview").contents().find("body");
 p.css("margin","2px");
 p.html('&lt;span id="subinsbdotcomhtmlpr"&gt;&lt;/span&gt;&lt;style id="subinsbdotcomcsspr"&gt;&lt;/style&gt;');
 $("#html").on('keyup',function(){
  p.find("#subinsbdotcomhtmlpr").html($(this).val());
 });
 $("#css").on('keyup',function(){
  p.find("#subinsbdotcomcsspr").html($(this).val());
 });
 $("#js").on('change',function(){
  p.find("#subinsbdotcomjspr").remove();
  p.append('&lt;script id="subinsbdotcomjspr"&gt;'+$(this).val()+'&lt;/script&gt;');
 });
 $("#upjs").on('click',function(){
  p.find("#subinsbdotcomjspr").remove();
  p.append('&lt;script id="subinsbdotcomjspr"&gt;'+$("#js").val()+'&lt;/script&gt;');
 });
});</code></pre>

The **jQuery** code is too less. The **jQuery** code is only binding some **keyup** functions and appending **HTML** code to the **preview** **iframe**. If you have any problems / feedback / suggestions write it on the comments. I&#8217;m here 10/7.