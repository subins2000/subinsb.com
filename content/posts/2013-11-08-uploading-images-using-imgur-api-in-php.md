---
title: Uploading Images Using Imgur API in PHP
author: Subin Siby
type: post
date: 2013-11-08T16:17:00+00:00
excerpt: "This tutorial was a request from Anandu.Imgur&nbsp;provides developers to upload images anonymously using their app. For this you only need to signup&nbsp;for Imgur&nbsp;and get an&nbsp;Application Client ID. In this post I'm going to tell you the step..."
url: /uploading-images-using-imgur-api-in-php
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 925f20ef29aec362e66a11cb2224a123
  - 925f20ef29aec362e66a11cb2224a123
  - 925f20ef29aec362e66a11cb2224a123
  - 925f20ef29aec362e66a11cb2224a123
  - 925f20ef29aec362e66a11cb2224a123
  - 925f20ef29aec362e66a11cb2224a123
syndication_permalink:
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
  - http://sag-3.blogspot.com/2013/11/upload-images-with-imgur-api-in-php.html
dsq_thread_id:
  - 1948264505
  - 1948264505
  - 1948264505
  - 1948264505
  - 1948264505
  - 1948264505
categories:
  - HTML
  - PHP
tags:
  - API
  - Demo
  - Download
  - Form
  - Image
  - Imgur
  - POST

---
<div dir="ltr" style="text-align: left;">
  <span style="font-family: inherit;">This tutorial was a request from <a href="http://disqus.com/anandubajith/">Anandu</a>.</span><br /> <span style="font-family: inherit;"><b>Imgur</b> provides developers to upload images anonymously using their app. For this you only need to <b>signup</b> for <b>Imgur</b> and get an <b>Application Client ID</b>. In this post I&#8217;m going to tell you the step by step process of uploading images using <b>Imgur API</b> in <b>PHP</b>. This is pretty easy. You only have to send a <b>POST</b> request to a file on their site.</span></p> 
  
  <div class="padlinks">
    <span style="font-family: inherit;"><a class="download" href="http://demos.subinsb.com/down.php?id=r8zy7w2bdzegmo5&class=13">Download</a> <a class="demo" href="http://demos.subinsb.com/imgur-api-image-upload">Demo</a></span>
  </div>
  
  <div id="toc_container" class="no_bullets">
    <p class="toc_title">
      Contents
    </p>
    
    <ul class="toc_list">
      <li>
        <a href="#sign-up"><span class="toc_number toc_depth_1">1</span> Sign Up</a>
      </li>
      <li>
        <a href="#create-application-get-client-id"><span class="toc_number toc_depth_1">2</span> Create Application & Get Client ID</a>
      </li>
      <li>
        <a href="#html"><span class="toc_number toc_depth_1">3</span> HTML</a>
      </li>
      <li>
        <a href="#uploadphp"><span class="toc_number toc_depth_1">4</span> upload.php</a>
      </li>
      <li>
        <a href="#fixes"><span class="toc_number toc_depth_1">5</span> Fixes</a>
      </li>
    </ul>
  </div>
  
  <h2 style="text-align: left;">
    <span id="sign-up">Sign Up</span>
  </h2>
  
  <p>
    <span style="font-family: inherit;">Go to <a href="https://imgur.com/register">this page</a> to sign up. It supports Social signup with <b>Google</b>, <b>Facebook</b>, <b>Twitter</b> & <b>Yahoo! </b>or you can sign up via the form.</span>
  </p>
  
  <h2 style="text-align: left;">
    <span id="create-application-get-client-id">Create Application & Get Client ID</span>
  </h2>
  
  <p>
    <span style="font-family: inherit;">You need to create an app to get the <b>Client ID</b>. For creating an application go to <a href="https://api.imgur.com/oauth2/addclient">this page</a>.</span><br /> <span style="font-family: inherit;">Select <span style="background-color: white; white-space: pre-wrap;"><b>Anonymous usage without user authorization</b> option as <b>Authorization Type</b>.</span></span>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="https://2.bp.blogspot.com/-dgydVjb63bA/Unu6-42TaFI/AAAAAAAADSs/Cs60K83uWhU/s1600/0066.png"><img src="https://2.bp.blogspot.com/-dgydVjb63bA/Unu6-42TaFI/AAAAAAAADSs/Cs60K83uWhU/s1600/0066.png" alt="" border="0" /></a>
  </div>
  
  <p>
    <span style="font-family: inherit;"><span style="background-color: white; white-space: pre-wrap;">Fill up all the other fields including <b>CAPTCHA</b>. Then click <b>submit</b> button.</span></span><br /> <span style="font-family: inherit;"><span style="background-color: white; white-space: pre-wrap;">You will now get a <b>client ID</b> and a <b>client Secret</b> key. We will only need a <b>client ID</b>.</span></span>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-VcTfGBwRRVM/Unu7eZ9KRSI/AAAAAAAADS0/G51gyDi2pl8/s1600/0067.png"><img src="//1.bp.blogspot.com/-VcTfGBwRRVM/Unu7eZ9KRSI/AAAAAAAADS0/G51gyDi2pl8/s1600/0067.png" alt="" border="0" /></a>
  </div>
  
  <h2 style="text-align: left;">
    <span id="html"><span style="font-family: inherit;"><span style="background-color: white; white-space: pre-wrap;">HTML</span></span></span>
  </h2>
  
  <pre class="prettyprint"><code>&lt;form action="upload.php" enctype="multipart/form-data" method="POST"&gt;
 Choose Image : &lt;input name="img" size="35" type="file"/&gt;&lt;br/&gt;
 &lt;input type="submit" name="submit" value="Upload"/&gt;
&lt;/form&gt;</code></pre>
  
  <p>
    The above form is just like any other normal file upload form.
  </p>
  
  <h2 style="text-align: left;">
    <span id="uploadphp"><span style="background-color: white; white-space: pre-wrap;">upload.php</span></span>
  </h2>
  
  <pre class="prettyprint"><code>&lt;?
$img=$_FILES['img'];
if(isset($_POST['submit'])){ 
 if($img['name']==''){  
  echo "&lt;h2&gt;An Image Please.&lt;/h2&gt;";
 }else{
  $filename = $img['tmp_name'];
  $client_id="88fd52d307ecceb";
  $handle = fopen($filename, "r");
  $data = fread($handle, filesize($filename));
  $pvars   = array('image' =&gt; base64_encode($data));
  $timeout = 30;
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
  curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);
  $out = curl_exec($curl);
  curl_close ($curl);
  $pms = json_decode($out,true);
  $url=$pms['data']['link'];
  if($url!=""){
   echo "&lt;h2&gt;Uploaded Without Any Problem&lt;/h2&gt;";
   echo "&lt;img src='$url'/&gt;";
  }else{
   echo "&lt;h2&gt;There's a Problem&lt;/h2&gt;";
   echo $pms['data']['error'];  
  } 
 }
}
?&gt;</code></pre>
  
  <h2>
    <a href="#fixes"><span id="fixes">Fixes</span></a>
  </h2>
  
  <p>
    Sometimes, the code won&#8217;t work or other problems will arise. In this case, check the following :
  </p>
  
  <ul>
    <li>
      Is PHP JSON extension installed ?
    </li>
    <li>
      Is PHP cURL extension installed ?
    </li>
  </ul>
  
  <p>
    If the above didn&#8217;t fix your problem, add this code just after setting the <strong>$curl</strong> variable in the code :
  </p>
  
  <p>
    <span style="color: #3f4549;"></span>
  </p>
  
  <pre class="prettyprint"><code>curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);</code></pre>
  
  <p>
    <span style="color: #3f4549;"></span>
  </p>
  
  <p>
    By doing this, we are disabling the <strong>SSL Certificate</strong> verification which is not good, so I recommend you do <a href="http://stackoverflow.com/a/6325154/1372424" target="_blank">this</a>.
  </p>
  
  <p>
    <span style="white-space: pre-wrap;">Replace the content of </span><span style="background-color: white; white-space: pre-wrap;"><b>$client_id</b> variable with the <b>Client ID</b> value you got. </span><span style="white-space: pre-wrap; line-height: 1.5em;">You can get the </span><b style="white-space: pre-wrap; line-height: 1.5em;">URL</b><span style="white-space: pre-wrap; line-height: 1.5em;"> of the image uploaded from </span><b style="white-space: pre-wrap; line-height: 1.5em;">$url</b><span style="white-space: pre-wrap; line-height: 1.5em;"> variable.</span><br /> <span style="background-color: white; white-space: pre-wrap;">If you have any problems / suggestions / feedback contact me through the comments.</span>
  </p>
</div>