---
title: 'Tutorial : How to add date and time using php'
author: Subin Siby
type: post
date: 2012-03-31T15:17:00+00:00
excerpt: 'You might want to add date and time using php in your php files. I will tell you how to do it.1 - Go to your PHP page. Paste this code where you want to add date and time&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;date("J-F- Y--H:I A")This code will Show ...'
url: /tutorial-how-to-add-date-and-time-using-php
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - eac7a0e3d5147d01f1213c21daed7c8c
  - eac7a0e3d5147d01f1213c21daed7c8c
  - eac7a0e3d5147d01f1213c21daed7c8c
  - eac7a0e3d5147d01f1213c21daed7c8c
  - eac7a0e3d5147d01f1213c21daed7c8c
  - eac7a0e3d5147d01f1213c21daed7c8c
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
  - http://sag-3.blogspot.com/2012/03/tutorial-how-to-add-date-and-time-using.html
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You might want to add date and time using php in your php files. I will tell you how to do it.</p> 
  
  <p>
    1 &#8211; Go to your PHP page. Paste this code where you want to add date and time<br />&nbsp; &nbsp;
  </p>
  
  <div class="code">
    &nbsp; &nbsp; &nbsp; &nbsp;date("J-F- Y&#8211;H:I A")
  </div>
  
  <p>
    This code will Show date and time in Date-Month-Year &#8211;Hour:Minute:AM/PM<br />If you want to change the type of showing date and time just change the position of the letters.<br />Here is a list of dates you can use in PHP
  </p>
  
  <table border="1" bordercolor="#000000" cellpadding="4" cellspacing="0">
    <colgroup> <col width="128*"></col> <col width="128*"></col> </colgroup> <tr valign="TOP">
      <td width="50%">
        Functions
      </td>
      
      <td width="50%">
        Date types
      </td>
    </tr>
    
    <tr valign="TOP">
      <td height="114" width="50%">
        J<br />F<br />Y<br />H<br />I<br />S<br />A
      </td>
      
      <td width="50%">
        Date<br />Month<br />Year<br />Hour<br />Minutes<br />Seconds<br />AM/PM
      </td>
    </tr>
  </table>
  
  <p>
    &nbsp; &nbsp; &nbsp; &nbsp;
  </p>
</div>