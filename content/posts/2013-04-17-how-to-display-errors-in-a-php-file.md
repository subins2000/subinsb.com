---
title: How to display errors in a PHP file ?
author: Subin Siby
type: post
date: 2013-04-17T05:42:00+00:00
excerpt: "When you code PHP&nbsp;some errors might have happened. But you won't know that and the program don't work. You can use PHP's error_reporting function to display errors. Here's how to do it: Create a file named errors.php&nbsp;in the folder where the f..."
url: /how-to-display-errors-in-a-php-file
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
syndication_item_hash:
  - f7a111eae8a544b320c886e84303a328
  - f7a111eae8a544b320c886e84303a328
  - f7a111eae8a544b320c886e84303a328
  - f7a111eae8a544b320c886e84303a328
  - f7a111eae8a544b320c886e84303a328
  - f7a111eae8a544b320c886e84303a328
blogger_permalink:
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
  - http://sag-3.blogspot.com/2013/04/how-to-display-errors-in-php-file.html
categories:
  - PHP
tags:
  - error
  - PHP ini

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">When you code <b>PHP</b>&nbsp;some errors might have happened. But you won&#8217;t know that and the program don&#8217;t work. You can use <b>PHP</b>&#8216;s <b>error_reporting</b> function to display errors. Here&#8217;s how to do it:</span><br /><span style="font-family: inherit;"><br /></span> <span style="font-family: inherit;">Create a file named <b>errors.php</b>&nbsp;in the folder where the file with errors exist.</span><br /><span style="font-family: inherit;">Open the <b>errors.php</b>&nbsp;file in text editor and paste these codes in the file:</span></p> 
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><span style="font-family: inherit;"><?<br /></span><span style="font-family: inherit;">error_reporting(E_ALL);</span><span style="font-family: inherit;"><br /></span><span style="font-family: inherit;">ini_set("display_errors", 1);</span><span style="font-family: inherit;"><br /></span><span style="font-family: inherit;">include("filename</span>.ph<span style="font-family: inherit;">p</span><span style="font-family: inherit;">"</span><span style="font-family: inherit;">);</span><span style="font-family: inherit;"><br /></span><span style="font-family: inherit;">?></span></span>
    </p>
  </blockquote>
  
  <p>
    Replace <b>filename.php</b>&nbsp;with the name of the file that has errors.<br />Open the <b>errors.php</b>&nbsp;file in browser. You can now see the errors in the file.</div>