---
title: 'Create Simple Username Availability Checker Using jQuery & PHP'
author: Subin Siby
type: post
date: 2013-10-13T07:07:00+00:00
excerpt: "Many sites have signup pages. Every programmers try to make the signup pages attractive by adding stylish input&nbsp;CSS&nbsp;codes and others. Why not add a stylish&nbsp;username availability checker ? It's very easy to add and will work on jQuery&nbs..."
url: /create-simple-username-availability-checker-jquery-php
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-username-availability-checker-jquery.html
dsq_thread_id:
  - 1957230045
  - 1957230045
  - 1957230045
  - 1957230045
  - 1957230045
  - 1957230045
syndication_item_hash:
  - 6f5d4a285098bcd4defa6df6d29f248c
  - 6f5d4a285098bcd4defa6df6d29f248c
  - 6f5d4a285098bcd4defa6df6d29f248c
  - 6f5d4a285098bcd4defa6df6d29f248c
  - 6f5d4a285098bcd4defa6df6d29f248c
  - 6f5d4a285098bcd4defa6df6d29f248c
categories:
  - AJAX
  - CSS
  - HTML
  - jQuery
  - PHP
tags:
  - Demo
  - Download
  - PDO

---
<div dir="ltr" style="text-align: left;">
  <p>
    Many sites have signup pages. Every programmers try to make the signup pages attractive by adding stylish input CSS codes and others. Why not add a stylish username availability checker ? It&#8217;s very easy to add and will work on <b>jQuery</b> versions <b>1.2</b> and up. I&#8217;m also going to tell you how to check if username is available in the server side. Of course it&#8217;s <b>PHP</b>. The checking is <b>SQL</b> <b>Injection</b> free too. For Executing SQL queries we will use <b>PHP PDO</b>.
  </p>
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=yxns9kk4ubhkosb&class=8">Download</a> <a class="demo" href="http://demos.subinsb.com/jquery-username-availability-check">Demo</a>
  </div>
  
  <p>
    First of all create two files<b> </b>&#8211; <b>index.php</b> (<b>signup page</b>), <b>check.php </b>& <b>checker.js</b><br /> It&#8217;s not necessary to set the name of signup page to <b>index.php</b>. Any name will work.
  </p>
  
  <h2 style="text-align: left;">
    index.php
  </h2>
  
  <pre class="prettyprint"><code>&lt;html&gt;
 &lt;head&gt;
  &lt;script src="http://code.jquery.com/jquery-latest.min.js"&gt;&lt;/script&gt;
  &lt;script src="checker.js"&gt;&lt;/script&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;input type="text" size="35" id="user" value="" placeholder="Type Your Desired Username"/&gt;
  &lt;input type="button" id="check" style="cursor:pointer;" value="Check Availablity"/&gt;&lt;br/&gt;
  &lt;div style="margin:5px 15px;" id="msg"&gt;http://mysite.com/&lt;b id="prev"&gt;subinsiby&lt;/b&gt;&lt;/div&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>
  
  <h2 style="text-align: left;">
    checker.js &#8211; Request To Check Username
  </h2>
  
  <pre class="prettyprint"><code>$(document).ready(function(){
 v=$("#user");
 $("#check").on('click',function(){
  v.attr("disabled","true");
  v.css({background:"url(../cdn/load.gif) no-repeat",backgroundSize: "2em",backgroundPosition: "center right"});
  $.post('check.php',{user:v.val().toLowerCase()},function(d){
   v.css("background","white");
   v.removeAttr("disabled");
   if(d=='available'){
    $("#msg").html("&lt;span style='color:green;'&gt;The username &lt;b&gt;"+v.val().toLowerCase()+"&lt;/b&gt; is available&lt;/span&gt;");
   }else{
    $("#msg").html("&lt;span style='color:red;'&gt;The username &lt;b&gt;"+v.val().toLowerCase()+"&lt;/b&gt; is not available&lt;/span&gt;");
   }
  });
 });
 v.bind('keyup',function(){
  $("#prev").text(v.val().toLowerCase());
 });
});</code></pre>
  
  <h2 style="text-align: left;">
    check.php &#8211; Checks Username
  </h2>
  
  <pre class="prettyprint"><code>&lt;?
include("config.php");
$user=strtolower($_POST['user']);
if(isset($_POST) && $user!=''){
 $sql=$dbh-&gt;prepare("SELECT username FROM users WHERE username=?");
 $sql-&gt;execute(array($user));
 if($sql-&gt;rowCount()==0){
  echo "available";
 }else{
  echo "not-available";
 }
}
?&gt;</code></pre>
  
  <p>
    and of course <b>config.php </b>
  </p>
  
  <pre class="prettyprint"><code>&lt;?
$host = "localhost"; // Database Hostname
$port = "3306"; / /MySQL Port : Default : 3306
$user = "username"; // Databse Username Here
$pass = "password"; // Databse Password Here
$db   = "database_name"; // Database Name
$dbh  = new PDO('mysql:dbname='.$db.';host='.$host.';port='.$port,$user,$pass);
?&gt;</code></pre>
  
  <p>
    When the user clicks the <b>Check Availability</b> button, jQuery will send a <b>POST</b> request to <b>check.php</b> with <b>user</b> parameter containing the username value the client entered. check.php will check it&#8217;s database with the username given by <b>AJAX</b> call. If it matches a user response will be not-available and if it doesn&#8217;t match a user, then the response will be available.<br /> jQuery will make changes to the page according to the <b>AJAX</b> response ie jQuery will take care of the rest. If you have any suggestions / feedback/ problems , say it in the comments.
  </p>
</div>