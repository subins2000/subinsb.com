---
title: Check whether a site is XSS vulnerable or not.
author: Subin Siby
type: post
date: 2013-07-03T03:30:00+00:00
excerpt: "XSS&nbsp;is a method to hack sites which most of the newbie programmers don't know. Here is a quick way to identify if a site is vulnerable to it. Go to a site which offers searching or other GET&nbsp;parameters which are outputted in the site's page.I..."
url: /check-whether-a-site-is-xss-vulnerable-or-not
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
  - http://sag-3.blogspot.com/2013/07/check-whether-site-is-xss-vulnerable-or.html
syndication_item_hash:
  - 9210437aca2fd4a5b6d8284c52ab657a
  - 9210437aca2fd4a5b6d8284c52ab657a
  - 9210437aca2fd4a5b6d8284c52ab657a
  - 9210437aca2fd4a5b6d8284c52ab657a
  - 9210437aca2fd4a5b6d8284c52ab657a
  - 9210437aca2fd4a5b6d8284c52ab657a
dsq_thread_id:
  - 1974667642
  - 1974667642
  - 1974667642
  - 1974667642
  - 1974667642
  - 1974667642
categories:
  - HTML
  - JavaScript
tags:
  - GET
  - Hack
  - POST
  - XSS

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;"><b>XSS</b>&nbsp;is a method to hack sites which most of the newbie programmers don&#8217;t know. Here is a quick way to identify if a site is vulnerable to it. Go to a site which offers searching or other <b>GET</b>&nbsp;parameters which are outputted in the site&#8217;s page.</span><br /><span style="font-family: inherit;"><br /></span><span style="font-family: inherit;">Instead of the value in one of the&nbsp;<b>GET</b>&nbsp;parameter use the following value :</span></p> 
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><span style="background-color: white; color: #3d3b3b; line-height: 24px;"><h1>XSS Vulnerability checker by Subin Siby<h1></span><span style="background-color: white; color: #3d3b3b; line-height: 24px;"><script>alert(&#8216;Site is XSS Vulnerable&#8217;)</script></span></span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; color: #3d3b3b; font-family: 'Droid Sans'; font-size: 16px; line-height: 24px;">For example a site with the <b>GET</b>&nbsp;parameter <b>query </b>with value <b>subins</b>&nbsp;and the url will be :</span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; color: #3d3b3b; font-family: 'Droid Sans'; font-size: 16px; line-height: 24px;">http://subins.com/search.php?query=</span><span style="background-color: #bf9000; font-family: 'Droid Sans'; font-size: 16px; line-height: 24px;"><span style="color: white;">subins</span></span>
    </p>
  </blockquote>
  
  <p>
    <span style="color: #3d3b3b; font-family: Droid Sans;"><span style="line-height: 24px;">For such a site you have to replace the parameter value with this:</span></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; color: #3d3b3b; line-height: 24px;"><h1>XSS Vulnerability checker by Subin Siby<h1></span><span style="background-color: white; color: #3d3b3b; line-height: 24px;"><script>alert(&#8216;Site is XSS Vulnerable&#8217;)</script></span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; color: #3d3b3b; line-height: 24px;">If the site outputs the <b>GET</b>&nbsp;type&nbsp;parameter then you will get a javascript alert. The alert means that the site is vulnerable to <b>XSS</b>.</span><br /><span style="background-color: white; color: #3d3b3b; line-height: 24px;">This trick can be also used in <b>POST</b>&nbsp;type parameter by injecting the code mentioned to the input field.</span></div>