---
title: Javascript code Compressor Tool
author: Subin Siby
type: post
date: 2013-07-26T03:30:00+00:00
excerpt: "If long long Javascript&nbsp;codes are compressed, it will make the page load faster. In this post I'm going to suggest a perfect tool for compressing Javascript&nbsp;code. The tool is JsCompress.Go to http://blimptontech.com/&nbsp;to see the compresso..."
url: /javascript-code-compressor-tool
authorsure_include_css:
  - 
  - 
  - 
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_item_hash:
  - 9db2f79140bdf5e1493638e7b66136ee
  - 9db2f79140bdf5e1493638e7b66136ee
  - 9db2f79140bdf5e1493638e7b66136ee
  - 9db2f79140bdf5e1493638e7b66136ee
  - 9db2f79140bdf5e1493638e7b66136ee
  - 9db2f79140bdf5e1493638e7b66136ee
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
  - http://sag-3.blogspot.com/2013/07/javascript-code-compressor-tool.html
categories:
  - JavaScript
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If long long <b>Javascript</b>&nbsp;codes are compressed, it will make the page load faster. In this post I&#8217;m going to suggest a perfect tool for compressing <b>Javascript</b>&nbsp;code. The tool is <b>JsCompress</b>.<br />Go to <a href="http://blimptontech.com/?utm_source=subinsblog" >http://blimptontech.com/</a>&nbsp;to see the compressor in action.<br />You have three options:</p> 
  
  <ol style="text-align: left;">
    <li>
      Paste Code and Compress
    </li>
    <li>
      Compress code in files
    </li>
    <li>
      Compress Multiple files into one.
    </li>
  </ol>
  
  <p>
    It&#8217;s a great tool and it will help you reduce the file size up to <b>100&nbsp;KB</b>. This site is similar to the <b>JsCompressor</b>&nbsp;tool&nbsp;@ <a href="http://jscompress.com/" >http://jscompress.com</a>.&nbsp;</div>