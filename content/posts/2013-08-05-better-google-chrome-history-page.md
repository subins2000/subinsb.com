---
title: Better Google Chrome History Page
author: Subin Siby
type: post
date: 2013-08-05T03:30:00+00:00
excerpt: "As you know, Google Chrome's Browser History is not efficient. It's history is not like any other browsers. I think they made the History&nbsp;page in a hurry. If we want to search for a page we visited before, we won't get it by searching on History&n..."
url: /better-google-chrome-history-page
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
  - http://sag-3.blogspot.com/2013/08/better-google-chrome-history-page.html
syndication_item_hash:
  - a4a57bb53c6a5b2e62dbc2454add9722
  - a4a57bb53c6a5b2e62dbc2454add9722
  - a4a57bb53c6a5b2e62dbc2454add9722
  - a4a57bb53c6a5b2e62dbc2454add9722
  - a4a57bb53c6a5b2e62dbc2454add9722
  - a4a57bb53c6a5b2e62dbc2454add9722
tags:
  - Chrome
  - Extension
  - Google
  - Recommends
  - Webstore

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="background-color: white; font-family: Times, Times New Roman, serif;">As you know, <b>Google Chrome</b>&#8216;s Browser History is not efficient. It&#8217;s history is not like any other browsers. I think they made the <b>History</b>&nbsp;page in a hurry. If we want to search for a page we visited before, we won&#8217;t get it by searching on <b>History</b>&nbsp;page. That&#8217;s the main problem.</span><br /><span style="background-color: white; font-family: Times, Times New Roman, serif;">This problem was solved when a lot of users created an alternate <b>History</b>&nbsp;Page extension on <b>Chrome Webstore</b>.</span><br /><span style="background-color: white; font-family: Times, Times New Roman, serif;">In this post I&#8217;m gonna tell the most used <b>History</b>&nbsp;Alternate Extension. It&#8217;s <b>Better History</b>&nbsp;extension. You can download the extension from&nbsp;<a href="https://chrome.google.com/webstore/detail/obciceimmggglbmelaidpjlmodcebijb" >here</a>.</span><br /><img src="https://lh4.googleusercontent.com/ftqlRdjF7Kq2Eo08QQHXdDoXtd7qJHXJaXmUsJyG7fuFZsK2ZsVr8X5vX_Yf7KE-zpYvTEF_oQ=s640-h400-e365-rw" /><br /><b><span style="font-size: large;">Deleting:</span></b></p> 
  
  <ul>
    <li>
      Delete all visits for a day
    </li>
    <li>
      Delete individual visits
    </li>
    <li>
      Delete visits to the same domain
    </li>
    <li>
      Delete all history
    </li>
  </ul>
  
  <p>
    <b><span style="font-size: large;">Grouping:</span></b>
  </p>
  
  <ul style="text-align: left;">
    <li>
      Group visits by 60, 30, or 15 minutes
    </li>
    <li>
      Group visits to the same domain
    </li>
    <li>
      Format hours to 12 or 24
    </li>
    <li>
      Customizable grouping behavior
    </li>
  </ul>
  
  <p>
    <b><span style="font-size: large;">Organizing:</span></b>
  </p>
  
  <ul>
    <li>
      Filter visits by day
    </li>
  </ul>
  
  <p>
    <b><span style="font-size: large;">Searching:</span></b>
  </p>
  
  <ul>
    <li>
      Search the last few months
    </li>
    <li>
      Search by date, url, and page title
    </li>
    <li>
      Highlighted search terms
    </li>
    <li>
      Right click option to search by domain
    </li>
    <li>
      Right click option to search by selected text
    </li>
  </ul>
  
  <p>
    <b><span style="font-size: large;">Styling:</span></b>
  </p>
  
  <ul>
    <li>
      Styled with Chrome UI
    </li>
    <li>
      Access from button in toolbar&nbsp;
    </li>
    <li>
      Remembers closing state
    </li>
    <li>
      Back button support
    </li>
  </ul>
</div>