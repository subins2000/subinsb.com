---
title: logSys Admin
author: Subin Siby
type: post
date: 2015-09-14T03:30:21+00:00
url: /logsys-admin
categories:
  - Lobby
  - Operating System
  - PHP
  - Program
tags:
  - logSys

---
After the release of [logSys][1] in May 2014, many have been asking for an admin panel of [logSys][1]. It&#8217;s finally here. I&#8217;m extremely happy to release **logSys Admin** as an app of **[Lobby][2]**.<figure class="wp-caption aligncenter">

[<img class="" src="http://lobby.subinsb.com/api/app/fr-logsys/logo" alt="logSys Admin Logo" width="300" height="300" />][3]<figcaption class="wp-caption-text">logSys Admin Logo</figcaption></figure> 

You can find the <a href="http://lobby.subinsb.com/apps/fr-logsys" target="_blank">app here on Lobby</a>.

# Installation

  1. Download & Install **<a href="http://lobby.subinsb.com/download" target="_blank">Lobby</a>**.
  2. Open Lobby
  3. Open **<a href="http://lobby.subinsb.com/apps" target="_blank">Lobby Store</a> **and search for "logSys Admin".
  4. Click Install button.

# Usage

  1. Open App from the Lobby Dashboard and click "Go To Admin" button.
  2. Click "**Setup logSys Admin**" button.
  3. Enter the credentials of your database as well as the table which **logSys** uses and Save.
  4. Once saved, you can use **logSys Admin**

The dashboard page has some quick facts about your users :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-dash.png" alt="logSys Admin Dashboard" width="1197" height="732" />][4]<figcaption class="wp-caption-text">logSys Admin Dashboard</figcaption></figure> 

You can manage your users : Add/Edit/Remove + Add New Column + Export Data<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-users.png" alt="logSys Admin Users' Page" width="1317" height="732" />][5]<figcaption class="wp-caption-text">logSys Admin Users&#8217; Page</figcaption></figure> 

See the active tokens that can be used to reset pasword :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-tokens.png" alt="logSys Admin Tokens' Page" width="1317" height="732" />][6]<figcaption class="wp-caption-text">logSys Admin Tokens&#8217; Page</figcaption></figure> 

See graphical representation of stats :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-stats.png" alt="logSys Admin Stats" width="1317" height="732" />][7]<figcaption class="wp-caption-text">logSys Admin Stats</figcaption></figure> 

It is not complete and I will keep continuing add more features. You will be notified in Lobby when a new version is available and you can update it with a single click.

Some features I&#8217;m hoping to add :

  * User Roles
  * Specific Pages For Specific Users

# Security

By default, **Lobby** can be accessed by anyone. So, your **logSys Admin** is accessible to everyone. To prevent this, you can install the **Lobby Admin** app which lets you password protect your Lobby installation.

Sidenote: Lobby Admin also uses **logSys** as it&#8217;s login system. After you install **Lobby Admin**, you have to login before accessing **Lobby**. The default username and password once you install is "admin".

 [1]: //subinsb.com/php-logsys
 [2]: //subinsb.com/lobby
 [3]: //googledrive.com/host/0B2VjYaTkCpiQcXh2SUNJSk90Yzg/logo.png
 [4]: //lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-dash.png
 [5]: //lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-users.png
 [6]: //lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-tokens.png
 [7]: //lab.subinsb.com/projects/blog/uploads/2015/09/fr-logsys-stats.png