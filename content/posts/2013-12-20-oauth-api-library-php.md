---
title: Implementing OAuth System Using OAuth API Library In PHP
author: Subin Siby
type: post
date: 2013-12-20T15:07:52+00:00
url: /oauth-api-library-php
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - OAuth

---
**OAuth Login System** is one of the needed system in a website. There are many online services that have their own **OAuth** system. **Popular Services** have their own **PHP OAuth** library and documentation.

But, we&#8217;re gonna use a simpler and a common **Library** for all of those services. This common **library** (OAuth API) is created by **PHPClasses.org** founder **Manuel Lemos**.

His library can be used to authorize many **API**&#8216;s including **Facebook**, **Twitter**, **Google**. Everything is made easy by Manuel. He is a hard working programmer. That&#8217;s why he has the **\# 1** rank on **PHP Classes**.


# Demo

<a href="http://open.subinsb.com/oauth/login_with_facebook" target="_blank">Login With Facebook</a>

<a href="http://open.subinsb.com/oauth/login_with_google" target="_blank">Login With Google</a>

# Download

Only some files are needed from the library to accomplish our tasks. You can see the all the **Library** files @ **<a href="http://www.phpclasses.org/package/7700-PHP-Authorize-and-access-APIs-using-OAuth.html" target="_blank">PHPClasses</a>**. **OAuth API** also need the **HTTP Protocol Client** class. Here are the files you need to download for **OAuth API** :

  1. <a href="http://www.phpclasses.org/browse/file/42008.html" target="_blank">oauth_client.php</a>
  2. <a href="http://www.phpclasses.org/browse/file/5.html" target="_blank">http.php</a>

# Features

  1. Implement the **OAuth** protocol to retrieve a token from a server to authorize the access to an API on behalf of the current user.
  2. Store authorization details (access token, refresh token etc..) according to users in a table using **SQL**.
  3. Offline Access to **API**&#8216;s
  4. Perform calls to a Web services API using a token previously obtained using this class or a token provided some other way by the Web services provider.

# Some Of The OAuth API&#8217;s

  1. Facebook
  2. Google
  3. Twitter
  4. LinkedIn
  5. Instagram
  6. Ubuntu
  7. Microsoft Live
  8. Tumblr
  9. Yahoo

and many more&#8230;..

# Including

You only have to include **oauth_client.php**. **http.php **will be included by **oauth_client.php** :

<pre class="prettyprint"><code>&lt;?php
require("oauth_client.php");
?&gt;</code></pre>

## Examples

As I mentioned in the demo part before, this has been implemented in [Open][7]. You can see the source code of them :

[login\_with\_facebook.php][8]

[login\_with\_google.php][9]

I will be publishing posts about implementing various **API**&#8216;s using this library in the future.

 [1]: #demo
 [2]: #download
 [3]: #features
 [4]: #some-of-the-oauth-api8217s
 [5]: #including
 [6]: #examples
 [7]: https://open.subinsb.com
 [8]: https://github.com/subins2000/open/blob/master/source/oauth/login_with_facebook.php
 [9]: https://github.com/subins2000/open/blob/master/source/oauth/login_with_google.php
