---
title: 'Tutorial : Write a file using php'
author: Subin Siby
type: post
date: 2012-03-31T15:39:00+00:00
excerpt: |
  Hi. You must have heard of writing file using PHP. You want to know how to do this. I'll tell you. Please follow these steps.1 - Open your php file in text editor. Paste this code in it.&lt;?php&nbsp; &nbsp; $myFile = "file.html";&nbsp; &nbsp; $fh = fo...
url: /tutorial-write-a-file-using-php
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 7f851e9d33d3b64d9ba78ede3c190c04
  - 7f851e9d33d3b64d9ba78ede3c190c04
  - 7f851e9d33d3b64d9ba78ede3c190c04
  - 7f851e9d33d3b64d9ba78ede3c190c04
  - 7f851e9d33d3b64d9ba78ede3c190c04
  - 7f851e9d33d3b64d9ba78ede3c190c04
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
  - http://sag-3.blogspot.com/2012/03/tutorial-write-file-using-php.html
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Hi. You must have heard of writing file using PHP. You want to know how to do this. I&#8217;ll tell you. Please follow these steps.<br />1 &#8211; Open your php file in text editor. Paste this code in it.</p> 
  
  <div class="code">
    <blockquote class="tr_bq">
      <p>
        <?php<br />&nbsp; &nbsp; $myFile = "file.html";<br />&nbsp; &nbsp; $fh = fopen($myFile, &#8216;a+&#8217; );<br />&nbsp; &nbsp; $stringData = ("Hello");<br /> &nbsp; &nbsp; fwrite($fh, $stringData);<br />&nbsp; &nbsp; fclose($fh);<br />?>
      </p>
    </blockquote>
  </div>
  
  <p>
    This will write Hello to a file named file.html. Enjoy!!!
  </p>
</div>