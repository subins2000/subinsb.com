---
title: Disable Cache For Web Development In Chrome
author: Subin Siby
type: post
date: 2014-02-27T14:28:50+00:00
url: /load-page-without-cached-resources-in-chrome
authorsure_include_css:
  - 
  - 
  - 
tags:
  - Cache
  - Chrome
  - Developing
  - featured
  - Google

---
If you&#8217;re developing a web page in Chrome and some changes you applied to code doesn&#8217;t take effect on the page, you will be angry. You will look and look into the code for errors. But it&#8217;s not the problem with your code. It&#8217;s the problem with the browser.

The resources loaded on the page are cached. Hence the changes made to the code won&#8217;t be applied. So, you need some way to disable the cache.  If you look around the Chrome Settings page, you will see an option &#8211; it&#8217;s **Clear Browsing Data**. If you do this, you will clear the entire cache. Even the website which you want the cache (like Facebook, Google +).

There isn&#8217;t any "temporarily disable cache" option in Chrome Settings. But there is one in the Developer Tools window. I will show you the way of disabling the cache temporarily in different Chrome versions.


## Versions 19 & Up

Open the developer tools by using the key combination **CTRL + SHIFT + I**. In mac it&#8217;s **COMMAND + OPTION + I**.

Click on the settings icon seen at the bottom right corner of the dev tools window.

You can see the **Disable cache** option. Check It. Now when you reload the page, any of the resources it loads won&#8217;t be cached.<figure id="attachment_2593" class="wp-caption aligncenter">

[<img class="size-full wp-image-2593" alt="Disable Cache In Chrome" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0108.png" width="706" height="234" />][2]<figcaption class="wp-caption-text">General Setting Page Of Chrome Dev Tools</figcaption></figure> 

This temporary disable only works when the Dev Tools is opened.

## Versions 15 &#8211; 18

In the versions between 15 & 18, the **Disable cache** option can be seen below **Network** :<figure id="attachment_2594" class="wp-caption aligncenter">

[<img class="size-full wp-image-2594" alt="Disable Cache In Chrome" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0109.png" width="609" height="191" />][4]<figcaption class="wp-caption-text">Disable Cache In Chrome Versions 15-18</figcaption></figure> 

This temporary disable only works when the Dev Tools is opened.

## Versions below 15

There is another way which I don&#8217;t know that it will work on all versions under 15. Open Developer Tools and click on the **Audits** tab.

Check the **Reload Page and Audit on Load** option and click on **Run**. The page will be reloaded without loading resources from cache memory :<figure id="attachment_2595" class="wp-caption aligncenter">

[<img class="size-full wp-image-2595" alt="Clear Cache for Page In Chrome" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0110.png" width="706" height="314" />][6]<figcaption class="wp-caption-text">Clear Cache for Page In Chrome using Audit</figcaption></figure> 

The only problem with this is that it will only load uncached content of the domain where dev tools. is opened. In the above case it&#8217;s **http://subinsb.com**.

## Other ways

This is an easy way. Open Dev Tools and right click or hold left clicking the reload button. A small window will be opened and you can choose the type of reload you want. Click the **Hard Reload** option.<figure id="attachment_2598" class="wp-caption aligncenter">

[<img class="size-full wp-image-2598" alt="Reload Without Cache In Chrome" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0111.png" width="364" height="110" />][8]<figcaption class="wp-caption-text">Reload Without Cache In Chrome easiest way</figcaption></figure> 

You can also use the key combination **SHIFT + CTRL + R** to reload page without cache.

So, which method worked for you ?

 [1]: #versions-19-up
 [2]: //lab.subinsb.com/projects/blog/uploads/2014/02/0108.png
 [3]: #versions-15-8211-18
 [4]: //lab.subinsb.com/projects/blog/uploads/2014/02/0109.png
 [5]: #versions-below-15
 [6]: //lab.subinsb.com/projects/blog/uploads/2014/02/0110.png
 [7]: #other-ways
 [8]: //lab.subinsb.com/projects/blog/uploads/2014/02/0111.png
