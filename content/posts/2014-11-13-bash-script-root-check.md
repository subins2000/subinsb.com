---
title: Check if Bash Script is ran by Root User
author: Subin Siby
type: post
date: 2014-11-13T03:30:56+00:00
url: /bash-script-root-check
categories:
  - Linux
  - Short Post
  - Unix
tags:
  - BASH
  - Script

---
Root users have the privilege to do administrative stuff in Linux and if you&#8217;re creating a **Bash** script to install something or change something in the system, you need **root**. So, checking whether the script was ran by **root** is very important.

<pre class="prettyprint"><code>if [ "$EUID" -ne 0 ]
   then echo "Please run as root"
   exit
fi</code></pre>

There is no **else** condition. You can add the code which will does stuff in root after the above. If root didn&#8217;t ran the script, then the script will terminate itself without continuing after "Please run as root" message is displayed.

Example :

<pre class="prettyprint"><code>if [ "$EUID" -ne 0 ]
   then echo "Please run as root"
   exit
fi
print "The script is continuing"
</code></pre>

&nbsp;