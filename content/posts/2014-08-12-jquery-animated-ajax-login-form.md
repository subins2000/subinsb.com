---
title: Animated AJAX Login Form With jQuery
author: Subin Siby
type: post
date: 2014-08-12T03:30:40+00:00
url: /jquery-animated-ajax-login-form
authorsure_include_css:
  - 
  - 
  - 
categories:
  - CSS
  - HTML
  - JavaScript
  - jQuery
  - PHP
tags:
  - Log In
  - Tutorials

---
The Web is growing rapidly. Each day, new features are arriving. With the coming of **AJAX**, many websites became more easy to handle. With **jQuery**, websites became more attractive with animations and styles. In this tutorial, we are going to create a stylish animated Login Form with **jQuery**, **CSS** & **AJAX**. With this, the login time for the user is greatly reduced as the page shouldn&#8217;t be loader all over again.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=s/qlv8a8lje4ktjpjfkke3&class=29" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/jquery/ajaxLogin" target="_blank">Demo</a>
</div>

## The Login Page

We make a login page first loading with **jQuery** and the stylesheet :

<pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
   &lt;head&gt;
      &lt;script src="http://code.jquery.com/jquery-1.9.0.min.js"&gt;&lt;/script&gt;
      &lt;script src="form.js"&gt;&lt;/script&gt;
      &lt;link href="form.css" rel="stylesheet" /&gt;
   &lt;/head&gt;
   &lt;body&gt;
      &lt;div id="content"&gt;
         &lt;div class="loginForm"&gt;
            &lt;label&gt;
               &lt;span&gt;Username&lt;/span&gt;
               &lt;input type="text" name="username" /&gt;
            &lt;/label&gt;
            &lt;label&gt;
               &lt;span&gt;Password&lt;/span&gt;
               &lt;input type="password" name="password" /&gt;
            &lt;/label&gt;
            &lt;div class="submit"&gt;Log In&lt;/div&gt;
            &lt;div class="overlay"&gt;
               &lt;div class="message processing"&gt;
                  &lt;h2&gt;Logging In&lt;/h2&gt;
               &lt;/div&gt;
               &lt;div class="message error"&gt;
                  &lt;h2&gt;Incorrect Credentials&lt;/h2&gt;
                  &lt;p&gt;The username or password you entered was not correct&lt;/p&gt;
               &lt;/div&gt;
               &lt;div class="message success"&gt;
                  &lt;h2&gt;Logged In&lt;/h2&gt;
                  &lt;p&gt;You have been logged in&lt;/p&gt;
               &lt;/div&gt;
            &lt;/div&gt;
         &lt;/div&gt;
      &lt;/div&gt;
   &lt;/body&gt;
&lt;/html&gt;</code></pre>

We have a login form with two fields : username and password with a login button. We also have an overlay showing messages on each actions. Note that the login form is not wrapped in the native **<form>** tag.

## form.css

We style the login form, make it attractive using this stylesheet :

<pre class="prettyprint"><code>.loginForm{
 display:table;
 margin: 20px auto;
 padding: 6px 24px 26px;
 position:relative;
 font-weight: 400;
 background: #fff;
 box-shadow: 0 1px 3px rgba(0,0,0,.13);
}
.loginForm .overlay{
 position: absolute;
 top: 0px;
 left: 0px;
 right: 0px;
 bottom: 0px;
 padding: 50px 30px;
 display: none;
 background: rgba(204, 204, 204, 0.7);
 text-align: center;
}
.loginForm .overlay .message{
 display: none;
}
.loginForm label{
 display: block;
 margin-top: 20px;
}
.loginForm label span{
 color: #777;
 display: block;
 font-size: 14px;
}
.loginForm input[type=text], .loginForm input[type=password]{
 margin-top: 5px;
 background: #fbfbfb;
 padding: 3px;
 border: 1px solid #ddd;
 box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
 font-size: 24px;
}
.loginForm .submit{
 display: inline-block;
 padding: 7px 15px;
 margin-top: 15px;
 background: #23A3F8;
 color: white;
 cursor: pointer;
 box-shadow: 0 1px 3px rgba(0,0,0,.13);
}
.loginForm .submit:hover{
 box-shadow: inset 0 1px 2px rgba(0,0,0,.07);
}
.loginForm .submit:active{
 box-shadow: rgba(0, 0, 0, 0.498039) 0px 2px 10px -3px inset;
}</code></pre>

## form.js

The core component of the login form lies within this JavaScript file :

<pre class="prettyprint"><code>var settings = {
    checkURL : "login.php",
    form     : ".loginForm",
    username : ".loginForm input[name=username]",
    password : ".loginForm input[name=password]",
    overlay  : ".loginForm .overlay",
    success  : ".loginForm .overlay .success",
    error    : ".loginForm .overlay .error",
    process  : ".loginForm .overlay .processing",
};
function overlayDisplay(elem, type){
    if( type == "hide" ){
       $(elem).fadeOut(1000, function(){
          $(settings.overlay).hide();
       });
    }else{
       $(settings.overlay).show();
       $(elem).fadeIn(1000);
 
       /* Auto fade the error overlay and clear the password field after 3 seconds */
       if( elem == settings.error ){
          $(settings.password).val("");
          setTimeout(function(){
             overlayDisplay(settings.error, "hide");
          }, 3000);
       }
    }
}
$(document).ready(function(){
    $(settings.form).find(".submit").on("click", function(){
       var data = {
          "username" : $(settings.username).val(),
          "password" : $(settings.password).val(),
       };
       overlayDisplay(settings.overlay);
 
       $(settings.process).fadeIn(1000, function(){
          $.post(settings.checkURL, data, function(response){
             $(settings.overlay).children().hide(); // Hide all Overlay windows
 
             if(response == "incorrect" || response == ""){
                overlayDisplay(settings.error);
             }else if(response == "correct"){
                overlayDisplay(settings.success);
                /* Do What Else you want here */
             }
          }).fail(function(){
             overlayDisplay(settings.error);
          });
       });
 
       $(settings.form + "#tryAgain").on("click", function(){
          $(settings.error).fadeOut(500, function(){
             $(settings.overlay).hide();
          });
       });
    });
});</code></pre>

You can change the configuration of how the login form work in the **settings** variable that contains a **JSON Object**. **login.php** is the file that process the login according to the username and password sent to it. It should respond "correct" if the login credentials were correct and if not, it should respond "incorrect". **jQuery** will make the necessary changes in the display according to the response. You can change the messages of each action from the **HTML** elements in the **.overlay** element.