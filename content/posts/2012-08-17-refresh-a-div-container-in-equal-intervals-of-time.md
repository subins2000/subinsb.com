---
title: Refresh a Div container in equal intervals of time
author: Subin Siby
type: post
date: 2012-08-17T08:14:00+00:00
excerpt: |
  Hi this tutorial will help you to refresh a div every 10 seconds. You can change the seconds if you want. For this we are going to use Jquery.The html of the div container&nbsp;&lt;div id='refreshdiv'&gt;&lt;/div&gt;Jquery Code&lt;script src="http://co...
url: /refresh-a-div-container-in-equal-intervals-of-time
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
dsq_thread_id:
  - 1958396626
  - 1958396626
  - 1958396626
  - 1958396626
  - 1958396626
  - 1958396626
syndication_item_hash:
  - 8bef0e387ac138dac620996a6e18089a
  - 8bef0e387ac138dac620996a6e18089a
  - 8bef0e387ac138dac620996a6e18089a
  - 8bef0e387ac138dac620996a6e18089a
  - 8bef0e387ac138dac620996a6e18089a
  - 8bef0e387ac138dac620996a6e18089a
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
  - http://sag-3.blogspot.com/2012/08/refresh-div-container-in-equal.html
categories:
  - HTML
  - jQuery
tags:
  - Education

---
<div dir="ltr" style="text-align: left;">
  Hi this tutorial will help you to refresh a div every 10 seconds. You can change the seconds if you want. For this we are going to use <b>Jquery</b>.</p> 
  
  <div>
  </div>
  
  <div>
    <b>The html of the div container </b>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <div id=&#8217;refreshdiv&#8217;></div>
    </p>
  </blockquote>
  
  <p>
    <b>jQuery Code</b>
  </p>
  
  <pre class="prettyprint"><code>&lt;script src="http://code.jquery.com/jquery-latest.js"&gt;&lt;/script&gt;
&lt;script&gt;setInterval(function() {$('#refreshdiv').load('divcontainer.html')}, 10000);&lt;/script&gt;</code></pre>
  
  <p>
    <b>divcontainer.html </b>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      This is loading in the div #refreshdiv.
    </p>
  </blockquote>
  
  <p>
    This example will load <b>divcontainer.html</b> in the div <b>#refreshdiv </b>and refresh the div after 10 seconds.<br /> If you want to change the file to load replace <b>divcontainer.html </b>in the jquery code to the file name you want to use.<br /> If you want to change the number of seconds, replace <b>10000</b> with the number to refresh(Must be in milliseconds).<br /> If you want help comment on this post.
  </p>
</div>