---
title: Get Absolute Directory Path of Current File in PHP
author: Subin Siby
type: post
date: 2014-03-15T04:35:36+00:00
url: /php-get-script-absolute-directory-path
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - File

---
If your code is including other files, you will encounter problems like "The file is not found". This is because, **PHP** doesn&#8217;t know the absolute path of file. So, if you are including files on another file, you should mention the absolute path of file.

But, If you&#8217;re PHP application is being used by other people, then the absolute path which you mentioned in your system won&#8217;t be the one of the others&#8217; systems. So, you have to dynamically mention the absolute path that will work on any systems.

## Example of Problem

Here is one of the case scenarios where you encounter the **include** problem.

Suppose, the **index.php** file is including the **file1.php** which is in the **inc** directory :

<pre class="prettyprint"><code>&lt;?
include("inc/file1.php");
?&gt;</code></pre>

The **inc** directory also contains other files such as **file2.php**, **file3.php**, **file4.php** etc&#8230;&#8230;. If you want to include the **file2.php** in **file1.php** like below, it won&#8217;t work :

<pre class="prettyprint"><code>&lt;?
include("file2.php");
?&gt;</code></pre>

In **file1.php**, the root directory is that of **index.php**. Because we are including the **file1.php** from **index.php** which is outside the directory.**
  
**

I think you understand the problem. Luckily, I have a solution that fixes this problem.

## Solution of Problem

Since PHP **4.0.2**, **\_\_FILE\_\_** returns the absolute path of the running script even if it&#8217;s an include file. By giving the **\_\_FILE\_\_** constant to the **dirname** function, you will get the directory of the script. So, code in **file1.php** would become :

<pre class="prettyprint"><code>&lt;?
include(dirname(__FILE__)."/file2.php");
?&gt;</code></pre>

The version **5.3.0** and it&#8217;s later versions have a new constant called **\_\_DIR\_\_** which is the short way of **dirname(\_\_FILE\_\_)**. You can also use this in replacement of what we used earlier. Now code in **file1.php** will become :

<pre class="prettyprint"><code>&lt;?
include(__DIR__."/file2.php");
?&gt;</code></pre>

You can see more **PHP Magic Constants** in <a href="http://www.php.net/manual/en/language.constants.predefined.php" target="_blank">this manual page</a>.