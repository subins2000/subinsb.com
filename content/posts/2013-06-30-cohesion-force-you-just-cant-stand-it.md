---
title: 'Cohesion Force : You just can’t stand it'
author: Subin Siby
type: post
date: 2013-06-30T16:05:00+00:00
url: /cohesion-force-you-just-cant-stand-it
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
  - http://sag-3.blogspot.com/2013/06/cohesion-force-you-just-cant-stand-it.html
syndication_item_hash:
  - 0e9a53397559097fa122a026061bc0c7
  - 0e9a53397559097fa122a026061bc0c7
  - 0e9a53397559097fa122a026061bc0c7
  - 0e9a53397559097fa122a026061bc0c7
  - 0e9a53397559097fa122a026061bc0c7
  - 0e9a53397559097fa122a026061bc0c7
categories:
  - Personal
tags:
  - Physics
  - School

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-xP0kviL_MGc/UdBW3VjdsQI/AAAAAAAAC0g/awUUQnIJYs4/s1600/cohesion.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-xP0kviL_MGc/UdBW3VjdsQI/AAAAAAAAC0g/awUUQnIJYs4/s1600/cohesion.png" /></a>
  </div>
  
  <p>
    </div>