---
title: Coding In Malayalam
author: Subin Siby
type: post
date: 2020-07-14T00:00:00+00:00
lastmod: 2020-07-14T00:00:06+00:00
url: /coding-in-malayalam
aliases:
    - /malayakkam
categories:
  - Tech
tags:
  - malayalam
  - free software
  - open source
---

Some days ago, I came across [Malayalam numerals](https://en.wikipedia.org/wiki/Malayalam_script#Numeral_System) and found it very interesting. Unlike Hindi or Kannada numerals, the numerical grammar of Malayalam was unique. It's a direct fork of the Tamil numeric system. So, Tamil & Malayalam has this unique numeral system. It is no longer commonly used though. Like the rest of the world, we use Hindu-Arabic numeral system now.

I had to include my phone number in a webpage recently for an event hosting and was concerned whether spammers would take hold of it with their fancy phone-number mining scripts. An idea to prevent this and still give the phone number was to write it in Malayalam numerals. The event was related to Malayalam, so some people would recognize it. Besides, if there were people who really really wanted to reach via phone, they'd go through the trouble to read the numerals eh ? 😉 :D (psst, see my [contact page](/contact))

Here's a phone number sample :

>    +91 7458367458
>
>    +൯൧ ൭൪൫൮൩൬൭൪൫൮

So this is a good usecase right, and it's very unlikely that there's a spam crawler script which can read malayalam numerals.

It's difficult to look up each number and do a manual replace. I automated it by making a webtool to convert Hindu-Arabic numerals to Malayalam numerals.

<div class="padlinks">
  <a class="demo" href="https://demos.subinsb.com/malayakkam/">Demo</a>
  <a class="download" href="https://github.com/subins2000/malayakkam">Source Code</a>
</div>

But I wanted to make this more cool, so I decided to write the code in Malayalam using [Thengascript](https://makers-of-kerala.now.sh/thengascript/). Thengascript is a dialect of JavaScript. Each JavaScript keyword has a Malayalam alternative in it. Once you have the dictionary, it's fairly easy to write code.

## Editor

My primary code editor is Sublime Text, but Malayalam rendering in it wasn't good. So I had to find alternatives.

KDE's Kate editor with Manjari (and other Unicode Malayalam) fonts worked really well.

VSCode failed to give correct rendering of some ligatures even after setting the font. This is definitely because of splitting up of characters due to syntax highlighting :

![Programming in Malayalam - VSCode](https://lab.subinsb.com/projects/blog/uploads/2020/07/programming-in-malayalam-vscode.png)

So I picked Kate.

## Typing

I used a mix of [Varnam](https://www.varnamproject.com/) & [Swanalekha](https://swanalekha.smc.org.in/) to code in Malayalam on Linux. With the [ibus integration](https://github.com/varnamproject/libvarnam-ibus), I can toggle between them and English very easily.

## Language Style

Since Malayalam is Subject-Object-Verb as opposed to English's Subject-Verb-Object, it feels awkward to write some statements.

Example :

```js
var arr = [1, 2, 3]
for (var a in arr) {
  if (a == 1) {
    continue
  }
}
```

In Thengascript, the equivalent is :

```js
ആവട്ടെ arr = [1, 2, 3]
ഇന് (ആവട്ടെ a ഇല്‍ arr) {
  ആണെങ്കിൽ (a == 1) {
    തുടരുക
  }
}
```

If following Malayalam grammar, the better way to write it would be :

```js
arr = [1, 2, 3] ആവട്ടെ
(arr ലെ a) ഓരോന്നും {
  (a == 1) ആണെങ്കിൽ {
    തുടരുക
  }
}
```

![Programming in Malayalam - KDE Kate Editor](https://lab.subinsb.com/projects/blog/uploads/2020/07/programming-in-malayalam-kde-kate.png)

At last, I've made a [simple script](https://github.com/subins2000/malayakkam/blob/master/malayakkam.js) to encode and decode Malayalam numerals <-> Hindu-Arabic numerals. I called it [Malayakkam (മലയക്കം)](https://github.com/subins2000/malayakkam) and it's just under 30 lines of Thengascript.

Also check out [malluscript](https://github.com/Sreyas-Sreelal/malluscript), a scripting language in Malayalam and Manglish made with Rust.

Tamil has its own programming language called [Ezhil](https://en.wikipedia.org/wiki/Ezhil_(programming_language)) from 2007 onwards. They specially made it for kids to understand programming better which is noble. It follows Tamil language logical constructs in the code syntax which makes it easier to understand for a Tamil native.