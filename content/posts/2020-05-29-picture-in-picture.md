---
title: Make Picture In Picture Browser Window Visible On All Workspaces in i3
author: Subin Siby
type: post
date: 2020-05-29T00:00:00+00:00
lastmod: 2020-05-29T00:00:06+00:00
url: /i3-make-picture-in-picture-window-seen-on-all-workspaces
categories:
  - Tech
  - short post
  - Linux
tags:
  - i3wm
---

If you're using i3 window manager and you want the Picture in Picture (PiP) window of Firefox and Chromium to be seen in all workspaces, just add this to your i3 config file :

```text
for_window [title="Picture-in-Picture"] sticky enable
```

This will work for both Firefox and Chromium/Chrome PiP.

If it doesn't work, open a video in Picture in Picture mode and run the command `wmctrl -l` in terminal which will produce a similar output :

```
0x0b800006  1 swift Screenshots - Dolphin
0x07e00005  2 swift Telegram (278659)
0x03c00003  3 swift Make Picture In Picture Browser Window Seen On All Workspaces in i3 - Subins Blog - Mozilla Firefox
0x08c00007  3 swift Konsole
0x08600001  4 swift Akkara Kazhchakal Ep 42 (Part A) - YouTube - Chromium
0x08c00003  3 swift Akkara Kazhchakal Ep 42 (Part B) - YouTube - Mozilla Firefox
0x03c008e7 -1 swift Picture-in-Picture
0x03c008e7 -1 swift Picture in picture
```

You can see the title of the PiP window there. For me, it was `Picture-in-Picture` for Firefox (v76) and `Picture in picture` for Chromium (v81). Use this obtained title in the i3 config above.

Sticky windows are windows that is visible on all workspaces. [Read more about sticky windows in i3](https://i3wm.org/docs/userguide.html#_sticky_floating_windows).

You can see my [i3 config](https://github.com/subins2000/dotfiles/blob/master/i3/.config/i3/config) and [other dotfiles here](//github.com/subins2000/dotfiles).
