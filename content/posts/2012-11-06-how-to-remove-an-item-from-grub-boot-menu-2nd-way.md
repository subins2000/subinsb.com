---
title: How to remove an item from GRUB boot menu 2nd Way.
author: Subin Siby
type: post
date: 2012-11-06T12:38:00+00:00
excerpt: 'There is another way to remove items from GRUB menu. By deleting the files in /boot. To remove the item in the GRUB do as shown below.Open Terminal (ALT + CTRL + T)Enter the Command.gedit /boot/grub/grub.cfgNow search for&nbsp;### BEGIN /etc/grub.d/10_...'
url: /how-to-remove-an-item-from-grub-boot-menu-2nd-way
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/11/how-to-remove-item-from-grub-boot-menu.html
syndication_item_hash:
  - b27f2163160d99e092f0d2878b24bd3c
  - b27f2163160d99e092f0d2878b24bd3c
  - b27f2163160d99e092f0d2878b24bd3c
  - b27f2163160d99e092f0d2878b24bd3c
  - b27f2163160d99e092f0d2878b24bd3c
  - b27f2163160d99e092f0d2878b24bd3c
categories:
  - Linux
  - Ubuntu
tags:
  - GRUB

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  There is another way to remove items from <b>GRUB</b> menu. By deleting the files in <b>/boot</b>. To remove the item in the GRUB do as shown below.</p> 
  
  <div>
    Open <b>Terminal </b>(<b>ALT + CTRL + T</b>)
  </div>
  
  <div>
    Enter the Command.
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; color: #222222; font-weight: bold; line-height: 16px;"><span style="font-family: inherit;">gedit /boot/grub/grub.cfg</span></span>
    </p>
  </blockquote>
  
  <div>
    <span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Now search for&nbsp;</span><span style="background-color: lime; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">### BEGIN /etc/grub.d/10_linux ###</span><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;"><b>.</b></span><br /><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">After that line you will see the items of your&nbsp;<b>GRUB </b>menu.</span>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-HJqIM2Gvlyw/UEQ1J6bIjjI/AAAAAAAABuo/6Ja_h0IJnnk/s1600/highlited.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="278" src="https://2.bp.blogspot.com/-HJqIM2Gvlyw/UEQ1J6bIjjI/AAAAAAAABuo/6Ja_h0IJnnk/s640/highlited.png" width="640" /></a>
  </div>
  
  <div>
    <span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Select which is the item you want to remove. Suppose we want to remove&nbsp;</span><span style="color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif;"><span style="font-size: 15px; line-height: 20px;"><b>Ubuntu, with Linux 2.6.32-30-generic</b>. For that find the line. and in between <b>{&#8230;..}</b>&nbsp;the characters find <b>initrd</b>&nbsp;and memorize the location after the text <b>initrd</b>.</span></span>
  </div>
  
  <div>
    <span style="color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif;"><span style="font-size: 15px; line-height: 20px;">Go to the location. For that do as below.</span></span>
  </div>
  
  <div>
    Open&nbsp;<b>Terminal&nbsp;</b>(<b>ALT + CTRL + T</b>)
  </div>
  
  <div>
    <b>Enter </b>the command.
  </div>
  
  <blockquote class="tr_bq">
    <p>
      sudo nautilus
    </p>
  </blockquote>
  
  <p>
    <a href="https://2.bp.blogspot.com/-gjnm6X5l7I4/UA6k4mtL05I/AAAAAAAABGA/b8fK3bZ5y8A/s1600/lpane.png" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" height="320" src="https://2.bp.blogspot.com/-gjnm6X5l7I4/UA6k4mtL05I/AAAAAAAABGA/b8fK3bZ5y8A/s320/lpane.png" width="72" /></a>A <b>root</b>&nbsp;<b>file browser </b>window will display.<br />Go to <b>/boot</b>&nbsp;folder and find the file with the name after <b>initrd</b>&nbsp;text you found above.<br /><b>Delete the file</b>. But be careful. If you delete the wrong file then you are in danger.<br /><b><span style="color: red;">So be Careful.</span></b></div>