---
title: Send Free E-Mails Using Mailgun API in PHP
author: Subin Siby
type: post
date: 2013-12-25T08:25:47+00:00
url: /send-free-emails-using-mailgun-api-in-php
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - PHP
tags:
  - API
  - Email

---
E-Mails increases your site&#8217;s traffic. E-Mails are used in websites that require E-Mail verification to Reset Passwords. There is already a **mail** function in **PHP**, but the E-Mails that are sent via **PHP**&#8216;s **mail** function goes to **SPAM** folder in most of the mail services. So, developers use **API** services to send E-Mails that gets received on user&#8217;s **Inbox**. Most of the **API**&#8216;s need money to use. If you are a starting developer, you need a **Free** service. In fact there is a free service provided by **Mailgun**. **10,000** E-Mails are free in every month. Isn&#8217;t that a good offer ? You can sign up free without entering any credit card details.  <img class="aligncenter" alt="" src="https://s3.amazonaws.com/tres-cms/mailgun_logo.png" /><span style="line-height: 1.5em;">They have their own class for sending E-Mail&#8217;s in </span><strong style="line-height: 1.5em;">PHP</strong><span style="line-height: 1.5em;">, but in this post I will be introducing you to a simple </span><strong style="line-height: 1.5em;">PHP</strong> <span style="line-height: 1.5em;">function (send_mail) that will send </span><strong style="line-height: 1.5em;">E-Mail</strong><span style="line-height: 1.5em;">&#8216;s through </span><strong style="line-height: 1.5em;">Mailgun</strong><span style="line-height: 1.5em;">&#8216;s API service vai </span><strong style="line-height: 1.5em;">cURL</strong><span style="line-height: 1.5em;">. I will also tell you the step by step instructions from signing up to sending mails.</span> Like any other services, you have to sign up to use the features of **Mailgun**. You can sign up @ <a href="https://mailgun.com/signup" target="_blank">https://mailgun.com/signup</a>. Fill up the required fields and click "Create Account" button. Now you need to get the **API Key**. Login to your new account. Note that you need to add your domain and verify your domain. If you don&#8217;t verify your domain, you will be limited to send only **300** mails per day. So you need to verify your domain. Go to the page <https://mailgun.com/cp/my_account>. On the left side, you can see the **API** key :<figure id="attachment_2165" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2165" alt="The API key is not real" src="//lab.subinsb.com/projects/blog/uploads/2013/12/0072-300x92.png" width="300" height="92" />][1]<figcaption class="wp-caption-text">The API key is not real</figcaption></figure> 

You will need this **API** key for future use in this tutorial. Let&#8217;s move on to the **send_mail** function :

<pre class="prettyprint"><code>&lt;?
function send_mail($email,$subject,$msg) {
 $api_key="";/* Api Key got from https://mailgun.com/cp/my_account */
 $domain ="";/* Domain Name you given to Mailgun */
 $ch = curl_init();
 curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
 curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$api_key);
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
 curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/'.$domain.'/messages');
 curl_setopt($ch, CURLOPT_POSTFIELDS, array(
  'from' =&gt; 'Open &lt;mail@youriste.com&gt;',
  'to' =&gt; $email,
  'subject' =&gt; $subject,
  'html' =&gt; $msg
 ));
 $result = curl_exec($ch);
 curl_close($ch);
 return $result;
}
?&gt;</code></pre>

Don&#8217;t forget to fill the following fields :   $api_key &#8211; API key got from <https://mailgun.com/cp/my_account> $domain &#8211; The domain name you gave in Mailgun (<https://mailgun.com/cp/my_account>).   You can now send E-Mails using **send_mail()** function with the values as below :

<pre class="prettyprint"><code>$email   - E-Mail address
$subject - Subject of E-Mail 
$msg     - Body of E-Mail  </code></pre>

&nbsp;

An example of sending email to **kerala@kerala.gov.in** with subject **Hello Kerala** and body as **Kerala is a nice place** :

<pre class="prettyprint"><code>&lt;?
send_mail("kerala@kerala.gov.in", "Hello Kerala", "Kerala is a nice place.");
?&gt;</code></pre>

If you encountered with any problems or have a feedback, please say it on the comments. I love to hear from you and **Merry Christmas**.

 [1]: //lab.subinsb.com/projects/blog/uploads/2013/12/0072.png