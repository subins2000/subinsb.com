---
title: 'E4KS : The Great Work Done By Rajeev Joseph'
author: Subin Siby
type: post
date: 2013-10-06T05:10:00+00:00
excerpt: 'There are millions of bloggers&nbsp;and blogs&nbsp;in the World Wide Web (WWW).&nbsp;90 %&nbsp;of those blogs are useful. There are a lot of people who would visit those blogs. Since there are a lot of viewers, most of the blog&nbsp;owners would set up...'
url: /e4ks-the-great-work-done-by-rajeev-joseph
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - a0a89dd2c5a68e7e7c16733fa81c52a9
  - a0a89dd2c5a68e7e7c16733fa81c52a9
  - a0a89dd2c5a68e7e7c16733fa81c52a9
  - a0a89dd2c5a68e7e7c16733fa81c52a9
  - a0a89dd2c5a68e7e7c16733fa81c52a9
  - a0a89dd2c5a68e7e7c16733fa81c52a9
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
  - http://sag-3.blogspot.com/2013/10/e4ks-great-work-done-by-rajeev-joseph.html
dsq_thread_id:
  - 1964594412
  - 1964594412
  - 1964594412
  - 1964594412
  - 1964594412
  - 1964594412
categories:
  - Blogger
tags:
  - Blog
  - Recommends

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  There are millions of <b>bloggers</b>&nbsp;and <b>blogs</b>&nbsp;in the <b>World Wide Web </b>(WWW).&nbsp;<b>90 %</b>&nbsp;of those blogs are useful. There are a lot of people who would visit those blogs. Since there are a lot of viewers, most of the <b>blog</b>&nbsp;owners would set up <b>ads</b>&nbsp;on their blog to get maximum revenue. Most of the <b>Bloggers</b>&nbsp;(including me) only set up <b>blogs</b>&nbsp;just to get <b>revenue</b>&nbsp;from it. I&#8217;m gonna introduce you to a man that has helped a lot of students (including me) of <b>Kerala</b>. His name is <b>Rajeev Joseph</b>. Check out his <b>Google&nbsp;</b><b>+ </b>profile <a href="https://plus.google.com/104655132380874995884" >here</a>. And his blog is <b><a href="http://www.english4keralasyllabus.com/" >English4KeralaSyllabus</a>&nbsp;:</b></p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-wXvTkO38-CA/UlDtRgfkSnI/AAAAAAAADFI/FzOPLkQuC3g/s1600/0055.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-wXvTkO38-CA/UlDtRgfkSnI/AAAAAAAADFI/FzOPLkQuC3g/s1600/0055.png" /></a>
  </div>
  
  <p>
    As you can see it&#8217;s a typical blog with a lot of posts and pages. But what you won&#8217;t see are <b>ads</b>&nbsp;!<br />Yes, He didn&#8217;t put up ads on his blog. With Page Views like that, he could have made thousands of <b>Dollars</b>. In the field of education in <b>Kerala</b>, this man has helped a lot of students (including me). That&#8217;s one of the reasons why the blog got a lot of Page Views.<br />He could have made thousands of <b>Dollars</b>&nbsp;by monetizing with ads, but he didn&#8217;t. He don&#8217;t even ask for donations. This blog is the best example for "<span style="background-color: white; color: #444444; font-family: arial, sans-serif; font-size: x-small; line-height: 16px;">giving&nbsp;</span><em style="background-color: white; color: #444444; font-family: arial, sans-serif; font-size: small; font-style: normal; font-weight: bold; line-height: 16px;">without expecting&nbsp;</em><span style="background-color: white; color: #444444; font-family: arial, sans-serif; font-size: x-small; line-height: 16px;">anything in return</span>" also known as <b>generosity</b>.<br />Another Example of <b>generosity </b>is <b>Wikipedia</b>. Actually <b>E4KS </b>(<b><a href="http://www.english4keralasyllabus.com/" >English4KeralaSyllabus</a></b>) is like <b>Wikipedia</b>.<br />Thank You <b>Rajeev</b>&nbsp;sir for making the <b>blog</b>&nbsp;ad-free and keep continuing the good work.</div>