---
title: The Francium Project
author: Subin Siby
type: post
date: 2015-03-24T06:51:19+00:00
url: /the-francium-project
categories:
  - Program

---
I have made lots of classes and plan to create more and more. But, all of these are just scrambled all over. Here&#8217;s what I&#8217;m talking about :

<pre class="prettyprint"><code>new LoginSystem();
new FDM();</code></pre>

If you use another class named the same, then it will bring conflicts within your code. So, I decided them to group it with the namespace **Fr**, short for **Francium** &#8211; one of the least electronegative elements in the periodic table. I&#8217;m calling this the **Francium** project. It has nothing to do with the chemical element though.

As per this project, all my current and future classes will become a child class of the **Fr** parent :

<pre class="prettyprint"><code>new Fr\LoginSystem();
new Fr\FDM()</code></pre>

This post is about how you work with **Francium**. You can see the base class <a href="https://github.com/subins2000/Francium/blob/master/class.php" target="_blank">here</a>.


## Configuration

There is a static variable inside it called "default_config" containing an array with the default configuration. In that array, database information and a main info about who is using the software is written. You can either change the default configuration or set custom ones without changing the default config.

### 1. Custom Config

For this, you pass your config array to the **config()** method. You do this by making another file like "config.php" and including it instead of the class file. Here is an example of "**config.php**" file :

<pre class="prettyprint"><code>require "ClassName.php";
new Fr\ClassName(array(
  "info" =&gt; array(
    "company" =&gt; "Test",
    "email" =&gt; "test@company.com"
  )
));</code></pre>

Then, you include the above file in pages :

<pre class="prettyprint"><code>require_once "&lt;strong>config.php&lt;/strong>"
</code></pre>

I recommend this custom config, because you can then easily update the class when a new version come out.

### 2. Change Default Config

You can directly change the values of the "\Fr\ClassName::$default_config" in the class file, so you won&#8217;t need to make another file for custom configuration. But beware, when you&#8217;re updating to a newer version of the class, be sure to reupdate the default configuration again.

Now, what Fr does is merge the default config with the config you provided when making the object.

## Debugging

\Fr\ClassName::log() function is used for entering log messages which is useful for debugging. These messages are sent by various functions and it is recorded in a file called "**Francium.log**".

To enable log messages, create a file named "**Francium.log**" in the **folder where the class file exists**. You should also make the file **writable** by changing the permissions.

Then set config->debug->enable to TRUE :

<pre class="prettyprint"><code>new Fr\ClassName(array(
  "debug" =&gt; array(
    "enable" =&gt; true
  )
));</code></pre>

&nbsp;

You can also change the path of log file by giving **absolute** path of the new log file to config->debug->log_file :

<pre class="prettyprint"><code>new Fr\ClassName(array(
  "debug" =&gt; array(
    "enable" =&gt; true,
    "log_file" =&gt; __DIR__ . "/custom-log-file.log"
  )
));
</code></pre>

After doing the above, log messages will be written to the file automatically.

Here is an example of a log message :

<pre class="prettyprint"><code>[2015-03-28 20:41:24] Couldn't connect to database. Check config-&gt;db credentials
[2016-04-23 12:13:41] two_step_login: Token Callback not present
</code></pre>

It is also to be noted that PHP&#8217;s error log will also be helpful in debugging. When you enable debugging in a Francium class, it also does this :

<pre class="prettyprint"><code>ini_set("display_errors", "on");</code></pre>

It&#8217;s recommended that you see both the logs for perfect debugging.

## JavaScript

Francium Project also applies to my JavaScrpt projects. My current projects are being slowly ported to the Francium Object. Note that my **jQuery** plugins will also be moving to **Francium** object. Example :

<pre class="prettyprint"><code>Fr.voice.record();</code></pre>

The debugging is not currently set for JS projects, but some messages are shown in console.

 [1]: #configuration
 [2]: #1-custom-config
 [3]: #2-change-default-config
 [4]: #debugging
 [5]: #javascript
