---
title: How To Remove Smart Quotes From Posts In WordPress
author: Subin Siby
type: post
date: 2013-11-18T16:04:45+00:00
url: /how-to-remove-smart-quotes-from-posts-in-wordpress
authorsure_include_css:
  - 
  - 
  - 
categories:
  - WordPress

---
If your **WordPress** blog contains **code** snippets, **WordPress** will make the quotes in the codes look fancy. The quotes will become something like **‘ **and **’**. If you can&#8217;t see it well, here it is :

## **‘ **’

When a reader of your blog copies your code, the special characters (quotes) will be copied with the code instead of the real code quotes. When this happens the code won&#8217;t work. This happened to me yesterday. I immediately tried to find a solution and I found the perfect solution. I&#8217;m going to share that perfect solution. There is a plugin named **Unfancy** **Quotes** on **WordPress**, but it didn&#8217;t do the job. Here&#8217;s what I did to remove **Fancy** **Quotes**.

Go to your **Theme** **Editor** which is available @ **Appearance **-> **Editor**.

On the right side which contains the name of the files of your theme, choose **Theme Functions (functions.php)**.

Add the following code to the **functions.php** file :

> remove\_filter(&#8216;the\_content&#8217;, &#8216;wptexturize&#8217;);
> 
> remove\_filter(&#8216;comment\_text&#8217;, &#8216;wptexturize&#8217;);
> 
> remove\_filter (&#8216;single\_post_title&#8217;, &#8216;wptexturize&#8217;);
> 
> remove\_filter (&#8216;the\_title&#8217;, &#8216;wptexturize&#8217;);
> 
> remove\_filter (&#8216;wp\_title&#8217;, &#8216;wptexturize&#8217;);

And save the file by clicking **Update File** below the **Editor** textarea. Now, when you go to your blog posts you won&#8217;t see any **Fancy Quotes**.