---
title: Jupiter and Venus near the moon today!!
author: Subin Siby
type: post
date: 2012-02-26T13:43:00+00:00
excerpt: 'Today 26 February the Jupiter and Venus are near the moon. you can see the two planets near the moon.See the sky and find the moon. You can see two bright stars. One is Jupiter and other one is Venus.&nbsp;Enjoy!!!!!Here is a picture of the moon and th...'
url: /jupiter-and-venus-near-the-moon-today
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - e3eefe4d4d11511e0e7d2ed2ca86f522
  - e3eefe4d4d11511e0e7d2ed2ca86f522
  - e3eefe4d4d11511e0e7d2ed2ca86f522
  - e3eefe4d4d11511e0e7d2ed2ca86f522
  - e3eefe4d4d11511e0e7d2ed2ca86f522
  - e3eefe4d4d11511e0e7d2ed2ca86f522
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
  - http://sag-3.blogspot.com/2012/02/jupiter-and-venus-near-moon-today.html
tags:
  - Sky

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Today 26 February the Jupiter and Venus are near the moon. you can see the two planets near the moon.</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-OeLd7mJUq94/T0o2XAElDTI/AAAAAAAAAh0/0cDu-zg7ALU/s1600/Screenshot-3.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-OeLd7mJUq94/T0o2XAElDTI/AAAAAAAAAh0/0cDu-zg7ALU/s1600/Screenshot-3.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    See the sky and find the moon. You can see two bright stars. One is Jupiter and other one is Venus.&nbsp;
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    Enjoy!!!!!
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    Here is a picture of the moon and the planets seen in the west side of the sky
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-FdKqOGbVLwg/T0uOAch8IUI/AAAAAAAAAiA/Sb5DlSMokZE/s1600/Screenshot-4.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-FdKqOGbVLwg/T0uOAch8IUI/AAAAAAAAAiA/Sb5DlSMokZE/s1600/Screenshot-4.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <p>
    </div>