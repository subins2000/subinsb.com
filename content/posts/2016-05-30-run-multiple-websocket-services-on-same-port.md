---
title: Run Multiple WebSocket Services On Same Port
author: Subin Siby
type: post
date: 2016-05-30T17:28:51+00:00
url: /run-multiple-websocket-services-on-same-port
categories:
  - Francium
  - HTML5
  - JavaScript
  - PHP
  - Program
  - WebSocket
tags:
  - composer
  - library
  - WebSocket

---
Are you as crazy as me when developing **WebSocket** applications ? It&#8217;s so awesome to work with **WebSockets** that will blow up your mind and irritate you at the same time.

**WebSockets** can be used for any real time applications such as :

  * Games
  * Chat
  * News
  * File Transfer

And because it&#8217;s impact is so vast, we can develop as many applications as we want. But, a problem arises for freebies like me and you.


## Problem

Running multiple **WebSocket** servers for multiple apps requires you to run a listening node on different ports. But, on some hosting servers, this isn&#8217;t possible.

In case of **OpenShift**, this is impossible as there&#8217;s only one port in the app where you listen for incoming data ie **8080**. <a href="https://developers.openshift.com/managing-your-applications/port-binding-routing.html" target="_blank">See Port Binding On Openshift</a>.

In this scenario, your only solution is to listen on a single port and send out data according to the service the client requested.

## Francium DiffSocket

And for this purpose, I developed a small **Composer** package called **Francium DiffSocket**, an abbreviation for :

<pre class="prettyprint"><code>Francium Different Sockets</code></pre>

What it does is, allow you to add your own custom services and serve it through a single port. This means you can host your **Online game server**, **Chat server** etc. on a single port.

## Usage

Instructions on how to use it is in the [README file of the project][4].

## Demo

All of the WebSocket demos that I have created uses **Francium DiffSocket**. List :

  * [Finding Value Of Pi][6]
  * [Advanced Live Group Chat With PHP, jQuery & WebSocket][7]
  * [Live Group Chat With PHP, jQuery & WebSocket][8]
  * [Online Chess Game][9]

## Example

Let us make three different services :

  * A service that responds "hello" to incoming messages
  * A service that responds the incoming message itself (mimic)
  * A service that responds "hey, how are you ?" when "hi" is sent (bot)

We make three separate class files for the three services. These are stored in a folder called "services".

### Hello Service

Location &#8211; services/Hello.php

<pre class="prettyprint"><code>namespace Fr\DiffSocket\Service;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Hello implements MessageComponentInterface {
  public function onOpen(ConnectionInterface $conn){
    echo "New Connection - " . $conn-&gt;resourceId;
  }

  public function onClose(ConnectionInterface $conn){}
  public function onError(ConnectionInterface $conn,\Exception $error){}

  public function onMessage(ConnectionInterface $conn, $message){
    $conn-&gt;send("Hello");
  }
}
</code></pre>

The API used to build the service is [Ratchet][12] as mentioned in the README file.

### Mimic Service

Location &#8211; services/Mimic.php

<pre class="prettyprint"><code>namespace Fr\DiffSocket\Service;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Mimic implements MessageComponentInterface {
  public function onOpen(ConnectionInterface $conn){
    echo "New Connection - " . $conn-&gt;resourceId;
  }

  public function onClose(ConnectionInterface $conn){}
  public function onError(ConnectionInterface $conn,\Exception $error){}

  public function onMessage(ConnectionInterface $conn, $message){
    $conn-&gt;send($message);
  }
}</code></pre>

### Bot Service

Location &#8211; services/Bot.php

<pre class="prettyprint"><code>namespace Fr\DiffSocket\Service;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Bot implements MessageComponentInterface {
  public function onOpen(ConnectionInterface $conn){
    echo "New Connection - " . $conn-&gt;resourceId;
  }

  public function onClose(ConnectionInterface $conn){}
  public function onError(ConnectionInterface $conn, \Exception $error){}

  public function onMessage(ConnectionInterface $conn, $message){
    if($message === "hi")
      $conn-&gt;send("hey, how are you ?");
  }
}</code></pre>

Okay, all of the services are made.

## Run The Server

Now, we must make the code to integrate the services into **Francium DiffSocket** and then run the server.

<pre class="prettyprint"><code>require_once "vendor/autoload.php";
$DS = new \Fr\DiffSocket(array(
  "server" =&gt; array(
    "host" =&gt; "127.0.0.1",
    "port" =&gt; "8000"
  ),
  "service" =&gt; array(
    "hello" =&gt; __DIR__ . "/services/Hello.php",
    "mimic" =&gt; __DIR__ . "/services/Mimic.php",
    "bot" =&gt; __DIR__ . "/services/Bot.php"
  )
));</code></pre>

You can add more services by calling the **addService()** method too :

<pre class="prettyprint"><code>$DS-&gt;addService("service-name", "path/to/class.php");
</code></pre>

Now, we can run the server :

<pre class="prettyprint"><code>$DS-&gt;run();
</code></pre>

That&#8217;s it ! When the server is started, this message will be printed out :

<pre class="prettyprint"><code>Server started on 127.0.0.1:8000
</code></pre>

You can connect to the different services in **JavaScript** like so :

<pre class="prettyprint"><code>var hello = new WebSocket("ws://localhost:8000/?service=hello");
hello.send("nothing"); // Server will respond "Hello"

var mimic = new WebSocket("ws://localhost:8000/?service=mimic");
mimic.send("Hola!"); // Server will respond "Hola!"

var bot = new WebSocket("ws://localhost:8000/?service=bot");
bot.send("hi"); // Server will respond "hey, how are you ?"
</code></pre>

Use and hack the project all you want. It is licensed under the "Apache License".

 [1]: #problem
 [2]: #francium-diffsocket
 [3]: #usage
 [4]: https://github.com/subins2000/Francium-DiffSocket/blob/master/README.md
 [5]: #demo
 [6]: http://demos.subinsb.com/pi/
 [7]: http://demos.subinsb.com/php/advanced-chat-websocket/
 [8]: http://demos.subinsb.com/php/websocketChat
 [9]: https://lobby.subinsb.com/apps/chess
 [10]: #example
 [11]: #hello-service
 [12]: https://github.com/cboden/ratchet
 [13]: #mimic-service
 [14]: #bot-service
 [15]: #run-the-server
