---
title: Setting up Google DNS in Ubuntu
author: Subin Siby
type: post
date: 2013-09-12T17:01:00+00:00
excerpt: "Google's DNS&nbsp;service is spreading rapidly. It's time for you to use it. I used it and noticed some great speed changes even though my internet is just a dial up connection. Here is how to set up Google's DNS&nbsp;on your Ubuntu Linux.Open Network ..."
url: /setting-up-google-dns-in-ubuntu
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
  - http://sag-3.blogspot.com/2013/09/setting-up-google-dns-in-ubuntu.html
syndication_item_hash:
  - 9460dd5f2431ec81c10351dd5947b1ea
  - 9460dd5f2431ec81c10351dd5947b1ea
  - 9460dd5f2431ec81c10351dd5947b1ea
  - 9460dd5f2431ec81c10351dd5947b1ea
  - 9460dd5f2431ec81c10351dd5947b1ea
  - 9460dd5f2431ec81c10351dd5947b1ea
categories:
  - Linux
  - Ubuntu
tags:
  - DNS
  - Google
  - IP
  - NET
  - Network
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>Google</b>&#8216;s <b>DNS</b>&nbsp;service is spreading rapidly. It&#8217;s time for you to use it. I used it and noticed some great speed changes even though my internet is just a dial up connection. Here is how to set up <b>Google</b>&#8216;s <b>DNS</b>&nbsp;on your <b>Ubuntu Linux</b>.<br />Open Network Settings (network-admin) by going to <b>System -> Preferences -> Network Connections</b>. Go to your default net connection and click on <b>Edit</b>&nbsp;button.</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-r18b5XEvKX0/UjHq_ZQEt7I/AAAAAAAAC_s/QXrT-bIgfgs/s1600/0040.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-r18b5XEvKX0/UjHq_ZQEt7I/AAAAAAAAC_s/QXrT-bIgfgs/s1600/0040.png" /></a>
  </div>
  
  <p>
    You will get a window like below :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-XVdhIwVntMk/UjHraKB3TMI/AAAAAAAAC_0/ufI_wVjx7FY/s1600/0041.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-XVdhIwVntMk/UjHraKB3TMI/AAAAAAAAC_0/ufI_wVjx7FY/s1600/0041.png" /></a>
  </div>
  
  <p>
    Change the Method option to <b>Automatic (PPP) addresses only</b>. Then change the <b>DNS servers</b>&nbsp;field to the following text(IP addresses):
  </p>
  
  <blockquote class="tr_bq">
    <p>
      8.8.8.8, 8.8.4.4
    </p>
  </blockquote>
  
  <p>
    Finally, click on <b>Apply</b> button. Now the settings have been saved. To try the new settings either disconnect the active internet connections and start a new connection or reboot your computer.</div>