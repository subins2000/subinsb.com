---
title: Add GET or POST parameters on include files in PHP
author: Subin Siby
type: post
date: 2013-07-13T03:30:00+00:00
excerpt: "If you want to add GET&nbsp;parameters or POST&nbsp;parameters to files loading using include&nbsp;function in PHP&nbsp;then you should know the following:It's not possible. But it is in a different way.If you are adding GET parameters to file name lik..."
url: /add-get-or-post-parameters-on-include-files-in-php
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1959548410
  - 1959548410
  - 1959548410
  - 1959548410
  - 1959548410
  - 1959548410
syndication_item_hash:
  - 51bb8d719ad12e95bab2fb3fae5867bb
  - 51bb8d719ad12e95bab2fb3fae5867bb
  - 51bb8d719ad12e95bab2fb3fae5867bb
  - 51bb8d719ad12e95bab2fb3fae5867bb
  - 51bb8d719ad12e95bab2fb3fae5867bb
  - 51bb8d719ad12e95bab2fb3fae5867bb
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
  - http://sag-3.blogspot.com/2013/07/get-post-parameters-to-include-files-php.html
categories:
  - PHP
tags:
  - Function
  - GET
  - POST

---
<div dir="ltr" style="text-align: left;">
  If you want to add <b>GET</b> parameters or <b>POST</b> parameters to files loading using <b>include</b> function in <b>PHP</b> then you should know the following:</p> 
  
  <h4 style="text-align: left;">
    <span style="color: red; font-size: large;">It&#8217;s not possible. But it is in a different way.</span>
  </h4>
  
  <p>
    If you are adding <b>GET </b>parameters to file name like the following, then it won&#8217;t work:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <?php include(&#8216;file.php?user=subin&#8217;);?>
    </p>
  </blockquote>
  
  <p>
    The above code won&#8217;t load the file, because PHP Include function only looks for files, it won&#8217;t send any parameters. It will search for <b>file.php?user=subin</b> in the directory for the case above. You can make it work in a different way.
  </p>
  
  <p>
    We use <b>$_GET</b> and <b>$_POST</b> to get the parameters sent for each type. You should know that these variables are <b>Arrays</b> and not <b>Strings</b>. So you can just add a <b>key</b> and <b>value</b> to the array.
  </p>
  
  <p>
    So to add the key and value just use the following code along with the <b>include</b> function :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <?php<br /> $_GET[&#8216;user&#8217;]="Subin";<br /> include(&#8216;file.php&#8217;);<br /> ?>
    </p>
  </blockquote>
  
  <p>
    The above code works with the same as <b>$_POST</b>. When the <b>file.php</b> is loaded you will get the parameters using <b>$_GET</b> or <b>$_POST</b>.
  </p>
</div>