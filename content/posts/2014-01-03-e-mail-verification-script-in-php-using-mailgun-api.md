---
title: E-Mail Verification Script In PHP Using Mailgun API
author: Subin Siby
type: post
date: 2014-01-03T15:47:12+00:00
url: /e-mail-verification-script-in-php-using-mailgun-api
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - HTML
  - PHP
tags:
  - API
  - Function
  - header
  - Mailgun
  - Script
  - Session
  - Tutorials

---
As years goes, so does the increase in **SPAM** on the web. Most users sign up on services with a fake E-Mail. They might even use other&#8217;s E-Mail for signing up. This is a big problem, because when you send an email to the signed up user, it goes to the wrong guy. So, you should verify user&#8217;s email before signing them up. You might think that it would cost **database** memory. Don&#8217;t worry, because my script won&#8217;t require storage of verification codes on database.

<div class="padlinks">
  <a class="demo" href="http://open.subinsb.com/register">DEMO</a>
</div>


# Dependencies

  1. Mailgun API key
  2. send_mail() function which you will get from <a title="Send Free E-Mails Using Mailgun API in PHP" href="//subinsb.com/2013/12/send-free-emails-using-mailgun-api-in-php" target="_blank">here</a>.
  3. rantext() function which you will get from <a title="Generating Random Text in PHP" href="//subinsb.com/2013/08/generating-random-text-in-php" target="_blank">here</a>.
  4. encrypter() function which you will get from <a title="Encryption & Decryption Using Mcrypt In PHP" href="//subinsb.com/2013/12/secure-encryption-decryption-using-mcrypt-php" target="_blank">here</a>.
  5. decrypter() function which you will get from <a title="Encryption & Decryption Using Mcrypt In PHP" href="//subinsb.com/2013/12/secure-encryption-decryption-using-mcrypt-php" target="_blank">here</a>.
  6. session_start() function which should be added on the starting of every page we are going to create

We have three pages :

  1. register.php
  2. verify.php
  3. config.php

# config.php

This file contains the configuration variables including **database** connection and the **checkVerification() **function which check whether the user has verified his/her email. Example :

> <?
  
> $db="database";
  
> $host="127.0.0.1&#8243;;
  
> $usr="Username";
  
> $pass="Password";
  
> $dbh=new&nbsp;PDO("mysql:dbname=$db;host=$host",&nbsp;$usr,&nbsp;$pass);
  
> /\*&nbsp;E-Mail&nbsp;Verification&nbsp;Configuration&nbsp;&#8211;&nbsp;START\*/
  
> $secure_code="";/\*&nbsp;A&nbsp;Security&nbsp;Code.&nbsp;Should&nbsp;only&nbsp;contain&nbsp;numerals&nbsp;and&nbsp;alphabetic&nbsp;characters&nbsp;\*/
  
> /\*&nbsp;E-Mail&nbsp;Verification&nbsp;Configuration&nbsp;&#8211;&nbsp;END\*/
  
> function&nbsp;checkVerification(){
  
> &nbsp;global&nbsp;$_SESSION;
  
> &nbsp;global&nbsp;$_POST;
  
> &nbsp;$code=$\_POST[&#8216;code&#8217;]==""&nbsp;?&nbsp;$\_SESSION[&#8216;code&#8217;]&nbsp;:&nbsp;"subinsb.com";
  
> &nbsp;if($_SESSION["token"]==decrypter($code)){
  
> &nbsp;&nbsp;return&nbsp;true;
  
> &nbsp;}else{
  
> &nbsp;&nbsp;return&nbsp;false;
  
> &nbsp;}
  
> }
  
> ?>

# register.php

This file contains the sign up form. If the email isn&#8217;t verified, the page will redirect to **verify.php** which shows verify form. If email&#8217;s verified, then the page will show other fields like name, age, location etc&#8230;. (this is represented as **/* Other form fields** */ in the code** **).

The following code is the whole script which displays the form for every action.

> <?
  
> include("config.php");
  
> if(checkVerification()==false){
  
> header("Location: /verify.php");
  
> }else{
  
> /\* Other Form Fields. Add Other Form Fields Here. Example : \*/
  
> ?>
  
> <form action="register.php" method="POST">
  
> Name :
  
> <input type="text" name="name"/><br/>
  
> <input type="submit" value="Submit" name="submit"/>
  
> </form>
  
> <?
  
> }
  
> if(isset($_POST[&#8216;submit&#8217;])){
  
> // Process Form Submission and finally destroy the session
  
> session_destroy();
  
> }
  
> ?>

# verify.php

This file processes the verification stuff. If the user has verified the email then this page will redirect to **register.php** :

> <?
  
> include("config.php");
  
> if(isset($\_POST[&#8216;verify&#8217;])&nbsp;&&&nbsp;isset($\_POST[&#8216;code&#8217;])){
  
> &nbsp;$code=$_POST[&#8216;code&#8217;];
  
> &nbsp;if($code==""){
  
> &nbsp;&nbsp;die("<h2>No&nbsp;Code&nbsp;Entered.</h2><div>Please&nbsp;Enter&nbsp;A&nbsp;Code</div>");
  
> &nbsp;}
  
> &nbsp;if(checkVerification()==false){
  
> &nbsp;&nbsp;die("<h2>Wrong&nbsp;Verification&nbsp;Code.</h2>");
  
> &nbsp;}
  
> &nbsp;$\_SESSION[&#8216;code&#8217;]=$\_POST[&#8216;code&#8217;];
  
> }
  
> if(isset($\_POST[&#8216;verify&#8217;])&nbsp;&&&nbsp;isset($\_POST[&#8216;mail&#8217;])){
  
> &nbsp;/\*&nbsp;Check&nbsp;values&nbsp;and&nbsp;send&nbsp;verification&nbsp;code&nbsp;to&nbsp;email&nbsp;\*/
  
> &nbsp;$email=$_POST[&#8216;mail&#8217;];
  
> &nbsp;$password="";/\*&nbsp;A&nbsp;key&nbsp;that&nbsp;is&nbsp;\*/
  
> &nbsp;if(!preg\_match(&#8216;/^[a-zA-Z0-9]+[a-zA-Z0-9\_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/&#8217;,$email)){
  
> &nbsp;&nbsp;die("<h2>E-Mail&nbsp;Is&nbsp;Not&nbsp;Valid</h2>");
  
> &nbsp;}
  
> &nbsp;/\*&nbsp;You&nbsp;can&nbsp;add&nbsp;the&nbsp;checking&nbsp;if&nbsp;the&nbsp;email&nbsp;exist&nbsp;on&nbsp;database&nbsp;in&nbsp;this&nbsp;section&nbsp;\*/
  
> &nbsp;$highcode=encrypter($\_POST[&#8216;mail&#8217;].$secure\_code);
  
> &nbsp;$_SESSION["token"]=$highcode;
  
> &nbsp;send\_mail($email,"Verify&nbsp;Your&nbsp;E-Mail","You&nbsp;requested&nbsp;for&nbsp;registering&nbsp;on&nbsp;".$\_SERVER[&#8216;HTTP_HOST&#8217;].".&nbsp;For&nbsp;signing&nbsp;up,&nbsp;you&nbsp;need&nbsp;to&nbsp;verify&nbsp;your&nbsp;E-Mail&nbsp;address.&nbsp;Paste&nbsp;the&nbsp;code&nbsp;below&nbsp;in&nbsp;the&nbsp;input&nbsp;field&nbsp;of&nbsp;the&nbsp;page&nbsp;where&nbsp;you&nbsp;requested&nbsp;for&nbsp;signing&nbsp;up.<blockquote>".$highcode."</blockquote>");
  
> ?>
  
> <div>An&nbsp;E-Mail&nbsp;containing&nbsp;a&nbsp;code&nbsp;have&nbsp;been&nbsp;sent&nbsp;to&nbsp;the&nbsp;E-Mail&nbsp;address&nbsp;you&nbsp;gave&nbsp;us.&nbsp;Check&nbsp;Your&nbsp;Inbox&nbsp;for&nbsp;that&nbsp;mail.&nbsp;The&nbsp;mail&nbsp;might&nbsp;have&nbsp;went&nbsp;to&nbsp;the&nbsp;SPAM&nbsp;folder.&nbsp;Hence&nbsp;you&nbsp;have&nbsp;to&nbsp;check&nbsp;that&nbsp;folder&nbsp;too.</div><br/>
  
> <form&nbsp;action="verify.php"&nbsp;method="POST">
  
> &nbsp;Paste&nbsp;The&nbsp;Code&nbsp;you&nbsp;received&nbsp;via&nbsp;E-Mail&nbsp;below<br/>
  
> &nbsp;<input&nbsp;name="code"&nbsp;style="width:290px;"&nbsp;autocomplete="off"&nbsp;placeholder="Paste&nbsp;The&nbsp;Code&nbsp;Here"&nbsp;type="text"/><br/>
  
> &nbsp;<input&nbsp;name="verify"&nbsp;type="submit"&nbsp;value="Complete&nbsp;Verification"/><br/>
  
> </form>
  
> <?
  
> }elseif(checkVerification()==false){
  
> ?>
  
> <form&nbsp;action="verify.php"&nbsp;method="POST">
  
> &nbsp;Type&nbsp;In&nbsp;Your&nbsp;E-Mail&nbsp;To&nbsp;Continue&nbsp;Signup&nbsp;Process.<br/>
  
> &nbsp;<input&nbsp;name="mail"&nbsp;style="width:290px;"&nbsp;placeholder="Don&#8217;t&nbsp;You&nbsp;Have&nbsp;An&nbsp;E-Mail&nbsp;?"&nbsp;type="text"/><br/>
  
> &nbsp;<input&nbsp;name="verify"&nbsp;type="submit"&nbsp;value="Verify&nbsp;E-Mail"/><br/>
  
> &nbsp;You&nbsp;can&nbsp;only&nbsp;sign&nbsp;up&nbsp;if&nbsp;you&nbsp;verify&nbsp;your&nbsp;email.
  
> </form>
  
> <?
  
> }elseif(checkVerification()==true){
  
> &nbsp;header("Location:&nbsp;/register.php");
  
> }
  
> ?>

This post is very long and it&#8217;s possible that I made errors on some places. So if you find any errors, or had errors when you run this script, please report it via comments. I will be glad to help you.

 [1]: #dependencies
 [2]: #configphp
 [3]: #registerphp
 [4]: #verifyphp
