---
title: How to make sure that a CSS file will load on your site using PHP
author: Subin Siby
type: post
date: 2013-04-15T13:50:00+00:00
excerpt: "This is a simple trick. Most of the web developers worry that the Stylesheet file will not load completely. This happens if your CSS&nbsp;file's size is too large. The fail in complete loading of the file mostly happens to users who are using slower co..."
url: /how-to-make-sure-that-a-css-file-will-load-on-your-site-using-php
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 888612483a5f0c85da7830a93f45b209
  - 888612483a5f0c85da7830a93f45b209
  - 888612483a5f0c85da7830a93f45b209
  - 888612483a5f0c85da7830a93f45b209
  - 888612483a5f0c85da7830a93f45b209
  - 888612483a5f0c85da7830a93f45b209
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-sure-that-css-file-will.html
categories:
  - CSS
  - PHP

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This is a simple trick. Most of the web developers worry that the Stylesheet file will not load completely. This happens if your <b>CSS</b>&nbsp;file&#8217;s size is too large. The fail in complete loading of the file mostly happens to users who are using slower connections like me.</p> 
  
  <div>
  </div>
  
  <div>
    The page will look disgusting if the <b>CSS</b>&nbsp;file don&#8217;t load. To avoid the file not getting loaded you can do this simple trick using <b>PHP</b>.
  </div>
  
  <div>
  </div>
  
  <div>
    Open your <b>PHP</b>&nbsp;page and replace the lines that embed the stylesheet file.
  </div>
  
  <div>
    <span style="font-size: large;"><b>Example:</b></span>
  </div>
  
  <div>
    Replace this:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <link href="<a href="https://subins.hp.af.cm/files/css/all.php"><span style="color: red;">https://subins.hp.af.cm/files/css/all.php</span></a>" rel="stylesheet" />
    </p>
  </blockquote>
  
  <div>
    With this:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <style><?echo file_get_contents(&#8216;<a href="https://subins.hp.af.cm/files/css/all.php"><span style="color: red;">https://subins.hp.af.cm/files/css/all.php</span></a>&#8216;);?></style>
    </p>
  </blockquote>
  
  <div>
    When the page loads the <b>CSS</b>&nbsp;styles of the external file will be loaded in the <b><style></b>&nbsp;tag. This method won&#8217;t fail. <b>PHP</b>&nbsp;make sure that the <b>CSS</b>&nbsp;file loads.
  </div>
  
  <div>
  </div>
  
  <div>
    You can also do this technique with <b>Javascript</b>&nbsp;files.
  </div>
</div>