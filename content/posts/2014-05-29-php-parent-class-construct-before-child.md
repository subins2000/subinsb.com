---
title: Call Parent Class construct Before Child’s In PHP
author: Subin Siby
type: post
date: 2014-05-29T15:32:09+00:00
url: /php-parent-class-construct-before-child
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - Child
  - Class
  - Function
  - Parent
  - Trick

---
If in **PHP**, you are making a child class of a parent class in which both have the **__construct** function and you want to call both of them when the child object is made, it won&#8217;t work.

This is because, when you make the child class object, it&#8217;s construct function is called. The parent&#8217;s construct function is being overridden by the child. The PHP manual on OOP (Object Oriented Programming) notes this :

<pre class="prettyprint"><code>Note: Parent constructors are not called implicitly if the child class defines a constructor. In order to run a parent constructor, a call to parent::__construct() within the child constructor is required. If the child does not define a constructor then it may be inherited from the parent class just like a normal class method (if it was not declared as private).</code></pre>

So, the parent&#8217;s construct function is not called if w&#8217;re making the child object.

## Classes

This is the parent class named "MyParentClass" :

<pre class="prettyprint"><code>class MyParentClass {
 public $myVal = 0;
 function __construct(){
  $this-&gt;myVal = 1;
 }
}</code></pre>

And we extend it to make the child class named "MyChildClass" :

<pre class="prettyprint"><code>class MyChildClass extends MyParentClass {
 function __construct(){
  echo $this-&gt;myVal;
 }
}</code></pre>

Now, when you make the child object, the output made by **echo** will be nothing but **** :

<pre class="prettyprint"><code>new MyChildClass()</code></pre>

(We&#8217;re using direct echo and not return).

## Reason

In the parent class, we make a public variable called "myVal" which is set to 0. When the parent class is made by this :

<pre class="prettyprint"><code>new MyParentClass()</code></pre>

the value of **MyParentClass::myVal** is changed to "1". This change only takes effect if the parent class is made and not the child class. So, when you make the child class, the output will be **** and not **1**.

## Solution

But, you can also make changes first in the parent class and then do the code in **MyChildClass::__construct()**. For this, we add the following code as the first code in **MyChildClass::__construct()** :

<pre class="prettyprint"><code>parent::__construct();</code></pre>

And the whole child class will become :

<pre class="prettyprint"><code>class MyChildClass extends MyParentClass {
 function __construct(){
  parent::__construct();
  echo $this-&gt;myVal;
 }
}</code></pre>

Don&#8217;t be worried about the constant **parent**, because it&#8217;s built in and it defines the parent class from which we are extending the main class.

Now, when you make the child class, the output will be **1** because the parent **__construct()** is called before the code in child&#8217;s **__construct()** is ran.

You can apply the same solution in your child classes, so that parent and class is linked instead of going separate ways.