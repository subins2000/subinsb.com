---
title: Accessing /var/log Files in PHP
author: Subin Siby
type: post
date: 2014-10-07T03:30:48+00:00
url: /php-access-log-files
categories:
  - Linux
  - PHP
  - Unix
tags:
  - Apache
  - File

---
If you have tried to access a log file in **/var/log** using PHP in Linux, you will see the "Permission denied" error. This is because, **Apache** doesn&#8217;t have read permission on the log file.

Let&#8217;s look at the user groups that have permission to access the log files : it&#8217;s **root** and the program which created the file. Suppose, let&#8217;s say the log file is "/var/log/squid/access.log". That file&#8217;s group and owner will be "proxy" and others won&#8217;t even have the read permission on it except **root **ofcourse.

Since, **Apache** is not in the group of **proxy**, it don&#8217;t have permission to read or write the file. Here are the 3 ways to grant **Apache** access to the file :

  1. Add **www-data** user into the **proxy** group
  2. Make the group of the log file **www-data**
  3. Change permission of log file to **0666**

But, if the **Squid** server is restarted, the log file is removed and re created. So, permission are reset to what it should have been. Hence the way 2 & 3 is not permanent.

You can add **www-data** into the **proxy** group by using this :

<pre class="prettyprint"><code>sudo usermod -G proxy www-data && sudo service apache2 restart</code></pre>

The above command also restarts the Apache server to update it&#8217;s cache of file permissions or something else (I exactly don&#8217;t know why).

Note that by doing the above command, you give all permissions that **proxy** group has to **Apache**, even the write permission. This is not exactly required, if you just want to read the log file.

In this case, we can use the **adm** group (it&#8217;s not "admin"). **adm** group only has read permission to some log files. The group was specifically created to read log files. Note that in versions of **Ubuntu** >= 12.04, this was renamed to **admin** (I read it somewhere, but in Lubuntu 14.04, it&#8217;s not renamed).

Hence, it&#8217;s better to add **www-data** user to the **adm** group :

<pre class="prettyprint"><code>sudo usermod -G adm www-data && sudo service apache2 restart</code></pre>

Now, all you have to do is, change the log file&#8217;s group to **adm**, by using :

<pre class="prettyprint"><code>sudo chown proxy:adm /var/log/squid/access.log</code></pre>

and the owner of the log file is proxy. The first string before ":" is the user and the second, the group.

In this tutorial, we used the **Squid Web Server**&#8216;s log file as example. It&#8217;s just an example and can be used for any other log files. It can also be used for other files, but be careful &#8211; you&#8217;re messing with the core of **Linux**.