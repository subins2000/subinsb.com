---
title: 'Tutorial : How to add floating share box in your blog'
author: Subin Siby
type: post
date: 2012-02-10T04:43:00+00:00
excerpt: 'Today we see some blogs have a floating share box. You can also have this share box.See a demo of floating share box here1st step : go to Blogger &gt;&nbsp;Template/Design&nbsp;&gt;&nbsp;Edit HTML&nbsp; &nbsp; &nbsp;&nbsp;Before &lt;/head&gt;&nbsp;past...'
url: /tutorial-how-to-add-floating-share-box-in-your-blog
authorsure_include_css:
  - 
  - 
  - 
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
syndication_item_hash:
  - 41d043bf8880c6e70f66a9375e8e7518
  - 41d043bf8880c6e70f66a9375e8e7518
  - 41d043bf8880c6e70f66a9375e8e7518
  - 41d043bf8880c6e70f66a9375e8e7518
  - 41d043bf8880c6e70f66a9375e8e7518
  - 41d043bf8880c6e70f66a9375e8e7518
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-floating-share-box-in-your.html
dsq_thread_id:
  - 838128365
  - 838128365
  - 838128365
  - 838128365
  - 838128365
  - 838128365
categories:
  - Blogger
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">Today we see some blogs have a floating share box. You can also have this share box.</span><br /><span style="font-family: inherit;">See a demo of floating share box <a href="http://pfogb.blogspot.in/" >here</a></span><br /><span style="font-family: inherit;">1st step : go to <b>Blogger >&nbsp;</b><strong style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">Template</strong><span class="Apple-style-span" style="background-color: white; color: #333333; line-height: 19px;">/</span><strong style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">Design</strong><span class="Apple-style-span" style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">&nbsp;>&nbsp;</span><strong style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">Edit HTML</strong><strong style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;">&nbsp; &nbsp; &nbsp;&nbsp;</strong></span><br /><strong style="background-color: white; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 19px; text-align: -webkit-auto;"><span style="font-family: inherit;"><br /></span></strong></p> 
  
  <div style="text-align: -webkit-auto;">
    <span class="Apple-style-span" style="color: #333333;"><span class="Apple-style-span" style="font-family: inherit; line-height: 19px;">Before <b></head></b>&nbsp;paste this code&nbsp;</span></span>
  </div>
  
  <blockquote class="tr_bq">
    <div style="text-align: left;">
      <b style="color: #333333; font-family: inherit; line-height: 19px;"><style type="text/css"></b>
    </div>
    
    <div style="text-align: left;">
      <span class="Apple-style-span" style="color: #333333; font-family: inherit;"><b style="font-family: inherit;">#leftsidebar {</b></span>
    </div>
    
    <p>
      <span class="Apple-style-span" style="color: #333333; font-family: inherit;"></span><span class="Apple-style-span" style="color: #333333; font-family: inherit;"></span>
    </p>
    
    <div style="text-align: left;">
      <span class="Apple-style-span" style="color: #333333; font-family: inherit;"><b style="font-family: inherit;">position:fixed;&nbsp;</b></span>
    </div>
    
    <p>
      <span class="Apple-style-span" style="color: #333333; font-family: inherit;"></span><span style="font-family: inherit;"></span>
    </p>
    
    <div style="text-align: left;">
      <span style="font-family: inherit;"><span class="Apple-style-span" style="color: #333333; font-family: inherit;"><b>float:left;</b></span><b style="color: #333333; font-family: inherit;">}</b></span>
    </div>
    
    <p>
      <span style="font-family: inherit;"></span><span style="font-family: inherit;"></p> 
      
      <div style="text-align: left;">
        <span class="Apple-style-span" style="color: #333333; font-family: inherit;"><b>#pageshare {position:fixed; bottom:10%; left:60px; float:left; border: 1px solid black; border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;background-color:#eff3fa;padding:0 0 2px </b></span><b style="color: #333333; font-family: inherit;">0;z-index:10; width:70px;}</b>
      </div>
      
      <p>
        </span><span class="Apple-style-span" style="color: #333333; font-family: inherit;"></p> 
        
        <div style="text-align: left;">
          <b style="font-family: inherit;">#pageshare .sbutton {float:left;clear:both;margin:5px 5px 0 5px;}</b>
        </div>
        
        <p>
          </span><span class="Apple-style-span" style="color: #333333; font-family: inherit;"></p> 
          
          <div style="text-align: left;">
            <b style="font-family: inherit;">.fb_share_count_top {width:48px !important;}</b>
          </div>
          
          <p>
            </span><span class="Apple-style-span" style="color: #333333; font-family: inherit;"></p> 
            
            <div style="text-align: left;">
              <b style="font-family: inherit;">.fb_share_count_top, .fb_share_count_inner {-moz-border-radius:3px;-webkit-border-radius:3px;}</b>
            </div>
            
            <p>
              </span><span class="Apple-style-span" style="color: #333333; font-family: inherit;"></p> 
              
              <div style="text-align: left;">
                <b style="font-family: inherit;">.FBConnectButton_Small, .FBConnectButton_RTL_Small {width:49px !important; -moz-border-radius:3px;-webkit-border-radius:3px;}</b>
              </div>
              
              <p>
                </span><span class="Apple-style-span" style="color: #333333; font-family: inherit;"></p> 
                
                <div style="text-align: left;">
                  <b style="font-family: inherit;">.FBConnectButton_Small .FBConnectButton_Text {padding:2px 2px 3px !important;-moz-border-radius:3px;-webkit-border-radius:3px;font-size:8px;}</b>
                </div>
                
                <p>
                  </span><span class="Apple-style-span" style="color: #333333; font-family: inherit;"></p> 
                  
                  <div style="text-align: left;">
                    <b style="font-family: inherit;"></style></b>
                  </div>
                  
                  <p>
                    </span>
                  </p></blockquote> 
                  
                  <div style="text-align: left;">
                    <span class="Apple-style-span" style="color: #333333; font-family: inherit;">2nd step : Go to <b>Blogger >&nbsp;Layout&nbsp;</b></span>
                  </div>
                  
                  <div style="text-align: left;">
                    <span class="Apple-style-span" style="color: #333333; font-family: inherit;"><br /></span>
                  </div>
                  
                  <div style="text-align: left;">
                    <span style="font-family: inherit;"><span class="Apple-style-span" style="color: #333333;">Click on&nbsp;</span><a href="https://2.bp.blogspot.com/-p_-U_7RM8q0/TzScYbr-bUI/AAAAAAAAAd0/nz4OjMco1JI/s1600/fb6.png" imageanchor="1" style="clear: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-p_-U_7RM8q0/TzScYbr-bUI/AAAAAAAAAd0/nz4OjMco1JI/s1600/fb6.png" /></a>&nbsp;button.</span>
                  </div>
                  
                  <div style="text-align: left;">
                    <span style="font-family: inherit;"><br /></span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <span style="font-family: inherit;">Then click on&nbsp;<a href="//1.bp.blogspot.com/-s8ay-Gy2O48/TzSctzVZjuI/AAAAAAAAAd8/JIXE39AMwh8/s1600/fb7.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="75" src="//1.bp.blogspot.com/-s8ay-Gy2O48/TzSctzVZjuI/AAAAAAAAAd8/JIXE39AMwh8/s320/fb7.png" width="320" /></a>&nbsp;option.</span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <span style="font-family: inherit;"><br /></span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <span style="font-family: inherit;">Type this in the "<b>Content Area"&nbsp;</b></span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: center;">
                    <span style="font-family: inherit;"><b></b></span>
                  </div>
                  
                  <blockquote class="tr_bq">
                    <p>
                      <b><span style="font-family: inherit;"><div id="fb-root"></div></span></b><b><span style="font-family: inherit;"><script>(function(d, s, id) {</span></b><b><span style="font-family: inherit;">&nbsp; var js, fjs = d.getElementsByTagName(s)[0];</span></b><b><span style="font-family: inherit;">&nbsp; if (d.getElementById(id)) return;</span></b><b><span style="font-family: inherit;">&nbsp; js = d.createElement(s); js.id = id;</span></b><b><span style="font-family: inherit;">&nbsp; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<span class="Apple-style-span" style="color: red;"><i><u>Your facebook App Id</u></i></span>";</span></b><b><span style="font-family: inherit;">&nbsp; fjs.parentNode.insertBefore(js, fjs);</span></b><b><span style="font-family: inherit;">}(document, &#8216;script&#8217;, &#8216;facebook-jssdk&#8217;));</script></span></b><span style="font-family: inherit;"><div id=&#8217;pageshare&#8217; title="Share This With Your Friends"></span><span style="font-family: inherit;"><div class=&#8217;sbutton&#8217; id=&#8217;gb&#8217;><script src="http://connect.facebook.net/en_US/all.js#xfbml=1&appId=</span><span style="font-family: inherit;"><b><span class="Apple-style-span" style="color: red;"><i><u>Your facebook App Id</u></i></span></b>"><b></script></b></span><span style="font-family: inherit;"><br /></span><b><span style="font-family: inherit;"><fb:like href="<span style="color: red;"><i><u>Your Blog Url</u></i></span>" send="true" layout="box_count" width="50&#8243; show_faces="true" font="arial"></fb:like></div></span></b><span style="font-family: inherit;"><div class=&#8217;sbutton&#8217; id=&#8217;rt&#8217;><a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical" >Tweet</a><script src=&#8217;http://platform.twitter.com/widgets.js&#8217; type="text/javascript"></script></div></span><span style="font-family: inherit;"><div class=&#8217;sbutton&#8217; id=&#8217;gplusone&#8217;><script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script><g:plusone size="tall"></g:plusone></div></span><span style="font-family: inherit;"><div class=&#8217;sbutton&#8217; id=&#8217;su&#8217;><script src="http://www.stumbleupon.com/hostedbadge.php?s=5&#8243;></script></div></span><span style="font-family: inherit;"><div class=&#8217;sbutton&#8217; id=&#8217;digg&#8217; style=&#8217;margin-left:3px;width:48px&#8217;><script src=&#8217;http://widgets.digg.com/buttons.js&#8217; type=&#8217;text/javascript&#8217;></script><a class="DiggThisButton DiggMedium"></a></div></span><span style="font-family: inherit;"><div class=&#8217;sbutton&#8217; id=&#8217;fb&#8217;><a name="fb_share" type="box_count" href="http://www.facebook.com/sharer.php">Share</a><script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script></div></span><span style="font-family: inherit;"><div class=&#8217;sbutton&#8217;></span><span style="font-family: inherit;"><a class=&#8217;addthis_button_more at300b&#8217; href=&#8217;#&#8217; title=&#8217;view more services&#8217;><center><h4><b>More</b></h4></center></a></span><span style="font-family: inherit;"></div></span><span style="font-family: inherit;"><script src=&#8217;http://s7.addthis.com/js/250/addthis_widget.js&#8217; type=&#8217;text/javascript&#8217;/></span><span style="font-family: inherit;"></div></span>
                    </p>
                  </blockquote>
                  
                  <div class="separator" style="clear: both; text-align: center;">
                  </div>
                  
                  <div class="separator" style="clear: both; display: inline !important;">
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <span style="font-family: inherit;">Then Click on "Save" button.</span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <span style="font-family: inherit;">Your result will be like this.</span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <a href="//1.bp.blogspot.com/-ksF2mK4sGjA/TzSfDoDnnzI/AAAAAAAAAeI/tK2QoBRVpWk/s1600/share.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><span style="font-family: inherit;"><img border="0" height="200" src="//1.bp.blogspot.com/-ksF2mK4sGjA/TzSfDoDnnzI/AAAAAAAAAeI/tK2QoBRVpWk/s200/share.png" width="34" /></span></a>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <span style="font-family: inherit;"><br /></span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: left;">
                    <span style="font-family: inherit;">Note : This share Box contains facebook Like button. if you dont want facebook like button remove these line from the "<b>Content Area"&nbsp;</b></span>
                  </div>
                  
                  <div class="separator" style="clear: both; text-align: center;">
                  </div>
                  
                  <blockquote class="tr_bq">
                    <p>
                      <span style="font-family: inherit;"><div id="fb-root"></div></span><span style="font-family: inherit;"><script>(function(d, s, id) {</span><span style="font-family: inherit;">&nbsp; var js, fjs = d.getElementsByTagName(s)[0];</span><span style="font-family: inherit;">&nbsp; if (d.getElementById(id)) return;</span><span style="font-family: inherit;">&nbsp; js = d.createElement(s); js.id = id;</span><span style="font-family: inherit;">&nbsp; js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<span class="Apple-style-span" style="color: red;"><i><u>Your facebook App Id</u></i></span>";</span><span style="font-family: inherit;">&nbsp; fjs.parentNode.insertBefore(js, fjs);</span><span style="font-family: inherit;">}(document, &#8216;script&#8217;, &#8216;facebook-jssdk&#8217;));</script></span>
                    </p>
                  </blockquote>
                  
                  <div class="separator" style="clear: both;">
                    <span style="font-family: inherit;">and this line too</span>
                  </div>
                  
                  <blockquote class="tr_bq">
                    <p>
                      <span style="font-family: inherit;"><span class="Apple-style-span" style="font-weight: bold;"><div class=&#8217;sbutton&#8217; id=&#8217;gb&#8217;><script src="http://connect.facebook.net/en_US/all.js#xfbml=1&appId=</span></span><b><span class="Apple-style-span" style="color: red; font-family: inherit;"><i><u>Your facebook App Id</u></i></span></b><span style="font-family: inherit; font-weight: bold;">"></script><fb:like href="http://pfogb.blogspot.com" send="true" layout="box_count" width="50&#8243; show_faces="true" font="arial"></fb:like></div></span>
                    </p>
                  </blockquote></div>