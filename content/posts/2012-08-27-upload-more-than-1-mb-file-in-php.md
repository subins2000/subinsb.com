---
title: Upload more than 1 MB file in PHP.
author: Subin Siby
type: post
date: 2012-08-27T06:43:00+00:00
excerpt: 'Some may have problem with uploading file more than 1 MB or 2 MB. You can fix this by editing the configuration file of PHP. To fix this follow the steps.Open Root Terminal (Applications -&gt; System Tools -&gt; Root Terminal).Type&nbsp;sudo gedit /etc...'
url: /upload-more-than-1-mb-file-in-php
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
dsq_thread_id:
  - 1966155852
  - 1966155852
  - 1966155852
  - 1966155852
  - 1966155852
  - 1966155852
syndication_item_hash:
  - 316ea55e0f712c9e0b69722b31212c7d
  - 316ea55e0f712c9e0b69722b31212c7d
  - 316ea55e0f712c9e0b69722b31212c7d
  - 316ea55e0f712c9e0b69722b31212c7d
  - 316ea55e0f712c9e0b69722b31212c7d
  - 316ea55e0f712c9e0b69722b31212c7d
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
  - http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html
categories:
  - Linux
  - PHP
  - Ubuntu

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Some may have problem with uploading file more than 1 MB or 2 MB. You can fix this by editing the configuration file of PHP. To fix this follow the steps.</p> 
  
  <p>
    Open <b>Root Terminal </b>(Applications -> System Tools -> Root Terminal).<br />Type<b>&nbsp;</b>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <b>sudo gedit /etc/php5/apache2/php.ini&nbsp;</b>
    </p>
  </blockquote>
  
  <p>
    and press enter key.<br />You will get gedit application with the <b>php.in </b>file.
  </p>
  
  <p>
    <span style="font-family: Arial, Helvetica, sans-serif;">Search for the words&nbsp;<span style="background-color: white; line-height: 22px; white-space: pre-wrap;"><b>post_max_size </b>&&nbsp;</span><span style="line-height: 22px; white-space: pre-wrap;"><b>upload_max_filesize</b></span><span style="background-color: white; line-height: 22px; white-space: pre-wrap;">.</span></span><br /><span style="font-family: Arial, Helvetica, sans-serif;"><span style="line-height: 22px; white-space: pre-wrap;">If you found the words you will see a <b>= </b>after the word </span><b style="line-height: 22px; white-space: pre-wrap;">post_max_size </b><span style="line-height: 22px; white-space: pre-wrap;">&&nbsp;</span><span style="line-height: 22px; white-space: pre-wrap;"><b>upload_max_filesize</b></span><span style="line-height: 22px; white-space: pre-wrap;">.</span></span><br /><span style="line-height: 22px; white-space: pre-wrap;"><span style="font-family: Arial, Helvetica, sans-serif;"><br /></span></span><span style="line-height: 22px; white-space: pre-wrap;"><span style="font-family: Arial, Helvetica, sans-serif;"><b>Change the number as you want</b> (Default will be <b>1M </b>or <b>2M</b>).</span></span><br /><span style="line-height: 22px; white-space: pre-wrap;"><span style="font-family: Arial, Helvetica, sans-serif;">After editing save the file and restart <b>Apache Web Server </b>by opening <b>Root Terminal </b>and pasting this code</span></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: Consolas, Monaco, Courier New, Courier, monospace;"><span style="font-size: 14px; line-height: 22px; white-space: pre-wrap;">sudo /etc/init.d/apache2 reload</span></span>
    </p>
  </blockquote>
  
  <p>
    and press enter.<br />That&#8217;s it. Upload a file in PHP and see the magic !</div>