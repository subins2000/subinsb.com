---
title: Check If A String Is HTML In PHP
author: Subin Siby
type: post
date: 2014-03-08T04:15:58+00:00
url: /php-check-if-string-is-html
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - PHP
tags:
  - Demo
  - Function

---
**HTML** detection can be hard, because HTML is complicated and there aren&#8217;t any good 100% parsers for HTML. RegEx won&#8217;t help you in detection, so you need something else. The trick to identify HTML which I&#8217;m gonna show you is very easy. It&#8217;s fast, short and doesn&#8217;t use RegEx. You can simply use an if statement to check if a string is HTML or not.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/php/isHTML/" target="_blank">Demo</a>
</div>

PHP&#8217;s **strip_tags** function when given a string, it returns a string that doesn&#8217;t have any HTML / PHP characters (<a href="http://in2.php.net/manual/en/function.strip-tags.php" target="_blank">according to the manual</a>). So, if we give a HTML string to **strip_tags()**, it strips out the HTML code from it and return a normal string.

So, if the original value is the same as the return value of **strip_tags()**, then that string doesn&#8217;t have any HTML code. Here is how you check if a string is HTML :

<pre class="prettyprint"><code>function isHTML($string){
 if($string != strip_tags($string)){
  // is HTML
  return true;
 }else{
  // not HTML
  return false;
 }
}</code></pre>

Or if you need a simple short function, use this code :

<pre class="prettyprint"><code>function isHTML($string){
 return $string != strip_tags($string) ? true:false;
}</code></pre>

Here are some usages of this function :

<pre class="prettyprint"><code>$html = "&lt;b&gt;subinsb.com is a programming blog&lt;/b&gt;";
if(isHTML($html)){
 echo "It's HTML";
}</code></pre>

The above code will surely print **It&#8217;s HTML** since the string we gave was HTML.

<pre class="prettyprint"><code>$notHTML = "subinsb.com is online for 3 years";
if(!is_HTML($notHTML)){
 echo "Not HTML";
}</code></pre>

Note the false checking character "!" in the above code. Of course, it will print **Not HTML** since the string we gave didn&#8217;t had any HTML code in it.