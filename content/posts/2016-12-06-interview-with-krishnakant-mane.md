---
title: An Interview With GNUKhata Developer Krishnakant Mane
author: Subin Siby
type: post
date: 2016-12-06T05:47:09+00:00
lastmod: 2020-01-27
url: /interview-with-krishnakant-mane
aliases:
  - /mane
categories:
  - Interview

---
This is my first interview with a person. I don't do interviews normally. But when I read about Krishnakant Mane I wanted to know more about him. So I contacted him by email and got the response within an hour. The guy is really hooked up with computers !

Krishnakant Mane is blind, but that didn't stop him from coding. He created and maintains the **[GNUKhata][1]** project, a free and open source accounting software. [GNUKhata][1] is an alternative to the proprietary softwares like Tally which is widely used all over the world.<figure class="wp-caption aligncenter">

![Krishnakant Mane](//lab.subinsb.com/projects/blog/uploads/2016/12/krishnakant-mane.png)

Even in my small town, people use Tally and they pay huge sums for it. I'm glad that there is an open source alternative to it. Here is the full interview with him :

### Which languages do you know ? Which is your favorite programming language ?

I know Java, PHP, C and Python, apart from SQL and Javascript.

Python is my personal favorite because it has the best balance of ease of use and great power. You almost end up writing English like code. Indentation and other things make it a clean syntax language.

### How do you code without seeing what you have made ? What softwares do you use for coding ?

I use a screen reader for all my computing.

It is a free and open source software called [**Orca**][5] which now comes with almost all distros of GNU/Linux. It is a complete screen reader which reads out every thing including web pages, documents, spreadsheet or code.

### You're working for FOSS. How do you pay the bills ?

Apart from being a great movement for people's digital freedom and rights, free software is a great source of earning. Lot of people come to free software due to it's low cost to start with and then tend to like the features most of the time. Then they need customization with few features specifically for their needs. Many corporates pay for free software support and also for specific modifications which they need for running their business. They see the transparency and the low Total Ownership Cost (TOC) as a benefit.

More than this there are a lot of crowd funding opportunities.

### Does people donate money for your works ? If so, what is the major contribution you received ?

People hardly donate specially in India. Yet the central government has given lakhs of Rs as grant to my projects, [GNUKhata][1] in particular.

Kerala state government has given more than 50,00,000 Rs (73,000$).

A commercial software company called Tech blue ENTP has also contributed.

### How does FOSS help you in your life ?

I use only and exclusively free software and I never use or recommend or advocate proprietary software in any form.  I have got a lot of benefit personally because I use software as I please and I never have virus issues. I totally get digital freedom of my own.

I can help build communities around technology including for training etc.  This way I also gain professionally both in terms of money and as a personal satisfaction, <span style="text-decoration: underline;">because I am doing some thing good for society</span>.

### GNUKhata is a rival software to Tally and similar proprietary softwares. Many over the world pay huge amount of money for Tally. Do you think GNUKhata is developed enough to be substituted for such proprietary softwares ?

As a matter of fact, in many aspect the said free software is having more and better features than the likes of Tally.  We had made it a point that we will not copy every thing blindly. We took new standards based approaches in many cases and what ever was good in other programs we took it.

[GNUKhata][1] is being used by many organizations now. In fact this project started on the very demand of many companies and a lot of accountants who found that <span style="text-decoration: underline;">a certain software is exploiting them</span>.

### How do you get inspired to do something ? Who are your role models ?

Every good work done by any individual is valuable so every one is a roll model in a way, yet Dr. RMS, the founder of free software movement is my role model.

In addition, Dr. Nagarjun from Homibhabha Center for Science Education of TIFR, Dr Prakash Amte, socialist leaders like Lal Bahadur Shastri are all my role models. I am a proud liberal socialist myself so the list becomes obvious.

### How are you planning to go forward in the future ? Any plans for a new project ?

My vision is to make rural india digitally empowered and raise the level of education and employment. I have an organization called [Digital Freedom Foundation][12] and we wish to bring cost of education down and also bring disabled people to the level of main stream society. All this is possible through ICT and if that has to be available for every one then free software is the only solution.

### What message do you have to say to the youth today ?

We all are capable and do what you feel best for you.

Unless your acts are physically harmful to an individual or a group and unless your acts are harming the nation's integrity and status, there is nothing right and wrong. So do what you feel, but think about those who might be as capable as you are but can't come up only because there are no opportunities and no high quality education because they are not provided with enough social and financial infrastructure. So always be socially conscious and do not play unfair.

-------

Krishnakant is an inspiration, to overcome the odds, do what you love and make a difference. You may also want to check out his [TEDx speech][14] and follow him on [Twitter](https://twitter.com/kkmane).

 [1]: http://gnukhata.in/
 [2]: //lab.subinsb.com/projects/blog/uploads/2016/12/krishnakant-mane.png
 [3]: #which-languages-do-you-know-which-is-your-favorite-programming-language
 [4]: #how-do-you-code-without-seeing-what-you-have-made-what-softwares-do-you-use-for-coding
 [5]: https://wiki.gnome.org/Projects/Orca
 [6]: #you8217re-working-for-foss-how-do-you-pay-the-bills
 [7]: #does-people-donate-money-for-your-works-if-so-what-is-the-major-contribution-you-received
 [8]: #how-does-foss-help-you-in-your-life
 [9]: #gnukhata-is-a-rival-software-to-tally-and-similar-proprietary-softwares-many-over-the-world-pay-huge-amount-of-money-for-tally-do-you-think-gnukhata-is-developed-enough-to-be-substituted-for-such-proprietary-softwares
 [10]: #how-do-you-get-inspired-to-do-something-who-are-your-role-models
 [11]: #how-are-you-planning-to-go-forward-in-the-future-any-plans-for-a-new-project
 [12]: http://dff.org.in
 [13]: #what-message-do-you-have-to-say-to-the-youth-today
 [14]: https://www.youtube.com/watch?v=DhMybi3eq3U
