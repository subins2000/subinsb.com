---
title: Can’t Boot Ubuntu after installing Nvidia Driver Fix 2
author: Subin Siby
type: post
date: 2012-09-22T12:50:00+00:00
excerpt: "A lot of people has trouble booting in to Ubuntu after installation of Nvidia Driver.Here is the second solution if first solution didn't wok.Since you get a terminal when you boot in to Ubuntu.Login to your account in the terminalThen run the command...."
url: /cant-boot-ubuntu-after-installing-nvidia-driver-fix-2
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1973412719
  - 1973412719
  - 1973412719
  - 1973412719
  - 1973412719
  - 1973412719
syndication_item_hash:
  - 8e856e46e722142258bcb366f006c952
  - 8e856e46e722142258bcb366f006c952
  - 8e856e46e722142258bcb366f006c952
  - 8e856e46e722142258bcb366f006c952
  - 8e856e46e722142258bcb366f006c952
  - 8e856e46e722142258bcb366f006c952
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
  - http://sag-3.blogspot.com/2012/09/nvidia-solution-2.html
categories:
  - Linux
  - Ubuntu
tags:
  - Nvidia

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">A lot of people has trouble booting in to Ubuntu after installation of Nvidia Driver.</span><br style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;" /><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Here is the second solution if <a href="http://sag-3.blogspot.in/2012/06/cant-boot-ubuntu-after-installing.html" >first solution</a> didn&#8217;t wok.</span><br /><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;"><br /></span><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Since you get a terminal when you boot in to <b>Ubuntu.</b></span><br /><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Login to your account in the terminal</span><br /><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;"><br /></span><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Then run the command.</span></p> 
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">sudo -i</span>
    </p>
  </blockquote>
  
  <p>
    and save a new <b>Nvidia Config File </b>by running this command.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      nvidia-xconfig
    </p>
  </blockquote>
  
  <p>
    Your new config file has been created!<br />Reboot your computer and boot in to your <b>Ubuntu </b>now.</div>