---
title: School Election Software "sElec"
author: Subin Siby
type: post
date: 2016-08-10T13:12:46+00:00
url: /school-election
enclosure:
  - |
    |
        https://raw.githubusercontent.com/subins2000/lobby-school-election/master/src/audio/beep.mp3
        24522
        audio/mpeg
        
categories:
  - Lobby

---
I&#8217;m currently studying in **12th grade** at **Government Model Girls Higher Secondary School**. Don&#8217;t mind the word "Girls" in the name, because in higher secondary both boys and girls study.

Like schools all over the world, we have an election for each classes to select our class leader. Even in 21st century, our schools were conducting the election with **ballot papers**.

So I thought why not make a software for it ? This led to the creation of "**sElec**" &#8211; a Lobby app for conducting school elections. (I just came up with the name at this moment writing this line)


## Features

  * Free, Open Source (Apache v2 licensed)
  * Web based (so it works on Linux/Windows)
  * [**99.99% Secure**][2]
  * Class & Division support
  * One software for all school elections
  * Easy to download and install
  * Can be used for various type of elections
  * Passwords for students (voters)
  * Networking capability
  
    Run election on multiple computers in a computer lab
  * Highly Customizable 
      * Can be used for various type of elections
      * Lock class-division choice to prevent fraud

## Download

There are two versions you can download :

<div class="padlinks">
  <a class="demo" href="https://drive.google.com/open?id=0B2VjYaTkCpiQUFNrZTNPdlh5SWc">sElec Linux</a><a class="download" href="https://drive.google.com/open?id=0B2VjYaTkCpiQU3pIc2pHZkhEWXc">sElec Windows</a>
</div>

Since I&#8217;m a FOSS guy, I recommend using the **sElec Linux** on **Ubuntu 16.04 Xenial Xerus**.

## Install

Installation is pretty simple if you do it rightly.

  * Download the ".zip" file according to your system (I recommend using Linux)
  * Extract the folder inside the zip file to a location of your choice (I prefer Home folder or Downloads folder)

## Open

Run the file "Lobby" inside the folder you just extracted.

If you&#8217;re using Linux you may double click on "Lobby.sh" file and choose "Execute" in the dialog that will pop out.

**NOTE (Windows)** &#8211; When you run "Lobby.exe", there is a chance for an error :

> VCRUNTIME110.dll is missing

To fix it, see <a href="https://lobby.subinsb.com/docs/quick/windows#running-lobby" target="_blank">this page and read it</a>.

After you run it, the URL "<http://127.0.0.1:2020>" will be opened in your default web browser :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-index.png" width="1368" height="768" />][6]<figcaption class="wp-caption-text">The voter registration</figcaption></figure> 

If you got a window similar to above, installation is completed. If not try :

  * Update your browser to a latest version. (I recommend Chromium or Google Chrome)
  * (Linux) Run Lobby.sh in terminal and see the output. Google the error shown in output to find a solution

## Administration

Okay, let&#8217;s add some candidates. For this let&#8217;s go to the admin page. Admin page&#8217;s URL is :

> <http://127.0.0.1:2020/admin>

A login page will be shown :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-login.png" alt="Lobby admin login" width="1327" height="741" />][8]<figcaption class="wp-caption-text">Admin login</figcaption></figure> 

Both the default username and password is "admin" :

> Username : admin
  
> Password : admin

After you login as admin, hover on the "**I**" symbol seen on the top of the page and choose "App Admin" :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-app.png" alt="Lobby Indi app admin" width="343" height="147" />][9]<figcaption class="wp-caption-text">Hover on "I" symbol and choose "App Admin"</figcaption></figure> 

Now you have entered the administration part of **sElec**. Whenever you want to enter the **Election Panel**, just click the "App Admin" button after you hover the "I" symbol.

## Election Panel

The Election Panel lets you configure the election :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-index.png" alt="Election Panel" width="1327" height="741" />][11]<figcaption class="wp-caption-text">Election Panel</figcaption></figure> 

Here is a brief description about the options you see in that page :

  * General Settings &#8211; Basic settings for election
  * Manage Candidates &#8211; Add, edit and remove candidates
  * Manage Voters &#8211; Generate passwords for user if you have chosen to set password for voters
  * Results &#8211; View the results of election
  * Voted List &#8211; See who all have voted or not in a class.
  * CLEAR ALL DATA! &#8211; Clear all data stored including candidate information, votes and voter passwords

## Clear All Data!

Before you start a election, **ALWAYS** clear data before proceeding to add candidates. So, click on the "CLEAR ALL DATA!" button colored in red and choose Ok on the dialog that comes. This will clear all data associated with election.

## General Settings

The page looks like this :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-general-settings.png" width="1327" height="741" />][14]<figcaption class="wp-caption-text">Election Panel -> General Settings</figcaption></figure> 

### Election Type

There are 3 types of election you can do with **sElec**. They are :

  * Normal Election (Default)
  
    Used for school leader elections.
  
    There will be a set of candidates for the whole school and students in each class must vote for them to become school leader
  * Class Wise
  
    Used for class leader election.
  
    There will be separate set of candidates for each class and students in those class must vote for their candidates to make them class leader.
  * Boys & Girls
  
    Used for school leader selection from boys and girls separately.
  
    Similar to Normal Election. But there will be separate "Boys" & "Girls" candidates list. The voter must choose their candidate from these two lists.

### Number of Votes

This option tells how many votes can one voter do. If this value is changed into "2", the voter can choose 2 candidates from the list. The default value is "1".

### Maximum Strength

This value indicates the maximum roll number one can enter. Example: If 5A has `60` students and 5B has `63` students, then the value should be `63`

### Password For Voters

Whether the voter need to enter password along with roll number to vote. If you enable this option, you would have to go to Election Panel -> Manage Voters and generate passwords for voters.

### Classes

Add the classes that will be using **sElec** to vote. You can add a new class field by clicking the "+" button and remove an existing field by clicking the "-" button alongside the input.

### Divisions

Add the divisions that will be using **sElec** to vote. You can add a new division field by clicking the "+" button and remove an existing field by clicking the "-" button alongside the input.<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-general-settings-classes.png" width="1327" height="741" />][21]<figcaption class="wp-caption-text">Add/remove classes and divisions</figcaption></figure> 

### Default Class

This option tells which class should be selected by default when the voter sees the page.

### Default Division

This option tells which division should be selected by default when the voter sees the page.

### Disable choice of class and division

The voter can change the class and division if he/she wants to. You can prevent them from doing so by enabling this option. When this is enabled the class and division remains constant according to the default class and division chose in the above setting.

After you have chose the settings, click "Save Settings" button.

## Return To Election Panel

You can return to the Election Panel by choosing the activated **sElec** tab in the sidebar :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-sidebar.png" width="210" height="257" />][26]<figcaption class="wp-caption-text">The sElec option lets you go to Election Panel</figcaption></figure> 

## Manage Candidates

In this page, you can add, edit or remove candidates. You can add a new candidate field by clicking the "+" button and remove one with "-" button.

The gender option is not actually necessary unless the election type is "Boy and Girls".

## Manage Voters

Passwords for the voters can be generated in this page. This should only be done if you have enabled "Passwords" in General Settings.

Enter the roll number up to which passwords should be generated and submit the form. The passwords will be shown :<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-passwords.png" width="1327" height="741" />][29]<figcaption class="wp-caption-text">Password generator</figcaption></figure> 

It is better to re generate passwords after a class has completed voting.

## Results

The results will have two sections "Standings" and "Graph". Both are color coded by Gold, Silver, Bronze and all others black. Gold is for 1st, Silver is for 2nd and Bronze for 3rd.<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-results.png" width="1012" height="609" />][31]<figcaption class="wp-caption-text">Results of election</figcaption></figure> 

If the election type is "Class Wise", then you have to submit a form asking for class and division to get the result in that class.

## Voted List

In this page, you can see :

  * How many votes in a class has been done
  * How many didn&#8217;t vote in a class
  * Time when a vote was made<figure class="wp-caption aligncenter">

[<img class="" src="//lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-voted-list.png" width="1147" height="629" />][33]<figcaption class="wp-caption-text">Voted List</figcaption></figure> 

There is no provision to see who has voted who. **sElec **does not store that data in any ways whatsoever.

## Implementation

We have seen the software. But, how to implement it ? Let me follow you through the procedure :

  * Clear data
  * Change General Settings
  * Add Candidates
  * Make a polling booth with a computer having **sElec** installed
  * Form a voter line to the polling booth
  * Generate Passwords in **sElec** (if you&#8217;re going to enable passwords)
  * Enter the first voter, tell him/her the password, let him/her go to the polling booth and do his vote
  * When the <a href="https://raw.githubusercontent.com/subins2000/lobby-school-election/master/src/audio/beep.mp3" target="_blank"><strong>BEEP sound</strong></a> is made, make him/her leave and let the next voter inside
  * Continue the process&#8230;

It is recommended to **re generate** the passwords at intervals like when a class completes voting.

## Networking

Another interesting feature about **sElec** is that you can use it on a **WiFi** or **Wired** network. For this, :

  * Connect the computer and a smartphone to the same **WiFi** network.
  * Get the computer&#8217;s **LAN IP address** (192.168.1.2)
  * Copy this IP address
  * In the folder where the software is installed,
  
    &#8211; Open "Lobby.sh" if system is Linux and change "127.0.0.1:2020" to the **IP address** you just copied before
  
    &#8211; Open "lobby.ini" if system is Windows and change "127.0.0.1:2020" to the **IP address** you just copied before
  * Stop the Lobby Server by running "Stop Lobby Server" file in that folder
  * [Start Lobby again by running "Lobby" inside the folder][36].

## Security

As I stated above, **no one will know who you voted for**. It&#8217;s secure, guaranteed.

The data is stored in an **SQLite** database. The data refers to candidates info, number of votes and passwords.

  * During the election, it&#8217;s impossible for one person to find this database and copy it. Even if he/she copied it, there is no use for it.
  * Admin area is password protected and this password is hashed with a very secure algorithm
  * Free of SQL injection and XSS attack vulnerability

If you faced any problem (however small it may be) or have any questions, feel absolute free to comment below. I&#8217;ll be more than happy to help.

 [1]: #features
 [2]: #article-security
 [3]: #download
 [4]: #install
 [5]: #open
 [6]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-index.png
 [7]: #administration
 [8]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-login.png
 [9]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-app.png
 [10]: #election-panel
 [11]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-index.png
 [12]: #clear-all-data
 [13]: #general-settings
 [14]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-general-settings.png
 [15]: #election-type
 [16]: #number-of-votes
 [17]: #maximum-strength
 [18]: #password-for-voters
 [19]: #classes
 [20]: #divisions
 [21]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-general-settings-classes.png
 [22]: #default-class
 [23]: #default-division
 [24]: #disable-choice-of-class-and-division
 [25]: #return-to-election-panel
 [26]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-sidebar.png
 [27]: #manage-candidates
 [28]: #manage-voters
 [29]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-passwords.png
 [30]: #results
 [31]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-results.png
 [32]: #voted-list
 [33]: //lab.subinsb.com/projects/blog/uploads/2016/08/selec-admin-voted-list.png
 [34]: #implementation
 [35]: #networking
 [36]: #article-open
 [37]: #security
