---
title: "My Article In Infokairali Magazine on Localization"
type: post
date: 2019-11-14T20:00:00+05:30
url: /my-article-infokairali-malayalam-localization
categories:
  - Tech
tags:
  - Malayalam
  - free software
  - open source
---

## About

Lately, I've been volunteering for [Swathanthra Malayalam Project (SMC)](https://smc.org.in/). I took up the initiative in revamping [KDE localization](https://wiki.smc.org.in/KDE_Malayalam) which was dorminant for many years. More about how, why I started [this is in this Twitter thread](https://twitter.com/SubinSiby/status/1162390557332627456). We were successful in pushing new Malayalam localization to the wide range of KDE products.

November 1 is the birthday of Kerala state of India. Kerala was formed on the basis of the language spoken, Malayalam. Being November, Infokairali magazine decided to roll this edition with the theme "Digital Malayalam". Infokairali is the only tech magazine in Malayalam.

Seeing my work with KDE localization, [Nandakumar Edamana](https://nandakumar.co.in/) of Infokairali invited me to write an article on localization. I've never published in a magazine before, that too with this wide reach. Well, I made one and here's my final draft.

This draft is very much likely boring to read. It's pieces scrambled finally at the end. Nandakumar fixed it all up and made it cool to read. Thanks ! You can see the [pictures of the article in the magazine here](https://aana.site/@subins2000/103125433222273578).

Well, here's the draft :

## Article

ഇംഗ്ലീഷിലോ മറ്റ് ഭാഷകളിലോ നിർമ്മിച്ചിട്ടുള്ള സോഫ്ട്‍വെയറുകൾ പ്രാദേശിക ഭാഷകളിലേക്ക് മാറ്റുന്ന പ്രക്രിയെയാണ് പ്രാദേശികവത്കരണം എന്ന് പറയുന്നത്. വെറും തർജ്ജമ്മയല്ല പ്രാദേശികവത്കരണത്തിൽ നടക്കുന്നത്. ആ സൊഫ്റ്റ്വയെറിന്റെ ഉപപോഗ്ത്താവിനു എളുപ്പം മനസ്സിലാവുന്നതും ഉപയോഗിക്കാവുന്ന രീതിയിലാണു പ്രാദേശികവത്കരണം നടത്തുന്നത്.

പ്രാദേശികവത്കരണം അനുവദിച്ചിട്ടുള്ള  സോഫ്ട്‍വെയറുകൾ പ്രത്യേകതരത്തിലുള്ള POT ഫയലുകൾ ലഭ്യമാക്കും. അതിൽ ഉപപോക്താവിന്  കാണത്തക്കമുള്ള എല്ലാ വാക്കുകളും വരികളും ഉൾപ്പെടുത്തിയിട്ടുണ്ടാവും. ഈ POT രൂപരേഖ ഫയലിൽ നിന്ന് ഓരോ ഭാഷയ്ക്ക് ഓരോ PO ഫയൽ ഉണ്ടാക്കും. ഇതിലാണ് പ്രാദേശികഭാഷയിലുള്ള തത്തുല്യ വാക്കുകളും വരികളും ഉൾപ്പെടുത്തുന്നത്. ഈ ഫയലിൽ നിന്നും സംഗ്രഹിച്ച  MO ഫയലുകൾ നിർമ്മിച്ച് സോഫ്ട്‍വെയറുകളിൽ ഉൾപ്പെടുത്തിയാണ് അവ പുറത്തിറക്കുന്നത്. ഇങ്ങനെയാവുമ്പോൾ  ആ സോഫ്ട്‍വെയർ പല ഭാഷകളിൽ ഉപയോഗിക്കാൻ നമുക്കാകുന്നു.

Android, Windows അങ്ങനെ പലതും മലയാളത്തിൽ ലഭ്യമാണു. ഇതിനായി ഈ കമ്പനികളുടെ ഉള്ളിൽ തന്നെ ഭാഷാ ടീമുകൾ ഉണ്ട്. സ്വതന്ത്ര സോഫ്ട്‍വെയറുകളുടെ കാര്യത്തിലാണെങ്കിൽ volunteers ആണ് മലയാളത്തിലേക്കാക്കാൻ ശ്രമിക്കുനത്.  സ്വതന്ത്ര മലയാളം കമ്പ്യൂട്ടിങ്ങിന്റെ ആദ്യ  കാലം മുതലേ ഇതിനായി പരിശ്രമങ്ങൾ തുടങ്ങിയിരുന്നു. GNOME, Debian, KDE അങ്ങനെ പല സ്വതന്ത്ര സോഫ്ട്‍വെയറുകളും SMCയുടെ നേതൃത്വത്തിൽ പ്രാദേശികവത്കരിചിട്ടുണ്ട്. ഇവയുടെ settingsൽ പോയി നിങ്ങൾക്ക് ഭാഷ മാറ്റാവുന്നതാണു.

ഇങ്ങനെ മലയാളത്തിലാക്കുമ്പോൾ ഒരു പ്രശ്നം അനുഭവിക്കുനത് എന്ത് വാക്കുകൾ ഉപയോഗിക്കണം എന്നാണ്. സാങ്കേതിക വാക്കുകൾക്കുള്ള പകരം വാക്ക് മലയാളത്തിൽ ഇല്ല.  മലയാളത്തിൽ പുതിയ വാക്കുകൾ ഉണ്ടാക്കാൻ ഒരു ഔദ്യോകിക സംരഭം ഇല്ലാത്തതുകൊണ്ട് തന്നെ ഇത് ബുദ്ധിമുട്ടാകുന്നു. അപ്പൊ അത് ഞങ്ങൾക്ക് നിർമ്മിക്കേണ്ടി വരുന്നു. മലയാളം UI ഉപയോഗിച്ച് ഉപയോഗിച്ചാണു ഈ വാക്കുകൾ പൊതുവായി വരുന്നത്. ഉദാഹരണത്തിനു "Folder" എന്ന വാക്കിനു "അറ" എന്നാണ് ഉപയോഗിക്കുക. ആദ്യമായി മലയാളം UI ഉപയോഗിക്കുന്ന ഒരു വ്യക്തിക്ക് ഇത് മനസ്സിലാവണമെന്നില്ല. കൂടുതൽ ആളുകൾ ഉപയോഗിച്ച് ഉപയോഗിച്ച് അത് പ്രചാരത്തിലാകുന്നു, മലയാളം ഭാഷ കാലത്തിനൊപ്പം വളരുകയും ചെയ്യുന്നു. ഒരു കമ്പനി എന്ത് വാക്ക് വേണം എന്നു തീരുമാനിക്കാതെ, ഉപപോക്താക്കൾ തന്നെ അത് ശൃഷ്ടിക്കുമ്പോഴാണു കൂടുതൽ ഇണങ്ങുക. സ്വതന്ത്ര സോഫ്ട്‍വെയറിൽ ആ സ്വാതന്ത്രം നമുക്ക് ലഭിക്കുന്നു.

### l10n & i18n

i18n - സോഫ്ട്‍വെയർ പ്രാദേശികവത്കരിക്കാൻ തരത്തിലുള്ള രീതിയിൽ മാറ്റുന്നതിനെയോ നിർമ്മിക്കുന്നതിനെയാണു "Internationalization" എന്ന് പറയുന്നത്. അത് ചുരുക്കമായി "i18n" എന്നുപയോഗിക്കുന്നു.
l10n - "Localization" എന്ന വാക്കിന്റെ ചുരുക്ക പേരാണിത്. പ്രാദേശികവത്കരിക്കുക എന്നർത്ഥം.

### AI & Localization

ഈ PO ഫയലുകൾ ഒരു dataset ആയി ഉപയോഗിച്ച് malayalam NLP AI വച്ച് വികസിപ്പിക്കാനാവും. അതാണ് indicnlp.org എന്ന സ്വതന്ത്ര സോഫ്ട്‍വെയർ പ്രോജെക്റ്റ് ചെയ്യുന്നത്. അപ്പോൾ പ്രാദേശികവൽക്കരണംകൊണ്ട് രണ്ടുണ്ട് ഗുണം !

### Apps localized good in ml

* Firefox - both mobile and desktop
* Calamares Linux Installer - https://t.me/calamaresl10n
* GNOME Chess game and lot of others - https://l10n.gnome.org/languages/ml/gnome-3-34/ui/
  Anish Sheela did the most
* KFind
* Kate - KDE code editor
* Kwrite - KDE text editor
* Dolphin file manager
* Konsole terminal

### KDE

ലോകത്തിലെ തന്നെ ഒരു വലിയ സ്വതന്ത്ര സോഫ്ട്‍വെയർ കൂട്ടായ്മയാണു കെ.ഡി.ഇ. കെ.ഡി.ഇയുടെ കീഴിൽ കൊറെയധികം സോഫ്ട്‍വെയറുകളുണ്ട്. ലിനക്സിൽ ഒരു ഡെസ്ക്ടോപ് UIയും കെ.ഡി.ഇ ക്കുണ്ട് (പ്ളാസ്മ). ഇവയൊക്കെ മലയാളത്തിലേക്ക് പ്രാദേശികവത്കരിക്കാനുള്ള ഉദ്യമം ഇപ്പോൾ നടന്നുകൊണ്ടിരിക്കുന്നു.

മലയാളികളുടെ സ്വന്തം OS ആയ തെങ്ങ് ഒഎസിന്റെ അടുത്ത പതിപ്പ് കെ.ഡി.ഇ സൊഫ്റ്റ്വയെറുകൾ ഉൾപ്പെടുത്തിക്കൊണ്ടായിരിക്കും. അതിനു വേണ്ടിയാണ് കെ.ഡി.ഇ പ്രാദേശികവൽക്കരിക്കാനുള്ള യഞ്ജം വീണ്ടും ഉണർന്നത്. ഇതിൽ പങ്കാളിയാകാൻ നിങ്ങൾക്കും സാധിക്കും : https://kde.smc.org.in

മലയാളീകരിച്ച PO ഫയലുകൾ കെഡിഇ സംഭരണിയില്‍ ചേര്‍ക്കാന്‍ അനുവാദം ഉള്ളവരാണ് "Localization Maintainers". ഇതിൽ ഒരാളാണ് ഞാൻ (സുബിൻ സിബി). പൊതുവേ ഈ PO ഫയലുകൾ ഓരോ ആളുകൾ download ചെയ്തിട്ടാണ് പണിയെടുക്കുക. പക്ഷെ അതിലും എളുപ്പമായി ഒരു websiteൽ തന്നെ പ്രാദേശികവത്കരിക്കാൻ സഹായിക്കുന്ന ഒരെണ്ണം ഞാൻ ഉണ്ടാക്കി. അതാണ് kde.smc.org.in. ഇതുപോലെ Firefoxനുള്ളതാണു https://pontoon.mozilla.org/

#### Top Contributors to KDE Malayalam

* നീനു ചാക്കോ
* അമൃതനാഥ്‌ മനോഹരൻ
* അതുൽ രാജ്
* ശ്രീരാം വെങ്കിടേഷ്
* ലക്ഷ്മി സുനില്‍,
* അനൂപ് എം എസ്

More : https://blog.smc.org.in/smc-monthly-report-april-2019/