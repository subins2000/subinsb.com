---
title: 'DNS Changer Trogan Virus: Thousands of Computers lose internet connection today'
author: Subin Siby
type: post
date: 2012-07-09T13:28:00+00:00
excerpt: '<div dir="ltr" trbidi="on"><br>From today on wards about 3 lakh of computers will lose their internet connection. The computers will become offline. This is because of a Trogan Virus named DNS Changer.<br><br><span><b>What Does DNSChanger Do to My Computer?</b></span><br><br>DNSChanger malware causes a computer to use rogue DNS servers in one of two ways.<br>First, it changes the computer&rsquo;s DNS server settings to replace the ISP&rsquo;s good DNS servers<br>with rogue DNS servers operated by the criminal. Second, it attempts to access devices on<br>the victim&rsquo;s small office/home office (SOHO) network that run a dynamic host configuration<br>protocol (DHCP) server (eg. a router or home gateway). The malware attempts to access<br>these devices using common default usernames and passwords and, if successful, changes<br>the DNS servers these devices use from the ISP&rsquo;s good DNS servers to rogue DNS servers<br>operated by the criminals. This is a change that may impact all computers on the SOHO<br>network, even if those computers are not infected with the malware.<br><br><span><b>Am I Infected?</b></span><br>The best way to know that is by going to the website&nbsp;<span><a href="http://dns-ok.us/" target="_blank">DNS Changer Check-Up</a></span><br>If it shows <b>Green</b> then you have nothing to worry about.<br><br><span><b>What Should I Do?</b></span><br>In addition to directing your computer to utilize rogue DNS servers, the DNSChanger<br>malware may have prevented your computer from obtaining operating system and antimalware updates, both critical to protecting your computer from online threats. This<br>behavior increases the likelihood of your computer being infected by additional malware.<br>The criminals who conspired to infect computers with this malware utilized various methods<br>to spread the infections. At this time, there is no single patch or fix that can be downloaded<br>and installed to remove this malware. Individuals who believe their computer may be<br>infected should consult a computer professional. <br>Individuals who do not have a recent back-up of their important documents, photos, music,<br>and other files should complete a back-up before attempting to clean the malware or utilize<br>the restore procedures that may have been packaged with your computer.<br>Information regarding malicious software removal can be found at the website of the United<br>States Computer Emergency Readiness Team: <a href="https://www.uscert.gov/reading_room/trojan-recovery.pdf" target="_blank">Recovering from a Trogan</a>.<br><br></div>'
url: /dns-changer-trogan-virus-thousands-of-computers-lose-internet-connection-today
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 363a656a66723d8488ec5ebebfc39790
  - 363a656a66723d8488ec5ebebfc39790
  - 363a656a66723d8488ec5ebebfc39790
  - 363a656a66723d8488ec5ebebfc39790
  - 363a656a66723d8488ec5ebebfc39790
  - 363a656a66723d8488ec5ebebfc39790
syndication_permalink:
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
  - http://sag-3.blogspot.com/2012/07/dns-changer-trogan-virus-thousands-of.html
categories:
  - Uncategorized
tags:
  - Uncategorized

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  From today on wards about 3 lakh of computers will lose their internet connection. The computers will become offline. This is because of a Trogan Virus named DNS Changer.</p> 
  
  <p>
    <span style="font-size: large;"><b>What Does DNSChanger Do to My Computer?</b></span>
  </p>
  
  <p>
    DNSChanger malware causes a computer to use rogue DNS servers in one of two ways.<br />First, it changes the computer’s DNS server settings to replace the ISP’s good DNS servers<br />with rogue DNS servers operated by the criminal. Second, it attempts to access devices on<br />the victim’s small office/home office (SOHO) network that run a dynamic host configuration<br />protocol (DHCP) server (eg. a router or home gateway). The malware attempts to access<br />these devices using common default usernames and passwords and, if successful, changes<br />the DNS servers these devices use from the ISP’s good DNS servers to rogue DNS servers<br />operated by the criminals. This is a change that may impact all computers on the SOHO<br />network, even if those computers are not infected with the malware.
  </p>
  
  <p>
    <span style="font-size: large;"><b>Am I Infected?</b></span><br />The best way to know that is by going to the website&nbsp;<span style="background-color: white; text-align: center;"><a href="http://dns-ok.us/" >DNS Changer Check-Up</a></span><br />If it shows <b>Green</b> then you have nothing to worry about.
  </p>
  
  <p>
    <span style="font-size: large;"><b>What Should I Do?</b></span><br />In addition to directing your computer to utilize rogue DNS servers, the DNSChanger<br />malware may have prevented your computer from obtaining operating system and antimalware updates, both critical to protecting your computer from online threats. This<br />behavior increases the likelihood of your computer being infected by additional malware.<br />The criminals who conspired to infect computers with this malware utilized various methods<br />to spread the infections. At this time, there is no single patch or fix that can be downloaded<br />and installed to remove this malware. Individuals who believe their computer may be<br />infected should consult a computer professional. <br />Individuals who do not have a recent back-up of their important documents, photos, music,<br />and other files should complete a back-up before attempting to clean the malware or utilize<br />the restore procedures that may have been packaged with your computer.<br />Information regarding malicious software removal can be found at the website of the United<br />States Computer Emergency Readiness Team: <a href="https://www.uscert.gov/reading_room/trojan-recovery.pdf" >Recovering from a Trogan</a>.
  </p>
</div>