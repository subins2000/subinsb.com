---
title: How To Create A Simple Password Strength Checker In jQuery
author: Subin Siby
type: post
date: 2013-10-13T16:55:00+00:00
excerpt: "A lot of new hacking methods are coming day by day. So it's the programmers duty to make their user's password&nbsp;secure. Here is a simple Password Strength&nbsp;Checker created using jQuery. The code contain a function got from Wordpress (I modified..."
url: /how-to-create-a-simple-password-strength-checker-using-jquery
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
  - http://sag-3.blogspot.com/2013/10/create-simple-password-strength-checker-using-jquery.html
dsq_thread_id:
  - 1961834149
  - 1961834149
  - 1961834149
  - 1961834149
  - 1961834149
  - 1961834149
syndication_item_hash:
  - 5cd7328d21df4db5ed87889fa2d9bf96
  - 5cd7328d21df4db5ed87889fa2d9bf96
  - 5cd7328d21df4db5ed87889fa2d9bf96
  - 5cd7328d21df4db5ed87889fa2d9bf96
  - 5cd7328d21df4db5ed87889fa2d9bf96
  - 5cd7328d21df4db5ed87889fa2d9bf96
categories:
  - CSS
  - HTML
  - JavaScript
  - jQuery
  - RegEx
  - Security
  - WordPress
tags:
  - Demo
  - Download
  - Passwords
  - Tutorials

---
<div dir="ltr" style="text-align: left;">
  <p>
    A lot of new hacking methods are coming day by day. So it&#8217;s the programmers duty to make their user&#8217;s <b>password</b> secure. Here is a simple <b>Password Strength</b> Checker created using <b>jQuery</b>. The code contain a function got from <b>WordPress </b>(I modified it) which is the core component of the checker. This checker will check the following on the password :
  </p>
  
  <ol style="text-align: left;">
    <li>
      Have minimum value of <b>4</b> characters.
    </li>
    <li>
      Whether the username matches with password
    </li>
    <li>
      Whether the password contain small <b>a-z</b> characters and capital <b>A-Z</b> characters.
    </li>
    <li>
      Whether the password has numeric characters and special characters.
    </li>
  </ol>
  
  <p>
    If all of the above criteria is matched, the checker will give a <b>Strong Password</b> message.
  </p>
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=5b6cxortxy85zbc&class=9">Download</a> <a class="demo" href="http://demos.subinsb.com/jquery-password-strength-checker">Demo</a>
  </div>
  
  <p>
    Create two files : <b>index.html</b> and <b>passchecker.js</b><br /> We will also include the <b>jQuery</b> library on the <b>HTML</b> file.
  </p>
  
  <h2 style="text-align: left;">
    index.html
  </h2>
  
  <pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
 &lt;head&gt;
  &lt;script src="http://code.jquery.com/jquery-latest.min.js"&gt;&lt;/script&gt;
  &lt;script src="passchecker.js"&gt;&lt;/script&gt;
  &lt;title&gt;Simple jQuery Password Strength Checker&lt;/title&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;div id="content" style="margin-top:10px;height:100%;"&gt;
   &lt;center&gt;&lt;h1&gt;Simple jQuery Password Strength Checker&lt;/h1&gt;&lt;/center&gt;
   &lt;table&gt;&lt;tbody&gt;
    &lt;tr&gt;
     &lt;td&gt;
      Username :
     &lt;/td&gt;
     &lt;td&gt;
      &lt;input type="text" size="35" id="user" placeholder="Username"/&gt;
     &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
     &lt;td&gt;
      Password :
     &lt;/td&gt;
     &lt;td&gt;
      &lt;input type="password" size="35" id="pass" placeholder="Type A Password"/&gt;
     &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
     &lt;td&gt;&lt;/td&gt;
     &lt;td&gt;
      &lt;div id="ppbar" title="Strength"&gt;&lt;div id="pbar"&gt;&lt;/div&gt;&lt;/div&gt;
      &lt;div id="ppbartxt"&gt;&lt;/div&gt;
     &lt;/td&gt;
    &lt;tr&gt;
     &lt;td&gt;
      Retype Password :
     &lt;/td&gt;
     &lt;td&gt;
      &lt;input type="password" size="35" id="pass2" placeholder="ReType Password"/&gt;
     &lt;/td&gt;
    &lt;/tr&gt;
   &lt;/tbody&gt;&lt;/table&gt;
  &lt;/div&gt;
  &lt;style&gt;
  input{
   border:none;
   padding:8px;
  }
  #ppbar{
   background:#CCC;
   width:300px;
   height:15px;
   margin:5px;
  }
  #pbar{
   margin:0px;
   width:0px;
   background:lightgreen;
   height: 100%;
  }
  #ppbartxt{
   text-align:right;
   margin:2px;
  }
  &lt;/style&gt;
&lt;!-- http://www.subinsb.com/2013/10/create-simple-password-strength-checker-using-jquery.html --&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>
  
  <h2 style="text-align: left;">
    passchecker.js
  </h2>
  
  <pre class="prettyprint"><code>function passwordStrength(f,i,d){var k=1,h=2,b=3,a=4,c=5,g=0,j,e;if((f!=d)&&d.length&gt;0){return c}if(f.length==0){return 0;}if(f.length&lt;4){return k}if(f.toLowerCase()==i.toLowerCase()){return h}if(f.match(/[0-9]/)){g+=10}if(f.match(/[a-z]/)){g+=26}if(f.match(/[A-Z]/)){g+=26}if(f.match(/[^a-zA-Z0-9]/)){g+=31}j=Math.log(Math.pow(g,f.length));e=j/Math.LN2;if(e&lt;40){return h}if(e&lt;56){return b}return a};
function widthofpr(p){
 if(p==0){return "0%";}
 if(p==1){return "25%";}
 if(p==2){return "50%";}
 if(p==3){return "75%";}
 if(p==4){return "100%";}
}
function textofpr(p){
 if(p==0){return "Type A Password";}
 if(p==1){return "Short Password";}
 if(p==2){return "Bad Password";}
 if(p==3){return "Good Password";}
 if(p==4){return "Strong Password";}
 if(p==5){return "Password Mismatch";}
}
$(document).ready(function(){
 user=$("#user");
 pass=$("#pass");
 pass2=$("#pass2");
 pbar=$("#pbar");
 pbart=$("#ppbartxt");
 function onKeyUp(){
  pbar.css('width',widthofpr(passwordStrength(pass.val(),user.val(),pass2.val())));
  pbart.text(textofpr(passwordStrength(pass.val(),user.val(),pass2.val())));
 }
 pass.bind('keyup',function(){
  onKeyUp();
 });
 pass2.bind('keyup',function(){
  onKeyUp();
 });
});</code></pre>
  
  <p>
    When a keypress event is fired on any one of the password fields, <b>jQuery</b> will check the password strength and print according to the strength. Easy as compiling <b>Python</b>.<br /> If you have any problems / suggestions / feedback, contact me via comments. I am here 10/7.
  </p>
</div>