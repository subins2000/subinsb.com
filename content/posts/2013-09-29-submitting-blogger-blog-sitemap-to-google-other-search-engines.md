---
title: 'Submitting Blogger Blog Sitemap To Google & Other Search Engines'
author: Subin Siby
type: post
date: 2013-09-29T06:35:00+00:00
excerpt: 'Sitemap is a&nbsp;XML&nbsp;file that contains all the links of your site/blog. Sitemap&nbsp;can be manually created or automatically created using some online tools. These Sitemap&nbsp;can be submitted to search engine via the tools they provide or by ...'
url: /submitting-blogger-blog-sitemap-to-google-other-search-engines
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 6dc34572671accd6fb985300a10d85ef
  - 6dc34572671accd6fb985300a10d85ef
  - 6dc34572671accd6fb985300a10d85ef
  - 6dc34572671accd6fb985300a10d85ef
  - 6dc34572671accd6fb985300a10d85ef
  - 6dc34572671accd6fb985300a10d85ef
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
  - http://sag-3.blogspot.com/2013/09/submitting-blogger-blog-sitemap-to.html
dsq_thread_id:
  - 1807176163
  - 1807176163
  - 1807176163
  - 1807176163
  - 1807176163
  - 1807176163
categories:
  - SEO
tags:
  - Google
  - Search
  - Search Engines
  - Sitemap

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;"><b>Sitemap </b>is a&nbsp;<b>XML</b>&nbsp;file that contains all the links of your site/blog. <b>Sitemap</b>&nbsp;can be manually created or automatically created using some online tools. These <b>Sitemap</b>&nbsp;can be submitted to search engine via the tools they provide or by mentioning the <b>sitemap</b> in <b>robots.txt</b>. In <b>Blogger</b>&nbsp;blogs the sitemap is located in different places.</span><br /><span style="font-family: inherit;">In <b>Blogger</b>&nbsp;<b>Blogs</b>&nbsp;with the <b>.blogspot.com</b>&nbsp;sub domain, the sitemap is in a simple location ie :</span></p> 
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">http://YOURBLOG.blogspot.com/sitemap.xml</span>
    </p>
  </blockquote>
  
  <p>
    <span style="font-family: inherit;">But on other blogs ie blogs on custom&nbsp;<b>domains</b>&nbsp;doesn&#8217;t have a sitemap like the above <b>URL</b>. Instead you should use the following <b>URL</b>&nbsp;:</span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">http://YOURBLOG.com<span style="white-space: pre-wrap;">/atom.xml?redirect=false&start-index=1&max-results=1000</span></span>
    </p>
  </blockquote>
  
  <p>
    <span style="white-space: pre-wrap;"><span style="font-family: inherit;">Now, Let&#8217;s mention the sitemap on <b>robots.txt</b>. For That Go To <b>Blogger </b>-><b> YOUR BLOG</b> -><b> Settings </b>-><b> Search Preferences</b>.</span></span><br /><span style="font-family: inherit;"><span style="white-space: pre-wrap;">Click on <b>Edit</b> button near </span><span style="color: #222222;"><b>Custom robots.txt </b>and add the following in the textbox:</span></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="color: #222222;">User-agent: *</span><br /><span style="color: #222222;">Disallow: /search?q=*</span><br /><span style="color: #222222;">Disallow: /*?updated-max=*</span><br /><span style="color: #222222;">Allow: /</span><br /><span style="color: #222222;">Sitemap: <span style="background-color: yellow;">http://www.subinsb.com/atom.xml?redirect=false&start-index=1&max-results=1000</span></span>
    </p>
  </blockquote>
  
  <p>
    <span style="color: #222222;">Replace The yellow background <b>URL</b> with your blog&#8217;s sitemap <b>URL </b>and click on <b>Save Changes</b>&nbsp;button :</span>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-D5ZGGJBQlBs/UkfHg6HBIYI/AAAAAAAADCQ/uRNuoLccAPw/s1600/0047.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-D5ZGGJBQlBs/UkfHg6HBIYI/AAAAAAAADCQ/uRNuoLccAPw/s1600/0047.png" /></a>
  </div>
  
  <p>
    <span style="color: #222222;">When the search engines crawl your site, they will get the <b>sitemap</b> <b>URL</b> from <b>robots.txt </b>and they will index all the <b>URL</b>&#8216;s in the sitemap.</span><br /><span style="color: #222222;">If you have any suggestions/problems/feedback say it out in the comments, I will help you.</span></div>