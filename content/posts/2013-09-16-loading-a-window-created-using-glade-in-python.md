---
title: Loading A Window Created Using GLADE in Python
author: Subin Siby
type: post
date: 2013-09-16T05:20:00+00:00
excerpt: 'If you have created a window using Glade&nbsp;and want to display this window on your Python&nbsp;program containing the event handlers of the Window, then you should follow this tutorial.This small code will load the Glade&nbsp;file and displays the w...'
url: /loading-a-window-created-using-glade-in-python
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
  - http://sag-3.blogspot.com/2013/09/load-window-created-using-glade-in-python.html
syndication_item_hash:
  - cc2b66fc9a6cc359448230b08edcfd0d
  - cc2b66fc9a6cc359448230b08edcfd0d
  - cc2b66fc9a6cc359448230b08edcfd0d
  - cc2b66fc9a6cc359448230b08edcfd0d
  - cc2b66fc9a6cc359448230b08edcfd0d
  - cc2b66fc9a6cc359448230b08edcfd0d
categories:
  - Linux
  - PyGTK
  - Python
  - Ubuntu
tags:
  - Glade
  - GTK
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you have created a window using <b>Glade</b>&nbsp;and want to display this window on your <b>Python</b>&nbsp;program containing the event handlers of the <b>Window</b>, then you should follow this tutorial.<br />This small code will load the <b>Glade</b>&nbsp;file and displays the window.<br />Note : The <b>Glade</b>&nbsp;Project file format should be <b>GtkBuilder</b>.<br />Now, let&#8217;s get down to the code. Create a&nbsp;<b>Python </b>file named <b>window.py</b>&nbsp;in the folder containing the glade file and put in the following contents :</p> 
  
  <blockquote class="tr_bq">
    <p>
      #!/usr/bin/python<br />import pygtk<br />pygtk.require("2.0")<br />import gtk<br />import gtk.glade<br />class SubinsWindow:<br />&nbsp;def __init__(self):<br />&nbsp; self.gladefile = "<span style="background-color: yellow;">window.glade</span>"<br />&nbsp; self.glade = gtk.Builder()<br />&nbsp; self.glade.add_from_file(self.gladefile)<br />&nbsp; self.glade.connect_signals(self)<br />&nbsp; self.win=self.glade.get_object("<span style="background-color: yellow;">window1</span>") # Window Name in GLADE<br />&nbsp; self.win.show_all()
    </p>
    
    <p>
      if __name__ == "__main__":<br />&nbsp;a = SubinsWindow()<br />&nbsp;gtk.main()
    </p>
  </blockquote>
  
  <p>
    The first <span style="background-color: yellow;">yellow backgrounded text</span>&nbsp;has the name of the <b>GLADE</b>&nbsp;file.<br />The second&nbsp;<span style="background-color: yellow;">yellow backgrounded text</span>&nbsp;has the name of the&nbsp;<b>GLADE </b>Window. Normally It would be <b>window1</b>.<br />Execute the <b>Python </b>program by opening a terminal with the folder and execute the following command:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      python window.py
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-GWeSNOSCe0o/UjaUsps12uI/AAAAAAAADA0/UqT5IzS_9A0/s1600/0044.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="185" src="https://2.bp.blogspot.com/-GWeSNOSCe0o/UjaUsps12uI/AAAAAAAADA0/UqT5IzS_9A0/s400/0044.png" width="400" /></a>
  </div>
  
  <p>
    Here is a sample window created using <b>GLADE</b>&nbsp;and executed in <b>Python </b>:
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-eTA7M5PSLi0/UjaT5nPllhI/AAAAAAAADAs/LQ_2KjD1l3s/s1600/0043.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-eTA7M5PSLi0/UjaT5nPllhI/AAAAAAAADAs/LQ_2KjD1l3s/s1600/0043.png" /></a>
  </div>
  
  <p>
    So, Now you know how to import a <b>GLADE</b>&nbsp;file in <b>Python</b>&nbsp;and display the window.<br />If you have any problems/suggestions/feedback just say it out in the comment box below.</div>