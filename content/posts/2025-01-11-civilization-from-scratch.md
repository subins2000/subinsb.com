---
title: Civilization from scratch
author: Subin Siby
type: post
draft: true
date: 2024-10-31T00:00:00+05:30
lastmod: 2024-11-24T00:00:00+05:30
url: /simple-analysis-of-kerala-place-names
aliases:
  - /civ
categories:
  - Personal
---

I started watching [The Walking Dead](https://en.wikipedia.org/wiki/The_Walking_Dead_\(TV_series\)) in the summer of 2017 and I was instantly hooked. I finished the last episode of the series this week. The final season explored advanced societies, administration of large number of people. While watching it, I had some thoughts that I'd like to write.

Humans are social animals. Humans like to group together based on some commonalities, primarily you can see this from friend groups. In a world of nothing, the commonality usually is the need of survival. As things progress, there will be quarrels among people. If the group is small, things can be sorted out with mediators that both parties know/trust.

Although things may look happy and shiny at the beginning, there is always the possibility of things going wrong. In real life I've seen this among siblings who were once tight-knit, friends who cheated, so on and on.

Take this scale to 1000 to 10,000 to 50,000. What do you do when problems happen?

## Law

Humans solved this by making laws. A set of rules that everyone in a group must obey. [10 Commandments](https://lifehopeandtruth.com/bible/10-commandments/the-ten-commandments/10-commandments-list/) of the Bible is an early example.

How do you make humans adhere to it? Fear is one way, religion is an advanced way. The idea of religion as a law is that when people fear the wrath in after-life, people will try to be good in current-life.

## Kings, Dictators

The usual form of administration is to have a central person. This can turn out good if the person doesn't get their ego into their heads, and try to be benevolent.

It can turn out bad for everyone if the person is bad in character or becomes bad later.

In the beginning, the king might be nice. But remember that people can change, power can get into people's heads.

A person's character is shaped by their experiences. No matter how many books one read, personal experiences gives you the most impactful learning.

A wise person will remember their learnings and act accordingly. But some will forget their past, and turns out to be bad later in life.

The usual succession in this administrative setup is through children. This is a huge bet, as some kids just turn out to be worse than their parents because of their lavish upbringing and not having the wisdom to understand it.

The point I want to make here is, humans can change, betting on a person and their family forever won't work out.

##
