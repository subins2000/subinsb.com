---
title: The Subins Project
author: Subin Siby
type: post
date: 2012-08-28T13:39:00+00:00
url: /the-subins-project
aliases:
  - /subins
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - 9c6266c0d3e1cfd6da04a7ccb0ba1790
  - 9c6266c0d3e1cfd6da04a7ccb0ba1790
  - 9c6266c0d3e1cfd6da04a7ccb0ba1790
  - 9c6266c0d3e1cfd6da04a7ccb0ba1790
  - 9c6266c0d3e1cfd6da04a7ccb0ba1790
  - 9c6266c0d3e1cfd6da04a7ccb0ba1790
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
  - http://sag-3.blogspot.com/2012/08/the-subins-project_400.html
tags:
  - Subins

---
[![](//3.bp.blogspot.com/-vuJXWoYeoGg/UDzDGoSOBjI/AAAAAAAABms/ASp6Tk9Douo/s640/fdprofile.png)](//3.bp.blogspot.com/-vuJXWoYeoGg/UDzDGoSOBjI/AAAAAAAABms/ASp6Tk9Douo/s1600/fdprofile.png)

[![](//4.bp.blogspot.com/-JGKtvKf9PsA/UDzDLZZKeoI/AAAAAAAABm0/MZLZ8b0OAy0/s320/fdshot.png)](//4.bp.blogspot.com/-JGKtvKf9PsA/UDzDLZZKeoI/AAAAAAAABm0/MZLZ8b0OAy0/s1600/fdshot.png)

[![](//4.bp.blogspot.com/-LDRKhSWaSMg/UDzDOR0T9hI/AAAAAAAABm8/atvhscZy7mw/s320/services.png)](//4.bp.blogspot.com/-LDRKhSWaSMg/UDzDOR0T9hI/AAAAAAAABm8/atvhscZy7mw/s1600/services.png)

[![](//1.bp.blogspot.com/-1R1rdS85QH8/UDzDQZO4iYI/AAAAAAAABnE/INQT_W0nGSo/s640/subins.png)](//1.bp.blogspot.com/-1R1rdS85QH8/UDzDQZO4iYI/AAAAAAAABnE/INQT_W0nGSo/s1600/subins.png)

[![](//3.bp.blogspot.com/-DR0tGvRYJpU/UDzDSoN2iXI/AAAAAAAABnM/zFptiZkQeRs/s640/subinschat.png)](//3.bp.blogspot.com/-DR0tGvRYJpU/UDzDSoN2iXI/AAAAAAAABnM/zFptiZkQeRs/s1600/subinschat.png)

[![](//1.bp.blogspot.com/-LdRhDkB_Tq8/UDzDg8LDj1I/AAAAAAAABnU/Jow91iXVtQA/s640/subinsgames.png)](//1.bp.blogspot.com/-LdRhDkB_Tq8/UDzDg8LDj1I/AAAAAAAABnU/Jow91iXVtQA/s1600/subinsgames.png)

[![](//1.bp.blogspot.com/-UE5OBbcpHuQ/UDzDixTKFmI/AAAAAAAABnc/44-E2Jyk9WA/s640/subinsinstant.png)](//1.bp.blogspot.com/-UE5OBbcpHuQ/UDzDixTKFmI/AAAAAAAABnc/44-E2Jyk9WA/s1600/subinsinstant.png)

[![](https://2.bp.blogspot.com/-LvaRQXqhiec/UDzDlSZWV2I/AAAAAAAABnk/LFhIAPjDaJg/s640/subinslogin.png)](https://2.bp.blogspot.com/-LvaRQXqhiec/UDzDlSZWV2I/AAAAAAAABnk/LFhIAPjDaJg/s1600/subinslogin.png)

[![](https://2.bp.blogspot.com/-sssTchInS_A/UDzDmzS6jZI/AAAAAAAABns/fTdhggSM3_0/s320/subinslogo.png)](https://2.bp.blogspot.com/-sssTchInS_A/UDzDmzS6jZI/AAAAAAAABns/fTdhggSM3_0/s1600/subinslogo.png)

[![](//4.bp.blogspot.com/-t-jGh6de-rE/UDzD0zyrUEI/AAAAAAAABn0/b3_VPQlzgcg/s640/subinsmusic.png)](//4.bp.blogspot.com/-t-jGh6de-rE/UDzD0zyrUEI/AAAAAAAABn0/b3_VPQlzgcg/s1600/subinsmusic.png)

[![](//4.bp.blogspot.com/-Nz6OFNfSFRs/UDzD4hd4qII/AAAAAAAABn8/C0u2Ssq6r7Y/s640/subinsquiz.png)](//4.bp.blogspot.com/-Nz6OFNfSFRs/UDzD4hd4qII/AAAAAAAABn8/C0u2Ssq6r7Y/s1600/subinsquiz.png)

[![](https://2.bp.blogspot.com/-OReAontMxLA/UDzD7-rnyAI/AAAAAAAABoE/-RqvIOqocEs/s640/subinssignup.png)](https://2.bp.blogspot.com/-OReAontMxLA/UDzD7-rnyAI/AAAAAAAABoE/-RqvIOqocEs/s1600/subinssignup.png)

[![](//3.bp.blogspot.com/-6AYCRjsvwxQ/UDzEDl9xFlI/AAAAAAAABoM/mflmbK6l1ck/s640/subinswdusource.png)](//3.bp.blogspot.com/-6AYCRjsvwxQ/UDzEDl9xFlI/AAAAAAAABoM/mflmbK6l1ck/s1600/subinswdusource.png)

[![](//4.bp.blogspot.com/-w6zeCCz_8DI/UDzEFqhvjTI/AAAAAAAABoU/RbepyIeEPJk/s320/sugeshot.png)](//4.bp.blogspot.com/-w6zeCCz_8DI/UDzEFqhvjTI/AAAAAAAABoU/RbepyIeEPJk/s1600/sugeshot.png)

## UPDATE - 2019

There are [some posts on Subins](/tags/subins/). But, I never wrote detailed about "The Subins Project". I don't know why. So I'm writing this for archival purposes.

I lost the source code of "The Subins Project" because I accidentally formatted by whole drive back in 2013 during an installation of Windows XP (their installer is very bad !).

I named it "Subins" seeing how Linux was named from its creator Linus. It had a

* Search engine
* Chat site (Google hangouts like)
* Social network site called **Friendshood** (like Facebook)
* Games site (like Chrome web store)
* Videos site (YouTube)
* Wiki site (like wikipedia)
* URL Shortener. [This was my first repo in GitHub](https://github.com/subins2000/shorturl), but it's not the exact site
* and some more (I don't remember)

I didn't know GitHub back then, else would have published the source code.

### Tech

It was created entirely with **PHP** and had design/style inspired (and copied ?) from Google, Facebook and elsewhere. I learnt web development with this project :

> Starting from a google search of `"How to create a website"` to `"How to host on AppFog"`.
>
> I spent countless hours working on it. I think I started this in **2011 November** or something and it got hosted in **2012 December**.
>
> I was in **7th & 8th grade** during the time.

The project was hosted online with help of [AppFog.com] (a PaaS provider) in 2012. They gave free hosting back then. [I wrote a blog post on it here](//subinsb.com/subins-project-online-thanks-to-app-fog/).

#### The code was a disaster

* I used **mysql_query()** for everything. The whole site was vulnerable to MySQL Injection and probably lots of other vulnerabilities.
  * One of my friend did point this to me and I took down the site cause all the user details was easily accessible.
* The code had bad indentation and coding standards. I kinda remember it. It was very ugly. YUCK !
* Passwords weren't hashed. DISASTER 💯

### What's Left

All that's left of "The Subins Project" are these screenshots and the archived webpages on archive.org :

* Search Engine - subins.hp.af.cm - https://web.archive.org/web/20130101000000*/http://subins.hp.af.cm:80/
* Accounts Management Site - accounts-subins.hp.af.cm - https://web.archive.org/web/*/http://accounts-subins.hp.af.cm:80
* Subins Chat - chat-subins.hp.af.cm - https://web.archive.org/web/*/http://chat-subins.hp.af.cm:80
* Subins Get - get-subins.hp.af.cm - https://web.archive.org/web/*/http://get-subins.hp.af.cm:80
* Friendshood - fd-subins.hp.af.cm - https://web.archive.org/web/*/http://fd-subins.hp.af.cm:80
* Subins Apps - apps-subins.hp.af.cm - https://web.archive.org/web/*/http://apps-subins.hp.af.cm:80

### Aftermath

* I went on to create a better social network with [Open](https://subinsb.com/open-an-open-source-social-network-is-released/)
* [I created a better search engine with a custom crawler](https://subinsb.com/search-engine-in-php-part-1/)
* I became a much better programmer !

And looking back now :

> Although it was a disaster, I learnt a LOT !
> 
> That's what matters at the end :)