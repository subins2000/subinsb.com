---
title: How to remove keyring password dialog in All Ubuntu versions
author: Subin Siby
type: post
date: 2013-06-26T12:34:00+00:00
excerpt: 'You will be getting a keyring unlocker&nbsp;dialog to type your password to unlock the keyrings such as:Password you stored in Web Browsers&nbsp;(Google Chrome, Chromium)Passwords stored in Chat and Mail Applications (Empathy, Pidgin, Evolution)and oth...'
url: /how-to-remove-keyring-password-dialog-in-all-ubuntu-versions
authorsure_include_css:
  - 
  - 
  - 
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
blogger_permalink:
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
  - http://sag-3.blogspot.com/2013/06/how-to-remove-keyring-password-dialog.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_item_hash:
  - a581bc51759ac6fe851bd28f7fc391cb
  - a581bc51759ac6fe851bd28f7fc391cb
  - a581bc51759ac6fe851bd28f7fc391cb
  - a581bc51759ac6fe851bd28f7fc391cb
  - a581bc51759ac6fe851bd28f7fc391cb
  - a581bc51759ac6fe851bd28f7fc391cb
categories:
  - Ubuntu
tags:
  - 10.04
  - Passwords

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You will be getting a <b>keyring unlocker</b>&nbsp;dialog to type your password to unlock the keyrings such as:</p> 
  
  <ul style="text-align: left;">
    <li>
      Password you stored in <b>Web Browsers</b>&nbsp;(Google Chrome, Chromium)
    </li>
    <li>
      Passwords stored in Chat and Mail Applications (Empathy, Pidgin, Evolution)
    </li>
    <li>
      and other passwords
    </li>
  </ul>
  
  <p>
    This passwords should be unlocked each time you login but won&#8217;t automatically unlock when you login. To automatically unlock you can do as follows:<br />Open Terminal (<b>CTRL&nbsp;+ ALT&nbsp;+ T</b>) and do the following command:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      seahorse
    </p>
  </blockquote>
  
  <p>
    You will get the <b>Passwords & Encryption Keys</b>&nbsp;window :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-5KK-VyQpIhs/UcnJQbsSIbI/AAAAAAAACyg/87ANmOEd-EQ/s1600/0018.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-5KK-VyQpIhs/UcnJQbsSIbI/AAAAAAAACyg/87ANmOEd-EQ/s1600/0018.png" /></a>
  </div>
  
  <p>
    Right click on the folder <b>Password: default</b>&nbsp;and choose <b>Change Password </b>:
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-gYkEGrm1Bd0/UcnJNQuxQGI/AAAAAAAACyc/H1tjfZRS-Q4/s1600/0019.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-gYkEGrm1Bd0/UcnJNQuxQGI/AAAAAAAACyc/H1tjfZRS-Q4/s1600/0019.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Type the old password you used to unlock the keyring and leave the other two fields empty and click on <b>OK</b>&nbsp;button.
  </div>
  
  <p>
    You will be asked to keep passwords unencrypted :<span id="goog_397668772"></span>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-kCmjCK_fKpw/UcrfIxH9vGI/AAAAAAAACy4/wjfVJS5H1n4/s1600/0020.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-kCmjCK_fKpw/UcrfIxH9vGI/AAAAAAAACy4/wjfVJS5H1n4/s1600/0020.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Click on <b>Use Unsafe Storage</b>&nbsp;and it&#8217;s done. The dialog won&#8217;t be seen again after you login.
  </div>
</div>