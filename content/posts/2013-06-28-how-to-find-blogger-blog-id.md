---
title: How to find Blogger Blog ID
author: Subin Siby
type: post
date: 2013-06-28T06:00:00+00:00
excerpt: "You may need your blogger blog id for various needs. Here's a simple tutorial on how to find the blog id.Go to your blog dashboard on Blogger. See the url of the page in the location bar.In the page url you can see a text blogID= followed by a number u..."
url: /how-to-find-blogger-blog-id
authorsure_include_css:
  - 
  - 
  - 
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_item_hash:
  - d936afad23a3debf6e4299447754b780
  - d936afad23a3debf6e4299447754b780
  - d936afad23a3debf6e4299447754b780
  - d936afad23a3debf6e4299447754b780
  - d936afad23a3debf6e4299447754b780
  - d936afad23a3debf6e4299447754b780
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
  - http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html
dsq_thread_id:
  - 1972435665
  - 1972435665
  - 1972435665
  - 1972435665
  - 1972435665
  - 1972435665
categories:
  - Blogger
tags:
  - Blog
  - ID
  - URL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You may need your <b>blogger blog id</b> for various needs. Here&#8217;s a simple tutorial on how to find the <b>blog id</b>.</p> 
  
  <div>
    Go to your blog dashboard on <b>Blogger</b>. See the url of the page in the location bar.
  </div>
  
  <div>
    In the page url you can see a text <b>blogID= </b>followed by a number until it stops with <b>#</b>.
  </div>
  
  <div>
    Here&#8217;s a sample url :
  </div>
  
  <blockquote class="tr_bq">
    <p>
      http://www.blogger.com/blogger.g?blogID=<span style="color: red;background:wheat;">123456789101112</span>#overview/src=dashboard
    </p>
  </blockquote>
  
  <div>
    The wheat color background text in the above url is the blog id of that blog.
  </div>
</div>