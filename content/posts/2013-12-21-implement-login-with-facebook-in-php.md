---
title: Implementing Login With Facebook In PHP
author: Subin Siby
type: post
date: 2013-12-21T07:04:15+00:00
url: /implement-login-with-facebook-in-php
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - CSS
  - Facebook
  - HTML
  - PHP
tags:
  - API
  - Facebook Oauth
  - Image
  - OAuth

---
In the last post, I introduced you to a **PHP OAuth Class** named **OAuth API**. In this post I&#8217;m going to tell you how to implement **Login With Facebook** feature in **PHP** using the class I introduced. It&#8217;s very simple. I will tell the ways of implementing step by step. This tutorial includes placing a Login with Facebook button on web site to authorization and finishes when we get user details. You should read the <a title="Implementing OAuth System Using OAuth API Library In PHP" href="//subinsb.com/2013/12/oauth-api-library-php" target="_blank">previous post</a> before starting this tutorial.

<div class="padlinks">
  <a class="demo" href="http://open.subinsb.com" target="_blank">DEMO</a>
</div>


# Appearance

<div>
  First of all, we will design our <strong>Login With Facebook button</strong>. Then we will move on to the server side. We are going to design the button using <strong>HTML</strong>, <strong>CSS</strong> and a small icon.
</div>

Place the below **HTML** code on the place where you want to show the button :

> <a href="login\_with\_facebook.php" id="login\_with\_facebook">Login With Facebook</a>

You should need a **Facebook** small logo for making the button attractive. I have included the image below :

<p style="text-align: center;">
  <a href="//lab.subinsb.com/projects/blog/uploads/2013/12/fb_icon.png"><img class="size-full wp-image-2118  aligncenter" style="background-color: black;" alt="fb_icon" src="//lab.subinsb.com/projects/blog/uploads/2013/12/fb_icon.png" width="28" height="28" /></a>
</p>

Here is the **CSS** code that will decorate the button. I have provided the image **URL** as **https://lab.subinsb.com/projects/blog/uploads/2013/12/fb_icon.png**

> #login\_with\_facebook{
  
> display: inline-block;
  
> height: 43px;
  
> margin: 0px;
  
> padding: 0px 20px 0px 52px;
  
> font-family: &#8216;Ubuntu&#8217;, sans-serif;
  
> font-size: 18px;
  
> font-weight: 400;
  
> color: #fff;
  
> line-height: 41px;
  
> background: #3b579d url(**https://lab.subinsb.com/projects/blog/uploads/2013/12/fb_icon.png**) no-repeat 14px 8px scroll;
  
> -webkit-border-radius: 4px;
  
> -moz-border-radius: 4px;
  
> border-radius: 4px;
  
> text-decoration: none;
  
> cursor:pointer;
  
> }

The finished button will look something like below :

[<img class="aligncenter size-full wp-image-2119" alt="The finished button" src="//lab.subinsb.com/projects/blog/uploads/2013/12/0068.png" width="245" height="46" />][2]

# Server Side

## login_with_facebook.php

This is the file that handles the authorization. You need the **oauth_client.php** which we mentioned in the <a title="Implementing OAuth System Using OAuth API Library In PHP" href="//subinsb.com/2013/12/oauth-api-library-php" target="_blank">previous post</a>. You need a **client id **and **client secret** which you get when you create an app @ <a href="https://developers.facebook.com/apps" target="_blank">https://developers.facebook.com/apps</a> (Tutorial <a href="//subinsb.com/2012/02/tutorial-how-to-create-a-facebook-app" target="_blank">here</a>).

Here is the complete code :

> <?php
  
> require(&#8216;http.php&#8217;);
  
> require(&#8216;oauth_client.php&#8217;);
  
> $client = new oauth\_client\_class;
  
> $client->server = &#8216;Facebook&#8217;;
  
> $client->debug = false;
  
> $client->debug_http = true;
  
> $client->redirect\_uri = &#8216;http://&#8217;.$\_SERVER[&#8216;HTTP\_HOST&#8217;].dirname(strtok($\_SERVER[&#8216;REQUEST\_URI&#8217;],&#8217;?&#8217;)).&#8217;/login\_with_facebook.php&#8217;;
  
> $client->client_id = ";/\*Client ID\*/
  
> $client->client_secret = ";/\*Client Secret\*/
  
> $client->scope = &#8217;email,user\_about\_me,user_birthday&#8217;;
  
> $application\_line = \\_\_LINE\_\_;
  
> if(strlen($client->client\_id) == 0 || strlen($client->client\_secret) == 0){
  
>  die(&#8216;Please go to Facebook Apps page https://developers.facebook.com/apps , &#8216;.
  
>   &#8216;create an application, and in the line &#8216;.$application_line.
  
>   &#8216; set the client\_id to App ID/API Key and client\_secret with App Secret&#8217;);
  
> }
  
> if(($success = $client->Initialize())){
  
>  if(($success = $client->Process())){
  
>   if(strlen($client->authorization_error)){
  
>    $client->error = $client->authorization_error;
  
>    $success = false;
  
>   }elseif(strlen($client->access_token)){
  
>    $success = $client->CallAPI(&#8216;https://graph.facebook.com/me&#8217;, &#8216;GET&#8217;, array(), array(&#8216;FailOnAccessError&#8217;=>true), $user);
  
>    if($success){
  
>     $email=$user->email;
  
>     $name=$user->name;
  
>     $gender=$user->gender;
  
>     $birthday=date(&#8216;d/m/Y&#8217;, strtotime($user->birthday));
  
>     $image=get\_headers("https://graph.facebook.com/me/picture?width=200&height=200&access\_token=".$client->access_token,1);
  
>     $image=$i[&#8216;Location&#8217;];
  
>    }
  
>   }
  
>  }
  
>  $success = $client->Finalize($success);
  
> }
  
> if($client->exit){
  
>  die("Something Happened","<a href='".$client->redirect_uri."&#8216;>Try Again</a>");
  
> }
  
> if(!$success){
  
> ?>
  
>  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  
>  <html>
  
>   <head>
  
>    <title>Error</title>
  
>   </head>
  
>   <body>
  
>    <h1>OAuth client error</h1>
  
>    <pre>Error: <?php echo HtmlSpecialChars($client->error); ?></pre>
  
>   </body>
  
>  </html>
  
> <?}?>

Don&#8217;t forget to replace **client_id** & **client_secret** values.

# How It Works

  1. When a user clicks **Login With Facebook** button, the user will be redirected to the **login\_with\_facebook.php** page.
  2. The **PHP** file will redirect the user to **Facebook**&#8216;s authorization page with all the app details needed by **Facebook**.
  3. When the user authorizes the app, he/she will be redirected back to **login\_with\_facebook.php** page with a **GET **parameter named **code**.
  4. **OAuth API** library will request an **access token** to **Facebook** with the given **code**.
  5. When it receives the **access token**, It sends a request to **https://graph.facebook.com/me** with the **access_token** and the **URL** will return the details of the user.
  6. It also sends a request to **https://graph.facebook.com/me/picture **with the access token and the image size needed (**200 x 200**).

# Variables

> $email &#8211; The E-Mail of the user
  
> $name &#8211; The Name of the user
  
> $gender &#8211; The Gender of the user
  
> $birthday &#8211; The Birthday of the user
  
> $image &#8211; The Image URL of the user

Isn&#8217;t this tutorial simple ?

If you encountered any problems or have a suggestion, please say it on the comments. I will be happy to respond.

 [1]: #appearance
 [2]: //lab.subinsb.com/projects/blog/uploads/2013/12/0068.png
 [3]: #server-side
 [4]: #login-with-facebookphp
 [5]: #how-it-works
 [6]: #variables
