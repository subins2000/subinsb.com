---
title: Friendshood Update new feature – Post to Facebook
author: Subin Siby
type: post
date: 2013-05-10T14:33:00+00:00
excerpt: 'I updated Friendshood. The new feature is that the user can post status update and photos to Facebook&nbsp;along with posting in Friendshood. I implemented this function using Facebook PHP SDK. You can download the SDK&nbsp;here.It was not quite easy. ...'
url: /friendshood-update-new-feature-post-to-facebook
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - e226ca49d3910656a8d7f1b1479e7b6c
  - e226ca49d3910656a8d7f1b1479e7b6c
  - e226ca49d3910656a8d7f1b1479e7b6c
  - e226ca49d3910656a8d7f1b1479e7b6c
  - e226ca49d3910656a8d7f1b1479e7b6c
  - e226ca49d3910656a8d7f1b1479e7b6c
syndication_permalink:
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
  - http://sag-3.blogspot.com/2013/05/friendshood-update-new-feature-post-to.html
categories:
  - Facebook
  - PHP
tags:
  - Facebook Oauth
  - FriendsHood
  - SDK

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  I updated <b><a href="http://fd-subins.hp.af.cm/" >Friendshood</a></b>. The new feature is that the user can post status update and photos to <b>Facebook</b>&nbsp;along with posting in <b><a href="http://fd-subins.hp.af.cm/" >Friendshood</a></b>. I implemented this function using <b>Facebook PHP SDK</b>. You can download the <b>SDK</b>&nbsp;<a href="http://get-subins.hp.af.cm/view.php?id=346" >here</a>.</p> 
  
  <p>
    It was not quite easy. It was hard to make <b>Picture</b>&nbsp;uploading function along with the normal status update. Next I&#8217;m gonna implement twitter posting from <a href="http://fd-subins.hp.af.cm/" >Friendshood</a>.
  </p>
  
  <p>
    I&#8217;m gonna tell you how to post a status update in the following posts. Wait for it.
  </p>
</div>