---
title: How To Create A Localhost Web Site In Linux Using Apache Web Server
author: Subin Siby
type: post
date: 2012-04-07T13:44:00+00:00
excerpt: "Video Tutorial  This tutorial will show you how to create a localhost site in Apache Server. Follow these steps.&nbsp;Note&nbsp;: If You don't have Apache Web Server you can't do these steps.   To install Apache Web Server press CTRL+ALT+T and type&nbs..."
url: /linux-apache-localhost
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/04/how-to-create-localhost-site-in-ubuntu.html
syndication_item_hash:
  - 09cd3efe7b8cdb2d959ebd1e2a54e528
  - 09cd3efe7b8cdb2d959ebd1e2a54e528
  - 09cd3efe7b8cdb2d959ebd1e2a54e528
  - 09cd3efe7b8cdb2d959ebd1e2a54e528
  - 09cd3efe7b8cdb2d959ebd1e2a54e528
  - 09cd3efe7b8cdb2d959ebd1e2a54e528
dsq_thread_id:
  - 823069314
  - 823069314
  - 823069314
  - 823069314
  - 823069314
  - 823069314
categories:
  - Linux
  - Localhost
  - Ubuntu
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;">
  <p>
    <span style="font-family: inherit;"><strong>Localhost</strong> is a hosting place in your own computer. There are a vast number of <strong>Localhost</strong> servers you can use. The most used and popular of them is <strong>Apache</strong>. Localhost enables you to test your work on your computer itself, instead of testing on a live server on the web. If you are creating a project, Localhost is the best place to test that project. In this tutorial I will show you how to create a localhost site running on <b>Apache Server</b> in any <strong>Linux Distribution</strong>.<br /> </span>
  </p>
  
  <p>
    This tutorial is for Ubuntu versions <strong>10.10</strong> and below. The latest versions from <strong>11.04</strong> should see <a href="//subinsb.com/ubuntu-linux-create-localhost-website">this tutorial.</a>
  </p>
  
  <h2>
    Apache Server Installation
  </h2>
  
  <p>
    <span style="font-family: inherit;">We need to install Apache server first. <span style="background-color: white; text-align: justify;">To install Apache Web Server open <strong>Terminal</strong> and do the following command :</span></span>
  </p>
  
  <pre class="prettyprint"><code>sudo apt-get install apache2</code></pre>
  
  <p>
    <span style="font-family: inherit;"><span style="background-color: white; text-align: justify;">The <strong>Apache Server</strong> will be downloaded and installed automatically. After installation continue to the next part of this tutorial.</span></span>
  </p>
  
  <h2>
    Site Creation
  </h2>
  
  <div>
    <span style="background-color: white; font-family: inherit; text-align: -webkit-auto;">Open the Terminal <b>CTRL + ALT </b>+ <strong>T</strong>.<b> </b>Make a copy of <b>/etc/apache2/sites-available/default</b>, let&#8217;s make the copy name <strong>mysite</strong>. (Name depends on the site you wish to create). The following command wil copy the file and name the new file as <strong>mysite</strong>.</span>
  </div>
  
  <div>
    <pre class="prettyprint"><code>sudo cp /etc/apache2/sites-available/default /etc/apache2/sites-available/&lt;span style="color: red;">mysite&lt;/span></code></pre>
  </div>
</div>

Now edit the new file to configure :

<pre class="prettyprint"><code>sudo gedit /etc/apache2/sites-available/&lt;span style="color: #ff0000;">mysite&lt;/span></code></pre>

<div>
  Then, Add the following contents in the file using <strong>gedit</strong> we just opened. Please read the <strong>3</strong> conditions below and then paste the content.
</div>

<div>
  <ol>
    <li>
      <span style="color: #ff6600;"><b>You will need to change the codes &#8216;username&#8217; shown below to your home folder name.</b></span>
    </li>
    <li>
      <span style="color: #ff6600;"><b style="color: red;"><span style="font-family: inherit;">You will need to change the names &#8216;mysite&#8217; shown below to your desired site name.</span></b></span>
    </li>
    <li>
      <span style="color: #ff6600;"><b>You can also change the web address of your site by renaming "mysite.com" to the site address you want.</b></span>
    </li>
  </ol>
</div>

<div>
  <p>
    Content that should be added to the file :
  </p>
  
  <pre class="prettyprint"><code>&lt;span style="font-family: inherit;">&lt;span class="tag">&lt;VirtualHost&lt;/span>&lt;span class="pln"> *:80&lt;/span>&lt;span class="tag">&gt;&lt;/span>&lt;/span>
 &lt;span class="pln"> ServerAdmin webmaster@localhost&lt;/span>
 &lt;span class="pln"> ServerName &lt;/span>&lt;span style="color: red;">&lt;span class="pln">mysite.com&lt;/span>&lt;/span>
 &lt;span class="pln"> DocumentRoot &lt;span style="color: red;">/home/username/mysite&lt;/span>&lt;/span>
 &lt;span class="pln"> &lt;/span>&lt;span class="tag">&lt;Directory &lt;/span>&lt;span style="color: red;">&lt;span class="pun">/&lt;/span>&lt;span class="atn">home&lt;/span>&lt;span class="pun">/&lt;/span>&lt;span class="atn">username&lt;/span>&lt;span class="pun">/&lt;/span>&lt;span class="atn">mysite&lt;/span>&lt;/span>&lt;span class="tag">/&gt;&lt;/span>
 &lt;span class="pln">  Options Indexes FollowSymLinks MultiViews&lt;/span>
 &lt;span class="pln">  AllowOverride All&lt;/span>
 &lt;span class="pln">  Order allow,deny&lt;/span>
 &lt;span class="pln">  allow from all&lt;/span>
 &lt;span class="pln"> &lt;/span>&lt;span class="tag">&lt;/Directory&gt;&lt;/span>
 &lt;span class="pln"> ErrorLog /var/log/apache2/&lt;/span>&lt;span style="color: red;">&lt;span class="pln">mysite&lt;/span>&lt;/span>&lt;span class="pln">/error.log&lt;/span>
 &lt;span class="pln"> # Possible values include: debug, info, notice, warn, error, crit,&lt;/span>
 &lt;span class="pln"> # alert, emerg.&lt;/span>
 &lt;span class="pln"> LogLevel warn&lt;/span>
 &lt;span class="pln"> CustomLog /var/log/apache2/&lt;/span>&lt;span style="color: red;">&lt;span class="pln">mysite&lt;/span>&lt;/span>&lt;span class="pln">/access.log combined&lt;/span>
 &lt;span class="tag">&lt;/VirtualHost&gt;&lt;/span></code></pre>
</div>

<span style="font-family: inherit;"><span style="background-color: white; font-family: inherit; text-align: -webkit-auto;">Once you&#8217;ve saved these changes, you&#8217;ll need to enable the site.</span> ( <b style="color: red;">You will need to change the name &#8216;mysite&#8217; shown below to the name of the file you create at "</b><b>/etc/apache2/sites-available/</b><b style="color: red;">" </b><span style="color: #000000;">).</span></span>

<pre class="prettyprint"><code>cd /etc/apache2/sites-available && sudo a2ensite &lt;span style="color: red;">mysite&lt;/span></code></pre>

<span style="font-family: inherit;">We specified a separate directory for storing log files, so we&#8217;ll need to create that log directory.( </span><span style="font-family: inherit;"><b style="color: red; text-align: left;">You have to change the name &#8216;mysite&#8217; shown below to the name you given in the configuration file on "</b><b>/etc/apache2/sites-available/</b><b style="color: red; text-align: left;">" </b><span style="color: #000000;">).</span></span>

<pre class="prettyprint"><code>sudo mkdir /var/log/apache2/&lt;span style="color: red;">mysite&lt;/span></code></pre>

<span style="font-family: inherit;"><span style="background-color: white; font-family: inherit; text-align: -webkit-auto;">Finally, we need to point the site address to localhost in your hosts file. For that, Edit <b>/etc/hosts</b> :</span></span>

<pre class="prettyprint"><code>sudo gedit /etc/hosts</code></pre>

and add the following at the end of the file. ( <span style="font-family: inherit;"><span style="color: red;"><b>You need to change the web address "mysite.com" shown below to your address given on the file in </b>sites-available <b>folder </b><span style="color: #000000;">).</span></span></span>

<pre class="prettyprint"><code>127.0.0.1 &lt;span style="color: red;">mysite.com&lt;/span></code></pre>

<span style="font-family: inherit;"><span style="background-color: white; font-family: inherit; text-align: -webkit-auto;">Now we restart Apache Server using Terminal to apply all the changes we made.</span></span>

<pre class="prettyprint"><code>sudo /etc/init.d/apache2 restart</code></pre>

<span style="background-color: white; font-family: inherit; text-align: -webkit-auto;">You can now store all your work in /home/<span style="color: red;">you home name</span>/<span style="color: red;">site name</span> and navigate to it by pointing your browser to the address you gave.</span>