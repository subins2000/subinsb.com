---
title: Upload your image to the web in just a minute without registering
author: Subin Siby
type: post
date: 2012-10-22T05:14:00+00:00
excerpt: "There are many websites for uploading images for various purposes. Some of the popular services are&nbsp;Picasa, Flickr&nbsp;etc....But you need an account for using the services shown above. If you don't want to waste any time and upload image in just..."
url: /upload-your-image-to-the-web-in-just-a-minute-without-registering
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 9e8d196b14a3742c46f68f03ae7d1bde
  - 9e8d196b14a3742c46f68f03ae7d1bde
  - 9e8d196b14a3742c46f68f03ae7d1bde
  - 9e8d196b14a3742c46f68f03ae7d1bde
  - 9e8d196b14a3742c46f68f03ae7d1bde
  - 9e8d196b14a3742c46f68f03ae7d1bde
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
  - http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
dsq_thread_id:
  - 895198104
  - 895198104
  - 895198104
  - 895198104
  - 895198104
  - 895198104
categories:
  - Blogger
tags:
  - Image
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  There are many websites for uploading images for various purposes. Some of the popular services are&nbsp;<b>Picasa</b>, <b>Flickr</b>&nbsp;etc&#8230;.<br />But you need an account for using the services shown above. If you don&#8217;t want to waste any time and upload image in just a minute, then there a re some services which will upload your image in their servers. In this tutorial I will introduce to a service called&nbsp;<b><a href="http://www.postimage.org/" >Post Image</a>.</b><br />Go to <b><a href="http://www.postimage.org/" >Post Image</a>&nbsp;(</b><a href="http://www.postimage.org/">http://www.postimage.org/</a><b>)</b>.</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-qCi-UIUZk_I/UITQreZDyjI/AAAAAAAACD4/LKxxlthvBbw/s1600/postimage.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="284" src="//3.bp.blogspot.com/-qCi-UIUZk_I/UITQreZDyjI/AAAAAAAACD4/LKxxlthvBbw/s640/postimage.png" width="640" /></a>
  </div>
  
  <p>
    Choose the image file. Choose whether you want to resize your image. After completing the form. Submit it by clicking <b>Upload</b>&nbsp;<b>It</b>.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-gWc7jf4k9L4/UITR6YlCVXI/AAAAAAAACEA/e5sXDbCu034/s1600/uploading.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="153" src="//1.bp.blogspot.com/-gWc7jf4k9L4/UITR6YlCVXI/AAAAAAAACEA/e5sXDbCu034/s400/uploading.png" width="400" /></a>
  </div>
  
  <p>
    Your image will start to upload. After uploading Copy the image URL in the <b>Direct Link</b> field.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-PKVNF1xTlxs/UITSxn9UcPI/AAAAAAAACEI/kFC3pB5UOV0/s1600/urlpostimage.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="194" src="//4.bp.blogspot.com/-PKVNF1xTlxs/UITSxn9UcPI/AAAAAAAACEI/kFC3pB5UOV0/s640/urlpostimage.png" width="640" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    That&#8217;s it. Now you got the URL of the image. You can use this URL to show the image on your website or blog using the <b>IMG</b>&nbsp;tag.
  </div>
</div>