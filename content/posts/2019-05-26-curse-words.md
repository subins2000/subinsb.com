---
title: "Curse Words"
type: post
date: 2019-05-26T23:45:28+05:30
url: /curse-words
aliases:
    - /myre
categories:
  - Personal
tags:
  - malayalam
  - manglish
---
I have an interesting story of how I learnt & used a curse word.

## Myru

In Malayalam, a main curse word is "myru" which translates to "pubic hair". In Tamil, it simply means "hair". You can just go to a barber shop and say "annaa, cut my myru", but in Malayalam this sentence is hilarious 🤣.

When you refer the word to someone, it becomes "myre". It's like calling someone bro, but in the worst way possible :

    eda myre = hey pubic hair

This word is weird.

[Urban dictionary calls it the Malayalam equivalent of f\*ck](https://www.urbandictionary.com/define.php?term=Myru).

## Story

I'm writing this because of [this meme](https://www.instagram.com/p/Bx7HfOzlO5V/) I saw earlier today :

![myre meme](//lab.subinsb.com/projects/blog/uploads/2019/05/myre-meme.jpg)

"been there done that"

When I was 12 (2012), there was this new word in school. I thought it was just a one more level upwards of those normie words like "patti" `പട്ടി` (dog), "thendi" `തെണ്ടി` (beggar) stuff. I never bothered to know what its meaning was. I was just happy that I'm not missing out on stuff (high school eh).

Some months after that, we were all in a family get-together. It was during nighttime. The room had us kids (8 of us cousins) just simply sitting (y'know the time before the smarthphones). My uncle was sitting and peeling onions. Amma was in the next room doing something. I remember all this vividly. I'll never forget this wonderful experience.

I have a sister (cousin to be exact) named "Ammu". I'm called Appu in home. She's just 6 months younger than me. We fight all the time like any other siblings do. This time, somehow we ended up calling each other words.

It started from "patti", went to thendi and then to some others. BTW, my "bad words" dictionary goes like this :

> patti thendi pulle naaye chette naari

After all these words we knew ended, it was my turn to call her something. And BEHOLD, THE LATEST WORD THAT NO ONE KNEW...

The room quickly got silent. Everyone looked at me in a weird way. I heard Amma who were in the other room saying

> Apputtaaaa, enthaadaa nee ee parayunne. Evidunnu kitti ninakkithokke

in the most exclamatory way possible ! Kinda like this (+/- 30%) :

<center>
    <video src="//lab.subinsb.com/projects/blog/uploads/2019/05/pizhachu-poyi-meme.mp4" controls="controls"></video>
</center>

I got frightened, my heart was racing. Damn, I remember every little detail.

My uncle still sat there peeling the onions. My older brothers were laughing trying to easen the mood. They were too "enthaaanu appuuu".

Meanwhile Ammu went around asking what the meaning of the word was which I didn't know too. I pleaded before amma "I DON'T KNOW THE MEANING MAAA". She was like "ente mon pizhachu poyeeee".

After the incident, I asked my friend Vishnu Manoj the meaning of the word. Well... he enlightened me. And have I never ever ever used the word... until college.

> aadyam arinja theriyum ath padippichu thannavanem aarum marakkillaaa 😌

Few years later we all went to watch a football game of Kerala Blasters. Blasterd were playing really poor and I heard the word there again. But this time it was my uncle 😂 who stood silent before peeling the onions. It was a surprise to us all, we have never heard him swearing before. As we grow up, we see new things and discover new faces. The elders too were kids once eh 😁.
## Now

Fast forward to now, Ammu cerainly hasn't forgotten the incident nor did anyone else of us cousins. I asked my mom if she remembers it, nope she doesn't.

Ammu, I taught you the word. I regretted it during the time and later. But, not now.

> Nee enthaayalum arinjene, ithithiri nerathe aayenne ullu 🥴

> ingane paranju samaadhaanikkaale :P

You would have eventually learnt anyway kiddo 🥴.

Looking back, it's just a funny moment and we can have a laugh on this forever 😂