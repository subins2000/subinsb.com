---
title: Check whether request is made from Ajax – PHP
author: Subin Siby
type: post
date: 2013-07-16T03:30:00+00:00
excerpt: |
  To check whether a request has been made is from Ajax&nbsp;or not you can use&nbsp;$_SERVER['HTTP_X_REQUESTED_WITH']. See the following example:&lt;?phpif(strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest'){&nbsp;echo "The request is from ...
url: /check-whether-request-is-made-from-ajax-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
  - http://sag-3.blogspot.com/2013/07/check-whether-request-is-made-from-ajax.html
syndication_item_hash:
  - f4f241557aeb5d6fcc5e35aa1f154bb2
  - f4f241557aeb5d6fcc5e35aa1f154bb2
  - f4f241557aeb5d6fcc5e35aa1f154bb2
  - f4f241557aeb5d6fcc5e35aa1f154bb2
  - f4f241557aeb5d6fcc5e35aa1f154bb2
  - f4f241557aeb5d6fcc5e35aa1f154bb2
categories:
  - AJAX
  - PHP
tags:
  - HTTP
  - IF
  - Server
  - XML

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  To check whether a request has been made is from <b>Ajax</b>&nbsp;or not you can use&nbsp;<b>$_SERVER[&#8216;HTTP_X_REQUESTED_WITH&#8217;]</b>. See the following example:</p> 
  
  <blockquote class="tr_bq">
    <p>
      <?php<br />if(strtolower($_SERVER[&#8216;HTTP_X_REQUESTED_WITH&#8217;])==&#8217;xmlhttprequest&#8217;){<br />&nbsp;echo "The request is from an Ajax Call";<br />}else{<br />&nbsp;echo "The request is not from an Ajax Call";<br />}<br />?>
    </p>
  </blockquote>
  
  <p>
    There you go. You now know how to check whether request made to file is from an <b>Ajax</b>&nbsp;Call or not in <b>PHP</b>.</div>