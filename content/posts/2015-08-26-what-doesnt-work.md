---
title: What Doesn’t Work ?
author: Subin Siby
type: post
date: 2015-08-26T05:58:01+00:00
url: /what-doesnt-work
categories:
  - Personal

---
I had to write this post, because I have experienced this a lot.

When something doesn&#8217;t work especially with code, the asker won&#8217;t say the details. He/she just says it doesn&#8217;t work. Then it is the duty of mine to find what doesn&#8217;t work.

To simplify things, when you ask please explain :

  1. <div id="toc_container" class="no_bullets">
      <p class="toc_title">
        Contents
      </p>
      
      <ul class="toc_list">
        <li>
          <a href="#what-doesn8217t-work"><span class="toc_number toc_depth_1">1</span> What doesn&#8217;t work ?</a>
        </li>
        <li>
          <a href="#any-errors"><span class="toc_number toc_depth_1">2</span> Any Errors ?</a>
        </li>
        <li>
          <a href="#code-samples"><span class="toc_number toc_depth_1">3</span> Code Samples</a>
        </li>
        <li>
          <a href="#explain-clearly"><span class="toc_number toc_depth_1">4</span> Explain Clearly</a>
        </li>
      </ul>
    </div>
    
    ### What doesn&#8217;t work ?
    
    Expected Result vs Actual Result comparison</li> 
    
      * ### Any Errors ?
        
        Does the compiler or interpreter (as in case of PHP) output any errors. For seeing the errors, you must first enable Error Logging. A sample of the log file would greatly help.</li> 
        
          * ### Code Samples
            
            Have you made any changes to the code which would result in the error ? If so, please post the code using <a href="http://pastebin.com" target="_blank">Pastebin</a> or <a href="http://paste.ubuntu.com" target="_blank">Ubuntu Paste</a></li> 
            
              * ### Explain Clearly
                
                The most important thing is to explain clearly about your problem. If you don&#8217;t know English well, say in your own language and translate it using <a href="http://translate.google.com" target="_blank">Google</a> to English.</li> </ol> 
                
                This post will be used as reference when queries are asked that are not able to be understood by me.

 [1]: #what-doesn8217t-work
 [2]: #any-errors
 [3]: #code-samples
 [4]: #explain-clearly