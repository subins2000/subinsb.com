---
title: Use uTorrent Without Installing (Portable uTorrent)
author: Subin Siby
type: post
date: 2013-12-24T14:53:07+00:00
url: /use-utorrent-without-installing-portable-utorrent
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - Windows
tags:
  - EXE
  - Portable
  - Software

---
Torrents are the most common thing computer users hear of. There are torrent downloading, torrent sharing and many others. Torrent has become a business in the **WWW**. Due to the increase in demand of torrents, there should be torrent clients that downloads the files of a torrent. The most popular of them is **uTorrent** &#8211; A **Windows** package. **uTorrent** is widely used by computer enthusiasts. **uTorrent** is widely used because of it&#8217;s lightness, simple usage and limitless options. But, you have to install **uTorrent**to use it&#8217;s features. What if, your friend needs a download from torrent and he doesn&#8217;t have a torrent client. He don&#8217;t know how to install a torrent client. In this case, the only way is to use a **Portable Torrent Client**. Since **uTorrent** is light weight and easy to use, it&#8217;s the best torrent client to give to anyone.

In this post I will tell you how to make a **uTorrent** software package portable.

First of all download **uTorrent** from [www.utorrent.com][1].

[<img class="aligncenter size-medium wp-image-2148" alt="0070" src="//lab.subinsb.com/projects/blog/uploads/2013/12/0070-300x153.png" width="300" height="153" />][2]

The downloaded file&#8217;s name is **utorrent.exe**. Move the file to an empty location (folder).

Create a file named **settings.dat** on the location where **utorrent.exe **is located.

Open **utorrent.exe**.

Some files and folders will be created in the directory where **utorrent.exe** is located.

[<img class="aligncenter size-full wp-image-2149" alt="0071" src="//lab.subinsb.com/projects/blog/uploads/2013/12/0071.png" width="166" height="205" />][3]

The folder where all of these files with **utorrent.exe** is the **Portable uTorrent**. You can give this folder to your friends and others and they can use **uTorrent** without installing.

 [1]: http://www.utorrent.com/
 [2]: //lab.subinsb.com/projects/blog/uploads/2013/12/0070.png
 [3]: //lab.subinsb.com/projects/blog/uploads/2013/12/0071.png