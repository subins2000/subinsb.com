---
title: 'Advanced Live Group Chat With WebSocket, PHP & jQuery'
author: Subin Siby
type: post
date: 2015-06-09T03:30:51+00:00
url: /php-websocket-advanced-chat
enclosure:
  - |
    |
        http://demos.subinsb.com/php/advanced-chat-websocket/assets/message.wav
        23448
        audio/wav
        
categories:
  - HTML
  - HTML5
  - JavaScript
  - jQuery
  - PHP
  - Program
  - WebSocket
tags:
  - WebSocket

---
**WebSocket** is one of the awesomest part of the **HTML5** and we developers should experiment with it as it will be the future of client to server communication.

In a recent post, I made a tutorial of <a href="//subinsb.com/live-group-chat-with-php-jquery-websocket" target="_blank">live chat with WebSockets and PHP</a>. It was the basic step into websockets. But, I have created another chat which is more advanced that has the capability to **upload images** as well as **record audio** from microphone of the user. It&#8217;s somewhat like **WhatsApp** or **Facebook Messenger**.

<div class="padlinks">
  <a class="demo" href="//demos.subinsb.com/php/advanced-chat-websocket" target="_blank">Demo</a><a class="download" href="//demos.subinsb.com/down.php?class=40" target="_blank">Download</a>
</div>

For this, you will need a **WebSocket** server and a web server (localhost).


# Features

  * Basic text messaging
  * Anyone can join chat
  * Voice messages
  * Send images
  * Live messaging

This tutorial has two parts, one for setting up the website and the other for setting up the WebSocket server.

# Website

The client doesn&#8217;t necessarily need to be PHP, because we aren&#8217;t gonna process the data here. But, since we are uploading images as well as audio, we should make files in the client site itself to upload these files.

## index.php

This is the file the users would see to chat :

<pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
  &lt;head&gt;
    &lt;script src="//lab.subinsb.com/projects/jquery/core/jquery-2.1.1.js"&gt;&lt;/script&gt;
    &lt;script src="//lab.subinsb.com/projects/Francium/voice/recorder.js"&gt;&lt;/script&gt;
    &lt;script src="//lab.subinsb.com/projects/Francium/voice/Fr.voice.js"&gt;&lt;/script&gt;
    
    &lt;script src="js/time.js"&gt;&lt;/script&gt;
    &lt;script src="js/chat.js"&gt;&lt;/script&gt;
    &lt;link href="css/chat.css" rel="stylesheet"/&gt;
    &lt;title&gt;Subin's Blog - Advanced Live Group Chat&lt;/title&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;div id="content"&gt;
      &lt;center&gt;&lt;h1&gt;Advanced Live Group Chat&lt;/h1&gt;&lt;/center&gt;
      &lt;p&gt;&lt;a href="//subinsb.com/php-websocket-advanced-chat"&gt;Tutorial&lt;/a&gt;&lt;/p&gt;
      &lt;div class="chatWindow"&gt;
        &lt;div style="display: none;postion: absolute;"&gt;
          &lt;input type="file" id="photoFile" accept="image/*" /&gt;
          &lt;audio src="cdn/message.wav" controls="false" id="notification"&gt;&lt;/audio&gt;
        &lt;/div&gt;
        &lt;div class="users"&gt;&lt;/div&gt;
        &lt;div class="chatbox"&gt;
          &lt;div class="topbar"&gt;
            &lt;span id="status" title="Click to Login/Logout"&gt;Offline&lt;/span&gt;
            &lt;span id="fullscreen" title="Toggle Fullscreen of Chat Box"&gt;Fullscreen&lt;/span&gt;
          &lt;/div&gt;
          &lt;div class="chat"&gt;
            &lt;div class="msgs"&gt;&lt;/div&gt;
            &lt;form id="msgForm"&gt;
              &lt;textarea name="msg" placeholder="Type message here...."&gt;&lt;/textarea&gt;
              &lt;a class="button" id="voice" title="Click to start recording message"&gt;&lt;/a&gt;
              &lt;a class="button" id="photo" title="Type in a message and choose image to send"&gt;&lt;/a&gt;
            &lt;/form&gt;
          &lt;/div&gt;
          &lt;div class="login"&gt;
            &lt;p&gt;Type in your name to start chatting !&lt;/p&gt;
            &lt;form id="loginForm"&gt;
              &lt;input type="text" value="" /&gt;
              &lt;button&gt;Submit&lt;/button&gt;
            &lt;/form&gt;
          &lt;/div&gt;
        &lt;/div&gt;
      &lt;/div&gt;
    &lt;/div&gt;
  &lt;/body&gt;
&lt;/html&gt;</code></pre>

It has a basic layout with some resources loaded in the head.

## upload.php

This file handles the uploads such as images and audios. It will upload the file into a folder named "uploads" in the root directory of the site. So, you should create that folder and set the permission of it to "Read & Write".

Here is the upload.php file :

<pre class="prettyprint"><code>&lt;?php
if(isset($_FILES['file'])){
  $file = file_get_contents($_FILES['file']['tmp_name']);
  $f = finfo_open();
  $mime_type = finfo_buffer($f, $file, FILEINFO_MIME_TYPE);
  
  $supported_types = array(
    "audio/x-wav" =&gt; "wav",
    "image/png" =&gt; "png",
    "image/jpeg" =&gt; "jpg",
    "image/pjpeg" =&gt; "jpg",
    "image/gif" =&gt; "gif"
  );
  
  $extension = isset($supported_types[$mime_type]) ? $supported_types[$mime_type] : false;
  if($extension !== false){
    $fileName = rand(10000, 65000) . "." . $extension;
    $location = __DIR__ . "/uploads/" . $fileName;
    
    move_uploaded_file($_FILES['file']['tmp_name'], $location);
    echo $fileName;
  }
}</code></pre>

You can see that the upload file checks the type of the file and then only it uploads to the server. When it is uploaded, the file name is outputted.

## 

## js/time.js

You might have heard about the **jQuery** **Timeago** plugin. This file has the plugin with some little modifications. Download it from <a href="http://demos.subinsb.com/php/advanced-chat-websocket/js/time.js" target="_blank">here</a>.

## js/chat.js

This is the heart of the chat. All functions related to the chat in the client are processed by this file such as connection to the server, sending messages, uploading files and others&#8230;

You can get this file from [here][7].

## 

## css/chat.css

This file will decorate the chat box. We are adding just a simple design.

You can get this file from [here][9].

## assets/message.wav

There is something else too in this chat. A notification sound. When new messages are retrieved, a sound will be also played. You can download this file from <a href="http://demos.subinsb.com/php/advanced-chat-websocket/assets/message.wav" target="_blank">here</a>. Thanks to the open source software **Pidgin IM** for this sound.

## assets

The "assets" folder should contain 3 images :

  * <a href="//demos.subinsb.com/php/advanced-chat-websocket/cdn/img/camera.png" target="_blank">camera.png</a>
  * <a href="//demos.subinsb.com/php/advanced-chat-websocket/cdn/img/mic.png" target="_blank">mic.png</a>
  * <a href="//demos.subinsb.com/php/advanced-chat-websocket/cdn/img/mic-active.png" target="_blank">mic-active.png</a>

These files are for the advanced part of the chat. With advancement comes more resources !

# WebSocket Server

Let&#8217;s move on to the **WebSocket** **Server**. This part handles the messages and does all the work to make the chat functional and awesome.

## Database

The server has the **Database**, so create the tables first :

<pre class="prettyprint"><code>CREATE TABLE `wsAdvancedChat` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user` varchar(20) NOT NULL,
  `msg` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
</code></pre>

Make sure the database <a href="http://stackoverflow.com/questions/5359495/how-to-change-a-mysql-database-to-utc" target="_blank">time zone is set to <strong>UTC</strong>.</a>

Then you should create a configuration file that will provide the credentials to connect to the database :

## config.php

We are going to use **PDO** for the connection with **MySQL** :

<pre class="prettyprint"><code>&lt;?php
ini_set("display_errors","on");
$docRoot  = realpath(dirname(__FILE__));

if( !isset($dbh) ){
  session_start();
  date_default_timezone_set("UTC");
  $musername = "root";
  $mpassword = "password";
  $hostname  = "127.0.0.1";
  $dbname    = "test";
  $port      = "3306";

  $dbh = new PDO("mysql:dbname={$dbname};host={$hostname};port={$port}", $musername, $mpassword);
  /**
   * Change The Credentials to connect to database.
   */
}
?&gt;
</code></pre>

We also set the timezone to **UTC** which is the same for database server and PHP server.

We will use **Francium DiffSocket** to set up our WebSocket server. In the "server" directory, run :

<pre class="prettyprint"><code>composer require francium/diffsocket
</code></pre>

Once DiffSocket is installed, we should set up the server class and the script to run the server.

## server/AdvancedChatServer.php

This file gets the sent message from server and inserts it into database. It also sends the messages in the database to the users when asked. In fact, this is the server who does everything for its clients.

You can get this file from [here][16].

## server/run-server.php

To add our "Advanced Chat" service and start the server, we must create another file :

<pre class="prettyprint"><code>&lt;?php
require_once __DIR__ . "/config.php";
require_once __DIR__ . "/vendor/autoload.php";

$ip = "127.0.0.1";
$port = "8000";
  
$DS = new Fr\DiffSocket(array(
  "host" =&gt; $ip,
  "port" =&gt; $port
));
$server-&gt;run();
</code></pre>

If you&#8217;re going to change the **$ip** and **$port** variables, you should also change the WebSocket URL in the "chat.js" file.

When you need to start the server, just run the "run-server.php" file :

<pre class="prettyprint"><code>php run-server.php</code></pre>

When new users are connected, a message will be shown in the shell that is something like this :

<pre class="prettyprint"><code>New connection! (29)</code></pre>

Each users connected (clients) will have a unique ID.

Open the website we set up, enter your name and voila !

There you go ! You just set up your own advanced chat system.

 [1]: #features
 [2]: #website
 [3]: #indexphp
 [4]: #uploadphp
 [5]: #jstimejs
 [6]: #jschatjs
 [7]: https://github.com/subins2000/ws-advanced-live-group-chat/blob/master/js/chat.js
 [8]: #csschatcss
 [9]: https://github.com/subins2000/ws-advanced-live-group-chat/blob/master/css/chat.css
 [10]: #assetsmessagewav
 [11]: #assets
 [12]: #websocket-server
 [13]: #database
 [14]: #configphp
 [15]: #serveradvancedchatserverphp
 [16]: https://github.com/subins2000/ws-advanced-live-group-chat/blob/master/server/AdvancedChatServer.php
 [17]: #serverrun-serverphp
