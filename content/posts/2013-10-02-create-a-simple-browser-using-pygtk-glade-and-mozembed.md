---
title: Create a Simple Browser Using PyGTK, GLADE And MozEmbed
author: Subin Siby
type: post
date: 2013-10-02T06:47:00+00:00
excerpt: "Let's start creating softwares using&nbsp;Python GTK (PyGTK). Python&nbsp;is the easiest programming language ever and If you are coming from other Programming Languages such as PHP, Here is something that you should understand :Python&nbsp;doesn't hav..."
url: /create-a-simple-browser-using-pygtk-glade-and-mozembed
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 5259286298d1fdf7e67e8ddddfde82ad
  - 5259286298d1fdf7e67e8ddddfde82ad
  - 5259286298d1fdf7e67e8ddddfde82ad
  - 5259286298d1fdf7e67e8ddddfde82ad
  - 5259286298d1fdf7e67e8ddddfde82ad
  - 5259286298d1fdf7e67e8ddddfde82ad
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
  - http://sag-3.blogspot.com/2013/10/create-simple-browser-using-pygtk-glade.html
dsq_thread_id:
  - 1958879551
  - 1958879551
  - 1958879551
  - 1958879551
  - 1958879551
  - 1958879551
categories:
  - PyGTK
  - Python
tags:
  - Glade
  - GTK

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Let&#8217;s start creating softwares using&nbsp;<b>Python GTK </b>(<b>PyGTK</b>). <b>Python</b>&nbsp;is the easiest programming language ever and If you are coming from other <b>Programming Languages </b>such as <b>PHP</b>, Here is something that you should understand :</p> 
  
  <div>
    <ol style="text-align: left;">
      <li>
        <b>Python</b>&nbsp;doesn&#8217;t have <b>semi-colons</b>&nbsp;for closing a code.
      </li>
      <li>
        <b>function</b>&nbsp;in <b>PHP</b>&nbsp;is <b>def</b>&nbsp;in <b>Python.</b>
      </li>
      <li>
        Spaces from the starting of a line decides the <b>wrapper</b>&nbsp;of the code.
      </li>
      <li>
        <b>{</b>&nbsp;in <b>PHP</b>&nbsp;is <b>:</b>&nbsp;in <b>Python.</b>
      </li>
      <li>
        There is no closing <b>}</b>&nbsp;in <b>Python</b>.
      </li>
      <li>
        Since there is no closing attribute ("<b>;</b>"), <b>2</b>&nbsp;or more code can&#8217;t be placed in a single line.
      </li>
      <li>
        <b>Python </b>doesn&#8217;t need variables starting with <b>$</b>.
      </li>
    </ol>
  </div>
  
  <div>
    The <b>Browser</b>&nbsp;we are going to create is built using <b>GTK 2</b>. The latest version of <b>GTK</b>&nbsp;is <b>GTK 3</b>.
  </div>
  
  <div>
    We are going to use <b>GLADE</b>&nbsp;for designing the interface. If you don&#8217;t have <b>GLADE</b>&nbsp;installed, install it using the following command :</p> 
    
    <blockquote class="tr_bq">
      <p>
        sudo apt-get install glade
      </p>
    </blockquote>
    
    <p>
      Let&#8217;s begin. First of all create a folder named <b>browser</b>&nbsp;on your <b>home</b> folder.<br />Create a new project in&nbsp;<b>GLADE</b>&nbsp;with the <b>Project File Format</b>&nbsp;as <b>GtkBuilder </b>:
    </p>
    
    <div class="separator" style="clear: both; text-align: center;">
      <a href="//1.bp.blogspot.com/-j7Z4wUCuAk0/Ukuq_mGcKLI/AAAAAAAADEA/ckE8w8y6yJ4/s1600/0051.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-j7Z4wUCuAk0/Ukuq_mGcKLI/AAAAAAAADEA/ckE8w8y6yJ4/s1600/0051.png" /></a>
    </div>
    
    <p>
      Save the project as <b>window.glade</b>&nbsp;in <b>browser</b>&nbsp;folder we created in <b>home </b>folder.<br />Create a Window by clicking on <b>Window</b>&nbsp;button in <b>Toplevels</b>&nbsp;section on the toolbox. Choose <b>Signals</b>&nbsp;and add <b>remove</b>&nbsp;signal of <b>GtkContainer</b>&nbsp;to&nbsp;<b>on_MainWindow_close_event</b>&nbsp;:
    </p>
    
    <div class="separator" style="clear: both; text-align: center;">
      <a href="//3.bp.blogspot.com/-QmJa9RgpBS8/Uku5E1QiqoI/AAAAAAAADEo/ZHgCP6TWBSk/s1600/0053.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-QmJa9RgpBS8/Uku5E1QiqoI/AAAAAAAADEo/ZHgCP6TWBSk/s1600/0053.png" /></a>
    </div>
    
    <p>
      Choose <b>Vertical Box</b>&nbsp;from the <b>Containers</b>&nbsp;and click on the blank space of the <b>window1</b>. The <b>Number Of Items</b>&nbsp;should be <b>2</b>. The <b>window1</b>&nbsp;would be divided into <b>2&nbsp;</b>now.
    </p>
    
    <p>
      Choose <b>Tool Bar</b>&nbsp;from the <b>Containers</b>&nbsp;and click on the <b>1</b>st division of&nbsp;<b>window1</b>.<br />Right click on <b>toolbar1</b>&nbsp;and choose <b>Edit</b>&nbsp;option. Toggle to <b>Hierarchy</b>&nbsp;tab and add the following Buttons :
    </p>
    
    <blockquote class="tr_bq">
      <table class="table">
        <tr>
          <td>
            Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>new</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Label
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>New</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Icon Name
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>list-add</b>
          </td>
        </tr>
      </table>
    </blockquote>
    
    <blockquote class="tr_bq">
      <table class="table">
        <tr>
          <td>
            Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>stop</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Label
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>Stop</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Icon Name
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>view-refresh</b>
          </td>
        </tr>
      </table>
    </blockquote>
    
    <blockquote class="tr_bq">
      <table class="table">
        <tr>
          <td>
            Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>reload</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Label
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>Reload</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Icon Name
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>edit-delete</b>
          </td>
        </tr>
      </table>
    </blockquote>
    
    <blockquote class="tr_bq">
      <table class="table">
        <tr>
          <td>
            Name &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>urlcont</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Type
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>Custom</b>
          </td>
        </tr>
        
        <tr>
          <td>
            Expand
          </td>
          
          <td>
            :
          </td>
          
          <td>
            <b>Yes</b>
          </td>
        </tr>
      </table>
    </blockquote>
    
    <div class="separator" style="clear: both; text-align: left;">
      Choose <b>Text Entry</b>&nbsp;from <b>Control and Display</b>&nbsp;and click on empty space of <b>urlcont</b>&nbsp;of <b>toolbar1</b>.
    </div>
    
    <div class="separator" style="clear: both; text-align: center;">
      The Current Window Will Look Like This:
    </div>
    
    <div class="separator" style="clear: both; text-align: center;">
    </div>
    
    <div class="separator" style="clear: both; text-align: center;">
      <a href="https://2.bp.blogspot.com/-vfrjGagDsKQ/Uku2QgFWXCI/AAAAAAAADEc/h8--HTDsvGo/s1600/0052.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-vfrjGagDsKQ/Uku2QgFWXCI/AAAAAAAADEc/h8--HTDsvGo/s1600/0052.png" /></a>
    </div>
    
    <p>
      The Interfacing is over, Now we should create the <b>Python</b>&nbsp;file. Create a file named <b>browser.py</b>&nbsp;in the directory containing <b>window.glade</b>. Add The Following contents on <b>browser.py</b>&nbsp;:
    </p>
    
    <blockquote class="tr_bq">
      <p>
        #!/usr/bin/python<br />import pygtk<br />pygtk.require("2.0")<br />import gtk<br />import gtk.glade<br />import gtkmozembed<br />class SubinsWindow:<br />&nbsp;def __init__(self):<br />&nbsp; self.gladefile = "window.glade"<br />&nbsp; self.glade = gtk.Builder()<br />&nbsp; self.glade.add_from_file(self.gladefile)<br />&nbsp; self.glade.connect_signals(self)<br />&nbsp; self.win=self.glade.get_object("window1")<br />&nbsp; self.win.show_all()<br />&nbsp; self.win.maximize()<br />&nbsp; self.win.set_title("Subin&#8217;s Blog Browser &#8211; http://www.subinsb.com")<br />&nbsp; self.mw = gtkmozembed.MozEmbed()<br />&nbsp; self.glade.get_object("vbox1").add(self.mw)<br />&nbsp; self.mw.set_size_request(800,600)<br />&nbsp; self.mw.load_url("http://www.subinsb.com")<br />&nbsp; self.mw.show()<br />&nbsp; self.glade.get_object("entry1").set_text("http://www.subinsb.com")<br />&nbsp; self.glade.get_object("entry1").connect("activate",self.on_entry1_activate)<br />&nbsp; self.glade.get_object("reload").connect("clicked",self.on_entry1_activate)<br />&nbsp; self.glade.get_object("stop").connect("clicked",self.on_stop_click)<br />&nbsp; self.glade.get_object("new").connect("clicked",self.on_new_click)
      </p>
      
      <p>
        &nbsp;def on_entry1_activate(self,widget):<br />&nbsp; v=self.glade.get_object("entry1").get_text()<br />&nbsp; self.mw.load_url(v)<br />&nbsp; self.win.set_title("Subin&#8217;s Blog Browser &#8211; "+v)
      </p>
      
      <p>
        &nbsp;def on_reload_click(self,widget):<br />&nbsp; self.mw.reload()
      </p>
      
      <p>
        &nbsp;def on_new_click(self,widget):<br />&nbsp; self.glade.get_object("entry1").set_text("http://www.subinsb.com")<br />&nbsp; self.mw.load_url("http://www.subinsb.com")
      </p>
      
      <p>
        &nbsp;def on_stop_click(self,widget):<br />&nbsp; v=self.mw.get_location()<br />&nbsp; self.mw.stop_load()<br />&nbsp; self.win.set_title("Subin&#8217;s Blog Browser &#8211; "+v+" &#8211; Stopped")
      </p>
      
      <p>
        &nbsp;def on_MainWindow_close_event(self, widget, event=1):<br />&nbsp; gtk.main_quit()
      </p>
      
      <p>
        if __name__ == "__main__":<br />&nbsp;a = SubinsWindow()<br />&nbsp;gtk.main()
      </p>
    </blockquote>
    
    <p>
      That&#8217;s It ! Run <b>browser.py </b>using <b>python browser.py</b>&nbsp;and see your own <b>Browser</b>&nbsp;in action :
    </p>
    
    <div class="separator" style="clear: both; text-align: center;">
      <a href="//3.bp.blogspot.com/-6vF3PBfr2yY/UkvAypPzcaI/AAAAAAAADE4/JJuFGWvCqXc/s1600/0054.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-6vF3PBfr2yY/UkvAypPzcaI/AAAAAAAADE4/JJuFGWvCqXc/s1600/0054.png" /></a>
    </div>
    
    <p>
      If you have any problems/doubts/suggestions/feedback print it out in the comments. Thank You. Hope You Like it.
    </p>
  </div>
</div>