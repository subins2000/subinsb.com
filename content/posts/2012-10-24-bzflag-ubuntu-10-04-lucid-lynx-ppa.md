---
title: BZFLAG Ubuntu 10.04 Lucid Lynx PPA
author: Subin Siby
type: post
date: 2012-10-24T06:42:00+00:00
excerpt: 'Bzflag stopped supporting Ubuntu 10.04 Lucid Lynx. But you can still install the latest version by adding this PPA.To add the PPA follow the steps.1 - Go to Ubuntu Software CenterUnder Edit Tab Choose Software sources.Click Add&nbsp;button.Type this in...'
url: /bzflag-ubuntu-10-04-lucid-lynx-ppa
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 65ea12df174523698ee314174d1418b2
  - 65ea12df174523698ee314174d1418b2
  - 65ea12df174523698ee314174d1418b2
  - 65ea12df174523698ee314174d1418b2
  - 65ea12df174523698ee314174d1418b2
  - 65ea12df174523698ee314174d1418b2
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
  - http://sag-3.blogspot.com/2012/10/bzflag-ppa.html
categories:
  - Linux
  - Ubuntu
tags:
  - BzFlag
  - Games
  - PPA
  - Software Center
  - Synaptic Package Manager

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Bzflag stopped supporting Ubuntu 10.04 Lucid Lynx. But you can still install the latest version by adding this PPA.<br />To add the PPA follow the steps.<br />1 &#8211; Go to <b>Ubuntu Software Center</b><br />Under Edit Tab Choose <b>Software sources.</b></p> 
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <p>
    <a href="//1.bp.blogspot.com/-CBeHXGjRxLw/UIa5a9_BX-I/AAAAAAAACFE/2ddIGem0uFI/s1600/Screenshot-Software+Sources.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-CBeHXGjRxLw/UIa5a9_BX-I/AAAAAAAACFE/2ddIGem0uFI/s1600/Screenshot-Software+Sources.png" /></a><br />Click <b>Add</b>&nbsp;button.<br />Type this in the <b>APT line </b>field.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      ppa:ferramroberto/game/ubuntu
    </p>
  </blockquote>
  
  <p>
    and click <b>Add Source</b>.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-7qKkK6T6hbg/UIeKXUFHChI/AAAAAAAACFM/ClK3xhNS9No/s1600/Screenshot-Untitled+Window.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-7qKkK6T6hbg/UIeKXUFHChI/AAAAAAAACFM/ClK3xhNS9No/s1600/Screenshot-Untitled+Window.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    <b>BZFLAG PPA </b>&nbsp;is now added. Close the <b>Software Resources</b>&nbsp;dialog.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    A window will ask for your password.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-Epi7rZ9Od2U/UIeLSBTVIFI/AAAAAAAACFU/fp_snMw4ZIk/s1600/Screenshot-Authenticate.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-Epi7rZ9Od2U/UIeLSBTVIFI/AAAAAAAACFU/fp_snMw4ZIk/s1600/Screenshot-Authenticate.png" /></a>
  </div>
  
  <div class="" style="clear: both; text-align: left;">
    Type your password and click <b>Authenticate</b>. Some files will start to Download.
  </div>
  
  <div class="" style="clear: both; text-align: left;">
    After the download is finished<b>&nbsp;</b>packages will be updated. Close <b>Ubuntu Software Center</b>. Open <b>Synaptic Package Manager</b>&nbsp;and search for <b>bzflag</b>&nbsp;you should see an exclamation mark on the left side of the package <b>bzflag</b>. It means an <b>upgrade</b>&nbsp;is available. Double click on the packages <b>bzflag</b>&nbsp;, <b>bzflag-data</b>&nbsp;, <b>bzflag-client</b>&nbsp;, <b>bzflag-server</b>. Then click <b>Apply</b>&nbsp;button on the top.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-mr1dveJ_9bU/UIeND11bkGI/AAAAAAAACFc/Ja0isStxQT4/s1600/apply.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="32" src="//3.bp.blogspot.com/-mr1dveJ_9bU/UIeND11bkGI/AAAAAAAACFc/Ja0isStxQT4/s200/apply.png" width="32" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    The packages you <b>double clicked </b>will be downloaded and upgraded. Enjoy <b>BzFlag</b>.
  </div>
</div>