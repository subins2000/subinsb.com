---
title: Preventing double encode of URL in PHP
author: Subin Siby
type: post
date: 2013-07-22T03:30:00+00:00
excerpt: "There is no practical way to check whether an URL&nbsp;is encoded or not to encode if it's not encoded. But you can do encode the URL by first decoding like below:$string = url_decode($url);And then you should encode as to prevent double encoding.$stri..."
url: /preventing-double-encode-of-url-in-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
  - http://sag-3.blogspot.com/2013/07/preventing-double-encode-of-url-in-php.html
syndication_item_hash:
  - 673c28181b8982cd9bc594d01108b6de
  - 673c28181b8982cd9bc594d01108b6de
  - 673c28181b8982cd9bc594d01108b6de
  - 673c28181b8982cd9bc594d01108b6de
  - 673c28181b8982cd9bc594d01108b6de
  - 673c28181b8982cd9bc594d01108b6de
dsq_thread_id:
  - 1957355802
  - 1957355802
  - 1957355802
  - 1957355802
  - 1957355802
  - 1957355802
categories:
  - PHP
tags:
  - Decode
  - Encode
  - URL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  There is no practical way to check whether an <b>URL</b>&nbsp;is encoded or not to <b>encode </b>if it&#8217;s not encoded. But you can do encode the URL by first decoding like below:</p> 
  
  <blockquote class="tr_bq">
    <p>
      $string = url_decode($url);
    </p>
  </blockquote>
  
  <p>
    And then you should encode as to prevent double encoding.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      $string = url_encode($url);
    </p>
  </blockquote>
  
  <p>
    By doing this we can prevent double encoding of <b>URL</b>&#8216;s.</div>