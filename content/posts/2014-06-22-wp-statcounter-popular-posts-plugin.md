---
title: StatCounter Popular Posts Plugin For WordPress
author: Subin Siby
type: post
date: 2014-06-22T15:39:03+00:00
url: /wp-statcounter-popular-posts-plugin
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
  - WordPress
tags:
  - Plugin
  - Stats

---
StatCounter doesn&#8217;t really have a Popular Posts plugin for WordPress. Though many WordPress blog uses SC, it doesn&#8217;t have a widget that shows the Popular Posts. I uses it and wanted to have this feature. So, I created a plugin called **StatCounter Popular Posts** written in short ad **SPP**.

It&#8217;s a plugin that adds a widget to WordPress which can be added to the WordPress Site layout (sidebar, footer, header &#8230;). It&#8217;s a simple plugin and doesn&#8217;t overheat your WP installation.

All you have to do is give plugin the project ID of your StatCounter Project and make the project Public which should be Visible for all, so that the plugin can access this like normal person and get the popular posts.

By Posts, it doesn&#8217;t mean just posts. All stuff that come in the "Popular Pages" section of your SC stats will be in the widget too.


## Features

  * Lightweight
  * All Popular Pages (posts, pages etc..)
  * Customization (items limit, type of counting [Pageviews, Visits])

## Installation

Download the plugin from <a href="https://github.com/subins2000/wp-spp/archive/master.zip" target="_blank">GitHub</a>.

Extract the folder "sc-popular-posts" from the archive you downloaded into "/wp-content/plugins" directory.

Activate the plugin through the &#8216;Plugins&#8217; menu in WordPress

## Configure

The Settings page of SPP can be accessed from **Settings **-> **StatCounter Popular Posts**

You can see an input field asking for the ID of your StatCounter project. Paste it and save the settings. That&#8217;s the only field that you need to configure.

Here&#8217;s how you find the project ID :

  * Go to your Project Summary Stats Page
  * See the URL

Here&#8217;s a URL :

<pre class="prettyprint"><code>http://statcounter.com/p1234567/summary/</code></pre>

<span style="color: #444444;">In the above URL "p1234567"</span><span style="color: #444444;"> is the Project ID.</span>

## Add Widget

Go to your Widgets Page (Appearance -> Widgets). You will see the new "<span style="color: #666666;">StatCounter Popular Posts" widget there :</span><figure id="attachment_2850" class="wp-caption aligncenter">

[<img class="wp-image-2850 size-full" src="//lab.subinsb.com/projects/blog/uploads/2014/06/0121.png" alt="StatCounter Popular Posts Widget" width="229" height="81" />][5]<figcaption class="wp-caption-text">StatCounter Popular Posts Widget</figcaption></figure> 

You can add it wherever you like. You can also set the following options on the Widget :

  * Type of Counting
  * Items to be displayed

The Type of counting gives the plugin to get the stats based on the pageviews or the visits.

The items that should be displayed is the second option. Any integer from 1 is acceptable to it.

## Extra

You can ask support for this plugin [here][7]. You can develop the plugin <a href="https://github.com/subins2000/wp-spp" target="_blank">here on GitHub</a>.

 [1]: #features
 [2]: #installation
 [3]: #configure
 [4]: #add-widget
 [5]: //lab.subinsb.com/projects/blog/uploads/2014/06/0121.png
 [6]: #extra
 [7]: //subinsb.com/ask/statcounter-popular-posts
