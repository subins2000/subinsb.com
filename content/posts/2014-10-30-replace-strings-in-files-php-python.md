---
title: 'Replace Strings in Files With PHP & Python'
author: Subin Siby
type: post
date: 2014-10-30T03:30:58+00:00
url: /replace-strings-in-files-php-python
categories:
  - PHP
  - Python
  - Short Post
tags:
  - String

---
There are softwares already that helps to replace a string in a file. Suppose, if you&#8217;re making a program that requires saving some data in to a file. By "adding", the program is actually replacing a word. Let&#8217;s see how it&#8217;s done in **PHP** & **Python**.

This is the file where we are going to replace the word :

<pre class="prettyprint"><code>thisWord
I got thisWord for making it thatWord
replace thisWord already my friend.</code></pre>

We replace the word "thisWord" to "thatWord" containing in this file.

## PHP

<pre class="prettyprint"><code>&lt;?php
$file = "/home/simsu/Other/projects/Programs/PHP/test.txt";
$contents = file_get_contents($file);
$newContent = str_replace("thisWord", "thatWord", $contents);
file_put_contents($file, $newContent);
?&gt;</code></pre>

I&#8217;m not using **fopen** for this, because to make the code short.

## Python

<pre class="prettyprint"><code>import os
file = "/home/simsu/Other/projects/Programs/PHP/test.txt"
fp = open(file, "r+")
contents = fp.read(os.path.getsize(file))
newContent = contents.replace("thisWord", "thatWord")
fp.seek(0)
fp.truncate()
fp.write(newContent)
fp.close()</code></pre>

We use **open** for getting the contents of the file as well as to write to it. We truncate the file because otherwise the original content will be repeated again and finally the file connection is closed.