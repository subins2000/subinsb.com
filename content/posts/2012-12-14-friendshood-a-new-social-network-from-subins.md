---
title: FriendsHood – A new Social Network from Subins
author: Subin Siby
type: post
date: 2012-12-14T05:26:00+00:00
excerpt: "Many of you now know the project I've been working on. In that project I have created a social Network called FriendsHood. I created this social network because my friends have been telling me to create a social network. They told me because School sai..."
url: /friendshood-a-new-social-network-from-subins
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 0ba093f7a59742613cc337d03f46cbee
  - 0ba093f7a59742613cc337d03f46cbee
  - 0ba093f7a59742613cc337d03f46cbee
  - 0ba093f7a59742613cc337d03f46cbee
  - 0ba093f7a59742613cc337d03f46cbee
  - 0ba093f7a59742613cc337d03f46cbee
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
  - http://sag-3.blogspot.com/2012/12/friendshood-new-social-network-from.html
tags:
  - AppFog
  - FriendsHood
  - Subins

---
<div dir="ltr" style="text-align: left;">
  Friendshood is vulnerable to MySQL Injection and shouldn&#8217;t be used anymore. Please see my latest project : <a href="//subinsb.com/2013/12/open-an-open-source-social-network-is-released">http://subinsb.com/2013/12/open-an-open-source-social-network-is-released</a>
</div>

<div dir="ltr" style="text-align: left;">
  Original Post :
</div>

<div dir="ltr" style="text-align: left; padding-left: 30px;">
  Many of you now know the project I&#8217;ve been working on. In that project I have created a social Network called <b><a href="http://fd-subins.hp.af.cm/?utm_source=blog">FriendsHood</a></b>. I created this social network because my friends have been telling me to create a social network. They told me because School said to block <b><a href="http://facebook.com/?utm_source=Subin'sBlog">Facebook</a> </b>and no student should use <b><a href="http://facebook.com/?utm_source=Subin'sBlog">Facebook</a></b>.</p> 
  
  <div style="padding-left: 30px;">
  </div>
  
  <div style="padding-left: 30px;">
    So I decided to create a social network. I started to create <b><a href="http://fd-subins.hp.af.cm/?utm_source=blog">FriendsHood</a></b> before the creation of <b>Subins</b>.
  </div>
  
  <div style="padding-left: 30px;">
    Now <b><a href="http://fd-subins.hp.af.cm/?utm_source=blog">FriendsHood</a></b> has <b>like function</b>, <b>Comment Function</b> & <b>Share function</b>.
  </div>
  
  <div style="padding-left: 30px;">
  </div>
  
  <div style="padding-left: 30px;">
    Note that <b>FriendsHood </b>is currently under development. Some functions will not work. Check out <b><a href="http://fd-subins.hp.af.cm/?utm_source=blog">Friendshood</a></b> <a href="http://fd-subins.hp.af.cm/?utm_source=blog">here</a>.
  </div>
  
  <div style="padding-left: 30px;">
  </div>
  
  <div style="padding-left: 30px;">
    With the help of <b><a href="http://appfog.com/?utm_source=Subin'sBlog">AppFog</a> </b>I got to host <b>FriendsHood</b> for free @ the link <a href="http://fd-subins.hp.af.cm/">http://fd-subins.hp.af.cm</a>.
  </div>
  
  <div style="padding-left: 30px;">
  </div>
  
  <div style="padding-left: 30px;">
    Please <a href="http://accounts-subins.hp.af.cm/signup.php">Signup</a>.
  </div>
</div>