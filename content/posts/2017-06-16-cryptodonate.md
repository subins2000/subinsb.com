---
title: CryptoDonate, The Donation Button For CryptoCurrencies
author: Subin Siby
type: post
date: 2017-06-16T08:12:53+00:00
url: /cryptodonate
categories:
  - Cryptocurrency
  - Cryptogrpahy
  - JavaScript
  - Program
tags:
  - bitcoin
  - ethereum
  - litecoin
  - monero
  - widget

---
CryptoDonate is a JavaScript (vanilla JS) library to embed a donation button for cryptocurrency. It doesn&#8217;t require any external library and is very lightweight. I created it to replace my existing BTC donation button in my blog.

<div class="padlinks">
  <a class="demo" href="https://demos.subinsb.com/Francium/CryptoDonate/" target="_blank" rel="noopener">Demo</a><a class="download" href="https://demos.subinsb.com/download/francium-cryptodonate" target="_blank" rel="noopener">Download</a>
</div>


## Features

  * Supports BTC, LTC, ETH, XMR
  * Lightweight
  * Have an embed script
  * Easy usage
  * Supports theming

## Widget

You can simply add the CryptoDonate widget to your blog or site by using this [Widget Maker Tool][3]. The widget maker tool generates a script that will load the files asynchoronously meaning your page&#8217;s load time won&#8217;t be affected.

The `Fr.loadCD()` function is used to load CryptoDonate to an element. It&#8217;s first parameter is the element (can be an element ID or element DOM object). The second parameter is the options.

<pre class="prettyprint"><code>Fr.loadCD(document.getElementById('cd-loc'), {
  coin: 'bitcoin'
});</code></pre>

To the second parameter, you can pass the same [options][4] as you pass to CryptoDonate object.<figure class="wp-caption aligncenter">

[<img class="size-medium" src="https://lab.subinsb.com/projects/blog/uploads/2017/06/cryptodonate-dialog.png" alt="CryptoDonate dialog" width="1327" height="741" />][5]<figcaption class="wp-caption-text">CryptoDonate dialog</figcaption></figure> 

## Usage

CryptoDonate comes under [The Francium Project][7] and therefore the object is referenced by `Fr.CryptoDonate`. Here is how it&#8217;s constructed :

<pre class="prettyprint"><code>var cd = new Fr.CryptoDonate({
  coin: 'bitcoin',
  address: '3Q2zmZA3LsW5JdxkJEPDRbsXu2YzzMQmBQ'
});</code></pre>

Then you can add the button to an element by using the `appendTo` function :

<pre class="prettyprint"><code>cd.appendTo(document.getElementById('target'));</code></pre>

## Options

Options is passed to CryptoDonate as the first parameter while constructing the CryptoDonate object. Here are the default options (JSON object) :

<pre class="prettyprint"><code>{
    coin: 'bitcoin',
    address: '3Q2zmZA3LsW5JdxkJEPDRbsXu2YzzMQmBQ',

    qr: true,
    getQrImage: function(data) {
        return 'https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=' + data;
    },

    strings: {
        button: 'Donate',
        buttonTitle: 'Donate {coinName}',
        coins: {
            bitcoin: 'Bitcoin',
            ethereum: 'Ether',
            litecoin: 'Litecoin',
            monero: 'Monero',
        },
        dialogHeader: 'Donate {coinName}',
        dialogHelper: 'Please use this {coin} address to donate. Thanks !',
        openInWallet: 'Click here to send this address to your wallet.'
    },

    baseURL: '//lab.subinsb.com/projects/francium/cryptodonate',
    buttonLarge: false,
    buttonClass: '',

    dialogClass: '',
}</code></pre>

Notice the sub-object `strings`. It helps you to change the text displayed. You can use another language or change the "donation" to a "Pay Now" button.

### coin

Type: string; Values: &#8216;bitcoin&#8217;, &#8216;litecoin&#8217;, &#8216;ethereum&#8217;, &#8216;monero&#8217;

### address

Type: string;

The address to which payment is made. This value will be in different format for different coins. CryptoDonate does not check whether it&#8217;s valid or not. Make sure it&#8217;s correct.

### qr

Type: boolean; Values: true, false

Should the QR image be displayed or not.

### getQRImage

Type: function(string data);

The callback function used to get the URL to QR image representation of the given string.

### strings

Type: object;

The strings that are displayed to the user is stored in this object. This is useful for translating strings.

Certain words closed in curly braces can be substituted :

  * {coin} &#8211; The coin&#8217;s name in lowercase. Example: bitcoin, ethereum
  * {coinName} &#8211; The coin&#8217;s name. Example: Bitcoin, Ether

### baseURL

Type: string;

The base URL from which resources are loaded. The folder structure of base folder should be like <a href="https://github.com/subins2000/CryptoDonate/tree/master/src" target="_blank" rel="noopener">this</a>.

### buttonLarge

Type: boolean; Values: true, false

Should the button be large sized.

### buttonClass

Type: string;

Additional classes that must be added to the button. The classes must be separated by a whitespace.

If &#8216;dark&#8217; class is added, then the button&#8217;s theme will be changed to dark.

### dialogClass

Type: string;

Similar to [buttonClass][15], it adds additional classes to dialog. Can be used for theming the dialog as well.

## Functions

### appendTo

Parameters:

  * element &#8211; Type: DOM Object; Element to which the button should be appended

Example :

<pre class="prettyprint"><code>cd.appendTo(document.getElementById('target'));</code></pre>

Using the CryptoDonate library directly may feel difficult for you if you don&#8217;t know JavaScript. In that case, I highly recommend you use the [widget][2].

 [1]: #features
 [2]: #widget
 [3]: https://demos.subinsb.com/Francium/CryptoDonate/
 [4]: #options
 [5]: //lab.subinsb.com/projects/blog/uploads/2017/06/cryptodonate-dialog.png
 [6]: #usage
 [7]: http://subinsb.com/the-francium-project
 [8]: #coin
 [9]: #address
 [10]: #qr
 [11]: #getqrimage
 [12]: #strings
 [13]: #baseurl
 [14]: #buttonlarge
 [15]: #buttonclass
 [16]: #dialogclass
 [17]: #functions
 [18]: #appendto
