---
title: How To Use FileZilla/SFTP To Update Site in OpenShift
author: Subin Siby
type: post
date: 2013-10-09T14:35:00+00:00
excerpt: 'In the recent post, I mentioned about OpenShift, a PaaS&nbsp;Service that offers Free&nbsp;Hosting. You can update your application using GIT. OpenShift&nbsp;also provides SSHing into your application. You may have heard of FTP&nbsp;protocol which allo...'
url: /how-to-use-filezillasftp-to-update-site-in-openshift
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - adf643c1cc0c12358da3ba5426429b6b
  - adf643c1cc0c12358da3ba5426429b6b
  - adf643c1cc0c12358da3ba5426429b6b
  - adf643c1cc0c12358da3ba5426429b6b
  - adf643c1cc0c12358da3ba5426429b6b
  - adf643c1cc0c12358da3ba5426429b6b
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
  - http://sag-3.blogspot.com/2013/10/how-to-use-filezillasftp-to-update-site.html
dsq_thread_id:
  - 1839755079
  - 1839755079
  - 1839755079
  - 1839755079
  - 1839755079
  - 1839755079
categories:
  - Linux
  - PHP
  - SSH
  - Ubuntu
  - Windows
tags:
  - FTP
  - OpenShift
  - SFTP
  - Site
  - Website

---
In the <a title="Goodbye AppFog, Hello OpenShift" href="//subinsb.com/goodbye-appfog-hello-openshift" target="_blank">recent post</a>, I mentioned about **OpenShift**, a **PaaS** Service that offers **Free** **Hosting**. You can update your application using GIT. OpenShift also provides **SSH**ing into your application. You may have heard of **FTP** protocol which allows you to transfer files through web. Most hosting companies use **FTP** protocol to update client apps. OpenShift unlike **AppFog** have **FTP** support, but in a different manner.

It has SFTP support ie **SSH File Transfer Protocol**. SFTP is a FTP like protocol that allows to transfer files via **SSH** sessions. Most FTP clients support **SFTP**. FTP client **FileZilla** supports SFTP which means that you can use FileZilla for updating site hosted on OpenShift. You can download FileZilla from [here][1] available for Linux, Mac & Windows.

You should mention the **ssh-key** you gave to OpenShift in FileZilla too.
  
For that Got to **Edit Menu **-> **Settings** and choose the tab SFTP :

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-B5FTshkmrNM/UlVbEkMj-6I/AAAAAAAADFY/cYXjZ7ABCgA/s1600/0056.png"><img alt="" src="//3.bp.blogspot.com/-B5FTshkmrNM/UlVbEkMj-6I/AAAAAAAADFY/cYXjZ7ABCgA/s1600/0056.png" border="0" /></a>
</div>

Click on **Add keyfile** and choose the file containing the **ssh-key**. Normally on a **Linux **system it will be **/home/user/.ssh/id_rsa**. When you select the **SSH** key file, **FileZilla** will ask for to save the key file in a format **FileZilla** understands. Choose **Yes** and save it in **/home/user/.ssh** with the name **filezilla**.

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-KWB9pk_5uzA/UlVdbIYXhzI/AAAAAAAADFk/3OIKT2iYeB0/s1600/0057.png"><img alt="" src="//3.bp.blogspot.com/-KWB9pk_5uzA/UlVdbIYXhzI/AAAAAAAADFk/3OIKT2iYeB0/s1600/0057.png" border="0" /></a>
</div>

Now, go to your **OpenShift** account and get the username of the **SSH** account.

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-ZxtATowtMJY/UlVnUFRNgJI/AAAAAAAADIM/of3ghPGQyoo/s1600/0058.png"><img alt="" src="//3.bp.blogspot.com/-ZxtATowtMJY/UlVnUFRNgJI/AAAAAAAADIM/of3ghPGQyoo/s1600/0058.png" border="0" /></a>
</div>

Here&#8217;s how to get the username :

<pre class="prettyprint"><code>ssh://198734c26793ca219000106d@demos.subins.com/~/git/demos.git/</code></pre>

The username of the above **SSH** command is **198734c26793ca219000106d **and the host is **demos.subinsb.com**.
  
Now, type in the credentials you got from the **SSH** command into **FileZilla**.
  
Example :

<pre class="prettyprint"><code>Host     : sftp://&lt;b>demos.subins.com&lt;/b>
Username : 198734c26793ca219000106d</code></pre>

Leave the **Password** field blank, because we already have an **ssh-key** that will authorize us.
  
You will now have complete access to your app&#8217;s hosted directory.
  
If you want to update the **source code** paste/add/remove you&#8217;re source code in the **app-root/repo/php **or whatever the language is.
  
Screenshot :

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//4.bp.blogspot.com/-KjN79ZQLADI/UlVo1AknemI/AAAAAAAADIY/VIPCAsefrUU/s1600/0059.png"><img alt="" src="//4.bp.blogspot.com/-KjN79ZQLADI/UlVo1AknemI/AAAAAAAADIY/VIPCAsefrUU/s1600/0059.png" border="0" /></a>
</div>

Note that if you do a **git push** to the application, the changes made by SFTP will be replaced. So, be careful while doing a git commit to application if you have made changes to code by SFTP.

If you have any problems / suggestions / feedback type it out in the comments.

 [1]: https://filezilla-project.org/