---
title: 18 Years Of Revolving Around The Sun
author: Subin Siby
type: post
date: 2018-01-20T06:29:06+00:00
url: /18-years-revolving-around-sun
aliases:
    - /18
categories:
  - Personal
tags:
  - birth

---
I'm legally an adult now. I have successfully revolved around the sun 18 times. Yay !

This post is dedicated to the future me who will be having nostalgic feelings reading this. Hey there, future me !

## Coding

Lobby was a mistake. I think I've wasted my 2 years in developing it. I was too ambitious. But I gained a lot of experience. This was the first time that I got into cross-compiling, virtual environments, Android development, etc.

    Every project is an experience may it be a disaster or not. Be grateful of that experience

^ I guess I have to just say this loud to comfort me. My GitHub commit history is filled with Lobby contributions. There was a day with **56 commits**.

I was very much focused on developing Lobby that I forgot to see the bigger picture, people aren't gonna use it if there aren't enough apps. Don't we already have Android Play Store and similar things.

The idea of Lobby is good though, open source apps where you can directly edit the code to modify it and so on. But I don't think it's possible without a good team of devs and a solid framework.

OpenShift ended their free plan and 2 of my sites is discontinued :

* [Open](https://open.subinsb.com/)
* [Lobby](https://lobby.subinsb.com)

Both the above projects were really helpful to me as a self-learning dev. [My lab site](https://demos.subinsb.com/) was migrated to Heroku and it runs fine on the free plan. And this blog is now a static site with the help of [Hugo](https://gohugo.io).

I started developing desktop apps with Python and Qt :

* [TorrentBro](https://github.com/subins2000/TorrentBro)
* [Setrix](https://gitlab.com/subins2000/setrix)

Python is beautiful ❤

My project [CryptoDonate](https://demos.subinsb.com/Francium/CryptoDonate/) got some attention and someone created a whole [new site for it](https://cryptodonate.online). He/she also linked back to me. That made me happy.

## College

I have joined Vidya Academy of Science And Technology in July 2017 pursuing a BTech degree in Computer Science Engineering. It's a private college and it's expensive ! There's a chance for a scholarship and I hope I get it.

I like it here. Got some really great friends ! I have company with everyone in my class and is getting to know more kind of people from different parts of Kerala.

    Every person is unique and it's interesting to know them

I've benn in internet too long that I know a variety of things. So there's always something in common to talk to a person. Be it politics, science or entertainment.

So I have very different set of friend circles. It's a **Venn diagram** where I intersect in every other sets.

Some noticeable changes :

* Much more active on WhatsApp (more friends)
* Less coding than before (as a consequence of #1)
* Addicted to social media (phones are dangerous, another consequence of #1 ?)
* Much more socially comfortable. I was awkward before. Now I don't mind it at all. I just ring up conversations and my sense of humour is okay-ish.

### Activities

I took initiative in revamping the FOSSers (Free and Open Source Software Users) club of our college. Even though we're freshmen, we were able to move it around and attract people to GNU/Linux :)

I think I have helped more than 25 people in installing a distro. I recommend Linux Mint for newbies.

## Teenage

I've always expressed myself as a "teenage developer". That's gonna go soon. I'm getting old.

This blog has been very inactive. I don't have that energy and enthusiasm I had during my early teenage years. What happened to me ?

Maybe it's gonna get better ?

Maybe.

    Originally wrote this post on March 14. I missed updating my blog about my birthday :(
