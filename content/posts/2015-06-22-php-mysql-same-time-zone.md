---
title: 'Set Same Time Zone in PHP & MySQL'
author: Subin Siby
type: post
date: 2015-06-21T20:30:20+00:00
url: /php-mysql-same-time-zone
categories:
  - Database
  - MySQL
  - PHP
  - Program
  - Short Post
  - SQL

---
Time Zones are important if you&#8217;re creating a social app which will include chat, social network and others which is interacted with the society.

There are many time zones and you have to choose one to set in your server. I would totally recommend using **UTC** instead of your time zone in server.

**Web Server** and **Database server** are different and they each have their own configuration. This includes the time zone too. In this short post, I&#8217;m going to show how you can set the same time zone in both of these servers.

# PHP

This is the most simplest one :

<pre class="prettyprint"><code>date_default_timezone_set("UTC");</code></pre>

Do the above before any other code.

# MySQL

You can set timezone in **MySQL** with this small **SQL** code :

<pre class="prettyprint"><code>SET GLOBAL time_zone = '+00:00';</code></pre>

But in servers like **OpenShift**, the above won&#8217;t work as the time zone will be resetted back after a server restart. So, the above is no avail in hosting providers like this.

So, we must use **PHP** itself to set the time zone in **MySQL** server. Execute the following **SQL** code just after the opening of **MySQ****L** connection :

<pre class="prettyprint"><code>SET time_zone = '+00:00';</code></pre>

An example in **PDO** :

<pre class="prettyprint"><code>$dbh=new PDO('mysql:dbname=mydb;host=localhost;port=3306',$username, $password);
$dbh-&gt;exec("SET time_zone='+00:00';");</code></pre>

Any other **SQL** operations must only follow after the above code execution.