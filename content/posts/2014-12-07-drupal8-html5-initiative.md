---
title: Drupal 8 HTML5 Initiative
author: Subin Siby
type: post
date: 2014-12-07T13:43:57+00:00
url: /drupal8-html5-initiative
categories:
  - HTML
  - HTML5
  - Program
tags:
  - Drupal
  - Open Source

---
<a href="https://html5.org/" target="_blank"><strong>HTML</strong><strong>5</strong></a> is the newest **HTML** version and it has tons of new features. New input types, output & input (audio, video) and many more. It is now absolutely necessary for projects to move to **HTML5** to enjoy new features.

<a href="http://www.drupal.org" target="_blank"><strong>Drupal</strong></a> is also converting to **HTML5** in the upcoming stable **8.0** release. **Drupal** has started the <a href="https://groups.drupal.org/html5/drupal-8" target="_blank"><strong>HTML5 Initiative</strong></a> to convert **Drupal** to **HTML5**. Now, this will upgrade **Drupal** into new heights.


## HTML5<figure class="wp-caption aligncenter">

<img class="" src="https://www.w3.org/html/logo/downloads/HTML5_Badge_512.png" alt="HTML5 Logo" width="378" height="378" /><figcaption class="wp-caption-text">HTML5 Logo</figcaption></figure> 

The latest version of **HTML** upgrades the old **HTML** to new technology. Most latest browsers already supports **HTML5** which includes :

  * Google Chrome
  * Chromium
  * Firefox
  * Safari
  * Opera
  * IE 10+

What&#8217;s awesome about **HTML5** is that it contains lots of new stuff that makes Web more like an OS. More integration with the Hardware is one of the core features of **HTML5**. This means that you can :

  * Record With Web Camera
  * Record With Microphone
  * Play Songs & Videos
  * Animations (2D & 3D)
  * Access User&#8217;s File information with restrictions
  * Create & Access Local Site Data
  * Create Offline Websites

**JavaScript** is also updated along with **HTML5** which also has the **Canvas API** that replaces the use of **Flash** in websites. This means **Adobe** will lose its monopoly over web browsers and no more need to **install Flash** !

I&#8217;ve chose **HTML5** initiative to blog, because it&#8217;s simply the best and most needed improvement for **Drupal** as it will improve **Drupal** very much.

## The Initiative<figure class="wp-caption aligncenter">

<img class="" src="https://groups.drupal.org/files/drupalhtml5logo_1.png" alt="Drupal HTML5 Initiative Logo" width="223" height="223" /><figcaption class="wp-caption-text">Drupal HTML5 Initiative Logo</figcaption></figure> 

The initiative is made to convert the existing **Drupal** to **HTML5**. As part of this initiative **Drupal** will be revamping most of the templates and theme functions and implementing new form input types and elements. It&#8217;s a big challenge, but also a great opportunity to make **Drupal**&#8216;s markup and **CSS** the best it can be.

The organizers of the Initiative are :

  * [bleen18][3]
  * [mitchell][4]
  * [jenlampton][5]
  * [ericduran][6]
  * [jackalope][7]
  * [jhood][8]
  * [Jacine][9]

The initiative is organizing discussions and meetings on what to implement in **Drupal 8** and how it should be implemented. They plan to make the themes responsive with **CSS** and style with new **CSS 3** transitions, animations and new effects.

They are also planning to restyle all of current **Drupal GUI** to something more modern and stylish. This includes comments, site body, themes and others. Mobile is becoming the most used device to access the internet and therefore **Drupal** should be ready to welcome the mobile users. The initiative organizers have agreed that the **Drupal** themes are not compatible with mobile and it should be made responsive.

Responsive images and updation of **jQuery** to **2.0** has been done in **Drupal 8**. This means that the new **Drupal 8** frontend <a href="https://groups.drupal.org/node/298753" target="_blank">won&#8217;t support <strong>IE</strong></a> versions of less than **9**. This is a good thing, because support of older browsers mean more sized files and if the code is only made for the modern browsers, less size in **JS** and **CSS** files can be achieved.

<a href="https://groups.drupal.org/mobile/drupal-8" target="_blank"><strong>Mobile</strong> initiative</a> is an another initiative linked with this :

  * <a href="https://groups.drupal.org/node/308208" target="_blank">Update on Mobile initiative #43 &#8211; Component Naming convention</a>
  * <a href="https://groups.drupal.org/node/302353" target="_blank">Update on Mobile Initiative #40</a>
  * <a href="https://groups.drupal.org/node/298753" target="_blank">Update on D8 Mobile Initiative &#8211; further to meeting #38</a>

## My Contribution

I joined the group and went over it&#8217;s <a href="https://twitter.com/drupal8html5" target="_blank"><strong>Twitter Page</strong></a> and found an issue. It was "<a href="https://twitter.com/drupal8html5/status/541482876919746560" target="_blank">Border on "details" element in system.theme.css producing weird artifact</a>". I checked the problem and it existed. Apparently, in the **Configuration -> Extend** page, if the description of each modules are over a specific limit, then it won&#8217;t be shown.

I fixed the problem by removing the height limit of ".system-modules td details", changing "white-space" property and by adding "word-wrap" property. As a whole, it became :

<pre class="prettyprint"><code>.system-modules td details {
  border: 0;
  margin: 0;
  display: flex;
  min-height: 20px;
  white-space: pre-line;
  word-wrap: break-word;
}</code></pre>

Now, the bug is gone and the description is shown even if it&#8217;s long. Here&#8217;s <a href="https://www.drupal.org/node/2158421#comment-9412735" target="_blank">my patch</a> on the issue page.

UPDATE : With the patch, there was another problem. **Flexbox** is not supported on **IE 9**. I hate IE. But, I think since **Flex** won&#8217;t work in **IE**, **block** will be the **display** property value. So, it will work normally like in **Chrome**. What made me add **flex** value is that, in **Firefox** and extra height is added since **white-space** is "pre-line".

## Contribute

Anyone can contribute to this <a href="https://groups.drupal.org/html5/drupal-8" target="_blank"><strong>HTML</strong> <strong>Initiative</strong></a> by going to the <a href="https://groups.drupal.org/html5" target="_blank">Group Page</a> and Join. The official **IRC** channel [#drupal-html5][12] is there to help anyone who want to contribute. There is also a <a href="http://twitter.com/drupal8html5" target="_blank"><strong>Twitter</strong> page of <b>Drupal HTML5</b> <strong>Initiative</strong>.</a>

<a href="irc://irc.freenode.net/drupal-html5" rel="nofollow">#drupal-html5</a> is smaller crowd, dedicated to discussions about **HTML5** and **Drupal**. An **IRC** meeting is conducted every two weeks to discuss priorities, progress and issues. The meeting announcements are posted in the <a href="http://groups.drupal.org/html5" rel="nofollow">HTML5 group</a>, which sends e-mail notifications for new posts/events.

 [1]: #html5
 [2]: #the-initiative
 [3]: https://groups.drupal.org/user/42814
 [4]: https://groups.drupal.org/user/22656
 [5]: https://groups.drupal.org/user/2771
 [6]: https://groups.drupal.org/user/21703
 [7]: https://groups.drupal.org/user/1358
 [8]: https://groups.drupal.org/user/69534
 [9]: https://groups.drupal.org/user/5867
 [10]: #my-contribution
 [11]: #contribute
 [12]: irc://irc.freenode.net/drupal-html5
