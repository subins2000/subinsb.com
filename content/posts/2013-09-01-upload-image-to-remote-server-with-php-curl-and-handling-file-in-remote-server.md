---
title: 'Upload Image To Remote Server With PHP cURL & Handle File in Remote Server'
author: Subin Siby
type: post
date: 2013-09-01T15:12:00+00:00
excerpt: 'If you want to upload images to an external server&nbsp;which is uploaded to your site by a client, you are at the right tutorial.For this submission we will use 2&nbsp;files :form.php&nbsp;- The Page Where we will show the client the form. This file a...'
url: /upload-image-to-remote-server-with-php-curl-and-handling-file-in-remote-server
authorsure_include_css:
  - 
  - 
  - 
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
  - http://sag-3.blogspot.com/2013/09/upload-image-to-remote-server-with-php.html
syndication_item_hash:
  - a98b0fbe5d531bf08206b734053c738c
  - a98b0fbe5d531bf08206b734053c738c
  - a98b0fbe5d531bf08206b734053c738c
  - a98b0fbe5d531bf08206b734053c738c
  - a98b0fbe5d531bf08206b734053c738c
  - a98b0fbe5d531bf08206b734053c738c
dsq_thread_id:
  - 1686163321
  - 1686163321
  - 1686163321
  - 1686163321
  - 1686163321
  - 1686163321
categories:
  - HTML
  - PHP
tags:
  - base64
  - cURL
  - Server

---
<div dir="ltr" style="text-align: left;">
  If you want to upload images to an external server which is uploaded to your site by a client, you are at the right tutorial.<br /> For this submission we will use <b>2</b> files :</p> 
  
  <ol style="text-align: left;">
    <li>
      <b>form.php</b> &#8211; The Page Where we will show the client the form. This file also sends the uploaded data to the external server.
    </li>
    <li>
      <b>handle.php</b> &#8211; The Page on the external server which receives the uploaded data from <b>form.php </b>using <b>cURL</b>.
    </li>
  </ol>
  
  <p>
    We won&#8217;t copy the uploaded file by the client to our server, instead we will directly send the file to the external server. For sending we will encrypt the file with <b>base64</b>.<br /> OK. Lets&#8217; Start. First, Let&#8217;s create the <b>FORM</b> page :
  </p>
  
  <pre class="prettyprint"><code>&lt;form enctype="multipart/form-data" encoding='multipart/form-data' method='post' action="form.php"&gt;
  &lt;input name="uploadedfile" type="file" value="choose"&gt;
  &lt;input type="submit" value="Upload"&gt;
&lt;/form&gt;
&lt;?
if ( isset($_FILES['uploadedfile']) ) {
 $filename  = $_FILES['uploadedfile']['tmp_name'];
 $handle    = fopen($filename, "r");
 $data      = fread($handle, filesize($filename));
 $POST_DATA = array(
   'file' =&gt; base64_encode($data)
 );
 $curl = curl_init();
 curl_setopt($curl, CURLOPT_URL, '&lt;span style="color: red;">http://extserver.com/handle.php&lt;/span>');
 curl_setopt($curl, CURLOPT_TIMEOUT, 30);
 curl_setopt($curl, CURLOPT_POST, 1);
 curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
 curl_setopt($curl, CURLOPT_POSTFIELDS, $POST_DATA);
 $response = curl_exec($curl);
 curl_close ($curl);
 echo "&lt;h2&gt;File Uploaded&lt;/h2&gt;";
}
?&gt;</code></pre>
  
  <p>
    Now the code of the <b>handle.php</b> in <b>external server</b> where we sent the data using <b>cURL </b>:
  </p>
  
  <pre class="prettyprint"><code>$encoded_file = $_POST['file'];
$decoded_file = base64_decode($encoded_file);
/* Now you can copy the uploaded file to your server. */
file_put_contents('&lt;span style="color: red;">subins&lt;/span>', $decoded_file);</code></pre>
  
  <p>
    The above code will receive the <b>base64</b> <b>encoded</b> file and it will <b>decode</b> and put the <b>image</b> to its server folder. This might come in handy when you want to have your own <b>user file storage</b> system. This trick is used by <b>ImgUr</b> and other file hosting services like <strong>Google</strong>.
  </p>
</div>