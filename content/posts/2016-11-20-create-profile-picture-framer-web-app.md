---
title: Create A Profile Picture Framer Web App
author: Subin Siby
type: post
date: 2016-11-20T09:10:40+00:00
url: /create-profile-picture-framer-web-app
categories:
  - AJAX
  - CSS
  - Facebook
  - HTML5
  - JavaScript
  - PHP
  - Program
  - Web
tags:
  - Demo
  - Download
  - Image
  - Profile
  - Tutorials
  - Website

---
You might have seen your Facebook friends&#8217; fancy profile pictures with the frame of the sports team they support. These days for any major events, the best way to support them is by adding a frame to your profile picture with their logo.

Football (soccer) is my favorite sport. My state Kerala have a franchise club in the Indian Super League called **Kerala Blasters**. We are the largest club by average crowd attendance and the fans are so passionate. What if a passionate, crazy fan is also a programmer. Then you will [get this][1].

Facebook didn&#8217;t have a profile picture frame for Kerala Blasters and so I had to make one myself. It was an interesting project and I was able to make it in a night. **UPDATE**: Facebook now has a frame for Kerala Blasters and all other Indian Super League clubs, but the frames are ugly 😛

<div class="padlinks">
  <a class="download" href="//demos.subinsb.com/down.php?class=45" target="_blank">Download</a><a class="demo" href="//demos.subinsb.com/isl-profile-pic" target="_blank">Demo</a>
</div>

When I released the **Kerala Blasters Profile Picture Framer**, it was very successful and thousands of fans in Kerala had used it. The visitor count reached 3,000 in a single day on a site that had an average of 200 in a day.

To be exact, **3759** people have downloaded their framed profile pictures. Here is a sample :

[<img class="size-medium aligncenter" src="//lab.subinsb.com/projects/blog/uploads/2016/11/kerala-blasters-profile-picture-frame.png" alt="" width="404" height="404" />][2]


## Features

<li style="text-align: left;">
  Crop image
</li>
<li style="text-align: left;">
  Preview before download
</li>
<li style="text-align: left;">
  Customisable frame
</li>
<li style="text-align: left;">
  Ability to choose different frames
</li>
<li style="text-align: left;">
  Direct upload to Facebook
</li>

<p style="text-align: left;">
  Besides all this, the greatest feature is that the code is simple and easy.
</p>

<h2 style="text-align: left;">
  <span id="set-up">Set Up</span>
</h2>

<p style="text-align: left;">
  Let&#8217;s begin by creating the files and libraries needed. The directory tree looks like this :
</p>

<li style="text-align: left;">
  css
</li>
<li style="text-align: left;">
  frames
</li>
<li style="text-align: left;">
  js
</li>
<li style="text-align: left;">
  uploads<br /> convert.php<br /> download.php<br /> index.php<br /> upload.php
</li>

<p style="text-align: left;">
  All except the ".php" files above are folders.
</p>

<p style="text-align: left;">
  We&#8217;ll use <a href="http://foliotek.github.io/Croppie/">Croppie</a> for letting user crop her/his profile picture. Download <a href="https://raw.githubusercontent.com/Foliotek/Croppie/master/croppie.css">this file</a> to "css/croppie.css" and <a href="https://raw.githubusercontent.com/Foliotek/Croppie/master/croppie.min.js">this file</a> to "js/croppie.min.js".
</p>

<p style="text-align: left;">
  Set the permissions of "uploads" folder to Read/Write. On Linux this would be :
</p>

<pre class="prettyprint"><code>sudo chown ${USER}:www-data uploads && sudo chmod 0755 uploads
</code></pre>

<h2 style="text-align: left;">
  <span id="makeframe">Make Frame</span>
</h2>

<p style="text-align: left;">
  Make a frame using your favorite image editor. It should be a square and should have a minimum size of <strong>400x400px</strong>. An example :
</p><figure class="wp-caption aligncenter">

[<img class="size-medium" src="https://demos.subinsb.com/isl-profile-pic/image/dp-fg.png" alt="Frame" width="400" height="400" />][4]<figcaption class="wp-caption-text">Frame</figcaption></figure> 

Make sure you make the frame keeping in mind that a face has to fit in it. Also make the background of the image **transparent** and save it as a **PNG** file.

You can make multiple frames, because our app supports choosing from a list of frames. You should save the frames in "frames" folder as "frame-0.png", "frame-1.png" etc. Note that we start numbering from "0" and not "1".

## index.php

Copy <a href="https://github.com/subins2000/profile-picture-framer/blob/master/index.php" target="_blank">this file</a> to "index.php".

The page has a small box for customizing the profile picture, a button to upload profile picture and a button to upload to Facebook.

## download.php

Copy <a href="https://github.com/subins2000/profile-picture-framer/blob/master/download.php" target="_blank">this file</a> to "download.php".

When user clicks the download button, he/she is redirected to this page. This page will show the generated profile picture and when user clicks on that image, the file will be downloaded. This is done by **download** attribute :

<pre class="prettyprint"><code>&lt;a href='uploads/123.png' download='kerala-blasters-profile'&gt;&lt;img src='uploads/123.png' /&gt;&lt;/a&gt;</code></pre>

The **download** attribute gives the file name to the downloaded file.

## convert.php

Copy <a href="https://github.com/subins2000/profile-picture-framer/blob/master/convert.php" target="_blank">this file</a> to "convert.php".

This file has a function called "makeDP()" :

<pre class="prettyprint"><code>binary makeDP(string $profilePicturePath, int $design = 0)
</code></pre>

"binary" denotes the image&#8217;s RAW data. Example usage :

<pre class="prettyprint"><code>makeDP("/home/me/profile-picture.png", 1);</code></pre>

If the **$design** argument is not passed, "frame-0.png" will be used.

## upload.php

Copy <a href="https://github.com/subins2000/profile-picture-framer/blob/master/upload.php" target="_blank">this file</a> to "upload.php".

When user clicks the download button, the profile picture is uploaded to this file via **AJAX**. Then this file will make the framed profile picture and save it to "uploads" folder.

The AJAX response would be something like this :

<pre class="prettyprint"><code>uploads/sk2e9c1cu2.png
</code></pre>

The file name would be of length 10.

## css/style.css

Copy <a href="https://github.com/subins2000/profile-picture-framer/blob/master/style.css" target="_blank">this file</a> to "css/style.css".

The stylesheet probably don&#8217;t work for old IE. But it is supposed to work in all modern browsers and is mobile compatible.

## js/app.js

Copy <a href="https://github.com/subins2000/profile-picture-framer/blob/master/js/app.js" target="_blank">this file</a> to "js/app.js".

When user uploads her/his profile picture, **onFileChange()** is called and a preview of the image is shown. If the image is smaller than **400x400px**, then an alert box is shown.

The user could then adjust the profile picture according to the frame. This feature is done with the help of [Croppie.js][11].

When user is done adjusting, the "Download Profile Picture" button is clicked and the blob data of cropped picture is obtained :

<pre class="prettyprint"><code>croppie.result({
  size: "viewport"
}).then(function(dataURI){
  var formData = new FormData();
  formData.append("design", $("#fg").data("design"));
  formData.append("image", dataURItoBlob(dataURI));
...</code></pre>

Croppie returns a data URI and we convert it to blob using **dataURItoBlob()** function. The form data that is sent to "upload.php" looks like this :

<pre class="prettyprint"><code>design=0;
image=&lt;binary&gt;;</code></pre>

The design parameter is an integer of design chosen. This value is got from the list of designs :

<pre class="prettyprint"><code>&lt;div id="designs"&gt;
  &lt;img class="design active" src="frames/frame-0.png" data-design="0" /&gt;
  &lt;img class="design" src="frames/frame-1.png" data-design="1" /&gt;
  &lt;img class="design" src="frames/frame-2.png" data-design="2" /&gt;
&lt;/div&gt;</code></pre>

The **data-design** attribute is the ID of that design.

## Upload To Facebook

There is an option to upload the framed profile picture to Facebook. But this requires you to [create a Facebook app][13] and submit a review request for the following permissions :

  * publish_actions

Before you submit the App Review request, you must set up the website and make a video of how your app works. It took me a week to get the permissions approved.

If you would like to implement this feature, copy [this file][14] to "js/fb.js". Don&#8217;t forget to replace the URL to image file with yours.

Note that we can&#8217;t directly set the user&#8217;s profile picture. The FB API doesn&#8217;t allow you to do that. What we can do is upload the image to user&#8217;s wall and redirect the user to that image in FB, so that user can click the Make Profile Picture button.

That&#8217;s it ! Enjoy your own profile picture framer web app. Use it for special events.

 [1]: https://demos.subinsb.com/isl-profile-pic
 [2]: //lab.subinsb.com/projects/blog/uploads/2016/11/kerala-blasters-profile-picture-frame.png
 [3]: #features
 [4]: https://demos.subinsb.com/isl-profile-pic/image/dp-fg.png
 [5]: #indexphp
 [6]: #downloadphp
 [7]: #convertphp
 [8]: #uploadphp
 [9]: #cssstylecss
 [10]: #jsappjs
 [11]: http://foliotek.github.io/Croppie/
 [12]: #upload-to-facebook
 [13]: http://developers.facebook.com/apps
 [14]: https://github.com/subins2000/profile-picture-framer/blob/master/js/fb.js
