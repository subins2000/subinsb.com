---
title: Blog Changes
author: Subin Siby
type: post
date: 2014-10-20T03:30:12+00:00
url: /blog-changes-1
categories:
  - Personal
tags:
  - Blog

---
It&#8217;s been some time since I made some changes to the blog apart from new posts. It took me this much time because of school, of course. Here are the main changes that I hope to add to the blog :

  * HTTPS
  * No Ads
  * Short Posts


## HTTPS

There&#8217;s nothing like feeling secure in Internet. So, I have decided to add **HTTPS** support to the site. It&#8217;s made possible **free of cost**, because **CloudFlare** actually gives **HTTPS** feature with certificate for absolutely free and it doesn&#8217;t require for your server to have a certificate or **SSL** support.

Check the **HTTPS** enabled [site][2] and the <a href="//blog-subins.rhcloud.com/" target="_blank">direct server one</a>.

## No More Ads

I have decided to remove the ads when the ad earnings reach **20$** (currently 18.5$). You might think I&#8217;m crazy for disabling ads. But I got pretty reasons for this :

  1. I got more donation money than from ad revenue
  2. Ads are not cool
  3. I hate ads
  4. It slows down the site
  5. Invasion of privacy

These 5 reasons and more made me wanted to stop showing ads. The ads won&#8217;t be showed in any of the sites on this domain including Demos, Open, Search & Labs.

## Short Posts

Since I&#8217;m constantly busy, I don&#8217;t have time to write long, long posts. So I decided to share useful stuff in less words. The first post will be published on 22nd October.

The short posts can be seen <a href="//subinsb.com/category/short-post" target="_blank">here</a>. With this feature, I can publish more posts easily and efficiently.

## Theme : Round Border

Before, there was no round border. Instead, it was just flat. To make it more modern, I have added "border-radius" property with value "10px" to the widgets of sidebar and the article holder.

 [1]: #https
 [2]: https://subinsb.com
 [3]: #no-more-ads
 [4]: #short-posts
 [5]: #theme-round-border
