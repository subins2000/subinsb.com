---
title: Make Snowfall on your site/blog using Javascript
author: Subin Siby
type: post
date: 2012-12-22T12:43:00+00:00
excerpt: "It's Christmas. It's time for snowing. Want to have a snowfall on your site just like in my blog. Here's the Java script for snowing.Note - This code is for websites. Code for blogger is below this code&lt;script&gt;&nbsp; //Configure below to change U..."
url: /make-snowfall-on-your-siteblog-using-javascript
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - de933520a4ec9d3d92772b2775c70ad9
  - de933520a4ec9d3d92772b2775c70ad9
  - de933520a4ec9d3d92772b2775c70ad9
  - de933520a4ec9d3d92772b2775c70ad9
  - de933520a4ec9d3d92772b2775c70ad9
  - de933520a4ec9d3d92772b2775c70ad9
syndication_permalink:
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
  - http://sag-3.blogspot.com/2012/12/snowfall-using-js.html
categories:
  - Blogger
  - JavaScript
tags:
  - Site

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div dir="ltr" style="text-align: left;" trbidi="on">
    It&#8217;s Christmas. It&#8217;s time for snowing. Want to have a snowfall on your site just like in my blog. Here&#8217;s the <b>Java script</b> for snowing.<br /><b><span style="color: red;">Note &#8211; This code is for websites. Code for blogger is below this code</span></b></p> 
    
    <blockquote class="tr_bq">
      <p>
        <script><br />&nbsp; //Configure below to change URL path to the snow image<br />&nbsp; var snowsrc="http://subins.hp.af.cm/files/images/snow.gif"<br />&nbsp; // Configure below to change number of snow to render<br />&nbsp; var no = 20;<br />&nbsp; // Configure whether snow should disappear after x seconds (0=never):<br />&nbsp; var hidesnowtime = 0;<br />&nbsp; // Configure how much snow should drop down before fading ("windowheight" or "pageheight")<br />&nbsp; var snowdistance = "pageheight";<br />///////////Stop Config//////////////////////////////////<br />&nbsp; var ie4up = (document.all) ? 1 : 0;<br />&nbsp; var ns6up = (document.getElementById&&!document.all) ? 1 : 0;<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>function iecompattest(){<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>}<br />&nbsp; var dx, xp, yp; &nbsp; &nbsp;// coordinate and position variables<br />&nbsp; var am, stx, sty; &nbsp;// amplitude and step variables<br />&nbsp; var i, doc_width = 800, doc_height = 600;<br />&nbsp; if (ns6up) {<br />&nbsp; &nbsp; doc_width = self.innerWidth;<br />&nbsp; &nbsp; doc_height = self.innerHeight;<br />&nbsp; } else if (ie4up) {<br />&nbsp; &nbsp; doc_width = iecompattest().clientWidth;<br />&nbsp; &nbsp; doc_height = iecompattest().clientHeight;<br />&nbsp; }<br />&nbsp; dx = new Array();<br />&nbsp; xp = new Array();<br />&nbsp; yp = new Array();<br />&nbsp; am = new Array();<br />&nbsp; stx = new Array();<br />&nbsp; sty = new Array();<br />&nbsp; snowsrc=(snowsrc.indexOf("subins.hp.af.cm")!=-1)? "http://subins.hp.af.cm/files/images/snow.gif" : snowsrc<br />&nbsp; for (i = 0; i < no; ++ i) { <br />&nbsp; &nbsp; dx[i] = 0; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// set coordinate variables<br />&nbsp; &nbsp; xp[i] = Math.random()*(doc_width-50); &nbsp;// set position variables<br />&nbsp; &nbsp; yp[i] = Math.random()*doc_height;<br />&nbsp; &nbsp; am[i] = Math.random()*20; &nbsp; &nbsp; &nbsp; &nbsp; // set amplitude variables<br />&nbsp; &nbsp; stx[i] = 0.02 + Math.random()/10; // set step variables<br />&nbsp; &nbsp; sty[i] = 0.7 + Math.random(); &nbsp; &nbsp; // set step variables<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>if (ie4up||ns6up) {<br />&nbsp; &nbsp; &nbsp; if (i == 0) {<br />&nbsp; &nbsp; &nbsp; &nbsp; document.write("<div id="dot"+ i +"" style="POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 15px; LEFT: 15px;"><a href="http://sag-3.blogspot.com/2012/12/snowfall-using-js.html"><img src='"+snowsrc+"&#8216; border="0&#8243;></a></div>");<br />&nbsp; &nbsp; &nbsp; } else {<br />&nbsp; &nbsp; &nbsp; &nbsp; document.write("<div id="dot"+ i +"" style="POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 15px; LEFT: 15px;"><img src='"+snowsrc+"&#8216; border="0&#8243;></div>");<br />&nbsp; &nbsp; &nbsp; }<br />&nbsp; &nbsp; }<br />&nbsp; }<br />&nbsp; function snowIE_NS6() { &nbsp;// IE and NS6 main animation function<br />&nbsp; &nbsp; doc_width = ns6up?window.innerWidth-10 : iecompattest().clientWidth-10;<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>doc_height=(window.innerHeight && snowdistance=="windowheight")? window.innerHeight : (ie4up && snowdistance=="windowheight")? &nbsp;iecompattest().clientHeight : (ie4up && !window.opera && snowdistance=="pageheight")? iecompattest().scrollHeight : iecompattest().offsetHeight;<br />&nbsp; &nbsp; for (i = 0; i < no; ++ i) { &nbsp;// iterate for every dot<br />&nbsp; &nbsp; &nbsp; yp[i] += sty[i];<br />&nbsp; &nbsp; &nbsp; if (yp[i] > doc_height-50) {<br />&nbsp; &nbsp; &nbsp; &nbsp; xp[i] = Math.random()*(doc_width-am[i]-30);<br />&nbsp; &nbsp; &nbsp; &nbsp; yp[i] = 0;<br />&nbsp; &nbsp; &nbsp; &nbsp; stx[i] = 0.02 + Math.random()/10;<br />&nbsp; &nbsp; &nbsp; &nbsp; sty[i] = 0.7 + Math.random();<br />&nbsp; &nbsp; &nbsp; }<br />&nbsp; &nbsp; &nbsp; dx[i] += stx[i];<br />&nbsp; &nbsp; &nbsp; document.getElementById("dot"+i).style.top=yp[i]+"px";<br />&nbsp; &nbsp; &nbsp; document.getElementById("dot"+i).style.left=xp[i] + am[i]*Math.sin(dx[i])+"px"; <br />&nbsp; &nbsp; }<br />&nbsp; &nbsp; snowtimer=setTimeout("snowIE_NS6()", 25);//Set Timeout<br />&nbsp; }<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>function hidesnow(){<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>if (window.snowtimer) clearTimeout(snowtimer)<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>for (i=0; i<no; i++) document.getElementById("dot"+i).style.visibility="hidden"<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>}<span class="Apple-tab-span" style="white-space: pre;"> </span>if (ie4up||ns6up){<br />&nbsp; &nbsp; snowIE_NS6();<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>if (hidesnowtime>0)<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>setTimeout("hidesnow()", hidesnowtime*1000)<br /><span class="Apple-tab-span" style="white-space: pre;"> </span>}<br /></script>
      </p>
    </blockquote>
  </div>
  
  <h3 style="text-align: left;">
    <u><b>Blogger Code</b></u>
  </h3>
  
  <p>
    Go to <b>Template</b> -> <b>Edit HTML</b><br />Paste this code after <b><body></b> tag.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <script><br />&nbsp; //Configure below to change URL path to the snow image<br />&nbsp; var snowsrc="http://subins.hp.af.cm/files/images/snow.gif"<br />&nbsp; // Configure below to change number of snow to render<br />&nbsp; var no = 20;<br />&nbsp; // Configure whether snow should disappear after x seconds (0=never):<br />&nbsp; var hidesnowtime = 0;<br />&nbsp; // Configure how much snow should drop down before fading ("windowheight" or "pageheight")<br />&nbsp; var snowdistance = "pageheight";<br />///////////Stop Config//////////////////////////////////<br />&nbsp; var ie4up = (document.all) ? 1 : 0;<br />&nbsp; var ns6up = (document.getElementById&&!document.all) ? 1 : 0;<br />&nbsp;function iecompattest(){<br />&nbsp;return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body<br />&nbsp;}<br />&nbsp; var dx, xp, yp; &nbsp; &nbsp;// coordinate and position variables<br />&nbsp; var am, stx, sty; &nbsp;// amplitude and step variables<br />&nbsp; var i, doc_width = 800, doc_height = 600;<br />&nbsp; if (ns6up) {<br />&nbsp; &nbsp; doc_width = self.innerWidth;<br />&nbsp; &nbsp; doc_height = self.innerHeight;<br />&nbsp; } else if (ie4up) {<br />&nbsp; &nbsp; doc_width = iecompattest().clientWidth;<br />&nbsp; &nbsp; doc_height = iecompattest().clientHeight;<br />&nbsp; }<br />&nbsp; dx = new Array();<br />&nbsp; xp = new Array();<br />&nbsp; yp = new Array();<br />&nbsp; am = new Array();<br />&nbsp; stx = new Array();<br />&nbsp; sty = new Array();<br />&nbsp; snowsrc=(snowsrc.indexOf("subins.hp.af.cm")!=-1)? "http://subins.hp.af.cm/files/images/snow.gif" : snowsrc<br />&nbsp; for (i = 0; i < no; ++ i) {<br />&nbsp; &nbsp; dx[i] = 0; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;// set coordinate variables<br />&nbsp; &nbsp; xp[i] = Math.random()*(doc_width-50); &nbsp;// set position variables<br />&nbsp; &nbsp; yp[i] = Math.random()*doc_height;<br />&nbsp; &nbsp; am[i] = Math.random()*20; &nbsp; &nbsp; &nbsp; &nbsp; // set amplitude variables<br />&nbsp; &nbsp; stx[i] = 0.02 + Math.random()/10; // set step variables<br />&nbsp; &nbsp; sty[i] = 0.7 + Math.random(); &nbsp; &nbsp; // set step variables<br />&nbsp; if (ie4up||ns6up) {<br />&nbsp; &nbsp; &nbsp; if (i == 0) {<br />&nbsp; &nbsp; &nbsp; &nbsp; document.write("<div id=&#8217;dot"+ i +"&#8216; style=&#8217;POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 15px; LEFT: 15px;&#8217;><a href=&#8217;http://subins.hp.af.cm&#8217;><img src='"+snowsrc+"&#8216; border=&#8217;0&#8217; /></a></div>");<br />&nbsp; &nbsp; &nbsp; } else {<br />&nbsp; &nbsp; &nbsp; &nbsp; document.write("<div id=&#8217;dot"+ i +"&#8216; style=&#8217;POSITION: absolute; Z-INDEX: "+ i +"; VISIBILITY: visible; TOP: 15px; LEFT: 15px;&#8217;><img src='"+snowsrc+"&#8216; border=&#8217;0&#8217; /></div>");<br />&nbsp; &nbsp; &nbsp; }<br />&nbsp; &nbsp; }<br />&nbsp; }<br />&nbsp; function snowIE_NS6() { &nbsp;// IE and NS6 main animation function<br />&nbsp; &nbsp; doc_width = ns6up?window.innerWidth-10 : iecompattest().clientWidth-10;<br />&nbsp; doc_height=(window.innerHeight && snowdistance=="windowheight")? window.innerHeight : (ie4up && snowdistance=="windowheight")? &nbsp;iecompattest().clientHeight : (ie4up && !window.opera && snowdistance=="pageheight")? iecompattest().scrollHeight : iecompattest().offsetHeight;<br />&nbsp; &nbsp; for (i = 0; i < no; ++ i) { &nbsp;// iterate for every dot<br />&nbsp; &nbsp; &nbsp; yp[i] += sty[i];<br />&nbsp; &nbsp; &nbsp; if (yp[i] > doc_height-50) {<br />&nbsp; &nbsp; &nbsp; &nbsp; xp[i] = Math.random()*(doc_width-am[i]-30);<br />&nbsp; &nbsp; &nbsp; &nbsp; yp[i] = 0;<br />&nbsp; &nbsp; &nbsp; &nbsp; stx[i] = 0.02 + Math.random()/10;<br />&nbsp; &nbsp; &nbsp; &nbsp; sty[i] = 0.7 + Math.random();<br />&nbsp; &nbsp; &nbsp; }<br />&nbsp; &nbsp; &nbsp; dx[i] += stx[i];<br />&nbsp; &nbsp; &nbsp; document.getElementById("dot"+i).style.top=yp[i]+"px";<br />&nbsp; &nbsp; &nbsp; document.getElementById("dot"+i).style.left=xp[i] + am[i]*Math.sin(dx[i])+"px";<br />&nbsp; &nbsp; }<br />&nbsp; &nbsp; snowtimer=setTimeout("snowIE_NS6()", 25);//Set Timeout<br />&nbsp; }<br />&nbsp;function hidesnow(){<br />&nbsp; if (window.snowtimer) clearTimeout(snowtimer)<br />&nbsp; for (i=0; i<no; i++) document.getElementById("dot"+i).style.visibility="hidden"<br />&nbsp;} &nbsp;if (ie4up||ns6up){<br />&nbsp; &nbsp; snowIE_NS6();<br />&nbsp; if (hidesnowtime>0)<br />&nbsp; setTimeout("hidesnow()", hidesnowtime*1000)<br />&nbsp; }<br /></script>
    </p>
  </blockquote>
</div>