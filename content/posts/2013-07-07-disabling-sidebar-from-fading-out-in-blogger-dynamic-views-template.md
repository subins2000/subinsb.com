---
title: Disabling sidebar from fading out in Blogger Dynamic Views Template
author: Subin Siby
type: post
date: 2013-07-07T03:30:00+00:00
excerpt: "The black&nbsp;sidebar on the right side of the blog is hidden and is only visible when mouse is over it. This is a problem because most of the users who are visiting your blog won't even see the sidebar. The following list shows the advantages of havi..."
url: /disabling-sidebar-from-fading-out-in-blogger-dynamic-views-template
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
  - http://sag-3.blogspot.com/2013/07/dynamic-views-blogger-disabling-sidebar-from-fading-out.html
syndication_item_hash:
  - 020f01f0652e8050d3a5c44fa24cda99
  - 020f01f0652e8050d3a5c44fa24cda99
  - 020f01f0652e8050d3a5c44fa24cda99
  - 020f01f0652e8050d3a5c44fa24cda99
  - 020f01f0652e8050d3a5c44fa24cda99
  - 020f01f0652e8050d3a5c44fa24cda99
categories:
  - Blogger
  - CSS
tags:
  - Dynamic Views
  - Template

---
<div dir="ltr" style="text-align: left;">
  The <b>black</b> sidebar on the right side of the blog is hidden and is only visible when mouse is over it. This is a problem because most of the users who are visiting your blog won&#8217;t even see the sidebar. The following list shows the advantages of having a sidebar not fading out :</p> 
  
  <ol style="text-align: left;">
    <li>
      Google + Followers Increases
    </li>
    <li>
      More people will subscribe to your blog
    </li>
    <li>
      People will know the popularity of your blog when they see the blog stats
    </li>
    <li>
      and others&#8230;&#8230;.
    </li>
  </ol>
  
  <p>
    So I&#8217;m gonna tell you how to prevent the sidebar from fading out to the right side.<br /> First of all Go to <b>Blogger</b> -> <b>Template</b> -><b> Customise</b><br /> Choose <b>Advanced </b>option and click on the <b>Add CSS</b> button from the sub options. Paste the following code in the white box:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      #gadget-dock{right: 0px !important;}
    </p>
  </blockquote>
  
  <p>
    Screenshot:
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="https://2.bp.blogspot.com/-wl7rIvS2yNA/Uda3BN6qY4I/AAAAAAAAC1k/UYqOBzYFL-U/s1600/0027.png"><img alt="" src="https://2.bp.blogspot.com/-wl7rIvS2yNA/Uda3BN6qY4I/AAAAAAAAC1k/UYqOBzYFL-U/s1600/0027.png" border="0" /></a>
  </div>
  
  <p>
    Now go to your blog and see the sidebar not moving anywhere and is standing still.
  </p>
</div>