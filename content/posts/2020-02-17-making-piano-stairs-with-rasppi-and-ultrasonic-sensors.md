---
title: Making Piano Stairs With Raspberry Pi and Ultrasonic sensors
author: Subin Siby
type: post
date: 2020-02-17T00:00:00+00:00
lastmod: 2020-02-17T00:00:06+00:00
url: /piano-stairs-raspberry-pi-ultrasonic
aliases:
    - /piano-stairs
    - /ps
categories:
  - Tech
tags:
  - Raspberry Pi
  - Python
  - Hardware
---

We made a stair into a Piano ! It was our college tech fest and we needed something cool to showcase, something audience can interact with and have fun ! Piano Stairs sounded like a cool idea, but implementing it was a challenge.

I'm a 3rd year B.Tech CS engineering student now and this is my first time hacking with hardware. So I didn't know much. I was joined by my juniors, [Muhammed Hashim](https://hashimjvz.github.io/) and [Athul Raj](https://twitter.com/Outoftune2000), both in their 2nd year of CS engineering. They have played with hardware before and has more experience than me. Having them with me was a great help for a noobie me.

**Note**: It's better if you use Infrared sensors or Light Sensitive Resistors instead of Ultrasonic. We learnt it the hard way 😬

## Demo

You can see the videos here :

* [Mastodon](https://aana.site/@subins2000/103663707322974207)
* [Twitter Mirror](https://twitter.com/SubinSiby/status/1228711855293526018)

<center>
  <video src="https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/aanasite/media_attachments/files/000/418/122/original/1a98f5d70723feb4.mp4" controls="true" style="height: 400px"></video>
  <video src="https://cf.mastohost.com/v1/AUTH_91eb37814936490c95da7b85993cc2ff/aanasite/media_attachments/files/000/418/056/original/58dd288065e13327.mp4" controls="true" style="height: 400px"></video>
</center>

## Requirements

* Raspberry Pi
* HC-SR04 Ultrasonic Distance Sensor
* [Raspbian Buster OS](https://www.raspberrypi.org/downloads/raspbian/) installed in Pi
* [Piano key sounds](https://freesound.org/people/Tesabob2001/packs/12995/)
* mpg123 program :
  ```
  sudo apt install mpg123
  ```

The ultrasonic sensor measures distance by sending sound waves. It bounces back to the sensor after hitting someting. This time dealy is used to calculate the distance.

You can directly see the [source code here](https://github.com/FOSSersVAST/piano-stairs).

## The Idea

### Piano Keys

The idea was to assign each step of a staircase with a piano key. There are primarily **7** piano tones :

1. C - Sa
2. D - Re
3. E - Ga
4. F - Ma
5. G - Pa
6. A - Da
7. B - Ni
8. C - Sa (repeat)

All other keys in a piano is a different variant of these **primary musical tones**, and those are named with numbers :

    C0, D0, E0, F0, ..., C1, D1, ..., F8, G8, A8, B8

We picked C4 to B4 (7 keys => 10 steps). You can download the [sound files from here](https://freesound.org/people/Tesabob2001/packs/12995/).

[In the code](https://github.com/FOSSersVAST/piano-stairs/blob/master/run.py), we make a simple dictionary in Python to easily track them :

```
pins = {
    # Index: [echo pin, trigger pin, "piano key sound filename"]
    0: [17, 18, "c4"],
    1: [27, 23, "d4"],
    2: [22, 24, "e4"],
    3: [5, 25, "f4"],
    4: [6, 12, "g4"],
    5: [13, 16, "a4"],
    6: [19, 20, "b4"],
    7: [26, 21, "c4"],
}
```

You don't have to give the file extension in the 3rd value of each list.

### Plan

* Place ultrasonic sensor on one side of each step
* Periodically measure distance. It should be a constant value throughout
* If there's an obstruction (leg on step), the distance measurement would change and when it does so, make the corresponding piano key sound
* The sound should not repeat as the leg is kept on the step. For this a `playStatus` list is kept (See the code)

## Setup

### Ultrasonic sensor

The HC-SR04 sensor has 4 pins :

1. Vcc - Where you give the voltage
2. GND - Ground
3. Trig - Trigger pin (Output : We trigger the sensor to send a sound wave)
4. Echo - Echo pin (Input : We receive the sound wave back.)

`Vcc` and `GND` can be each made common to all the sensors.

Trigger and echo should be separate for each sensor. (Trigger can be made common into one pin too, see [footnotes](#footnotes)).

Fix each sensor at one side of the step. Connect wires to the 4 pins and extend them all together to a single place where we'll join them with our Pi.

As said before, you can use one wire for the `Vcc` of all the sensors. Similarly for `GND` too.

### Pi

Get a Raspberry Pi.

#### Code

The source code is [available here](https://github.com/FOSSersVAST/piano-stairs). Download this to your Pi. You need to run `python3 run.py` to start the piano. But it'll only work once you connect the pins.

Make a folder named `sounds` and place each musical note file there like `c4.mp3`, `d4.mp3` etc. This filename should match the ones we gave in `pins` dictionary.

We use Python's `Process` to parallelly track distance with the sensors. This is only possible if the trigger wires are separate. You can make it common too. See [footnotes](#footnotes)

#### Connections
  
There are 40 pins on a Pi. We'll use GPIO pins to connect to our sensors.

Using this [pin diagram of a Pi](https://www.jameco.com/Jameco/workshop/circuitnotes/raspberry_pi_circuit_note_fig2a.jpg), connect the common `Vcc` to pin `2` (`5V`). Connect the common `GND` wire to pin `6`. There are many ground pins available. You can use [jump wires](https://en.wikipedia.org/wiki/Jump_wire) to easily connect.

Now, we need to connect the separate trigger and echo pins. Note these pin number down. We need to mention them in our code later. **Important**: We use the GPIO's pin number in the code, not the physical number from the `40` pins.

Example: Connect the trigger of the first sensor to `GPIO6` (`31`) and echo to `GPIO13` (`33`). Now, the dictionary in the code will be :

```
pins = {
    # Index: [echo pin, trigger pin, "piano key sound filename"]
    0: [6, 13, "c4"],
}
```

Note that in the above code that **we used `6` and `13` from `GPIO6` & `GPIO13` instead of `31` and `33`**. You should similarly add new item to this `pins` dictionary with the GPIO pin numbers for every sensor attached.

Once you've made every connections, you can now run the script `run.py`. There's a `maxDistance` value hardcoded in the script. This is the distance below which we count it as an obstruction.

## Footnotes

### Common Trigger

By using a common trigger, you can reduce the number of GPIO pins used. With the extra pins, you can connect it to a relay and turn on LEDs which will make it more cool.

When you use a common trigger, you'll have to change the code to remove usage of `Process` and instead, sense one by one :

```
while True:
    for k, v in pins.items():
        sonarSensor(k)
```

### Make It More Cool With LEDs

If you use a common trigger, then you can use the extra pins to run a [relay](https://en.wikipedia.org/wiki/Relay) and light up each stairs on stepping.

## Conclusion

Ultrasonic sensor is not the best way to make this. Because distance measurement is not accurate in case of an obstruction and there might be variations. The delay to calculate the distance will affect the smoothness of the piano. It only worked out smooth for us at lucky times.

I enquired with few of my hardware friends and they all recommended using light sensitive resistors. See [this piano stairs made by Bonnie](https://www.instructables.com/id/Piano-Stairs-with-Arduino-and-Raspberry-Pi/) which has a very good result, though it requires both Arduino and Pi.