---
title: 'Encryption & Decryption Using Mcrypt In PHP'
author: Subin Siby
type: post
date: 2013-12-25T17:39:41+00:00
url: /secure-encryption-decryption-using-mcrypt-php
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - PHP
  - Security
tags:
  - Decryption
  - Encryption
  - Secure

---
**Mcrypt** is a replacement of **Unix**&#8216;s popular command **crypt**. It is a file encryption tool. **Mcrypt** uses modern algorithms for encrypting. Hence it is considered as secure and safe. To encrypt a string, **Mcrypt** requires a key to encrypt and decrypt. This key have a critical role in encryption and decryption. In this post I&#8217;ll be giving out two functions that will encrypt and decrypt based on a key.


# The Key

We should use the same key for both encryption and decryption. Note that the key length should not exceed **25** characters.

For this post, we are going to take

<pre class="prettyprint"><code>a_key_in_2014</code></pre>

as the key. The variable **$key** contains the key in the functions used for encryption and decryption.

# Encryption

This small function will encrypt any string :

<pre class="prettyprint"><code>function encrypt($value){
  if($value == ''){
    return false;
  }else{
    $key     = "a_key_in_2014";
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv      = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $crypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $value, MCRYPT_MODE_ECB, $iv);
    return urlencode(trim(base64_encode($crypted)));
  }
}</code></pre>

## Usage

You can encrypt a string just like this :

<pre class="prettyprint"><code>&lt;?
echo encrypt("subinsb.com");
?&gt;</code></pre>

The above will print out the encrypted string :

<pre class="prettyprint"><code>Rl3PeBCve4vjjwu1SgzFvvj%2BG3Prl2TDN2AmXrWKSqw%3D</code></pre>

As you can see, it&#8217;s hard to figure out the original string from above.

# Decryption

This small function will decrypt strings that are encoded with the same key :

<pre class="prettyprint"><code>function decrypt($value){
  $key = "a_key_in_2014";
  $value = urldecode($value);
  if($value == "" || base64_encode(base64_decode($value)) != $value){
    return $value;
  }else{
    $crypttext = base64_decode($value);
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv);
    return trim($decrypted);
  }
}</code></pre>

## Usage

The decryption is also easy :

<pre class="prettyprint"><code>echo decrypt("Rl3PeBCve4vjjwu1SgzFvvj%2BG3Prl2TDN2AmXrWKSqw%3D");</code></pre>

And the above code will output "subinsb.com" which was the original string.

You can use this functions wherever you want. This trick is used in **<a href="http://open.subinsb.com" target="_blank">Open</a>** for encrypting and decrypting.

 [1]: #the-key
 [2]: #encryption
 [3]: #usage
 [4]: #decryption
 [5]: #usage-2
