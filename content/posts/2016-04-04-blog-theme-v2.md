---
title: Theme Version 2
author: Subin Siby
type: post
date: 2016-04-04T14:59:33+00:00
url: /blog-theme-v2
categories:
  - Content Management System
  - Short Post
  - WordPress
tags:
  - UI

---
If you are reading this post, you should have seen the new UI of my blog. It&#8217;s a custom made theme created in a week during the exams.

It&#8217;s similar to the old theme because, I liked it colorful and dark at the same time. I&#8217;m a **Batman** fan and black is good for the eyes. **As a programmer, you don&#8217;t want to hurt your eyes**.

The theme is named "Subin&#8217;s Blog V2". The name says it all. Coincidentally, this new theme comes exactly <a href="//subinsb.com/subins-blog-v1" target="_blank">after 1 year of the last theme update</a>.

It&#8217;s responsive &#8211; of course and super light. Even though the UI is similar, the CSS file of the new theme has less lines of code than the old one.

<img class="alignnone" src="https://raw.githubusercontent.com/subins2000/blog-theme/v2/screenshot.png" alt="" width="1327" height="680" />

As usual, the source code is Open Source and you can <a href="https://github.com/subins2000/blog-theme" target="_blank">see it on GitHub</a>.