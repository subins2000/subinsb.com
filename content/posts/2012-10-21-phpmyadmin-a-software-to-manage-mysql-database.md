---
title: phpMyAdmin – A software to manage MySql Database
author: Subin Siby
type: post
date: 2012-10-21T17:18:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on">If you want to manage your <b>MySql</b>&nbsp;table you can use a software called <b>phpMyAdmin</b>. You can install <b>phpMyAdmin</b>&nbsp;in any Operating Systems. This tutorial will help you to install <b>phpMyAdmin</b>&nbsp;in Linux.<br><div><br><h2>Requirements</h2><ul><li><b>PHP</b><ul><li>You need PHP 5.2.0 or newer, with&nbsp;<tt>session</tt>&nbsp;support&nbsp;and the Standard PHP Library (SPL) extension.</li><li>To support uploading of ZIP files, you need the PHP&nbsp;<tt>zip</tt>&nbsp;extension.</li><li>For proper support of multibyte strings (eg. UTF-8, which is currently default), you should install mbstring and ctype extensions.</li><li>You need GD2 support in PHP to display inline thumbnails of JPEGs ("image/jpeg: inline") with their original aspect ratio</li><li>When using the "cookie"&nbsp;authentication method, the&nbsp;<tt>mcrypt</tt>&nbsp;extension is strongly suggested for most users and is&nbsp;<b>required</b>&nbsp;for 64&ndash;bit machines. Not using mcrypt will cause phpMyAdmin to load pages significantly slower.</li><li>To support upload progress bars.</li></ul></li><li><b>MySQL</b>&nbsp;5.0 or newer (details);</li><li><b>Web browser</b>&nbsp;with cookies enabled.</li></ul></div><div>Press <b>ALT + CTRL + T</b>.</div><div>You will get the <b>Terminal</b>&nbsp;window.</div><div>Type the command shown below.</div><blockquote>sudo apt-get install phpmyadmin</blockquote><div><a href="//4.bp.blogspot.com/-MubgN7LY8_c/UIQs7l-2VUI/AAAAAAAACDw/gqoi0IcIa7U/s1600/Screenshot-simsu@simsu-veriton-series:+~.png" imageanchor="1"><img border="0" height="456" src="//4.bp.blogspot.com/-MubgN7LY8_c/UIQs7l-2VUI/AAAAAAAACDw/gqoi0IcIa7U/s640/Screenshot-simsu@simsu-veriton-series:+~.png" width="640"></a></div><br>Press <b>Enter</b>&nbsp;and the installation will begin.<br>You will get a setup window during the installation. Type in the details and continue the installation.<br>One of the setup process will ask for <b>MySql</b>&nbsp;<b>Database</b>&nbsp;password and username so be careful not to type in the wrong password or username.<br>After installation go to <a href="http://localhost/phpmyadmin" target="_blank">localhost/phpmyadmin</a>.<br>You will get a page of a login screen.<br><div><a href="//1.bp.blogspot.com/-dDu5swk797w/UIQr04GcSgI/AAAAAAAACDo/t8rQISvuo28/s1600/Screenshot-phpMyAdmin+-+Google+Chrome.png" imageanchor="1"><img border="0" height="338" src="//1.bp.blogspot.com/-dDu5swk797w/UIQr04GcSgI/AAAAAAAACDo/t8rQISvuo28/s640/Screenshot-phpMyAdmin+-+Google+Chrome.png" width="640"></a></div>Type the username and password of <b>MySql Database</b>. Press <b>Go</b>&nbsp;and you will enter in to <b>phpMyAdmin</b>. That's it.<br>In the next tutorial I will tell you how to use <b>phpMyAdmin</b>.</div>
url: /phpmyadmin-a-software-to-manage-mysql-database
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
syndication_item_hash:
  - cdf165168fcf0111c1000b67b608a62b
  - cdf165168fcf0111c1000b67b608a62b
  - cdf165168fcf0111c1000b67b608a62b
  - cdf165168fcf0111c1000b67b608a62b
  - cdf165168fcf0111c1000b67b608a62b
  - cdf165168fcf0111c1000b67b608a62b
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
  - http://sag-3.blogspot.com/2012/10/phpmyadmin-software-to-manage-mysql.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
categories:
  - Linux
  - PHP
  - phpMyAdmin
  - Ubuntu
tags:
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you want to manage your <b>MySql</b>&nbsp;table you can use a software called <b>phpMyAdmin</b>. You can install <b>phpMyAdmin</b>&nbsp;in any Operating Systems. This tutorial will help you to install <b>phpMyAdmin</b>&nbsp;in Linux.</p> 
  
  <div>
    </p> 
    
    <h2 id="require" style="font-family: sans-serif; max-width: 70em;">
      Requirements
    </h2>
    
    <ul style="font-family: sans-serif; margin: 1em; max-width: 70em;">
      <li style="margin-top: 0.5em;">
        <b>PHP</b> <ul style="margin: 1em 1em 0px; max-width: 70em;">
          <li style="margin-top: 0.5em;">
            You need PHP 5.2.0 or newer, with&nbsp;<tt>session</tt>&nbsp;support&nbsp;and the Standard PHP Library (SPL) extension.
          </li>
          <li style="margin-top: 0.5em;">
            To support uploading of ZIP files, you need the PHP&nbsp;<tt>zip</tt>&nbsp;extension.
          </li>
          <li style="margin-top: 0.5em;">
            For proper support of multibyte strings (eg. UTF-8, which is currently default), you should install mbstring and ctype extensions.
          </li>
          <li style="margin-top: 0.5em;">
            You need GD2 support in PHP to display inline thumbnails of JPEGs ("image/jpeg: inline") with their original aspect ratio
          </li>
          <li style="margin-top: 0.5em;">
            When using the "cookie"&nbsp;authentication method, the&nbsp;<tt>mcrypt</tt>&nbsp;extension is strongly suggested for most users and is&nbsp;<b>required</b>&nbsp;for 64–bit machines. Not using mcrypt will cause phpMyAdmin to load pages significantly slower.
          </li>
          <li style="margin-top: 0.5em;">
            To support upload progress bars.
          </li>
        </ul>
      </li>
      
      <li style="margin-top: 0.5em;">
        <b>MySQL</b>&nbsp;5.0 or newer (details);
      </li>
      <li style="margin-top: 0.5em;">
        <b>Web browser</b>&nbsp;with cookies enabled.
      </li>
    </ul>
  </div>
  
  <div>
    Press <b>ALT + CTRL + T</b>.
  </div>
  
  <div>
    You will get the <b>Terminal</b>&nbsp;window.
  </div>
  
  <div>
    Type the command shown below.
  </div>
  
  <blockquote class="tr_bq">
    <p>
      sudo apt-get install phpmyadmin
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-MubgN7LY8_c/UIQs7l-2VUI/AAAAAAAACDw/gqoi0IcIa7U/s1600/Screenshot-simsu@simsu-veriton-series:+~.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="456" src="//4.bp.blogspot.com/-MubgN7LY8_c/UIQs7l-2VUI/AAAAAAAACDw/gqoi0IcIa7U/s640/Screenshot-simsu@simsu-veriton-series:+~.png" width="640" /></a>
  </div>
  
  <p>
    Press <b>Enter</b>&nbsp;and the installation will begin.<br />You will get a setup window during the installation. Type in the details and continue the installation.<br />One of the setup process will ask for <b>MySql</b>&nbsp;<b>Database</b>&nbsp;password and username so be careful not to type in the wrong password or username.<br />After installation go to <a href="http://localhost/phpmyadmin" style="font-weight: bold;" >localhost/phpmyadmin</a>.<br />You will get a page of a login screen.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-dDu5swk797w/UIQr04GcSgI/AAAAAAAACDo/t8rQISvuo28/s1600/Screenshot-phpMyAdmin+-+Google+Chrome.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="338" src="//1.bp.blogspot.com/-dDu5swk797w/UIQr04GcSgI/AAAAAAAACDo/t8rQISvuo28/s640/Screenshot-phpMyAdmin+-+Google+Chrome.png" width="640" /></a>
  </div>
  
  <p>
    Type the username and password of <b>MySql Database</b>. Press <b>Go</b>&nbsp;and you will enter in to <b>phpMyAdmin</b>. That&#8217;s it.<br />In the next tutorial I will tell you how to use <b>phpMyAdmin</b>.</div>