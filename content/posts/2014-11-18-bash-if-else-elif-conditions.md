---
title: if, else, elif Conditions in Bash
author: Subin Siby
type: post
date: 2014-11-18T03:30:10+00:00
url: /bash-if-else-elif-conditions
categories:
  - Short Post
tags:
  - BASH
  - IF

---
All programming languages have the **if** & **else** conditions and they&#8217;re different in syntax according to each programming languages. In **Bash**, it&#8217;s really different and even a small space can cause syntax errors. So, you have to be careful while writing Bash code.

In this post, I&#8217;ll give you many examples on how to use **if** and **else** conditions in **Bash**.


## if, else

<pre class="prettyprint"><code>a="AC"
if [ $a = "AC" ]; then
	echo "yes"
else
	echo "no"
fi</code></pre>

Notice the usage of **then**. It&#8217;s necessary to let **Bash** know that if the condition returns **TRUE**, do the code below it.

The **if** condition is closed by **fi**. It&#8217;s the way of using "}" to close the condition in other languages and **then**, the way of starting "{" in other languages.

But, **else** doesn&#8217;t have special closing command; the **fi** will do. Unlike "==" in other languages, in Bash just an "=" will do to check if a variable contain a string.

The space between "[" is really necessary. If you remove it, syntax errors like these will occur :

<pre class="prettyprint"><code>file.sh: line 2: [AC: command not found
file.sh: line 2: syntax error near unexpected token `then'
file.sh: line 2: `if[ $a = "a" ]; then</code></pre>

### Examples

Check if the variable "a" doesn&#8217;t have the value "AC" :

<pre class="prettyprint"><code>a="a"
if [ ! $a = "AC" ]; then
    echo "yes, a is not 'AC'"
fi</code></pre>

Check if the file "/dev/null" exist :

<pre class="prettyprint"><code>if [ -a /dev/null ]; then
    echo "File Exists"
else
    echo "File doesn't exist"
fi</code></pre>

Check if the directory "/dev" exist :

<pre class="prettyprint"><code>if [ -d /dev ]; then
    echo "Directory Exists"
else
    echo "Directory doesn't exist"
fi</code></pre>

## elif

This command **elif** is the same as **else if** used in other programming languages. It should be used after **if** and before **fi**.

<pre class="prettyprint"><code>a="AC"
if [ $a = "a" ]; then
    echo "it's 'a'"
elif [ $a = "AC" ]; then
    echo "it's 'AC'"
fi</code></pre>

The **else** command also can be used within this :

<pre class="prettyprint"><code>a="k"
if [ $a = "a" ]; then
    echo "it's 'a'"
elif [ $a = "AC" ]; then
    echo "it's 'AC'"
else
    echo "it's none of it"
fi</code></pre>

### Examples

Check if variable "a" contains "a", else check if the value is "false" :

<pre class="prettyprint"><code>a=false
if [ $a = "a" ]; then
    echo "it's 'a'"
elif [ $a = false ]; then
    echo "it's FALSE"
fi</code></pre>

Check if the value of variable "a" is greater than or less than 51 :

<pre class="prettyprint"><code>a=50
if [ $a -gt 51 ]; then
    echo "it's more than 50"
elif [ $a -lt 51 ]; then
    echo "it's less than 51"
fi</code></pre>

 [1]: #if-else
 [2]: #examples
 [3]: #elif
 [4]: #examples-2
