---
title: Insert XML Data to MySQL Table Using PHP
author: Subin Siby
type: post
date: 2012-08-08T16:46:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on"><span>Hi, This tutorial will help you to insert XML file's node contents to a MySql Database table using php.<a href="http://www.blogger.com/blogger.g?blogID=1163006449268681643#fbinvites" rel="nofollow" title="Invite Your Facebook Friends"></a></span><br><div></div><span><br></span><b><span>Note- If you can't open PHP files in browser in Ubuntu Linux See this Tutorial :&nbsp;</span></b><br><b><span>http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html</span></b><br><div><span><br></span></div><div><span>This is the XML file we are going to insert to the Table.</span><br><blockquote><span>&lt;items&gt;</span><br><span>&nbsp;&lt;item&gt;</span><br><span>&nbsp; &lt;title&gt;Google&lt;/title&gt;</span><br><span>&nbsp; &lt;url&gt;google.com&lt;/url&gt;</span><br><span>&nbsp;&lt;/item&gt;</span><br><span>&nbsp;&lt;item&gt;</span><br><span>&nbsp; &lt;title&gt;Google Accounts&lt;/title&gt;</span><br><span>&nbsp; &lt;url&gt;accounts.google.com&lt;/url&gt;</span><br><span>&nbsp;&lt;/item&gt;</span><br><span>&lt;/items&gt;&nbsp;</span></blockquote></div><div><span>Save the XML file as <b>items.xml&nbsp;</b></span><br><span>MySql Database Table Creation</span><br><span>This is the Sql code to create a table named <b>items.</b></span><br><blockquote><span><span>CREATE TABLE&nbsp;</span><b>items</b><span>(</span><span>id</span><span>&nbsp;INT&nbsp;</span><span>PRIMARY KEY</span><span>&nbsp;</span><span>AUTO_INCREMENT</span><span>,</span><span><span>title</span>&nbsp;TEXT,</span><span>url</span><span>&nbsp;TEXT,</span><span>);</span></span></blockquote><span>Create a PHP file (<b>add.php</b>) in the same directory where <b>links.xml </b>is.</span><br><span><br></span><span>Open the <b>add.php </b>in text editor.</span><br><span>add the line shown below</span><br><blockquote><span>$xmlDoc = new DOMDocument();</span><br><span>$xmlDoc-&gt;load("<span>items.xml</span>");</span><br><span>$mysql_hostname = "<span>hostname eg:localhost</span>";</span><br><span>$mysql_user = "<span>username</span>";</span><br><span>$mysql_password = "<span>password</span>";</span><br><span>$mysql_database = "<span>database_name</span>";</span><br><span>$bd = mysql_connect($mysql_hostname, $mysql_user, $mysql_password)</span><br><span>or die("Opps some thing went wrong");</span><br><span>mysql_select_db($mysql_database, $bd)</span><br><span>or die("Opps some thing went wrong");</span><br><span>$x=$xmlDoc-&gt;getElementsByTagName('item');</span><br><span>for ($i=0; $i&lt;=15000; $i++)</span><br><span>&nbsp; {</span><br><span>&nbsp; $title=$x-&gt;item($i)-&gt;getElementsByTagName('title')</span><br><span>&nbsp; -&gt;item(0)-&gt;childNodes-&gt;item(0)-&gt;nodeValue;</span><br><span>&nbsp; $link=$x-&gt;item($i)-&gt;getElementsByTagName('url')</span><br><span>&nbsp; -&gt;item(0)-&gt;childNodes-&gt;item(0)-&gt;nodeValue;</span><br><span>&nbsp;<span> </span>$insert = "INSERT INTO <span>table_name</span> (title, url) VALUES ('$title', '$link')";</span><br><span>&nbsp;<span> </span>$add_member = mysql_query($insert);</span><br><span>}</span></blockquote><span>Open the file add.php in your website or in a localhost server.</span><br><span>When it's opened it will show each and every nodes in the <span>items.xml</span> file.</span><br><span>It make sure that the xml file contents are added to the Mysql Table.</span><br><span>That's it.</span></div></div>
url: /insert-xml-file-contents-to-mysql-database-table-using-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
  - http://sag-3.blogspot.com/2012/08/insert-xml-file-contents-to-mysql.html
syndication_item_hash:
  - fbaacaf4ec94eb7a4731fe06e605c763
  - fbaacaf4ec94eb7a4731fe06e605c763
  - fbaacaf4ec94eb7a4731fe06e605c763
  - fbaacaf4ec94eb7a4731fe06e605c763
  - fbaacaf4ec94eb7a4731fe06e605c763
  - fbaacaf4ec94eb7a4731fe06e605c763
dsq_thread_id:
  - 1957853067
  - 1957853067
  - 1957853067
  - 1957853067
  - 1957853067
  - 1957853067
categories:
  - Database
  - MySQL
  - PHP
  - SQL
tags:
  - Tutorials
  - XML

---
<div dir="ltr" style="text-align: left;">
  <span style="font-family: inherit;">This tutorial will help you to insert XML file&#8217;s contents to a MySQL Database table using php. </span><span style="font-family: inherit;">Note- If you can&#8217;t open PHP files in browser in Ubuntu Linux See the Tutorial : <a href="//subinsb.com/how-to-open-php-files-in-ubuntu-instead-of-downloading">http://subinsb.com/how-to-open-php-files-in-ubuntu-instead-of-downloading</a></span><b></b></p> 
  
  <div>
    <span style="font-family: inherit;">This is the XML data we are going to insert in to our MySQL Table.</span></p> 
    
    <pre class="prettyprint"><code>&lt;span style="font-family: inherit;">&lt;items&gt;&lt;/span>
 &lt;span style="font-family: inherit;"> &lt;item&gt;&lt;/span>
 &lt;span style="font-family: inherit;">  &lt;title&gt;Google&lt;/title&gt;&lt;/span>
 &lt;span style="font-family: inherit;">  &lt;url&gt;google.com&lt;/url&gt;&lt;/span>
 &lt;span style="font-family: inherit;"> &lt;/item&gt;&lt;/span>
 &lt;span style="font-family: inherit;"> &lt;item&gt;&lt;/span>
 &lt;span style="font-family: inherit;">  &lt;title&gt;Google Accounts&lt;/title&gt;&lt;/span>
 &lt;span style="font-family: inherit;">  &lt;url&gt;accounts.google.com&lt;/url&gt;&lt;/span>
 &lt;span style="font-family: inherit;"> &lt;/item&gt;&lt;/span>
&lt;span style="font-family: inherit;">&lt;/items&gt; &lt;/span></code></pre>
  </div>
</div>

<div>
  <span style="font-family: inherit;">The name of the XML file is "items.xml ".</span>
</div>

## MySQL Database Table Creation

<div>
  <span style="font-family: inherit;">This is the SQL code to create a table named "items" in which we will add the XML data to.</span></p> 
  
  <pre class="prettyprint"><code>&lt;span style="line-height: 16px; text-align: -webkit-auto;">CREATE TABLE &lt;/span>&lt;b style="border: 0px none; color: blue; line-height: 16px; margin: 0px; outline: 0px; padding: 0px; text-align: -webkit-auto;">items&lt;/b>&lt;span style="line-height: 16px; text-align: -webkit-auto;">(&lt;/span>&lt;span style="border: 0px none; color: #cc0000; line-height: 16px; margin: 0px; outline: 0px; padding: 0px; text-align: -webkit-auto;">id&lt;/span>&lt;span style="line-height: 16px; text-align: -webkit-auto;"> INT &lt;/span>&lt;span style="border: 0px none; color: blue; line-height: 16px; margin: 0px; outline: 0px; padding: 0px; text-align: -webkit-auto;">PRIMARY KEY&lt;/span>&lt;span style="line-height: 16px; text-align: -webkit-auto;"> &lt;/span>&lt;span style="border: 0px none; color: #a64d79; line-height: 16px; margin: 0px; outline: 0px; padding: 0px; text-align: -webkit-auto;">AUTO_INCREMENT&lt;/span>&lt;span style="line-height: 16px; text-align: -webkit-auto;">, &lt;/span>&lt;span style="line-height: 16px; text-align: -webkit-auto;">&lt;span style="color: #cc0000;">title&lt;/span> TEXT, &lt;/span>&lt;span style="border: 0px none; color: #cc0000; line-height: 16px; margin: 0px; outline: 0px; padding: 0px; text-align: -webkit-auto;">url&lt;/span>&lt;span style="line-height: 16px; text-align: -webkit-auto;"> TEXT&lt;/span>&lt;span style="line-height: 16px; text-align: -webkit-auto;">);&lt;/span></code></pre>
  
  <p>
    <span style="font-family: inherit;">Create a PHP file named "add.php" in the same directory where the XML file is.</span> <span style="font-family: inherit;">Open the <strong>add.php</strong> file<b> </b>in text editor and add the following code into it :</span>
  </p>
  
  <pre class="prettyprint"><code>$xmlDoc = new DOMDocument();
$xmlDoc-&gt;load("items.xml");
$mysql_hostname = "hostname"; // Example : localhost
$mysql_user     = "username";
$mysql_password = "password";
$mysql_database = "database_name";

$bd = mysql_connect($mysql_hostname, $mysql_user, $mysql_password) or die("Oops some thing went wrong");
mysql_select_db($mysql_database, $bd) or die("Oops some thing went wrong");

$xmlObject = $xmlDoc-&gt;getElementsByTagName('item');
$itemCount = $xmlObject-&gt;length;

for ($i=0; $i &lt; $itemCount; $i++){
  $title = $xmlObject-&gt;item($i)-&gt;getElementsByTagName('title')-&gt;item(0)-&gt;childNodes-&gt;item(0)-&gt;nodeValue;
  $link  = $xmlObject-&gt;item($i)-&gt;getElementsByTagName('url')-&gt;item(0)-&gt;childNodes-&gt;item(0)-&gt;nodeValue;
  $sql   = "INSERT INTO `my_table_name` (title, url) VALUES ('$title', '$link')";
  mysql_query($sql);
  print "Finished Item $title n&lt;br/&gt;";
}</code></pre>
  
  <p>
    We&#8217;re using the depreciated <strong>mysql_*</strong> functions in the above code, but for a simple task like this, it can be used. If you really want the latest technology, use <strong>PDO</strong> and the code will be something like this :
  </p>
  
  <pre class="prettyprint"><code>$xmlDoc = new DOMDocument();
$xmlDoc-&gt;load("items.xml");
$mysql_hostname = "hostname"; // Example : localhost
$mysql_user = "username";
$mysql_password = "password";
$mysql_database = "database_name";

$dbh = new PDO("mysql:dbname={$mysql_database};host={$mysql_hostname};port=3306", $mysql_user, $mysql_password);

$xmlObject = $xmlDoc-&gt;getElementsByTagName('item');
$itemCount = $xmlObject-&gt;length;

for ($i=0; $i &lt; $itemCount; $i++){
   $title = $xmlObject-&gt;item($i)-&gt;getElementsByTagName('title')-&gt;item(0)-&gt;childNodes-&gt;item(0)-&gt;nodeValue;
   $link = $xmlObject-&gt;item($i)-&gt;getElementsByTagName('url')-&gt;item(0)-&gt;childNodes-&gt;item(0)-&gt;nodeValue;
   $sql = $dbh-&gt;prepare("INSERT INTO `my_table_name` (`title`, `url`) VALUES (?, ?)");
   $sql-&gt;execute(array(
     $title,
     $link
   ));
   print "Finished Item $title n&lt;br/&gt;";
}</code></pre>
  
  <p>
    As you see, you have to change the variables for connecting to Database. <span style="font-family: inherit;">Open the file "add.php" in your website or in a localhost server Or Execute it via terminal with the command (Linux) :</span>
  </p>
  
  <pre class="prettyprint"><code>php add.php</code></pre>
  
  <p>
    <span style="font-family: inherit;">When it&#8217;s opened it will show a success message of every nodes in the items.xml file to </span><span style="font-family: inherit;">make sure that the XML file contents are added to the MySQL Table.</span> <span style="font-family: inherit;">That&#8217;s it.</span>
  </p>
</div>