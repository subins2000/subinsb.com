---
title: SQL Query in another SQL Query = SQL Subquery
author: Subin Siby
type: post
date: 2012-11-17T14:30:00+00:00
excerpt: "You might have wanted to do a SQL&nbsp;query inside an another SQL&nbsp;query. You can do this using SQL Subquery. It's simple as ABC.&nbsp;Here's an example :SELECT * FROM&nbsp;`fdposts`&nbsp;WHERE user IN (SELECT friend FROM `fdfriends` WHERE user='s..."
url: /sql-query-in-another-sql-query-sql-subquery
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 09ca6381ee09144242bf470b61436e0c
  - 09ca6381ee09144242bf470b61436e0c
  - 09ca6381ee09144242bf470b61436e0c
  - 09ca6381ee09144242bf470b61436e0c
  - 09ca6381ee09144242bf470b61436e0c
  - 09ca6381ee09144242bf470b61436e0c
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
  - http://sag-3.blogspot.com/2012/11/sql-subquery.html
categories:
  - MySQL
  - PHP
  - SQL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You might have wanted to do a <b>SQL</b>&nbsp;query inside an another <b>SQL</b>&nbsp;query. You can do this using <b>SQL Subquery</b>. It&#8217;s simple as <b>ABC</b>.&nbsp;Here&#8217;s an example :</p> 
  
  <blockquote class="tr_bq">
    <p>
      SELECT * FROM&nbsp;`fdposts`&nbsp;WHERE user IN (SELECT friend FROM `fdfriends` WHERE user=&#8217;subins2000&#8242;)
    </p>
  </blockquote>
  
  <p>
    The above code will select rows in table&nbsp;<b>fdposts</b>&nbsp;where user is the value of <b>friend</b> row in the table <b>fdfriends</b>.<br />As you can see it&#8217;s very simple. Here&#8217;s another example :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      SELECT * FROM&nbsp;`posts`&nbsp;WHERE user IN (SELECT name FROM `members` WHERE user=&#8217;subins2000&#8242;)
    </p>
  </blockquote>
  
  <p>
    The above code will select rows in table&nbsp;<b>posts </b>where user is the value of the row <b>name</b>&nbsp;in the table <b>members</b>.<br />Enjoy !!!</div>