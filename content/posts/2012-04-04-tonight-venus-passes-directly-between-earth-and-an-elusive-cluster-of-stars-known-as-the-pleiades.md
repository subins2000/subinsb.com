---
title: Tonight Venus passes directly between Earth and an elusive cluster of stars known as the Pleiades.
author: Subin Siby
type: post
date: 2012-04-04T06:38:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on">Once every eight years, a luminous Venus passes directly between Earth and an elusive cluster of stars known as the Pleiades. Tonight, you'll have a chance to witness this event with your own eyes.<br><div>The photo up top was taken a few days ago on the evening of March 31st, and showcases the two astronomical entities that you'll be looking for. Obvious-as-ever is Venus, the brightest object in the night sky save for the Moon, beaming on the right. Less dazzling, but conspicuous in its own right, is the cluster of blue stars gathered on the left. These are the Pleiades.<br><div><a href="//1.bp.blogspot.com/-GmPjyMmbt8c/Ub1kZlLqhcI/AAAAAAAACvo/zRI0aTWhrtA/s1600/0016.jpg" imageanchor="1"><img border="0" src="//1.bp.blogspot.com/-GmPjyMmbt8c/Ub1kZlLqhcI/AAAAAAAACvo/zRI0aTWhrtA/s1600/0016.jpg"></a></div></div><div>Also known as Messier object 45 (M45) or the "Seven Sisters," the young star cluster is thought to have formed less than 100 million years ago, which helps explain&nbsp;the blue-white hue of its largest members. At just 400 light years away, the Pleiades are still visible with the naked eye, but they're usually easier to spot with a pair of binoculars or a small telescope. Tonight, however, you should be able to spot them due to their proximity to Venus. According to NASA, the planet will join the Pleiades in the night sky beginning just after sunset, making it "look like a supernova has gone off inside the cluster."</div><div></div><div>The cosmic conjunction began last night, and will remain visible through Wednesday, when Venus finishes passing across the face of the cluster. Tonight, however, promises to provide the most spellbinding view. Look west as soon as the Sun dips below the horizon, and remember to look for the light of Venus &mdash; you'll find it resting in the company of the Seven Sisters. (Also: be sure to check out&nbsp;this breathtaking photo of the Pleiades posing with Ecuador's active Tungurahua volcano.)</div><div><br></div><div>Here's another picture</div><div><br></div><div><a href="//3.bp.blogspot.com/-tG5DPXPeKgE/T3vsGOqcrtI/AAAAAAAAAmw/qeF-7nhlHWg/s1600/venus+show.png" imageanchor="1"><img border="0" src="//3.bp.blogspot.com/-tG5DPXPeKgE/T3vsGOqcrtI/AAAAAAAAAmw/qeF-7nhlHWg/s1600/venus+show.png"></a></div><div><br></div><div><a href="//1.bp.blogspot.com/-yy4pjees9oc/T3vs7aKB75I/AAAAAAAAAm4/2VAocAH3NtA/s1600/venus-show.png" imageanchor="1"><img border="0" src="//1.bp.blogspot.com/-yy4pjees9oc/T3vs7aKB75I/AAAAAAAAAm4/2VAocAH3NtA/s1600/venus-show.png"></a></div></div>
url: /tonight-venus-passes-directly-between-earth-and-an-elusive-cluster-of-stars-known-as-the-pleiades
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 09a9ab340079f3e15902936bb4fdd2c1
  - 09a9ab340079f3e15902936bb4fdd2c1
  - 09a9ab340079f3e15902936bb4fdd2c1
  - 09a9ab340079f3e15902936bb4fdd2c1
  - 09a9ab340079f3e15902936bb4fdd2c1
  - 09a9ab340079f3e15902936bb4fdd2c1
syndication_permalink:
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
  - http://sag-3.blogspot.com/2012/04/tonight-venus-passes-directly-between.html
tags:
  - Sky

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Once every eight years, a luminous Venus passes directly between Earth and an elusive cluster of stars known as the Pleiades. Tonight, you&#8217;ll have a chance to witness this event with your own eyes.</p> 
  
  <div style="background-color: white; font-family: Georgia, Times, 'Liberation Serif', serif; font-size: 15px; line-height: 22px; text-align: -webkit-auto;">
    The photo up top was taken a few days ago on the evening of March 31st, and showcases the two astronomical entities that you&#8217;ll be looking for. Obvious-as-ever is Venus, the brightest object in the night sky save for the Moon, beaming on the right. Less dazzling, but conspicuous in its own right, is the cluster of blue stars gathered on the left. These are the Pleiades.</p> 
    
    <div class="separator" style="clear: both; text-align: center;">
      <a href="//1.bp.blogspot.com/-GmPjyMmbt8c/Ub1kZlLqhcI/AAAAAAAACvo/zRI0aTWhrtA/s1600/0016.jpg" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-GmPjyMmbt8c/Ub1kZlLqhcI/AAAAAAAACvo/zRI0aTWhrtA/s1600/0016.jpg" /></a>
    </div>
  </div>
  
  <div style="background-color: white; font-family: Georgia, Times, 'Liberation Serif', serif; font-size: 15px; line-height: 22px; text-align: -webkit-auto;">
    Also known as Messier object 45 (M45) or the "Seven Sisters," the young star cluster is thought to have formed less than 100 million years ago, which helps explain&nbsp;the blue-white hue of its largest members. At just 400 light years away, the Pleiades are still visible with the naked eye, but they&#8217;re usually easier to spot with a pair of binoculars or a small telescope. Tonight, however, you should be able to spot them due to their proximity to Venus. According to NASA, the planet will join the Pleiades in the night sky beginning just after sunset, making it "look like a supernova has gone off inside the cluster."
  </div>
  
  <div style="background-color: white; clear: both; color: #222222; font-family: Georgia, Times, 'Liberation Serif', serif; font-size: 15px; line-height: 22px; text-align: -webkit-auto;">
  </div>
  
  <div style="background-color: white; font-family: Georgia, Times, 'Liberation Serif', serif; font-size: 15px; line-height: 22px; text-align: -webkit-auto;">
    The cosmic conjunction began last night, and will remain visible through Wednesday, when Venus finishes passing across the face of the cluster. Tonight, however, promises to provide the most spellbinding view. Look west as soon as the Sun dips below the horizon, and remember to look for the light of Venus — you&#8217;ll find it resting in the company of the Seven Sisters. (Also: be sure to check out&nbsp;this breathtaking photo of the Pleiades posing with Ecuador&#8217;s active Tungurahua volcano.)
  </div>
  
  <div style="background-color: white; font-family: Georgia, Times, 'Liberation Serif', serif; font-size: 15px; line-height: 22px; text-align: -webkit-auto;">
  </div>
  
  <div style="background-color: white; font-family: Georgia, Times, 'Liberation Serif', serif; font-size: 15px; line-height: 22px; text-align: -webkit-auto;">
    Here&#8217;s another picture
  </div>
  
  <div style="background-color: white; font-family: Georgia, Times, 'Liberation Serif', serif; font-size: 15px; line-height: 22px; text-align: -webkit-auto;">
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-tG5DPXPeKgE/T3vsGOqcrtI/AAAAAAAAAmw/qeF-7nhlHWg/s1600/venus+show.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-tG5DPXPeKgE/T3vsGOqcrtI/AAAAAAAAAmw/qeF-7nhlHWg/s1600/venus+show.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-yy4pjees9oc/T3vs7aKB75I/AAAAAAAAAm4/2VAocAH3NtA/s1600/venus-show.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-yy4pjees9oc/T3vs7aKB75I/AAAAAAAAAm4/2VAocAH3NtA/s1600/venus-show.png" /></a>
  </div>
</div>