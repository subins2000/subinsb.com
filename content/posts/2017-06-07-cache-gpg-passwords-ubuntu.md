---
title: How To Cache GPG Passphrase In Ubuntu
author: Subin Siby
type: post
date: 2017-06-07T18:25:13+00:00
url: /cache-gpg-passwords-ubuntu
categories:
  - Linux
  - Operating System
  - Ubuntu
  - XFCE
tags:
  - BASH
  - GNU
  - GPG

---
I got into Debian packaging and one of the most brutal part was to type in the GPG secret key&#8217;s password every time I sign a package. I wasted a lot of time typing my long password.

I searched a lot to cache my password for some time after I type it once. All of them said to edit **gpg-agent.conf**, but none of them worked for my fresh installation of **Xubuntu 16.04**. I&#8217;m writing this to save you a lot of trouble.

First of all, install seashore :

<pre class="prettyprint"><code>sudo apt install seashore</code></pre>

Seashore is a great application to manage your keys. I&#8217;m asking you to install seashore as it comes with a graphical pinentry tool.

## Configuring gpg-agent

Edit the file `/home/username/gpg-agent.conf` and add these two lines :

<pre class="prettyprint"><code>default-cache-ttl 1800
max-cache-ttl 1800</code></pre>

If your gpg-agent version is less than 2 (you can check it by running `gpg-agent --version`), add these lines instead :

<pre class="prettyprint"><code>default-cache-ttl 1800
maximum-cache-ttl 1800</code></pre>

The value gives the maximum amount of time the password should be cached. **1800 seconds** mean 30 minutes. Here are some conversions :

> 600 seconds = 10 minutes
  
> 1800 seconds = 30 minutes
  
> 3600 seconds = 60 minutes = 1 hour

## Configure gpg

This is the secret sauce I&#8217;m going to give you that might solve your problem. This is what worked for me.

Edit the file `/home/username/.gnupg/gpg.conf`. If it doesn&#8217;t exist (most likely) create it. Add this line to it :

<pre class="prettyprint"><code>use-agent</code></pre>

This makes gpg to use an agent to enter passwords.

Restart gpg-agent and you&#8217;re all set :

<pre class="prettyprint"><code>killall -q gpg-agent && gpg-agent --daemon
</code></pre>

Now when you need to enter the password, a graphical dialog will be brought up :<figure class="wp-caption aligncenter">

[<img class="size-medium" src="https://lab.subinsb.com/projects/blog/uploads/2017/06/gpg-pinentry.png" alt="pinentry dialog" width="392" height="243" />][1]<figcaption class="wp-caption-text">Pinentry dialog for entering password to unlock GPG private key</figcaption></figure> 

and after you enter it, it&#8217;ll be cached according to gpg-agent configuration file.

 [1]: https://lab.subinsb.com/projects/blog/uploads/2017/06/gpg-pinentry.png