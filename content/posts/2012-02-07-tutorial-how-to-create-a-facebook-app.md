---
title: How To Create A Facebook App
author: Subin Siby
type: post
date: 2012-02-07T15:34:00+00:00
excerpt: 'Go to&nbsp;https://developers.facebook.com/ajax/create&nbsp;and create a new app.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Type your &nbsp;App name in "App Display Name". You dont need to type anyt...'
url: /tutorial-how-to-create-a-facebook-app
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
syndication_item_hash:
  - 1e45e7fb28f67d3d4c41c30b58b9172a
  - 1e45e7fb28f67d3d4c41c30b58b9172a
  - 1e45e7fb28f67d3d4c41c30b58b9172a
  - 1e45e7fb28f67d3d4c41c30b58b9172a
  - 1e45e7fb28f67d3d4c41c30b58b9172a
  - 1e45e7fb28f67d3d4c41c30b58b9172a
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
  - http://sag-3.blogspot.com/2012/02/how-to-create-facebook-app.html
categories:
  - Facebook
tags:
  - Tutorials

---
<p style="margin-bottom: 0cm;" align="LEFT">
  <span style="color: black;"><span style="font-family: 'Times New Roman';">Facebook App is something every site or app wants. A Facebook App can be used for so many purposes like <strong>Login With Facebook</strong> feature ,for a game on <strong>Facebook</strong> and so many others. Since there are so many Facebook users, it&#8217;s necessary to have a Facebook App for your site. In this post I &#8216;m going to tell you how to create a <strong>Facebook</strong> App.</span></span>
</p>

<p style="margin-bottom: 0cm;" align="LEFT">
  <span style="color: black;"><span style="font-family: 'Times New Roman';">Go to </span></span><a href="https://developers.facebook.com/apps" target="_blank">https://developers.facebook.com/apps</a><span style="color: black;"><span style="font-family: 'Times New Roman';"> and click the "Create New App" button from the "Apps" menu on the top :</span></span>
</p>

<p style="margin-bottom: 0cm;" align="LEFT">
  <a href="//lab.subinsb.com/projects/blog/uploads/2012/02/0082.png"><img class="aligncenter size-medium wp-image-2366" alt="0082" src="//lab.subinsb.com/projects/blog/uploads/2012/02/0082-300x79.png" width="300" height="79" /></a>
</p>

<p style="margin-bottom: 0cm;" align="LEFT">
  <p style="margin-bottom: 0cm;" align="LEFT">
    The App  Creation dialog will be opened :
  </p>
  
  <p style="margin-bottom: 0cm; text-align: center;" align="LEFT">
    <a href="//lab.subinsb.com/projects/blog/uploads/2012/02/0083.png"><img class="aligncenter size-medium wp-image-2367" alt="0083" src="//lab.subinsb.com/projects/blog/uploads/2012/02/0083-300x156.png" width="300" height="156" /></a>
  </p>
  
  <p style="margin-bottom: 0cm; text-align: center;" align="LEFT">
    <p style="margin-bottom: 0cm;" align="LEFT">
      <p style="margin-bottom: 0cm;" align="LEFT">
        <p style="margin-bottom: 0cm;">
          <p style="margin-bottom: 0cm;">
            <p style="margin-bottom: 0cm;">
              <p style="margin-bottom: 0cm;">
                <p style="margin-bottom: 0cm;">
                  <p style="margin-bottom: 0cm;">
                    <p>
                      <span style="color: black;"><span style="font-family: 'Times New Roman';">Type your App name in "Display Name".</span></span>
                    </p>
                    
                    <p>
                      <span style="color: black;"><span style="font-family: 'Times New Roman';">It&#8217;s not required to type anything on "App Namespace". If you are creating a canvas app (App On Facebook), then you should fill this with a name. This make your app accessible on :</span></span>
                    </p>
                    
                    <pre class="prettyprint"><code>https://apps.facebook.com/{namespace}</code></pre>
                    
                    <p>
                      <span style="color: black;"><span style="font-family: 'Times New Roman';"></span></span>
                    </p>
                    
                    <p>
                      <span style="color: black;"><span style="font-family: 'Times New Roman';">By submitting, you are agreeing to the Facebook Platform Policies.Click <strong>Continue</strong> button.</span></span>
                    </p>
                    
                    <p style="margin-bottom: 0cm;">
                      <span style="color: black;"><span style="font-family: 'Times New Roman';">Your app is being created. After creating, you will be redirected to your app page :</span></span>
                    </p>
                    
                    <p style="margin-bottom: 0cm;">
                      <a href="//lab.subinsb.com/projects/blog/uploads/2012/02/0084.png"><img class="aligncenter size-medium wp-image-2368" alt="0084" src="//lab.subinsb.com/projects/blog/uploads/2012/02/0084-300x185.png" width="300" height="185" /></a>
                    </p>
                    
                    <p style="margin-bottom: 0cm;">
                      <p style="margin-bottom: 0cm;">
                        If you want to change settings like adding an image to your app, go to the "Settings" page which you can see on the left side of your app page.
                      </p>
                      
                      <p>
                        From your main app page you can obtain the Application <strong>ID </strong>and <strong>Application </strong><strong>Secret </strong>values. You may need to enter your password if you want to see the "App Secret" value. To see it, click on the "Show" button near the "App Secret" input.
                      </p>