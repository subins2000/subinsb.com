---
title: Position AddThis Recommended Content Section With jQuery
author: Subin Siby
type: post
date: 2014-02-09T13:30:52+00:00
url: /position-addthis-recommended-content-section-using-jquery
authorsure_include_css:
  - 
  - 
  - 
categories:
  - CSS
  - HTML
  - JavaScript
  - jQuery
tags:
  - AddThis
  - Tutorials

---
AddThis smart layer have a recommended content feature that adds a recommended content box to your page. This box is by default will be placed at the end of the page. A normal user won&#8217;t notice this. To make the recommended content noticeable, you have to move it to someplace else. But, AddThis doesn&#8217;t support the positioning of the recommended content.

So, the only way to move the recommended content box is to find a hack. I found the hack and I&#8217;m going to share it with you. All your blog or website should need is a jQuery library loaded on the page and the AddThis Smart Layers Recommended Content Feature. The rest will be taken care by the script we are going to add.

You can see a demo if you scroll to the bottom of this blog just after the comments section. :<figure id="attachment_2453" class="wp-caption aligncenter">

[<img class="size-large wp-image-2453" alt="AddThis Smart Layer Recommended Content Above Footer" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0099-1024x415.png" width="474" height="192" />][1]<figcaption class="wp-caption-text">AddThis Smart Layer Recommended Content Above Footer</figcaption></figure> 

Add the following code before **</body>** or **</head>** of your HTML page. Or you can add this to a separate file and load it using **<script>** tag.

<pre class="prettyprint"><code>var __subinsbdotcomAddThist=".at4-recommended-outer-container";
function __subinsbdotcomAddThisintervalTrigger() {
 return window.setInterval(function(){
 if(jQuery(".cloned-at4").length==0 && jQuery(__subinsbdotcomAddThist).length!=0 && jQuery(__subinsbdotcomAddThist).text()!="" && jQuery(window).width() &gt; 1008){
  jQuery(__subinsbdotcomAddThist).clone().addClass("cloned-at4").insertBefore("footer").wrap("&lt;div style='z-index:99999999;position:relative;background:#F5F5F5;border-top:1px solid black;'&gt;&lt;/div&gt;");
  jQuery(__subinsbdotcomAddThist).not(".cloned-at4").remove();
  jQuery(".at4-recommended-recommended").css("cssText","padding:10px 0px;opacity:1 !important;");
  jQuery(".at4-visually-hidden").css("cssText","position: relative!important;");
  window.clearInterval(__subinsbdotcomAddThis);
 }
 },2500);
}
var __subinsbdotcomAddThis=__subinsbdotcomAddThisintervalTrigger();</code></pre>

Mention the place where you want to show it by changing the **insertBefore("footer")** code. By default it will be appended before the **<footer>**. If you want to place it after the Disqus comment box, replace the **insertBefore** code with the following :

<pre class="prettyprint"><code>insertBefore("#disqus_thread")</code></pre>

The variables used in the code are unique, so that it won&#8217;t conflict with any other codes. When the page starts loading a function will start to initiate every **2.5** seconds. The function checks if the AddThis Smart Layers Recommended Content is loaded. If it&#8217;s loaded, then it will be cloned and moved to the place you mentioned. By default it&#8217;s above the **<footer>**.

If you need help in positioning the AddThis Smart Layer etc&#8230; post a comment and I will be glad to help you.

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0099.png