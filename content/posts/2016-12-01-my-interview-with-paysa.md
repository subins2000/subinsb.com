---
title: My Interview With Paysa
author: Subin Siby
type: post
date: 2016-12-01T02:50:53+00:00
lastmod: 2018-07-23
url: /my-interview-with-paysa
categories:
  - Personal

---
**Paysa** is the world&#8217;s first platform to empower individuals to maximize their salary across the span of their career.

I have taken a look at **Paysa** and it&#8217;s wonderful to know that you can get self-evaluated about your career and salary. I didn&#8217;t know there was one. It also gives you guidance to follow the career path of your choice. An excellent service indeed !

I&#8217;m very happy that such a company like **Paysa** has interviewed me. Thank you **Paysa**. Please do [read it][1] !

<div class="padlinks">
  <a class="demo" href="https://www.paysa.com/blog/expert-interview-series-subin-siby/">Read Interview</a>
</div>

 [1]: https://www.paysa.com/blog/2016/11/28/expert-interview-series-subin-siby/
