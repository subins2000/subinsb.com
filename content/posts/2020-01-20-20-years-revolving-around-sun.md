---
title: 20 Years Of Revolving Around The Sun
author: Subin Siby
type: post
date: 2020-01-20T06:29:06+00:00
lastmod: 2020-05-20T06:00:06+00:00
url: /20-years-revolving-around-sun
aliases:
    - /20
categories:
  - Personal
tags:
  - birth

---

Writing this at Delhi airport waiting for the flight back home. I came to the capital city of India for [KDE India Conference](https://conf.kde.in/).

This blog is a part of the `revolving around the sun` series :

* [19 years of revolving around the sun](/19/)
* [18 years of revolving around the sun](/18/)
* [17 years of revolving around the sun](/17/)

This birthday is very fancy :
```php
01-20-2020 20:20:20
MM-DD-YYYY HH:mm:SS
```

I'm turning **20** on **20** in the year of **2020**. Very fancy ! And at `20:20` (8PM) tonight, it's the peak moment of fanciness 😂. The next such fanciness will be in the year `3030` at `1030` years old ? Nopeeeee, this is my fanciest birthday timestamp ever ! :P 😝

I have officially revolved around the sun 20 times ! Woah ! At the beginning of the last decade, I was in 5th grade and at the end, I'm in college 3rd year. That's a lot of time eh!

A tagline that I've been using for the blog were "A 21st century teen". Gonna have to replace "teen" with "millenial" or something else hehe 😁😌.

## Coding

* Went to many events, networked with people
* Contributed to [IndicNLP project](https://indicnlp.org/)
* I participated in my first ever International Hackathon, ETHIndia and won it !!
* Participated in national hackathon InOut. Didn't win any thing, but learnt about P2P tech, WebTorrent and those cool stuff. Torrent is now my favorite thing
* Released [Manglish app](https://www.reddit.com/r/Kerala/comments/bvygio/ive_made_this_android_app_to_convert_malayalam_to/). [Blog on it](/manglish)
* Released [Varnam Editor](https://www.reddit.com/r/india/comments/cicpcz/i_made_a_hinglish_to_hindi_editor_other_languages/). [Blog on it](/varnam)

### KDE

I'm now a KDE contributor ! I'm a maintainer of KDE Malayalam localization now ! This gives me a developer access to KDE's localization SVN repo. This means I can directly push to it which is kinda dangerous too 😬.

In the [post of '19](/19/), I shared my wish of getting selected for GSoC with KDE. I made my [own project idea proposal](https://gitlab.com/snippets/1838918) on improving the localization experience, but the community was divided on whether implementing it. So, that went down.

But !

I implemented that proposal anyway for our Malayalam localization. And it worked ! [KDE Malayalam localization](https://wiki.smc.org.in/KDE) is being done [now on Weblate](https://kde.smc.org.in/). The whole step to implement is [documented here](//github.com/subins2000/kde-weblate) which might be useful for other localization teams.

This led me to work with the [SMC Team](https://smc.org.in/) and become a SMC contributor.

Here's [the Twitter thread](https://twitter.com/SubinSiby/status/1162390557332627456?s=20) on completing a part of localization and having Malayalam included with KDE after many long years.

> ഒരു വെടിക്ക് രണ്ടു പക്ഷി => A contributor to both SMC & KDE.

## PyCon

I attended my very first PyCon last October in Chennai. It was a very big event. Lot of people from across India attended it. Got to meet some online folks in real life. Got some swags participating in contests in some booths.

I did a [lightning talk](https://www.youtube.com/watch?v=zIWdH1cV88s) on my project [Selfie A Day](https://github.com/subins2000/SelfieADay). It was both exciting and anxious to do a talk in front a wide audience. I have a habit of talking too fast and that helped me to finish this talk within the alloted time of 5 minutes.

Since I have to go to Chennai and it's a conf, I wanted to make it a bit more grand. My family has never got in an aeroplane before. My first was also last year when I came back from Bangalore attending ETHIndia. So I asked my family to come with. They can see Chennai while I attend the conf.

We went to Chennai by train. We haven't ever went on such a long journey together (Appa was never the travelling vibe person). It was a new experience for us. We enjoyed Chennai and we all got to fly back ! Amma was scared in her first flight. I love it though, the turbulences are a poor human's way to experience *almost* 0g.

So much has happened to write, but it better stay as memories & photographs in our minds.

I had some savings doing freelance and used that to buy the plane tickets. I'm proud that I got to do this for my family (eldest son extra proundness :P). Amma was so happy of all this :)

UPDATE: With 2020, lockdowns, masks & air-travel not being the same anymore, we're super glad that we did this before it all went down.

## College

### Academics

Just like ['19](/19/), I was more outside of college than in. I was afraid that I won't pass all subjects in semester 3rd last time. But that didn't happen. I passed all subjects, but with bad grades tho. Mehhh, it's alright.

The 5th semester exam just finished at the beginning of this month, and yes, I'm afraid there's a chance that I'll fail on atleast 1 subject :/ 😕.

### FOSSers

FOSSers is there, but I feel like it wasn't as active as the year previous to ['19](/19/).

For the first time, we had a [FOSS Stall in our college tech fest](http://fossers.vidyaacademy.ac.in/blog/vyvidh-2019/). We also did a [online puzzle-solving competition called CrypTux](http://cryptux.vidyaacademy.ac.in/), an offline treasure hunt event and more... All of the software we made/used is [open sourced](https://github.com/FOSSersVAST) of course ;)

![Group photo on tech-fest. 2019 February](//lab.subinsb.com/projects/blog/uploads/2020/01/fossers-group-pic-2019.jpg)

We did a [Debian Packaging workshop](https://news.vidyaacademy.ac.in/2019/08/28/fossers-club-of-vidya-conducts-one-day-workshop-on-debian-packaging/). We got to invite [Pirate Praveen](https://qa.debian.org/developer.php?login=praveen@debian.org) and [Sruthi Chandran](https://qa.debian.org/developer.php?login=srud@debian.org).

Attending the workshop, one FOSSer (oh yeah, that's a name we can call each member :D), [Abhijith Sheheer](https://abspython.gitlab.io/) digged deep into packaging. He has got more than 5 packages into Debian at the time of writing this.

We introduced Inkscape to [Radhika Sharma](https://twitter.com/radhikaa2001) and now she's really good at it ! She's making all her posters, artwork with Inkscape.

[Jijin KH](https://twitter.com/JijinKh), [Athul Raj](https://twitter.com/Outoftune2000), [Muhammed Hashim](https://twitter.com/Hashimjvz) worked on [making a bot](//github.com/FOSSersVAST/vaasu) to easily get our college attendance. It works ! We call the bot `VAASU` (`V`AST `A`ttendance `A`nalyzer & `S`tats `U`tilizer) :P

We hope to do more, grander events and more contributions.

## Life

The past year's been good, though I feel I could have done more. Time is moving pretty fast. Next year, at this time, I'll be in my last semester of college. Woah ! It was like yesterday I joined college.

I've always been a nostalgic person, having butterflies in stomach thinking of the past and memories. Y'know that feeling ? And I'm getting nostalgic now. Just 1 more year and my college life comes to an end. Another part of my life is going to end.

> ഈ കോളേജ് കാലം ഒക്കെ ഒന്നല്ലേ ഉള്ളൂ ഫ്രണ്ടേ

[Gonna listen to this song and delve in nostalgia](https://www.youtube.com/watch?v=nSSZvroFmtc).