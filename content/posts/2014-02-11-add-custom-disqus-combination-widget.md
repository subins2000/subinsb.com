---
title: How To Add Disqus Combination Widget With Customization
author: Subin Siby
type: post
date: 2014-02-11T16:47:22+00:00
url: /add-custom-disqus-combination-widget
authorsure_include_css:
  - 
  - 
  - 
categories:
  - CSS
  - HTML
  - JavaScript
tags:
  - Disqus

---
Disqus is one of the most used commenting systems in websites. It&#8217;s easy, fast and customizable. Disqus also support real time comments. Like any other commenting systems, Disqus provides various widgets for showcasing comments, discussions and commenters. They also have a widget that combines all these together. It is very easy to install this "combination" widget.<figure id="attachment_2469" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2469" alt="Disqus Combination Widget" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0100-145x300.png" width="145" height="300" />][1]<figcaption class="wp-caption-text">Disqus Combination Widget</figcaption></figure> 

You can include this code in Blogger&#8217;s HTML Widget or in WordPress&#8217; Text Widget :

<pre class="prettyprint"><code>&lt;div id="CombComments" class="dsq-widget"&gt;
 &lt;script type="text/javascript" src="http://example.disqus.com/combination_widget.js?num_items=5&hide_mods=0&color=grey&default_tab=popular&excerpt_length=100&hide_avatars=0"&gt;&lt;/script&gt;
&lt;/div&gt;</code></pre>

Replace "example" short name in the above code to your Disqus forum short name.

You can configure the widget by setting values on GET parameters :

<table class="table">
  <tr>
    <td>
      Name
    </td>
    
    <td>
      Default Value
    </td>
    
    <td>
      Description
    </td>
  </tr>
  
  <tr>
    <td>
      num_items
    </td>
    
    <td>
      5
    </td>
    
    <td>
      The number of items that should be shown (includes all items).
    </td>
  </tr>
  
  <tr>
    <td>
      hide_mods
    </td>
    
    <td>
    </td>
    
    <td>
      Set To "1" to hide Moderators&#8217; comments
    </td>
  </tr>
  
  <tr>
    <td>
      color
    </td>
    
    <td>
      grey
    </td>
    
    <td>
      The color theme of widget
    </td>
  </tr>
  
  <tr>
    <td>
      default_tab
    </td>
    
    <td>
      popular
    </td>
    
    <td>
      The Default Tab Of Widget
    </td>
  </tr>
  
  <tr>
    <td>
      excerpt_length
    </td>
    
    <td>
      100
    </td>
    
    <td>
      The excerpt (short version of comment) length
    </td>
  </tr>
  
  <tr>
    <td>
      hide_avatars
    </td>
    
    <td>
    </td>
    
    <td>
      Set to "1" if you don&#8217;t want to display avatars
    </td>
  </tr>
</table>

You can add custom styles using CSS. The parent element&#8217;s id is "CombComments" and the class is "dsq-widget".An Example of adding custom style :

<pre class="prettyprint"><code>.dsq-widget a{
 color: black;
}</code></pre>

The above code will make the color of all "a" elements "black".

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0100.png