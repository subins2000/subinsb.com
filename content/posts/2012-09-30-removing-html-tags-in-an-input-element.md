---
title: Removing HTML tags in an input element.
author: Subin Siby
type: post
date: 2012-09-30T05:09:00+00:00
excerpt: 'index.html (Removing HTML in INPUT element).&lt;html&gt;&lt;head&gt;&lt;script&gt;var el = document.getElementById("in"); // or just another HTML DOM elementvar text = el.value.replace(/&lt;[^&gt;]+&gt;/g, "");&nbsp;document.getElementById("in").value=...'
url: /removing-html-tags-in-an-input-element
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 9cc3d0c267849f209d9a0ed0a695040a
  - 9cc3d0c267849f209d9a0ed0a695040a
  - 9cc3d0c267849f209d9a0ed0a695040a
  - 9cc3d0c267849f209d9a0ed0a695040a
  - 9cc3d0c267849f209d9a0ed0a695040a
  - 9cc3d0c267849f209d9a0ed0a695040a
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
  - http://sag-3.blogspot.com/2012/09/replace-html.html
categories:
  - HTML
  - JavaScript

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>index.</b><b>html </b>(Removing HTML in INPUT element).</p> 
  
  <blockquote class="tr_bq">
    <p>
      <html><br /><head><br /><script><br />var el = document.getElementById("in"); // or just another HTML DOM element<br />var text = el.value.replace(/<[^>]+>/g, "");<br />&nbsp;document.getElementById("in").value=text;<br /></script><br /></head><br /><body><br /><input id="<span style="color: red;">in</span>" value="<span style="color: red;"><a>Subin</a></span>"><br /></body><br /></html>
    </p>
  </blockquote>
  
  <p>
    If the code above didn&#8217;t worked try placing the script after the <b>input element</b>.<br />This is the script
  </p>
  
  <blockquote class="tr_bq">
    <p>
      &nbsp;<script><br />var el = document.getElementById("<span style="color: red;">in</span>"); // or just another HTML DOM element<br />var text = el.value.replace(/<[^>]+>/g, "");<br />&nbsp;document.getElementById("<span style="color: red;">in</span>").value=text;<br /></script>
    </p>
  </blockquote>
</div>