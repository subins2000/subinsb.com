---
title: The best method to find client’s IP Address in PHP.
author: Subin Siby
type: post
date: 2013-01-05T13:27:00+00:00
excerpt: "This simple function will help you to find the client's IP. You could use $_SERVER['REMOTE_ADDR']&nbsp;, but this method will not returns the actual IP address.To get the real IP address use this method.function getUserIpAddr(){&nbsp; &nbsp; if (!empty..."
url: /the-best-method-to-find-clients-ip-address-in-php
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 9feab478da5c044b6bb3d6ef6aedf5fb
  - 9feab478da5c044b6bb3d6ef6aedf5fb
  - 9feab478da5c044b6bb3d6ef6aedf5fb
  - 9feab478da5c044b6bb3d6ef6aedf5fb
  - 9feab478da5c044b6bb3d6ef6aedf5fb
  - 9feab478da5c044b6bb3d6ef6aedf5fb
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
dsq_thread_id:
  - 1973364083
  - 1973364083
  - 1973364083
  - 1973364083
  - 1973364083
  - 1973364083
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
  - http://sag-3.blogspot.com/2013/01/the-best-method-to-find-clients-ip.html
categories:
  - PHP
tags:
  - IP

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This simple function will help you to find the client&#8217;s IP. You could use <b>$_SERVER[&#8216;REMOTE_ADDR&#8217;]</b>&nbsp;, but this method will not returns the actual IP address.</p> 
  
  <p>
    To get the real IP address use this method.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      function getUserIpAddr(){<br />&nbsp; &nbsp; if (!empty($_SERVER[&#8216;HTTP_CLIENT_IP&#8217;])){<br />&nbsp; &nbsp; &nbsp; &nbsp; return $_SERVER[&#8216;HTTP_CLIENT_IP&#8217;];<br />&nbsp; &nbsp; }else if (!empty($_SERVER[&#8216;HTTP_X_FORWARDED_FOR&#8217;])){<br />&nbsp; &nbsp; &nbsp; &nbsp; $ite=explode(&#8216;,&#8217;,$_SERVER[&#8216;HTTP_X_FORWARDED_FOR&#8217;]); return str_replace(" ",",$ite[0]);<br />&nbsp; &nbsp; }else{<br />&nbsp; &nbsp; &nbsp; &nbsp; return $_SERVER[&#8216;REMOTE_ADDR&#8217;];<br />&nbsp; &nbsp; }<br />}&nbsp;$cip=getUserIpAddr();
    </p>
  </blockquote>
  
  <p>
    The above function will returns the IP address of the client. You could use the variable <b>$cip</b>&nbsp;to get the IP.<br />Example (printing client&#8217;s IP) :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      echo "Your IP Address is ".$cip;
    </p>
  </blockquote>
</div>