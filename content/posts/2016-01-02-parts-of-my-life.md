---
title: Parts Of My Life
author: Subin Siby
type: post
date: 2016-01-02T15:55:45+00:00
url: /parts-of-my-life
categories:
  - Personal

---
A programmer, a teenager, a student, an adventurer &#8211; 4 components in my life.

When you get all of these at once, I feel a little pressure in my life. Let me explain these 4 parts :


## Programmer

I have been a programmer for 5 years. I started when I was **10** years old. I know these languages :

<pre class="prettyprint"><code>PHP, Python, Bash, HTML, CSS, JavaScript</code></pre>

My primary language is PHP. Though developers are going away from **PHP**, I still like it and would continue that way.

## Teenager

I&#8217;m in the 2nd year of teenage life and is on the verge of 3rd year (January 20). It&#8217;s a mystery how someone get through the teenage life.

If you get sad, you get very sad. I had days where I listened to music as much as I can to relieve the pressure.

If you become happy, you become crazy. You see, if I become happy, I become crazier. Sometimes, I get really excited and does some pretty stupid stuff.

The outcomes of these stupid stuff were both devastating and spectacular :

<pre class="prettyprint"><code>Devastating - Doing some really embarrassing things which haunts me
Spectacular - Wrote a complaint to Prime Minister and got an internet connection</code></pre>

Teenage is both crazy and wonderful. Hope it continues like that.

## Student

It is this part that I give my **75%** to. My parents say to give more to it. If I did give more amount to this part, then I couldn&#8217;t do my other parts.

School makes me busy. I&#8217;m the class leader and that makes my responsibilities more. Studies are the other hardest thing.

The "Student" part is what you have to blame for not replying to your comments and not responding to emails.

My parents and family members say :

<pre class="prettyprint"><code>Without a basic qualification, you're nothing
</code></pre>

I think it&#8217;s sad that people look at qualification more than their ability. Is qualification a value of ability. I don&#8217;t think so. Someone must create a better indicator of ability.

## Adventurer

I love to visit new places and meet new people ! It&#8217;s my favorite hobby. I travel using my bicycle.

The region I live :



is a terrain with many hills and depressions. So, it&#8217;s hard to ride a cycle. But despite my parents&#8217; warning, I travel as much as I want. The longest distance I have covered is **10 Kms** or maybe more than that.

And from those places, I got some beautiful images :<figure class="wp-caption aligncenter">

[<img class="" src="//i.imgur.com/1xR1hs3.png" alt="Edakkazhiyur Beach. Travelled by motorbike" width="2560" height="1918" />][5]<figcaption class="wp-caption-text">Edakkazhiyur Beach. Travelled by motorbike</figcaption></figure> 

Another place (Mangad) :<figure class="wp-caption aligncenter">

[<img class="" src="//imgur.com/wqsfmMl.png" alt="The land that separates Kizhoor and Mangad" width="1708" height="399" />][6]<figcaption class="wp-caption-text">The land that separates Kizhoor and Mangad</figcaption></figure> 

My adventures will continue and I have a dream that I can travel around the world one day. Perhaps I can. 🙂

## Conclusion

So, there you go. My life in a post. I&#8217;m writing this to reference when someone asks for doing a job by email or comment.

And ending with a smile :

🙂

 [1]: #programmer
 [2]: #teenager
 [3]: #student
 [4]: #adventurer
 [5]: //i.imgur.com/1xR1hs3.png
 [6]: //imgur.com/wqsfmMl.png
 [7]: #conclusion
