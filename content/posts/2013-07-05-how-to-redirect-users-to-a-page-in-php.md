---
title: How to redirect users to a page in PHP
author: Subin Siby
type: post
date: 2013-07-05T03:30:00+00:00
excerpt: 'If you want to redirect users to a specific page or url like when user is not logged in or something in PHP&nbsp;then you can accomplish this with header&nbsp;function or by Javascript&nbsp;printed by&nbsp;echo function.The following example will redir...'
url: /how-to-redirect-users-to-a-page-in-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
  - http://sag-3.blogspot.com/2013/07/how-to-redirect-users-to-page-in-php.html
syndication_item_hash:
  - a31047052fff2076700e45c51ba6d041
  - a31047052fff2076700e45c51ba6d041
  - a31047052fff2076700e45c51ba6d041
  - a31047052fff2076700e45c51ba6d041
  - a31047052fff2076700e45c51ba6d041
  - a31047052fff2076700e45c51ba6d041
categories:
  - JavaScript
  - PHP
tags:
  - echo
  - header
  - Location
  - Page

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you want to redirect users to a specific page or url like when user is not logged in or something in <b>PHP&nbsp;</b>then you can accomplish this with <b>header</b>&nbsp;function or by <b>Javascript</b>&nbsp;printed by&nbsp;<b>echo </b>function.<br />The following example will redirect user to <b>http://subins.hp.af.cm</b></p> 
  
  <blockquote class="tr_bq">
    <p>
      <?php header("Location: http://subins.hp.af.cm"); ?>
    </p>
  </blockquote>
  
  <p>
    Or you can use the normal <b>Javascript </b>:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <?php echo "<script>window.location=&#8217;http://subins.hp.af.cm&#8217;;</script>"; ?>
    </p>
  </blockquote>
  
  <p>
    <span style="font-size: large;"><b>What&#8217;s the difference ?</b></span><br /><b><br /></b><b>header</b>&nbsp;function is called in server itself not when the page reaches the browser. So no content will load, instead the client&#8217;s page will be redirected to the specified url.
  </p>
  
  <p>
    <b>Javascript</b>&nbsp;redirection takes place in browser. So when the page loads to the <b>Javascript</b>&nbsp;redirect call the user will be redirected. It takes some time if you put the redirection code below<b>&nbsp;</b>script loading or stylesheet loading.
  </p>
  
  <p>
    So it&#8217;s <b>better to use header</b>&nbsp;than calling a redirection in&nbsp;<b>Javascript</b>. If <b>header </b>function won&#8217;t work then you can use <b>Javascript</b>&nbsp;redirection. It&#8217;s that simple.</div>