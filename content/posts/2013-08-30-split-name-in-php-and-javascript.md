---
title: Split Name in PHP and Javascript
author: Subin Siby
type: post
date: 2013-08-30T13:41:00+00:00
excerpt: "If you have a user with a long name and want to short it because it won't fit into the element that contains the name, you can either do it on the server or the client. In this post I am going to tell you how to accomplish this with PHP&nbsp;and JavaSc..."
url: /split-name-in-php-and-javascript
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
  - http://sag-3.blogspot.com/2013/08/split-name-in-php-javascript.html
syndication_item_hash:
  - f92f601a586448ac4c5e4d9b318bd560
  - f92f601a586448ac4c5e4d9b318bd560
  - f92f601a586448ac4c5e4d9b318bd560
  - f92f601a586448ac4c5e4d9b318bd560
  - f92f601a586448ac4c5e4d9b318bd560
  - f92f601a586448ac4c5e4d9b318bd560
dsq_thread_id:
  - 1971837790
  - 1971837790
  - 1971837790
  - 1971837790
  - 1971837790
  - 1971837790
categories:
  - JavaScript
  - PHP
tags:
  - echo
  - Name

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you have a user with a long name and want to short it because it won&#8217;t fit into the element that contains the name, you can either do it on the server or the client. In this post I am going to tell you how to accomplish this with <b>PHP</b>&nbsp;and <b>JavaScript</b>.<br />Suppose We have a user named&nbsp;<b>Paul Steve Panakkal</b>&nbsp;(my cousin). To separate the first name, middle name and last name you can do the following in <b>PHP</b>&nbsp;:</p> 
  
  <blockquote class="tr_bq">
    <p>
      <?<br />$name="Paul Steve Panakkal";<br />$parts=explode(&#8216; &#8216;,$name);<br />$first_name=$parts[0];<br />$middle_name=$parts[1];<br />$last_name=$parts[2];<br />echo $first_name."<br/>";<br />echo $middle_name."<br/>";<br />echo $last_name;<br />?>
    </p>
  </blockquote>
  
  <p>
    The above code would print out :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      Paul<br />Steve<br />Panakkal
    </p>
  </blockquote>
  
  <p>
    There you go! Now Let&#8217;s do this with <b>JavaScript</b>&nbsp;:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <script><br />var name="Paul Steve Panakkal";<br />var parts=name.split(" ");<br />first_name=parts[0];<br />middle_name=parts[1];<br />last_name=parts[2];<br />document.write(first_name+"<br>"+middle_name+"<br>"+last_name);<br /></script>
    </p>
  </blockquote>
  
  <p>
    The above code will also print out :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      Paul<br />Steve<br />Panakkal
    </p>
  </blockquote>
  
  <p>
    Now you know how to split up names in to first name, middle name and last name in <b>PHP</b>&nbsp;and <b>JavaScript</b>.<br />If you have any suggestions/feedback/problems just <b>echo</b>&nbsp;it out in the comments below.</div>