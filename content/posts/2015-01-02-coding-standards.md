---
title: Coding Standards
author: Subin Siby
type: post
date: 2015-01-02T07:16:36+00:00
url: /coding-standards
categories:
  - PHP
  - Program

---
Writing programs should be meaningful and in a long future, if you or anyone looks back at the code, then he/she must understand it. We all write codes, but do we have a standard of writing the code ? If we write codes without any standards and just go for it, then the program would be messy and no other programmer would like to read it.

Here&#8217;s what I&#8217;m talking about : A guy writes code as he like :

<pre class="prettyprint"><code>&lt;?
//A function to forget. Just a demo function for showing code standards. Nothing is Nothing.
function forget ($get=false) {
echo $get===true ? "Yes":"No";
}
?&gt;</code></pre>

The above code is messy and doesn&#8217;t look good and here&#8217;s how it is made better :

<pre class="prettyprint"><code>&lt;?php
/**
 * A function to forget. Just a demo function for
 * showing code standards. Nothing is Nothing.
 */
function forget($get = false) {
  echo $get === true ? "Yes" : "No";
}
?&gt;</code></pre>

You can see that, now the code looks better and gives a temptation to read it. Not only does it help you, but others who might read the code in the future as well.

This post is about the coding standards that I use and maybe you could use it too ! This coding standards is loosely based on the <a href="http://pear.php.net/manual/en/standards.php" target="_blank">PEAR coding standards</a>.


## The PHP File

On most PHP systems, **<?php** works perfectly, but on others the short code is not enabled. In this case, **<?** won&#8217;t work. So, starting of **PHP** code should be by **<?php** :

<pre class="prettyprint"><code>&lt;?php
echo "Hello World";
?&gt;
</code></pre>

If the file is full **PHP**, then it need not be closed by **?>**. All files should end with a newline (\n) and not Windows newline (\r\n).

Semicolons is important. It should be placed. Do this :

<pre class="prettyprint"><code>&lt;?php echo "hello"; ?&gt;</code></pre>

and not this :

<pre class="prettyprint"><code>&lt;?php echo "hello" ?&gt;</code></pre>

## Indents & Spacing

The indents should be of **2 spaces** and no tabs. Tabs won&#8217;t work equally perfect in all text editors. Hence, instead of tabs, whitespaces should be used.

Lines should not have trailing whitespaces at the end. A space should be in between operators such as :

<pre class="prettyprint"><code>+, =, -, !=, !==, &gt;</code></pre>

etc&#8230; Examples :

<pre class="prettyprint"><code>$myVariable = "myContent";
if(5 &lt; 4) { }</code></pre>

rather than :

<pre class="prettyprint"><code>$myVariable="myContent";
if(5&lt;4){}
</code></pre>

Also, a space after "," should be made.

## Control Structures

Control structures include if, for, while, switch, etc&#8230; Here is an example of **if** statement :

<pre class="prettyprint"><code>if (5 &gt; 4 || 9 &gt; 8) {
  doAction1();
}elseif ("a" == "a") {
  doAction2();
}else{
  doAction3();
}</code></pre>

Always use **elseif** and not **else if**. For **siwtch** statements :

<pre class="prettyprint"><code>switch (condition) {
  case 1:
    action1;
    break;

  case 2:
    action2;
    break;

  default:
    defaultaction;
}</code></pre>

## Lines

All lines should not exceed 80 characters. If it does, make the overflowing chars into next line :

<pre class="prettyprint"><code>if($array['this']['that']['here']['pen'] ==
$array['there']['that']['myBigHat']['insideAPen']) {
  doAction1();
}</code></pre>

This applies to comments too :

<pre class="prettyprint"><code>/**
 * A function to forget. Just a demo function for
 * showing code standards. Nothing is Nothing.
 */</code></pre>

There might be a need to extend from 80 characters if it is a control structure. In this case, we can on one condition : it must be understandable.

Another thing is that all lines that are kept out for prettiness mustn&#8217;t have any characters, even whitespaces.

## Variables & Functions

As said before, a space should be in between &#8216;=&#8217; in variables :

<pre class="prettyprint"><code>$var = "content";
$var .= "content";</code></pre>

In case of variables that are together of different sizes, adding more space will help to read it better :

<pre class="prettyprint"><code>$name                 = "";
$fullName             = "";
$aLitttleLongVariable = ""; </code></pre>

In the case of functions :

<pre class="prettyprint"><code>myFunction($var1, $var2);</code></pre>

Functions should return meaningful data. It&#8217;s better to avoid **NULL** as return and instead use **boolean false**. Use :

<pre class="prettyprint"><code>function myFunction() { return true; }</code></pre>

and not :

<pre class="prettyprint"><code>function myFunction() { return "true"; }</code></pre>

or :

<pre class="prettyprint"><code>function myFunction() { return TRUE; }</code></pre>

## Arrays

The items of arrays should be separated with whitespace and inbetween "=>" :

<pre class="prettyprint"><code>$myArray = array('hello', 'world', 'me' =&gt; 'him');</code></pre>

It can be also made more pretty by :

<pre class="prettyprint"><code>$myArray = array(
 'hello' =&gt; 'a',
 'world' =&gt; 'b',
 'me' =&gt; 'him'
);</code></pre>

## Quotes

It&#8217;s better to use double quotes (") in **PHP**, but both single quote and double quote have good uses.

## Comments

Comments should start by "/*\*" and end with "\*/". Each line of comment must start with "*" too :

<pre class="prettyprint"><code>/**
 * Reads configuration & set Software according to it
 * All lines must start with *
 */</code></pre>

This also have the **80 chars** limit.

## Naming

All function names and variable names must be in lowercase and words should be separated by underscore (_) or you can do the caps way :

<pre class="prettyprint"><code>myFunction()
my_function() </code></pre>

But, your project must only use one of the above methods.

## Conclusion

It&#8217;s better if all your projects follow the same coding standards as it will help you and other who read the code as well. This coding standards are my opinion only. You can choose your own and write it down in a document or on your mind and follow it forever in your coding life.

 [1]: #the-php-file
 [2]: #indents-spacing
 [3]: #control-structures
 [4]: #lines
 [5]: #variables-functions
 [6]: #arrays
 [7]: #quotes
 [8]: #comments
 [9]: #naming
 [10]: #conclusion
