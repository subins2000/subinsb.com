---
title: Get extension of a file using PHP
author: Subin Siby
type: post
date: 2012-09-04T14:59:00+00:00
excerpt: |
  This function will allow you to get a file's extension in PHP.function getExtension($str) {         $i = strrpos($str,".");         if (!$i) { return ""; }&nbsp;         $l = strlen($str) - $i;         $ext = substr($str,$i+1,$l);         return $ext; ...
url: /get-extension-of-a-file-using-php
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - c09b94b4e6b979f62fec5f7bcccd7a54
  - c09b94b4e6b979f62fec5f7bcccd7a54
  - c09b94b4e6b979f62fec5f7bcccd7a54
  - c09b94b4e6b979f62fec5f7bcccd7a54
  - c09b94b4e6b979f62fec5f7bcccd7a54
  - c09b94b4e6b979f62fec5f7bcccd7a54
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
  - http://sag-3.blogspot.com/2012/09/get-extension-of-file-using-php.html
categories:
  - PHP
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;">
  This function will allow you to get a <b>file&#8217;s </b>extension in <b>PHP</b>.
</div>

<div dir="ltr" style="text-align: left;">
  <pre class="prettyprint"><code>function getExtension($fileName){
   $i = strrpos($fileName, ".");
   if (!$i) {
      return "";
   }
   $length = strlen($str) - $i;
   $extens = substr($str, $i+1, $length);
   return $extens;
}</code></pre>
  
  <p>
    For example you need to get a extension of the file <b>photo.jpg</b>. To get the extension type this code in your <b>PHP</b> file.
  </p>
  
  <pre class="prettyprint"><code>&lt;span style="color: #660000;">&lt;?php&lt;/span> &lt;span style="color: #660000;">echo&lt;/span> getExtension&lt;span style="color: #660000;">(&lt;/span>'&lt;b>&lt;span style="color: #e06666;">photo.jpg&lt;/span>&lt;/b>'&lt;span style="color: #660000;">);&lt;/span> &lt;span style="color: #660000;">?&gt;&lt;/span></code></pre>
  
  <p>
    The above code will print out "jpg".
  </p>
</div>