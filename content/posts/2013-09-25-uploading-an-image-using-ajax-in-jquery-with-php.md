---
title: Uploading An Image Using AJAX In jQuery With PHP
author: Subin Siby
type: post
date: 2013-09-25T17:25:00+00:00
excerpt: "As you know AJAX&nbsp;is the method of sending data without refreshing the page. Now I'm going to tell you how to upload images using AJAX&nbsp;in jQuery. For This You need to Download the jquery.form&nbsp;plugin from here. The Minimum requirement for ..."
url: /uploading-an-image-using-ajax-in-jquery-with-php
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
  - http://sag-3.blogspot.com/2013/09/uploading-image-using-ajax-in-jquery.html
syndication_item_hash:
  - f5bbc4c345647936e61e41cc67f3a181
  - f5bbc4c345647936e61e41cc67f3a181
  - f5bbc4c345647936e61e41cc67f3a181
  - f5bbc4c345647936e61e41cc67f3a181
  - f5bbc4c345647936e61e41cc67f3a181
  - f5bbc4c345647936e61e41cc67f3a181
categories:
  - AJAX
  - JavaScript
  - jQuery
  - PHP
tags:
  - File
  - Form
  - Image
  - Upload

---
<div dir="ltr" style="text-align: left;">
  As you know <b>AJAX</b> is the method of sending data without refreshing the page. Now I&#8217;m going to tell you how to upload images using AJAX in <b>jQuery</b>. For This You need to Download the <b>jquery.form</b> plugin from <b><a href="http://malsup.com/jquery/form/#download">here</a></b>. The Minimum requirement for this plugin is jQuery version <b>1.5</b> or later.</p> 
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=x2gngcvpjdurrlz&class=3">Download</a> <a class="demo" href="http://demos.subinsb.com/ajax-image-upload-jquery">Demo</a>
  </div>
  
  <p>
    A Sample of uploading image using <b>AJAX</b> in <b>jQuery</b> is shown below :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-2zQRbtHjad4/UkMbiwcJEdI/AAAAAAAADCA/mbn3sNJZ5DE/s1600/0046.png"><img alt="" src="//1.bp.blogspot.com/-2zQRbtHjad4/UkMbiwcJEdI/AAAAAAAADCA/mbn3sNJZ5DE/s1600/0046.png" border="0" /></a>
  </div>
  
  <div>
    <div class="separator" style="clear: both; text-align: center;">
    </div>
    
    <div>
      Now Let&#8217;s start on the code. Here is the <b>HTML</b> page :
    </div>
    
    <div>
      <pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
 &lt;head&gt;
  &lt;script src="//code.jquery.com/jquery-latest.min.js"&gt;&lt;/script&gt;
  &lt;script src="http://malsup.github.io/jquery.form.js"&gt;&lt;/script&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;form action="upload.php" method="POST" id="uploadform"&gt;
   &lt;input type="file" name="file"/&gt;
   &lt;input type="submit" value="Upload"/&gt;&lt;br/&gt;&lt;br/&gt;
   Message :
   &lt;div id="onsuccessmsg" style="border:5px solid #CCC;padding:15px;"&gt;&lt;/div&gt;
  &lt;/form&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>
    </div>
  </div>
</div>

<div>
  We don&#8217;t have to mention the <b>enctype</b> or <b>encoding</b> attribute on the form because the <b>jQuery.form</b> will take care of it.
</div>

<div>
  Add the following <b>JavaScript</b> code just before <b></body></b> :
</div>

<pre class="prettyprint"><code>&lt;script&gt;
$(document).ready(function(){
 function onsuccess(response,status){
  $("#onsuccessmsg").html("Status :&lt;b&gt;"+status+'&lt;/b&gt;&lt;br&gt;&lt;br&gt;Response Data :&lt;div id="msg" style="border:5px solid #CCC;padding:15px;"&gt;'+response+'&lt;/div&gt;');
 }
 $("#uploadform").on('submit',function(){
  var options={
   url     : $(this).attr("action"),
   success : onsuccess
  };
  $(this).ajaxSubmit(options);
 return false;
 });
});
&lt;/script&gt;</code></pre>

<div>
  Here is the upload form&#8217;s details :
</div>

<pre class="prettyprint"><code>ID        : uploadform
Action    : &lt;b>upload.php
&lt;/b>Method    : &lt;b>POST&lt;/b></code></pre>

<div>
  This is the <b>upload.php</b> file to which we send the form data :
</div>

<div>
  <pre class="prettyprint"><code>&lt;?
function getExtension($str) {$i=strrpos($str,".");if(!$i){return"";}$l=strlen($str)-$i;$ext=substr($str,$i+1,$l);return $ext;}
$formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST"){
 $name = $_FILES['file']['name'];
 $size = $_FILES['file']['size'];
 $tmp  = $_FILES['file']['tmp_name'];
 if(strlen($name)){
  $ext = getExtension($name);
  if(in_array($ext,$formats)){
   if($size&lt;(1024*1024)){
    $imgn = time().".".$ext;
    if(move_uploaded_file($tmp, "./uploads/".$imgn)){
     echo "File Name : ".$_FILES['file']['name'];
     echo "&lt;br/&gt;File Temporary Location : ".$_FILES['file']['tmp_name'];
     echo "&lt;br/&gt;File Size : ".$_FILES['file']['size'];
     echo "&lt;br/&gt;File Type : ".$_FILES['file']['type'];
     echo "&lt;br/&gt;Image : &lt;img style='margin-left:10px;' src='uploads/".$imgn."'&gt;";
    }else{
     echo "Uploading Failed.";
    }
   }else{
    echo "Image File Size Max 1 MB";
   }
  }else{
   echo "Invalid Image file format.";
  }
 }else{
  echo "Please select an image.";
  exit;
 }
}
?&gt;</code></pre>
</div>

<div>
  The above code only accepts a file if it met some conditions such as :
</div>

<div>
  <ol style="text-align: left;">
    <li>
      A File Should Be Uploaded
    </li>
    <li>
      Should be an Image Extension
    </li>
    <li>
      File Size Must Not Exceed 1 MB
    </li>
  </ol>
</div>

<div>
  If you have any suggestions/problems/feedback say it out in the comments, I will help you.
</div>