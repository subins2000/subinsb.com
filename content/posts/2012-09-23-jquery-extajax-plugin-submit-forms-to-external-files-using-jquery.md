---
title: 'Jquery ExtAjax Plugin : Submit forms to external files using Jquery'
author: Subin Siby
type: post
date: 2012-09-23T15:20:00+00:00
excerpt: "Hi. You can't submit a form to external file using Jquery Ajax. Finally I created a Jquery Plugin&nbsp;to solve this problem. I named it ExtAjax.This&nbsp;Jquery Plugin&nbsp;will help you to submit a form to a external file without refreshing the page...."
url: /jquery-extajax-plugin-submit-forms-to-external-files-using-jquery
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - cd8e2678a3bbd2b97bf3f8040b3b3b19
  - cd8e2678a3bbd2b97bf3f8040b3b3b19
  - cd8e2678a3bbd2b97bf3f8040b3b3b19
  - cd8e2678a3bbd2b97bf3f8040b3b3b19
  - cd8e2678a3bbd2b97bf3f8040b3b3b19
  - cd8e2678a3bbd2b97bf3f8040b3b3b19
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
  - http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html
categories:
  - jQuery
tags:
  - Plugin

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Hi. You can&#8217;t submit a form to external file using <b>Jquery Ajax. </b>Finally I created a <b>Jquery Plugin</b>&nbsp;to solve this problem. I named it <b>ExtAjax</b>.<br />This&nbsp;<b>Jquery Plugin</b>&nbsp;will help you to submit a form to a external file without refreshing the page.</p> 
  
  <h2 style="text-align: left;">
    <b><u>Requirements</u></b>
  </h2>
  
  <div>
    <b>1) Jquery Latest Version</b>
  </div>
  
  <div>
    <b>2) extajax.js file</b>
  </div>
  
  <div>
    <b><br /></b>
  </div>
  
  <p>
    Here&#8217;s how the&nbsp;<b>HTML&nbsp;</b>file will look like:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <html><br /><head><br /><script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script><br /><script type="text/javascript" src="extajax.js"></script><br /></head><br /><body><br /><script><br />$(document).ready(function(){<br />$("<span style="color: red;">#form</span>").submit(function(){<br />event.preventDefault();<br />$("<span style="color: red;">#form</span>").extajax();<br />});<br />});<br /></script><br /><form id="<span style="color: red;">form</span>" action="<span style="color: red;">http://yahoo.com/search</span>" method="<span style="color: red;">get</span>"><br /><input name=&#8217;q&#8217; value="Google"><input type="submit"><br /></form><br /></body><br /></html>
    </p>
  </blockquote>
  
  <p>
    Here is the Jquery code added in the file above.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <script><br />$(document).ready(function(){<br />$("<span style="color: red;">#form</span>").submit(function(){<br />event.preventDefault();<br />$("<span style="color: red;">#form</span>").extajax();<br />});<br />});<br /></script>
    </p>
  </blockquote>
  
  <p>
    Replace the red colored codes above with the id or class of your form eg: <b>#form2 </b>, .<b>myform</b><br />The form :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <form id="<span style="color: #0b5394;">form</span>" action="<span style="color: #38761d;">http://yahoo.com/search</span>" method="<span style="color: red;">get</span>"><br /><input name=&#8217;q&#8217; value="Google"><input type="submit"><br /></form>
    </p>
  </blockquote>
  
  <p>
    Replace the <span style="color: #0b5394;">blue</span> colored line with your form id. It should be same on the script too.<br />Replace the <span style="color: red;">red</span> colored line with the type of method of the post (<b>post </b>or <b>get</b>).<br />Replace the <span style="color: #38761d;">green</span> colored line with the place you want to submit the form to.
  </p>
  
  <h2 style="text-align: left;">
    Download
  </h2>
  
  <div style="text-align: center;">
    <embed allowfullscreen="true" allowscriptaccess="always" height="400" src="https://www.box.com/embed/y1ytji2akasouer.swf" type="application/x-shockwave-flash" width="466" wmode="opaque">
    </embed>
  </div>
</div>