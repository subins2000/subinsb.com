---
title: 'Send E-Mails Via SMTP Server In PHP – GMail & Outlook'
author: Subin Siby
type: post
date: 2014-01-28T16:51:53+00:00
url: /send-mails-via-smtp-server-gmail-outlook-php
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - Email
  - GMAIL
  - Outlook
  - SMTP

---
Email Service providers have POP and SMTP servers for incoming and outgoing E-Mails. SMTP is for Outgoing (Sending) emails and POP is for Incoming (Receiving) emails. If you have an account in email services, it means that you can send or receive emails using these servers. This service is free, fast and secure. You can change the "From" address and the "From" name if you send emails using SMTP server. In this post I will tell you how to send emails using **SMTP** servers of **GMail** & **Outlook** in **PHP**.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=n7t4ppks7xyn/s/f77onz2xf8ruzonhqc9y&class=18" target="_blank">Download</a><a class="demo" href="http://open.subinsb.com/invite" target="_blank">Demo</a>
</div>


# PHPMailer

We are going to use <a href="https://github.com/PHPMailer/PHPMailer" target="_blank">PHPMailer</a> class for sending emails. It&#8217;s an easy usable PHP library for sending emails. This class is included in the Download File.

# Configuration

This configuration contains account information, Recipient address with name and the message details :

<pre class="prettyprint"><code>$account="email_address@gmail.com";
$password="accountpassword";
$to="mail@subinsb.com";
$from="vishal@vishal.com";
$from_name="Vishal G.V";
$msg="&lt;strong&gt;This is a bold text.&lt;/strong&gt;"; // HTML message
$subject="HTML message";</code></pre>

# GMail

Include PHPMailer class first :

<pre class="prettyprint"><code>include("phpmailer/class.phpmailer.php");</code></pre>

Create the Class object and append the settings of the SMTP server :

<pre class="prettyprint"><code>$mail = new PHPMailer();
$mail-&gt;IsSMTP();
$mail-&gt;CharSet = 'UTF-8';
$mail-&gt;Host = "smtp.gmail.com";
$mail-&gt;SMTPAuth= true;
$mail-&gt;Port = 465; // Or 587
$mail-&gt;Username= $account;
$mail-&gt;Password= $password;
$mail-&gt;SMTPSecure = 'ssl';
$mail-&gt;From = $from;
$mail-&gt;FromName= $from_name;
$mail-&gt;isHTML(true);
$mail-&gt;Subject = $subject;
$mail-&gt;Body = $msg;
$mail-&gt;addAddress($to);</code></pre>

Then, check if the mail sends without error. If there is an error, it will print out the error :

<pre class="prettyprint"><code>if(!$mail-&gt;send()){
 echo "Mailer Error: " . $mail-&gt;ErrorInfo;
}else{
 echo "E-Mail has been sent";
}</code></pre>

This concludes the GMail part.

# Outlook

Include PHPMailer class first :

<pre class="prettyprint"><code>include("phpmailer/class.phpmailer.php");</code></pre>

Create the Class object and append the settings of the SMTP server :

<pre class="prettyprint"><code>$mail = new PHPMailer();
$mail-&gt;IsSMTP();
$mail-&gt;CharSet = 'UTF-8';
$mail-&gt;Host = "smtp.live.com";
$mail-&gt;SMTPAuth= true;
$mail-&gt;Port = 587;
$mail-&gt;Username= $account;
$mail-&gt;Password= $password;
$mail-&gt;SMTPSecure = 'tls';
$mail-&gt;From = $from;
$mail-&gt;FromName= $from_name;
$mail-&gt;isHTML(true);
$mail-&gt;Subject = $subject;
$mail-&gt;Body = $msg;
$mail-&gt;addAddress($to);</code></pre>

Then, check if the mail sends without any errors. If there is an error, it will print out the error :

<pre class="prettyprint"><code>if(!$mail-&gt;send()){
 echo "Mailer Error: " . $mail-&gt;ErrorInfo;
}else{
 echo "E-Mail has been sent";
}</code></pre>

If there was an error printed out and you need help, please comment.

 [1]: #phpmailer
 [2]: #configuration
 [3]: #gmail
 [4]: #outlook
