---
title: Newton Color Disc in jQuery
author: Subin Siby
type: post
date: 2014-11-11T03:30:05+00:00
url: /newton-color-disc
categories:
  - CSS
  - HTML
  - JavaScript
  - jQuery
tags:
  - Physics
  - Plugin
  - Tutorials

---
Newton Color Disc is used to find the color got by the combination of multiple colors. The color is not actually formed, but the eye identifies the combination as another color due to persistence of vision.

Normally, Newton’s Color disk is made by marking sectors filled with color on a circular shaped disc. When this disc is rotated at high speed, we see the circle as another color. This principle led to the method of creating different colors by the combination of some primary colors at different percentage compositions.

We use this principle in the web too. The **RGB** combination is used in web pages to make colors too. If the colors **Red**, **Green** and **Blue** is mixed together, we get **White** color.

I have decided to convert the **Newton Color Disc** in to the digital state which led to the creation of the **Newton Color Disc jQuery** plugin. With this plugin, a Newton Color Disc can be made with combinations of any colour !

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=s/7u8ll589fs48w6czs206&class=33" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/jquery/color-disc" target="_blank">Demo</a>
</div>

## Components

It mainly use **jQuery** and for creating the circle with sectors, the plugin uses **[Raphaël JS][1] **library. It is a great library for creating **SVG** images by **JavaScript** itself.

For animation, we completely use **CSS**. The new **CSS 3.0** specially helps to rotate the disc by animation.

## How It Works

When the plugin function is called on an element, then a **div** element containing a **SVG** image is appended to the element from which it was called.

The **SVG** image is of a circle with sectors filled with colors that was given to the plugin. Then, the disc is rotated by the calling of another functions. What this function does is that starts the animation using **CSS** after setting the speed using **animation-speed** property.

As the value of **animation-speed** property **decreases**, the speed of rotation **increases**. So, it’s inversely proportional.

## Usage

<pre class="prettyprint"><code>$("#holder").colorDisc()</code></pre>

Adds the color disc into **#holder** element. But, does not start rotating.

<pre class="prettyprint"><code>$("#holder").colorDisc("speed", "0.0001")</code></pre>

Sets the speed of the disc to "0.0001". As the value of the speed decreases, the rotation of disc in creases. So, the rotation of disc with the speed value "1" will be very slower than that of the disc which have value of "0.01".

The speed of the disc can be changes anytime even when it’s rotating. But, the disc will restart rotation from the beginning.

<pre class="prettyprint"><code>$("#holder").colorDisc("remove")</code></pre>

Stops the animation and removes the color disc inside the **#holder** element.

That’s all the usages which I think is enough to manage a color disc. Note that the color disc will work greater on computers which have a good **Graphics Processing Unit &#8211; GPU**.

 [1]: http://raphaeljs.com/