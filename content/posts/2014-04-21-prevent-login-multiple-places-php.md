---
title: Prevent Login From Multiple Places In PHP
author: Subin Siby
type: post
date: 2014-04-21T03:45:10+00:00
draft: true
url: /?p=2758
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Uncategorized
tags:
  - Uncategorized

---
This tutorial was a request from <span style="color: #333333;">Stole Popovski.</span>

If your **PHP** site have a login system, you should make it perfect. This is one of the tutorials that make a login system perfect. Some sits allows logins from multiple browsers. But, there&#8217;s a security risk in this. Here are some problems with allowing logins at multiple places :

  * If a cracker who knows the password of an account and used it to login the same time the original user logs in, it can cause trouble.
  * If user forgot to log out in a public computer, other people who uses the same computer can access the users&#8217; account

The easiest way to accomplish this is to create a hash using **sha256** or **sha512** that is stored as a cookie in the browser and in the Database. On each visit to the page by the user, we check if the hash in the session matches that of in the database. If not logging in won&#8217;t be possible.

The hash in the database is created when the user is logged in and is destroyed from the database when the user logs out. When the hash is not in the database and the user logs in with the correct username and password, a new hash is created as cookie and is inserted in to the database.

For this tutorial, we use **PDO** for **SQL** queries, you can see more about it <a href="http://in2.php.net/manual/en/book.pdo.php" target="_blank">here</a>.

## Code

First, we create a table for storing the hashes :

<pre class="prettyprint"><code>CREATE TABLE IF NOT EXISTS `userHash` (
 `user` varchar(20) NOT NULL,
 `hash` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;</code></pre>

The character length of the **hash** column is 512 because we encrypt using **sha512** and the length of the **user** column is set to **20**. You can change it according to your user system.

On your login file, add this code after you set the user id cookie or session variable :

<pre class="prettyprint"><code>$unHash=hash("sha512", rand(0, 1024));
$sql=$dbh-&gt;prepare("SELECT 1 FROM userHash WHERE `user`=:uid");
$sql-&gt;bindValue(":uid", $theuserid);
$sql-&gt;execute();
if($sql-&gt;rowCount()==0){
 $sql=$dbh-&gt;prepare("INSERT INTO userHash VALUES (:uid, :hash)");
 $sql-&gt;bindValue(":uid", $theuserid);
 $sql-&gt;bindValue(":hash", $unHash);
 $sql-&gt;execute();
}else{
 $sql=$dbh-&gt;prepare("UPDATE userHash SET `user`=:uid, `hash`=:hash)");
 $sql-&gt;bindValue(":uid", $theuserid);
 $sql-&gt;bindValue(":hash", $unHash);
 $sql-&gt;execute();
}
setcookie("sMingHash", $unHash, time()+3600*99*500);</code></pre>

&nbsp;

Replace the **$theuserid** variable with the user id containing variable of your login system. We encode the randomly generated number inside the range **0 &#8211; 1024**.

&nbsp;

Now, we create a file with code that checks if the hash in cookie is in the database. Let&#8217;s call the file **check.php** :

<?

if(isset($_COOKIE[&#8216;sMingHash&#8217;])){

$sql=$dbh->prepare("SELECT 1 FROM userHash WHERE \`user\`=:uid");
  
$sql->bindValue(":uid", $thecuruserid);
  
$sql->execute();

header("Location: login.php");

exit;

}else{

header("Location: login.php");

exit;

}

?>