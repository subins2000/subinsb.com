---
title: Greed – A Short Film
author: Subin Siby
type: post
date: 2016-05-20T15:18:10+00:00
url: /greed-a-short-film
categories:
  - Personal
tags:
  - movie

---
Besides coding, my other passion is film making. I have already made some films since the age of 8. But those were raw footage and I didn&#8217;t edit them.

When I was 11, I created an another film and edited it with Windows Movie Maker. I didn&#8217;t publish it and it still exist in my computer.

Now, after 5 years I decided to create another film. This time with the help of my friends. I got the idea when I was studying at 5 AM in the morning of Mathematics exam.

I&#8217;m glad to tell you that I have completed the short film and published it. Check it out :



## About Film

This is a film created by four friends. Here, in Kerala this summer was the most hottest reaching up to 42 degree celsius in some places.

Global Warming is real and we are experiencing it. We complain about it, but doesn&#8217;t do anything. The selfishness of humans are overcoming our kind hearts.

Together, we must throw over our greediness and work together to make our world habitable for the future generations. By not doing so, what is the need of us students studying and people working if there is no future ?

## How It Started

I got the story idea through a daydream while I was studying for Maths. Maths rocks ! At the start of the vacation, the first thing I did was create the script.

You know that Yes/No feeling in your gut when you try to do something ? I have experienced the same feeling when I tried to get out and do this.

At first, when I tried to meet up with my friends Vikas & Rejohn, it failed as Rejohn is too busy with his stuff. But with constant tries, I made a little meeting between us and decided what to do. But, further proceedings failed.

Then the summer classes began and at there my other friends asked me about the film. I replied it **didn&#8217;t work out**. Just after two days, the school was closed due to **severe heat**. This was a big coincidence, because the film we&#8217;re about to do was also about heat.

That night, Vikas called me asking whether I was going to do the film. Now, I knew that he had interest in doing it. This renewed my confidence in making the film. I asked my brother whether he could act and he said yes in a sudden. But, shooting with him was so tiring. He is a stubborn, arrogant person. I think this probably because he&#8217;s my brother.

The 6 minute film was shot in 4 days. The time difference between the shots blend really well.

## Final Thoughts

I&#8217;m glad that we could finish what we started. **This vacation was spent really well**.

> Now, I have a memory that I can look back after 20 years and say "I spent my teenage well"
> 
> If you are planning on doing something, do it. Gain confidence, you can do it !

Just do it&#8230;