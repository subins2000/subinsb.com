---
title: Can’t Boot Ubuntu after installing Nvidia Driver.
author: Subin Siby
type: post
date: 2012-06-29T15:24:00+00:00
excerpt: "NOTE - Go to this post&nbsp;to fix this problem. If that post didn't helped you the do the steps in this post.A lot of people has trouble booting in to Ubuntu after installation of Nvidia Driver.Finally I had found a solution to this problem.On the boo..."
url: /cant-boot-ubuntu-after-installing-nvidia-driver
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 2ed217873fc21d5ddbf6b8c1d35fe42f
  - 2ed217873fc21d5ddbf6b8c1d35fe42f
  - 2ed217873fc21d5ddbf6b8c1d35fe42f
  - 2ed217873fc21d5ddbf6b8c1d35fe42f
  - 2ed217873fc21d5ddbf6b8c1d35fe42f
  - 2ed217873fc21d5ddbf6b8c1d35fe42f
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
  - http://sag-3.blogspot.com/2012/06/cant-boot-ubuntu-after-installing.html
categories:
  - Ubuntu
tags:
  - Nvidia
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b><span style="color: red;">NOTE &#8211; Go to this <a href="http://sag-3.blogspot.in/2012/09/nvidia-solution-2.html" >post</a>&nbsp;to fix this problem. If that post didn&#8217;t helped you the do the steps in this post.</span></b></p> 
  
  <p>
    A lot of people has trouble booting in to Ubuntu after installation of Nvidia Driver.<br />Finally I had found a solution to this problem.
  </p>
  
  <p>
    On the boot menu Click<br /><b>Ubuntu Recovery Mode</b><br /><b><br /></b>You will see a <b>&nbsp;Blue Screen </b>. On the options scroll down and click on <b>failsafex</b><br />You will now see a small dialog box. Click on <b>OK</b> Button<b>.&nbsp;</b><br />Press OK two or three times more.<br />You will now boot into your Ubuntu OS.<br />Connect to the internet.<br />Open <b>Hardware Drivers </b>and install(activate) a recommended Nvidia Driver.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-4Gj8-xj5kZk/T-3IO4QGPaI/AAAAAAAAAyk/pmcXNn4KHro/s1600/hd.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-4Gj8-xj5kZk/T-3IO4QGPaI/AAAAAAAAAyk/pmcXNn4KHro/s1600/hd.png" /></a>
  </div>
  
  <p>
    <b>Reboot </b>and boot into Normal Ubuntu and your Ubuntu will boot just fine. If this doesn&#8217;t work ask me by commenting on this post or <a href="mailto:subins2000.help@blogger.com" >email me</a>.</div>