---
title: Block a site on all browsers in Linux
author: Subin Siby
type: post
date: 2012-09-27T12:58:00+00:00
excerpt: 'This tutorial will help you to block a site in all browsers on LinuxOpen Terminal (ALT+F2).Type sudo -i&nbsp;in the input field as shown in the picture above.Check the Run in terminal option.Finally click Run button.Type your password if necessary and ...'
url: /block-a-site-on-all-browsers-in-linux
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1964377510
  - 1964377510
  - 1964377510
  - 1964377510
  - 1964377510
  - 1964377510
syndication_item_hash:
  - 647940b27e78a554fba901a5ec247e63
  - 647940b27e78a554fba901a5ec247e63
  - 647940b27e78a554fba901a5ec247e63
  - 647940b27e78a554fba901a5ec247e63
  - 647940b27e78a554fba901a5ec247e63
  - 647940b27e78a554fba901a5ec247e63
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
  - http://sag-3.blogspot.com/2012/09/block-site-in-all-browser-on-linux.html
categories:
  - Linux
  - Ubuntu

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This tutorial will help you to block a site in all browsers on <b>Linux</b></p> 
  
  <p>
    Open Terminal (<b>ALT+F2</b>).
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-VoAxWEuLVtA/UGRKbYhRgHI/AAAAAAAAB6Q/6834dirz7qU/s1600/run.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-VoAxWEuLVtA/UGRKbYhRgHI/AAAAAAAAB6Q/6834dirz7qU/s1600/run.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Type <b>sudo -i</b>&nbsp;in the input field as shown in the picture above.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Check the <b>Run in terminal </b>option.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Finally click <b>Run </b>button.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Type your password if necessary and press enter.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Then enter the following commands.
  </div>
  
  <blockquote class="tr_bq">
    <p>
      gedit /etc/hosts
    </p>
  </blockquote>
  
  <p>
    You will get <b>Gedit Text Editor</b>&nbsp;window.<br />For Example we need to block&nbsp;<b>Facebook a</b>dd the following lines just after <b>127.0.0.1 localhost</b>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      0.0.0.1 &nbsp; &nbsp; &nbsp; &nbsp; facebook.com<br />0.0.0.1 &nbsp; &nbsp; &nbsp; &nbsp; www.facebook.com
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-p-iR57Twd0w/UGRMYliuZVI/AAAAAAAAB6Y/d7T1LjLxYDY/s1600/block.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-p-iR57Twd0w/UGRMYliuZVI/AAAAAAAAB6Y/d7T1LjLxYDY/s1600/block.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
  </div>
  
  <p>
    &nbsp;That&#8217;s it. When you now open <b>www.facebook.com </b>or <b>facebook.com</b>&nbsp;You cannot access it. In <b>Google Chrome</b>&nbsp;you will get this page.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-O20ixO2_ef4/UGRNAtgWr6I/AAAAAAAAB6g/wqD7Z-eETGo/s1600/blocked.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="338" src="//1.bp.blogspot.com/-O20ixO2_ef4/UGRNAtgWr6I/AAAAAAAAB6g/wqD7Z-eETGo/s640/blocked.png" width="640" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    To enable back <b>Facebook </b>remove the lines we added from the file&nbsp;<b>/etc/hosts</b>.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
  </div>
  
  <blockquote class="tr_bq">
    <p>
      0.0.0.1 &nbsp; &nbsp; &nbsp; &nbsp; facebook.com<br />0.0.0.1 &nbsp; &nbsp; &nbsp; &nbsp; www.facebook.com
    </p>
  </blockquote>
</div>