---
title: Add @mentions To Input Fields – jQuery sMention Plugin
author: Subin Siby
type: post
date: 2014-01-08T17:54:30+00:00
url: /jquery-smention-plugin
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - JavaScript
  - jQuery
tags:
  - JSON
  - Plugin
  - URL

---
Mentioning persons using **@ **is pretty common on social web sites. Facebook, Google+ and even StackOverflow have the **@mention **feature. They developed it on their own and doesn&#8217;t share the code with others. If you are developing a social site, then it&#8217;s better to have this mention feature. I wanted this, but the **jQuery** plugins I found was too big.

The first plugin I found was **20 KB** ! This made me create a plugin of my own to accomplish my need. Hence I created **sMention** &#8211; a plugin that implements the @mention feature on elements. I created it in a day, so there might be errors and it isn&#8217;t perfect. I made a <a href="https://github.com/subins2000/smention" target="_blank"><strong>GitHu</strong><strong>b</strong> repository</a> for sMention. You can contribute to sMention by suggestions, feedback and reporting bugs.


## Current Version : 0.2

## Size : 4.0 KB

## Last Update : 25 / 01 / 2014 (DD/MM/YYYY)

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=gjys13rzolod/s/4bap9bxwi5d2d8e5au5z&class=15" target="_blank">Download</a> <a class="demo" href="http://demos.subinsb.com/jquery/smention" target="_blank">Demo</a>
</div>

In this post I&#8217;m going to give you the details and usage of this plugin.

## Usage

Add the following two lines of code in the **head** section of your **HTML** :

<pre class="prettyprint"><code>&lt;script src="smention.js"&gt;&lt;/script&gt;
&lt;link href="smention.css" rel="stylesheet"/&gt;</code></pre>

The **smention.js** and **smention.css **can be found in the Download package or in the **GitHub** repository (<https://github.com/subins2000/smention>).

A simple example is :

<pre class="prettyprint"><code>$("#textarea").smention("get_users.php");</code></pre>

The **smention** function also accepts other parameters :

## Parameters

  * URL     &#8211; The URL to send the request
  * Options &#8211; A **JSON** array containing the options

Currently the options that supports are :

  * avatar : false ( If set to true, avatars will be shown aside to the name )
  * extraParams : {} ( If a JSON array is given, it will be sent as GET parameters within the request to the URL )
  * position : "above" ( If set to "below", the list will be shown below the input field )
  * after : null ( If a string is given, it will be added within the selection of user. eg : @1 text_added )
  * cache : true ( If set to false, requests will not be cached )

## Response Data

The response data which the plugin will parse should be in **JSON** format. An example is given :

<pre class="prettyprint"><code>{
 "0" : {
  "id":"1",
  "name":"Subin",
  "avatar":"http://open.subinsb.com/data/1/img/avatar"
 }, 
 "1" : {
  "id":"2",
  "name":"Simrin",
  "avatar":""
 }
}</code></pre>

By default the avatar win&#8217;t be shown unless you set the "avatar" parameter to "true".

The **id** key holds the unique id of the user. This can be a integer or a username according to your system.

## Customize

You can change the trigger character from **@** to **#** or any other characters. You only need to replace the keycode. In the **smention.js** file, replace all **50** character to **51** if you need to trigger on **#** character. Here are some of the keycodes for special characters :

<pre class="prettyprint"><code>keyCode - Key - Description
51      - 3   - used for &lt;strong>#&lt;/strong>
52      - 4   - used for &lt;strong>$&lt;/strong>
54      - 6   - used for &lt;strong>^&lt;/strong> </code></pre>

You can find more key codes from <a href="http://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes" target="_blank">here</a>.

## Examples

Adding on the #textarea element (textarea) with avatars :

<pre class="prettyprint"><code>$("#textarea").smention("get_users.php",{
 avatar : true
});</code></pre>

Adding on the #input element (input[type=text]) with extraParams :

<pre class="prettyprint"><code>$("#input").smention("get_users.php",{
 extraParams : {"akey" : "avalue"}
});</code></pre>

Adding a white space after the user selection :

<pre class="prettyprint"><code>$("#input").smention("get_users.php",{
 after : " "
});</code></pre>

The above code will add a white space after the string of the user selection. Example :

<pre class="prettyprint"><code>@subin </code></pre>

If the position parameter in options is set to "below", the suggestion box will be below the input :

[<img class="aligncenter size-medium wp-image-2315" alt="0080" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0080-300x122.png" width="300" height="122" />][9]

instead of above the input :

[<img class="aligncenter size-medium wp-image-2316" alt="0081" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0081-300x123.png" width="300" height="123" />][10]

This plugin is used in <a href="http://open.subinsb.com" target="_blank">Open</a>. You can see this plugin in action @ Open. Please contribute to this plugin. We can make this plugin more awesome. I will keep improving this project if I get time.

 [1]: #current-version-02
 [2]: #size-40-kb
 [3]: #last-update-25-01-2014-ddmmyyyy
 [4]: #usage
 [5]: #parameters
 [6]: #response-data
 [7]: #customize
 [8]: #examples
 [9]: //lab.subinsb.com/projects/blog/uploads/2014/01/0080.png
 [10]: //lab.subinsb.com/projects/blog/uploads/2014/01/0081.png
