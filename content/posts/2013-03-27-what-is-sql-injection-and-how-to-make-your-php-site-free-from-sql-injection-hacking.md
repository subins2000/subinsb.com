---
title: What is SQL Injection and how to make your PHP site free from SQL Injection hacking
author: Subin Siby
type: post
date: 2013-03-27T13:32:00+00:00
excerpt: "SQL Injection (SQLi) is a very dangerous thing that a hacker can do to your site. This happens mostly in SQL&nbsp;queries. Let me make you understand this in a simple way. Suppose you're SQL&nbsp;query code is this:$user=$_GET['user'];$sql=mysql_query(..."
url: /what-is-sql-injection-and-how-to-make-your-php-site-free-from-sql-injection-hacking
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 34893683de9a386556a263642b9bbfa8
  - 34893683de9a386556a263642b9bbfa8
  - 34893683de9a386556a263642b9bbfa8
  - 34893683de9a386556a263642b9bbfa8
  - 34893683de9a386556a263642b9bbfa8
  - 34893683de9a386556a263642b9bbfa8
syndication_permalink:
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
  - http://sag-3.blogspot.com/2013/03/what-is-sql-injection-and-how-to-make.html
categories:
  - MySQL
  - PHP
  - SQL
tags:
  - Hack
  - Hacker
  - Injection

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b><a href="http://en.wikipedia.org/wiki/SQL_injection" >SQL Injection (SQLi)</a></b> is a very dangerous thing that a hacker can do to your site. This happens mostly in <b>SQL</b>&nbsp;queries. Let me make you understand this in a simple way. Suppose you&#8217;re <b>SQL</b>&nbsp;query code is this:</p> 
  
  <blockquote class="tr_bq">
    <p>
      $user=$_GET[&#8216;user&#8217;];<br />$sql=mysql_query("SELECT * FROM users WHERE user='".$user."&#8216;");
    </p>
  </blockquote>
  
  <div>
    It&#8217;s a normal code. <b>BUT</b>&nbsp;it is a very easy method for hacker to easily destroy your database.
  </div>
  
  <div>
    The user ID is getting from a <b>GET</b>&nbsp;request. If the file&#8217;s name (where the request is going) is <b>user.php</b>. The <b>URL</b>&nbsp;may be like this:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      http://example.com/user.php?user=<span style="color: red;">subin</span>
    </p>
  </blockquote>
  
  <div>
    and the <b>SQL</b>&nbsp;query will be :
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        SELECT * FROM users WHERE user=&#8217;<span style="color: red;">subin</span>&#8216;
      </p>
    </blockquote>
  </div>
  
  <div>
    The file will print out user information and other stuffs. But what if the hacker put on more values in the user variable in the <b>URL</b>. Suppose like this:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      http://example.com/user.php?user=<span style="color: red;">subin&#8217;;DROP TABLE users;</span><span style="color: red;">SELECT * FROM user WHERE user=&#8217;otherguy</span>
    </p>
  </blockquote>
  
  <div>
    OR like this:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      http://example.com/user.php?user=<span style="color: red;">subin";DROP TABLE users;SELECT * FROM user WHERE user=&#8217;otherguy</span>
    </p>
  </blockquote>
  
  <div>
    and the SQL query will be:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      SELECT * FROM users WHERE user=&#8217;<span style="color: red;">subin</span>&#8216;;DROP TABLE users;SELECT * FROM user WHERE user=&#8217;otherguy&#8217;;
    </p>
  </blockquote>
  
  <div>
    You know what will happen. Yes that&#8217;s right. The table &#8216;users&#8217; will be deleted and your entire table is lost. The hacker can also delete the database if he/she wants. So now you understand what is&nbsp;<b>SQL Injection</b><br /><b><br /></b>
  </div>
  
  <div style="text-align: left;">
    <u><span style="font-size: large;"><b>How to make your PHP site free from SQL Injection hacking</b></span></u>
  </div>
  
  <div style="text-align: left;">
    <u><span style="font-size: large;"><b><br /></b></span></u>
  </div>
  
  <div>
    This method is real simple. All you have to do is add&nbsp;<b>mysql_real_escape_string()</b> function in variables in an &nbsp;<b>SQL</b>&nbsp;query. Example:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      $user=<b><span style="color: red;">mysql_real_escape_string(</span></b>$_GET[&#8216;user&#8217;]<span style="color: red;">)</span>;<br />$sql=mysql_query("SELECT * FROM users WHERE user='".$user."&#8216;");
    </p>
  </blockquote>
  
  <div>
    Enjoy un-hackable site.
  </div>
</div>