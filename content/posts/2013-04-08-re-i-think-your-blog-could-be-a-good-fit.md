---
title: 'Re: I think your blog could be a good fit…'
author: Subin Siby
type: post
date: 2013-04-08T16:56:00+00:00
draft: true
url: /?p=1348
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Uncategorized
tags:
  - Uncategorized

---
<table  width="650" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <!-- BEGIN Content Area 1 --></p> 
      
      <p>
        Hi Subin Siby,
      </p>
      
      <p>
        Just wanted to follow up on my note (below) &mdash; have you had a chance to take a look?
      </p>
      
      <p>
        As explained, after reading your blog, I think you would be a great fit as a wizpert and would love to have you join our community.
      </p>
      
      <p>
        To create a free account go to http://wizpert.com/register_expert?beta_key=computers
      </p>
      
      <p>
        Please contact me personally with any questions.
      </p>
      
      <p>
        All the best,<br />Michael<br />&#8212;&#8212;&#8212;&#8212;<br />Michael Weinberg<br />Founder, CEO<br />Wizpert &#8211; NY, NY<br />917-251-2445<br />&nbsp;
      </p>
      
      <p>
        ___________________________________
      </p>
      
      <p>
        &nbsp;
      </p>
      
      <p>
        Hi Subin Siby,
      </p>
      
      <p>
        I was reading your blog http://sag-3.blogspot.com/2012/08/upload-more-than-1-mb-file-in-php.html#.UKED1nZ_VTg and thought that your knowledge would be of great value to our users, who pay our experts a premium for advice.
      </p>
      
      <p>
        Our platform, called Wizpert, is a fast growing community of experts, where users seek advice and coaching on an array of topics, such as programming, computers, social media and more.
      </p>
      
      <p>
        If you decide to sign up, you will also get a customized Wizpert button to place on your blog &#8211; it will allow engaged readers to connect with you directly at your convenience for a live conversation.
      </p>
      
      <p>
        Please go to http://wizpert.com/register_expert?beta_key=computers and create your quick profile &#8211; it doesn&#8217;t cost you anything and takes just 2 minutes! (you need to use this link since we are still invitation only).
      </p>
      
      <p>
        <br />For more information, pls see the FAQ section on the site, or feel free contact me personally with any questions.
      </p>
      
      <p>
        All the best,<br />Michael<br />&#8212;&#8212;&#8212;&#8212;<br />Michael Weinberg<br />Founder, CEO<br />Wizpert &#8211; NY, NY<br />917-251-2445
      </p>
      
      <p>
        &nbsp;
      </p>
      
      <p>
        &nbsp;
      </p>
      
      <p>
        <!-- END Content Area 1 --></td> </tr> 
        
        <tr>
          <td>
            </p> 
            
            <p  align="center">
              <font  face="Arial" size="1">Click <a  href="http://www.magnetmail1.net/link.cfm?r=1213723160&#038;sid=22677471&#038;m=2489387&#038;u=WIZPERT&#038;j=13674613&#038;s=http://unsubscribe.magnetmail.net/Actions/unsubscribe.cfm?message_id=2489387&#038;user_id=WIZPERT&#038;recipient_id=1213723160&#038;email=subins2000.help@blogger.com&#038;group_id=981283">here</a> to unsubscribe</font>
            </p>
          </td>
        </tr></table>