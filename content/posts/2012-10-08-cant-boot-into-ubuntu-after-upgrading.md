---
title: Can’t boot into Ubuntu after upgrading.
author: Subin Siby
type: post
date: 2012-10-08T14:03:00+00:00
excerpt: 'When you upgrade to any new version of Ubuntu&nbsp;you will not be able to open Ubuntu.&nbsp;There might be a number of reasons. This post will help you to solve the problem of not completing the&nbsp;installation of new packages.To solve this, boot in...'
url: /cant-boot-into-ubuntu-after-upgrading
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 93eb9314014e413bc22a56d0eb4be8b5
  - 93eb9314014e413bc22a56d0eb4be8b5
  - 93eb9314014e413bc22a56d0eb4be8b5
  - 93eb9314014e413bc22a56d0eb4be8b5
  - 93eb9314014e413bc22a56d0eb4be8b5
  - 93eb9314014e413bc22a56d0eb4be8b5
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
  - http://sag-3.blogspot.com/2012/10/cant-boot-into-ubuntu-after-upgrading.html
categories:
  - Linux
  - Ubuntu

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  When you upgrade to any new version of <b>Ubuntu</b>&nbsp;you will not be able to open <b>Ubuntu.</b>&nbsp;There might be a number of reasons. This post will help you to solve the problem of <b>not completing the&nbsp;</b><br /><b>installation of new packages.</b></p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-0W5ayVCROYY/Ub1bpClTFPI/AAAAAAAACuk/hx55dmbpiV0/s1600/0011.jpg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-0W5ayVCROYY/Ub1bpClTFPI/AAAAAAAACuk/hx55dmbpiV0/s1600/0011.jpg" /></a>
  </div>
  
  <p>
    <b><br /></b>To solve this, boot in to <b>Ubuntu. </b>You will be stuck in the <b>Ubuntu Logo.</b>
  </p>
  
  <p>
    In there press <b>ALT + CTRL + F1.</b><br />You will get a terminal.<br /><b>Login</b>&nbsp;to your account there in the terminal by typing your username first and password second.<br />Then Enter the following commands in the terminal.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      sudo -i
    </p>
  </blockquote>
  
  <p>
    <b>You will be asked for your password. </b>Then the next command.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <em style="background-color: white; font-family: arial, sans-serif; font-style: normal; line-height: 16px;">sudo dpkg &#8211;configure -a</em>
    </p>
  </blockquote>
  
  <p>
    Wait for <b>10 Minutes or more </b>for completing it&nbsp;depending on your <b>RAM</b>.<br />After completion reboot your computer using the following command.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      sudo reboot&nbsp;
    </p>
  </blockquote>
  
  <p>
    You can now boot into <b>Ubuntu.</b>
  </p>
</div>