---
title: jQuery Popping Up Text Effect
author: Subin Siby
type: post
date: 2015-04-23T03:30:47+00:00
url: /jquery-effect-popup-text
categories:
  - JavaScript
  - jQuery
  - Program
  - Short Post

---
If you are creating a web app and there is conversation to the user without audio and just text, animations and effect will make the app better.

In this short post, I&#8217;m introducing a simple text effect without any trouble at all. All it does is animate the text&#8217;s font size. But, the effect is like the text is popping up.

<div class="padlinks">
  <a class="demo" href="https://jsfiddle.net/subins2000/jx94ecfp/" target="_blank">Demo</a>
</div>

Here&#8217;s the code. It&#8217;s wrapped in a function and you can easily customize it :

<pre class="prettyprint"><code>function pop_up(){
  var begin_size = 10;
  var end_size = 60;
  var speed = 3000;

  $("&lt;strong>.container&lt;/strong>").css("font-size", begin_size).animate({"font-size" : end_size}, speed);
}
pop_up();</code></pre>

In the above code, it is assumed that the element **.container** has text inside it.

What it does is change the font size to a smaller one, then animate the font-size value to a bigger one.