---
title: You have been invited to contribute to test blog
author: Subin Siby
type: post
date: 2012-05-09T20:53:00+00:00
draft: true
url: /?p=1429
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Uncategorized
tags:
  - Uncategorized

---
The Blogger user Diego Lucero has invited you to contribute to the blog: test blog.

To contribute to this blog, visit:     
<http://www.blogger.com/i.g?inviteID=4525088055043473225&blogID=353249120149731026> 

You'll need to sign in with a Google Account to confirm the invitation and start posting to this blog. If you don't have a Google Account yet, we'll show you how to get one in minutes.

To learn more about Blogger and starting your own free blog visit <http://www.blogger.com>.