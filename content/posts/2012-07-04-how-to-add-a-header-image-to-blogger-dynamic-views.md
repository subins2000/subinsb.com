---
title: How to add a header image to Blogger Dynamic Views
author: Subin Siby
type: post
date: 2012-07-04T14:54:00+00:00
excerpt: 'You must have seen some blogs like mine and Blogger Buzz which have a header image like this.Do you want to add to your blog like this ? Then you are in the right place.Note&nbsp;:- You must have enabled a dynamic view to use this feature.1 :- Create a...'
url: /how-to-add-a-header-image-to-blogger-dynamic-views
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - a784ffcd41e548ee35d5b31f5ef85573
  - a784ffcd41e548ee35d5b31f5ef85573
  - a784ffcd41e548ee35d5b31f5ef85573
  - a784ffcd41e548ee35d5b31f5ef85573
  - a784ffcd41e548ee35d5b31f5ef85573
  - a784ffcd41e548ee35d5b31f5ef85573
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
  - http://sag-3.blogspot.com/2012/07/how-to-add-header-image-to-blogger.html
dsq_thread_id:
  - 1957610889
  - 1957610889
  - 1957610889
  - 1957610889
  - 1957610889
  - 1957610889
categories:
  - Blogger
  - CSS
tags:
  - Tutorials

---
<p dir="ltr" style="text-align: left;">
  You must have seen some blogs like mine and Blogger Buzz which have a header image like this. These header images make the blog attractive. Do you want to add a header image to your blog like this ? Then you are in the right place. It is very easy to do this in your blog. You only need to add few codes to your Blogger Template.
</p>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-t6rqa__OHII/T_RPL32JARI/AAAAAAAAAzk/n3rHRRPiPVg/s1600/buzzheader.png"><img alt="" src="//1.bp.blogspot.com/-t6rqa__OHII/T_RPL32JARI/AAAAAAAAAzk/n3rHRRPiPVg/s1600/buzzheader.png" border="0" /></a>
</div>

<div class="separator" style="clear: both; text-align: center;">
</div>

<div class="separator" style="clear: both; text-align: center;">
</div>

<div class="separator" style="clear: both; text-align: center;">
</div>

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-t2nRjm7y3og/T_RWHAiVjxI/AAAAAAAAA0A/vhESdUXogdw/s1600/header-n.png"><img alt="" src="//3.bp.blogspot.com/-t2nRjm7y3og/T_RWHAiVjxI/AAAAAAAAA0A/vhESdUXogdw/s1600/header-n.png" border="0" /></a>
</div>

**Note** :- You must have enabled a Dynamic views Template on your blog to use this feature.

Create a banner image of size (resolution) ** 1200 X 65 pixels **and upload into [Picasa][1] or any other image uploading service. See this [tutorial][2] to see how to upload an image.

Go to **[Blogger][3] -> Template**. Click on **Customise **<span>button.</span>

<span style="background-color: white;"><a style="margin-left: 1em; margin-right: 1em; text-align: center;" href="//1.bp.blogspot.com/-6GYDqGYOJpg/T_RXipRKxzI/AAAAAAAAA0M/x3gX4j6Uo5g/s1600/cbutton.png"><img alt="" src="//1.bp.blogspot.com/-6GYDqGYOJpg/T_RXipRKxzI/AAAAAAAAA0M/x3gX4j6Uo5g/s1600/cbutton.png" border="0" /></a></span>

<span style="background-color: white;">You will now enter the <b>Template Designer </b>of your blog. </span><span style="line-height: 1.5em;">Go to </span><b style="line-height: 1.5em;">Advanced -> Add CSS</b> page.

You will see a text box on the right side of the screen as show in the picture below :

<img style="border: 0px;" alt="" src="https://2.bp.blogspot.com/-oyEAJ1_w7io/T_RX7jxYgEI/AAAAAAAAA0U/yaAXG6pb3ZY/s1600/tdesigner.png" width="1366" height="287" border="0" />

Type these text (code) in that text box :

<pre class="prettyprint"><code>/*Subin's Blog*/.header-bar{background-image:url('&lt;span style="color: red;">YOUR IMAGE URL&lt;/span>') !important;}</code></pre>

<div class="separator" style="clear: both;">
  An Example of adding the image <b>http://goo.gl/Xt1G1</b> as header:
</div>

<pre class="prettyprint"><code>/*Subin's Blog*/.header-bar{background-image:url('http://goo.gl/Xt1G1') !important;}</code></pre>

After doing all this click on "Apply To Blog"<a style="margin-left: 1em; margin-right: 1em; text-align: center;" href="//4.bp.blogspot.com/-wb0_X6ZPA8E/T_RZCJPHl9I/AAAAAAAAA0c/kwEmW3Dbw3E/s1600/ablog.png"><img alt="" src="//4.bp.blogspot.com/-wb0_X6ZPA8E/T_RZCJPHl9I/AAAAAAAAA0c/kwEmW3Dbw3E/s1600/ablog.png" border="0" /></a> button.

After the changes have been saved, open your blog and your header has a background image now.

 [1]: http://picasaweb.google.com/
 [2]: http://sag-3.blogspot.com/2012/10/upload-your-image-to-web-in-just-minute.html
 [3]: http://blogger.com/