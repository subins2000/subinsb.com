---
title: Posting Code Blocks On WordPress
author: Subin Siby
type: post
date: 2014-02-21T16:54:44+00:00
url: /posting-code-blocks-on-wordpress
authorsure_include_css:
  - 
  - 
  - 
categories:
  - WordPress
tags:
  - Code Blocks
  - Plugin

---
If you&#8217;re a web developer or a programmer and have a programming blog set up in WordPress, you will find it difficult to include codes in the posts. There is no tool in the Visual Editor toolbar menu to automatically wrap a piece of code into a code block.

So you have to manually go to the **Text** version of the post and add the code in **HTML** **&lt;pre&gt;** or **&lt;code&gt;** tag or inside **&lt;pre&gt;&lt;code&gt;&lt;/code&gt;&lt;/pre&gt;**. As you can see there are different ways to embed codes in your post. So, why not add a tool that satisfies this. I&#8217;m very happily introducing the **Code Blocks Plugin**, an easy plugin that adds a **Code Block** tool in the toolbox of **Visual** editor and **Text** editor.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/WordPress/code-blocks.zip" target="_blank">Download</a>
</div>

You can also use this plugin to add **Custom CSS** to Visual Editor of WordPress.

## Configure

You can access the Configuration Page from **Posts** -> **Code Blocks**.

Under the **Display** section you can choose where to display the Code Block button.

The **Content** section can be used to mention the starting code and ending code of the code block :<figure id="attachment_2531" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2531" alt="Configure Code - Code Blocks" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0105-300x117.png" />][1]<figcaption class="wp-caption-text">Configure Code &#8211; Code Blocks</figcaption></figure> 

If you would like to add **Custom CSS** to the Visual Editor, use the textarea in the **Custom CSS** section. By default, the plugin add styles to code tag of pre.<figure id="attachment_2532" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2532" alt="Custom CSS To WordPress Visual Editor" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0106-300x75.png" width="300" height="75" />][2]<figcaption class="wp-caption-text">Custom CSS To WordPress Visual Editor</figcaption></figure> 

If you are making a code inserted as paragraph in your WordPress post and is making that in to a Code Block, then the Paragraph tags will enter into the code block. This might increase the distance between lines of the code. If you don&#8217;t want that, tick the **Remove Paragraph** option :<figure id="attachment_2533" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2533" alt="Other Settings - Code Blocks" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0107-300x124.png" width="300" height="124" />][3]<figcaption class="wp-caption-text">Other Settings &#8211; Code Blocks</figcaption></figure> 

The icon of the tool in Visual Editor is :

<//subinsb.com/uploads/2017/10/codeblocks-tool-icon.png>

That&#8217;s all the configuration of Code Blocks. Hope you like this plugin. If you liked it, <a title="Donate" href="//subinsb.com/donate" target="_blank">please donate</a>.

If you need help, use the <a title="Code Blocks" href="//subinsb.com/ask/code-blocks" target="_blank">Code Blocks</a> plugin page.

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0105.png
 [2]: //lab.subinsb.com/projects/blog/uploads/2014/02/0106.png
 [3]: //lab.subinsb.com/projects/blog/uploads/2014/02/0107.png
