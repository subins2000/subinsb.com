---
title: Create MySQL Injection free Secure Login System in PHP
author: Subin Siby
type: post
date: 2013-08-07T03:30:00+00:00
excerpt: "There were a lot of people who created tutorials to create a PHP&nbsp;Login System. But they were all vulnerable to MySQL&nbsp;Injection. In this post I'm going to demonstrate a login system free of this vulnerability. There are mysqli&nbsp;and PDO&nbs..."
url: /php-secure-login-system
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
  - http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html
dsq_thread_id:
  - 1958540733
  - 1958540733
  - 1958540733
  - 1958540733
  - 1958540733
  - 1958540733
syndication_item_hash:
  - c7605e15dd13b8825cecfd5885a04f5b
  - c7605e15dd13b8825cecfd5885a04f5b
  - c7605e15dd13b8825cecfd5885a04f5b
  - c7605e15dd13b8825cecfd5885a04f5b
  - c7605e15dd13b8825cecfd5885a04f5b
  - c7605e15dd13b8825cecfd5885a04f5b
categories:
  - CSS
  - HTML
  - MySQL
  - PHP
  - SQL
tags:
  - Hash
  - Injection
  - PDO
  - Secure

---
There were a lot of people who created tutorials to create a **PHP** Login System. But they were all vulnerable to **MySQL** Injection. In this post I&#8217;m going to demonstrate a login system free of this vulnerability. It is very secure. There are **mysqli** and **PDO** in **PHP** to escape these injections. We are going to use **PDO **( PHP Data Object** **).

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=v8xwuepqlefmot8&class=1">Download</a> <a class="demo" href="http://demos.subinsb.com/secure-php-login-system">Demo</a>
</div>


# UPDATE &#8211; logSys

There is a new, free, better Advanced Login System which you can check out [here][2].

First of all create a file named **login.php**, **home.php**, **logout.php**

## Create Users Table

****<span style="line-height: 1.5em;">For storing user information you have to create a table named </span><b style="line-height: 1.5em;">users</b><span style="line-height: 1.5em;">. Here is the </span><b style="line-height: 1.5em;">SQL</b><span style="line-height: 1.5em;"> code to create the table.</span>

<pre class="prettyprint"><code>CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`username` text NOT NULL,
`password` text NOT NULL,
`psalt` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;</code></pre>

<ol style="text-align: left;">
  <li>
    The column <b>username</b> is to store the <b>e-mail</b> of the user. This e-mail is used as the username.
  </li>
  <li>
    The column <b>password</b> is to store user&#8217;s password which will be heavily encrypted using <b>SHA256</b>.
  </li>
  <li>
    The column <b>psalt</b> contains a random text to check if password is true.
  </li>
</ol>

Now we should add a user to the table. Execute the following **SQL** code to create a user.

<pre class="prettyprint"><code>INSERT INTO `users` (
 `id`, 
 `username`, 
 `password`, 
 `psalt`
) VALUES (
 NULL, 
 'subins2000@gmail.com', 
 '4f8ee01c497c8a7d6f44334dc15bd44fe5acea9aed07f67e34a22ec490cfced1', 
 's*vl%/?s8b*b4}b/w%w4'
);</code></pre>

The user is inserted with the following values: [<img class="aligncenter" src="//1.bp.blogspot.com/-3hts0gYWk4E/UgB-1cse2FI/AAAAAAAAC38/CfhndrRg-6Q/s1600/0033.png" alt="" border="0" />][4]

## <b>login.php</b>

Create a login form :

<pre class="prettyprint"><code>&lt;form method="POST" action="login.php" style="border:1px solid black;display:table;margin:0px auto;padding-left:10px;padding-bottom:5px;"&gt;
 &lt;table width="300" cellpadding="4" cellspacing="1"&gt;
  &lt;tr&gt;&lt;td&gt;&lt;td colspan="3"&gt;&lt;strong&gt;User Login&lt;/strong&gt;&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td width="78"&gt;E-Mail&lt;/td&gt;&lt;td width="6"&gt;:&lt;/td&gt;&lt;td width="294"&gt;&lt;input size="25" name="mail" type="text"&gt;&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td&gt;Password&lt;/td&gt;&lt;td&gt;:&lt;/td&gt;&lt;td&gt;&lt;input name="pass" size="25" type="password"&gt;&lt;/td&gt;&lt;/tr&gt;
  &lt;tr&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;&lt;input type="submit" name="Submit" value="Login"&gt;&lt;/td&gt;&lt;/tr&gt;
 &lt;/table&gt;
 Login System provided by &lt;a target="_blank" href='http://sag-3.blogspot.com/2013/08/secure-injection-free-login-system-php.html'&gt;Subins&lt;/a&gt;
&lt;/form&gt;</code></pre>

Now we should add the **PHP** code to check whether the **username** and **password** is correct. You should add the **PHP** code before **</form**> we just added in **login.php**.

<pre class="prettyprint"><code>&lt;?php
session_start();
if(isset($_SESSION['user']) && $_SESSION['user']!=''){header("Location:home.php");}
$dbh=new PDO('mysql:dbname=db;host=127.0.0.1', 'username', 'password');/*Change The Credentials to connect to database.*/
$email=$_POST['mail'];
$password=$_POST['pass'];
if(isset($_POST) && $email!='' && $password!=''){
 $sql=$dbh-&gt;prepare("SELECT id,password,psalt FROM users WHERE username=?");
 $sql-&gt;execute(array($email));
 while($r=$sql-&gt;fetch()){
  $p=$r['password'];
  $p_salt=$r['psalt'];
  $id=$r['id'];
 }
 $site_salt="subinsblogsalt";/*Common Salt used for password storing on site. You can't change it. If you want to change it, change it when you register a user.*/
 $salted_hash = hash('sha256',$password.$site_salt.$p_salt);
 if($p==$salted_hash){
  $_SESSION['user']=$id;
  header("Location:home.php");
 }else{
  echo "&lt;h2&gt;Username/Password is Incorrect.&lt;/h2&gt;";
 }
}
?&gt;</code></pre>

## <b>home.php</b>

<pre class="prettyprint"><code>&lt;html&gt;&lt;head&gt;&lt;/head&gt;
&lt;body&gt;
&lt;?
session_start();
if($_SESSION['user']==''){
 header("Location:login.php");
}else{
 $dbh=new PDO('mysql:dbname=db;host=127.0.0.1', 'root', 'backstreetboys');
 $sql=$dbh-&gt;prepare("SELECT * FROM users WHERE id=?");
 $sql-&gt;execute(array($_SESSION['user']));
 while($r=$sql-&gt;fetch()){
  echo "&lt;center&gt;&lt;h2&gt;Hello, ".$r['username']."&lt;/h2&gt;&lt;/center&gt;";
 }
}
?&gt;
&lt;/body&gt;
&lt;/html&gt;</code></pre>

## <b>logout.php</b>

This file is simple. Just add the following :

<pre class="prettyprint"><code>&lt;?
session_start();
session_destroy();
?&gt;</code></pre>

Now login using username as **subins2000@gmail.com** and password as **subinsiby**. You will be redirected to **home.php** and it will say the following:

<div class="separator" style="clear: both; text-align: center;">
  <a style="margin-left: 1em; margin-right: 1em;" href="//3.bp.blogspot.com/-mbpIZ8S5k7I/UgCIQd-ynMI/AAAAAAAAC4M/NhXZLikRMhg/s1600/0034.png"><img src="//3.bp.blogspot.com/-mbpIZ8S5k7I/UgCIQd-ynMI/AAAAAAAAC4M/NhXZLikRMhg/s1600/0034.png" alt="" border="0" /></a>
</div>

## register.php

What&#8217;s logging in without registering ? Here&#8217;s a sample Registration page :

<pre class="prettyprint"><code>&lt;?
session_start();
if(isset($_SESSION['user']) && $_SESSION['user']!=''){
 header("Location:home.php");
}
?&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;
 &lt;head&gt;&lt;/head&gt;
 &lt;body&gt;
 &lt;form action="register.php" method="POST"&gt;
  &lt;label&gt;E-Mail &lt;input name="user" /&gt;&lt;/label&gt;&lt;br/&gt;
  &lt;label&gt;Password &lt;input name="pass" type="password"/&gt;&lt;/label&gt;&lt;br/&gt;
  &lt;button name="submit"&gt;Register&lt;/button&gt;
 &lt;/form&gt;
 &lt;?
  if(isset($_POST['submit'])){
   $musername = "root";
   $mpassword = "backstreetboys";
   $hostname = "127.0.0.1";
   $db = "p";
   $port = 3306;
   $dbh=new PDO('mysql:dbname='.$db.';host='.$hostname.";port=".$port,$musername, $mpassword);/*Change The Credentials to connect to database.*/
   if(isset($_POST['user']) && isset($_POST['pass'])){
    $password=$_POST['pass'];
    $sql=$dbh-&gt;prepare("SELECT COUNT(*) FROM `users` WHERE `username`=?");
    $sql-&gt;execute(array($_POST['user']));
    if($sql-&gt;fetchColumn()!=0){
     die("User Exists");
    }else{
     function rand_string($length) {
      $str="";
      $chars = "subinsblogabcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      $size = strlen($chars);
      for($i = 0;$i &lt; $length;$i++) {
       $str .= $chars[rand(0,$size-1)];
      }
      return $str; /* http://subinsb.com/php-generate-random-string */
     }
     $p_salt = rand_string(20); /* http://subinsb.com/php-generate-random-string */
     $site_salt="subinsblogsalt"; /*Common Salt used for password storing on site.*/
     $salted_hash = hash('sha256', $password.$site_salt.$p_salt);
     $sql=$dbh-&gt;prepare("INSERT INTO `users` (`id`, `username`, `password`, `psalt`) VALUES (NULL, ?, ?, ?);");
     $sql-&gt;execute(array($_POST['user'], $salted_hash, $p_salt));
     echo "Successfully Registered.";
    }
   }
  }
  ?&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

Note to change the Database credentials on above code.

This login system is totally **99%** secure. It&#8217;s very hard to crack for a hacker and it&#8217;s completely **MySQL Injection **free. It took me less than 1 hour to create this system and create this post. Happy Logging. If you have any problems/suggestions/feedbacks just comment. I will help you.

 [1]: #update-8211-logsys
 [2]: //subinsb.com/php-logsys "PHP Secure, Advanced Login System"
 [3]: #create-users-table
 [4]: //1.bp.blogspot.com/-3hts0gYWk4E/UgB-1cse2FI/AAAAAAAAC38/CfhndrRg-6Q/s1600/0033.png
 [5]: #loginphp
 [6]: #homephp
 [7]: #logoutphp
 [8]: #registerphp
