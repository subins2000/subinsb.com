---
title: Log In With Username / E-Mail in PHP Login System
author: Subin Siby
type: post
date: 2014-03-17T05:52:57+00:00
url: /php-log-in-with-username-email
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - Secure

---
If your login system have both username and email parameter, then you can allow your users to log in with username or email. This is very easy for users, because if they forget their username, then they can log in with their email address. I will tell you an easy to way to set up log in with both username and email. It&#8217;s very easy.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/php/username-email-login" target="_blank">Demo</a>
</div>

A change in your **SQL** query can make this possible. Here&#8217;s how to do this.

## Change Your Login Form

Since we&#8217;re going to make login with both username and email possible, we have to change the username field name to **Username / E-Mail**.

<pre class="prettyprint"><code>&lt;label&gt;Username / E-Mail&lt;/label&gt;
&lt;span style="line-height: 1.5em;">&lt;input type="text" name="login" /&gt;&lt;/span></code></pre>

We will use the name parameter as **login** because we accept both email and username.

If you are making a new login form, see <a title="Create MySQL Injection free Secure Login System in PHP" href="//subinsb.com/php-secure-login-system" target="_blank">this post on How to create a secure Login System</a>. The users table in the post have username column containing the email of user. Here is a proper user table with username column containing username and email column containing the email of user :

<pre class="prettyprint"><code>CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`username` text NOT NULL,
`email` text NOT NULL,
`password` text NOT NULL,
`psalt` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;</code></pre>

The others are all same except the login form. You should change the name to **Username / E-Mail** instead of just **E-Mail**.

## SQL query Change

The login system should authorize if the user entered username or email. So we should change the SQL query like this :

<pre class="prettyprint"><code>SELECT id,password,psalt FROM users WHERE (username=:login OR email=:login) AND password=:pass</code></pre>

In PDO, we execute the above SQL query as :

<pre class="prettyprint"><code>$sql = $dbh-&gt;prepare("SELECT id,password,psalt FROM users WHERE (username=:login OR email=:login) AND password=:pass");
$sql-&gt;bindValue(":login", $_POST['login']);
$sql-&gt;bindValue(":pass", $_POST['pass']);
$sql-&gt;execute();</code></pre>

If you&#8217;re using <a href="//subinsb.com/php-secure-login-system" target="_blank">this tutorial</a> to create the login system, change the code :

<pre class="prettyprint"><code>$sql=$dbh-&gt;prepare("SELECT id,password,psalt FROM users WHERE username=?");
$sql-&gt;execute(array($email));
while($r=$sql-&gt;fetch()){
 $p=$r['password'];
 $p_salt=$r['psalt'];
 $id=$r['id'];
}</code></pre>

to this code :

<pre class="prettyprint"><code>$sql = $dbh-&gt;prepare("SELECT id,password,psalt FROM users WHERE username=:login OR email=:login");
$sql-&gt;bindValue(":login", $_POST['login']);
$sql-&gt;execute(array($email));
while($r=$sql-&gt;fetch()){
 $p=$r['password'];
 $p_salt=$r['psalt'];
 $id=$r['id'];
}</code></pre>

This will make **MySQL** to check columns that has the email or username value with what user entered.