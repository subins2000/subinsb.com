---
title: New Theme Brenton
author: Subin Siby
type: post
date: 2016-11-18T12:51:18+00:00
url: /new-theme-brenton
categories:
  - Content Management System
  - Personal
  - Themes
  - Web
  - WordPress
tags:
  - Open Source
  - UI

---
It&#8217;s been some time since I have been active on this blog. For the past months, I had been paying little attention to my blog. It&#8217;s because I was busy with other projects and school of course.

From now on, I&#8217;ll try to make the blog more active. As a first step, I have updated the theme. This is the third update to the theme.

The previous themes were named "Subin&#8217;s Blog V1" & "Subin&#8217;s Blog V2". That&#8217;s a pretty boring name. So I went to a random name picker website and chose one &#8211; "Brenton".

Someone who liked my last theme wanted it to be open sourced and so I did it. The old repo is moved to the new one.

<div class="padlinks">
  <a class="demo" href="https://github.com/subins2000/brenton" target="_blank">GitHub</a>
</div>

## Changes

You&#8217;ll notice the UI change first. The comet falling background is removed for good. The site was laggy in performance because of that bg. Here is how I did that bg :

<pre class="prettyprint"><code>body:before{
  content: "";
  position: fixed;
  top: -50%;
  left: -50%;
  bottom: -50%;
  right: -50%;
  z-index: -1;
  -webkit-transform: rotate(-30deg);
  -moz-transform: rotate(-30deg);
  -ms-transform: rotate(-30deg);
  -o-transform: rotate(-30deg);
  transform: rotate(-30deg);
  background-image: url("bg.png");
  background-color: #000;
  background-repeat: repeat;
}</code></pre>

The "bg.png" file was rotated diagonally to make the comet falling effect. This was a huge mistake as browser had trouble rendering this effect every time the user scrolled. As a result scrolling caused a lag and it was messy as glue. Yuck !

That background is replaced by a linear gradient now :

<pre class="prettyprint"><code>body{
  background: linear-gradient( 0deg, #4CA454, #000 );
}</code></pre>

And overall this is how the site look :

[<img src="https://raw.githubusercontent.com/subins2000/brenton/master/screenshot.png" alt="Brenton WordPress Theme" height="673" />][1] 

Please comment your feedback & suggestions. It will be very helpful. As I said, I&#8217;m gonna drive this blog on the right track once again. 🙂

 [1]: https://raw.githubusercontent.com/subins2000/brenton/master/screenshot.png