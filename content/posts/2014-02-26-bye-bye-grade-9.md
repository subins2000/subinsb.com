---
title: 9th Grade Is Over :-(
author: Subin Siby
type: post
date: 2014-02-26T12:39:47+00:00
url: /bye-bye-grade-9
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Personal

---
Today marks the end of 9th grade in my school life. It&#8217;s been a wonderful journey with a lot of laughs and seriousness. Got nice grades in the exams and all ends well & happy. Today, my classmates went wild. We celebrated and celebrated until the window was broken. The student who didn&#8217;t break the window with consciousness got beat up by the principal. It was painful to watch and hear his screaming.

Today was a great day and also a bad day. One of my classmates cried and others tried their best to make the final day of 9th grade fun and happier. Like students, teachers were also in a good mood today. Everyone was happy and we communicated together like friends.

Maths teacher asked us to introduce puzzles for solving in class. Recently while I was surfing through StackOverflow, I read <a href="http://math.stackexchange.com/questions/675522/whats-the-intuition-behind-pythagoras-theorem" target="_blank">this StackExchange Maths Question</a> of Pythagorus&#8217; Theorem and it helped me ask it as a puzzle. Nobody got the answer and I had to explain it myself which made me understand more about it :<figure class="wp-caption aligncenter">

<img alt="Pythagorus' Theorem With Relation Of Squares" src="https://i.stack.imgur.com/POhH1.png" width="400" height="205" /><figcaption class="wp-caption-text">Pythagorus&#8217; Theorem With Relation Of Squares</figcaption></figure> <figure class="wp-caption aligncenter"><img alt="Animation Showing relation between Pythagorus' theorem and Square Area" src="http://i.stack.imgur.com/VqNSK.gif" width="251" height="231" /><figcaption class="wp-caption-text">Animation Showing relation between Pythagorus&#8217; theorem and Square Area</figcaption></figure> 

Tomorrow, Friday and Monday are holidays for studying. But I wouldn&#8217;t open my book until Saturday / Sunday. Probably because of the laziness.

Now, I only have a year left in my school life &#8211; the 10th grade. I&#8217;m very sad right now. Where did all my years go ? I really hope someone invent a Time Machine, so that I could recover my lost years when I thought school was s\***. But, now I understand that school is a real blessing that you won&#8217;t get back in life. So, I have to enjoy my last year as I can.

As any 9th graders would think, this year went on too fast. Everything was going on way too fast. I still remember the first time I entered in to the class on June 2013. Now, it&#8217;s 2014 and there goes everything.

This year was so awesome that I wrote a post about it on my blog unlike the previous years. There were so many memories and I won&#8217;t forget at all. Thank you and this journey was awesome. I hope next year would too become as fun and awesome as this year.

Since my exams are coming, I won&#8217;t be able to publish posts like before. Sorry&#8230;