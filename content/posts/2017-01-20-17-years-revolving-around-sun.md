---
title: 17 Years Of Revolving Around The Sun
author: Subin Siby
type: post
date: 2017-01-20T06:29:06+00:00
url: /17-years-revolving-around-sun
aliases:
    - /17
categories:
  - Personal
tags:
  - birth

---
Sweet 17 ! The age in between an adult and juvenile. I&#8217;m going to take a relook at my 16th year.

## Coding

  * Lobby 1.0 was released. That was a trough one.
  * Francium logSys 0.7 was released
  * Updates to Francium voice
  * Release of Francium Diffsocket & Francium Star
  * Many more contributions to FOSS

I could have done more if there was less school. I also got into Android programming and released my first app Lobby.

## Videos

I took my old hobby of making videos. Editing using Kdenlive makes me happy. I look forward to making tutorial videos, and other kinds in the future.

I&#8217;ll continue maintaining my projects and making new ones !
