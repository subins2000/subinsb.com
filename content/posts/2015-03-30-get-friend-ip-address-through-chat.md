---
title: Getting IP Address of Your Friend Through Chat
author: Subin Siby
type: post
date: 2015-03-30T03:30:46+00:00
url: /get-friend-ip-address-through-chat
categories:
  - Security
  - Web
tags:
  - Apache
  - IP
  - Server

---
It&#8217;s awesome to get into the depth of the network, especially when it&#8217;s the internet. An **IP address** can get you to a person far away from your home. It will make you mind blown when you dig deeper into the internet network.

As you may have read, **netstat -an** command gave you the **IP address** of user through **Facebook** chat. But, this works no more. But, there are still ways to do this.

**Facebook **uses the <a href="http://en.wikipedia.org/wiki/XMPP#Strengths" target="_blank"><strong>XMPP</strong></a> protocol for chatting which does not disclose the **IP** address of the chatters. So, what way is there to get the **IP** address of your friend ? Well, you can :

  1. Ask him/her to give his/her **IP address**
  2. Trick him/her into disclosing the **IP address**

The 1st method is simple if the friend is a dearest one. If you&#8217;re trying to catch a cracker, 2nd is the most suited way.

## Trick The Friend

It&#8217;s very easy and if you&#8217;re a good programmer, you probably know how to do this. Every time when a user visits a website, the user is know by his/her **IP address**. So, the **IP address** will be known by the server.

We will use this basic **HTTP** protocol concept to obtain the **IP** **address** of the friend. It&#8217;s really simple if you already have a site running on a **PHP** server. Here&#8217;s how we&#8217;re going to do it :

  * Make a fake page in a website that redirects somewhere else.
  * Send the link to the friend through chat.
  * See the log file of site and get the IP address.

Make a file in the root location of the site called "redirect.php". We name it "redirect.php", because we don&#8217;t want the firend to be suspicious about it.

<pre class="prettyprint"><code>&lt;p&gt;Please Wait...&lt;/p&gt;
&lt;script&gt;window.location = "&lt;?php echo $_GET['redirect'];?&gt;";&lt;/script&gt;</code></pre>

What the above script does is simple : redirects user to the URL given through the **GET **parameter "redirect".

Next we got to do is, send the friend to this page. But before that, it&#8217;s better to make the URL you need to send to the friend :

<pre class="prettyprint"><code>http://mysite.com/redirect.php?redirect=http%3A%2F%2Fsubinsb.com&user=friend+name</code></pre>

Why we add the "user" parameter is to identify the user from which we got the IP when we check the server log.

You should mention a redirect URL in the "redirect" parameter. I recommend a great article, one which the friend will enjoy.

Now, shorten the URL using <a href="http://goo.gl" target="_blank">goo.gl</a> or any other URL shortening services, so that the friend doesn&#8217;t get suspicious about the URL.

Send this URL by saying that it&#8217;s about a great article. You&#8217;re not lying (BTW, I hate lying), because the URL you&#8217;re going to send will redirect to that "great article". This is done by mentioning the article URL in the "redirect" parameter of the **GET** request. AND CLICK SEND !

The user will be taken to the server and then redirected to the article that he likes picked by you.

Next the thing you want is recorded in the server.

The script is short, then how is the **IP address** got ? Pretty simple. All web servers have an **access** log file. On a **Linux** system running **Apache **server, the location is :

<pre class="prettyprint"><code>/var/log/apache/access.log</code></pre>

Open the log file and search for your friend&#8217;s name. Alongside the name, you will see the **IP address**. All web servers logs the visitors reached with the **IP address** and the page visited.

So, now you got the **IP address**. Amaze your friend. But, try to do good things and not be **EVIL !**

You can do this in all chats and emails as long as the friend visits your server.