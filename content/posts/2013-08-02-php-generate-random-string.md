---
title: Generating Random String in PHP
author: Subin Siby
type: post
date: 2013-08-02T03:30:00+00:00
excerpt: "In PHP&nbsp;you can generate random number by using rand&nbsp;function, but there is no specific function to generate a random text. In this post I'm going to show the function to generate random text.Here is the function:function rantext($length) {&nb..."
url: /php-generate-random-string
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
  - http://sag-3.blogspot.com/2013/08/generating-random-text-in-php.html
syndication_item_hash:
  - 567c1ce4ab0dd78898f3e31c9f8f80e2
  - 567c1ce4ab0dd78898f3e31c9f8f80e2
  - 567c1ce4ab0dd78898f3e31c9f8f80e2
  - 567c1ce4ab0dd78898f3e31c9f8f80e2
  - 567c1ce4ab0dd78898f3e31c9f8f80e2
  - 567c1ce4ab0dd78898f3e31c9f8f80e2
categories:
  - PHP
tags:
  - Function
  - Random
  - String
  - Text

---
In **PHP** you can generate random number by using **rand** function, but there is no specific function to generate a random text or string. In this post I&#8217;m going to show the **function** that will generate a random string of length mentioned.
  
Here is the function:

<pre class="prettyprint"><code>function rand_string($length) {
 $str="";
 $chars = "subinsblogabcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
 $size = strlen($chars);
 for($i = 0;$i &lt; $length;$i++) {
  $str .= $chars[rand(0,$size-1)];
 }
 return $str;
}</code></pre>

<h2 style="text-align: left;">
  Usage
</h2>

To generate a random string use **rand_string()** function with the parameter of length of the to be generated string. Here&#8217;s an example:

<pre class="prettyprint"><code>echo rand_string(6);</code></pre>

The above code will print out something like this :

<pre class="prettyprint"><code>oSuN1R</code></pre>

Note that the above printed out string will be different for everyone, not the same.

<h2 style="text-align: left;">
  Explanation
</h2>

<ol style="text-align: left;">
  <li>
    The first variable <strong>$str</strong> is the variable where the function adds random single strings to make the long random string.
  </li>
  <li>
    The variable <strong>$chars</strong> contains lots of characters including integers and alphabetic letters. You can change it if you want.
  </li>
  <li>
    The <strong>$size</strong> variable contains the number of characters in the <b>$chars</b> string.
  </li>
  <li>
    Now it&#8217;s looped at <b>n </b>times where <b>n </b>is the length of to be generated string we mentioned in the function.
  </li>
  <li>
    At each loop a character is added to the <b>$str</b> variable from random letter got from the <b>$chars</b> variable.
  </li>
  <li>
    And finally the generated string is returned.
  </li>
</ol>