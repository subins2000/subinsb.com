---
title: Create a Search Engine In PHP, MySQL | Part 3
author: Subin Siby
type: post
date: 2014-04-10T05:41:35+00:00
url: /search-engine-in-php-part-3
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - MySQL
  - PHP
  - SQL
tags:
  - Tutorials

---
This is the Part 3 of "How To Create A Search Engine In PHP". In this part, We make additional adjustments and make the search engine more awesome and cool.

<div class="padlinks">
  <a class="download" href="https://github.com/subins2000/search/archive/master.zip" target="_blank">Download</a><a class="demo" href="http://search.subinsb.com" target="_blank">Demo</a>
</div>


## Bot Page

Our crawler leaves a mark on every pages it visit. In other words a custom User Agent String goes with each requests to web pages. In the previous part, we set the user agent string as :

<pre class="prettyprint"><code>DingoBot (http://search.subinsb.com/about/bot.php)</code></pre>

We add a URL with it, because if the site owner sees this in their stats, they would think "what&#8217;s that ?". So, to answer that question we add our bot info link. Also to promote our site.

The site owner can put this in to their **robots.txt** file to block a certain page for the **Dingo!** crawler :

<pre class="prettyprint"><code>User-agent: DingoBot
Disallow: /dontgohere/</code></pre>

So, what I&#8217;m saying is that you should add a "Bot Info" page.

## Stats

What&#8217;s more great than showcasing the crawler&#8217;s job ? Here&#8217;s the stat page of Web Search :

<pre class="prettyprint"><code>&lt;?include("../inc/functions.php");?&gt;
&lt;html&gt;
 &lt;head&gt;
  &lt;?head("Stats");?&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;?headerElem();?&gt;
  &lt;div class="container" style="width:100px;"&gt;
   &lt;h2&gt;Stats&lt;/h2&gt;
   &lt;p&gt;See information about the crawled URLs by DingoBot.&lt;/p&gt;
   &lt;h3&gt;Total URLs Crawled&lt;/h3&gt;
   &lt;strong&gt;
   &lt;?
   $sql=$dbh-&gt;query("SELECT COUNT(id) FROM `search`");
   echo $sql-&gt;fetchColumn();
   ?&gt;
   &lt;/strong&gt;
   &lt;h3&gt;Last Crawled URLs&lt;/h3&gt;
   &lt;ul style="width: 400px;overflow: auto;"&gt;
   &lt;?
   $sql=$dbh-&gt;query("SELECT `url` FROM `search` ORDER BY id DESC LIMIT 5");
   while($r=$sql-&gt;fetch()){
    echo "&lt;li style='margin-bottom:5px;'&gt;".$r['url']."&lt;/li&gt;";
   }
   ?&gt;
  &lt;/ul&gt;
 &lt;/div&gt;
 &lt;?footer();?&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

## Did You Mean ? (Spell Check)

Google and other search engines gives you a suggestion when there&#8217;s a typo on the query you submitted. I have found a way to implement this using Google. It&#8217;s a very easy implementation.

Create a file named **spellcheck.php** in **inc** folder and add this code to it :

<pre class="prettyprint"><code>&lt;?
class SpellCheck{
 private $url="http://translate.google.com/translate_a/t";
 function __construct(){
  return true;
 }
 private function makeURL($s){
  $s=urlencode($s);
  $url=$this-&gt;url."?client=t&sl=en&tl=en&hl=en&sc=2&ie=UTF-8&oe=UTF-8&uptl=en&oc=1&otf=1&ssel=3&tsel=0";
  $url.="&q=$s";
  return $url;
 }
 public function check($s){
  $a="";
  $c=file_get_contents($this-&gt;makeURL($s));
  $c=substr_replace($c, "", 0, 41);
  preg_match('/u003e","(.*?)",[1]/', $c, $m);
  if(isset($m[1])){
   $a=$m[1];
   $a=str_replace('",', '', $a);
  }
  return $a;
 }
}
?&gt;</code></pre>

Now, we&#8217;ll add the code in the **search.php** file that displays the "Did You Mean" suggestion. Actually, In Part 2 where we made the **search.php** file, we already added the code. So, no need for codes.

## Starting the Crawler

When you visit the page **crawler/****runCrawl.php** on your browser, the crawler starts. But, if you keep visiting the page the crawler won&#8217;t start again and again. It will only start the first time you visit the page. When visited, the page prints whether the crawler is currently running or just started running.

The only thing you have to make sure is that the file **crawler/crawlStatus.txt** have permissions to **Read & Write**.

## .htaccess

You can make all error pages to a single file like Google showing the same page for all errors :

<pre class="prettyprint"><code>DirectoryIndex index.php /inc/error.php
ErrorDocument 403 /inc/error.php
ErrorDocument 404 /inc/error.php
ErrorDocument 405 /inc/error.php
ErrorDocument 408 /inc/error.php
ErrorDocument 410 /inc/error.php
ErrorDocument 411 /inc/error.php
ErrorDocument 412 /inc/error.php
ErrorDocument 413 /inc/error.php
ErrorDocument 414 /inc/error.php
ErrorDocument 415 /inc/error.php
ErrorDocument 500 /inc/error.php
ErrorDocument 501 /inc/error.php
ErrorDocument 502 /inc/error.php
ErrorDocument 503 /inc/error.php
ErrorDocument 506 /inc/error.php</code></pre>

In Part 1, we already created one Error Page File. We&#8217;re just linking that file path in **.htaccess**.

## Stats

If you would like to track visitors and count pageviews using Google Analytics or StatCounter, you should paste the code in **inc/track.php** file. If the file is not present, create the file and add your Tracking code inside it.

If you don&#8217;t want tracking, you should remove this line from **inc/functions.php** :

<pre class="prettyprint"><code>include("track.php");</code></pre>

That&#8217;s about it. The Search Engine is completed. You can enhance the search engine more with your coding skills like I did on <a href="http://search.subinsb.com" target="_blank">search.subinsb.com</a>

It is possible that I missed out things from the 3 part tutorial. If you found one, I&#8217;ll be really glad that you point the problem in the comments. Thank you and hope you get it well.

 [1]: #bot-page
 [2]: #stats
 [3]: #did-you-mean-spell-check
 [4]: #starting-the-crawler
 [5]: #htaccess
 [6]: #stats-2
