---
title: Setting Infinite Width on an element – HTML
author: Subin Siby
type: post
date: 2013-08-04T03:30:00+00:00
excerpt: 'The maximum width we can add on a div is&nbsp;100%. What if you want more than that ? The solution is not to add more percentage to div but to do something other. Adding white-space&nbsp;property.This will only work on the main parent elements such as ...'
url: /setting-infinite-width-on-an-element-html
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
  - http://sag-3.blogspot.com/2013/08/infinite-width-html-css.html
dsq_thread_id:
  - 1957095222
  - 1957095222
  - 1957095222
  - 1957095222
  - 1957095222
  - 1957095222
syndication_item_hash:
  - 25b4e3b56fe372497f2487d1894c7c36
  - 25b4e3b56fe372497f2487d1894c7c36
  - 25b4e3b56fe372497f2487d1894c7c36
  - 25b4e3b56fe372497f2487d1894c7c36
  - 25b4e3b56fe372497f2487d1894c7c36
  - 25b4e3b56fe372497f2487d1894c7c36
categories:
  - CSS
  - HTML
tags:
  - Infinity
  - Width

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  The maximum width we can add on a div is&nbsp;<b>100%</b>. What if you want more than that ? The solution is not to add more percentage to div but to do something other. Adding <b>white-space</b>&nbsp;property.<br />This will only work on the main parent elements such as elements with <b>display</b>&nbsp;property as follows:</p> 
  
  <ol style="text-align: left;">
    <li>
      block
    </li>
    <li>
      table
    </li>
    <li>
      inline-block
    </li>
  </ol>
  
  <p>
    So now you know what are the main parent elements. Now back to the point, to add infinite width set the <b>white-space</b>&nbsp;property to the value&nbsp;<b>nowrap</b>. Example:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      div{<br />&nbsp;white-space:nowrap;<br />}
    </p>
  </blockquote>
  
  <p>
    and put <b>display:inline-block</b>&nbsp;on all the children elements.<br />Now the div will get an infinite width. Enjoy !!!</div>