---
title: Move Cursor To End Of Input Or Textarea In jQuery
author: Subin Siby
type: post
date: 2014-01-31T16:03:21+00:00
url: /move-cursor-to-end-input-textarea-jquery
authorsure_include_css:
  - 
  - 
  - 
categories:
  - jQuery
tags:
  - Cursor
  - Text

---
The function that moves the cursor of an input element to the end in jQuery is **.focus()**. But sometimes, this won&#8217;t work even if you replace it with **[0].click()**. It&#8217;ll maybe work on input fields properly, but won&#8217;t properly at all in text areas. Mostly it won&#8217;t work, if you are calling the **.focus()** function inside an event listener function.

To solve this you can use 2 other methods if the first one don&#8217;t work. It&#8217;s pretty simple and I don&#8217;t know why it works. I found this solution on a Stack Overflow question and the one who answered doesn&#8217;t know why it works. Here is a list of the events that happens when an element is clicked :

  1. mousedown
  2. focus
  3. mouseup
  4. click

If you are calling mousedown on an element, you won&#8217;t be able to focus that element in it&#8217;s callback. So use mouseup or click for listening events to focus an element.

Let&#8217;s get back on the topic. As I said before, there are 3 solutions. If 1 didn&#8217;t work move on the Plan B (2) or Plan C (3).

## Solution 1

Focus before appending the value :

<pre class="prettyprint"><code>$("textarea").focus().val("How Did This Happen ?");</code></pre>

## Solution 2

Add value first, then focus and add value again (original solution by <a href="http://stackoverflow.com/a/1056406/1372424" target="_blank">Stack Overflow user</a>) :

<pre class="prettyprint"><code>$("textarea").val("Two Values").focus().val("Two Values");</code></pre>

## Solution 3

Focus, clear value, then revert to original value :

<pre class="prettyprint"><code>$("textarea").focus().val("").val("Original Value");</code></pre>

If you have any other methods to do this confusing task, please share it.