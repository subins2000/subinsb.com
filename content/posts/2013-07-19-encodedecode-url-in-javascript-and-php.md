---
title: Encode/Decode URL in Javascript and PHP
author: Subin Siby
type: post
date: 2013-07-19T03:30:00+00:00
excerpt: "URL&nbsp;encoding and decoding can be done in both Javascript&nbsp;and PHP. In this post I'm going to tell you the functions used in both languages for url encoding/decoding.Javascript&nbsp;EncodingencodeURIComponent(url);  DecodingdecodeURIComponent(u..."
url: /encodedecode-url-in-javascript-and-php
authorsure_include_css:
  - 
  - 
  - 
syndication_source:
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/encodedecode-url-in-javascript-and-php.html
  - http://sag-3.blogspot.com/2013/07/encodedecode-url-in-javascript-and-php.html
  - http://sag-3.blogspot.com/2013/07/encodedecode-url-in-javascript-and-php.html
  - http://sag-3.blogspot.com/2013/07/encodedecode-url-in-javascript-and-php.html
  - http://sag-3.blogspot.com/2013/07/encodedecode-url-in-javascript-and-php.html
  - http://sag-3.blogspot.com/2013/07/encodedecode-url-in-javascript-and-php.html
syndication_item_hash:
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
  - 8351e2f50b9aab899b6e08e0b3821d3e
blogger_permalink:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
blogger_blog:
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
categories:
  - PHP
tags:
  - Decode
  - Encode
  - URL

---
<div dir="ltr" style="text-align: left;">
  <b>URL</b> encoding and decoding can be done in both <b>Javascript</b> and <b>PHP</b>. In this post I&#8217;m going to tell you the functions used in both languages for url encoding/decoding.<br /> <b><span style="font-size: x-large;"><span style="text-decoration: underline;">Javascript</span></span></b><br /> <b><span style="font-size: large;"> Encoding</span></b></p> 
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; font-family: inherit; white-space: pre-wrap;">encodeURIComponent(url);</span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; white-space: pre-wrap;"><span style="font-family: inherit; font-size: large;"><b> Decoding</b></span></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; white-space: pre-wrap;">decodeURIComponent(url);</span>
    </p>
  </blockquote>
  
  <p>
    <b><span style="font-size: x-large;"><span style="text-decoration: underline;">PHP</span></span></b><br /> <b><span style="font-size: large;"> Encoding</span></b>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; font-family: inherit; white-space: pre-wrap;">urlencode(url);</span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; white-space: pre-wrap;"><span style="font-family: inherit; font-size: large;"><b> Decoding</b></span></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; white-space: pre-wrap;">urldecode(url);</span>
    </p>
  </blockquote>
  
  <p>
    <span style="white-space: pre-wrap;">There is<b> </b>an <b>encodeURI</b> and <b>decodeURI</b> function in <b>Javascript</b> but it won&#8217;t encode/decode the url. Note that the the word used in <b>Javascript</b> is <b>URI</b> not <b>URL</b>. But in <b>PHP</b> both are <b>URL</b>. Don&#8217;t mistake the word.</span>
  </p>
</div>