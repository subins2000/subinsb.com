---
title: Ubuntu Dekko
author: Subin Siby
type: post
date: 2015-12-22T20:11:52+00:00
url: /ubuntu-dekko
categories:
  - Ubuntu

---
This post is part of the Google Code in 2015. The task was to Build the dekko app and run all testsuites. You can see the task <a href="https://codein.withgoogle.com/dashboard/task-instances/6201826592423936/" target="_blank">here</a>.


## My System

Xubuntu 15.10 Wily Werewolf :<figure class="wp-caption aligncenter">

[<img class="" src="https://scontent.fmaa1-1.fna.fbcdn.net/hphotos-xat1/t31.0-8/12322860_916853241741348_2794994122051128816_o.jpg" alt="" width="1368" height="768" />][2]<figcaption class="wp-caption-text">Xubuntu 15.10 Wily Werewolf</figcaption></figure> 

## Dependencies Installation

First of all, I downloaded Ubuntu SDK with the help of <a href="https://developer.ubuntu.com/en/start/ubuntu-sdk/installing-the-sdk/" target="_blank">this guide</a>.

Then, I got the source code URL from <a href="https://developer.ubuntu.com/en/community/core-apps/dekko/" target="_blank">Ubuntu Developers Site</a> :

    git clone https://git.launchpad.net/dekko

The source code was about 29 MB.

Next thing I did was read "HACKING.md" file. There was a section called "Building" in it. There were two steps :

  1. ensure we have the armhf click chroot created
  2. the click chroot is up to date and has required dependencies installed

The first step was not clear. So, I went to the "<a href="https://git.launchpad.net/dekko/tree/README.md" target="_blank">README.md</a>" file and found that there is a group for <a href="https://telegram.me/joinchat/AyyC-QIH4bXHjpRXpqWWkQ" target="_blank">Dekko in Telegram</a>.

I chatted and found <a href="https://dekko.gitbooks.io/development-guide/content/SetupDevEnv.html" target="_blank">this page</a>. Thanks to Dan Chapman. I also learnt that the "HACKING.md" file is no longer valid.

To build, the doc said :

<pre class="prettyprint"><code>If you just want to build & run Dekko on your desktop then you should use the run-desktop script with the --setup option.
</code></pre>

<pre class="prettyprint prettyprinted"><code class="lang-bash">&lt;span class="pln">$ &lt;/span>&lt;span class="pun">./&lt;/span>&lt;span class="pln">scripts&lt;/span>&lt;span class="pun">/&lt;/span>&lt;span class="pln">run&lt;/span>&lt;span class="pun">-&lt;/span>&lt;span class="pln">desktop &lt;/span>&lt;span class="pun">--&lt;/span>&lt;span class="pln">setup&lt;/span></code></pre>

**224 MB** of archives need to be retrieved. It were the dependencies. That took a while :<figure class="wp-caption alignnone">

<img class="" src="https://i.imgur.com/vHZMM8J.png" alt="" width="964" height="410" /><figcaption class="wp-caption-text">Installing dependencies of Dekko</figcaption></figure> 

The dependencies were successfully installed in a single try.

## Building

I ran :

<pre class="prettyprint prettyprinted"><code class="lang-bash">&lt;span class="pun">./&lt;/span>&lt;span class="pln">scripts&lt;/span>&lt;span class="pun">/&lt;/span>&lt;span class="pln">run&lt;/span>&lt;span class="pun">-&lt;/span>&lt;span class="pln">desktop &lt;/span>&lt;span class="pun">--&lt;/span>&lt;span class="pln">with&lt;/span>&lt;span class="pun">-&lt;/span>&lt;span class="pln">uitk&lt;/span></code></pre>

It started to build :<figure class="wp-caption alignnone">

<img class="" src="//imgur.com/NfHfJEpl.png" alt="" width="640" height="272" /><figcaption class="wp-caption-text">Bulding Dekko</figcaption></figure> 

There was this awesome little loading icon using ASCII chars to indicate the build process :

<pre class="prettyprint"><code>/ - | \ | /
</code></pre>

Never saw such thing before. I should make a SVG animated image of this.

The building caused and error :

<pre class="prettyprint"><code>E: You must put some 'source' URIs in your sources.list</code></pre>

I looked over the script file and found why the error is coming. It was because I have disabled all the "source code" repositories.

So, I reloaded the package list and tried again. This time it worked. After that, I ran :

&nbsp;

<pre class="prettyprint"><code>&lt;span class="pun">./&lt;/span>&lt;span class="pln">scripts&lt;/span>&lt;span class="pun">/&lt;/span>&lt;span class="pln">run&lt;/span>&lt;span class="pun">-&lt;/span>&lt;span class="pln">desktop&lt;/span></code></pre>

&nbsp;

&nbsp;

to run Dekko app. Now, it was compiling :<figure class="wp-caption alignnone">

<img class="" src="//imgur.com/OGEivOtl.png" alt="" width="640" height="357" /><figcaption class="wp-caption-text">Dekko app compiling</figcaption></figure> 

And finally Dekko ran :<figure class="wp-caption alignnone">

<img class="" src="//imgur.com/mR2MBTLl.png" alt="" width="640" height="359" /><figcaption class="wp-caption-text">Dekko on Ubuntu Wily Werewolf</figcaption></figure> 

## Running Testsuites

As per the doc, I ran this :<code class="lang-bash"></code>

<pre class="prettyprint prettyprinted"><code class="lang-bash">&lt;span class="pun">./&lt;/span>&lt;span class="pln">scripts&lt;/span>&lt;span class="pun">/&lt;/span>&lt;span class="pln">run&lt;/span>&lt;span class="pun">-&lt;/span>&lt;span class="pln">desktop &lt;/span>&lt;span class="pun">--&lt;/span>&lt;span class="pln">tests&lt;/span></code></pre>

The tests were started :<figure class="wp-caption alignnone">

<img class="" src="//imgur.com/q1RqZb2l.png" alt="" width="640" height="357" /><figcaption class="wp-caption-text">Running Dekko Testsuites</figcaption></figure> 

The testsuites was successfully finished :<figure class="wp-caption alignnone">

<img class="" src="//imgur.com/vk82mN0l.png" alt="" width="640" height="357" /><figcaption class="wp-caption-text">Dekko Testuites Passed</figcaption></figure> 

## Problems Faced

  * The file "HACKING.md" was outdated in the Git repo. I had to find the better documented page from Dan Chapman &#8211; maintainer of Dekko
  * <pre class="prettyprint prettyprinted"><code class="lang-bash">&lt;span class="pun">./&lt;/span>&lt;span class="pln">scripts&lt;/span>&lt;span class="pun">/&lt;/span>&lt;span class="pln">run&lt;/span>&lt;span class="pun">-&lt;/span>&lt;span class="pln">desktop &lt;/span>&lt;span class="pun">--&lt;/span>&lt;span class="pln">with&lt;/span>&lt;span class="pun">-&lt;/span>&lt;span class="pln">uitk&lt;/span></code></pre>
    
    The requirement of  the above command to work is that the "Source Code" repository should be enabled.
  
    I had them disabled and it didn&#8217;t work on my system. Everything worked after it was enabled and when package list was updated</li> </ul> 
    
    ## Suggestions
    
      * The "HACKING.md" file should be removed or updated
  
        Dan Chapman said that he was planning to remove it. So, that&#8217;s OK
      * The <a href="https://dekko.gitbooks.io/development-guide/content/SetupDevEnv.html" target="_blank">doc</a> should mention that the "Source Code" repository should be enabled for the building to be done successfully.
    
    ## Conclusion
    
    Dekko is neat, beautiful and well written. I could only find Dan Chapman active in the project. Still, the Dekko <a href="https://dekko.gitbooks.io/" target="_blank">doc site</a> and project looks awesome.
    
    Can&#8217;t wait to get an Ubuntu Touch Phone.

 [1]: #my-system
 [2]: https://scontent.fmaa1-1.fna.fbcdn.net/hphotos-xat1/t31.0-8/12322860_916853241741348_2794994122051128816_o.jpg
 [3]: #dependencies-installation
 [4]: #building
 [5]: #running-testsuites
 [6]: #problems-faced
 [7]: #suggestions
 [8]: #conclusion
