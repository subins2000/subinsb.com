---
title: 'How To Add Back jQuery .live() & .die() Function'
author: Subin Siby
type: post
date: 2013-11-24T12:31:47+00:00
url: /how-to-add-back-jquery-live-die-function
authorsure_include_css:
  - 
  - 
  - 
categories:
  - JavaScript
  - jQuery

---
As you may know, **jQuery** removed **.live()** function and instead they told to use **.on()** function. But I didn&#8217;t like this because **.on()** needs up more parameters than **.live()** and to handle a **click** function you have to add an event to the **document**, instead of the element. A normal **live** **click event** code is like this

<pre class="prettyprint"><code>$("#element").live("click",function(){
  dosomething();
});</code></pre>

When using **.on()** to do the same task, the code will be like this :

<pre class="prettyprint"><code>$(document).on("click","#element",function(){
  dosomething();
});</code></pre>

Notice the big difference. Using **.on()** event have a lot more code than **.live()** and it&#8217;s not easy to use. It&#8217;s not comfortable. The motto of **jQuery** is **Write Less, Do More**. In the case of **.on()** the motto doesn&#8217;t apply. So we need **.live()** back. **jQuery** won&#8217;t bring it back. Hence we should take things on our own.

In this post I&#8217;m going to tell you **How To Bring Back .live() function**. It&#8217;s pretty simple. All you have to do is add some lines at a place of your **jQuery** file.

Open your **jQuery** source file and search for :

<pre class="prettyprint"><code>unbind:</code></pre>

You will get a code something like this :

<pre class="prettyprint"><code>unbind: function( types, fn ) {
  return this.off( types, null, fn );
},</code></pre>

If it&#8217;s a **jQuery** minified version, you will get something like this :

<pre class="prettyprint"><code>unbind:function(a,b){return this.off(a,null,b)},</code></pre>

Now add the following code just after the comma (**,**) of **unbind **:

<pre class="prettyprint"><code>live:function(types,data,fn){jQuery(document).on(types,this.selector,data,fn);return this;},die:function(types,data,fn){jQuery(document).off(types,this.selector);return this;},</code></pre>

And the whole code will be :

<pre class="prettyprint"><code>unbind:function(a,b){return this.off(a,null,b)},live:function(types,data,fn){jQuery(document).on(types,this.selector,data,fn);return this;},die:function(types,data,fn){jQuery(document).off(types,this.selector);return this;},</code></pre>

And on the unminified version :

<pre class="prettyprint"><code>unbind: function( types, fn ) {
  return this.off( types, null, fn );
},live:function(types,data,fn){jQuery(document).on(types,this.selector,data,fn);return this;},die:function(types,data,fn){jQuery(document).off(types,this.selector);return this;},</code></pre>

Now, try using **.live()** & **.die()** functions and it will work ! If you have any problems /suggestions /feedback, don&#8217;t hesitate to say it in the comments.