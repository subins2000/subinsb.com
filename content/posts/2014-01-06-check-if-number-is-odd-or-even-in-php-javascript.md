---
title: 'Check If a Number is Odd Or Even In PHP & JavaScript'
author: Subin Siby
type: post
date: 2014-01-06T14:42:52+00:00
url: /check-if-number-is-odd-or-even-in-php-javascript
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - JavaScript
  - PHP
tags:
  - Numbers

---
Odd numbers and even numbers are used in programming languages. To check if a number is even or odd needs brilliant code. In this post, you will be introduced to **checkNum()** function in both **PHP** and **JavaScript**. This functions will return **even** for even numbers and **odd** for odd numbers.


# Parameters

$num &#8211; An integer (PHP)

num   &#8211; An integer (JS)

# Returns

"odd" for odd number, "even" for even number.

# Function

## JavaScript

> function&nbsp;checkNum(num){
  
> &nbsp;if(num&nbsp;%&nbsp;2){
  
> &nbsp;&nbsp;return&nbsp;"even";
  
> &nbsp;}else{
  
> &nbsp;&nbsp;return&nbsp;"odd";
  
> &nbsp;}
  
> }

## PHP

> function&nbsp;checkNum($num){
  
> &nbsp;if($num&nbsp;%&nbsp;2){
  
> &nbsp;&nbsp;return&nbsp;"even";
  
> &nbsp;}else{
  
> &nbsp;&nbsp;return&nbsp;"odd";
  
> &nbsp;}
  
> }

 [1]: #parameters
 [2]: #returns
 [3]: #function
 [4]: #javascript
 [5]: #php
