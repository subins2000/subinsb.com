---
title: Toggle Element visibility using a little piece of code – Jquery Toggle
author: Subin Siby
type: post
date: 2012-10-20T16:00:00+00:00
excerpt: 'You might wanted to Show/Hide&nbsp;element based on the visibility of the element. If the visibility is block&nbsp; ,&nbsp;hide the element and if visibility is none,&nbsp;show the element.You can do this without the&nbsp;if statement. You can do this ...'
url: /toggle-element-visibility-using-a-little-piece-of-code-jquery-toggle
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - ca44f5e3b79f112cf7c401e8948c7171
  - ca44f5e3b79f112cf7c401e8948c7171
  - ca44f5e3b79f112cf7c401e8948c7171
  - ca44f5e3b79f112cf7c401e8948c7171
  - ca44f5e3b79f112cf7c401e8948c7171
  - ca44f5e3b79f112cf7c401e8948c7171
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
  - http://sag-3.blogspot.com/2012/10/jquery-toggle.html
categories:
  - CSS
  - HTML
  - jQuery

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You might wanted to <b>Show/Hide</b>&nbsp;element based on the visibility of the element. If the visibility is <b>block</b>&nbsp; ,&nbsp;<b>hide </b>the element and if visibility is <b>none</b>,&nbsp;<b>show</b> the element.<br />You can do this without the<b>&nbsp;if </b>statement. You can do this in just a line of code.<br />For example : if we want to toggle the visibility of a <b>div</b>&nbsp;with the id <b>sh</b>.</p> 
  
  <blockquote class="tr_bq">
    <p>
      $("#sh").toggle();
    </p>
  </blockquote>
  
  <p>
    If the <b>div</b>&nbsp;<b>#sh</b>&nbsp;is <b>hidden</b> the code above code will make it <b>visible</b> and if it is <b>visible</b>&nbsp;the code will <b>hide</b>&nbsp;the <b>div #sh</b>.</div>