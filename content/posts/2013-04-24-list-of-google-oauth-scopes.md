---
title: List of Google OAuth Scopes
author: Subin Siby
type: post
date: 2013-04-24T07:45:00+00:00
excerpt: 'Here is the list of Google OAuth Scopes. It can be used for different purposes.     Adsense Management     https://www.googleapis.com/auth/adsense      Google Affitrate Network     https://www.googleapis.com/auth/gan      Analytics     https://www.goog...'
url: /list-of-google-oauth-scopes
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - d6234c63c89037749e7ccc0afaa0ec13
  - d6234c63c89037749e7ccc0afaa0ec13
  - d6234c63c89037749e7ccc0afaa0ec13
  - d6234c63c89037749e7ccc0afaa0ec13
  - d6234c63c89037749e7ccc0afaa0ec13
  - d6234c63c89037749e7ccc0afaa0ec13
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
blogger_permalink:
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
  - http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
dsq_thread_id:
  - 1957159122
  - 1957159122
  - 1957159122
  - 1957159122
  - 1957159122
  - 1957159122
tags:
  - Google
  - OAuth

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div dir="ltr" style="text-align: left;" trbidi="on">
    Here is the list of <b>Google OAuth</b> Scopes. It can be used for different purposes.</p>
  </div>
  
  <div class="shadow" style="opacity: 0;">
  </div>
  
  <table border="1" style="line-height: 25px;">
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/adsense">
      <td style="margin-right: 20px;">
        Adsense Management
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/adsense
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/gan">
      <td style="margin-right: 20px;">
        Google Affitrate Network
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/gan
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/analytics.readonly">
      <td style="margin-right: 20px;">
        Analytics
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/analytics.readonly
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/books">
      <td style="margin-right: 20px;">
        Google Books
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/books
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/blogger">
      <td style="margin-right: 20px;">
        Blogger
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/blogger
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/calendar">
      <td style="margin-right: 20px;">
        Calendar
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/calendar
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/devstorage.read_write">
      <td style="margin-right: 20px;">
        Google Cloud Storage
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/devstorage.read_write
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.google.com/m8/feeds/">
      <td style="margin-right: 20px;">
        Contacts
      </td>
      
      <td class="secondary">
        https://www.google.com/m8/feeds/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/structuredcontent">
      <td style="margin-right: 20px;">
        Content API for Shopping
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/structuredcontent
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/chromewebstore.readonly">
      <td style="margin-right: 20px;">
        Chrome Web Store
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/chromewebstore.readonly
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://docs.google.com/feeds/">
      <td style="margin-right: 20px;">
        Documents trst
      </td>
      
      <td class="secondary">
        https://docs.google.com/feeds/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/drive">
      <td style="margin-right: 20px;">
        Google Drive
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/drive
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/drive.file">
      <td style="margin-right: 20px;">
        Google Drive Files
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/drive.file
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://mail.google.com/mail/feed/atom">
      <td style="margin-right: 20px;">
        Gmail
      </td>
      
      <td class="secondary">
        https://mail.google.com/mail/feed/atom
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/plus.me">
      <td style="margin-right: 20px;">
        Google+
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/plus.me
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/plus.me">
      <td style="margin-right: 20px;">
        Google+ Friend list
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/plus.login
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://apps-apis.google.com/a/feeds/groups/">
      <td style="margin-right: 20px;">
        Groups Provisioning
      </td>
      
      <td class="secondary">
        https://apps-apis.google.com/a/feeds/groups/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/latitude.all.best https://www.googleapis.com/auth/latitude.all.city">
      <td style="margin-right: 20px;">
        Google Latitude
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/latitude.all.best https://www.googleapis.com/auth/latitude.all.city
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/moderator">
      <td style="margin-right: 20px;">
        Moderator
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/moderator
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://apps-apis.google.com/a/feeds/atras/">
      <td style="margin-right: 20px;">
        Nicknames Provisioning
      </td>
      
      <td class="secondary">
        https://apps-apis.google.com/a/feeds/atras/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/orkut">
      <td style="margin-right: 20px;">
        Orkut
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/orkut
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://picasaweb.google.com/data/">
      <td style="margin-right: 20px;">
        Picasa Web
      </td>
      
      <td class="secondary">
        https://picasaweb.google.com/data/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://sites.google.com/feeds/">
      <td style="margin-right: 20px;">
        Sites
      </td>
      
      <td class="secondary">
        https://sites.google.com/feeds/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://spreadsheets.google.com/feeds/">
      <td style="margin-right: 20px;">
        Spreadsheets
      </td>
      
      <td class="secondary">
        https://spreadsheets.google.com/feeds/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/tasks">
      <td style="margin-right: 20px;">
        Tasks
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/tasks
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/urlshortener">
      <td style="margin-right: 20px;">
        URL Shortener
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/urlshortener
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/userinfo.email">
      <td style="margin-right: 20px;">
        Userinfo &#8211; Email
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/userinfo.email
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.googleapis.com/auth/userinfo.profile">
      <td style="margin-right: 20px;">
        Userinfo &#8211; Profile
      </td>
      
      <td class="secondary">
        https://www.googleapis.com/auth/userinfo.profile
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://apps-apis.google.com/a/feeds/user/">
      <td style="margin-right: 20px;">
        User Provisioning
      </td>
      
      <td class="secondary">
        https://apps-apis.google.com/a/feeds/user/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://www.google.com/webmasters/tools/feeds/">
      <td style="margin-right: 20px;">
        Webmaster Tools
      </td>
      
      <td class="secondary">
        https://www.google.com/webmasters/tools/feeds/
      </td>
    </tr>
    
    <tr class="op-menutrstitem op-menuchecktrstitem multiple" val="https://gdata.youtube.com">
      <td style="margin-right: 20px;">
        YouTube
      </td>
      
      <td class="secondary">
        https://gdata.youtube.com
      </td>
    </tr>
  </table>
  
  <p>
    <span style="font-size: large;">See <a href="http://sag-3.blogspot.com/2013/04/login-with-google-oauth-without-using.html" >this post</a> to know how to login/signup your users without using&nbsp;<b>Google OAuth PHP</b>&nbsp;Library.</span></div>