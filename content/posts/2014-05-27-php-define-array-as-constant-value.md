---
title: Define Array as Value of Constants in PHP
author: Subin Siby
type: post
date: 2014-05-27T15:41:09+00:00
url: /php-define-array-as-constant-value
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - Array
  - echo
  - Serialize

---
Constants are something that is needed for every computer languages. In **PHP**, too you can set constants. But you can only set **string** values which is a pity, because if you want to set arrays for making it easy for obtaining multiple values, you can&#8217;t do it. In this case, we explore the unimaginable possibilities of programming languages. As like any other language, PHP too has a secret weapon to accomplish this task.


## Long Things Short

We&#8217;ll use PHP&#8217;s **serialize** function to make an **Array** look like string and this string is defined as the constant value. For making the string back to the original Array, we&#8217;ll use **unserialize** function. It&#8217;s very easy and works in both **PHP 4 **and **PHP 5**.

## The Array

We make the array first. Here&#8217;s the array containing some sample values :

<pre class="prettyprint"><code>$array = array(
 "Subin" =&gt; "Siby",
 "John" =&gt; "Resig",
 "Larry" =&gt; "Page",
 "Coding" =&gt; "Horror"
);</code></pre>

As you can see, the index is the first name of persons and the values are the last name of the persons.

## <a href="http://in3.php.net/manual/en/function.serialize.php" target="_blank">Serialize</a>

Now, we serialize the Array and put it in to a variable :

<pre class="prettyprint"><code>$serialized = serialize($array);</code></pre>

The above variable **$serialized** will now contain a string something like this :

<pre class="prettyprint"><code>a:4:{s:5:"Subin";s:4:"Siby";s:4:"John";s:5:"Resig";s:5:"Larry";s:4:"Page";s:6:"Coding";s:6:"Horror";}</code></pre>

You can see that the index and values of the array are in the above serialized output. We&#8217;ll make this as the value to be set for constant.

## Set Constant

We define the constant :

<pre class="prettyprint"><code>define("MyArray", $serialized);</code></pre>

Now, you can access the string from anywhere, just like this :

<pre class="prettyprint"><code>echo MyArray;</code></pre>

## Get back the Array

To get back the array, we unserialize the string and an array value is returned :

<pre class="prettyprint"><code>$BackArray = unserialize(MyArray);</code></pre>

**unserialize()** will return values according to the value first serialized. So, if you serialize a string the return value of the unserialize function will be a string. This applies to all cases like Arrays, booleans, integers etc&#8230; In this case, we can directly get values from the array now :

<pre class="prettyprint"><code>echo $BackArray["Larry"]; // Prints Page</code></pre>

And now you know how setting Array as Constant values work. Hope you understood the procedure. If you have a problem with this, you can ask by the comments or <a href="http://stackoverflow.com" target="_blank">SO</a>.

 [1]: #long-things-short
 [2]: #the-array
 [3]: #serialize
 [4]: #set-constant
 [5]: #get-back-the-array
