---
title: 'SPAM URL’s in Blogger Traffic Source Stats : What should I do ?'
author: Subin Siby
type: post
date: 2013-04-28T14:34:00+00:00
excerpt: "You might have noticed a great volume of sites you never heard of coming to your site apart from Search Engine URL's. Some of those URL's&nbsp;are filmhill.com&nbsp;and vampirestat.com. These kind of URL's are SPAM. The SPAM&nbsp;site is referring your..."
url: /spam-urls-in-blogger-traffic-source-stats-what-should-i-do
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - bebf8fca238679ac051581bcb172fa5e
  - bebf8fca238679ac051581bcb172fa5e
  - bebf8fca238679ac051581bcb172fa5e
  - bebf8fca238679ac051581bcb172fa5e
  - bebf8fca238679ac051581bcb172fa5e
  - bebf8fca238679ac051581bcb172fa5e
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
blogger_permalink:
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
  - http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html
dsq_thread_id:
  - 1592505495
  - 1592505495
  - 1592505495
  - 1592505495
  - 1592505495
  - 1592505495
categories:
  - Blogger
  - PHP
tags:
  - BOT
  - SPAM
  - Stats
  - Traffic
  - URL

---
<div dir="ltr" style="text-align: left;">
  <div dir="ltr" style="text-align: left;">
    <span style="font-family: inherit;">You might have noticed a great volume of sites you never heard of coming to your site apart from Search Engine URL&#8217;s. Some of those <b>URL&#8217;s</b> are <b>filmhill.com</b> and <b>vampirestat.com</b>.</span><br /> <span style="font-family: inherit;"><br /> </span> <span style="font-family: inherit;">These kind of <b>URL</b>&#8216;s are <b>SPAM</b>. The SPAM site is referring your site but they are not persons, they&#8217;re <b>BOTS</b>. These bots are visiting your site just for fun. These bots don&#8217;t read your posts or anything. They&#8217;re just a pile of robots. The bot is<b> </b>ran by a script. Probably <b>PHP</b>.</span><br /> <span style="font-family: inherit;">This scripts visits your site using CURL function with the referrer of their site. In PHP if you echo $_SERVER[&#8216;HTTP_REFERER&#8217;] you will get the site where the user just came from. <b>Blogger</b> is using the same function but not PHP to record the traffic sources.</span><span style="font-family: inherit;"><br /> </span></p> 
    
    <h1>
      What&#8217;s the benefit for my blog and the SPAM site ?
    </h1>
    
    <p>
      <span style="font-family: inherit;"><b>My Blog </b>: Your stats will rise up. Even though the visitor is a <b>bot,</b> <b>Blogger</b> will still count it as a visitor. The human viewers will see the <b>rise up in your blog&#8217;s stats</b> making them believe that your blog is a popular blog.</span><br /> <span style="font-family: inherit;"><br /> </span> <span style="font-family: inherit;"><b>SPAM</b> <b>Site</b> : The site do this because they want to promote their site to you. When the blog owner sees this traffic source, they will click the link to check it out. Hence the SPAM site&#8217;s stats will also rise and more advertisers will come to advertise on the SPAM site.</span><span style="font-family: inherit;"><br /> </span>
    </p>
    
    <h1>
      What&#8217;s the disadvantage ?
    </h1>
    
    <p>
      <b>Ad</b> <b>Networks</b> such as <b>Google Adsense</b> and <b>BuySellAds</b> tracks all the visits coming to your blog. When they sees the <b>SPAM</b> URL&#8217;s visiting, they count it as a <b>paid-to-click</b> visit. According to the <b>Adsense</b> Policies, You shouldn&#8217;t use a <b>Pay To Click</b> service. Hence your Adsense Account will get <b>disapproved</b>.<span style="font-family: inherit;"><br /> </span>
    </p>
    
    <h1>
      What should I do ?
    </h1>
    
    <p>
      <span style="font-family: inherit;">You can&#8217;t do anything to stop these traffic sources, Only Blogger can. But you can do two things : </span>
    </p>
    
    <ol>
      <li>
        Don&#8217;t click on these kind of URL&#8217;s
      </li>
      <li>
        Don&#8217;t add those URL&#8217;s to your blog pages.
      </li>
    </ol>
    
    <p>
      <span style="font-family: inherit;">If you link the SPAM URL&#8217;s they will keep coming to your blog.</span>
    </p>
    
    <p>
      <span style="font-family: inherit;"><b><br /> </b></span> <span style="font-family: inherit;">Here is a list of <b>SPAM</b> sites found in Blogger Traffic Sources.<b></b></span>
    </p>
  </div>
  
  <table style="line-height: 25px;" border="1">
    <tr>
      <td>
        http://www.omgmachines.com/?hop=gerbill3
      </td>
    </tr>
    
    <tr>
      <td>
        http://gsniper2.com/?hop=gerbill3
      </td>
    </tr>
    
    <tr>
      <td>
        http://0rz.tw/orp80
      </td>
    </tr>
    
    <tr>
      <td>
        http://t.co/6kSaxKTT
      </td>
    </tr>
    
    <tr>
      <td>
        http://www3.t-8jy1o8pij.trickip.net/
      </td>
    </tr>
    
    <tr>
      <td>
        http://6dd260ed.urlbeat.net
      </td>
    </tr>
    
    <tr>
      <td>
        http://adsenserrors.info
      </td>
    </tr>
    
    <tr>
      <td>
        http://software.refererx.com/
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.elebda3.net/redirect.php?url=t.co/TpCBMDg9
      </td>
    </tr>
    
    <tr>
      <td>
        http://zombiestat.com/
      </td>
    </tr>
    
    <tr>
      <td>
        http://uglystat.com/
      </td>
    </tr>
    
    <tr>
      <td>
        http://adsensewatchdog.com
      </td>
    </tr>
    
    <tr>
      <td>
        http://adsresultpages.com/?aff=5801&saff=1&source=1&q=buy+viagra
      </td>
    </tr>
    
    <tr>
      <td>
        http://bit.ly/FreeDownloadMoneyMakingPackage
      </td>
    </tr>
    
    <tr>
      <td>
        http://tinyurl.com/EarnMoneyFromADFLY
      </td>
    </tr>
    
    <tr>
      <td>
        http://myhealthscore.com/frame.cfm?page=http%3a%2f%2ft.co%2fz1v6ZazZ
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.stumbleupon.com/su/2gA2gc/t.co/TIlBBraD
      </td>
    </tr>
    
    <tr>
      <td>
        http://tr.netlog.com/go/out/url=http%3a%2f%2ft.co%2fcM5qnlnl?kjKJHl8HKd23
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.filmhill.com/redirect.php?url=http%3A%2F%2Ft.co%2F5UQiESCF
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.filmhill.com/redirect.php?url=http://flf-course.com?a_aid=510d2acc92117&a_bid=6f93443e
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.filmhill.com
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.vampirestat.com
      </td>
    </tr>
    
    <tr>
      <td>
        http://yandex.ru/yandsearch?
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.meendo.com/?partner=2631
      </td>
    </tr>
    
    <tr>
      <td>
        http://pregolom.com/find/in/index.php
      </td>
    </tr>
    
    <tr>
      <td>
        http://super-online-search.com/find/in/index.php
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.elebda3.net/redirect.php?url=t.co/TpCBMDg9
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.youtube.com/watch?v=s4p9PjdqHN4
      </td>
    </tr>
    
    <tr>
      <td>
        http://bytovaya-tehnika.blogspot.ru/ &#8211; Dec. 2012
      </td>
    </tr>
    
    <tr>
      <td>
        http://electronika-tech.blogspot.ru/ &#8211; Dec. 2012
      </td>
    </tr>
    
    <tr>
      <td>
        http://obo-vsem-blog.blogspot.ru/ &#8211; Dec. 2012
      </td>
    </tr>
    
    <tr>
      <td>
        http://lshirlene.github.com/166.html
      </td>
    </tr>
    
    <tr>
      <td>
        http://thesecrettool.blogspot.com
      </td>
    </tr>
    
    <tr>
      <td>
        http://kechinf.phpnet.us
      </td>
    </tr>
    
    <tr>
      <td>
        http://members.multimania.co.uk
      </td>
    </tr>
    
    <tr>
      <td>
        http://olshopu.netai.net
      </td>
    </tr>
    
    <tr>
      <td>
        http://flf-course.com/?a_aid=517d0f042c205
      </td>
    </tr>
    
    <tr>
      <td>
        http://r-e-f-e-r-e-r.com/
      </td>
    </tr>
    
    <tr>
      <td>
        http://r-e-f-e-r-r-e-r.com/
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.linkedin.com/redir/redirect?url=http%3A%2F%2Fflf-course%2Ecom%3Fa_aid%3D517d0f042c205&urlhash=e75j5
      </td>
    </tr>
    
    <tr>
      <td>
        http://t.co/MaAptuGFVu6
      </td>
    </tr>
    
    <tr>
      <td>
        http://www.kmzackblogger.com
      </td>
    </tr>
    
    <tr>
      <td>
        http://kallery.net/
      </td>
    </tr>
    
    <tr>
      <td>
        http://7secretsearch.com/
      </td>
    </tr>
  </table>
  
  <p>
    <span style="font-family: inherit;">Thank you <span class="publisher-anchor-color" style="box-sizing: border-box; line-height: 17px;">Rahul Saitand</span><span style="line-height: 17px;"> & </span>Brittny for reporting some <b>SPAM</b> <b>URL&#8217;s</b>.</span><br /> <span style="font-family: inherit;"><b><br /> </b></span><span style="color: red;">If you have any other URL&#8217;s than this, please report it via comments.</span>
  </p>
</div>