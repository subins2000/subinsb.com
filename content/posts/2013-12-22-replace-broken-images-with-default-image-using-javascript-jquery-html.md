---
title: Replace Broken Images With Default Image Using JavaScript OR jQuery OR HTML
author: Subin Siby
type: post
date: 2013-12-22T08:22:59+00:00
url: /replace-broken-images-with-default-image-using-javascript-jquery-html
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - HTML
  - JavaScript
  - jQuery
tags:
  - Image

---
Images are common in web pages now a days. These images make the web page attractive and beautiful. But sometimes due to problems, some images won&#8217;t load completely. When the image doesn&#8217;t load completely, that image is said to be a **broken image**. When the image is broken, the **Browser** will replace that broken image with an ugly image or replace the broken image with a blank space. I have included a broken image below. Different **Browsers** will render the broken image in different ways.
  
[<img class="aligncenter" alt="" src="http://open.subinsb.com/a-broken-image" />][1] The replaced image by browsers look ugly and the page loses it&#8217;s beauty.


# What Causes a Broken Image ?

  1. Slow Internet Connection makes the image broken
  2. Image Does not exist (404)
  3. Image URL redirects to a non Image URL
  4. User stops the loading of the page.

# Fix # 1

Using **JavaScript**,we can easily replace the image with a default image. In **Fix #** **1**, we will add **onerror** attribute tothe image element :

> <img src="//subinsb.com/to-an-image" onerror="this.src=&#8217;http://subinsb.com/path-to-default-image&#8217;;"/>

If you have a lot of images and need to apply an **onerror** attribute on each of them, it&#8217;s very difficult. If you want something like that, see **Fix # 2**.

# Fix # 2

The following code will add an **onerror** function on all images of a web page. It&#8217;s pure **JavaScript**.

> (function(){
  
> for(i=0;i<nodes.length;i++){
  
> nodes[i].onerror=nodes[i].src=&#8217;http://subinsb.com/path-to-default-image&#8217;;
  
> }
  
> })();

If you really need to do the task in **jQuery**, see **Fix # 3**.

# Fix # 3

The following **jQuery** code will do the same function as **Fix # 2**.

> $(document).ready(function(){
  
> $("img").each(function(){
  
> $(this).attr("onerror","this.src=&#8217;http://subinsb.com/path-to-default-image'");
  
> });
  
> });

In all of the fixes above, the path of the default image is :

> http://subinsb.com/path-to-default-image

 [1]: http://open.subinsb.com/a-broken-image
 [2]: #what-causes-a-broken-image
 [3]: #fix-1
 [4]: #fix-2
 [5]: #fix-3
