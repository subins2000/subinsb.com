---
title: How To Use Hard Disk As RAM In Ubuntu Linux
author: Subin Siby
type: post
date: 2013-09-22T15:32:00+00:00
excerpt: "I heard that it is possible to use a portion of hard disk as RAM&nbsp;in Windows. I wondered if this trick is available for Ubuntu, so I googled about it and couldn't find anything related to it. But I found out an AskUbuntu&nbsp;answer of using USB&nb..."
url: /how-to-use-hard-disk-as-ram-in-ubuntu-linux
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1962886075
  - 1962886075
  - 1962886075
  - 1962886075
  - 1962886075
  - 1962886075
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/09/use-hard-disk-as-ram-in-ubuntu-linux.html
syndication_item_hash:
  - cf54fdccf07b9b65258ffbb0b85fad5c
  - cf54fdccf07b9b65258ffbb0b85fad5c
  - cf54fdccf07b9b65258ffbb0b85fad5c
  - cf54fdccf07b9b65258ffbb0b85fad5c
  - cf54fdccf07b9b65258ffbb0b85fad5c
  - cf54fdccf07b9b65258ffbb0b85fad5c
categories:
  - Linux
  - Ubuntu
  - Windows
tags:
  - Hard Disk
  - Memory
  - RAM
  - Terminal
  - Tip
  - Trick

---
<div dir="ltr" style="text-align: left;">
  I heard that it is possible to use a portion of hard disk as <b>RAM</b> in <b>Windows</b>. I wondered if this trick is available for <b>Ubuntu</b>, so I googled about it and couldn&#8217;t find anything related to it. But I found out an <b>AskUbuntu</b> answer of using <b>USB</b> sticks as <b>RAM</b>. I did the same trick on <b>Hard Disk</b> and it works !!<br /> This trick can be accomplished with the use of some small commands in <b>Terminal</b>. The <b>RAM</b> memory increase can&#8217;t be noted in the <b>System</b> <b>Monitor</b> application.So, Let&#8217;s begin.<br /> Create a file of <b>512</b> <b>MB</b> (The <b>512</b> indicates the <b>RAM</b> memory to be added):</p> 
  
  <pre class="prettyprint"><code>dd if=/dev/zero of=~/subinsblog bs=4096 count=&lt;span style="background-color: yellow;">131072&lt;/span></code></pre>
  
  <p>
    The <span style="background-color: yellow;">yellow background text</span> above shows the count of the file we are going to create. This <b>count</b> and <b>bs</b> determines the file size. This is how I got the <b>count</b> :
  </p>
  
  <pre class="prettyprint"><code>&lt;span style="background-color: yellow;">512&lt;/span> * 1024^2 / 4096 = 131072</code></pre>
  
  <p>
    The <span style="background-color: yellow;">yellow background text</span> above shows the file size we need to create in <b>Mega Bytes (MB)</b>. If you need to create <b>SWAP</b> space with more than <b>512 MB</b> change the <span style="background-color: yellow;">yellow background text</span> above to the <b>MB</b> you want and get the result of the calculation. The result is the <b>count</b>.<br /> Example of <b>1 GB</b> :
  </p>
  
  <pre class="prettyprint"><code>1024 * 1024^2 / 4096 = &lt;span style="background-color: yellow;">262144&lt;/span></code></pre>
  
  <p>
    and the command will become :
  </p>
  
  <pre class="prettyprint"><code>dd if=/dev/zero of=~/subinsblog bs=4096 count=&lt;span style="background-color: yellow;">262144&lt;/span></code></pre>
  
  <p>
    The command will create a file named <b>subinsblog</b> on your home directory.
  </p>
  
  <p>
    Now let&#8217;s create the <b>SWAP</b> space on the file and enable the <b>SWAP </b>:
  </p>
  
  <pre class="prettyprint"><code>sudo mkswap ~/subinsblog -f && sudo swapon -p 1000 ~/subinsblog</code></pre>
  
  <p>
    You&#8217;re <b>RAM</b> <b>Memory</b> is now increased. To check whether if it is increased, do the following command :
  </p>
  
  <pre class="prettyprint"><code>free -m</code></pre>
  
  <p>
    You will get a result like below :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a style="margin-left: 1em; margin-right: 1em;" href="https://2.bp.blogspot.com/-Ij3bqBN2EzA/Uj8MXR8WzfI/AAAAAAAADBM/gcsMgzYcf6A/s1600/0045.png"><img alt="" src="https://2.bp.blogspot.com/-Ij3bqBN2EzA/Uj8MXR8WzfI/AAAAAAAADBM/gcsMgzYcf6A/s1600/0045.png" border="0" /></a>
  </div>
  
  <p>
    The picture says it all. Good Luck ! 🙂<br /> If you have any <b>problems/suggestions/feedback</b> just print it out in the comment box below.
  </p>
</div>