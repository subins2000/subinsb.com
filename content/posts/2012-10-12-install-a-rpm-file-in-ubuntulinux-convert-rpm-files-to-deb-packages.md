---
title: Install a RPM file in Ubuntu/Linux – Convert RPM files to DEB packages
author: Subin Siby
type: post
date: 2012-10-12T11:57:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on">Some programs will only have an <b>RPM</b>&nbsp;file which can be downloaded and can be installed. But most of the people don't know how to install. If you open the <b>RPM</b>&nbsp;file you will see the archive in <b>Archive Manager</b>.<br><br>This post will help you to convert <b>RPM</b>&nbsp;files to <b>DEB</b>&nbsp;packages which we can install in <b>Linux</b>.<br>Open the terminal (<b>Applications &#9656; Accessories &#9656; Terminal</b><span>) or by pressing <b>CTRL + F2</b>.</span><br><span>Install <b>Alien</b>&nbsp;package to convert the <b>RPM</b>&nbsp;file by using the following command.</span><br><blockquote><span><span>sudo apt-get install alien dpkg-dev debhelper build-essential</span></span></blockquote>After installation we can convert the file. For that use this command.<br><blockquote>sudo alien <span>file location</span></blockquote>Replace <span>file location</span><b>&nbsp;</b>on the above command&nbsp;with the source of your file. For Example :<br><blockquote><b>sudo alien <span><span>Downloads/wine-1.3.rpm</span></span></b></blockquote>The process would take some minutes depending on the size of the <b>RPM&nbsp;</b>file.<br>After the process open the folder where your <b>RPM</b>&nbsp;file is. You would see another file with the same name with the only difference that it's a <b>DEB</b>&nbsp;package.<br>Open the <b>DEB</b>&nbsp;package and install it using <b>GDebi Package Installer</b>.<br><br>That's it !! Hope this worked for you. If it didn't worked Tell the problem in the comments and I will help you.</div>
url: /install-a-rpm-file-in-ubuntulinux-convert-rpm-files-to-deb-packages
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
  - http://sag-3.blogspot.com/2012/10/install-rpm-file-in-ubuntulinux-convert.html
syndication_item_hash:
  - 75f56ba1a39fc8ec74dda5edb902c6a9
  - 75f56ba1a39fc8ec74dda5edb902c6a9
  - 75f56ba1a39fc8ec74dda5edb902c6a9
  - 75f56ba1a39fc8ec74dda5edb902c6a9
  - 75f56ba1a39fc8ec74dda5edb902c6a9
  - 75f56ba1a39fc8ec74dda5edb902c6a9
categories:
  - Linux
  - Ubuntu

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Some programs will only have an <b>RPM</b>&nbsp;file which can be downloaded and can be installed. But most of the people don&#8217;t know how to install. If you open the <b>RPM</b>&nbsp;file you will see the archive in <b>Archive Manager</b>.</p> 
  
  <p>
    This post will help you to convert <b>RPM</b>&nbsp;files to <b>DEB</b>&nbsp;packages which we can install in <b>Linux</b>.<br />Open the terminal (<b style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Applications ▸ Accessories ▸ Terminal</b><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">) or by pressing <b>CTRL + F2</b>.</span><br /><span style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">Install <b>Alien</b>&nbsp;package to convert the <b>RPM</b>&nbsp;file by using the following command.</span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif;"><span style="font-size: 15px; line-height: 20px;">sudo apt-get install alien dpkg-dev debhelper build-essential</span></span>
    </p>
  </blockquote>
  
  <p>
    After installation we can convert the file. For that use this command.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      sudo alien <span style="background-color: #bf9000; color: white;">file location</span>
    </p>
  </blockquote>
  
  <p>
    Replace <span style="background-color: #bf9000; color: white;">file location</span><b style="color: red;">&nbsp;</b>on the above command&nbsp;with the source of your file. For Example :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <b>sudo alien <span style="background-color: #bf9000;"><span style="color: white;">Downloads/wine-1.3.rpm</span></span></b>
    </p>
  </blockquote>
  
  <p>
    The process would take some minutes depending on the size of the <b>RPM&nbsp;</b>file.<br />After the process open the folder where your <b>RPM</b>&nbsp;file is. You would see another file with the same name with the only difference that it&#8217;s a <b>DEB</b>&nbsp;package.<br />Open the <b>DEB</b>&nbsp;package and install it using <b>GDebi Package Installer</b>.
  </p>
  
  <p>
    That&#8217;s it !! Hope this worked for you. If it didn&#8217;t worked Tell the problem in the comments and I will help you.
  </p>
</div>