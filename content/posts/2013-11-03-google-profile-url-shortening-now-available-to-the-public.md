---
title: Google + Profile URL Shortening Now Available To The Public
author: Subin Siby
type: post
date: 2013-11-03T13:59:00+00:00
excerpt: 'The wait is over. Now you can short your Google&nbsp;+&nbsp;long profile URL&nbsp;to something like http://www.google.com/+SubinSiby. Yes, you can get a short URL&nbsp;on google.com. Google&nbsp;made this public 3 or 4&nbsp;days ago. I got a mail&nbsp;...'
url: /google-profile-url-shortening-now-available-to-the-public
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
  - http://sag-3.blogspot.com/2013/11/get-short-google-plus-profile-url.html
dsq_thread_id:
  - 1932465994
  - 1932465994
  - 1932465994
  - 1932465994
  - 1932465994
  - 1932465994
syndication_item_hash:
  - 98cf2325a8a8557926c8ec40f6f5191b
  - 98cf2325a8a8557926c8ec40f6f5191b
  - 98cf2325a8a8557926c8ec40f6f5191b
  - 98cf2325a8a8557926c8ec40f6f5191b
  - 98cf2325a8a8557926c8ec40f6f5191b
  - 98cf2325a8a8557926c8ec40f6f5191b
tags:
  - Google
  - My Short URL
  - Profile
  - Username

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  The wait is over. Now you can short your <b>Google&nbsp;</b><b>+</b>&nbsp;long profile <b>URL</b>&nbsp;to something like <b>http://www.google.com/+SubinSiby</b>. Yes, you can get a short <b>URL</b>&nbsp;on <b>google.com</b>. <b>Google</b>&nbsp;made this public <b>3 </b>or <b>4&nbsp;</b>days ago. I got a <b>mail</b>&nbsp;saying that your <b>profile</b>&nbsp;is now eligible to short. In this post I&#8217;m going to tell the step by step procedure to short the long <b>URL</b>. If you got the <b>mail</b>&nbsp;already you can skip <b>Step 1</b>.</p> 
  
  <h2 style="text-align: left;">
    <b>Step 1</b>
  </h2>
  
  <p>
    Go to your <b>Google+ </b><a href="http://plus.google.com/u/o/me" >profile page</a>. Wait till the page is completely loaded. If you get the bar (shown below) at the top of your <b>profile</b>&nbsp;page, then you are eligible to short your <b>URL</b>.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-GyG763qmu9Q/UnZSs5j0bWI/AAAAAAAADQM/9FkhP8S8VVw/s1600/0061.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-GyG763qmu9Q/UnZSs5j0bWI/AAAAAAAADQM/9FkhP8S8VVw/s1600/0061.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <h2 style="text-align: left;">
    Step 2
  </h2>
  
  <p>
    Click on <b>Get URL</b>&nbsp;button on the bar. You will get a window like below :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-ackSzSsf3-c/UnZSwacitFI/AAAAAAAADQg/mt2X3DTgzfs/s1600/0062.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-ackSzSsf3-c/UnZSwacitFI/AAAAAAAADQg/mt2X3DTgzfs/s1600/0062.png" /></a>
  </div>
  
  <p>
    Tick the <b>I agree to the Terms of Service</b>&nbsp;check box and click on <b>Change URL</b>.
  </p>
  
  <h2 style="text-align: left;">
    Step 3
  </h2>
  
  <p>
    <b>Google</b>&nbsp;will ask for confirmation. Note that you can&#8217;t change this short <b>URL</b>&nbsp;to something else in the future once done.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-YgWRY8SEHxs/UnZSwyEbLTI/AAAAAAAADQo/14RTxRZty2M/s1600/0063.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-YgWRY8SEHxs/UnZSwyEbLTI/AAAAAAAADQo/14RTxRZty2M/s1600/0063.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <p>
    To confirm click on <b>Confirm choice</b>&nbsp;button. Once clicked a loading screen will be shown :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-1V8Uz5RJ1I4/UnZSwRKWo7I/AAAAAAAADQc/1W0AXkIP8ic/s1600/0064.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-1V8Uz5RJ1I4/UnZSwRKWo7I/AAAAAAAADQc/1W0AXkIP8ic/s1600/0064.png" /></a>
  </div>
  
  <p>
    Everything&#8217;s now finished. You will get a success box if there are no errors :
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-b-yLN2zWt5Y/UnZSyDP4vaI/AAAAAAAADQw/Cv7IJ5k49WA/s1600/0065.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-b-yLN2zWt5Y/UnZSyDP4vaI/AAAAAAAADQw/Cv7IJ5k49WA/s1600/0065.png" /></a>
  </div>
  
  <p>
    Congratulations, you now have a short <b>URL</b>. Try it out. Here&#8217;s my short <b>URL</b>&nbsp;: <a href="http://www.google.com/+SubinSiby">http://www.google.com/+SubinSiby</a></div>