---
title: PHP Output Buffering For Noobies
author: Subin Siby
type: post
date: 2016-06-10T16:37:00+00:00
url: /php-output-buffering
categories:
  - PHP
  - Program

---
You might have heard about output buffering while you code in **PHP**. I will tell you what it is and how to use it simply.


## "Output Buffering" Meaning

"Buffering" in **computing** means

> store (data) in a buffer while it is being processed or transferred.

This is exactly what PHP does too. But, the default mechanism is not this. It is turned **off** by default i.e :

> data is not stored in a buffer while it is being processed or transferred.

## Output Buffering Off (Default)

When a user requests a **PHP** page from your server, PHP processes the page and on the fly, the HTML is sent to the browser (user).

This means, that HTML is received by the browser in chunks during the connection and the connection won&#8217;t close until the processing is finished.

This is the **default** setting in **PHP** i.e output buffer turned off.

## Output Buffering On

As the word meaning says, the **processed HTML** is stored in a variable until the processing is finished and when it is finished, the contents of the variable is sent.

So, instead of chunks of data, the full data is sent at an instant. These are the advantages if output buffering is turned **on** :

  * Able to do redirects even at the end of page : <pre class="prettyprint"><code>Hello World
&lt;?php
header("Location: https://subinsb.com");
?&gt;</code></pre>
    
    Normally, the above code will print "headers already sent error". But, with output buffering turned on, this error won&#8217;t be produced.</li> 
    
      * "Warning: Cannot modify header information &#8211; headers already sent by (output)" error and all such errors won&#8217;t be produced</ul> 
    
    ## Toggle Output Buffering
    
    You can choose to turn on/off output buffering **permanently** by editing "php.ini" file. Find "output_buffering" key and set the value :
    
    <pre class="prettyprint"><code>&lt;a class="link" href="http://php.net/manual/en/outcontrol.configuration.php#ini.output-buffering">output_buffering&lt;/a>=On</code></pre>
    
    You can also set it by ".htaccess" file :
    
    <pre class="prettyprint"><code>php_flag output_buffering On</code></pre>

 [1]: #8220output-buffering8221-meaning
 [2]: #output-buffering-off-default
 [3]: #output-buffering-on
 [4]: #toggle-output-buffering
