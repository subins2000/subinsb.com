---
title: Google + going to host a hangout with Steven Spielberg and Joseph Gordon-Levitt
author: Subin Siby
type: post
date: 2012-09-13T16:15:00+00:00
excerpt: '<div dir="ltr" trbidi="on"><h3><em>Steven Spielberg and Joseph Gordon-Levitt </em><em>discuss about their new film </em><em>LINCLON.</em></h3><div><em>The hangout is&nbsp;</em><span>Tomorrow, 14 September, 04:30 GMT+05:30.</span></div><div><span><br></span></div><div><span><span>Google Play presents: Steven Spielberg and Joseph Gordon-Levitt in their first ever Hangout On Air where they will be exclusively premiering the trailer for their upcoming movie, Lincoln. If you want to be one of the lucky fans to join the hangout please:</span><br><br><span>-Let us know how we can contact you here&nbsp;</span><a href="http://goo.gl/ApTvq">http://goo.gl/ApTvq</a><span>.&nbsp;</span><br><span>-Upload a short video to your YouTube channel with the&nbsp;</span><a href="https://plus.google.com/s/%23lincolnhangout">#lincolnhangout</a><span>&nbsp;tag explaining who you are, why you are interested in &ldquo;Lincoln&rdquo; and what you would like to ask Spielberg and Gordon-Levitt</span><br><span>-Reshare this G+ event</span><br><br><span><span>+</span><a href="https://plus.google.com/106886664866983861036" oid="106886664866983861036">Google Play</a></span><span>&nbsp;will reach out to you if you&rsquo;ve been selected to join.</span><a href="https://plus.google.com/s/%23lincolnhangout">#lincolnhangout</a><span>&nbsp;.</span><br><br><span>ABOUT THE MOVIE:</span><br><br><span>Steven Spielberg directs Joseph Gordon-Levitt in &nbsp;&ldquo;Lincoln,&rdquo; a revealing drama that focuses on the 16th President&rsquo;s tumultuous final months in office. In a nation divided by war and the strong winds of change, Lincoln pursues a course of action designed to end the war, unite the country and abolish slavery. With the moral courage and fierce determination to succeed, his choices during this critical moment will change the fate of generations to come.</span></span></div></div>'
url: /google-going-to-host-a-hangout-with-steven-spielberg-and-joseph-gordon-levitt
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 5a05aaac3161dd72db09deb0db03c94f
  - 5a05aaac3161dd72db09deb0db03c94f
  - 5a05aaac3161dd72db09deb0db03c94f
  - 5a05aaac3161dd72db09deb0db03c94f
  - 5a05aaac3161dd72db09deb0db03c94f
  - 5a05aaac3161dd72db09deb0db03c94f
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
  - http://sag-3.blogspot.com/2012/09/google-going-to-host-hangout-with.html
tags:
  - Google
  - Hangout
  - News

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <h3 class="r" style="background-color: white; color: #222222; font-family: arial, sans-serif; font-size: medium; margin: 0px; overflow: hidden; padding: 0px; text-align: -webkit-auto; text-overflow: ellipsis; white-space: nowrap;">
    <em style="font-style: normal; font-weight: bold;">Steven Spielberg and Joseph Gordon-Levitt </em><em style="font-style: normal; font-weight: normal;">discuss about their new film </em><em style="font-style: normal;">LINCLON.</em>
  </h3>
  
  <div>
    <em style="font-style: normal;">The hangout is&nbsp;</em><span style="background-color: white; color: #444444; font-family: arial, sans-serif; font-size: 13px; line-height: 18px; text-align: -webkit-auto; white-space: nowrap;">Tomorrow, 14 September, 04:30 GMT+05:30.</span>
  </div>
  
  <div>
    <span style="background-color: white; color: #444444; font-family: arial, sans-serif; font-size: 13px; line-height: 18px; text-align: -webkit-auto; white-space: nowrap;"><br /></span>
  </div>
  
  <div style="text-align: -webkit-auto;">
    <span style="font-family: inherit;"><span style="background-color: white; color: #444444; line-height: 19px;">Google Play presents: Steven Spielberg and Joseph Gordon-Levitt in their first ever Hangout On Air where they will be exclusively premiering the trailer for their upcoming movie, Lincoln. If you want to be one of the lucky fans to join the hangout please:</span><br style="background-color: white; color: #444444; line-height: 19px;" /><br style="background-color: white; color: #444444; line-height: 19px;" /><span style="background-color: white; color: #444444; line-height: 19px;">-Let us know how we can contact you here&nbsp;</span><a class="ot-anchor" href="http://goo.gl/ApTvq" style="background-color: white; color: #3366cc; cursor: pointer; line-height: 19px; text-decoration: none;">http://goo.gl/ApTvq</a><span style="background-color: white; color: #444444; line-height: 19px;">.&nbsp;</span><br style="background-color: white; color: #444444; line-height: 19px;" /><span style="background-color: white; color: #444444; line-height: 19px;">-Upload a short video to your YouTube channel with the&nbsp;</span><a class="ot-hashtag" href="https://plus.google.com/s/%23lincolnhangout" style="background-color: white; color: #3366cc; cursor: pointer; line-height: 19px; text-decoration: none;">#lincolnhangout</a><span style="background-color: white; color: #444444; line-height: 19px;">&nbsp;tag explaining who you are, why you are interested in "Lincoln" and what you would like to ask Spielberg and Gordon-Levitt</span><br style="background-color: white; color: #444444; line-height: 19px;" /><span style="background-color: white; color: #444444; line-height: 19px;">-Reshare this G+ event</span><br style="background-color: white; color: #444444; line-height: 19px;" /><br style="background-color: white; color: #444444; line-height: 19px;" /><span class="proflinkWrapper" style="background-color: white; color: #444444; line-height: 19px;"><span class="proflinkPrefix" style="color: #999999;">+</span><a class="proflink" href="https://plus.google.com/106886664866983861036" oid="106886664866983861036" style="color: #3366cc; cursor: pointer; text-decoration: none;">Google Play</a></span><span style="background-color: white; color: #444444; line-height: 19px;">&nbsp;will reach out to you if you’ve been selected to join.</span><a class="ot-hashtag" href="https://plus.google.com/s/%23lincolnhangout" style="background-color: white; color: #3366cc; cursor: pointer; line-height: 19px; text-decoration: none;">#lincolnhangout</a><span style="background-color: white; color: #444444; line-height: 19px;">&nbsp;.</span><br style="background-color: white; color: #444444; line-height: 19px;" /><br style="background-color: white; color: #444444; line-height: 19px;" /><span style="background-color: white; color: #444444; line-height: 19px;">ABOUT THE MOVIE:</span><br style="background-color: white; color: #444444; line-height: 19px;" /><br style="background-color: white; color: #444444; line-height: 19px;" /><span style="background-color: white; color: #444444; line-height: 19px;">Steven Spielberg directs Joseph Gordon-Levitt in &nbsp;"Lincoln," a revealing drama that focuses on the 16th President’s tumultuous final months in office. In a nation divided by war and the strong winds of change, Lincoln pursues a course of action designed to end the war, unite the country and abolish slavery. With the moral courage and fierce determination to succeed, his choices during this critical moment will change the fate of generations to come.</span></span>
  </div>
</div>