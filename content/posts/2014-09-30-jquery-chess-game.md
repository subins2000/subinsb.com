---
title: Create HTML Chess Game With jQuery
author: Subin Siby
type: post
date: 2014-09-30T03:30:24+00:00
url: /jquery-chess-game
categories:
  - HTML
  - JavaScript
  - jQuery
tags:
  - Plugin
  - Script

---
We all love to play Chess, the most fascinating game that absolutely needs good thinking powers. You know that, comparing a human to a computer logically and arithmetically, computer has the superior power. So, certainly if you play a chess game with computer as opponent, a 90% chance is for the computer to be the winner. But, it is not sure.

It was actually in **C** language by <a href="https://en.wikipedia.org/wiki/Toledo_Nanochess" target="_blank">Oscar Toledo Gutiérrez</a>, who created chess game program in less than 2 KB. Since, it&#8217;s **C**, compiling is necessary. But, who have time for that. Let&#8217;s make it in the web.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=s/6295t23lh4vj2qk6dz0h&class=32" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/jquery/chess" target="_blank">Demo</a>
</div>

We only need **jQuery** and a plugin I created for making the game. You can see the plugin code <a href="//lab.subinsb.com/projects/jquery/chess/jquery.chess.js" target="_blank">here</a> and the minified version <a href="//lab.subinsb.com/projects/jquery/chess/jquery.chess.min.js" target="_blank">here</a>.

The first thing you need to do is make a **HTML** file and select the element where you want to add the game into :

<pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
 	&lt;head&gt;
		&lt;script src="//lab.subinsb.com/projects/jquery/core/jquery-latest.js"&gt;&lt;/script&gt;
		&lt;script src="//lab.subinsb.com/projects/jquery/chess/jquery.chess.min.js"&gt;&lt;/script&gt;
 	&lt;/head&gt;
 	&lt;body&gt;
		&lt;div id="content"&gt;
			&lt;h2&gt;jQuery Chess Game&lt;/h2&gt;
			&lt;div id="game"&gt;&lt;/div&gt;
		&lt;/div&gt;
		&lt;!-- http://subinsb.com/jquery-chess-game --&gt;
 	&lt;/body&gt;
&lt;/html&gt;</code></pre>

We are loading the latest **jQuery** version and the minified version of the plugin.

Now, we tell the plugin to make the game in the **#game** element :

<pre class="prettyprint"><code>$(document).ready(function(){
    $("#game").chess();
});</code></pre>

Insert the above code in a **<script>** tag or in a separate **JS** file and the game will be started on the element.

## The Game

You will see the 8&#215;8 blocks of the chess board with slightly brown and yellow colours. The moulds of the board is not images, but characters of **UTF-8** format.

When each moulds of game are clicked and moved, **JS** processes if the move is valid and makes the move. The chess game code is created in **JS** by <a href="http://1kchess.an3.es/" target="_blank">Andrés Moreno</a> by porting from the **C** code by Óscar Toledo G.

My plugin makes the game board, places the moulds and handle events.

Hope you have enjoyed this. I&#8217;m planning to create a multiplayer **AJAX** version of this in the future. Stay tuned.