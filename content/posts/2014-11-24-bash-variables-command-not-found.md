---
title: 'Variables in Bash ": command not found"'
author: Subin Siby
type: post
date: 2014-11-24T03:30:51+00:00
url: /bash-variables-command-not-found
categories:
  - Short Post
tags:
  - BASH
  - error
  - Terminal

---
**Bash** is like the very complicated language, even a space can make a syntax error. But, when you get into it often, you will get a hang of it and it will be easy.

This post is a solution to one of the most common problems that happen to a newbie in **Bash** and it&#8217;s about a single whitespace.

Here is a sample **Bash** file :

<pre class="prettyprint"><code>a = 50
echo $a</code></pre>

and when you execute it, the error will be as follows :

<pre class="prettyprint"><code>script.sh: line 1: a: command not found</code></pre>

This error is produced by the error in defining the variable "a". There should be no space before or after the "=". If you remove it, then the script will run successfully.

When there is a space after "a", **Bash** thinks that "a" is a command followed by it&#8217;s attributes. Since there is no command named "a", it will produce a "command not found" error.