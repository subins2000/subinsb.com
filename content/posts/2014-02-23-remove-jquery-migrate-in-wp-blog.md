---
title: Disable jQuery Migrate Loading In WordPress Blog
author: Subin Siby
type: post
date: 2014-02-23T17:09:31+00:00
url: /remove-jquery-migrate-in-wp-blog
authorsure_include_css:
  - 
  - 
  - 
categories:
  - jQuery
  - PHP
  - WordPress

---
If you run a WordPress blog with all the new stuff (functions) and still WordPress loads the **jQuery Migrate** on your blog, you will be angry (I know hot it feels). The jQuery Migrate file&#8217;s minified version is about **7 KB** and the unminified version is about **17 KB**. That&#8217;s a file you don&#8217;t need if your blog&#8217;s jQuery functions aren&#8217;t deprecated.


## What&#8217;s jQuery Migrate ?

jQuery Migrate is a JavaScript file that adds the jQuery deprecated functions to the page which makes us to use the deprecated functions of jQuery.

## Do I Need It ?

It depends on the jQuery code you&#8217;re using on your blog. If you use the deprecated functions, you need jQuery Migrate. If you&#8217;re not and is pretty sure that none of the plugins you installed is using it, you can move on to the next part of post.

## Know If Blog Has Deprecated Functions

The hardest way is to got to jQuery docs, find functions one by one and look for it in your code. This <a href="http://stackoverflow.com/a/14386696/1372424" target="_blank">SO answer</a> has a great info on deprecated functions.

Another way is to first remove jQuery migrate and check the browser console for any jQuery errors. If there is an error saying something like this :

<pre class="prettyprint"><code>ReferenceError: foo is not defined</code></pre>

then there is a depreciated function in your code. You can find it and remove or you can stop this tutorial itself.

## Remove jQuery Migrate

Finally, let&#8217;s remove jQuery Migrate. Go to your themes&#8217; **functions.php** file and add the following code at the end of the file :

<pre class="prettyprint"><code>/* No JQ Migrate - http://subinsb.com/remove-jquery-migrate-in-wp-blog */
add_filter( 'wp_default_scripts', 'removeJqueryMigrate' );
function removeJqueryMigrate(&$scripts){
 if(!is_admin()){
  $scripts-&gt;remove('jquery');
  $scripts-&gt;add('jquery', false, array('jquery-core'), '1.10.2');
 }
}</code></pre>

Save the file and close. Now, go to your blog and there won&#8217;t be any **jQuery Migrate** file loaded in the page. You just saved **7 KB**.

 [1]: #what8217s-jquery-migrate
 [2]: #do-i-need-it
 [3]: #know-if-blog-has-deprecated-functions
 [4]: #remove-jquery-migrate
