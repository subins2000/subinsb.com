---
title: Get Architecture Plainly Without Version In Android
author: Subin Siby
type: post
date: 2016-09-03T19:09:08+00:00
url: /just-plainly-get-android-architecture
categories:
  - Android
  - Short Post

---
If you want to just get plainly the architecture of system like "arm" or "x86" in your apps, then this post is just for you.

So with this function that I&#8217;m gonna present, you will get 3 values, "arm", "mips" or "x86" whether or not the system is 32 bit or 64 bit.

Here is the function :

<pre class="prettyprint"><code>public static String getArch() {
  String arch = System.getProperty("os.arch").substring(0, 3).toLowerCase();
  if (arch.equals("aar") || arch.equals("arm")) {
    return "arm";
  }else if(arch.equals("mip")){
    return "mips";
  }else{
    return arch;
  }
}</code></pre>

Just call it statically if it&#8217;s under a class :

<pre class="prettyprint"><code>MyClass.getArch()
</code></pre>

and you can correctly check if it&#8217;s an ARM or MIPS or x86 :

<pre class="prettyprint"><code>if(getArch().equals("arm"))
  // Hurray, it's an ARM system</code></pre>