---
title: Connecting database to both mysqli and mysql entension
author: Subin Siby
type: post
date: 2013-06-08T07:26:00+00:00
excerpt: 'You can use both mysql&nbsp;and mysqli&nbsp;extension for executing SQL&nbsp;queries. Here is a tutorial on how to connect both the extensions to a database.Create a file named config.php&nbsp;where you will connect to the database. Put the following c...'
url: /connecting-database-to-both-mysqli-and-mysql-entension
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 283187a5456daad4e3250433cba71336
  - 283187a5456daad4e3250433cba71336
  - 283187a5456daad4e3250433cba71336
  - 283187a5456daad4e3250433cba71336
  - 283187a5456daad4e3250433cba71336
  - 283187a5456daad4e3250433cba71336
syndication_permalink:
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
blogger_permalink:
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
  - http://sag-3.blogspot.com/2013/06/connecting-database-to-both-mysqli-and.html
categories:
  - Database
  - MySQL
  - SQL
tags:
  - Mysqli

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You can use both <b>mysql</b>&nbsp;and <b>mysqli</b>&nbsp;extension for executing <b>SQL</b>&nbsp;queries. Here is a tutorial on how to connect both the extensions to a database.</p> 
  
  <p>
    Create a file named <b>config.php</b>&nbsp;where you will connect to the database. Put the following code in it:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <?<br />$mysqli = new mysqli("<span style="color: red;">localhost</span>", "<span style="color: red;">username</span>", "<span style="color: red;">password</span>", "<span style="color: red;">db</span>");<br />if ($mysqli->connect_errno) {<br />&nbsp; &nbsp; echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;<br />}<br />$bd = mysql_connect("<span style="color: red;">localhost</span>", "<span style="color: red;">username</span>", "<span style="color: red;">password</span>") or die(mysql_error());<br />mysql_select_db("<span style="color: red;">db</span>",$bd) or die(mysql_error());<br />?>
    </p>
  </blockquote>
  
  <p>
    Replace the red highlighted text with the credentials of your database connection.
  </p>
</div>