---
title: Revert back to normal resolution after running a fullscreen app in Wine
author: Subin Siby
type: post
date: 2013-07-11T03:30:00+00:00
excerpt: "While running a fullscreen&nbsp;app in Wine&nbsp;such as games, you might not get the screen resolution back after you close the app. It's because Linux&nbsp;won't automatically change it's resolution like Windows.But you could if you run a simple file..."
url: /revert-back-to-normal-resolution-after-running-a-fullscreen-app-in-wine
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
  - http://sag-3.blogspot.com/2013/07/revert-back-to-normal-resolution-after.html
syndication_item_hash:
  - 1e364d08a1cc3c8f2b0edf856543f6da
  - 1e364d08a1cc3c8f2b0edf856543f6da
  - 1e364d08a1cc3c8f2b0edf856543f6da
  - 1e364d08a1cc3c8f2b0edf856543f6da
  - 1e364d08a1cc3c8f2b0edf856543f6da
  - 1e364d08a1cc3c8f2b0edf856543f6da
categories:
  - Linux
  - Ubuntu
  - Windows
  - Wine
tags:
  - BASH
  - Desktop
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  While running a <b>fullscreen</b>&nbsp;app in <b>Wine</b>&nbsp;such as games, you might not get the screen resolution back after you close the app. It&#8217;s because <b>Linux</b>&nbsp;won&#8217;t automatically change it&#8217;s resolution like <b>Windows</b>.</p> 
  
  <p>
    But you could if you run a simple file in terminal. Here is how you could do it.<br />Create a file named <b>normal.sh</b>&nbsp;in your <b>Desktop </b>with the following contents:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      #!/bin/bash<br />xrandr -s 0
    </p>
  </blockquote>
  
  <p>
    Save it on a place where you can open it easily. Make the file&nbsp;<b>Executable</b>&nbsp;to run it in a terminal. See <a href="http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html" >this post</a> to see how you could make a file executable.
  </p>
  
  <p>
    After you run a full screen program and closed it, run the file we created to return back to the normal resolution.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-R2xYTfVaSR8/Udra0x13VtI/AAAAAAAAC18/f5Np776DYo0/s1600/0029.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-R2xYTfVaSR8/Udra0x13VtI/AAAAAAAAC18/f5Np776DYo0/s1600/0029.png" /></a>
  </div>
  
  <p>
    If you get a dialog like above when executing the <b>normal.sh</b>&nbsp;click on <b>Run in Terminal</b>&nbsp;button.</div>