---
title: Password Protect folder in AppFog
author: Subin Siby
type: post
date: 2013-08-30T15:45:00+00:00
excerpt: "You won't know the location of the .htpasswd&nbsp;file in AppFog. Hence you won't be able to password protect folders in the normal way. To password protect folders in AppFog&nbsp;you should use a different way. You should add the following location of..."
url: /password-protect-folder-in-appfog
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - c5530919b6a5a7b7c16724cd82e2cdd0
  - c5530919b6a5a7b7c16724cd82e2cdd0
  - c5530919b6a5a7b7c16724cd82e2cdd0
  - c5530919b6a5a7b7c16724cd82e2cdd0
  - c5530919b6a5a7b7c16724cd82e2cdd0
  - c5530919b6a5a7b7c16724cd82e2cdd0
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
  - http://sag-3.blogspot.com/2013/08/password-protect-folders-appfog.html
tags:
  - .htaccess
  - .htpasswd
  - AppFog
  - Location
  - Passwords

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You won&#8217;t know the location of the <b>.htpasswd</b>&nbsp;file in <b>AppFog</b>. Hence you won&#8217;t be able to password protect folders in the normal way. To password protect folders in <b>AppFog</b>&nbsp;you should use a different way. You should add the following location of <b>.htpasswd</b>&nbsp;instead of the full location.<br />If the full location is :</p> 
  
  <blockquote class="tr_bq">
    <p>
      /home/simsu/projects/TestPalace/Blog/Password_Protect/.htpasswd
    </p>
  </blockquote>
  
  <p>
    The Location of <b>.htpasswd</b>&nbsp;in <b>.htaccess</b>&nbsp;deployed in&nbsp;<b>AppFog</b>&nbsp;should be :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      ../app/Blog/Password_Protect/.htpasswd
    </p>
  </blockquote>
  
  <p>
    If the file is on the root directory, then the location would be :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      ../app/.htpasswd
    </p>
  </blockquote>
  
  <p>
    If you have any suggestions/feedback/problems just <b>echo</b> it out in the comments below.</div>