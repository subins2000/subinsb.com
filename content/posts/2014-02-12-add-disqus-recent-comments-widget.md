---
title: How To Add Disqus Recent Comments Widget
author: Subin Siby
type: post
date: 2014-02-12T14:58:42+00:00
url: /add-disqus-recent-comments-widget
authorsure_include_css:
  - 
  - 
  - 
categories:
  - CSS
  - HTML
  - JavaScript
tags:
  - Disqus

---
In the last post, I told the way to implement Disqus Combination widget to your web site which have recent comments, popular threads and commented users, all in one. But, if you only need one of it, why load all 3 of them. So, if you only want the Recent Comments widget, you will find this post useful.


# Screenshot<figure id="attachment_2476" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2476" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0101-300x276.png" alt="Disqus Recent Comments Widget" width="300" height="276" />][2]<figcaption class="wp-caption-text">Disqus Recent Comments Widget</figcaption></figure> 

You can also see a demo on JsFiddle.

<div class="padlinks">
  <a class="demo" href="http://jsfiddle.net/subins2000/6qhN9/" target="_blank" rel="noopener">Demo</a>
</div>

# The Code

Insert the following code in the place where you want to display the widget :

<pre class="prettyprint"><code>&lt;div id="RecentComments" class="dsq-widget"&gt;
 &lt;h2&gt;Recent Comments&lt;/h2&gt;
 &lt;script type="text/javascript" src="https://example.disqus.com/recent_comments_widget.js?num_items=5&hide_mods=0&hide_avatars=0&avatar_size=32&excerpt_length=100"&gt;&lt;/script&gt;
&lt;/div&gt;</code></pre>

You should replace the "example.disqus.com" with your Disqus forum URL ( "shortname.disqus.com" ).

# Configure

As I said in my previous post, this widget also supports configuration like the "Combination Widget".

<table class="table">
  <tr>
    <td>
      Name
    </td>
    
    <td>
      Default Value
    </td>
    
    <td>
      Description
    </td>
  </tr>
  
  <tr>
    <td>
      num_items
    </td>
    
    <td>
      5
    </td>
    
    <td>
      The number of items that should be shown (includes all items).
    </td>
  </tr>
  
  <tr>
    <td>
      hide_mods
    </td>
    
    <td>
    </td>
    
    <td>
      Set To "1" to hide Moderators&#8217; comments
    </td>
  </tr>
  
  <tr>
    <td>
      excerpt_length
    </td>
    
    <td>
      100
    </td>
    
    <td>
      The excerpt (short version of comment) length
    </td>
  </tr>
  
  <tr>
    <td>
      hide_avatars
    </td>
    
    <td>
    </td>
    
    <td>
      Set to "1" if you don&#8217;t want to display avatars
    </td>
  </tr>
  
  <tr>
    <td>
      avatar_size
    </td>
    
    <td>
      32
    </td>
    
    <td>
      The Size of the avatar of user.
    </td>
  </tr>
</table>

You can add this code in Blogger, WordPress and any other Blogging services.

# Blogger

Go to your **Blogger Blog -> Layout** and click on the "Add a Gadget" button. Choose "HTML/JavaScript" widget :<figure id="attachment_2477" class="wp-caption aligncenter">

[<img class="size-full wp-image-2477" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0102.png" alt="Add HTML/Javascript Widget In Blogger" width="1049" height="244" />][6]<figcaption class="wp-caption-text">Add HTML/Javascript Widget In Blogger</figcaption></figure> 

Then, paste the code and click "Save" button.

 [1]: #screenshot
 [2]: //lab.subinsb.com/projects/blog/uploads/2014/02/0101.png
 [3]: #the-code
 [4]: #configure
 [5]: #blogger
 [6]: //lab.subinsb.com/projects/blog/uploads/2014/02/0102.png
