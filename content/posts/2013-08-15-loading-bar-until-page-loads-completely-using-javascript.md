---
title: Loading Bar until page loads completely using Javascript
author: Subin Siby
type: post
date: 2013-08-15T03:30:00+00:00
excerpt: "This tutorial was requested by Sumit. In this tutorial I'm going to tell how to put up a loading screen until the page has loaded completely.DEMOHere is the screenshot of loading screen :The loading bar will fade out when loading is completed.This code..."
url: /loading-bar-until-page-loads-completely-using-javascript
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - f167e6406fc3c86ce3a142ab48dbfdc5
  - f167e6406fc3c86ce3a142ab48dbfdc5
  - f167e6406fc3c86ce3a142ab48dbfdc5
  - f167e6406fc3c86ce3a142ab48dbfdc5
  - f167e6406fc3c86ce3a142ab48dbfdc5
  - f167e6406fc3c86ce3a142ab48dbfdc5
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
  - http://sag-3.blogspot.com/2013/08/load-bar-until-page-loads-completely.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
categories:
  - JavaScript
tags:
  - Loading

---
This tutorial was requested by Sumit. In this tutorial I&#8217;m going to tell how to put up a loading screen until the page has loaded completely.

<center>
  <span style="background-color: #6aa84f; border-radius: 10px; border: 2px solid black; display: inline-block; font-size: medium; padding: 10px; width: 150px;"><a style="color: white;" href="http://subin-demos.blogspot.in/">DEMO</a></span>
</center>Here is the screenshot of loading screen :

<a style="margin-left: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-mQVSLDWX03g/UguA1UTiynI/AAAAAAAAC8c/QpPXA5hO4fg/s1600/0035.png"><img class="aligncenter" src="//1.bp.blogspot.com/-mQVSLDWX03g/UguA1UTiynI/AAAAAAAAC8c/QpPXA5hO4fg/s640/0035.png" width="640" height="324" border="0" /></a>

The loading bar will fade out when loading is completed.

This code will work for all sites including **Blogger Blogs**, **WordPress Blogs** except **Dynamic Views Template** of **Blogger **(because it uses ajax loading). You can change the image if you want. I have created an image of my own :

<a style="margin-left: 1em; margin-right: 1em;" href="//4.bp.blogspot.com/-4WVJgCO93zc/UgpU2Y60CjI/AAAAAAAAC8E/R3XujnTjz3Y/s1600/initializing.png"><img class="aligncenter" style="background: black;" src="//4.bp.blogspot.com/-4WVJgCO93zc/UgpU2Y60CjI/AAAAAAAAC8E/R3XujnTjz3Y/s320/initializing.png" width="320" height="270" border="0" /></a>

Ok. Now let&#8217;s get down to the code. This code is pure **Javascript**, no **jQuery** because the site might be loading **jQuery** and we should have to show the loading bar before the loading of **jQuery**.
  
Here is the code. You have to place it just after **<head>**

<pre class="prettyprint"><code>subinsblogla = 0;
setInterval(function() {
    if (document.readyState != 'complete') {
        document.documentElement.style.overflow = "hidden";
        var subinsblog = document.createElement("div");
        subinsblog.id = "subinsblogldiv";
        var polu = 999999999;
        subinsblog.style.zIndex = polu;
        subinsblog.style.background = "black url(&lt;strong>https://lh4.googleusercontent.com/-4WVJgCO93zc/UgpU2Y60CjI/AAAAAAAAC8E/R3XujnTjz3Y/s474/initializing.png&lt;/strong>) 50% 50% no-repeat";
        subinsblog.style.backgroundPositionX = "50%";
        subinsblog.style.backgroundPositionY = "50%";
        subinsblog.style.position = "absolute";
        subinsblog.style.right = "0px";
        subinsblog.style.left = "0px";
        subinsblog.style.top = "0px";
        subinsblog.style.bottom = "0px";
        if (subinsblogla === 0) {
            document.documentElement.appendChild(subinsblog);
            subinsblogla = 1;
        }
    } else if (document.getElementById('subinsblogldiv') != null) {
        document.getElementById('subinsblogldiv').style.display = "none";
        document.documentElement.style.overflow = "auto";
    }
}, 1000);</code></pre>

You may replace the variable names, but make sure that the variable name is unique.

If you want to change the Loading Image just change the

<blockquote class="tr_bq">
  <p>
    <b>https://lh4.googleusercontent.com/-4WVJgCO93zc/UgpU2Y60CjI/AAAAAAAAC8E/R3XujnTjz3Y/s474/initializing.png </b>
  </p>
</blockquote>

url in the variable **subinsblog.style.background**.

Well that&#8217;s it for now. If you have any problems/suggestions/feedback, just say it out in the comment form below and **Happy Independence Day** to all **Indians** and **Pakistanis**.