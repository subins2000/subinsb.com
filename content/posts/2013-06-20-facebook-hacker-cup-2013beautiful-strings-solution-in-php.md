---
title: Facebook Hacker Cup 2013:Beautiful Strings solution in PHP
author: Subin Siby
type: post
date: 2013-06-20T16:31:00+00:00
excerpt: |
  Here is the solution for FHC 2013 Beautiful Strings problem in PHP:$ip = fopen('input.txt', "r");$test_cases = trim(fgets($ip));&nbsp; &nbsp; $c=1;&nbsp; &nbsp; while($c!= $test_cases+1){&nbsp; &nbsp; &nbsp; &nbsp; $case=preg_replace('/[^a-z]+/', '', s...
url: /facebook-hacker-cup-2013beautiful-strings-solution-in-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
  - http://sag-3.blogspot.com/2013/06/facebook-hacker-cup-2013beautiful.html
syndication_item_hash:
  - b85de8eea5b3408dc4b7475d462eed3d
  - b85de8eea5b3408dc4b7475d462eed3d
  - b85de8eea5b3408dc4b7475d462eed3d
  - b85de8eea5b3408dc4b7475d462eed3d
  - b85de8eea5b3408dc4b7475d462eed3d
  - b85de8eea5b3408dc4b7475d462eed3d
dsq_thread_id:
  - 1963743866
  - 1963743866
  - 1963743866
  - 1963743866
  - 1963743866
  - 1963743866
categories:
  - Facebook
  - PHP
tags:
  - Hacker
  - Hacker Cup

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">Here is the solution for FHC 2013 <b>Beautiful Strings</b> problem in <b>PHP</b>:</span></p> 
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">$ip = fopen(&#8216;<span style="color: red;">input.txt</span>&#8216;, "r");<br />$test_cases = trim(fgets($ip));<br />&nbsp; &nbsp; $c=1;<br />&nbsp; &nbsp; while($c!= $test_cases+1){<br />&nbsp; &nbsp; &nbsp; &nbsp; $case=preg_replace(&#8216;/[^a-z]+/&#8217;, ", strtolower(trim(fgets($ip))));<br />&nbsp; &nbsp; &nbsp; &nbsp; $vals=array_count_values(str_split($case));<br />&nbsp; &nbsp; &nbsp; &nbsp; arsort($vals);<br />&nbsp; &nbsp; &nbsp; &nbsp; $beauty=26;<br />&nbsp; &nbsp; &nbsp; &nbsp; $max_b=0;<br />&nbsp; &nbsp; &nbsp; &nbsp; foreach($vals as $v){<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $max_b +=$v*$beauty;<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $beauty&#8211;;<br />&nbsp; &nbsp; &nbsp; &nbsp; }<br />&nbsp; &nbsp; &nbsp; &nbsp; print("Case #".$c.": ".$max_b);<br />&nbsp; &nbsp; &nbsp; &nbsp; if($c < $test_cases){<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print("<br/>");<br />&nbsp; &nbsp; &nbsp; &nbsp; }<br />&nbsp; &nbsp; &nbsp; &nbsp; $c++;<br />&nbsp; &nbsp; }</span>
    </p>
  </blockquote>
  
  <p>
    <span style="font-family: inherit;">Replace the red highlighted text with the input file name.</span><br /><b><span style="font-family: inherit; font-size: large;"><u>Sample Output</u></span></b>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      Case #1: 152<br />Case #2: 754<br />Case #3: 491<br />Case #4: 729<br />Case #5: 646
    </p>
  </blockquote>
</div>