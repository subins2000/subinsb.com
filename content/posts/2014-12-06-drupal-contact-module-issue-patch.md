---
title: Reproducing Drupal Contact Module Issue
author: Subin Siby
type: post
date: 2014-12-06T17:44:25+00:00
url: /drupal-contact-module-issue-patch
categories:
  - PHP
  - Program
tags:
  - Open Source

---
This post contains information regarding the Drupal Core <a href="https://www.drupal.org/node/2348119" target="_blank">issue</a>. This post will explain how the bug was reproduced and how the test patch was created.

The bug was "Wrong message displayed after contact form mail(s) cannot be sent".


## Requires

  * Drupal 8.0
  * Contact Module

Note that URLs mentioned in this post "http://mydrupalsite.com/" is the URL of your **Drupal** installation and "/mydrupalsite" is the folder location of **Drupal** installation.

## Reproducing Bug

1. First of all we need to make a contact form page. Go to the Contact Module configuration page :

<pre class="prettyprint"><code>http://mydrupalsite.com/admin/structure/contact</code></pre>

Click on "Add Contact Form" button and in the new window that you go to, fill up the form with the asked information. Example :<figure id="attachment_3004" class="wp-caption aligncenter">

[<img class="size-large wp-image-3004" src="//lab.subinsb.com/projects/blog/uploads/2014/12/0123-1024x471.png" alt="Drupal Contact Form Config Page" width="640" height="294" />][3]<figcaption class="wp-caption-text">Drupal Contact Form Config Page</figcaption></figure> 

In the Recipients filed, type in an email address that **doesn&#8217;t exist**. Tick the "Make this the default form" and submit the form by "Save" button.

2. Go to the Contact Page of your site :

<pre class="prettyprint"><code>http://mydrupalsite.com/contact</code></pre>

Fill up the contact form with some data and submit the form :<figure id="attachment_3005" class="wp-caption aligncenter">

[<img class="size-large wp-image-3005" src="//lab.subinsb.com/projects/blog/uploads/2014/12/0124-1024x435.png" alt="Filling up the Drupal Contact Form" width="640" height="271" />][4]<figcaption class="wp-caption-text">Filling up the Drupal Contact Form</figcaption></figure> 

3. Now, we will see the bug :<figure id="attachment_3006" class="wp-caption aligncenter">

[<img class="size-large wp-image-3006" src="//lab.subinsb.com/projects/blog/uploads/2014/12/0125-1024x299.png" alt="Drupal Bug" width="640" height="186" />][5]<figcaption class="wp-caption-text">Drupal Bug</figcaption></figure> 

As you can see, the success message is shown even when the email was failed to send.

## Reproducing Bug in Tests

1. Create following test file :

<pre class="prettyprint"><code>/mydrupalsite/core/modules/contact/src/Tests/FailedContactPersonalTest.php</code></pre>

2. I made a new test class by extending "WebTestBase" class and made the **testPersonalContactFail()** method to reproduce bug :

<pre class="prettyprint"><code>function testPersonalContactFail() {
  // Create a valid form.
  $this-&gt;admin_user = $this-&gt;drupalCreateUser(array('access site-wide contact form', 'administer contact forms', 'administer permissions', 'administer users'));

  $this-&gt;drupalLogin($this-&gt;admin_user);
  $this-&gt;addContactForm($id = Unicode::strtolower($this-&gt;randomMachineName(16)), $label = $this-&gt;randomMachineName(16), "mailnoexist@done1ec.com", '', TRUE);
 $this-&gt;assertRaw(t('Contact form %label has been added.', array('%label' =&gt; $label)));

  // Submit the contact form.
  $this-&gt;drupalLogout();
  $this-&gt;drupalGet('/contact');
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access site-wide contact form'));
  $this-&gt;submitContact("Test", "test@nodoamin.com", "Sample Message", $id, "Just a sample message");

  // Check the bug messages
  $this-&gt;assertText(t('Your message has been sent.'), 'Displayed "Message sent"');
  /* The following code only works if emails can be sent in tests */
  //$this-&gt;assertText(t('Unable to send email. Contact the site administrator if the problem persists.'), 'Displayed "Unable to send email..."');
}</code></pre>

The messages due to the bug that are these :

<pre class="prettyprint"><code>Unable to send email. Contact the site administrator if the problem persists.
Your message has been sent.</code></pre>

was checked in the output using **$this->assertText()** function. But, checking of the "Unable to send email. Contact the site administrator if the problem persists." is not possible, because email is not actually sent in testing phase, so to output that message. Therefore that line is commented.

3. Go to "http://mydrupalsite.com/admin/config/development/testing", expand "contact" and tick the checkbox aside to "DrupalcontactTestsNotExistEmailContactTest" :<figure id="attachment_3013" class="wp-caption aligncenter">

[<img class="wp-image-3013 size-full" src="//lab.subinsb.com/projects/blog/uploads/2014/12/0126.png" alt="Choose the contact module for testing" width="984" height="607" />][7]<figcaption class="wp-caption-text">Choose the contact module for testing</figcaption></figure> 

4. Click on "Run Tests" button at the end of the page. Now the testing is started. It will take some time to finish.

5. Now, the success test page is shown :<figure id="attachment_3014" class="wp-caption aligncenter">

[<img class="size-full wp-image-3014" src="//lab.subinsb.com/projects/blog/uploads/2014/12/0127.png" alt="Success Test Page" width="910" height="473" />][8]<figcaption class="wp-caption-text">Success Test Page</figcaption></figure> 

## Patching

1. By using **git**, create a patch. For this, go to the root of **Drupal** installation, open terminal and commit the changed file :

<pre class="prettyprint"><code>git branch 2348119-Reproduce-bug-test-case
git checkout 2348119-Reproduce-bug-test-case
git add core/modules/contact/src/Tests/NotExistEmailContactTest.php
git commit core/modules/contact/src/Tests/NotExistEmailContactTest.php -m "Reproduced the bug https://www.drupal.org/node/2348119"</code></pre>

2. Create the patch file :

<pre class="prettyprint"><code>git diff 8.0.x &gt; Reproduce-bug-test-case-2348119-38.patch</code></pre>

## Uploading to Issue page

Went to the issue page and added a new comment with the Patch file "Reproduce-bug-test-case-2348119-38.patch" :<figure id="attachment_3015" class="wp-caption aligncenter">

[<img class="size-full wp-image-3015" src="//lab.subinsb.com/projects/blog/uploads/2014/12/0128.png" alt="Uploading the Patch to Issue Page" width="668" height="326" />][11]<figcaption class="wp-caption-text">Uploading the Patch to Issue Page</figcaption></figure> 

Here is the comment link : <a href="https://www.drupal.org/node/2348119#comment-9409977" target="_blank">https://www.drupal.org/node/2348119#comment-9409977</a>

## Conclusion

This experience gave me new information about Testing in Drupal. I realised that emails being sent in tests doesn&#8217;t actually get sent. Instead it is gone into the Drupal mail inbox. I agree with this method as if it was not like this, many emails will go into the recipient&#8217;s or admin&#8217;s inbox.

This was my first patch into the Drupal Core. Felt a little scary before doing it as I&#8217;m taking part in the CORE of DRUPAL !

With this little experience, I gained the confidence to make patches for the Drupal Core more !

 [1]: #requires
 [2]: #reproducing-bug
 [3]: //lab.subinsb.com/projects/blog/uploads/2014/12/0123.png
 [4]: //lab.subinsb.com/projects/blog/uploads/2014/12/0124.png
 [5]: //lab.subinsb.com/projects/blog/uploads/2014/12/0125.png
 [6]: #reproducing-bug-intests
 [7]: //lab.subinsb.com/projects/blog/uploads/2014/12/0126.png
 [8]: //lab.subinsb.com/projects/blog/uploads/2014/12/0127.png
 [9]: #patching
 [10]: #uploading-to-issue-page
 [11]: //lab.subinsb.com/projects/blog/uploads/2014/12/0128.png
 [12]: #conclusion
