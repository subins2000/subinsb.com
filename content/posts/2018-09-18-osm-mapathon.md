---
title: "OpenStreetMap Mapathon on Software Freedom Day 2018"
author: Subin Siby
type: post
date: 2018-09-18T23:06:19+05:30
url: /openstreetmap-mapathon-on-software-freedom-day
categories:
  - Personal
tags:
  - fossers
  - free software
  - open source
  - open street map
---

I mentioned about me working with the FOSS club of my college in a [recent post](https://subinsb.com/18-years-revolving-around-sun/). Well, we're getting more active ! On [Software Freedom Day 2018](http://www.softwarefreedomday.org/), we did an OpenStreetMap mapathon.

[OpenStreetMap (OSM)](//osm.org) is a collaborative project to create a free editable map of the world. Unlike Google Maps, OSM is completely open source. Its data can be accessed and used by anyone. Because of this, many softwares and organizations use OSM. Examples would be `Pokemon Go`, `Facebook` and many others.

## Mapathon

An OSM mapathon is a collaborative work of mapping places on OSM. From satellite imagery and local surveys, we mark the buildings, roads, lakes etc. in OSM. By doing so, we can extract specific layers of data from the map. For example, if I need to **see all houses in a particular area** or if I need to see all the roads in an area, then I can simply use OSM to get that data. This is not possible in Google Maps.

But who will add these data ? **The world is vaaaast**. This is where communities come in. Free Software communities, students, social groups etc. contribute to OSM. They go to a certain place, go around surveying places, note the details down, head to a lab and upload the info by looking at the satellite imagery.

Nodes are created and joined to make a building, road etc. Information such as elevation, floors, name can also be added to each buildings. In case of roads, the type, lanes, width etc. are also given. Overall, each item is related with a lot of data.

## Relevance

My home state Kerala went through the worst flood in over a century. When the dams were opened, government failed to accurately evacuate places. As a result, a lot of places got flooded and people suffered.

During the flood, developers in Kerala came together to make [keralarescue.in](http://keralarescue.in) and [microid.in/keralaflood](microid.in/keralaflood). Both helped a lot during the flood. **Both used OSM**.

* keralaresuce.in - Requests for help were collected, organized and shown in maps for help to reach
* [microid.in/keralaflood](microid.in/keralaflood) - A map to see the flooded roads. Useful while travelling

Technology has evolved so much and we can use it in situations like these. **Never before in human history had the capability of doing this**. We have grown so much and we should use the technology to help people and make the world a better place.

[GIS](https://en.wikipedia.org/wiki/Geographic_information_system) can be used for knowing that **if a river overflows**, which all places would be flooded. It can also be used to exactly know which places require to be evacuated. It helps a lot in disaster management as well as prevention.

[QGIS](https://en.wikipedia.org/wiki/QGIS) is a free and open-source desktop GIS application that supports viewing, editing, and analysis of geospatial data. **OSM**'s data can be used in QGIS to do this.

But for that to happen, OSM should be populated with data and it is necessary to do it. I understood its importance during the flood.

## Event

We planned this event just 4 days before SFD. Made a Google Forms link and passed to college groups. We got **36 volunteers** through it.

Our FOSS club is called **FOSSers** and our group is on Telegram, Matrix and a mailing list. So these new members were added to each. We have grown from 2 (last year) to 80+ now ! Yay !

Together with OSM Kerala, [FSUG Thrissur](http://thrissur.fsug.in), [Kole Birders](http://kole.org.in), River Research Centre Thrissur, we conducted a mapathon on [Software Freedom Day](//softwarefreedomday.org).

We also had experts from outside who joined the program :

* [Manoj K](https://www.facebook.com/manoj.k.mohan), our alumni, Free Software Activist, Wikipedia/OpenStreetMap volunteer, coordinator of Kole Birders Collective led the mapathon.
* Ajith Kumar (GIS Expert from CSRD)
* Rajaneesh P (River Research Centre, Thrissur)
* Sujin NS (Jawarhalal Nehru Tropical Botanical Garden, Thiruvananthapuram)
* Sreenath K (Soil Test expert, Irinjalakkuda)

People who have also helped include :

* Naveen Francis (Open Street Map and Wikimedia India) gave Technical Help for the event
* [Jaison Nedumbala](https://grandalstonia.wordpress.com/) (Asst. Secretary , Koorachundu Village Panchayat and Member of [Swathandra Malayalam Computing](//smc.org.in)) gave advice in Mapping Efforts.

Led by Manoj, we were given instructions on how to map and what to do. I had no idea on how to do it. We used [tasks.openstreetmap.in](http://tasks.openstreetmap.in/) to manage tasks. In it, our district was divided into many blocks and each volunteer would pick a block and start mapping.

## Result

In under 5 hours of working, we were able to do :

* **4,388** edits
* **2,799** buildings
* **118 km** roads
* Map Lakes, Streams, etc.

Complete info of contributions can be found [here on MissingMaps](http://www.missingmaps.org/leaderboards/#/fossersvast).

![The Team](//lab.subinsb.com/projects/blog/uploads/2018/06/osm_mapathon_vast.jpg)

## Links

* [MissingMaps](http://www.missingmaps.org/leaderboards/#/fossersvast)
* [Album: Mapathon on Software Freedom Day 2018](https://www.facebook.com/pg/FOSSersVAST/photos/?tab=album&album_id=2365937880300059)
