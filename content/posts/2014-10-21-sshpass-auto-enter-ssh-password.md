---
title: Configuring SSH to Enter a Password Automatically
author: Subin Siby
type: post
date: 2014-10-21T03:30:46+00:00
url: /sshpass-auto-enter-ssh-password
categories:
  - SSH
  - Ubuntu
tags:
  - Firefox
  - Terminal

---
<p class="">
  As you know, when you <strong>SSH</strong> into a system, it will ask for the password which you have to enter everytime. But in cases where you’re <strong>SSH</strong>ing in a script or in a loop, its a really time consuming and boring thing to enter password everytime.
</p>

So, in this case you have to configure **SSH** to use a password to enter when asked. But, there is no default option in the **ssh** command to set this due to security reasons I think. Therefore we should use another way to automatically enter the password.

Fortunately, there is a program called **sshpass** that will enter the password automatically and logs in. All you have to do is, give the password and the normal **ssh** command.

You can install the **sshpass** command in an **Ubuntu** system using this :

<pre class="prettyprint"><code>sudo apt-get install sshpass</code></pre>

Yes, it&#8217;s in the official **Ubuntu** repository. **sshpass** works by fooling **ssh** into thinking that the password was entered by an interactive user.

The password should be given in the **-p** parameter and the following should be the **ssh** command. No separate host name should be given to **sshpass**. Here&#8217;s an example :

This is the normal **ssh** command which is used for opening firefox browser in system of **IP** address **192.168.10.1** :

<pre class="prettyprint"><code>ssh root@192.168.10.1 "firefox http://subinsb.com"</code></pre>

And this is how the above command become in **sshpass** assuming the password as "subin" :

<pre class="prettyprint"><code>sshpass -p "subin" ssh root@192.168.10.1 "firefox http://subinsb.com"</code></pre>

You can also use **sshpass** to use a file to read the password and more.

Note that **sshpass** is its infacy at the moment and therefore bugs are highly possible. But, for me it worked like a charm. You can read the manual of **sshpass** <a href="http://linux.die.net/man/1/sshpass" target="_blank">here at the online man page</a>.