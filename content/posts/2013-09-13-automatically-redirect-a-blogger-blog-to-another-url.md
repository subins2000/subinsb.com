---
title: Automatically Redirect A Blogger Blog To Another URL
author: Subin Siby
type: post
date: 2013-09-13T06:37:00+00:00
excerpt: 'You can redirect your blog to another URL. This redirection can be done using JavaScript&nbsp;or by adding a special meta&nbsp;tag. This redirection is useful when you want to redirect your old blog users to your new blog. The function we use in JavaSc...'
url: /automatically-redirect-a-blogger-blog-to-another-url
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 028cd43e4efe8ddce96f6ee8f36c0eea
  - 028cd43e4efe8ddce96f6ee8f36c0eea
  - 028cd43e4efe8ddce96f6ee8f36c0eea
  - 028cd43e4efe8ddce96f6ee8f36c0eea
  - 028cd43e4efe8ddce96f6ee8f36c0eea
  - 028cd43e4efe8ddce96f6ee8f36c0eea
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
  - http://sag-3.blogspot.com/2013/09/blogger-blog-redirect-to-another-url.html
categories:
  - Blogger
  - HTML
  - JavaScript
tags:
  - Blog
  - Meta
  - Redirect

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">You can redirect your blog to another <b>URL</b>. This redirection can be done using <b>JavaScript</b>&nbsp;or by adding a special <b>meta</b>&nbsp;tag. This redirection is useful when you want to redirect your old blog users to your new blog. The function we use in <b>JavaScript</b>&nbsp;is <b>window.location </b>and the <b>meta</b>&nbsp;tag has <b>http-equiv</b>&nbsp;attribute.</span></p> 
  
  <div style="text-align: center;">
    <span style="background-color: #6aa84f; border-radius: 10px; border: 2px solid black; display: inline-block; font-family: inherit; margin: 10px; padding: 10px; width: 150px;"><a href="http://simsuteam.blogspot.com/" style="color: white; text-decoration: none;" >DEMO</a></span>
  </div>
  
  <p>
    <span style="font-family: inherit;">So Let&#8217;s begin.</span><br /><span style="font-family: inherit;"><br /></span><span style="font-family: inherit;">Go to <b>Blogger </b>-> <b>Blog to be redirected&nbsp;</b>-> <b>Template</b>&nbsp;-> <b>Edit HTML</b>.</span><br /><span style="font-family: inherit;">You will get a box with a lot of text. Use one of the following methods to redirect :</span><br /><span style="font-family: inherit;"><br /></span>
  </p>
  
  <h3 style="text-align: left;">
    <b><span style="font-family: inherit; font-size: large;">1st method (JavaScript)</span></b>
  </h3>
  
  <p>
    <span style="font-family: inherit;">Add the following code just below <b><head></b>&nbsp;:</span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><script></span><br /><span style="font-family: inherit;">window.location="http://sag-3.blogspot.com";/*Change the URL to your desired one.*/</span><br /><span style="font-family: inherit;"></script></span>
    </p>
  </blockquote>
  
  <div class="separator" style="clear: both; text-align: center;">
    <span style="font-family: inherit; margin-left: 1em; margin-right: 1em;"><a href="//3.bp.blogspot.com/-A-NKJt5JK8E/UjKxcSzhWEI/AAAAAAAADAE/WTjBELsvqnA/s1600/0042.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-A-NKJt5JK8E/UjKxcSzhWEI/AAAAAAAADAE/WTjBELsvqnA/s1600/0042.png" /></a></span>
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <h3 style="text-align: left;">
    <b><span style="font-family: inherit; font-size: large;">2nd method (META Tag)</span></b>
  </h3>
  
  <div>
    <span style="font-family: inherit;">Add the following code just below <b><head></b>&nbsp;:</span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><meta content=&#8217;5;url=<span style="background-color: yellow;">http://www.REDIRECT_TO.com</span>/&#8217; http-equiv=&#8217;refresh&#8217;/></span>
    </p>
  </blockquote>
  
  <div>
    <span style="font-family: inherit;">&nbsp;Change the <span style="background-color: yellow;">yellow background</span> url to the url you want to redirect to.</span><br /><span style="font-family: inherit;"><br /></span>
  </div>
  
  <h3 style="text-align: left;">
    <b><span style="font-family: inherit; font-size: large;">What&#8217;s the Difference ?</span></b>
  </h3>
  
  <div>
    <span style="font-family: inherit;">The <b>JavaScript</b>&nbsp;redirection is executed when the page starts to load, but the meta tag method can be configured to set the no of seconds to wait before redirecting. The second is mentioned just before the <b>;url</b>. To set the no of seconds to <b>10</b>&nbsp;just use the following code :</span>
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        <span style="font-family: inherit;"><meta content=&#8217;<span style="background-color: yellow;">10</span>;url=<span style="background-color: yellow;">http://sag-3.blogsppot.com</span>/&#8217; http-equiv=&#8217;refresh&#8217;/></span>
      </p>
    </blockquote>
  </div>
  
  <div>
    <span style="font-family: inherit;">The first <span style="background-color: yellow;">yellow backgrounded</span> text is the no of seconds to wait before redirecting.</span><br /><span style="background-color: white; color: #5b5b5b; line-height: 20px;"><span style="font-family: inherit;">If there&#8217;s any problems/suggestion/feedback just echo out in the comment box below.</span></span>
  </div>
</div>