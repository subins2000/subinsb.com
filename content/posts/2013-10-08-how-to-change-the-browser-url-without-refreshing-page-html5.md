---
title: How To Change The Browser URL Without Refreshing Page – HTML5
author: Subin Siby
type: post
date: 2013-10-08T16:03:00+00:00
excerpt: 'This post was suggested by&nbsp;Sumit Kumar Pradhan. I recently saw many questions like this on Stack Overflow. Let me start explaining, there is already a function in JavaScript&nbsp;to do this special task. The function is window.history.replaceState...'
url: /how-to-change-the-browser-url-without-refreshing-page-html5
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - a5a0415a01d1776cb07ba520853c9e76
  - a5a0415a01d1776cb07ba520853c9e76
  - a5a0415a01d1776cb07ba520853c9e76
  - a5a0415a01d1776cb07ba520853c9e76
  - a5a0415a01d1776cb07ba520853c9e76
  - a5a0415a01d1776cb07ba520853c9e76
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
  - http://sag-3.blogspot.com/2013/10/change-browser-url-without-reloading-page-html-5-window-history-replaceState.html
dsq_thread_id:
  - 1962513823
  - 1962513823
  - 1962513823
  - 1962513823
  - 1962513823
  - 1962513823
categories:
  - HTML5
  - JavaScript
  - jQuery
tags:
  - Browsers
  - Suggestion
  - URL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This post was suggested by&nbsp;<a href="http://disqus.com/sumitkumarpradhan/" >Sumit Kumar Pradhan</a>. I recently saw many questions like this on <b>Stack Overflow</b>. Let me start explaining, there is already a function in <b>JavaScript</b>&nbsp;to do this special task. The function is <b>window.history.replaceState</b>. It&#8217;s a simple function that needs <b>3 </b>values.<br />This is actually a <b>HTML5</b>&nbsp;function that came out on <b>July 2012</b>&nbsp;(I think). All the latest <b>browsers</b>&nbsp;support the new function except <b>IE 9</b>.</p> 
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=ofo66cuias9wgrj&class=6" >Download</a> <a class="demo" href="http://demos.subinsb.com/change-url-without-reloading" >Demo</a>
  </div>
  
  <p>
    Here is a small example of the usage of <b>window.history.replaceState </b>:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      window.history.replaceState({},&#8217;subinsb.com&#8217;,&#8217;/html5&#8242;);
    </p>
  </blockquote>
  
  <p>
    If someone clicks a special&nbsp;<b>URL</b>&nbsp;on your site, you could also change the <b>browser</b>&nbsp;<b>URL</b> with the <b>URL</b>&nbsp;clicked by the user. Here is a small example :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      $(".link").on(&#8216;click&#8217;,function(){<br />&nbsp;window.history.replaceState({},&#8217;subinsb.com&#8217;,&#8217;/&#8217;+$(this).attr("href"));<br />&nbsp;return false;<br />});
    </p>
  </blockquote>
  
  <p>
    The <b>return false;</b>&nbsp;is used at the end of the above code to prevent the redirection of <b>browser</b> to the&nbsp;<b>URL</b>. <b>window.history.pushState</b>&nbsp;is similar to <b>window.history.replaceState</b>&nbsp;function.<br />If you want to see a demo, you can stop by <a href="http://demos.subinsb.com/change-url-without-reloading" >here</a>.</div>