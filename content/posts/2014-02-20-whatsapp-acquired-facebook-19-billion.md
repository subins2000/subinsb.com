---
title: WhatsApp Acquired By Facebook For $19 Billion
author: Subin Siby
type: post
date: 2014-02-20T16:24:34+00:00
url: /whatsapp-acquired-facebook-19-billion
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Facebook
tags:
  - Google
  - News
  - WhatsApp

---
The giant mobile messaging service **WhatsApp** has been acquired by social network giant **Facebook** for **19** billion US dollars. This is the biggest acquisition by Facebook in their 10 years. Facebook is making it&#8217;s way to enter the mobile communication world.

[<img class="aligncenter size-full wp-image-2521" alt="0104" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0104.png" width="210" height="61" />][1]

The acquisition will make the WhatsApp CEO a billionare. This acquisition will make benefit to both WhatsApp and Facebook. Both companies&#8217; CEO has been in discussions for 2 years. And the time has come for WhatsApp to sell to Facebook.

By this acquisition Facebook is getting more users in which there is a great number of young people. The employees of WhatsApp will have restricted stock units to vest over 4 years following the closing of the deal. WhatsApp C.E.O **Jan Koum** will be joining the Facebook board of directors.

There has been reports that Google tried to buy WhatsApp (<a href="http://tech.fortune.cnn.com/2014/02/20/google-whatsapp-10-billion" target="_blank">link</a>). This report is not yet verified. But there is a chance of Google trying to buy WhatsApp. Facebook & WhatsApp will totally be benefited by this acquisition.

In my opinion, WhatsApp shouldn&#8217;t be sold. More people would have joined WhatsApp in a few more years when mobile becomes the most used device in the world. It&#8217;s a real shame that WhatsApp has been sold. On an emerging era of mobile phones, products like WhatsApp have a great chance in the market. If the C.E.O Jan Koum didn&#8217;t sell WhatsApp, he would have got more that **$****16** billion by **2020**.

A very bad decision by WhatsApp. Jan Koum should have played the game like **Mark Zuckerberg** by not selling the company even if **Microsoft** comes to buy.

PS : I have never used WhatsApp.

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0104.png