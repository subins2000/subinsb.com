---
title: How to take a screenshot in Ubuntu/Linux
author: Subin Siby
type: post
date: 2012-10-12T06:56:00+00:00
excerpt: '<div dir="ltr" trbidi="on">You can take screenshots of your desktop in <b>Linux</b>. For taking screenshots you can use <b>Take Screenshot</b>&nbsp;application.<br><ul><li><b>Use shortcut keys</b></li></ul>To take a screenshot, use the following shortcut keys:<br><br><table border="1" bordercolor="#000000" cellpadding="4" cellspacing="0"><colgroup><col width="128*"><col width="128*"></colgroup><tbody><tr valign="TOP"><td width="50%"><b>Default Shortcut Keys</b></td>  <td width="50%"><b>Function</b></td> </tr><tr valign="TOP"><td width="50%">Print Screen</td>  <td width="50%">Takes a screenshot of the entire screen.</td> </tr><tr valign="TOP"><td width="50%">Alt+Print Screen</td>  <td width="50%">Takes a screenshot of the window which is active.</td> </tr></tbody></table><br><div></div><ul><li><b>From the Menubar</b></li></ul>&nbsp; &nbsp; &nbsp; Choose <b>Applications &#9656; Accessories &#9656; Take Screenshot.</b><br><div></div><ul><li><b>From the Terminal</b></li></ul>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; You can use the<br><blockquote>gnome-screenshot&nbsp;</blockquote>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; command to&nbsp;take a screenshot. The <b>gnome-screenshot</b> command takes<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a screenshot of the entire screen, and displays the Save Screenshot<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dialog. Use the Save Screenshot dialog to save the screenshot.<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; You can also use options on the <b>gnome-screenshot</b> command<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; as follows:<br>&nbsp; <br><table border="1" bordercolor="#000000" cellpadding="4" cellspacing="0"><colgroup><col width="128*"><col width="128*"></colgroup><thead><tr valign="TOP"><td width="50%"><a href="//4.bp.blogspot.com/-3FAjC8w52XI/UHe_a7ti_sI/AAAAAAAACAk/7akxt1DdqFs/s1600/Screenshot-Take+Screenshot.png" imageanchor="1"><br></a><b>Option</b></td>   <td width="50%"><b>Function</b></td>  </tr></thead><tbody><tr valign="TOP"><td height="6" width="50%">--window</td>   <td width="50%">Takes a screenshot of the window that has focus.</td>  </tr></tbody><colgroup><col width="128*"><col width="128*"></colgroup><tbody><tr valign="TOP"><td width="50%">--include-border</td>  <td width="50%">Takes a screenshot after the specified number of seconds, and    displays the Save Screenshot dialog. Use the Save Screenshot    dialog to save the screenshot.            </td> </tr><tr valign="TOP"><td width="50%">--delay=seconds</td>  <td width="50%">Takes a screenshot including the border of the window.</td> </tr><tr valign="TOP"><td width="50%">--border-effect=shadow</td>  <td width="50%">Takes a screenshot without the border of the window.</td> </tr><tr valign="TOP"><td width="50%">--border-effect=border</td>  <td width="50%">Takes a screenshot and adds a shadow bevel effect around it.</td> </tr><tr valign="TOP"><td width="50%">--interactive</td>  <td width="50%">Takes a screenshot and adds a border effect around it.</td> </tr><tr valign="TOP"><td width="50%">--remove-border</td>  <td width="50%">Opens a window that lets you set options before taking the    screenshot.</td> </tr><tr valign="TOP"><td width="50%">--help</td>  <td width="50%">Displays the options for the command.</td> </tr></tbody></table><div><a href="//4.bp.blogspot.com/-3FAjC8w52XI/UHe_a7ti_sI/AAAAAAAACAk/7akxt1DdqFs/s1600/Screenshot-Take+Screenshot.png" imageanchor="1"><img border="0" height="283" src="//4.bp.blogspot.com/-3FAjC8w52XI/UHe_a7ti_sI/AAAAAAAACAk/7akxt1DdqFs/s320/Screenshot-Take+Screenshot.png" width="320"></a></div><br><br>When you take a screenshot, the Save Screenshot dialog opens. To save the screenshot as an image file, enter the file name for the screenshot, choose a location from the drop-down list and click the Save button. You can also use the Copy to Clipboard button to copy the image to the clipboard or transfer it to another application by drag-and-drop.</div>'
url: /how-to-take-a-screenshot-in-ubuntulinux
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - eb3311c26655fae663d73ff03f54c0ab
  - eb3311c26655fae663d73ff03f54c0ab
  - eb3311c26655fae663d73ff03f54c0ab
  - eb3311c26655fae663d73ff03f54c0ab
  - eb3311c26655fae663d73ff03f54c0ab
  - eb3311c26655fae663d73ff03f54c0ab
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/how-to-take-screenshot-in-ubuntu.html
categories:
  - Linux
  - Ubuntu
tags:
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You can take screenshots of your desktop in <b>Linux</b>. For taking screenshots you can use <b>Take Screenshot</b>&nbsp;application.</p> 
  
  <ul style="text-align: left;">
    <li>
      <b>Use shortcut keys</b>
    </li>
  </ul>
  
  <p>
    To take a screenshot, use the following shortcut keys:
  </p>
  
  <table border="1" bordercolor="#000000" cellpadding="4" cellspacing="0" style="width: 100%px;">
    <colgroup> <col width="128*"></col> <col width="128*"></col> </colgroup> <tr valign="TOP">
      <td width="50%">
        <b>Default Shortcut Keys</b>
      </td>
      
      <td width="50%">
        <b>Function</b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        Print Screen
      </td>
      
      <td width="50%">
        Takes a screenshot of the entire screen.
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        Alt+Print Screen
      </td>
      
      <td width="50%">
        Takes a screenshot of the window which is active.
      </td>
    </tr>
  </table>
  
  <p>
  </p>
  
  <div style="text-align: left;">
  </div>
  
  <ul style="text-align: left;">
    <li>
      <b>From the Menubar</b>
    </li>
  </ul>
  
  <p>
    &nbsp; &nbsp; &nbsp; Choose <b>Applications ▸ Accessories ▸ Take Screenshot.</b>
  </p>
  
  <div style="text-align: left;">
  </div>
  
  <ul style="text-align: left;">
    <li>
      <b>From the Terminal</b>
    </li>
  </ul>
  
  <p>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; You can use the
  </p>
  
  <blockquote class="tr_bq">
    <p>
      gnome-screenshot&nbsp;
    </p>
  </blockquote>
  
  <p>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; command to&nbsp;take a screenshot. The <b>gnome-screenshot</b> command takes<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; a screenshot of the entire screen, and displays the Save Screenshot<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dialog. Use the Save Screenshot dialog to save the screenshot.<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; You can also use options on the <b>gnome-screenshot</b> command<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; as follows:<br />&nbsp;
  </p>
  
  <table border="1" bordercolor="#000000" cellpadding="4" cellspacing="0" style="page-break-before: always; width: 100%px;">
    <colgroup> <col width="128*"></col> <col width="128*"></col> </colgroup> <tr valign="TOP">
      <td width="50%">
        <a href="//4.bp.blogspot.com/-3FAjC8w52XI/UHe_a7ti_sI/AAAAAAAACAk/7akxt1DdqFs/s1600/Screenshot-Take+Screenshot.png" imageanchor="1" style="clear: right; float: right; margin-bottom: 1em; margin-left: 1em;"><br /></a><b>Option</b>
      </td>
      
      <td width="50%">
        <b>Function</b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td height="6" width="50%">
        &#8211;window
      </td>
      
      <td width="50%">
        Takes a screenshot of the window that has focus.
      </td>
    </tr><colgroup> <col width="128*"></col> <col width="128*"></col> </colgroup> 
    
    <tr valign="TOP">
      <td width="50%">
        &#8211;include-border
      </td>
      
      <td width="50%">
        Takes a screenshot after the specified number of seconds, and displays the Save Screenshot dialog. Use the Save Screenshot dialog to save the screenshot.
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        &#8211;delay=seconds
      </td>
      
      <td width="50%">
        Takes a screenshot including the border of the window.
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        &#8211;border-effect=shadow
      </td>
      
      <td width="50%">
        Takes a screenshot without the border of the window.
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        &#8211;border-effect=border
      </td>
      
      <td width="50%">
        Takes a screenshot and adds a shadow bevel effect around it.
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        &#8211;interactive
      </td>
      
      <td width="50%">
        Takes a screenshot and adds a border effect around it.
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        &#8211;remove-border
      </td>
      
      <td width="50%">
        Opens a window that lets you set options before taking the screenshot.
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        &#8211;help
      </td>
      
      <td width="50%">
        Displays the options for the command.
      </td>
    </tr>
  </table>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-3FAjC8w52XI/UHe_a7ti_sI/AAAAAAAACAk/7akxt1DdqFs/s1600/Screenshot-Take+Screenshot.png" imageanchor="1" style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;"><img border="0" height="283" src="//4.bp.blogspot.com/-3FAjC8w52XI/UHe_a7ti_sI/AAAAAAAACAk/7akxt1DdqFs/s320/Screenshot-Take+Screenshot.png" width="320" /></a>
  </div>
  
  <p>
    When you take a screenshot, the Save Screenshot dialog opens. To save the screenshot as an image file, enter the file name for the screenshot, choose a location from the drop-down list and click the Save button. You can also use the Copy to Clipboard button to copy the image to the clipboard or transfer it to another application by drag-and-drop.
  </p>
</div>