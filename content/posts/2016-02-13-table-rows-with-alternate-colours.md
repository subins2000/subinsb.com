---
title: Create Table Rows With Alternate Colours in PHP
author: Subin Siby
type: post
date: 2016-02-13T04:31:02+00:00
url: /table-rows-with-alternate-colours
categories:
  - PHP
  - Program
tags:
  - Table
  - UI

---
If you have a lot of rows on a table, users will find it difficult to find which cell is in what row. This is because all the rows have the same colour.

If we alternate the colours, then the table will be more understandable. We can alternate colours odd-even wise or by 3 rows etc.

It can be easily done in PHP. I will tell you two ways to do it.

Suppose this is how we output rows :

<pre class="prettyprint"><code>for($i=0;$i &lt; 10;$i++){
  echo "&lt;tr&gt;&lt;td&gt;What's Up ?&lt;/td&gt;&lt;td&gt;Sky (Troll Face)&lt;/td&gt;&lt;/tr&gt;";
}</code></pre>

## Odd-Even

Now let&#8217;s add blue colour to odd rows and green colour to even rows.

<pre class="prettyprint"><code>for($i=0;$i &lt; 10;$i++){
  $bg_color = $i % 2 === 0 ? "green" : "blue";
  echo "&lt;tr style='background-color: ". $bg_color .";'&gt;&lt;td&gt;What's Up ?&lt;/td&gt;&lt;td&gt;Sky (Troll Face)&lt;/td&gt;&lt;/tr&gt;";
}
</code></pre>

The table will looks something like this (forget the row contents) :<figure class="wp-caption aligncenter">

[<img title="Table with alternate rows having different background colors" src="//lab.subinsb.com/projects/blog/uploads/2016/02/table-alternate-color-odd-even.png" alt="Table with alternate rows having different background colors" width="949" height="470" />][1]<figcaption class="wp-caption-text">Table with alternate rows having different background colors</figcaption></figure> 

You can also add class to the row instead of setting the "style" attribute.

## Multiple of A Number &#8216;n&#8217;

You can change colour of every 3rd element or nth element too :

<pre class="prettyprint"><code>$n = 3;
for($i=0;$i &lt; 10;$i++){
  $bg_color = $i % $n === 0 ? "green" : "blue";
  echo "&lt;tr style='background-color: ". $bg_color .";'&gt;&lt;td&gt;What's Up ?&lt;/td&gt;&lt;td&gt;Sky (Troll Face)&lt;/td&gt;&lt;/tr&gt;";
}
</code></pre>

In the above case, all 3rd rows will have green background color and others would have a blue color :<figure class="wp-caption aligncenter">

[<img title="Table with alternate 3rd rows having different background colors" src="//lab.subinsb.com/projects/blog/uploads/2016/02/table-alternate-color-multiple-of-3.png" alt="Table with alternate 3rd rows having different background colors" width="957" height="412" />][2]<figcaption class="wp-caption-text">Table with alternate 3rd rows having different background colors</figcaption></figure> 

You can change the value of $n as you like to change the row which will have the special background color.

Life is Good 🙂

 [1]: //lab.subinsb.com/projects/blog/uploads/2016/02/table-alternate-color-odd-even.png
 [2]: //lab.subinsb.com/projects/blog/uploads/2016/02/table-alternate-color-multiple-of-3.png