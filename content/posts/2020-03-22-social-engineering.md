---
title: Rickroll, Love Calculator & Social Engineering
author: Subin Siby
type: post
date: 2020-03-22T00:00:00+00:00
lastmod: 2020-03-22T00:00:06+00:00
url: /rickroll-love-calculator-social-engineering
aliases:
    - /rickroll
categories:
  - Tech
tags:
  - Music
  - Facebook
  - Instagram
  - XSS
---

A global pandemic is going on and we're all staying in our homes. College's been closed for a week now. Yes, it gets boring staying in home all day. But, we must !

Out of boredom and a wish to cheer up people in my circle, I made a website which ends up rickrolling the visitor. It took me an hour to make it.

If you're reading this, you probably got rickrolled by me ;) 😉

In this post, I'm gonna talk about [Social Engineering](https://en.wikipedia.org/wiki/Social_engineering_(security)) and the experience I've had with it.

## What's A Rickroll ?

There's a [whole Wikipedia article](https://en.wikipedia.org/wiki/Rickrolling) on it :D

Have you heard the awesome song & dance moves of Rick Astley in the song "[Never Gonna Give You Up](https://invidio.us/watch?v=dQw4w9WgXcQ)" ([YouTube](https://www.youtube.com/watch?v=dQw4w9WgXcQ)) ? IF NOT, YOU MUST !

In the early internet days, people used to prank each other with this. You would see a link like "The cutest cat ever" and when you click it, it goes to the Rick Astley's dance moves in Never Gonna Give You Up. You just got rickrolled !

![Rickroll gif](//lab.subinsb.com/projects/blog/uploads/2020/03/rickroll.gif)

## See who has visited your Instagram profile ! OMG !!

I haven't ever rickrolled anyone before (maybe in reddit, but that doesn't count), but I've been rickrolled.

If I'm gonna rickroll them, how can I convince them to click the link I send them ? I didn't want it to be like a normal [here's the cutest cat ever] cause 50% people likes dogs :P 😛. It should be something creative, and harmless fun !

What's the one thing millenials/teenagers/youth has in common ? Enjoying memes ? Yes. Also, majority of them are on Instagram. Some people just use Instagram for the memes. It's the safe haven for kids these days. It was Facebook before, but our parents' generation has overcrowded there 🤷.

Majority of "Instagrammers" do want to know who's seeing their profile. Teenagers, eh ? They want all the attention 😆.

So that's what I chose. I made the website, this message :

> I just found a new secret endpoint in Instagram that allows you to know who has visited your profile.
>
> Find out who has visited your Instagram profile : https://cdpn.io/subins2000/fullpage/QWbVWry

and sent it to people (plus WhatsApp status / Instagram story). And boy oh boy, didn't I trick many people ! 👿

![Jim-Best Prank Ever gif](//lab.subinsb.com/projects/blog/uploads/2020/03/jim-best-prank-ever.gif)

### Why I did it ?

* Fun, Fun, Fun :D !!
* Kids these days don't know what a rickroll is. I'm enlightening them about the good ol' Internet days. Don't `Ok Boomer` me :P
* To demonstrate social engineering
* For motivation to write this blog (It's the success of the prank that made me write this)
* Ay, come on ! You'll obviously smile when you get rickrolled. I did make you laugh eh ;) 😉

### How it works

I made the website in CodePen. You can see the [pen here](https://codepen.io/subins2000/pen/QWbVWry).

It's simple. An alright looking form which asks user to enter their Instagram username. When user clicks the button, a purposeful delay is made (`setTimeout`) before showing the Rick Astley gif and the YouTube video.

**The video should have autoplayed**. Autoplaying videos work in desktop browsers, but is [disabled in mobile browsers](https://stackoverflow.com/a/40537827/1372424). It would have been wayyyy better if the music autoplayed 😭. But, sadly, mobile browsers won't allow it :(

Hence, I added the gif and then the video. The gif is 2MB long and it'd take time to load in a slow connection. Pffttt.

This rickroll ain't perfect (-\_-), but it works.

## Love Calculator

IIRC (If I Remember Correctly), it was in 2016 that I came across a message like this in the school group on WhatsApp :

> Find how match you and your crush is. Check the love calculator : https://linkto.that.love.calculator.com

Pretty bored at the time, I checked out this link.

Pretty stupid at the time, I typed in the original name of me and my crush.

Pretty nishku (innocent) at the time, I submitted the form without any thinking.

The screen showed "Calculating... Calculating..." and after some seconds, it showed :

> YOU'VE BEEN PRANKED !! Your name and your crush's name has been sent to v*****99@gmail.com

😨😱 ARGHHHHHHH !!!! OH NO !!!

At the time, it was a secret known only to me and me only. How stupid was I to believe it !! I immediately sent a message to the guy who set it up and begged him to not share it. I remember his exact reply :

> Ithra valiya computer daavaayittum neeyithil veenallo
>
> You even fell for it, you big computer geek

That's the point. Anyone can be fooled or tricked or pranked.

End of story: He didn't share my secret, though he kept ridiculing me when we saw again face-to-face. Ah, embarassing !

Anyone can do this prank with this Love Calculator website. You only gotta sign-up with your email and prank anyone you want to. I don't remember the site, otherwise would have linked here. PS: There's lot of ads in it, so it's a good revenue booster for the site owner 😂.

## Social Engineering

[Social Engineering](https://en.wikipedia.org/wiki/Social_engineering_(security)) in the context of information security is defined as :

> any act that influences a person to take an action that may or may not be in their best interests.

Love Calculator is definitely an example of Social Engineering. Rickroll can be considered as an example of it too.

I had one more experience of being a victim to Social Engineering, a severe one, it was [self XSS](https://en.wikipedia.org/wiki/Self-XSS) 😨

### See who has visited your Facebook profile ! OMG !!

IIRC, it was in 2014 that I saw a Facebook friend adding me to a group in Facebook. The group was titled **"100% Working. See who has visited your Facebook profile"**. It also had a pretty convincing header image (thinking now, it probbably didn't :P).

A post in the group specifically had the instructions on how to find it :

1. Press `F12`
2. Go to the `Console` tab
3. Paste this long code (script) there
4. Press Enter key

Whoa! Cool! You have to paste a weird looking code ? Pretty legit.

I did exactly as it said.

A window popped up, showing some of my Facebook friends in the list, I believed it.

In fact, all my Facebook friends were added to this group by me ! In the background, this script (the long code) picked up my friends list and added each of them to this **"100% Working. See who has visited your Facebook profile"** group. And the list was just random people. This scam would then continue with other people and so on and on.

This is called [**Self-XSS**](#self-xss).

### Self XSS

As per [Wikipedia](https://en.wikipedia.org/wiki/Self-XSS) :

> In a self-XSS attack, the victim of the attack unknowingly runs malicious code in their own web browser, thus exposing personal information to the attacker.

In the previous "Find Facebook profile visitors" mishap, the malicious code added all of my friends to that group.

This malicious code can be changed to do much dangerous things :

* Send rude messages to everyone in your friend list
* Unfriend all your friends
* Post obscene messages in your wall

Basically, the script can do anything like what you can normally do, at many times faster than a human.

Since this scam became rampant, Facebook started putting this in browser console :

![Self-XSS Facebook warning in Firefox Browser console](//lab.subinsb.com/projects/blog/uploads/2020/03/selfxss-facebook-warning-in-browser-console.png)

Self-XSS can be used for much worse things with your banking site, financial services etc.

Don't do any things that ask you to open browser console and paste some code (unless you absolutely know what you're doing and know the code). The browser console can be used for good things too (TODO: Write about it in a new post).

## Conclusion

So I've shared my experience with Social Engineering and being a victim of it. I have one more experience to share, but later :)

This can happen to anyone. You should be careful putting your trust on people on the internet. There are many nice people, yes, but there are others too.

Be careful on the internet.

Stay Safe.

Garnier.

PS: Since yesterday, I dug into a hole of Rick Astley interviews and videos. And I can't stop listening to "Never Gonna Give You Up". Send help.