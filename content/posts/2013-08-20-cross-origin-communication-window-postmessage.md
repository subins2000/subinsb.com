---
title: Cross Origin Communication – window.postMessage
author: Subin Siby
type: post
date: 2013-08-20T03:30:00+00:00
excerpt: "The Window.postMessage&nbsp;helps to make scripts talk over different domain connections. When you call a function on an external iframe from the parent window, Javascript&nbsp;won't allow it.That's because of the Cross Domain Policy&nbsp;that makes th..."
url: /cross-origin-communication-window-postmessage
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
  - http://sag-3.blogspot.com/2013/08/cross-domain-talk-window-postmessage.html
syndication_item_hash:
  - e8102b77fec33ac4b8a2c508c9d6d076
  - e8102b77fec33ac4b8a2c508c9d6d076
  - e8102b77fec33ac4b8a2c508c9d6d076
  - e8102b77fec33ac4b8a2c508c9d6d076
  - e8102b77fec33ac4b8a2c508c9d6d076
  - e8102b77fec33ac4b8a2c508c9d6d076
dsq_thread_id:
  - 1966567006
  - 1966567006
  - 1966567006
  - 1966567006
  - 1966567006
  - 1966567006
categories:
  - JavaScript
tags:
  - Cross-Domain
  - window

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  The <b>Window.postMessage</b>&nbsp;helps to make scripts talk over different domain connections. When you call a function on an external iframe from the parent window, <b>Javascript</b>&nbsp;won&#8217;t allow it.<br />That&#8217;s because of the <b>Cross Domain Policy</b>&nbsp;that makes this not possible. But it&#8217;s possible since <b>Javascript</b>&nbsp;introduced <b>postMessage</b>&nbsp;function.</p> 
  
  <p>
    Suppose you want to call a function on an cross domain <b>iframe</b> with id <b>monu</b>. Let the function be <b>subins()</b>. Here&#8217;s how to do it using <b>postMessage</b>&nbsp;technic.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      window.frames[&#8216;monu&#8217;].postMessage(&#8216;alert&#8217;,&#8217;*&#8217;);
    </p>
  </blockquote>
  
  <p>
    The <b>alert</b>&nbsp;in the code above is not a function. The parameter <b>targetOrigin</b>&nbsp;is set to <b>*</b>&nbsp;because we don&#8217;t know the url of the iframe. Now the code in the <b>iframe</b>&nbsp;:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      function subins(){<br />&nbsp;alert("Support Wikipedia");<br />}<br />window.addEventListener("message",function(e){<br />&nbsp;if(e.origin==&#8217;http://example.com&#8217;){<br />&nbsp; subins();<br />&nbsp;}<br />},false);
    </p>
  </blockquote>
  
  <p>
    When a <b>postmessage</b>&nbsp;callback is received on the <b>iframe</b>&nbsp;the origin where the <b>postMessage</b>&nbsp;callback is called is checked on the script in <b>iframe </b>and if it is true, <b>subins()</b>&nbsp;is called and it will alert the text <b>Support Wikipedia</b>.<br />So now you know how to communicate between <b>cross</b>&nbsp;<b>domains</b>. Code Great.</div>