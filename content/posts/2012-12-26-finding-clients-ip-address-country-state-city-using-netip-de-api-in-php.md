---
title: Finding Client’s IP address, Country, State, City using NetIP.de api in PHP
author: Subin Siby
type: post
date: 2012-12-26T05:39:00+00:00
excerpt: "If you want to get client's IP address you will use $_SERVER['REMOTE_ADDR'] function in PHP.But you will not get the real IP&nbsp;address of the client. To get the real IP&nbsp;address You can use the method I used.Also With this method you can also fi..."
url: /finding-clients-ip-address-country-state-city-using-netip-de-api-in-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
  - http://sag-3.blogspot.com/2012/12/find-ip-country-state-city.html
syndication_item_hash:
  - e7f236441b4f3ab1e70b7a807d72fd3e
  - e7f236441b4f3ab1e70b7a807d72fd3e
  - e7f236441b4f3ab1e70b7a807d72fd3e
  - e7f236441b4f3ab1e70b7a807d72fd3e
  - e7f236441b4f3ab1e70b7a807d72fd3e
  - e7f236441b4f3ab1e70b7a807d72fd3e
dsq_thread_id:
  - 1962319166
  - 1962319166
  - 1962319166
  - 1962319166
  - 1962319166
  - 1962319166
categories:
  - PHP
tags:
  - Geographic
  - IP
  - NetIP.de

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you want to get client&#8217;s IP address you will use <b>$_SERVER[&#8216;REMOTE_ADDR&#8217;] </b>function in <b>PHP</b>.<br />But you will not get the real <b>IP</b>&nbsp;address of the client. To get the real <b>IP</b>&nbsp;address You can use the method I used.<br />Also With this method you can also find the City, State, Country client&#8217;s in.<br />The code is pretty simple. With the help of <b><a href="http://netip.de/">NetIP.de</a></b>&nbsp;it was pretty easy.</p> 
  
  <blockquote class="tr_bq">
    <p>
      $response=@file_get_contents(&#8216;http://www.netip.de&#8217;);if (empty($response)){throw new InvalidArgumentException("Error contacting Geo-IP-Server");}$patterns=array();<span class="pln" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">$patterns</span><span class="pun" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">[</span><span class="str" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">"state"</span><span class="pun" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">]</span><span class="pln" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">&nbsp;</span><span class="pun" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">=</span><span class="pln" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">&nbsp;</span><span class="str" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">&#8216;#State/Region: (.*?)<br#i&#8217;</span><span class="pun" style="border: 0px; line-height: 18px; margin: 0px; padding: 0px; vertical-align: baseline;">;</span>$patterns["IP"] = &#8216;#IP: (.*?)&nbsp;#i&#8217;;$patterns["country"] = &#8216;#Country: (.*?)&nbsp;#i&#8217;;$patterns["city"] = &#8216;#City: (.*?)<br#i&#8217;;foreach ($patterns as $key => $pattern){$ipInfo[$key] = preg_match($pattern,$response,$value) && !empty($value[1]) ? $value[1] : &#8216;not found&#8217;;} $uct=$ipInfo["state"];$uip=$ipInfo["IP"];$uci=$ipInfo["city"];$ipInfo=explode(&#8216;-&#8216;,$ipInfo["country"]);$ucs=$ipInfo[0];$uco=$ipInfo[1];
    </p>
  </blockquote>
  
  <p>
    Below are the variables for different purposes.
  </p>
  
  <p>
    $uip //IP address<br />$uco //Client&#8217;s Country<br />$uci //Client&#8217;s City<br />$ucs //Client&#8217;s Country in Short Form(eg: if user is from INDIA then the variable will be "IN")<br />$uct //Client&#8217;s State
  </p>
  
  <p>
    You should <b>echo</b>&nbsp;the variables as needed. Hope it helps.</div>