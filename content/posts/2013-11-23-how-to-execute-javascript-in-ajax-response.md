---
title: Executing JavaScript Code In AJAX Response
author: Subin Siby
type: post
date: 2013-11-23T15:21:46+00:00
url: /how-to-execute-javascript-in-ajax-response
authorsure_include_css:
  - 
  - 
  - 
categories:
  - AJAX
  - HTML
  - JavaScript
  - jQuery

---
This is the first post I am creating related to the Open project. This trick is used for posting posts and making that post appear at the first of the post feed. This tutorial will guide you on how to execute JavaScript code which is in the response of **AJAX** call. We are going to use **jQuery **for both AJAX handling.

This is the **AJAX** response :

<pre class="prettyprint"><code>&lt;script&gt;alert("Hello Subin");&lt;/script&gt;</code></pre>

With a plain **AJAX** response, the code won&#8217;t execute. Even If you append the **HTML** code in **AJAX** response to the **DOM**, it won&#8217;t execute. To make it execute, you should use **eval** function. Some say **eval is evil**, but there is no other way for this purpose.

You should need a div wrapper that will store the **AJAX** responses :

<pre class="prettyprint"><code>&lt;div id="ajax_responses" style="display:none;"&gt;&lt;/div&gt;</code></pre>

The following code will execute an **AJAX** call and handle the **AJAX** response in a way to make the code in the **AJAX** response execute.

<pre class="prettyprint"><code>&lt;script&gt;
 $.post("ajaxResponse.php",{data : "some data"}, function(response){
   $("#ajax_responses").html(response);
   $("#ajax_responses").find("script").each(function(){
     eval($(this).text());
   });
 });
 &lt;/script&gt;</code></pre>

## What Happens ?

When the request is made, the server will return the **JavaScript** code inside **script** tag. **jQuery** will append the **HTML** response in **#ajax_responses** element. Then **jQuery** will loop through the **script** elements in the **#ajax_responses** element. Then the contents of the **script** tag is passed through **eval** function and the code will execute.