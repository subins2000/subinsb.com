---
title: Using Regex to validate E-mail and Phone Number
author: Subin Siby
type: post
date: 2013-01-26T08:45:00+00:00
excerpt: 'Here is the RegEx&nbsp;expression to validate Phone number and E-mail.RegEx&nbsp;expression for validating E-mail :/^(([^&lt;&gt;()[].,;:s@"]+(.[^&lt;&gt;()[].,;:s@"]+)*)|(".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-z...'
url: /using-regex-to-validate-e-mail-and-phone-number
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
  - http://sag-3.blogspot.com/2013/01/using-regex-to-validate-e-mail-and.html
syndication_item_hash:
  - 7ba88695111a51990ae172183ce76557
  - 7ba88695111a51990ae172183ce76557
  - 7ba88695111a51990ae172183ce76557
  - 7ba88695111a51990ae172183ce76557
  - 7ba88695111a51990ae172183ce76557
  - 7ba88695111a51990ae172183ce76557
categories:
  - JavaScript
  - Mobile
  - RegEx
tags:
  - Email

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">Here is the <b>RegEx</b>&nbsp;expression to validate Phone number and E-mail.</span></p> 
  
  <div>
    <span style="font-family: inherit;"><b>RegEx</b>&nbsp;expression for validating E-mail :</span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">/^(([^<>()[].,;:s@"]+(.[^<>()[].,;:s@"]+)*)|(".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/</span>
    </p>
  </blockquote>
  
  <div>
    <span style="font-family: inherit;">and the&nbsp;<b>RegEx</b>&nbsp;expression for validating Phone number :</span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">/^(?([0-9]{3}))?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/</span>
    </p>
  </blockquote>
  
  <div>
    <span style="font-family: inherit;">Here is an example of usage of these <b>RegEx</b>&nbsp;expressions in <b>JavaScript</b>.</span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">var emailregex=/^(([^<>()[].,;:s@"]+(.[^<>()[].,;:s@"]+)*)|(".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA-Z-0-9]+.)+[a-zA-Z]{2,}))$/;</span><span style="font-family: inherit;">var phoneregex=/^(?([0-9]{3}))?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;</span><span style="font-family: inherit;">if (emailregex.test(<span style="color: red;">string</span>)){</span><span style="font-family: inherit;">// valid email</span><span style="font-family: inherit;">}else{</span><span style="font-family: inherit;">// Invalid email</span><span style="font-family: inherit;">}</span><span style="font-family: inherit;">if (phoneregex.test(<span style="color: red;">string</span>)){</span><span style="font-family: inherit;">// valid phone number</span><span style="font-family: inherit;">}else{</span><span style="font-family: inherit;">// Invalid phone number</span><span style="font-family: inherit;">}</span>
    </p>
  </blockquote>
  
  <div>
    <span style="font-family: inherit;">You can replace string with whatever you want. For Example:</span>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">if (phoneregex.test($("#phone").val())){</span><span style="font-family: inherit;">// valid phone number</span><span style="font-family: inherit;">}else{</span><span style="font-family: inherit;">// Invalid phone number</span><span style="font-family: inherit;">}</span>
    </p>
  </blockquote>
  
  <p>
    Enjoy !!.
  </p>
</div>