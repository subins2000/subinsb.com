---
title: Check If A String Is JSON in PHP
author: Subin Siby
type: post
date: 2014-03-08T07:21:30+00:00
url: /php-check-if-string-is-json
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - error
  - Function
  - JSON

---
In my previous post, I gave the **isHTML** function that returns if a string is HTML or not. In this post, I will give you a simple function of different ways that helps in the detection of valid **JSON** string in **PHP**. The function I&#8217;m giving are fast, easy and doesn&#8217;t take up too much CPU memory.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/php/isJSON" target="_blank">Demo</a>
</div>

## Method 1

**JSON** (JavaScript Object Notation) can be made in to a PHP object using **json_decode**. If the return is not an object, the string we gave is not JSON. This is the principal of Method 1 function.

<pre class="prettyprint"><code>function isJSON($string){
   return is_string($string) && is_array(json_decode($string, true)) ? true : false;
}</code></pre>

## Method 2

Requires PHP **versions 5.3 or more**.

<pre class="prettyprint"><code>function isJSON($string){
   return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
}
</code></pre>

Method 2 does 3 checks for calculating if the string given to it is **JSON**. So, it is the most perfect one, but it&#8217;s slower than the other.

## Usage

If you have used any methods, the usage is the same.

Checking when the string given is perfectly valid :

<pre class="prettyprint"><code>$string = '{"host" : "demos.subinsb.com"}';
if(isJSON($string)){
 echo "It's JSON";
}</code></pre>

The above code when executed will echoes **It&#8217;s JSON**.**
  
**

Checking when the string given is not valid :

<pre class="prettyprint"><code>$string = '{"host : "demos.subinsb.com"}do you think this is valid ?';
if(!isJSON($string)){
 echo "Not JSON";
}</code></pre>

The above code when executed will print **Not JSON**.