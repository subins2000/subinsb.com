---
title: Append Contents to Files in Bash
author: Subin Siby
type: post
date: 2014-11-19T03:30:08+00:00
url: /bash-file-append-content
categories:
  - Short Post
tags:
  - BASH
  - File
  - Text

---
In **Bash**, we can add contents to file easily using the **>** character. Like this :

<pre class="prettyprint"><code>echo "myContents" &gt; ~/myfile.txt</code></pre>

What if you want to append contents to the file without overwriting ? The easiest way to do this is change the ">" to ">>". Just by making this small change, appending will be done :

<pre class="prettyprint"><code>echo "myContents" &gt;&gt; ~/myfile.txt</code></pre>

Even if the file doesn&#8217;t exist, **Bash** will create one with the contents given to append.

You can also use **cat** to append :

<pre class="prettyprint"><code>cat &gt;&gt; ~/file.txt &lt;&lt; EOF
This is a content that can have any type of characters
single quotes and double quotes can be included.
EOF</code></pre>

&nbsp;