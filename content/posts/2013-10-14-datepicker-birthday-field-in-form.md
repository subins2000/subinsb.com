---
title: How To Add A Simple Birthday Field In A Form Using jQuery
author: Subin Siby
type: post
date: 2013-10-14T14:55:00+00:00
excerpt: "There are certain laws on the Web. One of them is the COPPA law&nbsp;which clarifies the minimum age of a user to signup for a site. The minimum age is 13. If any site allows users under 13&nbsp;to signup, it is considered illegal. So it's necessary to..."
url: /datepicker-birthday-field-in-form
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
  - http://sag-3.blogspot.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html
dsq_thread_id:
  - 1957201086
  - 1957201086
  - 1957201086
  - 1957201086
  - 1957201086
  - 1957201086
syndication_item_hash:
  - 4b30e91e68046dffd1ac67accfbc68c2
  - 4b30e91e68046dffd1ac67accfbc68c2
  - 4b30e91e68046dffd1ac67accfbc68c2
  - 4b30e91e68046dffd1ac67accfbc68c2
  - 4b30e91e68046dffd1ac67accfbc68c2
  - 4b30e91e68046dffd1ac67accfbc68c2
categories:
  - CSS
  - HTML
  - JavaScript
  - jQuery
  - PHP
tags:
  - Age
  - Birthdate
  - Demo
  - Download

---
<div dir="ltr" style="text-align: left;">
  There are certain laws on the <b>Web</b>. One of them is the <b>COPPA </b>law which clarifies the minimum age of a user to signup for a site. The minimum age is <b>13</b>. If any site allows users under <b>13</b> to signup, it is considered <b>illegal</b>. So it&#8217;s necessary to add a <b>birthday</b> field in site&#8217;s <b>Sign-Up</b> form. So I&#8217;m going to tell you how to add a <b>simple, stylish</b> <b>birthday</b> field to your <b>Sign-Up</b> form.</p> 
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=4i3stvjau05bh0o&class=10">Download</a> <a class="demo" href="http://demos.subinsb.com/birthday-form-jquery">Demo</a>
  </div>
  
  <p>
    Extract <b>datepicker.js</b> and <b>datepicker.css </b>from the archive downloadable from above link. Create <b>index.php</b> (Containing Form) & <b>birthchecker.js</b>.
  </p>
  
  <h3 style="text-align: left;">
    <b><span style="font-size: large;">index.php</span></b>
  </h3>
  
  <blockquote class="tr_bq">
    <p>
      <!DOCTYPE html><br /> <html><br /> <head><br /> <link href="datepicker.css" rel="stylesheet"/><br /> <script src="http://code.jquery.com/jquery-latest.min.js"></script><br /> <script src="datepicker.js"></script><br /> <script src="birthchecker.js"></script><br /> <title>jQuery Birthday Form Field & Checker</title><br /> </head><br /> <body><br /> <div id="content" style="margin-top:10px;height:100%;"><br /> <center><h1>jQuery Birthday Form Field & Checker</h1></center><br /> <form method="POST" action="submit.php"><br /> <table class="table"><tbody><br /> <tr><br /> <td>Birthday :</td><br /> <td><input id="birth" name="birth" value="2000-01-20&#8243;/><div id="ermsg">Users under 13 are not allowed.</div></td><br /> <td>YY-MM-DD</td><br /> </tr><br /> <tr><br /> <td></td><br /> <td><input type="submit"/></td><br /> </tr><br /> </tbody></table><br /> </form><br /> </div><br /> <style><br /> input{<br /> border:none;<br /> padding:8px;<br /> }<br /> #ermsg{<br /> display:none;<br /> color:red;<br /> }<br /> </style><br /> <!&#8211; http://www.subinsb.com/2013/10/add-simple-birthday-field-in-form-jquery-and-validate-in-php.html &#8211;><br /> </body><br /> </html>
    </p>
  </blockquote>
  
  <h3 style="text-align: left;">
    <b><span style="font-size: large;">birthchecker.js</span></b>
  </h3>
  
  <blockquote class="tr_bq">
    <p>
      $(document).ready(function(){<br /> v=$(&#8216;#birth&#8217;);<br /> function above13(v){<br /> cur=new Date();<br /> cls=new Date(v.val());<br /> age = new Date(cur &#8211; cls).getFullYear() &#8211; 1970;<br /> console.log(age);<br /> if(age<13){<br /> v.css("background","red");<br /> $("#ermsg").show();<br /> }else{<br /> v.css("background","white");<br /> $("#ermsg").hide();<br /> }<br /> }<br /> v.DatePicker({<br /> format:&#8217;Y-m-d&#8217;,<br /> date: v.val(),<br /> current: v.val(),<br /> starts: 1,<br /> position: &#8216;r&#8217;,<br /> onBeforeShow:function(){<br /> v.DatePickerSetDate(v.val(), true);<br /> },<br /> onChange:function(formated, dates){<br /> v.val(formated);<br /> v.DatePickerHide();<br /> above13(v);<br /> }<br /> });<br /> v.on("keyup",function(){<br /> above13(v);<br /> });<br /> });
    </p>
  </blockquote>
  
  <h3 style="text-align: left;">
    <b><span style="font-size: large;">submit.php</span></b>
  </h3>
  
  <blockquote class="tr_bq">
    <p>
      <?<br /> function age($birthday){<br /> list($day,$month,$year) = explode("/",$birthday);<br /> $year_diff  = date("Y") &#8211; $year;<br /> $month_diff = date("m") &#8211; $month;<br /> $day_diff   = date("d") &#8211; $day;<br /> if ($day_diff < 0 && $month_diff==0){$year_diff&#8211;;}<br /> if ($day_diff < 0 && $month_diff < 0){$year_diff&#8211;;}<br /> return $year_diff;<br /> /* http://www.subinsb.com/2013/05/the-best-age-calculation-code-in-php.html */<br /> }<br /> $birth=$_POST[&#8216;birth&#8217;];<br /> if(isset($_POST) && $birth!="){<br /> $birth=explode("-",$birth);<br /> $nbir=$birth[2]."/".$birth[1]."/".$birth[0];<br /> $age=age($nbir);<br /> echo "Your Age is $age.<br/><a href=&#8217;index.php&#8217;>Click Here To Go To Demo Form</a>";<br /> }<br /> ?>
    </p>
  </blockquote>
  
  <p>
    That&#8217;s all. If you have any problems / suggestions / feedback, comment it. I am here 10/7.
  </p>
</div>