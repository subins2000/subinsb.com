---
title: How to make a text bold in Google + post.
author: Subin Siby
type: post
date: 2012-09-29T12:54:00+00:00
excerpt: |
  How to make a text bold in Google + post.Did you ever see some post with bold text. Do you want to add this to your Google + post. Here's How.When typing a text add "*" to the starting word and ending word.For example type "*Text*" without the quotatio...
url: /how-to-make-a-text-bold-in-google-post
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - 59fc1d4cca66f830ac80ef436fa38020
  - 59fc1d4cca66f830ac80ef436fa38020
  - 59fc1d4cca66f830ac80ef436fa38020
  - 59fc1d4cca66f830ac80ef436fa38020
  - 59fc1d4cca66f830ac80ef436fa38020
  - 59fc1d4cca66f830ac80ef436fa38020
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
  - http://sag-3.blogspot.com/2012/09/how-to-make-text-bold-in-google-post.html
tags:
  - Google

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <h2 style="text-align: left;">
    <b style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;"><span style="font-size: large;">How to make a text bold in Google + post.</span></b>
  </h2>
  
  <p>
    <b><br style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;" /></b><span style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;">Did you ever see some post with bold text. Do you want to add this to your Google + post. Here&#8217;s How.</span><br style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;" /><span style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;">When typing a text add "*" to the starting word and ending word.</span><br style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;" /><span style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;">For example type "*Text*" without the quotations and when you post the word, "Text" will change to a bold text.</span><br /><br style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;" /><span style="background-color: white; font-family: arial, sans-serif; line-height: 18px; text-align: -webkit-auto;"><span style="font-size: large;"><a href="https://plus.google.com/100940821362767953768/posts/VoKr8nhQrKr" >DEMO</a></span></span></div>