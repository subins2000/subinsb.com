---
title: Selecting a particular parent in Jquery
author: Subin Siby
type: post
date: 2012-11-09T17:59:00+00:00
excerpt: "If you want to select a particular parent div. Then you should use parents()&nbsp;function. If you use parent()&nbsp;function It won't work. For Example:index.html&lt;div class='main' id='p1'&gt;&nbsp;&lt;div id='p2'&gt;&nbsp;&nbsp;&lt;div id='p3'&gt;&..."
url: /selecting-a-particular-parent-in-jquery
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 89519eced72d592bad523307f82740ef
  - 89519eced72d592bad523307f82740ef
  - 89519eced72d592bad523307f82740ef
  - 89519eced72d592bad523307f82740ef
  - 89519eced72d592bad523307f82740ef
  - 89519eced72d592bad523307f82740ef
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
  - http://sag-3.blogspot.com/2012/11/selecting-particular-parent-in-jquery.html
categories:
  - HTML
  - JavaScript
  - jQuery

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you want to select a particular parent div. Then you should use <b>parents()</b>&nbsp;function. If you use <b>parent()</b>&nbsp;function It won&#8217;t work. For Example:<br /><b>index.html</b></p> 
  
  <blockquote class="tr_bq">
    <p>
      <div class=&#8217;main&#8217; id=&#8217;p1&#8242;><br />&nbsp;<div id=&#8217;p2&#8242;><br />&nbsp;&nbsp;<div id=&#8217;p3&#8242;><br />&nbsp; &nbsp; <a id=&#8217;click&#8217;>This is the a tag!!</a><br />&nbsp; </div><br />&nbsp;</div><br /></div>
    </p>
  </blockquote>
  
  <p>
    If you want to get the <b>id </b>of the main parent<b>&nbsp;</b>which is <b>.main</b>&nbsp;then you should use <b>Jquery</b>&nbsp;code like this:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      $("#click").live(&#8216;click&#8217;, function(){<br />alert($(this).parents(".main").attr(&#8216;id&#8217;));<br />});
    </p>
  </blockquote>
  
  <p>
    The code above will alert the <b>id</b>&nbsp;of the main parent. Try it out yourself.</div>