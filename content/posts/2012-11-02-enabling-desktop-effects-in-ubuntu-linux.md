---
title: Enabling Desktop Effects in Ubuntu Linux
author: Subin Siby
type: post
date: 2012-11-02T15:54:00+00:00
excerpt: 'When you are using Windows 7&nbsp;you will notice that it have window effects like shadow on windows opened, animation effects while moving a window. You can also have these kind of effects in Ubuntu.To enable the feature right click on your desktop.Ch...'
url: /enabling-desktop-effects-in-ubuntu-linux
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 01925208532bdf8328d89524637944f7
  - 01925208532bdf8328d89524637944f7
  - 01925208532bdf8328d89524637944f7
  - 01925208532bdf8328d89524637944f7
  - 01925208532bdf8328d89524637944f7
  - 01925208532bdf8328d89524637944f7
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
  - http://sag-3.blogspot.com/2012/11/adding-desktop-effects-in-ubuntu-linux.html
categories:
  - Linux
  - Ubuntu
tags:
  - Graphics
  - Nvidia

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  When you are using <b>Windows 7</b>&nbsp;you will notice that it have window effects like shadow on windows opened, animation effects while moving a window. You can also have these kind of effects in Ubuntu.</p> 
  
  <div>
    To enable the feature <b>right click</b> on your desktop.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-hlpEKfixiIE/UJPqQ_vvDRI/AAAAAAAACMg/oJ367k02l5c/s1600/rclick.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="179" src="//4.bp.blogspot.com/-hlpEKfixiIE/UJPqQ_vvDRI/AAAAAAAACMg/oJ367k02l5c/s320/rclick.png" width="320" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Choose <b>Change Desktop Background</b>. A window will now open with 4 tabs. Open <b>Visual Effects </b>tab.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-q8vk5f0VpDw/UJPrQrIDN0I/AAAAAAAACMo/cTsY2RkpOnY/s1600/Screenshot-Appearance+Preferences.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="275" src="https://2.bp.blogspot.com/-q8vk5f0VpDw/UJPrQrIDN0I/AAAAAAAACMo/cTsY2RkpOnY/s320/Screenshot-Appearance+Preferences.png" width="320" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Choose <b>Norma</b>&nbsp;if you have a Graphics Card with a memory of <b>512 MB</b>. Choose <b>Extra&nbsp;</b>if you have <b>1 GB</b>&nbsp;graphics card. A installation will begin. After downloading required files the screen will become blank 1 or 2 times. Don&#8217;t worry! That&#8217;s it. Try out your new <b>Desktop Effects.</b>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
  </div>
  
  <div>
  </div>
</div>