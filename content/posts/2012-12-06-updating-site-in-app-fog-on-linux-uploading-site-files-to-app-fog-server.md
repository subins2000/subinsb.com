---
title: Updating site in App Fog on Linux / Uploading site files to App Fog Server
author: Subin Siby
type: post
date: 2012-12-06T17:12:00+00:00
excerpt: 'You might have know what is App Fog in my previous post. In this tutorial I am going to tell you how to upload your site files to their servers. For that first install Ruby by using this command :sudo apt-get install rubyThen install Ruby Gem&nbsp;:sud...'
url: /updating-site-in-app-fog-on-linux-uploading-site-files-to-app-fog-server
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - c1bc823096edb8040fe8d4763845acbb
  - c1bc823096edb8040fe8d4763845acbb
  - c1bc823096edb8040fe8d4763845acbb
  - c1bc823096edb8040fe8d4763845acbb
  - c1bc823096edb8040fe8d4763845acbb
  - c1bc823096edb8040fe8d4763845acbb
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
  - http://sag-3.blogspot.com/2012/12/appfog-linux-updation.html
dsq_thread_id:
  - 1957092545
  - 1957092545
  - 1957092545
  - 1957092545
  - 1957092545
  - 1957092545
categories:
  - Linux
  - Ubuntu
tags:
  - AppFog
  - Subins
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You might have know what is <b><a href="http://appfog.com/?utm_source=subinsblog" >App Fog</a></b> in my previous post. In this tutorial I am going to tell you how to upload your site files to their servers. For that first install <b>Ruby </b>by using this command :</p> 
  
  <blockquote class="tr_bq">
    <p>
      sudo apt-get install ruby
    </p>
  </blockquote>
  
  <div>
    Then install <b>Ruby Gem</b>&nbsp;:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      sudo apt-get install gem
    </p>
  </blockquote>
  
  <div>
    <b><a href="http://appfog.com/?utm_source=subinsblog" >AppFog</a> </b>uses a gem named <b>AF</b>. Install the <b>AF</b>&nbsp;gem by using the command :
  </div>
  
  <blockquote class="tr_bq">
    <p>
      sudo gem install af
    </p>
  </blockquote>
  
  <div>
    Wait some time. After the installation is finished you can use <b>AF</b> command.
  </div>
  
  <div>
    To upload the files first you need to <b>CD</b>&nbsp;to the folder where the files of your site is. To do that&nbsp;use the command below:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      cd <span style="color: red;">/pathtoyourfolder</span>
    </p>
  </blockquote>
  
  <div>
    Then you only need to upload the files by using the command below:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      sudo af&nbsp;update <span style="color: red;">sitename</span>
    </p>
  </blockquote>
  
  <div>
    Be patient. It will finish fast depending on the size of the folder. This process may require a restart of your site.
  </div>
  
  <div>
    <b>Note &#8211; </b>&nbsp;sometimes the command <b>af</b>&nbsp;may not work and you will get the error :
  </div>
  
  <blockquote class="tr_bq">
    <p>
      af: command not found
    </p>
  </blockquote>
  
  <div>
    To fix this replace af with ruby <b>/home/<span style="color: red;">username</span>/.gem/ruby/1.8/gems/af-0.3.18.11/bin/af</b> on all of the commands using <b>af</b>.
  </div>
  
  <div>
    For Example :&nbsp;
  </div>
  
  <blockquote class="tr_bq">
    <p>
      <b>ruby /home/simsu/.gem/ruby/1.8/gems/af-0.3.18.11/bin/af update subins</b>
    </p>
  </blockquote>
  
  <div>
    Thank you for visiting my blog. Hope it helped you. If this post helped you please signup for <b>Subins</b>&nbsp;<a href="http://accounts-subins.hp.af.cm/signup.php?utm_source=sagblog" >here</a>.
  </div>
</div>