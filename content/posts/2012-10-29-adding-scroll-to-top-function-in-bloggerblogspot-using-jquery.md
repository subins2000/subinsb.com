---
title: Adding Scroll To Top function in Blogger/Blogspot using Jquery
author: Subin Siby
type: post
date: 2012-10-29T13:27:00+00:00
excerpt: "This Post will help you to add a Scroll To Top function in your Blogger Blog. Follow the steps given below.  DEMO 1 - Go to Blogger -&gt; Template -&gt; Edit HTML&nbsp;2 - Adding Codes.Add this script before &lt;/head&gt; if you haven't added a script ..."
url: /adding-scroll-to-top-function-in-bloggerblogspot-using-jquery
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_item_hash:
  - 7ec51655be638a90d106d537cdc8f622
  - 7ec51655be638a90d106d537cdc8f622
  - 7ec51655be638a90d106d537cdc8f622
  - 7ec51655be638a90d106d537cdc8f622
  - 7ec51655be638a90d106d537cdc8f622
  - 7ec51655be638a90d106d537cdc8f622
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
  - http://sag-3.blogspot.com/2012/10/adding-scroll-to-top-function-in.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
categories:
  - Blogger
  - HTML
  - jQuery

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div class="tr_bq">
    <span style="font-family: inherit;">This Post will help you to add a <b>Scroll To Top </b>function in your <b>Blogger Blog</b>. Follow the steps given below.</span> </p> 
    
    <div style="text-align: center;">
      <span style="background-color: #93c47d; border-radius: 10px; font-family: inherit; font-size: x-large; padding: 8px 10px;"><a href="http://subin-demos.blogspot.com/#scrolltop" >DEMO</a></span>
    </div>
  </div>
  
  <p>
    <span style="font-family: inherit;"><br /></span> <span style="font-family: inherit;">1 &#8211; Go to <b>Blogger -> Template -> Edit HTML&nbsp;</b></span><br /><span style="font-family: inherit;">2 &#8211; Adding Codes.</span><br /><span style="font-family: inherit;">Add this script before <b></head></b> if you haven&#8217;t added a <b>script src</b> of&nbsp;<b>Jquery</b>&nbsp;<b>Library</b>.</span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white; font-family: inherit; line-height: 18px; white-space: pre; word-spacing: -3px;"><script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script></span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; font-family: inherit; line-height: 18px; white-space: pre; word-spacing: -3px;">Add this Code before <b></head></b> tag.</span>
  </p>
  
  <blockquote>
    <p>
      <span style="font-family: inherit; line-height: 18px; white-space: pre; word-spacing: -3px;"><script>$(document).ready(function(){$(&quot;body&quot;).append(&quot;<p id=&#8217;back-top&#8217; style=&#8217;display: block; &#8216;><a href=&#8217;#top&#8217;><span/>Back to Top</a></p>&quot;); $(&quot;#back-top&quot;).hide(); $(function () {$(window).scroll(function () {if ($(this).scrollTop() > 100) {$(&#39;#back-top&#39;).fadeIn(); }else {$(&#39;#back-top&#39;).fadeOut(); }});$(&#39;#back-top a&#39;).click(function () {$(&#39;body,html&#39;).animate({scrollTop: 0 }, 800); return false; }); });});</script></span>
    </p>
  </blockquote>
  
  <p>
    <span style="font-family: inherit;">Add this <b>CSS</b>&nbsp;Style sheet code before <b></head></b></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;"><style>#back-top { position: fixed; bottom: 30px;right:0px;}#back-top &nbsp;a { width: 108px; display: block; text-align: center; font: 11px/100% Arial, Helvetica, sans-serif; text-transform: uppercase; text-decoration: none; color: #bbb; -webkit-transition: 1s; -moz-transition: 1s; transition: 1s;}#back-top &nbsp;a:hover { color: #000;}#back-top &nbsp;span { width: 108px; height: 108px; display: block; margin-bottom: 7px; background: #ddd url(&#39;//4.bp.blogspot.com/-0mlo-caVkrQ/Ub835FbxwKI/AAAAAAAACv4/y9bfGt2b1fs/s1600/0017.png&#39;) no-repeat center center; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; -webkit-transition: 1s; -moz-transition: 1s; transition: 1s;}#back-top &nbsp;a:hover span{background-color: #777;}</style></span>
    </p>
  </blockquote>
  
  <p>
    <span style="background-color: white; font-family: inherit;">No need of adding any <b>HTML</b> element because the <b>Jquery</b> script is adding the <b>HTML</b> element in <b>BODY</b> tag.</span></div>