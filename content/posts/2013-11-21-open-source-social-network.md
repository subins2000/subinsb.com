---
title: 'Open : An Open Source Social Network'
author: Subin Siby
type: post
date: 2013-11-21T15:31:10+00:00
url: /open-source-social-network
authorsure_include_css:
  - 
  - 
  - 
tags:
  - Network
  - Open

---
I decided to create a new social network. I already created 3 social networks, 2 is under project and one another was online. Among the 2 under going projects is **Open** which I decided to make it **Open Source**. Hence the name, **Open**. It is currently available for subscribing @ <http://open.subinsb.com> which will soon be completely online. I didn&#8217;t had the time to work on it because of the **Bundles **of school projects (GOD! I hate School). The code will be available on [GitHub][1] soon. ****

&nbsp;

Here is the current logo of **Open** :

[<img class="size-full wp-image-2062 aligncenter" alt="logo" src="//lab.subinsb.com/projects/blog/uploads/2013/11/logo1.png" width="185" height="69" />][2]

&nbsp;

The logo was created using **GIMP**. The font of the logo is **ubuntu**. It&#8217;s a very simple logo which only has 2 layers. The color of the text is **#009CFF**.

You can subscribe to **Open **@ <http://open.subinsb.com> . If you subscribe you will be notified when **Open** becomes online. The subscribers of **Subin&#8217;s Blog** will be automatically notified of **Open**.

As I go through developing **Open** so will the new posts. I will tell you the technics, codes used and the features on the upcoming posts. I need your feedback on this **Open Source Project** movement. Tell your ideas, suggestions, feedback via comments. I will reply to them If I can.

 [1]: https://github.com/subins2000/open
 [2]: //lab.subinsb.com/projects/blog/uploads/2013/11/logo1.png