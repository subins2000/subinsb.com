---
title: Goodbye AppFog, Hello OpenShift
author: Subin Siby
type: post
date: 2013-10-06T17:27:00+00:00
excerpt: 'AppFog and Heroku&nbsp;were one of the most popular PaaS&nbsp;services. But due to the limitations of service many moved out. At first when AppFog&nbsp;came, there were a lot of cool awesome features more than Heroku. For the Free&nbsp;plan AppFog&nbsp...'
url: /goodbye-appfog-hello-openshift
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 6f677cdaebb7c73007a7fe2c27b88bb8
  - 6f677cdaebb7c73007a7fe2c27b88bb8
  - 6f677cdaebb7c73007a7fe2c27b88bb8
  - 6f677cdaebb7c73007a7fe2c27b88bb8
  - 6f677cdaebb7c73007a7fe2c27b88bb8
  - 6f677cdaebb7c73007a7fe2c27b88bb8
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
  - http://sag-3.blogspot.com/2013/10/bye-appfog-hello-openshift.html
dsq_thread_id:
  - 1958609250
  - 1958609250
  - 1958609250
  - 1958609250
  - 1958609250
  - 1958609250
categories:
  - MySQL
  - NodeJs
  - PHP
  - PostgreSQL
  - Python
tags:
  - AppFog
  - Java
  - Jenkins
  - MongoDB
  - OpenShift
  - PaaS
  - Perl

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>AppFog </b>and <b>Heroku</b>&nbsp;were one of the most popular <b>PaaS</b>&nbsp;services. But due to the limitations of service many moved out. At first when <b>AppFog</b>&nbsp;came, there were a lot of cool awesome features more than <b>Heroku</b>. For the <b>Free</b>&nbsp;plan <b>AppFog</b>&nbsp;had <b>SSL</b>, <b>Custom Domain </b>and other features. But by the end of <b>February</b>&nbsp;<b>2013</b>, <b>AppFog</b>&nbsp;removed <b>Custom Domain </b>from free plan. Many customers (including me) got sad, angry and were trying to find another <b>PaaS</b>&nbsp;service. And Here it is, the <b>next Awesome</b>&nbsp;<b>PaaS</b>&nbsp;service &#8211; <b><span style="font-size: large;"><a href="https://www.openshift.com/" >OpenShift</a></span></b>. Here is the best part : <b><a href="https://www.openshift.com/" >OpenShift</a></b>&nbsp;is created by the <b>Open-Source</b>&nbsp;legend <b>Red Hat</b>. How awesome is that ? <b>OpenShift</b>&nbsp;is easy to use, fast and reliable. I used it for like <b>5</b>&nbsp;<b>hours</b>&nbsp;now and I already liked it. I have moved the <a href="http://demos.subinsb.com/" ><b>Demo </b>site</a>&nbsp;from <b>3owl</b>&nbsp;to <b>OpenShift</b>.</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://openshift.redhat.com/app/assets/openshift-logo-horizontal-99a90035cbd613be7b6293335eb05563.svg" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://openshift.redhat.com/app/assets/openshift-logo-horizontal-99a90035cbd613be7b6293335eb05563.svg" style="background-color: black; background-position: initial initial; background-repeat: initial initial;" /></a>
  </div>
  
  <div>
    </p> 
    
    <div>
      You will be amazed by the speed of the <b>PaaS</b>, From what I experienced <b>OpenShift</b>&nbsp;is faster than <b>AppFog</b>.&nbsp;<b>OpenShift</b>&nbsp;is just like <b>AppFog</b>, <b>OpenShift</b>&nbsp;have a large list of supported languages and services. Here are some of them :</p> 
      
      <ol style="text-align: left;">
        <li>
          Java
        </li>
        <li>
          PHP
        </li>
        <li>
          Ruby
        </li>
        <li>
          Node.js
        </li>
        <li>
          Python
        </li>
        <li>
          Perl
        </li>
        <li>
          MySQL
        </li>
        <li>
          MongoDB
        </li>
        <li>
          PostgreSQL
        </li>
        <li>
          Jenkins
        </li>
      </ol>
      
      <p>
        See More <a href="https://www.openshift.com/developers/technologies" >Here</a>. Why wait ? Use <b><a href="https://www.openshift.com/" >OpenShift</a> </b>Now.&nbsp;I totally recommend you to use <b><a href="https://www.openshift.com/" >OpenShift</a></b>.<br />As long as <b>OpenShift</b>&nbsp;don&#8217;t remove <b>Custom Domains</b>, I&#8217;m gonna stick with <b>OpenShift</b>.</div> </div> </div>