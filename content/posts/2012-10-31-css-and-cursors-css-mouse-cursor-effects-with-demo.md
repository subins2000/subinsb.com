---
title: CSS and Cursors CSS mouse cursor effects with DEMO
author: Subin Siby
type: post
date: 2012-10-31T13:25:00+00:00
excerpt: '<div dir="ltr" trbidi="on"><span>Ever wanted to change the cursor in your web page ? Well you can change it in <b>CSS</b>. There is a way of doing that, the <b>cursor</b>&nbsp;propery.</span><br><span>There is a way in <b>CSS</b>&nbsp;to change the cursor to <b>pointer</b>,<b>cursor</b>,<b>resize</b>,<b>normal</b>&nbsp;etc.....</span><br><span>This table show what will be the cursor when the value of&nbsp;<b>cursor</b>&nbsp;property<b>&nbsp;</b>changes as following.</span><br><table border="1" bordercolor="#000000" cellpadding="4" cellspacing="0"><colgroup><col width="128*"><col width="128*"></colgroup><tbody><tr valign="TOP"><td width="50%"><b><u><span>Value</span></u></b></td>  <td width="50%"><b><u><span>Cursor</span></u></b></td> </tr><tr valign="TOP"><td width="50%"><span><span><b>Not-allowed,default,inherit,initial,no-drop,vertical-text</b></span></span></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>all-scroll</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>col-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>crosshair</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>e-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>ew-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>help</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>move</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>n-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>none</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>ne-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>nesw-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>ns-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>nw-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>nwse-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>pointer</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>Progress,wait</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><b><span>row-resize</span></b></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><div align="LEFT"><span><span><b>s-resize</b></span></span></div></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><div align="LEFT"><span><span><b>se-resize</b></span></span></div></td>  <td width="50%"><b><span><br></span></b></td> </tr><tr valign="TOP"><td width="50%"><div align="LEFT"><b><span>sw-resize </span></b></div></td>  <td width="50%"></td> </tr><tr valign="TOP"><td width="50%"><b><span>text</span></b></td>  <td width="50%"></td> </tr><tr valign="TOP"><td width="50%"><b><span>w-resize</span></b></td>  <td width="50%"></td> </tr></tbody></table></div>'
url: /css-and-cursors-css-mouse-cursor-effects-with-demo
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - dcc3f3b186bcc341724ca0f6a91692d5
  - dcc3f3b186bcc341724ca0f6a91692d5
  - dcc3f3b186bcc341724ca0f6a91692d5
  - dcc3f3b186bcc341724ca0f6a91692d5
  - dcc3f3b186bcc341724ca0f6a91692d5
  - dcc3f3b186bcc341724ca0f6a91692d5
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
  - http://sag-3.blogspot.com/2012/10/mouse-cursors-css.html
categories:
  - CSS
  - HTML

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">Ever wanted to change the cursor in your web page ? Well you can change it in <b>CSS</b>. There is a way of doing that, the <b>cursor</b>&nbsp;propery.</span><br /><span style="font-family: inherit;">There is a way in <b>CSS</b>&nbsp;to change the cursor to <b>pointer</b>,<b>cursor</b>,<b>resize</b>,<b>normal</b>&nbsp;etc&#8230;..</span><br /><span style="font-family: inherit;">This table show what will be the cursor when the value of&nbsp;<b>cursor</b>&nbsp;property<b>&nbsp;</b>changes as following.</span></p> 
  
  <table border="1" bordercolor="#000000" cellpadding="4" cellspacing="0" style="width: 100%px;">
    <colgroup> <col width="128*"></col> <col width="128*"></col> </colgroup> <tr valign="TOP">
      <td width="50%">
        <b><u><span style="font-family: inherit;">Value</span></u></b>
      </td>
      
      <td width="50%">
        <b><u><span style="font-family: inherit;">Cursor</span></u></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <span style="color: black;"><span style="font-family: inherit; font-size: small;"><b>Not-allowed,default,inherit,initial,no-drop,vertical-text</b></span></span>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">all-scroll</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">col-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">crosshair</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">e-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">ew-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">help</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">move</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">n-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">none</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">ne-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">nesw-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">ns-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">nw-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">nwse-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">pointer</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">Progress,wait</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">row-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <div align="LEFT">
          <span style="color: black;"><span style="font-family: inherit; font-size: small;"><b>s-resize</b></span></span>
        </div>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <div align="LEFT">
          <span style="color: black;"><span style="font-family: inherit; font-size: small;"><b>se-resize</b></span></span>
        </div>
      </td>
      
      <td id="vattu" width="50%">
        <b><span style="font-family: inherit;"><br /> </span></b>
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <div align="LEFT">
          <b><span style="font-family: inherit;">sw-resize </span></b>
        </div>
      </td>
      
      <td id="vattu" width="50%">
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">text</span></b>
      </td>
      
      <td id="vattu" width="50%">
      </td>
    </tr>
    
    <tr valign="TOP">
      <td width="50%">
        <b><span style="font-family: inherit;">w-resize</span></b>
      </td>
      
      <td id="vattu" width="50%">
      </td>
    </tr>
  </table>
  
  <p>
    </div>