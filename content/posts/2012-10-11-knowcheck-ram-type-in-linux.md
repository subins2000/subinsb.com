---
title: Know/Check Ram Type in Linux
author: Subin Siby
type: post
date: 2012-10-11T13:38:00+00:00
excerpt: 'If you want to know what type is your RAM. This command will help you in&nbsp;Linux.sudo dmidecode --type 17Type your password and press Enter. The details of your ram will be shown.You can now see the DDR of your Ram&nbsp;as shown in the picture above.'
url: /knowcheck-ram-type-in-linux
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - f6360b398e29f21867640009733e07ba
  - f6360b398e29f21867640009733e07ba
  - f6360b398e29f21867640009733e07ba
  - f6360b398e29f21867640009733e07ba
  - f6360b398e29f21867640009733e07ba
  - f6360b398e29f21867640009733e07ba
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
  - http://sag-3.blogspot.com/2012/10/ddr-ram.html
categories:
  - Linux
  - Ubuntu
tags:
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you want to know what type is your RAM. This command will help you in&nbsp;<b>Linux</b>.</p> 
  
  <blockquote class="tr_bq">
    <p>
      sudo dmidecode &#8211;type 17
    </p>
  </blockquote>
  
  <p>
    Type your password and press Enter. The details of your ram will be shown.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-8EDRj6O5A_g/UHbKtcK-d0I/AAAAAAAACAc/PUtyIdkbRys/s1600/ddr.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-8EDRj6O5A_g/UHbKtcK-d0I/AAAAAAAACAc/PUtyIdkbRys/s1600/ddr.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    You can now see the <b>DDR </b>of your <b>Ram</b>&nbsp;as shown in the picture above.
  </div>
</div>