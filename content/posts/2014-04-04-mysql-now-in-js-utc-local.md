---
title: 'MySQL NOW() in JavaScript (UTC & Local)'
author: Subin Siby
type: post
date: 2014-04-04T06:56:35+00:00
url: /mysql-now-in-js-utc-local
authorsure_include_css:
  - 
  - 
  - 
categories:
  - JavaScript
  - MySQL
tags:
  - Date

---
If you know **MySQL**, you will certainly know the **NOW()** function that outputs the current date and time with the timezone of the MySQL server. For easy use and compatibility, we use **UTC** time zone for storing time in database. If you are running a web app that comply with MySQL, then you need the date function as same as the **NOW()**.

So, I will give you a small function that return the same value as MySQL&#8217;s NOW() function in both UTC and local time. There are 2 versions of this function, one little big and the other very short (1 line of code). The shorter one is supported by modern browsers and the other is supported by all browsers.


## Longer Function

### Common Function

In both cases (UTC, Local) you need a common function that corrects a single digit value to 2 digit value. Here is the function :

<pre class="prettyprint"><code>function twoDigits(d) {
 if(0 &lt;= d && d &lt; 10) return "0" + d.toString();
 if(-10 &lt; d && d &lt; 0) return "-0" + (-1*d).toString();
 return d.toString();
}</code></pre>

### UTC

This function gives the current **UTC** date time :

<pre class="prettyprint"><code>function NOW(){
 t=new Date();
 return t.getUTCFullYear() + "-" + twoDigits(1 + t.getUTCMonth()) + "-" + twoDigits(t.getUTCDate()) + " " + twoDigits(t.getUTCHours()) + ":" + twoDigits(t.getUTCMinutes()) + ":" + twoDigits(t.getUTCSeconds());
}</code></pre>

Note that we use the **twoDigits** function in the above code.

### Local

Get the current local time of the client&#8217;s browser / system :

<pre class="prettyprint"><code>function NOW(){
 t=new Date();
 return t.getFullYear() + "-" + twoDigits(1 + t.getMonth()) + "-" + twoDigits(t.getDate()) + " " + twoDigits(t.getHours()) + ":" + twoDigits(t.getMinutes()) + ":" + twoDigits(t.getSeconds());
}</code></pre>

All we have to do is replace all **UTC** word from the UTC NOW() code and you&#8217;ll get the local date time code.

## Easy, Shorter Function

There&#8217;s another easy approach to the NOW() function. It&#8217;s by using **.toISOString()**. This supports all modern browsers.

### UTC

<pre class="prettyprint"><code>function NOW(){
 return new Date().toISOString().slice(0, 19).replace('T', ' ');
}</code></pre>

### Local

<pre class="prettyprint"><code>function NOW(){
 return new Date(new Date()+" UTC").toISOString().slice(0, 19).replace('T', ' ');
}</code></pre>

## Usage

The usage is same in both cases :

<pre class="prettyprint"><code>console.log(NOW())</code></pre>

If you used the UTC version of NOW(), then it will print something like :

<pre class="prettyprint"><code>2014-04-04 06:50:44</code></pre>

In the local case, it would be :

<pre class="prettyprint"><code>2014-04-04 12:20:44</code></pre>

You can include the date time in a variable too :

<pre class="prettyprint"><code>var curDateTime = NOW();</code></pre>

Happy coding.

 [1]: #longer-function
 [2]: #common-function
 [3]: #utc
 [4]: #local
 [5]: #easy-shorter-function
 [6]: #utc-2
 [7]: #local-2
 [8]: #usage
