---
title: Make A Blank Template / HTML Page In Blogger
author: Subin Siby
type: post
date: 2014-02-16T04:54:10+00:00
url: /make-a-blank-blogger-template
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Blogger
tags:
  - Template
  - Tutorials

---
Blogger Templates can be very messed up with lots and lots of code. If you are trying to create a Blogger Template from scratch, the first thing you need is a blank HTML page. That&#8217;s exactly what we are going to do today.

<div class="padlinks">
  <a class="demo" href="http://subinsb-blank.blogspot.com/" target="_blank">Demo</a>
</div><figure id="attachment_2500" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2500" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0103-300x212.png" alt="Simple Template To Blank Page" width="300" height="212" />][1]<figcaption class="wp-caption-text">Blogger Simple Template To Blank Page</figcaption></figure> 


## Criterias

Blogger won&#8217;t allow you to just add the following code as template :

<pre class="prettyprint"><code>&lt;html&gt;
 &lt;head&gt;
  &lt;title&gt;My Awesome Blank Blog&lt;/title&gt;
 &lt;/head&gt;
 &lt;body&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

They will print out errors if you try to save the template. There are certain criteria for making a template for Blogger Blog :

  1. There must be skin (<b:skin></b:skin>) in the template
  2. A template must have at least one b:section tag
  3. Every section should have a unique id.
  4. Correct Syntax

If the above criterias are met, Blogger won&#8217;t produce any errors when saving the template. For creating a blank HTML Template, we will make sure these criterias are met.

## Contents Of Blogger Blank Template

As we said before, Blogger Templates should meet the criterias of Blogger. A Blank Blogger Template should contain the following :

  1. Basic HTML Page Tags (html, head, body) and their closings
  2. Only one **<b:skin></b:skin>** tag
  3. Need At least a **<b:section></b:section>** tag.

## Create Template

Make sure your blog is using Simple Template. If not, apply Simple Template to your blog.

Go to your **Blogger Blog **-> **Template** and click on **Edit HTML** button. Remove the contents of Template.

We will now add the Basic HTML page to the Template :

<pre class="prettyprint"><code>&lt;?xml version="1.0" encoding="UTF-8" ?&gt;
&lt;html xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'&gt;
 &lt;head&gt;
 &lt;/head&gt;
 &lt;body&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

### Head

The contents of the **<head></head****>** tag are the title, skin and other elements. We should add the normal codes that are seen on Blogger Templates first :

<pre class="prettyprint"><code>&lt;meta content='IE=EmulateIE7' http-equiv='X-UA-Compatible'/&gt; 
 &lt;b:if cond='data:blog.isMobile'&gt; 
  &lt;meta content='width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0' name='viewport'/&gt; 
 &lt;b:else/&gt; 
  &lt;meta content='width=1100' name='viewport'/&gt; 
 &lt;/b:if&gt; 
&lt;b:include data='blog' name='all-head-content'/&gt;</code></pre>

Add the title :

<pre class="prettyprint"><code>&lt;title&gt;&lt;data:blog.pageTitle/&gt;&lt;/title&gt;</code></pre>

Add the Skin with basic CSS that can be changed using Blogger&#8217;s Template Designer :

<pre class="prettyprint"><code>&lt;b:skin&gt;
 &lt;![CDATA[/* 
  body { 
   font: $(body.font); 
   color: $(body.text.color); 
   background: $(body.background); 
   padding: 0 $(content.shadow.spread) $(content.shadow.spread) $(content.shadow.spread); 
   $(body.background.override) margin: 0; 
   padding: 0; 
  }
 ]]&gt;
&lt;/b:skin&gt;</code></pre>

There are more styles that can be inserted in **<b:skin>**, but only body&#8217;s is mentioned here. By adding more style rules into it, you will be easily able to update the colors, background etc&#8230; from the Template Designer.

There are additional **CSS** files that are loaded by Blogger before **<b:skin>**. If you want to disable that, see <a title="Disable Blogger Official CSS From Loading In Template" href="//subinsb.com/disable-blogger-official-css-loading" target="_blank">this post</a>.

### Body

Blogger needs a **<b:section> **element in template. So, we should add it inside body.

<pre class="prettyprint"><code>&lt;b:section id='main' showaddelement='yes'/&gt;</code></pre>

This section is the main section of the blog where we can add gadgets to it.

## The Whole Code

And the whole Template code would be :

<pre class="prettyprint"><code>&lt;?xml version="1.0" encoding="UTF-8" ?&gt;
&lt;html xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'&gt;
 &lt;head&gt;
  &lt;meta content='IE=EmulateIE7' http-equiv='X-UA-Compatible'/&gt; 
  &lt;b:if cond='data:blog.isMobile'&gt; 
   &lt;meta content='width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0' name='viewport'/&gt; 
  &lt;b:else/&gt; 
   &lt;meta content='width=1100' name='viewport'/&gt; 
  &lt;/b:if&gt; 
  &lt;b:include data='blog' name='all-head-content'/&gt;
  &lt;title&gt;&lt;data:blog.pageTitle/&gt;&lt;/title&gt;
  &lt;b:skin&gt;
   &lt;![CDATA[/* 
    body { 
     font: $(body.font); 
     color: $(body.text.color); 
     background: $(body.background); 
     padding: 0 $(content.shadow.spread) $(content.shadow.spread) $(content.shadow.spread); 
     $(body.background.override) margin: 0; 
     padding: 0; 
    }
   ]]&gt;
  &lt;/b:skin&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;b:section class='main' id='main' showaddelement='yes'/&gt;
  &lt;!-- Please Keep The Credits --&gt;
  &lt;center&gt;&lt;a href="//subinsb.com/make-a-blank-blogger-template"&gt;Blank Template By subinsb.com&lt;/a&gt;&lt;/center&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

## Additional Code adding

  * Add additional CSS codes inside **<b:skin></b:skin>**
  * Add JavaScript codes before **</head>** or after **<head>**
  * Add HTML codes (widgets, code) inside **<body>**.
  * You can do all the stuff in this template just like you do it a **HTML** page.

If you want to display the Posts, add the following code inside **<b:section></b:section>** in body :

<pre class="prettyprint"><code>&lt;b:widget id='Blog1' locked='true' title='Blog Posts' type='Blog'/&gt;</code></pre>

Example :

<pre class="prettyprint"><code>&lt;b:section class='main' id='main' showaddelement='yes'&gt;
  &lt;b:widget id='Blog1' locked='true' title='Blog Posts' type='Blog'/&gt;
&lt;/b:section&gt;</code></pre>

Save your template and check out your blog. You will now have a plain blank Blog.

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/02/0103.png
 [2]: #criterias
 [3]: #contents-of-blogger-blank-template
 [4]: #create-template
 [5]: #head
 [6]: #body
 [7]: #the-whole-code
 [8]: #additional-code-adding
