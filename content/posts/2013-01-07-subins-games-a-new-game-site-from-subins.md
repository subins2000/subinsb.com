---
title: 'Subins Games : A new Game site from Subins'
author: Subin Siby
type: post
date: 2013-01-07T16:37:00+00:00
excerpt: 'This is my 5th project. The Subins Games. This site is for playing games.&nbsp;FunctionsCommentingScore savingPlay HistoryAdd your gameGame stats&nbsp;(And adding new ones!)Like all the other projects this project has used Javascript Library&nbsp;Jquer...'
url: /subins-games-a-new-game-site-from-subins
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
  - http://sag-3.blogspot.com/2013/01/subins-games-new-game-site-from-subins.html
syndication_item_hash:
  - 8ff95adae9655a4258cab856e34067ea
  - 8ff95adae9655a4258cab856e34067ea
  - 8ff95adae9655a4258cab856e34067ea
  - 8ff95adae9655a4258cab856e34067ea
  - 8ff95adae9655a4258cab856e34067ea
  - 8ff95adae9655a4258cab856e34067ea
categories:
  - MySQL
  - PHP
  - WordPress
tags:
  - G2E
  - Games
  - PFOGB
  - Subins

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This is my 5th project. <b><a href="http://games-subins.hp.af.cm/" >The Subins Games</a></b>. This site is for playing games.&nbsp;</p> 
  
  <h3 style="text-align: left;">
    <u>Functions</u>
  </h3>
  
  <div>
    <ul style="text-align: left;">
      <li>
        Commenting
      </li>
      <li>
        Score saving
      </li>
      <li>
        Play History
      </li>
      <li>
        Add your game
      </li>
      <li>
        Game stats
      </li>
    </ul>
  </div>
  
  <div>
    &nbsp;(And adding new ones!)
  </div>
  
  <div>
  </div>
  
  <div>
    Like all the other projects this project has used Javascript Library&nbsp;<b>Jquery</b>&nbsp;and the server side language is <b>PHP</b>.
  </div>
  
  <div>
    This site&#8217;s games is the combination of old <b><a href="http://pfogb.blogspot.com/" >PFOGB</a></b>&nbsp;and <a href="http://g2enjoy.blogspot.com/" style="font-weight: bold;" >G2E</a>.
  </div>
  
  <div>
  </div>
  
  <div>
    The main problem I faced was the merging of two blogs to one. It was very difficult to merge manually.
  </div>
  
  <div>
    So I used <b>WordPress</b>&nbsp;in my localhost to import posts from blogger and exported the posts in <b>XML</b>&nbsp;format.
  </div>
  
  <div>
    Then I used <b>PHP </b>to insert the <b>XML</b>&nbsp;file contents to <b>MySql</b>&nbsp;Database. Thankyou <b>WordPress</b>.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="http://games-subins.hp.af.cm/" ><img border="0" src="//3.bp.blogspot.com/-4ZwBt1WxCWs/UOr5s-aTRHI/AAAAAAAACZI/wspKoWF2swI/s1600/logo.png" /></a>
  </div>
  
  <div>
  </div>
</div>