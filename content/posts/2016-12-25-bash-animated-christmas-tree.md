---
title: Draw Animated Christmas Tree In Bash
author: Subin Siby
type: post
date: 2016-12-25T07:34:24+00:00
lastmod: 2018-12-25
url: /bash-animated-christmas-tree
categories:
  - BASH
  - Linux
  - Operating System
  - Program
  - Ubuntu
  - Unix
aliases:
  - /xmas

---
It's Christmas again and you'll be receiving all kinds of greetings. How about greeting them back with a fancy way ? Show off your nerdiness with this super cool **Christmas Tree in Bash**.

All you need is a bash shell. Most systems have bash shell by default, so you don't have to install anything for this !

All thanks goes to the Argentian programmer [Sergio Lepore][3] for this cool script.

Here's how it looks :<figure class="wp-caption aligncenter">

[<img src="https://i.redd.it/080aqe51c0601.gif" alt="Christmas Tree in Bash" />][1]<figcaption class="wp-caption-text">Christmas Tree in Bash</figcaption></figure>

First of all open a terminal and copy [this script][2] to your computer :

<pre class="prettyprint"><code>wget -d -c -O "christmas.sh" "https://raw.githubusercontent.com/sergiolepore/ChristBASHTree/master/tree-EN.sh"</code></pre>

Make the file executable :

<pre class="prettyprint"><code>chmod u+x ./christmas.sh</code></pre>

RUN !

<pre class="prettyprint"><code>./christmas.sh
</code></pre>

Ain't that fancy ? All thanks goes to the Argentian programmer [Sergio Lepore][3] for this cool trick.

## Fork

You can make your own little changes in the script to add your name and other stuff. I added my name using this line :

<pre class="prettyprint"><code>tput cup $((lin + 3)) $((c - 4)); echo subins2000</code></pre>

I added the above line just after "And lots of CODE in $new_year" line.

You can do anything with this script. Maybe translate it your own language or add more fancy stuff. Let me know what you have done 🙂

## Make A Gif

Let's make a gif of this great terminal gift. You can use this gif to send to your friends or post on social networks.

We'll use a screen recorder and a gif converter. For this, install these :

<pre class="prettyprint"><code>sudo apt install gtk-recordmydesktop ffmpeg winff
</code></pre>

Use gtk-recordmydesktop to record the terminal. This will output a OGV file. Then do the following 2 commands to make the gif :

<pre class="prettyprint"><code>ffmpeg -y -i input.ogv -vf fps=10,scale=800:-1:flags=lanczos,palettegen palette.png
ffmpeg -i input.ogv -i palette.png -filter_complex "fps=10,scale=800:-1:flags=lanczos[x];[x][1:v]paletteuse" output.gif</code></pre>

If you want to trim the video, you can add the following options to the second command :

    -ss 3 -t 30

which stands for "start from 3rd second and end at 30th second.

## Make A Video With Background Sound

How about mixing a christmas song to the gif and make a video ! We can use `ffmpeg` for this. I'm gonna mix [Feliz Navidad](https://www.youtube.com/watch?v=RTtc2pM1boE) which is saved as `sound.mp3`. Here's what the command looks :

```bash
ffmpeg -ss 34 -i "sound.mp3" -i output.gif -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" -strict -2 -c:v libx264 -c:a aac -b:a 192k -pix_fmt yuv420p -t 17 xmas.mp4
```

In the above command, I specifically mention the point of audio to mix : start from `34`th second and duration `17` seconds. This is the point where the lyrics is *I wanna wish you a merry christmas, I wanna wish you a merry christmas from the bottom of my heart*

[And here's the result](https://v.redd.it/krdropdtc9621)


 [1]: //i.imgur.com/oCU56St.gif
 [2]: https://github.com/sergiolepore/ChristBASHTree/blob/master/tree-EN.sh
 [3]: http://www.sergiolepore.net/
