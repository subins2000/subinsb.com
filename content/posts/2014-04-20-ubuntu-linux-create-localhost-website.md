---
title: 'Create a localhost Website in Ubuntu 11.04 & Up'
author: Subin Siby
type: post
date: 2014-04-20T03:45:14+00:00
url: /ubuntu-linux-create-localhost-website
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Linux
  - Localhost
  - PHP
  - Ubuntu
tags:
  - Tutorials
  - Website

---
In a post, I told <a title="How To Create A Localhost Web Site In Linux Using Apache Web Server" href="//subinsb.com/linux-apache-localhost" target="_blank">how to create a localhost website using Apache server in Ubuntu</a>. It worked on all versions up to Ubuntu&#8217;s **10.10**th version. But on Ubuntu versions 11.04 and the versions released after it, there were some changes to the **Apache** server.

So, by this post I&#8217;m going to update the way to create a localhost hosted website in Ubuntu versions **11.04** and the versions that came after **11.04** or **2011 April**. Some of them are :

  * 11.04
  * 11.10
  * 12.04
  * 12.10
  * 13.04
  * 13.10
  * 14.04
  * 14.10

## Install Apache Server

First of all, we will install the Apache server. Open the terminal (**CTRL + ALT + T**) and do the following command :

<pre class="prettyprint"><code>sudo apt-get install apache2</code></pre>

Say **Yes** when the program asks for confirmation. In a while, the package **apache2** will be downloaded and installed automatically. Folders and configuration files of **Apache** will automatically be created. To make sure that **Apache** has installed properly, visit <a href="http://localhost" target="_blank">the localhost website</a>.

If installed properly and successfully, you will get a page like this :<figure id="attachment_2753" class="wp-caption aligncenter">

[<img class="size-full wp-image-2753" src="//lab.subinsb.com/projects/blog/uploads/2014/04/0118.png" alt="Apache localhost Default Page" width="771" height="233" />][1]<figcaption class="wp-caption-text">Apache localhost Default Page</figcaption></figure> 

**"It works**". What a feeling, right ? You just completed the first step successfully

## Create a Site

Now let&#8217;s create a site that is hosted by the **Apache** server we just installed on your computer which is a accessible only by you.

For this tutorial, we are going to create a site called "mysite" which is accessible by the domain "mysite.com".

The Site&#8217;s configuration file is stored in the following directory :

<pre class="prettyprint"><code>/etc/apache2/sites-available</code></pre>

As you can see, you need **Root Access** for making changes in the directory. So, open the terminal and do this command :

<pre class="prettyprint"><code>sudo -i</code></pre>

You will be asked for the root password. Type it and press Enter (The password won&#8217;t be showed at all in Terminal while you type). You will now enter the root shell.

Now, let&#8217;s create the configuration file of the site "mysite" we are going to create. As I said before, the site&#8217;s configuration file should be kept in "sites-available" directory.

On the root shell, do this command to create an empty file in "sites-available" directory :

<pre class="prettyprint"><code>touch /etc/apache2/sites-available/mysite.conf</code></pre>

Now, let&#8217;s add some text to the file we created. Using **gEdit** or other text editors, edit the file we created using root :

<pre class="prettyprint"><code>gedit /etc/apache2/sites-available/mysite.conf</code></pre>

Using the text editor **leafpad**, we edit the file with the following command :

<pre class="prettyprint"><code>leafpad /etc/apache2/sites-available/mysite.conf</code></pre>

Whatever the text editor is, the contents of the file will be the same. Here is the content that you should copy & paste to the file :

<pre class="prettyprint"><code>&lt;VirtualHost *:80&gt;
 ServerAdmin webmaster@localhost
 ServerName &lt;span style="color: #ff0000;">mysite.com&lt;/span>
 DocumentRoot &lt;span style="color: #ff0000;">/home/username/websites/mysite&lt;/span>
 &lt;Directory &lt;span style="color: #ff0000;">/home/username/websites/mysite&lt;/span>/&gt;
  Options Indexes FollowSymLinks
  AllowOverride All
  Order allow,deny
  allow from all
  Require all granted
 &lt;/Directory&gt;
 ErrorLog /var/log/apache2/error.log
 LogLevel warn
 CustomLog /var/log/apache2/access.log combined
&lt;/VirtualHost&gt;</code></pre>

The highlighted text of color <span style="color: #ff0000;">red</span> should be changed according to you. Here is a summary of the changes that should be done in the file :

<table style="height: 133px;" width="821">
  <tr>
    <td>
      String
    </td>
    
    <td>
      Description
    </td>
  </tr>
  
  <tr>
    <td>
      <span style="color: #ff0000;">mysite.com</span>
    </td>
    
    <td>
      The site&#8217;s domain
    </td>
  </tr>
  
  <tr>
    <td>
      <span style="color: #ff0000;">/home/username/websites/mysite</span>
    </td>
    
    <td>
      The location of site&#8217;s source code
    </td>
  </tr>
  
  <tr>
    <td>
      /var/log/apache2/error.log
    </td>
    
    <td>
      Change this if you need a special error log file for your site
    </td>
  </tr>
  
  <tr>
    <td>
      /var/log/apache2/access.log
    </td>
    
    <td>
      Change this if you need a special access log file for your site
    </td>
  </tr>
</table>

The red highlighted text in the table above is the most important strings. It should be changed by you or not if you want the site&#8217;s domain as "mysite.com".

If you changed the location of the log files, make sure that the location exists. If it&#8217;s to a new directory, you should create the new directory. The "access.log" and "error.log" file will be automatically created by **Apache** even if it doesn&#8217;t exist. Save the configuration file and close the text editor.

Now, we should enable the site. Do the following command in root shell :

<pre class="prettyprint"><code>a2ensite &lt;span style="color: #ff0000;">mysite.conf&lt;/span></code></pre>

The red highlighted text above is the name of the file we created in the "sites-available" directory. If you chose a different name, change it when you enter the command in Terminal.

A restart of the **Apache** server is needed after enabling the site. So, do this command :

<pre class="prettyprint"><code>/etc/init.d/apache2 restart</code></pre>

or do the following command :

<pre class="prettyprint"><code>service apache2 restart</code></pre>

The site is created in localhost. But, still when you visit "mysite.com", the browser will go to the original "mysite.com" that is in the web. Browser or the network of your system doesn&#8217;t know that there is a "mysite.com" hosted in localhost. So, we should make it understand. For this, on the root shell do this command to edit the "hosts" file :

<pre class="prettyprint"><code>gedit /etc/hosts</code></pre>

At the end of the file, add the following line :

<pre class="prettyprint"><code>127.0.0.1 mysite.com</code></pre>

"127.0.0.1" is the IP Address of localhost and the other string we typed aside it is the domain name. Save the file and close the terminal.

Now, open a browser and go to the site "<a href="http://mysite.com" target="_blank">http://mysite.com</a>" and you will see your own localhost site. You can add the site&#8217;s files to the directory you mentioned in the configuration file we created. **index.html** will be the main file of the site. This main file will be shown when the site is directly visited (http://mysite.com).

Happy coding. 🙂

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/04/0118.png