---
title: The main reason why adsense was disapproved
author: Subin Siby
type: post
date: 2013-07-14T03:30:00+00:00
excerpt: 'From my experience with Adsense&nbsp;I got disapproved after a visit from a URL&nbsp;to my blog. The URL&nbsp;was SPAM which is known as REFERRAL SPAM. See more about REFERRAL&nbsp;SPAM&nbsp;here.Why Adsense account got disapproved of Referral SPAM&nbs...'
url: /the-main-reason-why-adsense-was-disapproved
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 668e1a9c4869bb7770a51d66ab0ca53c
  - 668e1a9c4869bb7770a51d66ab0ca53c
  - 668e1a9c4869bb7770a51d66ab0ca53c
  - 668e1a9c4869bb7770a51d66ab0ca53c
  - 668e1a9c4869bb7770a51d66ab0ca53c
  - 668e1a9c4869bb7770a51d66ab0ca53c
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
  - http://sag-3.blogspot.com/2013/07/main-reason-adsense-disapprove-blog.html
dsq_thread_id:
  - 1499258133
  - 1499258133
  - 1499258133
  - 1499258133
  - 1499258133
  - 1499258133
tags:
  - Adsense
  - Blog
  - Google
  - Referrer
  - SPAM

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  From my experience with <b>Adsense</b>&nbsp;I got disapproved after a visit from a <b>URL</b>&nbsp;to my blog. The <b>URL</b>&nbsp;was <b>SPAM </b>which is known as <b><a href="http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html" >REFERRAL SPAM</a></b>. See more about <b>REFERRAL</b>&nbsp;<b>SPAM</b>&nbsp;<a href="http://sag-3.blogspot.com/2013/04/spam-urls-in-blogger-traffic-source.html" >here</a>.</p> 
  
  <h4 style="text-align: left;">
    <span style="font-size: large;">Why Adsense account got disapproved of Referral SPAM&nbsp;?</span>
  </h4>
  
  <div>
    In the <b>Google</b>&nbsp;<b>Adsense</b>&nbsp;policies, It says that you should not pay someone to visit your blog/site. When you are placing the ad on your blog, <b>Google</b>&nbsp;tracks each visit to your blog. When they see a suspicious <b>URL</b>&nbsp;in the traffic sources they count it as a paid to click <b>URL</b>. Hence you will get disapproved.</p>
  </div>
  
  <h4 style="text-align: left;">
    <span style="font-size: large;">What should I do If I got disapproved&nbsp;</span><span style="font-size: large;">of Referral SPAM&nbsp;</span><span style="font-size: large;">?</span>
  </h4>
  
  <div>
    You should appeal to <b>Google</b>&nbsp;<b>Adsense</b>&nbsp;that you didn&#8217;t violated any policies. <a href="https://support.google.com/adsense/troubleshooter/2707037?hl=en" >Click this link to appeal</a>. You will be asked some questions. The <b>sample answers</b> of those questions with <b>can login</b> option are below:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      Contact E-Mail address ?
    </p>
    
    <p>
      user@somedomain.com
    </p>
  </blockquote>
  
  <blockquote class="tr_bq">
    <p>
      Have you ever purchased traffic to your site(s) ?
    </p>
    
    <p>
      No
    </p>
  </blockquote>
  
  <blockquote class="tr_bq">
    <p>
      How do users get to your site? How do you promote your site?
    </p>
    
    <p>
      Users get to my site mostly from <b>Google Search</b>. I promote my site using <b>Google</b>&nbsp;Adwords, Chatting (Facebook, Google+ etc&#8230;.)
    </p>
  </blockquote>
  
  <blockquote class="tr_bq">
    <p>
      <span style="color: #222222; line-height: 18.200000762939453px;"><span style="font-family: inherit;">Have you or your site ever violated the AdSense program policies or Terms & Conditions? If so, how? Also, include any relevant information that you believe may have resulted in invalid activity. *</span></span><span style="color: #222222; line-height: 18.200000762939453px;"><span style="font-family: inherit;"><br /></span></span><span style="color: #222222; line-height: 18.200000762939453px;"><span style="font-family: inherit;"><br /></span></span><span style="color: #222222; line-height: 18.200000762939453px;"><span style="font-family: inherit;">No. There are SPAM url&#8217;s on my traffic sources. Adsense may have disapproved because of the invalid visits from these SPAM sites.</span></span>
    </p>
  </blockquote>
  
  <blockquote>
    <p>
      <span style="color: #222222; line-height: 18.200000762939453px;"><span style="font-family: inherit;">Please include any data from your site traffic logs or reports that indicate suspicious IP addresses, referrers, or requests which could explain invalid activity. *</span></span><span style="color: #222222; line-height: 18.200000762939453px;"><span style="font-family: inherit;"><br /></span></span><span style="color: #222222; line-height: 18.200000762939453px;"><span style="font-family: inherit;"><br /></span></span><span style="color: #222222; line-height: 18.200000762939453px;">http://www.filmhill.com/redirect.php?url=http://flf-course.com?a_aid=510d2acc92117&a_bid=6f93443e</span><br /><span style="color: #222222; font-family: inherit; line-height: 18.200000762939453px;">http://vampirestat.com</span><br />http://r-e-f-e-r-e-r.com/<br />http://t.co/MaAptuGFVu6
    </p>
  </blockquote>
</div>