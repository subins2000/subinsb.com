---
title: 'Infinite loop in PHP & Python'
author: Subin Siby
type: post
date: 2014-10-22T03:30:38+00:00
url: /infinite-loop-in-php-python
categories:
  - PHP
  - Python
  - Short Post

---
**Infinite loops** are loops that doesn&#8217;t have a defined ending. It will continue to run until the program it&#8217;s running in is terminated. Normally an **infinite loop** is created using **while** loop and is used for continuity of the same program.

In this short tutorial, you will learn how to make an **infinite loop** in both **PHP** & **Python**.

## PHP

There is no need of defining any variables.

<pre class="prettyprint"><code>while(1){
  print "Hello"
}</code></pre>

&nbsp;

It can be done in two other ways :

<pre class="prettyprint"><code>while(True){
  print "Hello"
}</code></pre>

<pre class="prettyprint"><code>for (;;) {
  print "Hello"
}</code></pre>

## Python

Same as before, but with some minor changes :

<pre class="prettyprint"><code>while(True) :
   print "Hello"</code></pre>