---
title: Creating a Contact Me page for Blogger Blog
author: Subin Siby
type: post
date: 2013-06-28T06:08:00+00:00
excerpt: 'Blogger has added a new widget in their widget page. This widget is a Contact Me&nbsp;widget which allows blog visitors to contact you.This can be added in your blog sidebar or footer. In this tutorial I will tell you to how to add this to a blog page....'
url: /creating-a-contact-me-page-for-blogger-blog
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
  - http://sag-3.blogspot.com/2013/06/creating-contact-me-page-for-blogger.html
syndication_item_hash:
  - 0b0ba427faca88d04a70f6784e434965
  - 0b0ba427faca88d04a70f6784e434965
  - 0b0ba427faca88d04a70f6784e434965
  - 0b0ba427faca88d04a70f6784e434965
  - 0b0ba427faca88d04a70f6784e434965
  - 0b0ba427faca88d04a70f6784e434965
categories:
  - Blogger
  - HTML
  - JavaScript
tags:
  - Blog
  - Email
  - Form
  - ID

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;"><b>Blogger </b>has added a new widget in their widget page. This widget is a <b>Contact Me</b>&nbsp;widget which allows blog visitors to contact you.</span></p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-NHz9t7Bhhww/Uc0slEnFCyI/AAAAAAAAC0Q/yrAJ6F9wgCY/s328/0023.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-NHz9t7Bhhww/Uc0slEnFCyI/AAAAAAAAC0Q/yrAJ6F9wgCY/s1600/0023.png" /></a>
  </div>
  
  <p>
    <span style="font-family: inherit;"><br /></span>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-0MIXMyRVXyI/Uc0eBfNy9cI/AAAAAAAACzw/MNCjZ_d8MG4/s678/0022.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><span style="font-family: inherit;"><img border="0" src="//3.bp.blogspot.com/-0MIXMyRVXyI/Uc0eBfNy9cI/AAAAAAAACzw/MNCjZ_d8MG4/s1600/0022.png" /></span></a>
  </div>
  
  <div>
    <span style="font-family: inherit;"><br /></span>
  </div>
  
  <div>
    <span style="font-family: inherit;">This can be added in your blog sidebar or footer. In this tutorial I will tell you to how to add this to a blog page.</span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Create a blank page from <b>Blogger -> Pages -> New Page -> Blank Page</b></span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Toggle to tab <b>HTML</b>&nbsp;and paste these code in the <b>HTML</b>&nbsp;textarea :</span>
  </div>
  
  <div>
    <blockquote>
      <p>
        <span style="font-family: inherit;"><div class="contact-form-widget"></span><br /><span style="font-family: inherit;"><div class="form"></span><br /><span style="font-family: inherit;"><form name="contact-form"></span><br /><span style="font-family: inherit;">Name</span><br /><span style="font-family: inherit;"><br /></span><br /><span style="font-family: inherit;"><input class="contact-form-name" id="ContactForm1_contact-form-name" name="name" size="30&#8243; type="text" value="" /></span><br /><span style="font-family: inherit;">Email</span><br /><span style="font-family: inherit;"><span style="font-weight: bolder;">*</span></span><br /><span style="font-family: inherit;"><br /></span><br /><span style="font-family: inherit;"><input class="contact-form-email" id="ContactForm1_contact-form-email" name="email" size="30&#8243; type="text" value="" /></span><br /><span style="font-family: inherit;">Message</span><br /><span style="font-family: inherit;"><span style="font-weight: bolder;">*</span></span><br /><span style="font-family: inherit;"><br /></span><br /><span style="font-family: inherit;"><textarea class="contact-form-email-message" cols="25&#8243; id="ContactForm1_contact-form-email-message" name="email-message" rows="5&#8243;></textarea><br /></span><br /><span style="font-family: inherit;"><br /></span><br /><span style="font-family: inherit;"><input class="</span><span style="background-color: white; font-family: inherit; white-space: pre-wrap;">contact-form-button contact-form-button-submit</span><span style="font-family: inherit;">" id="ContactForm1_contact-form-submit" type="button" value="Send" /></span><p style="display: inline-block;">Powered By <a href="http://sag-3.blogspot.com">Subin&#8217;s Blog</a></p><br /><span style="font-family: inherit;"><div style="max-width: 222px; text-align: center; width: 100%;"></span><br /><span style="font-family: inherit;"><div class="contact-form-error-message" id="ContactForm1_contact-form-error-message"></span><br /><span style="font-family: inherit;"></div></span><br /><span style="font-family: inherit;"><div class="contact-form-success-message" id="ContactForm1_contact-form-success-message"></span><br /><span style="font-family: inherit;"></div></span><br /><span style="font-family: inherit;"></div></span><br /><span style="font-family: inherit;"></form></span><br /><span style="font-family: inherit;"></div></span>
      </p>
    </blockquote>
  </div>
  
  <div>
    Now we have to register the widget to make it work properly. To do that Go to <b>Template -> Edit HTML</b>&nbsp;and scrollto the bottom of the code and paste the following code before <<b>/html></b>
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        <script><br />_WidgetManager._RegisterWidget(&#8216;_ContactFormView&#8217;, new _WidgetInfo(&#8216;ContactForm1&#8217;, &#8216;lowerbar2&#8217;, null, document.getElementById(&#8216;ContactForm1&#8217;), {&#8216;contactFormMessageSendingMsg&#8217;: &#8216;Sending&#8230;&#8217;, &#8216;contactFormMessageSentMsg&#8217;: &#8216;Your message has been sent.&#8217;, &#8216;contactFormMessageNotSentMsg&#8217;: &#8216;Message could not be sent. Please try again later.&#8217;, &#8216;contactFormInvalidEmailMsg&#8217;: &#8216;A valid email address is required.&#8217;, &#8216;contactFormEmptyMessageMsg&#8217;: &#8216;Message field cannot be empty.&#8217;, &#8216;title&#8217;: &#8216;Contact Form&#8217;, &#8216;blogId&#8217;: &#8216;<span class="str" style="background: wheat;">YOUR BLOG ID</span>&#8216;, &#8216;contactFormNameMsg&#8217;: &#8216;Name&#8217;, &#8216;contactFormEmailMsg&#8217;: &#8216;Email&#8217;, &#8216;contactFormMessageMsg&#8217;: &#8216;Message&#8217;, &#8216;contactFormSendMsg&#8217;: &#8216;Send&#8217;, &#8216;submitUrl&#8217;: &#8216;http://www.blogger.com/contact-form.do&#8217;}, &#8216;displayModeFull&#8217;));<br /></script>
      </p>
    </blockquote>
    
    <div>
      <span style="font-weight: bold;">Replace the <span style="color: red;">Wheat color background text</span> above to your blog id</span>. See <a href="http://sag-3.blogspot.com/2013/06/find-blogger-blog-id.html" >this post</a> to see how you can find blogger blog id.
    </div>
  </div>
  
  <div>
  </div>
  
  <div>
    When a user submits the <b>Contact Me</b>&nbsp;form you will get a mail from&nbsp;<b>Blogger </b>like this :
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-LKR0EUOdHLg/Uc0oCbyGqgI/AAAAAAAAC0A/vxI0JhzSqhY/s855/0024.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-LKR0EUOdHLg/Uc0oCbyGqgI/AAAAAAAAC0A/vxI0JhzSqhY/s1600/0024.png" /></a>
  </div>
</div>