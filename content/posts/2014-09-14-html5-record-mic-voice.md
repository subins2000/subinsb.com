---
title: Record, Play, Download Microphone Sound With HTML5
author: Subin Siby
type: post
date: 2014-09-14T18:12:23+00:00
url: /html5-record-mic-voice
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - HTML5
  - jQuery
tags:
  - Audio
  - Tutorials

---
**HTML5** has tons of new stuff that are exciting and very interesting. It arrived with features that would replace the need of **Flash** in browsers. Since the features that were available in Flash is coming into the native **JavaScript**, soon we won&#8217;t have to update our Flash player plugin and depend on **Adobe** for new awesome features.

More integration with hardware technologies to the Web is the main important thing. We can now access the audio & video hardware of the user&#8217;s system efficiently with less code.

In this tutorial, I will introduce you to a small plugin I created to record, play & download the microphone input voice of the user. This is made possible by the **Web Audio API**.

<div class="padlinks">
  <a class="download" href="https://github.com/subins2000/Francium-voice/archive/master.zip" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/Francium/voice" target="_blank">Demo</a>
</div>


# Project Abandoned

I&#8217;m abandoning Francium Voice due to lack of interest in maintaining it. No more support will be given in comments or email.

As an alternative you may use Yuji Miyane&#8217;s [WebAudioRecorderJS][2].

# NOTICE

Since Chrome 47, Web Audio API works only on HTTPS sites and localhost.

When Audio API is used for recording sound on non HTTPS sites and non localhost, console would say :

<pre><code class="php">getUserMedia() no longer works on insecure origins. To &lt;span class="keyword">use&lt;/span> &lt;span class="keyword">this&lt;/span> feature, you should consider switching your application to a secure origin, such &lt;span class="keyword">as&lt;/span> HTTPS. See https:&lt;span class="comment">//goo.gl/rStTGz for more details.
&lt;/span></code></pre>

and the Fr.voice would alert the message :

<pre class="prettyprint"><code>No Live Audio Input</code></pre>

So to use Fr.voice, your site must have an **SSL** certificate. But it will work in localhost even if it doesn&#8217;t have an HTTPS connection.

# Features

  * Record, Play, Download recorded audio
  * Ability to Pause Recording & Resume (Thanks <a href="//subinsb.com/html5-record-mic-voice#comment-2344087099" target="_blank">Gaurav</a>)
  * [Record for a specific time][5]
  * Convert **WAV audio to MP3** which will help reduce the size of recorded audio
  * Upload recorded audio to server as BLOB or base64 etc.

# Introduction

We will only use **HTML**, **JS** & **CSS**. There aren&#8217;t any server side stuff in this one. We only have one sub folder for the **JS** files called "cdn".

As I said before, I have created a **jQuery plugin** for recording the voice and managing it. It needs **RecorderJS** for working. You can get it from <a href="https://github.com/mattdiamond/Recorderjs" target="_blank">here</a>. Note that the plugin also needs "recorderWorker.js" which is in the same GitHub repository.

You can get my Voice plugin from <a href="//lab.subinsb.com/projects/jquery/voice/jquery.voice.js" target="_blank">here</a> and name it as "jquery.voice.js" in the "cdn" directory.

# Main Page

We have only one page to display to the user :

<pre class="prettyprint"><code>&lt;!DOCTYPE html&gt;
&lt;html&gt;
   &lt;head&gt;
    &lt;script src="//lab.subinsb.com/projects/jquery/core/jquery-2.1.1.js"&gt;&lt;/script&gt;
    &lt;script src="//lab.subinsb.com/projects/jquery/voice/recorder.js"&gt;&lt;/script&gt;
    &lt;script src="//lab.subinsb.com/projects/jquery/voice/jquery.voice.min.js"&gt;&lt;/script&gt;
    &lt;script src="cdn/record.js"&gt;&lt;/script&gt;
   &lt;/head&gt;
   &lt;body&gt;
    &lt;div id="content"&gt;
      &lt;h2&gt;Record, Play & Download Microphone Voice&lt;/h2&gt;
      &lt;audio controls src="" id="audio"&gt;&lt;/audio&gt;
      &lt;div style="margin:10px;"&gt;
        &lt;a class="button" id="record"&gt;Record&lt;/a&gt;
        &lt;a class="button disabled one" id="stop"&gt;Reset&lt;/a&gt;
        &lt;a class="button disabled one" id="play"&gt;Play&lt;/a&gt;
        &lt;a class="button disabled one" id="download"&gt;Download&lt;/a&gt;
        &lt;a class="button disabled one" id="base64"&gt;Base64 URL&lt;/a&gt;
        &lt;a class="button disabled one" id="mp3"&gt;MP3 URL&lt;/a&gt;
      &lt;/div&gt;
      &lt;input class="button" type="checkbox" id="live"/&gt;
      &lt;label for="live"&gt;Live Output&lt;/label&gt;
      &lt;style&gt;
      .button{
        display: inline-block;
        vertical-align: middle;
        margin: 0px 5px;
        padding: 5px 12px;
        cursor: pointer;
        outline: none;
        font-size: 13px;
        text-decoration: none !important;
        text-align: center;
        color:#fff;
        background-color: #4D90FE;
        background-image: linear-gradient(top,#4D90FE, #4787ED);
        background-image: -ms-linear-gradient(top,#4D90FE, #4787ED);
        background-image: -o-linear-gradient(top,#4D90FE, #4787ED);
        background-image: linear-gradient(top,#4D90FE, #4787ED);
        border: 1px solid #4787ED;
        box-shadow: 0 1px 3px #BFBFBF;
      }
      a.button{
        color: #fff;
      }
      .button:hover{
        box-shadow: inset 0px 1px 1px #8C8C8C;
      }
      .button.disabled{
        box-shadow:none;
        opacity:0.7;
      }
      &lt;/style&gt;
    &lt;/div&gt;
   &lt;/body&gt;
&lt;/html&gt;</code></pre>

As you can see, we only use CSS for styling the button and nothing else. We also don&#8217;t include the "recorderWorker.js", because it&#8217;s loaded by the "recorder.js" which we have included in the page.

# record.js

This file adds event listeners to the buttons to control recording and initiates the functions :

<pre class="prettyprint"><code>function restore(){
  $("#record, #live").removeClass("disabled");
  $(".one").addClass("disabled");
  Fr.voice.stop();
}
$(document).ready(function(){
  $(document).on("click", "#record:not(.disabled)", function(){
    elem = $(this);
    Fr.voice.record($("#live").is(":checked"), function(){
      elem.addClass("disabled");
      $("#live").addClass("disabled");
      $(".one").removeClass("disabled");
    });
  });
  
  $(document).on("click", "#stop:not(.disabled)", function(){
    restore();
  });
  
  $(document).on("click", "#play:not(.disabled)", function(){
    Fr.voice.export(function(url){
      $("#audio").attr("src", url);
      $("#audio")[0].play();
    }, "URL");
    restore();
  });
  
  $(document).on("click", "#download:not(.disabled)", function(){
    Fr.voice.export(function(url){
      $("&lt;a href='"+url+"' download='MyRecording.wav'&gt;&lt;/a&gt;")[0].click();
    }, "URL");
    restore();
  });
  
  $(document).on("click", "#base64:not(.disabled)", function(){
    Fr.voice.export(function(url){
      console.log("Here is the base64 URL : " + url);
      alert("Check the web console for the URL");
      
      $("&lt;a href='"+ url +"' target='_blank'&gt;&lt;/a&gt;")[0].click();
    }, "base64");
    restore();
  });
  
  $(document).on("click", "#mp3:not(.disabled)", function(){
    alert("The conversion to MP3 will take some time (even 10 minutes), so please wait....");
    Fr.voice.export(function(url){
      console.log("Here is the MP3 URL : " + url);
      alert("Check the web console for the URL");
      
      $("&lt;a href='"+ url +"' target='_blank'&gt;&lt;/a&gt;")[0].click();
    }, "mp3");
    restore();
  });
});</code></pre>

The **restore()** function reverts the action buttons to it&#8217;s original state ( Record &#8211; enabled, other buttons &#8211; disabled).

# Configure

The location of the "recorderWorker.js" should be mentioned in the **Fr.workerPath** item of "jquery.voice.js" file. This is really necessary for the working. By default, it&#8217;s value is :

<pre class="prettyprint"><code>cdn/recorderWorker.js</code></pre>

It&#8217;s a relative path from the file it&#8217;s ran. If the file location is http://mySite.com/project/index.html and "recorderWorker.js" is in http://mySite.com/project/cdn/recorderWorker.js , then the value should be the above "cdn/recorderWorker.js".

From version **0.3** onwards, if you&#8217;re going to use **MP3** conversion, you have to configure the value of **Fr.mp3WorkerPath** item. The default value is :

<pre class="prettyprint"><code>cdn/mp3Worker.js</code></pre>

Also, for **MP3** you have to define the path of **<a href="//lab.subinsb.com/projects/jquery/voice/libmp3lame.min.js" target="_blank">libmp3lame.min.js</a>** in the first line of **<a href="//lab.subinsb.com/projects/jquery/voice/mp3Worker.js" target="_blank">mp3Worker.js</a>**.

# Recording

We start recording by calling the function **Fr****.voice.record();** and the user will be asked for authorizing the use of microphone. if the user accepts it, recording will start and if user declined, an alert box will be opened.

<pre class="prettyprint"><code>Fr.voice.record(false, callback());</code></pre>

There is also a feature to live output the recording voice to the speaker. You can do this by making the first parameter of **Fr.voice.record()** to boolean **true**.

The recording is stopped using :

<pre class="prettyprint"><code>Fr.voice.stop();
</code></pre>

It doesn&#8217;t accept any parameters and the return value will be **Fr.voice** object itself.

# Pausing & Resuming

You can pause the audio recording and resume later with Fr.voice. To pause recording, just do this :

<pre class="prettyprint"><code>Fr.voice.pause();</code></pre>

and sometime later, you can resume the recording by :

<pre class="prettyprint"><code>Fr.voice.resume();</code></pre>

This feature was added in version **0.4**

# Record For A Specific Time

You can record audio for a specific time :

<pre class="prettyprint"><code>Fr.voice.record(...);
Fr.voice.stopRecordingAfter(5000, function(){
  alert("Time limit reached.");
});
</code></pre>

Do the recording normally and call **Fr.voice.stopRecordingAfter()** with the limiting time. The time should be in milliseconds. The above example uses "5000" = 5 seconds.

# Export

**Fr.voice.export** can be used for obtaining the recorded audio **URL** and the blob data of the recorder audio. Example of obtaining blob data :

<pre class="prettyprint"><code>Fr.voice.export(function(blob){
  console.log(blob); // The blob data
}, "blob");</code></pre>

It is not necessarily required to make the 2nd parameter value to "blob", because by default it&#8217;s set as "blob".

You can also get the **blob URL** of the recorded audio :

<pre class="prettyprint"><code>Fr.voice.export(function(url){
  console.log(url); // The blob URL
}, "URL");</code></pre>

An example of making an audio element to play the audio file :

<pre class="prettyprint"><code>Fr.voice.export(function(url){
  $("&lt;audio src='"+ url +"'&gt;&lt;/audio&gt;").appendTo("body");
  $("body audio:last")[0].play();
}, "URL");</code></pre>

## Base64

(This feature has been suggested by the user [**payam**][15] in the comments)

You can obtain the **base64** data of the audio with **$.voice.export** and this data can be used to store the audio in the **database**.

<pre class="prettyprint"><code>Fr.voice.export(function(base64){
  $.post("server.php", {"audio" : base64}, function(){
    // Sent To Server
  });
}, "base64");
</code></pre>

All you got to do in the server is to store the sent data in the database.

## Send BLOB Directly

You can also upload the BLOB file using **HTML5**&#8216;s **FormData** function. Example :

<pre class="prettyprint"><code>Fr.voice.export(function(blob){
  var data = new FormData();
  data.append('file', blob);
  
  $.ajax({
    url: "server.php",
    type: 'POST',
    data: data,
    contentType: false,
    processData: false,
    success: function(data) {
      // Sent to Server
    }
  });
}, "blob");</code></pre>

# Export MP3

You can export **MP3** (from version 0.3) as blob, base64 or blob URL just like exporting WAV :

<pre class="prettyprint"><code>Fr.voice.exportMP3(function(blob){
  console.log(blob);
});

Fr.voice.exportMP3(function(base64){
  console.log(base64);
}, "base64");

Fr.voice.exportMP3(function(url){
  console.log(url); // The blob URL
}, "URL");
</code></pre>

# Server

## Save In Database

Here is an example of the server side script using **PDO** :

<pre class="prettyprint"><code>&lt;?php
if(isset($_POST['audio'])){
  $audio = $_POST['audio'];
  $sql = $PDO-&gt;prepare("INSERT INTO `myTable` VALUES (?)");
  $sql-&gt;execute(array($audio));
}</code></pre>

Now, the database table will have a value like this :

<pre class="prettyprint"><code>data:audio/wav;base64,UklGRiQAAgBXQVZFZm10IBAAAAABAAIARKwAABCxAgAEABAAZGF0YQAAAgAAAAAAAAAAAAAAA.......</code></pre>

This will be a long long value. So, make sure the database column&#8217;s type is set to "LONGTEXT" in **MySQL**.

When you want to play it, make an audio element with \`src\` attribute with this base64 value obtained from database. Example :

<pre class="prettyprint"><code>&lt;?php
$sql = $PDO-&gt;query("SELECT `audio` FROM `myTable` WHERE `id` = 'whatever'");
$base64 = $sql-&gt;fetchColumn();
echo "&lt;audio src='". $base64 ."'&gt;&lt;/audio&gt;";</code></pre>

## Saving as Files

This is the same as the previous one. Send the base64 data to the server and in the server, do as follows :

<pre class="prettyprint"><code>&lt;?php
if(isset($_POST['audio'])){
  $audio = $_POST['audio'];
  $audio = str_replace('data:audio/wav;base64,', '', $audio);
  $decoded = base64_decode($audio);
  $file_location = "./save_folder/recorded_audio.wav";
 
  file_put_contents($file_location, $decoded);
}</code></pre>

And when you need to retrieve audio, just link the src value of the audio object to the file location in server.

## Saving As BLOB In Database

Another method to save is storing the original audio data as blob in MySQL database. For this create a uploads table :

<pre class="prettyprint"><code>CREATE TABLE IF NOT EXISTS `uploads` (
  `id` int(11) NOT NULL,
  `audio` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
</code></pre>

Then, get the blob from browser and save it. Example :

<pre class="prettyprint"><code>&lt;?php
if(isset($_FILES['file'])){
  $audio = file_get_contents($_FILES['file']['tmp_name']);
 
  require_once __DIR__ . "/db.php";
  $sql = $dbh-&gt;prepare("INSERT INTO `uploads` (`audio`) VALUES(?)");
  $sql-&gt;execute(array($audio));
 
  $sql = $dbh-&gt;query("SELECT `id` FROM `uploads` ORDER BY `id` DESC LIMIT 1");
  $id = $sql-&gt;fetchColumn();
 
  echo "play.php?id=$id";
}</code></pre>

The "db.php" contains variable **$PDO** that contains the PDO object to access database. Then when you need to get it back, use this as "play.php" file :

<pre class="prettyprint"><code>&lt;?php
if(isset($_GET['id'])){
  require_once __DIR__ . "/db.php";
  $sql = $dbh-&gt;prepare("SELECT `audio`, LENGTH(`audio`) FROM `uploads` WHERE `id` = ?");
  $sql-&gt;execute(array($_GET['id']));
  $result = $sql-&gt;fetch();

  $audio = $result[0];
  $size = $result[1];
 
  header("Content-Length: $size");
  header("Content-Type: audio/wav");
  echo $audio;
}</code></pre>

If you used the **HTML5**&#8216;s **FormData()** method, then use **$_FILES[&#8216;file&#8217;]** variable to access the file. You can see this example above.

That&#8217;s it. The Voice plugin contains only **90** lines of code, because most of the work is done by "recorder.js". The Voice plugin is just for making the complicated "recorder.js" file to a simple one.

 [1]: #project-abandoned
 [2]: https://github.com/higuma/web-audio-recorder-js
 [3]: #notice
 [4]: #features
 [5]: #article-record-for-a-specific-time
 [6]: #introduction
 [7]: #main-page
 [8]: #recordjs
 [9]: #configure
 [10]: #recording
 [11]: #pausing-resuming
 [12]: #record-for-a-specific-time
 [13]: #export
 [14]: #base64
 [15]: //subinsb.com/html5-record-mic-voice#comment-1813336374
 [16]: #send-blob-directly
 [17]: #export-mp3
 [18]: #server
 [19]: #save-in-database
 [20]: #saving-as-files
 [21]: #saving-as-blob-in-database
