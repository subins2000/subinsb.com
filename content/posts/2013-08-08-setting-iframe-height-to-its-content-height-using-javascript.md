---
title: Setting Iframe height to its content height using Javascript
author: Subin Siby
type: post
date: 2013-08-08T03:30:00+00:00
excerpt: "While loading an iframe on page the most difficult thing is to set it's height to the height of the content of the iframe. I looked around the web and got the code. This is pure Javascript, doesn't contain&nbsp;jQuery&nbsp;or any other libraries. Set t..."
url: /setting-iframe-height-to-its-content-height-using-javascript
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
  - http://sag-3.blogspot.com/2013/08/set-iframe-height-to-its-content-height.html
syndication_item_hash:
  - 8db8433dc4e069040678700011ce5ed8
  - 8db8433dc4e069040678700011ce5ed8
  - 8db8433dc4e069040678700011ce5ed8
  - 8db8433dc4e069040678700011ce5ed8
  - 8db8433dc4e069040678700011ce5ed8
  - 8db8433dc4e069040678700011ce5ed8
categories:
  - CSS
  - HTML
  - JavaScript
tags:
  - event
  - iframe
  - Scroll

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  While loading an iframe on page the most difficult thing is to set it&#8217;s height to the height of the content of the iframe. I looked around the web and got the code. This is pure <b>Javascript</b>, doesn&#8217;t contain&nbsp;<b>jQuery</b>&nbsp;or any other libraries. Set these functions :</p> 
  
  <blockquote class="tr_bq">
    <p>
      function getDocHeight(doc) {<br />&nbsp; &nbsp; doc = doc || document;<br />&nbsp; &nbsp; var body = doc.body, html = doc.documentElement;<br />&nbsp; &nbsp; var height = Math.max( body.scrollHeight, body.offsetHeight,<br />&nbsp; &nbsp; &nbsp; &nbsp; html.clientHeight, html.scrollHeight, html.offsetHeight );<br />&nbsp; &nbsp; return height;<br />}<br />function setIframeHeight(id) {<br />&nbsp; &nbsp; var ifrm = id;<br />&nbsp; &nbsp; var doc = ifrm.contentDocument? ifrm.contentDocument: ifrm.contentWindow.document;<br />&nbsp; &nbsp; ifrm.style.visibility = &#8216;hidden&#8217;;<br />&nbsp; &nbsp; ifrm.style.height = "10px"; // reset to minimal height in case going from longer to shorter doc<br />&nbsp; &nbsp; // some IE versions need a bit added or scrollbar appears<br />&nbsp; &nbsp; ifrm.style.height = getDocHeight( doc ) + 4 + "px";<br />&nbsp; &nbsp; ifrm.style.visibility = &#8216;visible&#8217;;<br />}
    </p>
  </blockquote>
  
  <p>
    Now add an event listener to the iframe :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      onload=&#8217;setIframeHeight(this)&#8217;
    </p>
  </blockquote>
  
  <p>
    After adding the event listener, the iframe element will look like this :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <iframe&nbsp;onload="setIframeHeight(this)" src="subins.html"></iframe>
    </p>
  </blockquote>
  
  <p>
    When the iframe completes loading the function will check it&#8217;s content height and set the height of the iframe to it&#8217;s content height. Pretty Neat, Huh ?
  </p>
</div>