---
title: How to add Subins Like button to blogger/blogspot
author: Subin Siby
type: post
date: 2012-12-09T08:07:00+00:00
excerpt: 'You might have know The Subins Project&nbsp;created by me. It is hosted by AppFog. If you want to add Subins Like button to your blog, then follow these steps.Go to BloggerPlace this code where you want to add the Subins&nbsp;Like Button.&lt;script src...'
url: /how-to-add-subins-like-button-to-bloggerblogspot
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - e1a18e1791d5948cb9ca120f7ed5d611
  - e1a18e1791d5948cb9ca120f7ed5d611
  - e1a18e1791d5948cb9ca120f7ed5d611
  - e1a18e1791d5948cb9ca120f7ed5d611
  - e1a18e1791d5948cb9ca120f7ed5d611
  - e1a18e1791d5948cb9ca120f7ed5d611
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
  - http://sag-3.blogspot.com/2012/12/subins-like-button-code.html
categories:
  - Blogger
  - HTML
  - JavaScript
tags:
  - AppFog
  - Subins

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">You might have know <b>The Subins Project</b>&nbsp;created by me. It is hosted by <b><a href="http://appfog.com/?utm_source=subinsblog" >AppFog</a></b>. If you want to add Subins Like button to your blog, then follow these steps.</span></p> 
  
  <div>
    <span style="font-family: inherit;">Go to <b><a href="http://blogger.com/?utm_source=subinsblog" >Blogger</a></b></span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Place this code where you want to add the <b>Subins</b>&nbsp;<b>Like Button</b>.</span>
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        <span style="font-family: inherit;"><script src="http://subins.hp.af.cm/dev/all.js" type="text/javascript"></script><br /><div class="subins-like" data-href=&#8217;<span style="color: red;">BLOG URL</span>&#8216;></div></span>
      </p>
    </blockquote>
  </div>
  
  <div>
    <span style="font-family: inherit;">If you want to add like button on post footer then follow the steps mentioned below.</span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Go to <b><a href="http://blogger.com/?utm_source=subinsblog" >Blogger</a></b>&nbsp;-> <b>Template</b>&nbsp;-><b>&nbsp;Edit HTML</b></span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Click <b>Expand Widget Templates</b>.</span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Search for the line <b>post-footer&#8217; </b>(within the quote).</span>
  </div>
  
  <div>
    <span style="font-family: inherit;">Add this code after the first line you just founded.</span>
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        <span style="font-family: inherit;"><script src=&#8217;http://subins.hp.af.cm/dev/all.js&#8217; type=&#8217;text/javascript&#8217;/></span><span style="font-family: inherit;"><div class=&#8217;subins-like&#8217; expr:data-href=&#8217;data:post.url&#8217;></div></span>
      </p>
    </blockquote>
  </div>
  
  <div>
    That&#8217;s it. Check your blog to see your new <b>Subins Like button</b>.
  </div>
  
  <div>
    <b><a href="http://appfog.com/?utm_source=subinsblog" >AppFog</a></b>&nbsp;servers are a little slow so sometimes you might get this error<b> this webpage isn&#8217;t available</b>.
  </div>
</div>