---
title: 'Open : An Open Source Social Network Is Released'
author: Subin Siby
type: post
date: 2013-12-04T17:40:33+00:00
url: /open-an-open-source-social-network-is-released
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 2024668289
  - 2024668289
  - 2024668289
  - 2024668289
  - 2024668289
  - 2024668289
tags:
  - Network
  - Open

---
I&#8217;m excited to announce that **Open**, the **Open Source Social Network** initiated by me is released. Actually it has been released on **December 1**st, but was undergoing many bug fixes and was making it smooth on the **WWW**. You can access **Open** @ **<http://open.subinsb.com>. **If you would like to contribute to this project, there are a few ways :

  1. Report Bugs, Issues & Suggestions (<https://github.com/subins2000/open/issues>)
  2. Fix errors on Source Code (<https://github.com/subins2000/open>)
  3. Try Out Open (**<http://open.subinsb.com>**)
  4. Share the link (**<http://open.subinsb.com>**) with your friends and others.

As it is an **Open Source Project**, there are no ads. Each contributer of this project is a developer of this project. **It&#8217;s your contribution that makes Open Powerful**. Let&#8217;s create the **world more connective and more Open**. Here is the logo of **Open**. If you have a better logo, please submit it.

[<img class="size-full wp-image-2062 aligncenter" alt="Open Logo" src="//lab.subinsb.com/projects/blog/uploads/2013/11/logo1.png" width="185" height="69" />][1]

Your feedback is my everything. What do you think about this project ? Please suggest out anything you have in your mind even if it is harsh.

 [1]: //lab.subinsb.com/projects/blog/uploads/2013/11/logo1.png