---
title: 'Advanced Rest Client : Create and Test custom HTTP requests.'
author: Subin Siby
type: post
date: 2013-06-08T07:42:00+00:00
excerpt: 'I recently noticed a Google Chrome Webstore&nbsp;app that can be used for making a request to a web site or local site. I loved the app from the beginning. You can send a request to any site with header data, form data etc...You can also attach a file ...'
url: /advanced-rest-client-create-and-test-custom-http-requests
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1964405456
  - 1964405456
  - 1964405456
  - 1964405456
  - 1964405456
  - 1964405456
syndication_item_hash:
  - c9a4a8b6e5440d8dc2967c658ff3a668
  - c9a4a8b6e5440d8dc2967c658ff3a668
  - c9a4a8b6e5440d8dc2967c658ff3a668
  - c9a4a8b6e5440d8dc2967c658ff3a668
  - c9a4a8b6e5440d8dc2967c658ff3a668
  - c9a4a8b6e5440d8dc2967c658ff3a668
syndication_permalink:
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
  - http://sag-3.blogspot.com/2013/06/advanced-rest-client-create-and-test.html
tags:
  - Chrome
  - Client
  - HTTP
  - Rest Client
  - Webstore

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  I recently noticed a <b>Google Chrome Webstore</b>&nbsp;app that can be used for making a request to a web site or local site. I loved the app from the beginning. You can send a request to any site with header data, form data etc&#8230;</p> 
  
  <p>
    You can also attach a file to the form. <span style="color: red;"><b>Ain&#8217;t that Awesome</b>.</span><br />Every web developer must have this app. Install it from <b><a href="https://chrome.google.com/webstore/detail/advanced-rest-client/hgmloofddffdnphfgcellkdfbfbjeloo" >Chrome&nbsp;Webstore</a></b>
  </p>
  
  <h3 style="text-align: left;">
    <span style="font-size: large;">Features</span>
  </h3>
  
  <ul style="text-align: left;">
    <li>
      &#8211; Integrated with Google Drive
    </li>
    <li>
      &#8211; It&#8217;s own backend service to store and share data between coworkers
    </li>
    <li>
      &#8211; convenient HTTP headers and payload editor thanks CodeMirror
    </li>
    <li>
      &#8211; Make a HTTP request (via XmlHttpRequest level 2)
    </li>
    <li>
      &#8211; Debug socket (via web socket API).
    </li>
    <li>
      &#8211; JSON response viewer
    </li>
    <li>
      &#8211; XML response viewer
    </li>
    <li>
      &#8211; set custom headers &#8211; even does not supported by XmlHttpRequest object
    </li>
    <li>
      &#8211; help with filling HTTP headers (hint + code completion)
    </li>
    <li>
      &#8211; add headers list as raw data or via form
    </li>
    <li>
      &#8211; construct POST or PUT body via raw input, form or send file(s) with request
    </li>
    <li>
      &#8211; set custom form encoding
    </li>
    <li>
      &#8211; remember latest request (save current form state and restore on load)
    </li>
    <li>
      &#8211; save (Ctrl+S) and open (Ctrl+O) saved request forms
    </li>
    <li>
      &#8211; history support
    </li>
    <li>
      &#8211; data import/export
    </li>
  </ul>
  
  <p>
  </p>
  
  <div class="separator" style="clear: both; text-align: center; vertical-align: top;">
  </div>
  
  <p>
    <span style="vertical-align: top;">I am giving this app </span><span style="color: red; font-size: x-large; vertical-align: top;">4/5 </span><span style="font-size: x-large; vertical-align: top;">stars&nbsp;</span><br />It&#8217;s <b>4</b>&nbsp;because it doesn&#8217;t support image mime types and it gives a <b>Unexpected Token</b>&nbsp;while parsing some <b>JSON</b>&nbsp;responses.</div>