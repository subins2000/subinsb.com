---
title: Subins Update 1.2
author: Subin Siby
type: post
date: 2013-04-01T04:45:00+00:00
excerpt: "Subins has been updated to 1.2. The new changes have been made on Subins and is updated today. The sites that affected the changes are:Subins ChatIn Subins&nbsp;Chat the design has been changed. The bugs have been fixed and It's more fast than before.S..."
url: /subins-update-1-2
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - f29d09abb37fe2c833939b4d77bfebed
  - f29d09abb37fe2c833939b4d77bfebed
  - f29d09abb37fe2c833939b4d77bfebed
  - f29d09abb37fe2c833939b4d77bfebed
  - f29d09abb37fe2c833939b4d77bfebed
  - f29d09abb37fe2c833939b4d77bfebed
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
  - http://sag-3.blogspot.com/2013/04/subins-update-12.html
tags:
  - AppFog
  - FriendsHood
  - Games
  - Subins

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Subins has been updated to 1.2. The new changes have been made on Subins and is updated today. The sites that affected the changes are:</p> 
  
  <div id="toc_container" class="no_bullets">
    <p class="toc_title">
      Contents
    </p>
    
    <ul class="toc_list">
      <li>
        <a href="#subins-chat"><span class="toc_number toc_depth_1">1</span> Subins Chat</a>
      </li>
      <li>
        <a href="#accounts"><span class="toc_number toc_depth_1">2</span> Accounts</a>
      </li>
      <li>
        <a href="#subins"><span class="toc_number toc_depth_1">3</span> Subins</a>
      </li>
      <li>
        <a href="#friendshood"><span class="toc_number toc_depth_1">4</span> Friendshood</a>
      </li>
      <li>
        <a href="#subins-get"><span class="toc_number toc_depth_1">5</span> Subins GET</a>
      </li>
    </ul>
  </div>
  
  <h2 style="text-align: left;">
    <span id="subins-chat"><u><a href="http://chat-subins.hp.af.cm/" >Subins Chat</a></u></span>
  </h2>
  
  <div>
    In <b>Subins</b>&nbsp;<b>Chat </b>the design has been changed. The bugs have been fixed and It&#8217;s more fast than before.<br /><b>Subins Chat</b>&nbsp;is not only a group chat but a place for submitting <b>suggestions, bugs, errors and feedback</b>.<br />Please report any errors or bugs&nbsp;happening on any of the sites of <b>Subins</b>&nbsp;in&nbsp;<b>Subins Chat</b>.</p> 
    
    <h2 style="text-align: left;">
      <span id="accounts"><u><a href="http://accounts-subins.hp.af.cm/" >Accounts</a></u></span>
    </h2>
  </div>
  
  <div>
    Only the <b>PHP</b>&nbsp;code is changed. Others are all the same. Added extra security measures.
  </div>
  
  <h2 style="text-align: left;">
    <span id="subins"><u><a href="http://subins.hp.af.cm/" >Subins</a></u></span>
  </h2>
  
  <div>
    <b>PHP</b>&nbsp;code changed. The pagination <b>CSS</b>&nbsp;has been changed. More colourful. Automatic adding of results.
  </div>
  
  <h2 style="text-align: left;">
    <span id="friendshood"><u><a href="http://fd-subins.hp.af.cm/" >Friendshood</a></u></span>
  </h2>
  
  <div>
    <b>Friendshood</b> was discontinued a while back, but now it has been restored. <b>Friendshood</b>&#8216;s style has been changed. <b>Like</b>&nbsp;button, <b>Comment</b>&nbsp;button and<b>&nbsp;Share</b>&nbsp;button style is changed to new and attarctive style. <b>Uploading</b>&nbsp;<b>Image</b>&nbsp;bug has been fixed, Thanks to <b>ImgUr</b>.&nbsp;
  </div>
  
  <h2 style="text-align: left;">
    <span id="subins-get"><u><a href="http://get-subins.hp.af.cm/" >Subins GET</a></u></span>
  </h2>
  
  <div>
    <b>Subins Apps</b>&nbsp;and <b>Subins Games</b>&nbsp;have been merged to <b>Subins GET</b>.&nbsp;
  </div>
  
  <div>
    <b>Subins GET </b>has a new function which&nbsp;<b>Converts YouTube video to MP3</b>. The converted MP3 file is added to <b>Subins GET&nbsp;</b>&nbsp;database with the video picture, <b>URL</b>&nbsp;and uploader name.&nbsp;
  </div>
  
  <div>
    <b>Subins GET</b>&nbsp;is the site which has a lot of changes in this update.
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-54eg75gVyWI/UVkQchwwwoI/AAAAAAAACdE/KO0_x6NhJmU/s1600/logo.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="146" src="https://2.bp.blogspot.com/-54eg75gVyWI/UVkQchwwwoI/AAAAAAAACdE/KO0_x6NhJmU/s400/logo.png" width="400" /></a>
  </div>
  
  <div>
  </div>
</div>