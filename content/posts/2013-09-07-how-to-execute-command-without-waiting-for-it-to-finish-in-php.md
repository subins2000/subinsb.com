---
title: Execute Command Without Waiting For It To Finish
author: Subin Siby
type: post
date: 2013-09-07T03:41:00+00:00
excerpt: 'If you just execute a terminal command using exec&nbsp;or system&nbsp;in PHP, the page will only load completely until the command has been finished. If you are running commands to run a websocket&nbsp;server, the page will not load, because the start ...'
url: /how-to-execute-command-without-waiting-for-it-to-finish-in-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
  - http://sag-3.blogspot.com/2013/09/how-to-execute-command-without-waiting.html
syndication_item_hash:
  - 76d6aedc22a63785613c2bd26f039c85
  - 76d6aedc22a63785613c2bd26f039c85
  - 76d6aedc22a63785613c2bd26f039c85
  - 76d6aedc22a63785613c2bd26f039c85
  - 76d6aedc22a63785613c2bd26f039c85
  - 76d6aedc22a63785613c2bd26f039c85
dsq_thread_id:
  - 1966153605
  - 1966153605
  - 1966153605
  - 1966153605
  - 1966153605
  - 1966153605
categories:
  - PHP
tags:
  - exec
  - Execute
  - Terminal
  - WebSocket

---
<div dir="ltr" style="text-align: left;">
  If you just execute a terminal command using <b>exec</b> or <b>system</b> in <b>PHP</b>, the page will only load completely until the command has been finished. If you are running commands to run a <b>websocket</b> <b>server</b>, the page will not load, because the start server command won&#8217;t terminate. So to avoid this problem you should just do a simple trick, Just add <b>> /dev/null &</b> after the command you&#8217;re running.<br /> Here is a quick example :</p> 
  
  <blockquote class="tr_bq">
    <p>
      exec("php -q server.php");
    </p>
  </blockquote>
  
  <p>
    becomes:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      exec("php -q server.php  > /dev/null &");
    </p>
  </blockquote>
  
  <p>
    Or you can wrap it all in a function to run the commands in both <b>Windows </b>and <b>Linux</b> :
  </p>
  
  <pre class="prettyprint"><code>function &lt;span style="color: red;">bgExec&lt;/span>($cmd) {
 if(substr(php_uname(), 0, 7) == "Windows"){
  pclose(popen("start /B ". $cmd, "r")); 
 }else {
  exec($cmd . " &gt; /dev/null &"); 
 }
}</code></pre>
  
  <p>
    So, by the function you can call the command like this :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      bgExec("php -q server.php");
    </p>
  </blockquote>
  
  <p>
    Use it as you wish.
  </p>
</div>