---
title: Wine 1.6 is released with 10,000 changes
author: Subin Siby
type: post
date: 2013-07-20T03:30:00+00:00
excerpt: 'After 16 months of development, Wine&nbsp;version 1.6&nbsp;has been officially released by the Wine Team. Wine&nbsp;is the Windows&nbsp;Program emulator for Linux&nbsp;and Mac. You can download the source files from here. You can only get the source fi...'
url: /wine-1-6-is-released-with-10000-changes
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
syndication_item_hash:
  - 65e5e7f01c31f5106e42bef19b4f8f80
  - 65e5e7f01c31f5106e42bef19b4f8f80
  - 65e5e7f01c31f5106e42bef19b4f8f80
  - 65e5e7f01c31f5106e42bef19b4f8f80
  - 65e5e7f01c31f5106e42bef19b4f8f80
  - 65e5e7f01c31f5106e42bef19b4f8f80
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
  - http://sag-3.blogspot.com/2013/07/wine-16-is-released-with-10000-changes.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
categories:
  - Linux
  - Windows
  - Wine
tags:
  - Mac
  - NET

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">After 16 months of development, <b>Wine</b>&nbsp;version <b>1.6&nbsp;</b>has been officially released by the <b>Wine Team</b>. <b>Wine</b>&nbsp;is the <b>Windows</b>&nbsp;Program emulator for <b>Linux</b>&nbsp;and <b>Mac</b>. You can download the source files from <a href="http://prdownloads.sourceforge.net/wine/wine-1.6.tar.bz2">here</a>. You can only get the source files now.&nbsp;<span style="background-color: white;">Binary packages are in the process of being built. If it&#8217;s been build you can d</span>ownload the <b>Wine</b>&nbsp;<b>1.6</b>&nbsp;version from <a href="http://www.winehq.org/download" >here</a>.</span></p> 
  
  <h2 style="text-align: left;">
    <b><span style="font-family: inherit; font-size: large;">What&#8217;s New ?</span></b>
  </h2>
  
  <ol style="text-align: left;">
    <li>
      <span style="font-family: inherit;">New User Interface</span>
    </li>
    <li>
      <span style="font-family: inherit;">Networking capabilities</span>
    </li>
    <li>
      <span style="font-family: inherit;">Support for <b>Windows</b>&nbsp;Transparency</span>
    </li>
    <li>
      <span style="font-family: inherit;">Graphics improved</span>
    </li>
    <li>
      <span style="font-family: inherit;">JoyStick Installing</span>
    </li>
    <li>
      <span style="background-color: white;"><span style="font-family: inherit;">Mono package for .NET applications support</span></span>
    </li>
    <li>
      <span style="background-color: white;"><span style="font-family: inherit;">Support of new softwares and games.</span></span>
    </li>
    <li>
      <span style="background-color: white;"><span style="font-family: inherit;">Improved Input validation</span></span>
    </li>
    <li>
      <span style="background-color: white;"><span style="font-family: inherit;">Mac Driver</span></span>
    </li>
    <li>
      Direct3D
    </li>
  </ol>
  
  <p>
    <span style="font-family: inherit;">and more. <b>Wine Team</b>&nbsp;officially said that the new <b>Wine </b>version&nbsp;more than <b>10,000</b>&nbsp;changes.&nbsp;</span></div>