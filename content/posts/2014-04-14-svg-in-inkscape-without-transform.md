---
title: Create SVG Images In Inkscape Without Transforms
author: Subin Siby
type: post
date: 2014-04-14T06:21:12+00:00
url: /svg-in-inkscape-without-transform
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML5
  - SVG
tags:
  - Image
  - Inkscape

---
<a href="http://inkscape.org" target="_blank">Inkscape</a> is a beautiful Open Source Image editing software. It can be used to create PNG images, JPG images and even SVG images. But, when you export an SVG image, it will have some kind of transform style in it such as translate. This will make the SVG image code so long. To make it short, it&#8217;s better to remove the transform on the image and save it as it should be.

But, Inkscape doesn&#8217;t provide a option on the save dialog for this. You have to do it on your own. In this post, I&#8217;ll tell you the manual way to remove the transform on SVG images created using Inkscape.

This the SVG image code where w are going to remove the transform property :

<pre class="prettyprint"><code>&lt;?xml version="1.0" encoding="UTF-8" standalone="no"?&gt;
&lt;!-- Created with Inkscape (http://www.inkscape.org/) --&gt;
&lt;svg height="48px" width="48px" style="background:black;"&gt;
 &lt;g id="layer1" transform="translate(7,17.428571)"&gt;
  &lt;path style="stroke-linejoin:miter;stroke:#000;stroke-linecap:butt;stroke-width:1px;fill:#ffffff;" d="m15.571,7.2857c0.86668,0.25785,0.08001,1.2955-0.42857,1.4405-1.3782,0.39293-2.4292-1.0759-2.4524-2.2976-0.0415-2.1854,2.1429-3.6267,4.1667-3.4643,2.97,0.23833,4.8481,3.2202,4.4762,6.0357-0.495,3.753-4.3,6.079-7.904,5.488-4.5359-0.744-7.3153-5.3823-6.5004-9.7737,0.98692-5.3186,6.4649-8.5534,11.643-7.5119,6.1021,1.2273,9.7936,7.5478,8.5238,13.512-1.467,6.886-8.632,11.035-15.382,9.536-7.6693-1.704-12.277-9.715-10.547-17.25,1.9402-8.4536,10.798-13.52,19.119-11.56,9.2377,2.1763,14.763,11.882,12.571,20.988" /&gt;
 &lt;/g&gt;
&lt;/svg&gt;</code></pre>

As you can see there is a transform property on the "g" tag of the image. Here is how the image look like :

<center>
  <svg height="48px" width="48px" style="background:black;"><g id="layer1" transform="translate(7,17.428571)"><path style="stroke-linejoin:miter;stroke:#000;stroke-linecap:butt;stroke-width:1px;fill:#ffffff;" d="m15.571,7.2857c0.86668,0.25785,0.08001,1.2955-0.42857,1.4405-1.3782,0.39293-2.4292-1.0759-2.4524-2.2976-0.0415-2.1854,2.1429-3.6267,4.1667-3.4643,2.97,0.23833,4.8481,3.2202,4.4762,6.0357-0.495,3.753-4.3,6.079-7.904,5.488-4.5359-0.744-7.3153-5.3823-6.5004-9.7737,0.98692-5.3186,6.4649-8.5534,11.643-7.5119,6.1021,1.2273,9.7936,7.5478,8.5238,13.512-1.467,6.886-8.632,11.035-15.382,9.536-7.6693-1.704-12.277-9.715-10.547-17.25,1.9402-8.4536,10.798-13.52,19.119-11.56,9.2377,2.1763,14.763,11.882,12.571,20.988" /></g></svg>
</center>Now, we&#8217;re going to remove the transform property on the image without any changes to the image.

Open the image in Inkscape. Choose **XML Editor** from **Edit** menu. The SVG code will be opened in a dialog box :

[<img class="aligncenter size-full wp-image-2736" alt="0116" src="//lab.subinsb.com/projects/blog/uploads/2014/04/0116.png" width="646" height="418" />][1]

You can see the nodes of the SVG image in the tree. You can also see the transfrom property of the "g" tag on right side window when you click the node. Our goal is to remove this transfrom attribute of the image.

For this, click on the **<svg:path** inside the **#layer1** SVG tag. Click and drag it upwards and drop it above the parent ( **<svg:g id="layer1&#8243; **). Now the node tree will look like this :<figure id="attachment_2737" class="wp-caption aligncenter">

[<img class="size-full wp-image-2737" alt="Inkscape XML Editor" src="//lab.subinsb.com/projects/blog/uploads/2014/04/0117.png" width="647" height="419" />][2]<figcaption class="wp-caption-text">Inkscape XML Editor when path became the parent of g.</figcaption></figure> 

Since, the node which had the **transform** attribute was removed as the parent of the **path** tag, the **transform** styles won&#8217;t be applied to the **path** anymore. So, the image will be on a different position than before. Like this :

<center>
  <svg height="48px" width="48px" style="background:black;"><path style="stroke-linejoin:miter;stroke:#000;stroke-linecap:butt;stroke-width:1px;fill:#ffffff;" d="m15.571,7.2857c0.86668,0.25785,0.08001,1.2955-0.42857,1.4405-1.3782,0.39293-2.4292-1.0759-2.4524-2.2976-0.0415-2.1854,2.1429-3.6267,4.1667-3.4643,2.97,0.23833,4.8481,3.2202,4.4762,6.0357-0.495,3.753-4.3,6.079-7.904,5.488-4.5359-0.744-7.3153-5.3823-6.5004-9.7737,0.98692-5.3186,6.4649-8.5534,11.643-7.5119,6.1021,1.2273,9.7936,7.5478,8.5238,13.512-1.467,6.886-8.632,11.035-15.382,9.536-7.6693-1.704-12.277-9.715-10.547-17.25,1.9402-8.4536,10.798-13.52,19.119-11.56,9.2377,2.1763,14.763,11.882,12.571,20.988" id="path3081"/></svg>
</center>You can now align the image back to it&#8217;s original position using the mouse and save it. The image code won&#8217;t have any more 

**transfrom** attributes. Like this :

<center>
  <svg height="48px" width="48px" style="background:black;"><path style="fill:#ffffff;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter" d="m 21.882419,25.389506 c 0.86668,0.25785 0.08001,1.2955 -0.42857,1.4405 -1.3782,0.39293 -2.4292,-1.0759 -2.4524,-2.2976 -0.0415,-2.1854 2.1429,-3.6267 4.1667,-3.4643 2.97,0.23833 4.8481,3.2202 4.4762,6.0357 -0.495,3.753 -4.3,6.079 -7.904,5.488 -4.5359,-0.744 -7.3153,-5.3823 -6.5004,-9.7737 0.98692,-5.3186 6.4649,-8.5534 11.643,-7.5119 6.1021,1.2273 9.7936,7.5478 8.5238,13.512 -1.467,6.886 -8.632,11.035 -15.382,9.536 -7.6693,-1.704 -12.2770003,-9.715 -10.5470003,-17.25 1.9402,-8.4536 10.7980003,-13.5199998 19.1190003,-11.5599998 9.2377,2.1762998 14.763,11.8819998 12.571,20.9879998" id="path3081"/></svg>
</center>

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/04/0116.png
 [2]: //lab.subinsb.com/projects/blog/uploads/2014/04/0117.png