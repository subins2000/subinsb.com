---
title: Creating a custom 404 Not Found page in Apache Server.
author: Subin Siby
type: post
date: 2012-11-11T07:22:00+00:00
excerpt: "DEMOYou might have seen a 404&nbsp;page on different sites such as Google,Yahoo&nbsp;etc... You can make this on your website too if you are using Apache Server. The default 404&nbsp;page will be like this :It's just simple plain HTML&nbsp;404&nbsp;pag..."
url: /creating-a-custom-404-not-found-page-in-apache-server
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 450cfc8fa46ef89c658e797b03acc484
  - 450cfc8fa46ef89c658e797b03acc484
  - 450cfc8fa46ef89c658e797b03acc484
  - 450cfc8fa46ef89c658e797b03acc484
  - 450cfc8fa46ef89c658e797b03acc484
  - 450cfc8fa46ef89c658e797b03acc484
blogger_permalink:
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
  - http://sag-3.blogspot.com/2012/11/custom-404-page.html
dsq_thread_id:
  - 1960077041
  - 1960077041
  - 1960077041
  - 1960077041
  - 1960077041
  - 1960077041
categories:
  - HTML
  - Linux
  - Ubuntu
tags:
  - Apache
  - Google
  - Server
  - Yahoo

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <a href="http://subins.hp.af.cm/sdfvd" >DEMO</a><br />You might have seen a <b>404</b>&nbsp;page on different sites such as <b>Google</b>,<b>Yahoo</b>&nbsp;etc&#8230; You can make this on your website too if you are using <b>Apache Server</b>. The default <b>404</b>&nbsp;page will be like this :</p> 
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-8sSNV-hTWWw/UJ9OXA9xtrI/AAAAAAAACQ4/gedQz1-kwls/s1600/404.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="66" src="//1.bp.blogspot.com/-8sSNV-hTWWw/UJ9OXA9xtrI/AAAAAAAACQ4/gedQz1-kwls/s640/404.png" width="640" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    It&#8217;s just simple plain <b>HTML</b>&nbsp;<b>404</b>&nbsp;page and it&#8217;s ugly. To Change the default <b>404</b>&nbsp;page Do as below.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Go to your <b>site&#8217;s folder</b>.
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Create a <b>file</b>&nbsp;named<b>&nbsp;.htaccess</b>&nbsp;(just <b>.htaccess</b>&nbsp;no name only file extension).
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    Open the file and add the line.
  </div>
  
  <blockquote class="tr_bq">
    <p>
      ErrorDocument 404 <span style="color: red;">/pathtofile</span>
    </p>
  </blockquote>
  
  <p>
    <b>Replace&nbsp;</b><span style="color: red;">/pathtofile </span>with your&nbsp;<b>404</b>&nbsp;page&nbsp;file name which should be situated in the same folder as the file <b>.htaccess</b>&nbsp;. The custom <b>404</b>&nbsp;file can be in <b>PHP</b>&nbsp;or <b>HTML</b>.<br />You can also add <b>HTML </b>to it&nbsp;like this:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      ErrorDocument 404&nbsp;&#8216;<html><head></head><body><h1>404 Not Found-Subin&#8217;s Blog</h1></body></html>&#8217;
    </p>
  </blockquote>
  
  <p>
    You don&#8217;t have to reload Apache Server ! It&#8217;s finished. Test it out. Here&#8217;s an example of a custom <b>404</b>&nbsp;page.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-Vwo_1y6EDmI/UJ9RgZ5eOJI/AAAAAAAACRE/6-n5BzTx_rY/s1600/404-custom.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="312" src="https://2.bp.blogspot.com/-Vwo_1y6EDmI/UJ9RgZ5eOJI/AAAAAAAACRE/6-n5BzTx_rY/s640/404-custom.png" width="640" /></a>
  </div>
  
  <p>
    </div>