---
title: The best age calculation code in PHP
author: Subin Siby
type: post
date: 2013-05-02T14:36:00+00:00
excerpt: "Age calculation is a tricky calculation especially in Programming Languages. I'm going to tell you a simple way to find out age in PHP.&nbsp;This is a simple function that will calculate age if it is given a birthday date in the format date/month/year...."
url: /the-best-age-calculation-code-in-php
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
  - http://sag-3.blogspot.com/2013/05/the-best-age-calculation-code-in-php.html
dsq_thread_id:
  - 1968721304
  - 1968721304
  - 1968721304
  - 1968721304
  - 1968721304
  - 1968721304
syndication_item_hash:
  - c13222e81f9c7a8ef5b68b7be8c08780
  - c13222e81f9c7a8ef5b68b7be8c08780
  - c13222e81f9c7a8ef5b68b7be8c08780
  - c13222e81f9c7a8ef5b68b7be8c08780
  - c13222e81f9c7a8ef5b68b7be8c08780
  - c13222e81f9c7a8ef5b68b7be8c08780
categories:
  - PHP
tags:
  - Age
  - Birthdate
  - Date
  - echo
  - Function

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Age calculation is a tricky calculation especially in <b>Programming Languages</b>. I&#8217;m going to tell you a simple way to find out age in <b>PHP</b>.&nbsp;This is a simple function that will calculate age if it is given a birthday date in the format <b>date/month/year</b>. For example : <b>20/01/2000</b>&nbsp;or <b>04/12/1990</b><br />Here is the function :</p> 
  
  <blockquote class="tr_bq">
    <p>
      function age($birthday){<br />&nbsp;list($day,$month,$year) = explode("/",$birthday);<br />&nbsp;$year_diff &nbsp;= date("Y") &#8211; $year;<br />&nbsp;$month_diff = date("m") &#8211; $month;<br />&nbsp;$day_diff &nbsp; = date("d") &#8211; $day;<br />&nbsp;if ($day_diff < 0 && $month_diff==0){$year_diff&#8211;;}<br />&nbsp;if ($day_diff < 0 && $month_diff < 0){$year_diff&#8211;;}<br />&nbsp;return $year_diff;<br />}
    </p>
  </blockquote>
  
  <p>
    You can print out the age like this:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      echo age(&#8217;20/01/2000&#8242;);
    </p>
  </blockquote>
  
  <p>
    This echo function will write <b>13</b>. See another example :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      echo age(&#8217;04/12/1990&#8242;);
    </p>
  </blockquote>
  
  <p>
    This echo function will write&nbsp;<b>22</b>.</div>