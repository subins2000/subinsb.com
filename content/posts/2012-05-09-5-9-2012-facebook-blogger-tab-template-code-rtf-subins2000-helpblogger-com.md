---
title: 5.9.2012 facebook blogger tab template code.rtf (subins2000.help@blogger.com)
author: Subin Siby
type: post
date: 2012-05-09T22:18:00+00:00
draft: true
url: /?p=1428
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Uncategorized
tags:
  - Uncategorized

---
<div style="background-color: #d1e4f0; max-width: 650px; font-family: Arial, sans-serif; color: #000; padding: 5px;">
  <div style="height: 36px; font-size: 14px; font-weight: bold; padding-bottom: 4px;">
    <table style="display: inline;width: 100%;">
      <tr>
        <td width="32px" style="padding: 0;">
          <img src="https://ssl.gstatic.com/docs/documents/share/images/services/document_large-2.png" style="height: 32px; margin-right: 5px;" alt="Document" />
        </td>
        
        <td valign="middle" height="32px" style="padding: 0;">
          I&#8217;ve shared <a href='https://docs.google.com/document/d/1fCqog4I3Ms1dzSFDhfRnFS_pWIhCrUCLi3TH8iKs3XI/edit?invite=COCH7sUB'>5.9.2012 facebook blogger tab template code.rtf</a>
        </td>
      </tr>
    </table>
  </div>
  
  <div style="font-size: 13px; background-color: #FFF; padding: 10px 7px 7px 7px;">
    Click to open: </p> 
    
    <ul style="list-style-type: none; padding: 0; margin: 0;">
      <li style="margin: 0;">
        <a href="https://docs.google.com/document/d/1fCqog4I3Ms1dzSFDhfRnFS_pWIhCrUCLi3TH8iKs3XI/edit?invite=COCH7sUB">5.9.2012 facebook blogger tab template code.rtf</a>
      </li>
    </ul>
    
    <p>
      <span style="color: #898989;">Google Docs makes it easy to create, store and share online documents, spreadsheets and presentations.</span>
    </p>
    
    <div style="text-align: right;">
      <a href="https://docs.google.com"><img style="border: 0;margin-top: 10px;" src="https://ssl.gstatic.com/docs/documents/share/images/services/docs_logo-1.gif" alt="Logo for Google Docs" /></a>
    </div>
  </div>
</div>