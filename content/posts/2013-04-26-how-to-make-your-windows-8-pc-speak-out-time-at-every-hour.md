---
title: How to make your Windows 8 PC speak out Time at Every Hour
author: Subin Siby
type: post
date: 2013-04-26T17:33:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on"><div><span>Thanks for this useful information by <a href="http://www.nextofwindows.com/?source=subinsblog" target="_blank">NextOfWindows</a></span><br><span>This small tutorial will make your <b>PC</b>&nbsp;speak out the time every hour. This will help <b>especially if you're a programer</b>. <b>Because programmers usually lose track of time.</b></span></div><div><b><span><br></span></b></div><div><span>Most Programmers don't use a <b>Windows </b>OS. But there are some ! This tutorial is for those people.</span></div><div><span>Create a file named <b>time.vbs</b>&nbsp;containing these code :</span></div><div><blockquote><span>Dim speaks, speech</span><br><div><span><span>speaks = &ldquo;It is &rdquo; &amp; hour(time) &amp; &rdquo; O&rsquo;clock&rdquo;</span></span></div><span><span></span></span><br><div><span><span>Set speech = CreateObject(&ldquo;sapi.spvoice&rdquo;)</span></span></div><span><span></span></span><br><div><span><span>speech.Speak speaks</span></span></div><span></span></blockquote><span>Save the file wherever you want.</span><br><span>Open the <b>Run </b>dialog<b>&nbsp;</b>by pressing&nbsp;<b>WIN&nbsp;+ R</b>. Type <b>taskschd.msc</b>&nbsp;and hit <b>Enter</b>&nbsp;to run the program.</span><br><div></div><span><b>WIN =&nbsp;</b>Windows button on keyboard.</span><br><div><a href="//4.bp.blogspot.com/-jQKV82iZM4M/Ub1P7qOCdQI/AAAAAAAACsw/09G1Rhp9dpU/s1600/0003.png" imageanchor="1"><span><img border="0" src="//4.bp.blogspot.com/-jQKV82iZM4M/Ub1P7qOCdQI/AAAAAAAACsw/09G1Rhp9dpU/s1600/0003.png"></span></a></div><span><br></span><br><div><span><span>In <b>Task Scheduler</b>, click&nbsp;</span><strong>Create Task</strong><span>.</span></span></div><div><span>Give the name <b>Time</b>&nbsp;</span><span>under&nbsp;</span><strong>General</strong><span>&nbsp;tab</span><span>.</span><br><span>Run the task repeatedly every hour by adding a trigger. Like below:</span><br><div></div><div><a href="//1.bp.blogspot.com/-ZT7BBT-AXco/Ub1QdM6-S2I/AAAAAAAACtE/7lY7AOF9EMM/s1600/0004.png" imageanchor="1"><img border="0" height="553" src="//1.bp.blogspot.com/-ZT7BBT-AXco/Ub1QdM6-S2I/AAAAAAAACtE/7lY7AOF9EMM/s640/0004.png" width="640"></a></div><span><span><br></span></span><span><span>Point the location of the <b>time.vbs</b>&nbsp;file in <b>Program/Script</b>&nbsp;field, like below:</span></span><br><div><a href="//4.bp.blogspot.com/-Fgvwu8Hrvck/Ub1P9vDn2RI/AAAAAAAACtA/hiPv8jRDI6c/s1600/0005.png" imageanchor="1"><img border="0" src="//4.bp.blogspot.com/-Fgvwu8Hrvck/Ub1P9vDn2RI/AAAAAAAACtA/hiPv8jRDI6c/s1600/0005.png"></a></div><span><br></span><span>Change the program input to the path where you saved </span><b>time.vbs</b><span>.Click </span><b>OK</b><span>&nbsp;button.</span><br><span>That's it for today. I will post something more cool next ti</span></div></div><div><div><span></span></div></div></div>
url: /how-to-make-your-windows-8-pc-speak-out-time-at-every-hour
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 5d7be0cb65effb509081430d16adb8ec
  - 5d7be0cb65effb509081430d16adb8ec
  - 5d7be0cb65effb509081430d16adb8ec
  - 5d7be0cb65effb509081430d16adb8ec
  - 5d7be0cb65effb509081430d16adb8ec
  - 5d7be0cb65effb509081430d16adb8ec
blogger_permalink:
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
  - http://sag-3.blogspot.com/2013/04/how-to-make-your-windows-8-pc-speak-out.html
dsq_thread_id:
  - 1974101095
  - 1974101095
  - 1974101095
  - 1974101095
  - 1974101095
  - 1974101095
categories:
  - Windows
tags:
  - Scheduler
  - VBS

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div style="text-align: left;">
    <span style="background-color: white; font-family: inherit;">Thanks for this useful information by <a href="http://www.nextofwindows.com/?source=subinsblog" >NextOfWindows</a></span><br /><span style="background-color: white; font-family: inherit;">This small tutorial will make your <b>PC</b>&nbsp;speak out the time every hour. This will help <b>especially if you&#8217;re a programer</b>. <b>Because programmers usually lose track of time.</b></span>
  </div>
  
  <div style="text-align: left;">
    <b><span style="background-color: white; font-family: inherit;"><br /></span></b>
  </div>
  
  <div style="text-align: left;">
    <span style="background-color: white; font-family: inherit;">Most Programmers don&#8217;t use a <b>Windows </b>OS. But there are some ! This tutorial is for those people.</span>
  </div>
  
  <div style="text-align: left;">
    <span style="background-color: white; font-family: inherit;">Create a file named <b>time.vbs</b>&nbsp;containing these code :</span>
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        <span style="background-color: white; font-family: inherit;">Dim speaks, speech</span>
      </p>
      
      <div style="text-align: left;">
        <span style="background-color: white;"><span style="font-family: inherit; line-height: 25px; text-align: center;">speaks = "It is " & hour(time) & " O’clock"</span></span>
      </div>
      
      <p>
        <span style="font-family: inherit;"><span style="background-color: white;"></span></span>
      </p>
      
      <div style="text-align: left;">
        <span style="background-color: white;"><span style="font-family: inherit; line-height: 25px; text-align: center;">Set speech = CreateObject("sapi.spvoice")</span></span>
      </div>
      
      <p>
        <span style="font-family: inherit;"><span style="background-color: white;"></span></span>
      </p>
      
      <div style="text-align: left;">
        <span style="background-color: white;"><span style="font-family: inherit; line-height: 25px; text-align: center;">speech.Speak speaks</span></span>
      </div>
      
      <p>
        <span style="background-color: white; font-family: inherit;"></span>
      </p>
    </blockquote>
    
    <p>
      <span style="background-color: white; font-family: inherit;">Save the file wherever you want.</span><br /><span style="background-color: white; font-family: inherit;">Open the <b>Run </b>dialog<b>&nbsp;</b>by pressing&nbsp;<b>WIN&nbsp;+ R</b>. Type <b>taskschd.msc</b>&nbsp;and hit <b>Enter</b>&nbsp;to run the program.</span>
    </p>
    
    <div style="text-align: center;">
    </div>
    
    <p>
      <span style="background-color: white; font-family: inherit;"><b>WIN =&nbsp;</b>Windows button on keyboard.</span>
    </p>
    
    <div class="separator" style="clear: both; text-align: center;">
      <a href="//4.bp.blogspot.com/-jQKV82iZM4M/Ub1P7qOCdQI/AAAAAAAACsw/09G1Rhp9dpU/s1600/0003.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><span style="font-family: inherit;"><img border="0" src="//4.bp.blogspot.com/-jQKV82iZM4M/Ub1P7qOCdQI/AAAAAAAACsw/09G1Rhp9dpU/s1600/0003.png" /></span></a>
    </div>
    
    <p>
      <span style="background-color: white; font-family: inherit;"><br /></span>
    </p>
    
    <div style="text-align: left;">
      <span style="background-color: white; font-family: inherit;"><span style="line-height: 22px; text-align: center;">In <b>Task Scheduler</b>, click&nbsp;</span><strong style="border: 0px; font-style: inherit; line-height: 22px; margin: 0px; outline: 0px; padding: 0px; text-align: center; vertical-align: baseline;">Create Task</strong><span style="line-height: 22px; text-align: center;">.</span></span>
    </div>
    
    <div style="border: 0px; line-height: 22px; margin-bottom: 20px; outline: 0px; padding: 0px; text-align: left; vertical-align: baseline;">
      <span style="background-color: white; font-family: inherit; margin: 0px; padding: 0px;">Give the name <b>Time</b>&nbsp;</span><span style="background-color: white;">under&nbsp;</span><strong style="border: 0px; font-style: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;">General</strong><span style="background-color: white;">&nbsp;tab</span><span style="background-color: white; font-family: inherit;">.</span><br /><span style="font-family: inherit;">Run the task repeatedly every hour by adding a trigger. Like below:</span></p> 
      
      <div class="separator" style="clear: both; text-align: center;">
      </div>
      
      <div class="separator" style="clear: both; text-align: center;">
        <a href="//1.bp.blogspot.com/-ZT7BBT-AXco/Ub1QdM6-S2I/AAAAAAAACtE/7lY7AOF9EMM/s1600/0004.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="553" src="//1.bp.blogspot.com/-ZT7BBT-AXco/Ub1QdM6-S2I/AAAAAAAACtE/7lY7AOF9EMM/s640/0004.png" width="640" /></a>
      </div>
      
      <p>
        <span style="background-color: white;"><span style="font-family: inherit;"><br /></span></span><span style="font-family: inherit;"><span style="background-color: white;">Point the location of the <b>time.vbs</b>&nbsp;file in <b>Program/Script</b>&nbsp;field, like below:</span></span>
      </p>
      
      <div class="separator" style="clear: both; text-align: center;">
        <a href="//4.bp.blogspot.com/-Fgvwu8Hrvck/Ub1P9vDn2RI/AAAAAAAACtA/hiPv8jRDI6c/s1600/0005.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-Fgvwu8Hrvck/Ub1P9vDn2RI/AAAAAAAACtA/hiPv8jRDI6c/s1600/0005.png" /></a>
      </div>
      
      <p>
        <span style="font-family: inherit;"><br /></span><span style="font-family: inherit;">Change the program input to the path where you saved </span><b style="font-family: inherit;">time.vbs</b><span style="font-family: inherit;">.Click </span><b style="font-family: inherit;">OK</b><span style="font-family: inherit;">&nbsp;button.</span><br /><span style="font-family: inherit;">That&#8217;s it for today. I will post something more cool next ti</span></div> </div> 
        
        <div style="border: 0px; line-height: 22px; margin-bottom: 20px; outline: 0px; padding: 0px; text-align: left; vertical-align: baseline;">
          <div style="background-color: white; border: 0px; color: #373737; font-family: 'Segoe UI', Segoe, 'Helvetica Neue', Verdana, Geneva, Arial; font-size: 1.1em; margin-bottom: 20px; outline: 0px; padding: 0px; text-align: left; vertical-align: baseline;">
            <span style="margin: 0px; padding: 0px;"></span>
          </div>
        </div></div>