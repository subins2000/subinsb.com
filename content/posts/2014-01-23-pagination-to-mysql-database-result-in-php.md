---
title: Add Pagination To MySQL Database Result In PHP
author: Subin Siby
type: post
date: 2014-01-23T18:14:07+00:00
url: /pagination-to-mysql-database-result-in-php
authorsure_include_css:
  - 
  - 
  - 
categories:
  - CSS
  - HTML
  - MySQL
  - PHP
  - SQL
tags:
  - Tutorials

---
Pagination makes the results tidy and simple. When there are many results, pagination helps sorting them out and adding each results to specific pages. In **PHP**, it&#8217;s real simple. You can use this tutorial to add a pagination to your database results in PHP.

<div class="padlinks">
  <a class="demo" href="http://open.subinsb.com/find">Demo</a>
</div>

We will be using PDO for database connection and queries. The Database table named "users" is like this :

<table class="table">
  <tr>
    <td>
      id
    </td>
    
    <td>
      First_Name
    </td>
    
    <td>
      Last_Name
    </td>
  </tr>
  
  <tr>
    <td>
      1
    </td>
    
    <td>
      Subin
    </td>
    
    <td>
      Siby
    </td>
  </tr>
  
  <tr>
    <td>
      2
    </td>
    
    <td>
      Peter
    </td>
    
    <td>
      Charles
    </td>
  </tr>
  
  <tr>
    <td>
      3
    </td>
    
    <td>
      Thomas
    </td>
    
    <td>
      Chacko
    </td>
  </tr>
</table>

It contains more and more data. It&#8217;s just a sample. Let&#8217;s move on.

## Variables

We will set the result limit in a page using the **$limit** variable :

<pre class="prettyprint"><code>$limit = 10;</code></pre>

The **$p** variable have the page number got from $_GET :

<pre class="prettyprint"><code>$p=$_GET['p']=="" ? 1:$_GET['p'];</code></pre>

Note that the above variable will have the value **1** even if the $_GET[&#8216;p&#8217;] is empty or not defined.

The **$start** variable calculates the starting number we&#8217;re going to add in the **SQL** queries **LIMIT** clause :

<pre class="prettyprint"><code>$start=($p-1)*$limit;</code></pre>

The page number is subtracted by **1** and is multiplied by the **$limit** number which will return the start number used in the **LIMIT caluse**.

## Get Data

We will first get the data from database into the **$data** variable :

<pre class="prettyprint"><code>$sql=$dbh-&gt;prepare("SELECT * FROM users ORDER BY id LIMIT :start, :limit");
$sql-&gt;bindValue(':limit', $limit, PDO::PARAM_INT);
$sql-&gt;bindValue(':start', $start, PDO::PARAM_INT);
$sql-&gt;execute();
$data=$sql-&gt;fetch();
</code></pre>

You can output the data you got from SQL query as you like :

<pre class="prettyprint"><code>if($sql-&gt;rowCount()!=0){
 while($r=$data){
  echo $r['First_Name']." ".$r['Last_Name'];
 }
}else{
 echo "No Users Found";
}
</code></pre>

We add the row checking, because if the page number mentioned is not correct SQL query will return nothing.

## Pagination

We will display the pages now and CSS styling to it. The following PHP code will get the actual Total row numbers as **$count** variable :

<pre class="prettyprint"><code>$count=$db-&gt;prepare("SELECT COUNT(id) FROM users ORDER BY id");
$count-&gt;execute();
$count=$count-&gt;fetchColumn();</code></pre>

Now, we will generate the pages list in a horizontal scrollable element :

<pre class="prettyprint"><code>$countP=(ceil($count/$limit)) + 1;
$tW=($countP*50) + $countP;
echo"&lt;center style='overflow-x:auto;margin-top:10px;padding-bottom:10px;'&gt;";
 echo"&lt;div style='width:".$tW."px'&gt;";
 for($i=1;$i&lt;$countP;$i++){
  $isC=$i==$_GET['p'] ? "b-green":"";
  echo "&lt;a href='?p=$i'&gt;&lt;button class='pgbutton $isC'&gt;$i&lt;/button&gt;&lt;/a&gt;";
 }
 echo"&lt;/div&gt;";
echo"&lt;/center&gt;";</code></pre>

The styling of the page buttons is :

<pre class="prettyprint"><code>&lt;style&gt;
.pgbutton{
 width:45px;
 margin:0px 5px;
}
&lt;/style&gt;
</code></pre>

If you want to display the no of results, you can use :

<pre class="prettyprint"><code>echo"$count Results Found.";</code></pre>

You can make changes to the code as you like. When you change the SQL queries, be sure that you have changed the SQL queries of both occurences, one with **LIMIT clause** and one without.