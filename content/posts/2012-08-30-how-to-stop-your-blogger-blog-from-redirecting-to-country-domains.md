---
title: How to Stop your Blogger Blog from Redirecting to Country Domains
author: Subin Siby
type: post
date: 2012-08-30T05:14:00+00:00
excerpt: '<div dir="ltr" trbidi="on">When you open your blog you will be redirected to the country based domain of your blog.<br>This will affect your blog badly. For Example:<br><ol><li><span>The social stats &ndash; or Facebook Likes, Google +1s and Tweet counts &ndash; for your blog stories may be diluted as the URLs for the same story become different from different visitors.</span></li><li><span>You will have a similar problem if you are using an external commenting platform like Disqus or Facebook Comments.</span></li></ol>To prevent this you need to add this code to your <b>Template</b>.<br>Go to <b>Blogger -&gt; Template -&gt; EDIT HTML</b><br><br>Paste this code below <b>&lt;head&gt; </b>tag<br><blockquote><span>&lt;script type="text/javascript"&gt;</span><span>var blog = document.location.hostname;</span><span>var slug = document.location.pathname;</span><span>var ctld = blog.substr(blog.lastIndexOf("."));</span><span>if (ctld != ".com") {</span><span>var ncr = "http://" + blog.substr(0, blog.indexOf("."));</span><span>ncr += ".blogspot.com/ncr" + slug;</span><span>window.location.replace(ncr);</span><span>}</span><span>&lt;/script&gt;</span></blockquote><br></div>'
url: /how-to-stop-your-blogger-blog-from-redirecting-to-country-domains
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 9017bc1f4299ae23aa6a782c86f272a6
  - 9017bc1f4299ae23aa6a782c86f272a6
  - 9017bc1f4299ae23aa6a782c86f272a6
  - 9017bc1f4299ae23aa6a782c86f272a6
  - 9017bc1f4299ae23aa6a782c86f272a6
  - 9017bc1f4299ae23aa6a782c86f272a6
syndication_permalink:
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
  - http://sag-3.blogspot.com/2012/08/how-to-prevent-your-blogger-blog-from.html
categories:
  - Blogger
  - HTML
  - JavaScript

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  When you open your blog you will be redirected to the country based domain of your blog.<br />This will affect your blog badly. For Example:</p> 
  
  <ol style="background-color: white; color: #222222; line-height: 19px; margin: 1em 0px 1.4em 24px; padding: 0px; text-align: -webkit-auto;">
    <li style="margin: 0px 0px 0.5em; padding: 0px;">
      <span style="font-family: inherit;">The social stats – or Facebook Likes, Google +1s and Tweet counts – for your blog stories may be diluted as the URLs for the same story become different from different visitors.</span>
    </li>
    <li style="margin: 0px 0px 0.5em; padding: 0px;">
      <span style="font-family: inherit;">You will have a similar problem if you are using an external commenting platform like Disqus or Facebook Comments.</span>
    </li>
  </ol>
  
  <p>
    To prevent this you need to add this code to your <b>Template</b>.<br />Go to <b>Blogger -> Template -> EDIT HTML</b>
  </p>
  
  <p>
    Paste this code below <b><head> </b>tag
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <span style="background-color: white;"><script type="text/javascript"></span><span style="background-color: white;">var blog = document.location.hostname;</span><span style="background-color: white;">var slug = document.location.pathname;</span><span style="background-color: white;">var ctld = blog.substr(blog.lastIndexOf("."));</span><span style="background-color: white;">if (ctld != ".com") {</span><span style="background-color: white;">var ncr = "http://" + blog.substr(0, blog.indexOf("."));</span><span style="background-color: white;">ncr += ".blogspot.com/ncr" + slug;</span><span style="background-color: white;">window.location.replace(ncr);</span><span style="background-color: white;">}</span><span style="background-color: white;"></script></span>
    </p>
  </blockquote>
  
  <p>
    </div>