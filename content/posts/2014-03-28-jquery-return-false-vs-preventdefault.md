---
title: ‘return false’ vs ‘e.preventDefault()’ In jQuery
author: Subin Siby
type: post
date: 2014-03-28T06:23:08+00:00
url: /jquery-return-false-vs-preventdefault
authorsure_include_css:
  - 
  - 
  - 
categories:
  - JavaScript
  - jQuery
tags:
  - Function

---
I decided to add some improvements to Open and fix some bugs. While I was adding two **submit** listeners on a form, it didn&#8217;t work. I have tried everything but still the problem came up. So, I removed **return false;** and used **e.preventDefault();**. It worked !

So, why didn&#8217;t the **submit** listener called on the element ? The problem was **return false** will do **e.stopPropagation() **and **e.preventDefault()**. If **e.stopPropagation()** is ran, then any other submit event handlers of the element won&#8217;t call. Here&#8217;s the 2 submit calls :

<pre class="prettyprint"><code>$(".postForm").on("submit", function(){
 // do something .....
 return false;
});
$(".postForm").on("submit", function(){
 // do something other than the first one
});</code></pre>

When you submit the form, the 1st function will run successfully, but the 2nd one won&#8217;t run, because **return false;** stops the calling of second function. If you rewrite the code by using **e.preventDefault()** like this :

<pre class="prettyprint"><code>$(".postForm").on("submit", function(e){
 e.preventDefault();
 // do something .....
});
$(".postForm").on("submit", function(){
 // do something other than the first one
});</code></pre>

&nbsp;

It will call both functions without any problem. Now you know when to use **return false;** and **e.preventDefault();**. A short summary :

<pre class="prettyprint"><code>return false; - calls e.preventDefault(); and e.stopPropagation();
e.preventDefault(); - e.preventDefault(); (Prevents the default action)</code></pre>

&nbsp;

## External Resources

You can see <a href="http://www.mail-archive.com/jquery-en@googlegroups.com/msg71371.html" target="_blank">the post</a> where jQuery creator John Resig tells about what return false does.

<a href="http://api.jquery.com/category/events/event-object/" target="_blank">jQuery Event Object</a>