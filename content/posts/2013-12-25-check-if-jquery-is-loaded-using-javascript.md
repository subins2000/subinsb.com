---
title: Check If jQuery is Loaded Using typeof Operator in JavaScript
author: Subin Siby
type: post
date: 2013-12-25T05:45:25+00:00
url: /check-if-jquery-is-loaded-using-javascript
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - JavaScript
  - jQuery

---
**jQuery** is the most used and popular library of **JavaScript**. If **jQuery** isn&#8217;t loaded, a **jQuery** coded site will not function properly. Such sites will reload automatically or give an error when **jQuery** isn&#8217;t loaded. But, how do they check if **jQuery** is loaded ? You will find a simple solution to accomplish this task in this post. Since, **jQuery** won&#8217;t be loaded, we can&#8217;t check if **jQuery** is loaded using **jQuery**. But we can use **JavaScript** which will always be available in browsers.

In **JavaScript**, there is an operator called **typeof**, which will return the type of a variable. We are going to pass the **jQuery** global function which is **$** to **typeof**, if it returns the string "function", then **jQuery** is loaded. Here is a simple example :

> if(typeof($)=="function"){
> 
> // jQuery is loaded
> 
> }else{
> 
> // jQuery isn&#8217;t loaded
> 
> }

The above code isn&#8217;t perfect, because there are other libraries which has the global function **$**. So, we have to check if **jQuery** is a function too :

> if(typeof($)=="function" && typeof(jQuery)=="function"){
> 
> // jQuery is loaded
> 
> }else{
> 
> // jQuery isn&#8217;t loaded
> 
> }

The above code is perfect if your site don&#8217;t have a custom function named **jQuery**.