---
title: Parsing sMention Form Data On Server In PHP
author: Subin Siby
type: post
date: 2014-01-09T17:24:30+00:00
url: /parsing-smention-form-data-in-php
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - HTML
  - jQuery
  - PHP
tags:
  - sMention

---
In my previous post, I introduced the **sMention** **jQuery** plugin &#8211; a plugin that adds the **@mentions** to a input element. But I haven&#8217;t told in the post how to handle the data on the server side after the form has been submitted. This handling replaces the **@mentions** to the user&#8217;s name in **a[href]** tag. It&#8217;s made possible using **smention** function bind as the callback function of **preg\_replace\_callback**. This smention function can be seen on the file <a href="https://github.com/subins2000/open/blob/master/comps/config.php" target="_blank"><strong>comps/config.php</strong></a> in the <a href="https://github.com/subins2000/open" target="_blank">Open Repository</a>.

<div class="padlinks">
  <a class="download" href="https://github.com/subins2000/smention/archive/master.zip" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/jquery/smention" target="_blank">Demo</a>
</div>

The form&#8217;s data is searched for the **@** character. The callback function is smention. The following line will set the callback :

> $data=preg\_replace\_callback("/@(.*?)(s|z)/", function($match) use ($data){return smention($match,data);},$data);

where $data is the input data. An Example input data :

> @1 Google is the best search engine ever.

The above data when parsed using **smention** function will return the data as (in my case [Open]) :

> <a href="http://open.subinsb.com/1&#8243;>Subin Siby</a> Google is the best search engine ever.

Here&#8217;s the code I used on **Open **(<a href="http://open.subinsb.com" target="_blank">http://open.subinsb.com</a>) :

> function&nbsp;smention($s,$t){
  
> &nbsp;$userid=$t[1];
  
> &nbsp;$nxs=strpos($s,"@$userid");
  
> &nbsp;$nxs=strlen("@$userid")+$nxs;
  
> &nbsp;$nxs=substr($s,$nxs,1);
  
> &nbsp;global$db;
  
> &nbsp;$sql=$db->prepare("SELECT&nbsp;name&nbsp;FROM&nbsp;users&nbsp;WHERE&nbsp;id=?");
  
> &nbsp;$sql->execute(array($userid));
  
> &nbsp;if($sql->rowCount()==0){
  
> &nbsp;&nbsp;return"@$userid".$nxs;
  
> &nbsp;}else{
  
> &nbsp;&nbsp;while($r=$sql->fetch()){
  
> &nbsp;&nbsp;&nbsp;$name=$r[&#8216;name&#8217;];
  
> &nbsp;&nbsp;}
  
> &nbsp;&nbsp;$html="<a&nbsp;href=&#8217;//open.com/$userid&#8217;>@$name</a>".$nxs;
  
> &nbsp;&nbsp;return$html;
  
> &nbsp;}
  
> }
  
> $data=$_POST[&#8216;data&#8217;];
  
> $data=preg\_replace\_callback("/@(.*?)(s|z|[^0-9])/",&nbsp;function($t)&nbsp;use&nbsp;($s){return&nbsp;smention($s,$t);},$s);

You can see the whole smention function at <a href="https://github.com/subins2000/open/blob/master/comps/config.php" target="_blank">https://github.com/subins2000/open/blob/master/comps/config.php</a>