---
title: How to Empty Trash of root user in Ubuntu
author: Subin Siby
type: post
date: 2012-07-27T14:03:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on"><a href="http://www.blogger.com/blogger.g?blogID=1163006449268681643#fbinvites" rel="nofollow" title="Invite Your Facebook Friends"></a><br><div></div><br><div dir="ltr" trbidi="on">Install&nbsp;<a href="http://apt.ubuntu.com/p/nautilus-gksu" target="_blank">nautilus-gksu</a><br><span><b><u>About Nautilus-gksu</u></b></span><br><br>Privilege granting extension for nautilus using gksu<br>The gksu extension for nautilus allows you to open files with administration privileges using the context menu when browsing your files with nautilus.<br><br><b>Reboot computer after installing nautilus-gksu&nbsp;</b><br>Open your home folder.<br>Right click on any folder and click <b>Open as administrator</b>.<br><a href="//1.bp.blogspot.com/-3hnvIkeW8VM/UAwgzB3SMgI/AAAAAAAABF0/oVtinZPEc78/s1600/lpane.png" imageanchor="1"><img border="0" src="//1.bp.blogspot.com/-3hnvIkeW8VM/UAwgzB3SMgI/AAAAAAAABF0/oVtinZPEc78/s1600/lpane.png"></a>You will enter root user.<br>&nbsp;On the left pane you will see root home.<br><br>Go to <b>/root/.local/share/Trash/files</b><br>Delete the files you don't want.<br>All Done !!<br>Close the window and your root trash is emptied.<br><br><br></div></div>
url: /how-to-empty-trash-of-root-user-in-ubuntu
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 
  - 
  - 
  - 
  - 
  - 
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
syndication_item_hash:
  - 2de5ea4f5fedbad63033078e78645108
  - 2de5ea4f5fedbad63033078e78645108
  - 2de5ea4f5fedbad63033078e78645108
  - 2de5ea4f5fedbad63033078e78645108
  - 2de5ea4f5fedbad63033078e78645108
  - 2de5ea4f5fedbad63033078e78645108
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
  - http://sag-3.blogspot.com/2012/07/empty-root-trash.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
categories:
  - Ubuntu
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;">
  <div dir="ltr" style="text-align: left;">
    Install <a href="http://apt.ubuntu.com/p/nautilus-gksu">nautilus-gksu</a><br /> <span style="font-size: large;"><b><span style="text-decoration: underline;">About Nautilus-gksu</span></b></span>
  </div>
  
  <p>
    Privilege granting extension for nautilus using gksu<br /> The gksu extension for nautilus allows you to open files with administration privileges using the context menu when browsing your files with nautilus.
  </p>
  
  <p>
    <b>Reboot computer after installing nautilus-gksu </b><br /> Open your home folder.<br /> Right click on any folder and click <b>Open as administrator</b>.<br /> <a style="clear: left; float: left; margin-bottom: 1em; margin-right: 1em;" href="//1.bp.blogspot.com/-3hnvIkeW8VM/UAwgzB3SMgI/AAAAAAAABF0/oVtinZPEc78/s1600/lpane.png"><img alt="" src="//1.bp.blogspot.com/-3hnvIkeW8VM/UAwgzB3SMgI/AAAAAAAABF0/oVtinZPEc78/s1600/lpane.png" border="0" /></a>You will enter root user.<br /> On the left pane you will see root home.
  </p>
  
  <p>
    Go to <b>/root/.local/share/Trash/files</b><br /> Delete the files you don&#8217;t want.<br /> All Done !!<br /> Close the window and your root trash is emptied.
  </p>
  
  <p>
    &nbsp;
  </p>
</div>