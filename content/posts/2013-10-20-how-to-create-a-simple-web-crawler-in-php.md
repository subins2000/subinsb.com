---
title: How To Create A Simple Web Crawler in PHP
author: Subin Siby
type: post
date: 2013-10-20T07:08:00+00:00
excerpt: "A Web Crawler&nbsp;is a program that crawls through the sites in the Web&nbsp;and indexes those URL's. Search Engines&nbsp;uses a crawler&nbsp;to index URL's on the Web. Google uses a crawler written in Python. There are some other search engines that ..."
url: /how-to-create-a-simple-web-crawler-in-php
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
  - http://sag-3.blogspot.com/2013/10/simple-web-crawler-in-php.html
dsq_thread_id:
  - 1878063579
  - 1878063579
  - 1878063579
  - 1878063579
  - 1878063579
  - 1878063579
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
syndication_item_hash:
  - 123ad3ec33ab27c7041690656ef5d66c
  - 123ad3ec33ab27c7041690656ef5d66c
  - 123ad3ec33ab27c7041690656ef5d66c
  - 123ad3ec33ab27c7041690656ef5d66c
  - 123ad3ec33ab27c7041690656ef5d66c
  - 123ad3ec33ab27c7041690656ef5d66c
categories:
  - PHP
tags:
  - Crawler
  - Demo
  - Download
  - Google
  - Search
  - Search Engines
  - Simple
  - Tutorials
  - URL
  - WEB

---
A **Web Crawler** is a program that crawls through the sites in the Web and indexes those **URL**&#8216;s. **Search Engines** uses a crawler to index URL&#8217;s on the Web. Google uses a crawler written in **Python**. There are other search engines that uses different types of crawlers.
  
In this post I&#8217;m going to tell you how to create a simple Web Crawler in **PHP**.

The codes shown here was created by me. It took me 2 days to create a simple crawler. Then How much time would it take to create a perfect crawler ? Creating a Crawler is a very hard task. It&#8217;s like creating a Robot. Let&#8217;s start building a crawler.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=r8zy7w2bdzegmo5&class=12">Download</a> <a class="demo" href="http://demos.subinsb.com/php-web-crawler">Demo</a>
</div>

For parsing the web page of a URL, we are going to use **Simple HTML Dom** class which can be downloaded at **[Sourceforge][1]**. Include the file "simple\_html\_dom.php" and mention the variables we are going to use :

<pre class="prettyprint"><code>include "simple_html_dom.php";
$crawled_urls = array();
$found_urls = array();</code></pre>

Then, Add the functions we are going to use. The following function will convert **relative** **URL**&#8216;s to **absolute URL**&#8216;s :

<pre class="prettyprint"><code>function rel2abs($rel,   $base) {
    if (parse_url($rel,  PHP_URL_SCHEME)  !=  '') {
        return  $rel;
    }
    if ($rel[0] == '#'  ||   $rel[0] == '?') {
        return  $base . $rel;
    }
    extract(parse_url($base));
    $path  =  preg_replace('#/[^/]*$#',  '',   $path);
    if ($rel[0]  ==  '/') {
        $path  =  '';
    }
    $abs  =  "$host$path/$rel";
    $re   =  array('#(/.?/)#',  '#/(?!..)[^/]+/../#');
    for ($n = 1;   $n & gt; 0; $abs = preg_replace($re, '/',   $abs, -1, $n)) {}
    $abs = str_replace('../', '', $abs);
    return  $scheme . '://' . $abs;
}</code></pre>

The following function will change the **URL**&#8216;s found when crawling to real **URL**&#8216;s :

<pre class="prettyprint"><code>function perfect_url($u, $b) {
    $bp = parse_url($b);
    if (($bp['path'] != '/'  & amp; & amp;   $bp['path'] != '')  ||   $bp['path'] == '') {
        if ($bp['scheme'] == '') {
            $scheme = 'http';
        } else {
            $scheme = $bp['scheme'];
        }
        $b = $scheme . '://' . $bp['host'] . '/';
    }
    if (substr($u, 0, 2) == '//') {
        $u = 'http:' . $u;
    }
    if (substr($u, 0, 4) != 'http') {
        $u = rel2abs($u, $b);
    }
    return  $u;
}</code></pre>

This code is the **core** of the crawler :

<pre class="prettyprint"><code>function crawl_site($u) {
    global  $crawled_urls,   $found_urls;
    $uen = urlencode($u);
    if ((array_key_exists($uen, $crawled_urls) == 0  ||   $crawled_urls[$uen]  & lt;  date('YmdHis', strtotime('-25 seconds',  time())))) {
        $html              =  file_get_html($u);
        $crawled_urls[$uen] = date('YmdHis');
        foreach ($html- & gt; find('a') as  $li) {
            $url   = perfect_url($li- & gt; href, $u);
            $enurl = urlencode($url);
            if ($url != ''  & amp; & amp;  substr($url, 0, 4) != 'mail'  & amp; & amp;  substr($url, 0, 4) != 'java'  & amp; & amp;  array_key_exists($enurl, $found_urls) == 0) {
                $found_urls[$enurl] = 1;
                echo  $url . PHP_EOL;
            }
        }
    }
}</code></pre>

Finally, we will call the **crawl_site** function to **crawl** a **URL**. I&#8217;m going to use **http://subinsb.com** for **crawling**.

<pre class="prettyprint"><code>crawl_site("http://subinsb.com");</code></pre>

When you run the PHP crawler now, you will get all the URL&#8217;s in the page. You can again crawl those founded URL&#8217;s to find more URL&#8217;s, but you would need a fast Server and a High Speed Internet Connection.

A Super Computer and an Internet Connection of 10 GB/Second would be perfect for that. If you think that your computer is fast and can crawl many URL&#8217;s, then change the following line in the code :

<pre class="prettyprint"><code>echo  $url . PHP_EOL;
</code></pre>

to :

<pre class="prettyprint"><code>crawl_site($url);</code></pre>

Note :- The code isn&#8217;t perfect, there may be errors when crawling some URL&#8217;s. I don&#8217;t recommend you to crawl the URL&#8217;s found again unless you have a Super Computer and a High Speed Internet Connection. Feel free to make the crawler better, awesome and fast @ [GitHub][2].

If you have any problems / suggestions / feedback, echo it in the comments. Your Feedback is my happiness.

 [1]: http://sourceforge.net/project/showfiles.php?group_id=218559
 [2]: https://github.com/subins2000/phpwebcrawler