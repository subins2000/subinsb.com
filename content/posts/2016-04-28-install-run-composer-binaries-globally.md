---
title: 'How To Install & Run Composer Binaries Globally'
author: Subin Siby
type: post
date: 2016-04-28T15:19:18+00:00
url: /install-run-composer-binaries-globally
categories:
  - Composer
  - Linux
  - Operating System
  - PHP
  - Program
  - Short Post
  - Unix
tags:
  - BASH
  - Terminal

---
Take **PHPUnit** as an example. You can install it using Composer doing this :

<pre class="prettyprint"><code>composer require phpunit/phpunit</code></pre>

This will install a **phpunit** shortcut in **vendor/bin** directory. This obviously means you can run it. But, you would have to point to the exact location of the **phpunit** file to run : 

<pre class="prettyprint"><code>/home/username/mysite/vendor/bin/phpunit phpunit.xml</code></pre>

But, that&#8217;s long and uncomfortable. And you would have to install **PHPUnit** separately for each projects.

I&#8217;m gonna tell you how to install binaries globally as well as running it simply.

## Install Packages Globally

Same command, but add a "global" before "require" : 

<pre class="prettyprint"><code>composer global require phpunit/phpunit</code></pre>

If you&#8217;re using Linux, this will install **PHPUnit** to :

<pre class="prettyprint"><code>/home/username/.config/composer/vendor&lt;br /></code></pre>

This will also create the **phpunit** binary shortcut in **vendor/bin** directory.

## Use It Simply

Now, let&#8217;s add the location so that we can use it like this in terminal :

<pre class="prettyprint"><code>phpunit phpunit.xml&lt;br /></code></pre>

For this, you simply enter this into your terminal session :

<pre class="prettyprint"><code>export PATH=$PATH:~/.config/composer/vendor/bin</code></pre>

But this (^) is only temporary and will fade away when the terminal is closed.

To make it permanent, open any of the following files using your text editor :

<pre class="prettyprint"><code>/home/username/.bashrc&lt;br />/home/username/.bash_profile</code></pre>

Then, add the command we used before at the end of the file :

<pre class="prettyprint"><code># http://subinsb.com/install-run-composer-binaries-globally&lt;br />export PATH=$PATH:~/.config/composer/vendor/bin&lt;br /></code></pre>

I added the link to this post, because in the future if you open this file, you shouldn&#8217;t confuse yourself. You should close all running terminals and re open before you test it out.

Now, you can use any binary shortcuts in "/home/username/.config/composer/vendor/bin" by simply typing it on your terminal. Also, make sure the **Linux** native programs doesn&#8217;t conflict with the file names of **Composer** binaries.