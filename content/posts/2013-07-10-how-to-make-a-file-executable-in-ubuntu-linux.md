---
title: How to make a file executable in Ubuntu Linux
author: Subin Siby
type: post
date: 2013-07-10T03:30:00+00:00
excerpt: 'Executable files are files that can be used to run a program or to do a specific purpose specified in the file. Normally not all files defaultly would be executable in Linux&nbsp;because of security reasons. If all files are defaultly executable then t...'
url: /how-to-make-a-file-executable-in-ubuntu-linux
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
  - http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html
syndication_item_hash:
  - 1994aebbe6ab65afda414fd68aedd617
  - 1994aebbe6ab65afda414fd68aedd617
  - 1994aebbe6ab65afda414fd68aedd617
  - 1994aebbe6ab65afda414fd68aedd617
  - 1994aebbe6ab65afda414fd68aedd617
  - 1994aebbe6ab65afda414fd68aedd617
categories:
  - Linux
  - Ubuntu
tags:
  - dialog
  - Execute
  - Virus

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>Executable</b> files are files that can be used to run a program or to do a specific purpose specified in the file. Normally not all files defaultly would be executable in <b>Linux</b>&nbsp;because of security reasons. If all files are defaultly executable then the scripts can do whatever the hell they want to do in your computer. As we call them <b>VIRUSES</b>. This is one of the main reasons why you should use <b>Linux</b>. It&#8217;s virus free.</p> 
  
  <p>
    If you want a file executable you can do it with a simple trick in <b>Ubuntu</b>.<br />Right click on the file and click <b>Properties</b>. Choose the tab <b>Permissions</b>&nbsp;and click on <b>Allow executing file as program</b>.
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//3.bp.blogspot.com/-vyc_rUSU6ac/UdratpZDCiI/AAAAAAAAC10/krSRAiRw4Jo/s1600/0028.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//3.bp.blogspot.com/-vyc_rUSU6ac/UdratpZDCiI/AAAAAAAAC10/krSRAiRw4Jo/s1600/0028.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    After clicking close the dialog. Now the file is <b>executable</b>. While opening the file you will get a dialog :
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//1.bp.blogspot.com/-ReDQr3AFPRY/UdrbA73fRCI/AAAAAAAAC2E/KB3wbLQERDg/s1600/0029.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//1.bp.blogspot.com/-ReDQr3AFPRY/UdrbA73fRCI/AAAAAAAAC2E/KB3wbLQERDg/s1600/0029.png" /></a>
  </div>
  
  <div class="separator" style="clear: both; text-align: left;">
    You want to execute the file so click <b>Run in Terminal</b>.
  </div>
</div>