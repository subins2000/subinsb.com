---
title: Taste Testing Without Smell
author: Subin Siby
type: post
date: 2011-04-22T13:56:00+00:00
excerpt: "Taste Testing Without SmellTaste Testing Without SmellWe all know that some foods taste better than others but what gives us the ability to experience all these unique flavours? This simple experiment shows that there's a lot more to taste than you mig..."
url: /taste-testing-without-smell
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2011/04/taste-testing-without-smell-taste.html
  - http://sag-3.blogspot.com/2011/04/taste-testing-without-smell-taste.html
  - http://sag-3.blogspot.com/2011/04/taste-testing-without-smell-taste.html
  - http://sag-3.blogspot.com/2011/04/taste-testing-without-smell-taste.html
  - http://sag-3.blogspot.com/2011/04/taste-testing-without-smell-taste.html
  - http://sag-3.blogspot.com/2011/04/taste-testing-without-smell-taste.html
syndication_item_hash:
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
  - eeb17b8603fe5747f83247acb55d787f
blogger_permalink:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
blogger_blog:
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
categories:
  - Uncategorized
tags:
  - Uncategorized

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span class="Apple-style-span" style="color: #444444; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 13px; line-height: 18px;"></span></p> 
  
  <h3 class="post-title entry-title" style="color: #444444; font: normal normal bold 22px/normal Arial, Tahoma, Helvetica, FreeSans, sans-serif; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; position: relative;">
    Taste Testing Without Smell
  </h3>
  
  <div class="post-header" style="line-height: 1.6; margin-bottom: 1em; margin-left: 0px; margin-right: 0px; margin-top: 0px;">
    <div class="post-header-line-1">
    </div>
  </div>
  
  <div class="post-body entry-content" id="post-body-2764652920138868021" style="line-height: 1.4; position: relative; width: 326px;">
    <div dir="ltr" style="text-align: left;" trbidi="on">
      <div align="center" style="text-align: center;">
        <span class="style3style23"><b><span style="font-family: Arial;">Taste Testing Without Smell</span></b></span><b><span style="font-family: Arial;"></span></b>
      </div>
      
      <div class="MsoNormal" style="margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in;">
      </div>
      
      <div class="MsoNormal" style="margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in;">
        We all know that some foods taste better than others but what gives us the ability to experience all these unique flavours? This simple experiment shows that there&#8217;s a lot more to taste than you might have first thought.
      </div>
      
      <div class="MsoNormal" style="margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in;">
        <strong>What you&#8217;ll need:</strong>
      </div>
      
      <ul style="line-height: 1.4; list-style-image: initial; list-style-position: initial; list-style-type: disc; margin-bottom: 0.5em; margin-left: 0px; margin-right: 0px; margin-top: 0.5em; padding-bottom: 0px; padding-left: 2.5em; padding-right: 2.5em; padding-top: 0px;" type="disc">
        <li class="MsoNormal" style="border-bottom-color: transparent; border-bottom-style: none; border-bottom-width: 1px; border-color: initial; border-left-style: none; border-right-style: none; border-top-color: initial; border-top-style: none; border-top-width: initial; border-width: initial; color: #444444; margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in; padding-bottom: 0.25em; padding-left: 0px; padding-right: 0px; padding-top: 0.25em; text-indent: 0px;">
          <span style="font-family: Arial; font-size: 10pt;">A small piece of peeled potato</span>
        </li>
        <li class="MsoNormal" style="border-bottom-color: transparent; border-bottom-style: none; border-bottom-width: 1px; border-color: initial; border-left-style: none; border-right-style: none; border-top-color: rgb(238, 238, 238); border-top-style: none; border-top-width: 1px; border-width: initial; color: #444444; margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in; padding-bottom: 0.25em; padding-left: 0px; padding-right: 0px; padding-top: 0.25em; text-indent: 0px;">
          A small piece of peeled apple (same shape as the potato so you can&#8217;t tell the difference)
        </li>
        <li class="MsoNormal" style="border-bottom-color: initial; border-bottom-style: none; border-bottom-width: initial; border-color: initial; border-left-style: none; border-right-style: none; border-top-color: rgb(238, 238, 238); border-top-style: none; border-top-width: 1px; border-width: initial; color: #444444; margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in; padding-bottom: 0.25em; padding-left: 0px; padding-right: 0px; padding-top: 0.25em; text-indent: 0px;">
          &nbsp;<strong>Instructions:</strong>
        </li>
      </ul>
      
      <ol type="1">
        <li class="MsoNormal" style="margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-indent: 0px;">
          Close your eyes and mix up the piece of potato and the piece of apple so you don&#8217;t know which is which.
        </li>
        <li class="MsoNormal" style="margin-bottom: 0pt; margin-left: 0in; margin-right: 0in; margin-top: 0in; padding-bottom: 0px; padding-left: 0px; padding-right: 0px; padding-top: 0px; text-indent: 0px;">
          <shapetype coordsize="21600,21600" filled="f" id="_x0000_t75" o:preferrelative="t" o:spt="75" path="m@4@5l@4@11@9@11@9@5xe" stroked="f"><stroke joinstyle="miter"></stroke><formulas><f eqn="if lineDrawn pixelLineWidth 0"></f><f eqn="sum @0 1 0"></f><f eqn="sum 0 0 @1"></f><f eqn="prod @2 1 2"></f><f eqn="prod @3 21600 pixelWidth"></f><f eqn="prod @3 21600 pixelHeight"></f><f eqn="sum @0 0 1"></f><f eqn="prod @6 1 2"></f><f eqn="prod @7 21600 pixelWidth"></f><f eqn="sum @8 21600 0"></f><f eqn="prod @7 21600 pixelHeight"></f><f eqn="sum @10 21600 0"></f></formulas><path gradientshapeok="t" o:connecttype="rect" o:extrusionok="f"></path><lock aspectratio="t" v:ext="edit"></lock></shapetype><shape alt="Does this apple taste like a potato?" id="_x0000_s1026" o:allowoverlap="f" style="height: 115.5pt; left: 0px; margin-left: -9pt; margin-top: 21.55pt; position: absolute; text-align: left; width: 111.6pt; z-index: 1;" type="#_x0000_t75"><imagedata o:title="apple (230 x 238)" src="file:///C:/DOCUME~1/sim/LOCALS~1/Temp/msoclip1/01/clip_image001.jpg"></imagedata><wrap type="square"></wrap></shape>Hold your nose and eat each piece, can you tell the difference?
        </li>
      </ol>
      
      <div class="style24style1style23" style="margin-bottom: auto; margin-left: 0in; margin-right: 0in; margin-top: auto;">
        &nbsp;<strong>What&#8217;s happening?</strong>
      </div>
      
      <div class="style1style26" style="margin-bottom: auto; margin-left: 0in; margin-right: 0in; margin-top: auto;">
        Holding your nose while tasting the potato and apple makes it hard to tell the difference between the two. Your nose and mouth are connected through the same airway which means that you taste and smell foods at the same time. Your sense of taste can recognize salty, sweet, bitter and sour but when you combine this with your sense of smell you can recognize many other individual &#8216;tastes&#8217;. Take away your smell (and sight) and you limit your brains ability to tell the difference between certain foods.
      </div>
      
      <div>
      </div>
    </div>
  </div>
</div>