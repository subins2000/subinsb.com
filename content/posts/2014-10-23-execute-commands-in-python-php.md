---
title: 'Executing Terminal Commands in Python & PHP'
author: Subin Siby
type: post
date: 2014-10-23T03:30:55+00:00
url: /execute-commands-in-python-php
categories:
  - PHP
  - Python
  - Short Post
tags:
  - Terminal

---
<p class="">
  Python & PHP are great languages and there are many similarities between them. In this short post, you will learn how to execute terminal commands in PHP & Python.
</p>

## PHP

As I said before many times in other posts, there areexec andsystem command to execute commands :

<pre class="prettyprint"><code>exec("firefox ‘http://subinsb.com’");</code></pre>

Or in the other way :

<pre class="prettyprint"><code>system("firefox ‘http://subinsb.com’");</code></pre>

## Python

In Python, there is no **exec** or **system** function. But there is a **system** function in the **os** nodule. So, you have to include the **os** module first and then execute the command :

<pre class="prettyprint"><code>import os
os.system("firefox ‘http://subinsb.com’")</code></pre>