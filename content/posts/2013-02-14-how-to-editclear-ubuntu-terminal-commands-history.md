---
title: How to edit/clear Ubuntu Terminal commands history ?
author: Subin Siby
type: post
date: 2013-02-14T14:44:00+00:00
excerpt: 'This simple trick will help you to clear or edit terminal commands you entered in Terminal of Ubuntu.Open the file /home/user/.bash_history&nbsp;(Replace red text with your username.To make it simple Go to your home folder and Press CTRL&nbsp;+ H. It w...'
url: /how-to-editclear-ubuntu-terminal-commands-history
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
blogger_permalink:
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
  - http://sag-3.blogspot.com/2013/02/how-to-editclear-ubuntu-terminal.html
syndication_item_hash:
  - 03bf80abdce3658600c82fd1c4ffaf30
  - 03bf80abdce3658600c82fd1c4ffaf30
  - 03bf80abdce3658600c82fd1c4ffaf30
  - 03bf80abdce3658600c82fd1c4ffaf30
  - 03bf80abdce3658600c82fd1c4ffaf30
  - 03bf80abdce3658600c82fd1c4ffaf30
categories:
  - Linux
  - Ubuntu
tags:
  - Hidden
  - Terminal

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This simple trick will help you to clear or edit terminal commands you entered in <b>Terminal </b>of <b>Ubuntu</b>.<br />Open the file <b>/home/<span style="color: red;">user</span>/.bash_history</b>&nbsp;(Replace red text with your username.</p> 
  
  <p>
    To make it simple Go to your home folder and Press <b>CTRL&nbsp;+ H</b>. It will show hidden files.<br />search for the file&nbsp;<b>.bash_history</b><br /><b>Edit the file</b>.<br />You can see all the&nbsp;commands you entered in&nbsp;<b>Terminal</b>.
  </p>
  
  <p>
    <b>Remove what you don&#8217;t want and save.</b></div>