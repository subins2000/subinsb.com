---
title: Keypad not working in Ubuntu fix
author: Subin Siby
type: post
date: 2012-10-13T13:02:00+00:00
excerpt: |
  <div dir="ltr" trbidi="on">Some can't type numbers using keypad. This because you enabled the <b>Pointer cab be controlled using keypad</b>. To disable it there are two ways.<br><div><br></div><div><ul><li><b>By Keyboard</b></li></ul><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CTRL + SHIFT + NUM LOCK</b></div><div><ul><li><b>By Terminal</b></li></ul>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Command :-</div><blockquote>gnome-keyboard-properties</blockquote><div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<u><b>OR</b></u></div><div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;go to&nbsp;<b>System &#9656; Preferences &#9656; Keyboard</b></div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;You will get a window.<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Under <b>Mouse Keys </b>tab there is a option called&nbsp;<b>Pointer cab be controlled using keypad</b>.<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Uncheck it as shown in the image below.<br><div><a href="//4.bp.blogspot.com/-DAekZTlaHRA/UHll83cXVAI/AAAAAAAACAs/5l65kPO3Pxk/s1600/numpad.png" imageanchor="1"><img border="0" src="//4.bp.blogspot.com/-DAekZTlaHRA/UHll83cXVAI/AAAAAAAACAs/5l65kPO3Pxk/s1600/numpad.png"></a></div></div>
url: /keypad-not-working-in-ubuntu-fix
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 48333b8b31059849efe7f531b11f15e9
  - 48333b8b31059849efe7f531b11f15e9
  - 48333b8b31059849efe7f531b11f15e9
  - 48333b8b31059849efe7f531b11f15e9
  - 48333b8b31059849efe7f531b11f15e9
  - 48333b8b31059849efe7f531b11f15e9
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
  - http://sag-3.blogspot.com/2012/10/keypad-not-working-in-ubuntu.html
categories:
  - Linux
  - Ubuntu
tags:
  - Keyboard

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Some can&#8217;t type numbers using keypad. This because you enabled the <b>Pointer cab be controlled using keypad</b>. To disable it there are two ways.</p> 
  
  <div>
  </div>
  
  <div>
    <ul style="text-align: left;">
      <li>
        <b>By Keyboard</b>
      </li>
    </ul>
    
    <p>
      <b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CTRL + SHIFT + NUM LOCK</b></div> 
      
      <div style="text-align: left;">
        <ul style="text-align: left;">
          <li>
            <b>By Terminal</b>
          </li>
        </ul>
        
        <p>
          &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Command :-
        </p>
      </div>
      
      <blockquote class="tr_bq">
        <p>
          gnome-keyboard-properties
        </p>
      </blockquote>
      
      <div style="text-align: center;">
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<u><b>OR</b></u>
      </div>
      
      <div style="text-align: left;">
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;go to&nbsp;<b style="background-color: white; color: #333333; font-family: Arial, Tahoma, Helvetica, FreeSans, sans-serif; font-size: 15px; line-height: 20px;">System ▸ Preferences ▸ Keyboard</b>
      </div>
      
      <p>
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;You will get a window.<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Under <b>Mouse Keys </b>tab there is a option called&nbsp;<b>Pointer cab be controlled using keypad</b>.<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Uncheck it as shown in the image below.
      </p>
      
      <div class="separator" style="clear: both; text-align: center;">
        <a href="//4.bp.blogspot.com/-DAekZTlaHRA/UHll83cXVAI/AAAAAAAACAs/5l65kPO3Pxk/s1600/numpad.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-DAekZTlaHRA/UHll83cXVAI/AAAAAAAACAs/5l65kPO3Pxk/s1600/numpad.png" /></a>
      </div></div>