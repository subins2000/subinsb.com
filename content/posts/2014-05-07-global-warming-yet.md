---
title: Global Warming, you here yet ?
author: Subin Siby
type: post
date: 2014-05-07T16:17:58+00:00
url: /global-warming-yet
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Personal

---
We all have been hearing about **Global Warming** and it&#8217;s consequences. The studies tell us that the effects of it will soon be experienced by us. In my place (Kerala), I say it&#8217;s already started. Millions of Trees are going down each day and people push out harmful Carbon compunds into the atmosphere without thinking. On a cruel world like this, I feel really sad.

Kerala faces heavy summer and heavy monsoon. On summer days, rain is very often. But, now in the year 2014, it rains like monsoon in the middle of summer. Throughout Kerala, it&#8217;s raining heavily. The cost of the damage is estimated to be 70 crore rupees and may increase more.

In my 14 years of life, I have never seen a rain like this in the summer, that too in the middle. Climate change was one of the effect of Global Warming and now I&#8217;ve seen it. If this is what it;s going to be, then I wonder if the monsoon season will be the summer season (:O).

The Western Ghats issue was all over India. The vast rain forests are getting destroyed and the Carbon compounds in the atmosphere could not be controlled. When the controlling vanishes, we&#8217;re all dead.

The modernization, the transition to a new industrial world will only destroy the nature. Either we take action now or we will extinct ourselves. In the coming years, we will all experience the effects of Global Warming and hopefully we&#8217;ll see 2030 with a living heart.

PS : I hate going to school on a rainy day