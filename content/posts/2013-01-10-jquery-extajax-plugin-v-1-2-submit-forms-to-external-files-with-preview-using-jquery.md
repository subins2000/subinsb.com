---
title: 'Jquery ExtAjax Plugin V 1.2: Submit forms to external files with preview using Jquery'
author: Subin Siby
type: post
date: 2013-01-10T15:45:00+00:00
excerpt: "If you don't know what this plugin will do then visit the page of the previous version here.The last version of Jquery ExtAjax plugin didn't have preview feature. But in this version it have a preview feature.The latest version is V 1.2What's New in th..."
url: /jquery-extajax-plugin-v-1-2-submit-forms-to-external-files-with-preview-using-jquery
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
  - http://sag-3.blogspot.com/2013/01/extajax-v1.2.html
syndication_item_hash:
  - 02e29c635a2b9e349f86eacd3ffb8a9e
  - 02e29c635a2b9e349f86eacd3ffb8a9e
  - 02e29c635a2b9e349f86eacd3ffb8a9e
  - 02e29c635a2b9e349f86eacd3ffb8a9e
  - 02e29c635a2b9e349f86eacd3ffb8a9e
  - 02e29c635a2b9e349f86eacd3ffb8a9e
categories:
  - JavaScript
  - jQuery
tags:
  - ExtAjax
  - Plugin

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you don&#8217;t know what this plugin will do then visit the page of the previous version <a href="http://sag-3.blogspot.com/2012/09/jquery-extajax-plugin-submit-forms-to.html" >here</a>.<br />The last version of <b>Jquery ExtAjax </b>plugin didn&#8217;t have preview feature. But in this version it have a preview feature.<br />The latest version is <b>V 1.2</b></p> 
  
  <h3 style="text-align: left;">
    <b><u>What&#8217;s New in this version</u></b>
  </h3>
  
  <div>
    <ul style="text-align: left;">
      <li>
        Faster
      </li>
      <li>
        Preview feature.&nbsp;
      </li>
    </ul>
  </div>
  
  <div>
    <h3 style="text-align: left;">
      <span style="color: #333333; font-family: inherit;"><span style="line-height: 16px;"><u>What should I do after downloading.</u></span></span>
    </h3>
    
    <div>
      <span style="color: #333333;"><span style="line-height: 16px;">After downloading upload "<b>extajax.js</b>", "<b>asdva.php</b>" to your server.</span></span>
    </div>
    
    <div>
      <span style="color: #333333;"><span style="line-height: 16px;">Before you do this configure the place in <b>JS</b>&nbsp;file&nbsp;where <b>asdva.php</b>&nbsp;is located.</span></span>
    </div>
    
    <h4 style="text-align: left;">
      <span style="color: #333333;"><span style="line-height: 16px;">&nbsp;<b>How can I configure ?</b></span></span>
    </h4>
    
    <div>
      <span style="color: #333333;"><span style="line-height: 16px;"><b>&nbsp;</b>Go to <b>extajax.js</b>&nbsp;file. Search for&nbsp;<b>var asdvafile&nbsp;</b>. Enter the location of the <b>asdva.php </b>if the file is not in the same folderas <b>extajax.js</b>.</span></span>
    </div>
    
    <div>
      <span style="color: #333333;"><span style="line-height: 16px;"><a name='more'></a><br /></span></span>
    </div>
    
    <p>
      <span style="color: #333333; font-family: inherit;"><span style="line-height: 16px;">Jquery Code :</span></span>
    </p>
    
    <blockquote class="tr_bq">
      <p>
        <span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;"><script></span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;"><br /></span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">$("</span><span style="background-color: white; color: red; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">#form</span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">").submit(function(){</span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">event.preventDefault();</span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">$("</span><span style="background-color: white; color: red; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">#form</span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">").extajax({target:"</span><span style="background-color: white; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;"><span style="color: red;">#preview</span></span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;">"});</span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;"><br />});</span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;"><br /></span><span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;"></script></span>
      </p>
    </blockquote>
    
    <p>
      <span style="background-color: white; color: #333333; font-family: Courier, monospace, 'Courier New'; font-size: 13px; font-weight: bold; line-height: 16px;"><br /></span><span style="font-size: large;"><span style="border: 2px solid black;"><span style="color: white;"><span style="background-color: #6aa84f;"><a href="http://subins.tk/labs/extajax/index.html" >View Demo</a></span></span></span>&nbsp; &nbsp; &nbsp; &nbsp;<span style="background-color: #6aa84f; border: 2px solid black;"><span style="color: white;"><a href="https://drive.google.com/uc?export=download&id=0B2VjYaTkCpiQbTJtY1ZyNmFjZUk" >Download(ZIP)</a></span></span>&nbsp; &nbsp; &nbsp; &nbsp;<span style="background-color: #6aa84f; border: 2px solid black; color: white;"><a href="https://drive.google.com/uc?export=download&id=0B2VjYaTkCpiQcXRreFNxLXRCam8" >Download(Javascript file)</a></span></span></div> </div>