---
title: Create Group Chat In PHP With MySQL, jQuery And AJAX
author: Subin Siby
type: post
date: 2014-01-15T17:34:29+00:00
url: /group-chat-in-php-with-ajax-jquery-mysql
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - AJAX
  - HTML
  - JavaScript
  - jQuery
  - MySQL
  - PHP
  - SQL

---
Chatting is one of the most implemented feature on websites. Group chat makes users to share their feelings and other news easily to their friends easily. AJAX makes this chatting more beautiful, elegant, simple and comfortable. Group chats may be vulnerable to **SQL injections** and **XSS attacks**. But in this post where we&#8217;re going to make a group chat that **doesn&#8217;t have these vulnerabilities** which makes it more awesome. You can see a demo, or you can download the files directly.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=b4i8alv3xd7k/s/6wle8d7632fcj0w2j80x&class=16" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/php/chat" target="_blank">Demo</a>
</div>


## Things To Note

Make sure the Time Zone on MySQL server is the same as on PHP server. If the time zone is different, the user&#8217;s online presence can&#8217;t be understood.

We limit the name of the user to **20 characters**, because we don&#8217;t want a long name that overflows the "online users" display element. There is no error message displayed when a user submits a name of more than 20 chars. If the user submits the name of 20 chars, the first 20 chars will only be inserted in to the table. Other chars after 20 chars will be removed by MySQL.

If you need **Users&#8217; Typing Status** display with the chat, see <a title="Group Chat In PHP With Users' Typing Status" href="//subinsb.com/php-group-chat-with-user-type-status" target="_blank">this tutorial</a> after completing this tutorial.

## tables.sql

The SQL code that creates the two tables needed for the group chat is contained in the **tables.sql** file :

<pre class="prettyprint"><code>-- Set MySQL timezone to UTC
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET GLOBAL time_zone = "+00:00";
-- Table structure for table `chatters`
CREATE TABLE IF NOT EXISTS `chatters` (
  `name` varchar(20) NOT NULL,
  `seen` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
-- Table structure for table `messages`
CREATE TABLE IF NOT EXISTS `messages` (
  `name` varchar(20) NOT NULL,
  `msg` text NOT NULL,
  `posted` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;</code></pre>

When you execute the above **SQL** code, you will have two tables, one **chatters** and the other **messages**. In **chatters** table, we add the logged in users&#8217; details where as the other one, we will add the messages sent by the users.

## config.php

The database configuration is stored in this file. You should change the credentials according to your database :

<pre class="prettyprint"><code>&lt;?
// ini_set("display_errors","on");
if(!isset($dbh)){
 session_start();
 date_default_timezone_set("UTC");
 $musername = "username";
 $mpassword = "password";
 $hostname  = "hostname";
 $dbname    = "dbname";
 $dbh=new PDO('mysql:dbname='.$dbname.';host='.$hostname.";port=3306",$musername, $mpassword);
 /*Change The Credentials to connect to database.*/
 include("user_online.php");
}
?&gt;
</code></pre>

The session is started when this file loads. There is a file named **user_online.php** included in the file. This file deletes the offline users and updates the time stamp of the currently logged in user. This time stamp determines the online, offline status. I explained more about it in the **user_online.php** heading in this post.

## index.php

The main character of our chat is this file. **index.php** joins everything together.

<pre class="prettyprint"><code>&lt;?include("config.php");include("login.php");?&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;
 &lt;head&gt;
  &lt;script src="//code.jquery.com/jquery-1.11.1.min.js"&gt;&lt;/script&gt;
  &lt;script src="chat.js"&gt;&lt;/script&gt;
  &lt;link href="chat.css" rel="stylesheet"/&gt;
  &lt;title&gt;PHP Group Chat With jQuery & AJAX&lt;/title&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;div id="content" style="margin-top:10px;height:100%;"&gt;
   &lt;center&gt;&lt;h1&gt;Group Chat In PHP&lt;/h1&gt;&lt;/center&gt;
   &lt;div class="chat"&gt;
    &lt;div class="users"&gt;
     &lt;?include("users.php");?&gt;
    &lt;/div&gt;
    &lt;div class="chatbox"&gt;
     &lt;?
     if(isset($_SESSION['user'])){
      include("chatbox.php");
     }else{
      $display_case=true;
      include("login.php");
     }
     ?&gt;
    &lt;/div&gt;
   &lt;/div&gt;
  &lt;/div&gt;
 &lt;/body&gt;
&lt;/html&gt;
</code></pre>

When a user is not logged in, this file will load the **login.php** file which have the login box and others. If the user is logged in, then it will directly display the **chatbox.php** which contains the chatbox. Users who are currently online are shown using **users.php** file.

## login.php

The login box and the login authentication, filtering, checking are added in this file. This file is included twice in the **index.php** file, one for checking and other for displaying login box.

<pre class="prettyprint"><code>&lt;?
if(isset($_POST['name']) && !isset($display_case)){
 $name=htmlspecialchars($_POST['name']);
 if($name!=""){
  $sql=$dbh-&gt;prepare("SELECT name FROM chatters WHERE name=?");
  $sql-&gt;execute(array($name));
  if($sql-&gt;rowCount()!=0){
   $ermsg="&lt;h2 class='error'&gt;Name Taken. &lt;a href='index.php'&gt;Try another Name.&lt;/a&gt;&lt;/h2&gt;";
  }else{
   $sql=$dbh-&gt;prepare("INSERT INTO chatters (name,seen) VALUES (?,NOW())");
   $sql-&gt;execute(array($name));
   $_SESSION['user']=$name;
  }
 }else{
  $ermsg="&lt;h2 class='error'&gt;&lt;a href='index.php'&gt;Please Enter A Name.&lt;/a&gt;&lt;/h2&gt;";
 }
}elseif(isset($display_case)){
 if(!isset($ermsg)){
?&gt;
 &lt;h2&gt;Name Needed For Chatting&lt;/h2&gt;
 You must provide a name for chatting. This name will be visible to other users.&lt;br/&gt;&lt;br/&gt;
 &lt;form action="index.php" method="POST"&gt;
  &lt;div&gt;Your Name : &lt;input name="name" placeholder="A Name Please"/&gt;&lt;/div&gt;
  &lt;button&gt;Submit & Start Chatting&lt;/button&gt;
 &lt;/form&gt;
&lt;?
 }else{
  echo $ermsg;
 }
}
?&gt;</code></pre>

If the user has submitted the login form, then this file will do the following :

  * Filter Name (Remove HTML entities)
  * Check If there is another user with the name

If everything is OK, then the file changes the **user** value of session to the name submitted. **index.php** and **chatbox.php** takes care of the rest.

## chatbox.php

This file don&#8217;t have that much content. This file contains the log out link, chat messages container and chat form :

<pre class="prettyprint"><code>&lt;?
include("config.php");
if(isset($_SESSION['user'])){
?&gt;
 &lt;h2&gt;Room For ALL&lt;/h2&gt;
 &lt;a style="right: 20px;top: 20px;position: absolute;cursor: pointer;" href="logout.php"&gt;Log Out&lt;/a&gt;
 &lt;div class='msgs'&gt;
  &lt;?include("msgs.php");?&gt;
 &lt;/div&gt;
 &lt;form id="msg_form"&gt;
  &lt;input name="msg" size="30" type="text"/&gt;
  &lt;button&gt;Send&lt;/button&gt;
 &lt;/form&gt;
&lt;?
}
?&gt;</code></pre>

The messages are displayed from **msgs.php** and the form is also displayed.

## msgs.php

Displays the messages sent by the other users and himself/herself. A request is made to this file every 5 seconds to check new messages. When the user logs out from another page of the browser window, **msgs.php** will make the current page reload to make sure everything is alright.

<pre class="prettyprint"><code>&lt;?
include("config.php");
$sql=$dbh-&gt;prepare("SELECT * FROM messages");
$sql-&gt;execute();
while($r=$sql-&gt;fetch()){
 echo "&lt;div class='msg' title='{$r['posted']}'&gt;&lt;span class='name'&gt;{$r['name']}&lt;/span&gt; : &lt;span class='msgc'&gt;{$r['msg']}&lt;/span&gt;&lt;/div&gt;";
}
if(!isset($_SESSION['user']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest'){
 echo "&lt;script&gt;window.location.reload()&lt;/script&gt;";
}
?&gt;</code></pre>

## send.php

When the user submits a message, the message is sent to **send.php**. This file handles the message, filters it and insert into database.

<pre class="prettyprint"><code>&lt;?
include("config.php");
if(!isset($_SESSION['user']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest'){
 die("&lt;script&gt;window.location.reload()&lt;/script&gt;");
}
if(isset($_SESSION['user']) && isset($_POST['msg'])){
 $msg=htmlspecialchars($_POST['msg']);
 if($msg!=""){
  $sql=$dbh-&gt;prepare("INSERT INTO messages (name,msg,posted) VALUES (?,?,NOW())");
  $sql-&gt;execute(array($_SESSION['user'],$msg));
 }
}
?&gt;</code></pre>

## users.php

Currently online users are displayed using this file. This file is also requested in every 5 seconds by **jQuery**.

<pre class="prettyprint"><code>&lt;?
include("config.php");
echo "&lt;h2&gt;Users&lt;/h2&gt;";
$sql=$dbh-&gt;prepare("SELECT name FROM chatters");
$sql-&gt;execute();
while($r=$sql-&gt;fetch()){
 echo "&lt;div class='user'&gt;{$r['name']}&lt;/div&gt;";
}
?&gt;</code></pre>

## user_online.php

Whenever the **config.php** file is called, this file is also called. This file loops through the online users on table **chatters** and check if their time stamp is lesser that 25 seconds the current time. If it is lesser, then that user is dropped (deleted) from the table. It also updates the time stamp of the currently logged in user if there is one making it impossible for the other script to delete the current user. It is necessary to have the same time zone on **MySQL **server and the **PHP** server. If the currently logged in user is accidentally deleted in case of misunderstanding, the script will automatically add the user to the table **chatters**. What an important file !

<pre class="prettyprint"><code>&lt;?
if(isset($_SESSION['user'])){
 $sqlm=$dbh-&gt;prepare("SELECT name FROM chatters WHERE name=?");
 $sqlm-&gt;execute(array($_SESSION['user']));
 if($sqlm-&gt;rowCount()!=0){
  $sql=$dbh-&gt;prepare("UPDATE chatters SET seen=NOW() WHERE name=?");
  $sql-&gt;execute(array($_SESSION['user']));
 }else{
  $sql=$dbh-&gt;prepare("INSERT INTO chatters (name,seen) VALUES (?,NOW())");
  $sql-&gt;execute(array($_SESSION['user']));
 }
}
/* Make sure the timezone on Database server and PHP server is same */
$sql=$dbh-&gt;prepare("SELECT * FROM chatters");
$sql-&gt;execute();
while($r=$sql-&gt;fetch()){
 $curtime=strtotime(date("Y-m-d H:i:s",strtotime('-25 seconds', time())));
 if(strtotime($r['seen']) &lt; $curtime){
  $kql=$dbh-&gt;prepare("DELETE FROM chatters WHERE name=?");
  $kql-&gt;execute(array($r['name']));
 }
}
?&gt;</code></pre>

## logout.php

If the user can log in,.there should be a log out option. Here is the file that will destroy the session and redirects to the main page or as you call it "logout" :

<pre class="prettyprint"><code>&lt;?
session_start();
include("config.php");
$sql=$dbh-&gt;prepare("DELETE FROM chatters WHERE name=?");
$sql-&gt;execute(array($_SESSION['user']));
session_destroy();
header("Location: index.php");
?&gt;</code></pre>

# Client Side

It&#8217;s time to move to the client side, where we will design our chatbox with **CSS** and add the **jQuery** code to make it easy.

## chat.css

The chatbox, online users and other styling is in here :

<pre class="prettyprint"><code>.chat .users, .chat .chatbox{
 display:inline-block;
 vertical-align:top;
 height:350px;
 padding:0px 15px;
 position:relative;
}
.chat .users{
 background:#CCC;
 color:white;
 width:98px;
 overflow-y:auto;
}
.chat .chatbox{
 background:#fff;
 color:black;
 margin-left:4px;
 width:330px;
}
.chat .chatbox .msgs{
 border-top:1px solid black;
 border-bottom:1px solid black;
 overflow-y:auto;
 height:260px;
}
.chat .chatbox #msg_form{
 padding-top:1.5px;
}
.chat .error{color:red;}
.chat .success{color:green;}
.chat .msgs .msg, .chat .users .user{border-bottom:1px solid black;padding:4px 0px;white-space:pre-line;word-break:break-word;}</code></pre>

The elements we added are all wrapped in the **.chat** container, so the **chat.css** won&#8217;t mess up any other styles of your site.

## chat.js

The **jQuery** code is the content of this file. Note that you should add a **script[src] **that links to the **jQuery** library source.

<pre class="prettyprint"><code>function scTop(){
 $(".msgs").animate({scrollTop:$(".msgs")[0].scrollHeight});
}
function load_new_stuff(){
 localStorage['lpid']=$(".msgs .msg:last").attr("title");
 $(".msgs").load("msgs.php",function(){
  if(localStorage['lpid']!=$(".msgs .msg:last").attr("title")){
   scTop();
  }
 });
 $(".users").load("users.php");
}
$(document).ready(function(){
 scTop();
 $("#msg_form").on("submit",function(){
  t=$(this);
  val=$(this).find("input[type=text]").val();
  if(val!=""){
   t.after("&lt;span id='send_status'&gt;Sending.....&lt;/span&gt;");
   $.post("send.php",{msg:val},function(){
    load_new_stuff();
    $("#send_status").remove();
    t[0].reset();
   });
  }
  return false;
 });
});
setInterval(function(){
 load_new_stuff();
},5000);</code></pre>

I think that&#8217;s all the files. I made it to these much files to make the tutorial easy. Hope you like it. Be open source, share this with your developer friends. I&#8217;m sure they would love to see this. If you have any problems / suggestions, please say it out in the comments, I would love to hear it from you and I will reply if there isn&#8217;t any stupid school projects.

 [1]: #things-to-note
 [2]: #tablessql
 [3]: #configphp
 [4]: #indexphp
 [5]: #loginphp
 [6]: #chatboxphp
 [7]: #msgsphp
 [8]: #sendphp
 [9]: #usersphp
 [10]: #user-onlinephp
 [11]: #logoutphp
 [12]: #client-side
 [13]: #chatcss
 [14]: #chatjs
