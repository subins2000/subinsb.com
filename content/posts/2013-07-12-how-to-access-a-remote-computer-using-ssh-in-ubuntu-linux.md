---
title: How to access a remote computer using SSH in Ubuntu Linux
author: Subin Siby
type: post
date: 2013-07-12T03:30:00+00:00
excerpt: "SSH&nbsp;is the easiest way to access a remote computer not in graphical interface but in character interface.To access a remote computer you should need the computer's ip address&nbsp;&amp; a username of the computer. Now connect using the credentials..."
url: /how-to-access-a-remote-computer-using-ssh-in-ubuntu-linux
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/remote-computer-accessing-ssh-ubuntu-linux.html
syndication_item_hash:
  - 630a6ed561f10c7cc3955148f87011c3
  - 630a6ed561f10c7cc3955148f87011c3
  - 630a6ed561f10c7cc3955148f87011c3
  - 630a6ed561f10c7cc3955148f87011c3
  - 630a6ed561f10c7cc3955148f87011c3
  - 630a6ed561f10c7cc3955148f87011c3
categories:
  - Linux
  - SSH
  - Ubuntu
tags:
  - IP
  - Log In
  - poweroff
  - Remote

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>SSH</b>&nbsp;is the easiest way to access a remote computer not in graphical interface but in character interface.</p> 
  
  <div>
    To access a remote computer you should need the computer&#8217;s <b>ip address</b>&nbsp;& a username of the computer. Now connect using the credentials you now have. For Example:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      ssh simsu@192.168.10.1
    </p>
  </blockquote>
  
  <div>
    Type the password of the user and hit enter. You now entered the user&#8217;s system.
  </div>
  
  <div>
    You can <b>logout</b>, <b>shut down</b>&nbsp;the computer if you ssh <b>root</b>&nbsp;user. For that use the following <b>ssh</b>&nbsp;command:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      ssh root@192.168.10.1
    </p>
  </blockquote>
  
  <div>
    Type the password of the <b>root&nbsp;</b>user and hit enter. You now entered the user&#8217;s system with full privilege.
  </div>
  
  <div>
    You can shut down using <b>poweroff </b>command and to <b>log out</b> see <a href="http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html" >this post</a>.
  </div>
</div>