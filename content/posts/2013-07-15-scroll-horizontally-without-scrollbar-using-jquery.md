---
title: Scroll Horizontally Without Scrollbar Using jQuery
author: Subin Siby
type: post
date: 2013-07-15T03:30:00+00:00
excerpt: "To scroll horizontally with&nbsp;CSS&nbsp;is very annoying, because the whole page will move vertically and sometimes the x&nbsp;axis will scroll. So I decided to use jQuery&nbsp;and it's very simple. Only 237&nbsp;characters which is only 2&nbsp;lines..."
url: /scroll-horizontally-without-scrollbar-using-jquery
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
  - http://sag-3.blogspot.com/2013/07/horizontal-scroll-no-scrollbar-jquery.html
syndication_item_hash:
  - 91f213262215ca97294aa373439bc28e
  - 91f213262215ca97294aa373439bc28e
  - 91f213262215ca97294aa373439bc28e
  - 91f213262215ca97294aa373439bc28e
  - 91f213262215ca97294aa373439bc28e
  - 91f213262215ca97294aa373439bc28e
categories:
  - jQuery
tags:
  - event
  - Mouse
  - Scroll

---
To scroll horizontally with **CSS** is very annoying, because the whole page will move vertically and sometimes the **Y** axis will scroll. So I decided to use **jQuery** and it&#8217;s very simple. Only **237** characters which is only **2** lines. Here is the **jQuery **code with the div id **#scrollho**.

## jQuery

<pre class="prettyprint"><code>$('#scrollho').on('mousewheel',function(e){
 e.preventDefault();
 scrollLeft=$('#scrollho').scrollLeft();
 if(e.originalEvent.wheelDeltaY.toString().slice(0,1)=='-'){
  $('#scrollho').scrollLeft(scrollLeft+100);
 }else{
  $('#scrollho').scrollLeft(scrollLeft-100);
 }
});</code></pre>

## CSS

<pre class="prettyprint"><code>#scrollho{
 overflow:hidden;
}</code></pre>

## Explanation

The **#scrollho** div&#8217;s scrollbars are hidden with **CSS**. When a **mousewheel** event occurs on **#scrollho** the default event is neutralized and the div is scrolled horizontally to the direction of the mousewheel using **e.originalEvent.wheelDeltaY**. **e.originalEvent.wheelDeltaY **is converted into string as to do the **slice** function to check whether the number is negative or not.