---
title: Subin Siby sent you an invitation
author: Subin Siby
type: post
date: 2013-06-26T18:35:00+00:00
draft: true
url: /?p=1327
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Uncategorized
tags:
  - Uncategorized

---
<!-- 100% body table, class outerinvite -->

<table class="outerinvite" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#eeeeee">
      <!-- Frame --></p> 
      
      <table class="frame" width="542" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td style="font-size: 1px;" class="top-spacer" height="40" valign="top">
            <img width="1" height="1" style="display: block;margin:0px;padding:0px;display:block;" src="https://twitter.com/scribe/ibis?uid=0&#038;iid=9327319b-3fcb-4052-9e30-bccc23a4157e&#038;nid=9+20&#038;t=1" />&nbsp;
          </td>
        </tr>
        
        <tr>
          <td valign="top">
            <!-- Top corners --></p> 
            
            <table class="top-corners" bgcolor="#eeeeee" width="522" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td height="12" valign="top">
                  <img class="cut" style="margin: 0px; padding: 0px; display: block;margin:0px;padding:0px;display:block;" border="0" src="https://ea.twimg.com/email/t1/invite/top-corners.png" width="522" height="12" alt="Top corners image" />
                </td>
              </tr>
            </table>
            
            <p>
              <!--/ Top corners -->
              
              <!-- Content -->
            </p>
            
            <table style="border-left-style: solid; border-left-width: 1px; border-left-color: #cccccc; border-right-style: solid; border-right-width: 1px; border-right-color: #cccccc;" class="content" bgcolor="#ffffff" width="522" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="522" align="center" valign="top">
                  <table class="invite" width="520" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="spacer" width="9%">
                        &nbsp;
                      </td>
                      
                      <td class="invite-top spacer" height="38" width="82%" valign="top">
                        &nbsp;
                      </td>
                      
                      <td class="spacer" width="9%">
                        &nbsp;
                      </td>
                    </tr>
                    
                    <tr>
                      <td>
                        &nbsp;
                      </td>
                      
                      <td align="center" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>
                              &nbsp;
                            </td>
                            
                            <td class="avatar avatar-last" width="96">
                              <a href="https://twitter.com/i/redirect?url=https%3A%2F%2Ftwitter.com%2FSubinSiby&#038;sig=b3d27a3eba10d8cc3363c274d45fca240e1cedc9&#038;uid=0&#038;iid=9327319b-3fcb-4052-9e30-bccc23a4157e&#038;nid=9+525&#038;t=1" style="color:#0084b4;text-decoration:none;"><img border="0" class="avatar-image" src="https://si0.twimg.com/profile_images/2939851931/499c8e053bf3d136a5afcfd5166c1635_reasonably_small.jpeg" width="96" height="96" style="margin:0px;padding:0px;display:block;border-radius:3px;" /></a>
                            </td>
                            
                            <td>
                              &nbsp;
                            </td>
                          </tr>
                          
                          <tr>
                            <td height="20" colspan="5">
                              &nbsp;
                            </td>
                          </tr>
                        </table>
                        
                        <p>
                          <!--/ Avatar images --></td> 
                          
                          <td>
                            &nbsp;
                          </td></tr> 
                          
                          <tr>
                            <td>
                              &nbsp;
                            </td>
                            
                            <td valign="top" align="center">
                              <p class="names" style="font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:18px;line-height:normal;color:#333333;margin-top:0px;margin-bottom:25px;">
                                <a href="https://twitter.com/i/redirect?url=https%3A%2F%2Ftwitter.com%2FSubinSiby&#038;sig=b3d27a3eba10d8cc3363c274d45fca240e1cedc9&#038;uid=0&#038;iid=9327319b-3fcb-4052-9e30-bccc23a4157e&#038;nid=9+524&#038;t=1" class="name" style="color:#0084b4;text-decoration:none;font-weight:bold;color:#333333;text-decoration:none;">Subin Siby</a> has invited you to join Twitter! <img width="1" height="1" src="loadimage" style="margin:0px;padding:0px;display:block;" />
                              </p>
                            </td>
                            
                            <td>
                              &nbsp;
                            </td>
                          </tr>
                          
                          <tr>
                            <td>
                              &nbsp;
                            </td>
                            
                            <td valign="top" align="center">
                              <!-- Button --></p> 
                              
                              <table bgcolor="#33a9e5" class="newbutton" border="0" cellspacing="0" cellpadding="0" style="white-space:nowrap;background:#33a9e5 url(https://ea.twimg.com/email/t1/button_bg.png) top repeat-x;border-radius:5px;border-color:#2288cc;border-style:solid;border-width:1px;text-align:center;height:28px;">
                                <tr>
                                  <td class="spacer" width="15">
                                    &nbsp;
                                  </td>
                                  
                                  <td height="28" align="center">
                                    <span class="newbutton-text" style="color:#ffffff;font-size:15px;font-weight:bold;text-shadow:0px -1px 0px #3399dd;white-space:nowrap;overflow:hidden;padding:0px;margin:0px;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;"><a class="newbutton-link" href="https://twitter.com/i/redirect?url=https%3A%2F%2Ftwitter.com%2Fi%2Fe64bf650-4461-4edd-b7d4-a04a8ba54f49&#038;sig=8c5f3656abcab972d8f44995b549341133d56231&#038;uid=0&#038;iid=9327319b-3fcb-4052-9e30-bccc23a4157e&#038;nid=9+442&#038;t=1" style="color:#0084b4;text-decoration:none;color:#ffffff;text-decoration:none;"> Accept invitation </a></span>
                                  </td>
                                  
                                  <td class="spacer" width="15">
                                    &nbsp;
                                  </td>
                                </tr>
                              </table>
                              
                              <p>
                                <!--/ Button --></td> 
                                
                                <td>
                                  &nbsp;
                                </td></tr> 
                                
                                <tr>
                                  <td>
                                    &nbsp;
                                  </td>
                                  
                                  <td height="20">
                                    &nbsp;
                                  </td>
                                  
                                  <td>
                                    &nbsp;
                                  </td>
                                </tr>
                                
                                <tr>
                                  <td>
                                    &nbsp;
                                  </td>
                                  
                                  <td valign="top" align="center">
                                    <p class="extra" style="font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:12px;line-height:normal;color:#666666;margin-top:0px;margin-bottom:10px;">
                                      Twitter helps you stay connected with what&#8217;s happening right now and with the people and organizations you care about.
                                    </p>
                                  </td>
                                  
                                  <td>
                                    &nbsp;
                                  </td>
                                </tr></tbody> </table> </td> </tr> </tbody> </table> 
                                
                                <p>
                                  <!--/ Content -->
                                  
                                  <!-- Envelope -->
                                </p>
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td class="envelope" valign="top">
                                      <img class="cut" border="0" src="https://ea.twimg.com/email/t1/invite/envelope-with-logo.png" width="542" height="133" style="margin:0px;padding:0px;display:block;" />
                                    </td>
                                  </tr>
                                </table>
                                
                                <p>
                                  <!--/ Envelope --></td> </tr> </tbody> </table> 
                                  
                                  <p>
                                    <!--/ Frame -->
                                    
                                    <!-- Footer -->
                                  </p>
                                  
                                  <table class="footer" style="background: url(https://ea.twimg.com/email/t1/invite/footer-shadow.png) top repeat-x; border-top-style: solid; border-top-width: 1px; border-top-color: #cccccc;" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                      <td valign="top">
                                        <table class="footer-inner" width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                                          <tr>
                                            <td height="25">
                                              &nbsp;
                                            </td>
                                          </tr>
                                          
                                          <tr>
                                            <td class="ios" valign="top">
                                              <p style="font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:11px;line-height:normal;color:#777777;text-shadow:0 1px 0 #ffffff;margin-top:0px;margin-bottom:15px;">
                                                This message was sent by Twitter on behalf of Twitter users who entered your email address to invite you to Twitter. You can <a href="https://twitter.com/i/o?t=1&#038;iid=9327319b-3fcb-4052-9e30-bccc23a4157e&#038;uid=0&#038;c=nFT9T2HSRv8bnyIN0C7e0YFpRDRxkrZGNfrnP05PYRo%3D&#038;nid=9+26" style="text-decoration:none;color:#999999;color:#0084b4;text-decoration:none;">unsubscribe</a> if you&#8217;d prefer not to receive these emails. Please do not reply to this message; it was sent from an unmonitored email address. For general inquiries or to request support with your twitter account, please visit us at <a href="https://twitter.com/i/redirect?url=https%3A%2F%2Fsupport.twitter.com&#038;sig=d5a93ec8dc3f5086b47a0022aac6b1bd71661580&#038;uid=0&#038;iid=9327319b-3fcb-4052-9e30-bccc23a4157e&#038;nid=9+97&#038;t=1" style="text-decoration:none;color:#999999;color:#0084b4;text-decoration:none;">Twitter Support</a>.
                                              </p>
                                              
                                              <p class="address" style="font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:11px;line-height:normal;color:#777777;text-shadow:0 1px 0 #ffffff;margin-top:0px;margin-bottom:15px;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size:11px;line-height:normal;color:#999999;text-shadow:0 1px 0 #ffffff;margin-top:0px;margin-bottom:15px;">
                                                <a href="#" style="text-decoration:none;color:#999999;color:#0084b4;text-decoration:none;text-decoration:none;color:#999999;">Twitter, Inc. 1355 Market St., Suite 900<span class="break"></span> San Francisco, CA 94103</a>
                                              </p>
                                            </td>
                                          </tr>
                                          
                                          <tr>
                                            <td height="30">
                                              &nbsp;
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                  
                                  <p>
                                    <!--/ Footer --></td> </tr> </tbody> </table> 
                                    
                                    <p>
                                      <!--/ 100% body table, class outerinvite -->
                                    </p>