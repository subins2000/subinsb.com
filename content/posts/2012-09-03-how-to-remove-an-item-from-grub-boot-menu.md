---
title: How to remove an item from GRUB boot menu
author: Subin Siby
type: post
date: 2012-09-03T04:45:00+00:00
excerpt: 'When you upgrade from old Ubuntu to new Ubuntu. Your GRUB menu will contain more than one Ubuntu. To remove unwanted Ubuntu from GRUB you need to edit the GRUB configuration file.Follow these steps.Backup your GRUB&nbsp;configuration file which is in&n...'
url: /how-to-remove-an-item-from-grub-boot-menu
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
  - http://sag-3.blogspot.com/2012/09/how-to-remove-item-from-grub-boot-menu.html
syndication_item_hash:
  - c1bfdb461dd022e467cddb907a7bc496
  - c1bfdb461dd022e467cddb907a7bc496
  - c1bfdb461dd022e467cddb907a7bc496
  - c1bfdb461dd022e467cddb907a7bc496
  - c1bfdb461dd022e467cddb907a7bc496
  - c1bfdb461dd022e467cddb907a7bc496
categories:
  - Linux
  - Ubuntu
tags:
  - GRUB

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <span style="font-family: inherit;">When you upgrade from old <b>Ubuntu </b>to new <b>Ubuntu</b>. Your <b>GRUB </b>menu will contain more than one <b>Ubuntu</b>. To remove unwanted <b>Ubuntu </b>from <b>GRUB </b>you need to edit the <b>GRUB </b>configuration file.</span><br /><span style="font-family: inherit;">Follow these steps.</span><br /><span style="font-family: inherit;">Backup your <b>GRUB&nbsp;</b>configuration file which is in&nbsp;<span style="background-color: lime; color: #222222;">/boot/grub/grub.cfg</span><span style="background-color: white; color: #222222;">&nbsp;which we will need if something goes wrong</span>.</span><br /><span style="font-family: inherit;">Open <b>Terminal </b>(<b>Applications -> Accessories -> Terminal</b>). You need to be the root. For that :</span></p> 
  
  <blockquote class="tr_bq">
    <p>
      <span style="font-family: inherit;">sudo -i</span>
    </p>
  </blockquote>
  
  <p>
    <span style="font-family: inherit;">&nbsp;Now we want to edit the file. For that:</span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <code style="background-color: white; border: 0px; color: #222222; margin: 0px; padding: 0px; vertical-align: baseline;">&lt;span style="font-family: inherit;">gedit /boot/grub/grub.cfg&lt;/span></code>
    </p>
  </blockquote>
  
  <p>
    <span style="font-family: inherit;">You will get <b>GEDIT </b>window.</span><br /><span style="font-family: inherit;">Now search for&nbsp;<span style="background-color: lime;">### BEGIN /etc/grub.d/10_linux ###</span><span style="background-color: white;"><b>.</b></span></span><br /><span style="background-color: white; font-family: inherit;">After that line you will see the items of your <b>GRUB</b>.</span><br /><span style="font-family: inherit;">So if we want to remove&nbsp;<span style="background-color: lime;">Ubuntu, with Linux 2.6.32-30-generic</span><span style="background-color: white;">&nbsp;from <b>GRUB,&nbsp;</b>Remove the red highlited line in the image.</span></span>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-HJqIM2Gvlyw/UEQ1J6bIjjI/AAAAAAAABuo/6Ja_h0IJnnk/s1600/highlited.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><span style="font-family: inherit;"><img border="0" height="279" src="https://2.bp.blogspot.com/-HJqIM2Gvlyw/UEQ1J6bIjjI/AAAAAAAABuo/6Ja_h0IJnnk/s640/highlited.png" width="640" /></span></a>
  </div>
  
  <p>
    <span style="background-color: white; font-family: inherit;">You will now get the idea of removing items from <b>GRUB.</b></span><br /><span style="background-color: white; font-family: inherit;">Do as you like <b>but be careful not to remove other lines</b>.</span></div>