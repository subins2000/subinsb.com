---
title: How to execute a PHP PDO prepared statement
author: Subin Siby
type: post
date: 2013-07-06T03:30:00+00:00
excerpt: 'PDO - a method used to execute&nbsp;SQL&nbsp;statements risk free. Fast, secure and easy when compared to mysqli and mysql&nbsp;functions in PHP.A lot of newbies including me heard about PDO&nbsp;mostly in the 2010&nbsp;- 2013&nbsp;years when a lot of ...'
url: /how-to-execute-a-php-pdo-prepared-statement
authorsure_include_css:
  - 
  - 
  - 
syndication_source:
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/how-to-execute-php-pdo-prepared.html
  - http://sag-3.blogspot.com/2013/07/how-to-execute-php-pdo-prepared.html
  - http://sag-3.blogspot.com/2013/07/how-to-execute-php-pdo-prepared.html
  - http://sag-3.blogspot.com/2013/07/how-to-execute-php-pdo-prepared.html
  - http://sag-3.blogspot.com/2013/07/how-to-execute-php-pdo-prepared.html
  - http://sag-3.blogspot.com/2013/07/how-to-execute-php-pdo-prepared.html
syndication_item_hash:
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
  - a7f69398cb74745d935b5fb84f444370
blogger_permalink:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
blogger_blog:
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
categories:
  - PHP
  - SQL
tags:
  - PDO

---
<div dir="ltr" style="text-align: left;">
  <b>PDO </b>&#8211; a method used to execute<b> SQL </b>statements risk free. Fast, secure and easy when compared to <b>mysqli </b>and <b>mysql</b> functions in <b>PHP</b>.<br /> A lot of newbies including me heard about <b>PDO</b> mostly in the <b>2010</b> &#8211; <b>2013</b> years when a lot of sites got hacked of <b>Mysql</b> <b>Injection</b>. <b>PDO</b> is the easiest and safest way to execute an <b>SQL</b> code, complete risk free.<br /> In this post I&#8217;m gonna tell you how to execute a prepared statement in <b>PDO </b>and the common mistakes made by programmers which results an error while executing <b>SQL</b> code.</p> 
  
  <div id="toc_container" class="no_bullets">
    <p class="toc_title">
      Contents
    </p>
    
    <ul class="toc_list">
      <li>
        <a href="#how-to-execute-a-prepared-statement"><span class="toc_number toc_depth_1">1</span> How to execute a prepared statement ?</a>
      </li>
      <li>
        <a href="#common-mistakes"><span class="toc_number toc_depth_1">2</span> Common mistakes</a><ul>
          <li>
            <ul>
              <li>
                <a href="#-right-way"><span class="toc_number toc_depth_3">2.0.1</span>    Right Way:</a>
              </li>
              <li>
                <a href="#-right-way-2"><span class="toc_number toc_depth_3">2.0.2</span>    Right Way:</a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  
  <h1 style="text-align: left;">
    <span id="how-to-execute-a-prepared-statement"><b><span style="font-size: large;">How to execute a prepared statement ?</span></b></span>
  </h1>
  
  <div>
    The following example shows how to execute an <b>INSERT</b> statement with the credentials <b>{database_name:"db",host:"localhost",username:"root",password:"subins",table:"users",fields:"name,age,city"</b><b>}</b>.
  </div>
  
  <pre class="prettyprint"><code>&lt;?php
$name=$_POST['name'];
$age=$_POST['age'];
$city=$_POST['city'];
$db = new PDO('mysql:dbname=&lt;span style="color: red;">db&lt;/span>;host=&lt;span style="color: red;">127.0.0.1&lt;/span>', '&lt;span style="color: red;">root&lt;/span>', '&lt;span style="color: red;">subins&lt;/span>');
$sql=$db-&gt;prepare("INSERT INTO users (name,age,city) VALUES(?,?,?)");
$sql-&gt;execute(array($name,$age,$city));
?&gt;</code></pre>
  
  <h1 style="text-align: left;">
    <span id="common-mistakes"><span style="font-size: large;"><b>Common mistakes</b></span></span>
  </h1>
  
  <div>
    Now the common mistakes made while executing a code like above.
  </div>
  
  <div>
    1. Some will put variables inside <b>execute</b> function instead of putting it in <b>array</b> like below:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      $sql->execute($name,$age,$city);
    </p>
  </blockquote>
  
  <div>
    <h3 style="text-align: left;">
      <span id="-right-way"><span style="color: #6aa84f; font-size: large;"><b>   Right Way:</b></span></span>
    </h3>
  </div>
  
  <blockquote class="tr_bq">
    <p>
      $sql->execute(array($name,$age,$city));
    </p>
  </blockquote>
  
  <div>
    2. The other mistake is calling a function from <b>$db</b> instead of <b>$sql</b> like the following:
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        $sql=$db->prepare("INSERT INTO users (name,age,city) VALUES(?,?,?)");<br /> $db->execute(array($name,$age,$city));
      </p>
    </blockquote>
  </div>
  
  <div>
    <h3 style="text-align: left;">
      <span id="-right-way-2"><b style="color: #6aa84f; font-size: x-large;">   </b><b style="color: #6aa84f;"><span style="font-size: large;">Right Way:</span></b></span>
    </h3>
  </div>
  
  <div>
    <blockquote class="tr_bq">
      <p>
        $sql=$db->prepare("INSERT INTO users (name,age,city) VALUES(?,?,?)");<br /> $sql->execute(array($name,$age,$city));
      </p>
    </blockquote>
  </div>
  
  <div>
  </div>
</div>