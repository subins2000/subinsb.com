---
title: 'Create Your Own URL Shortening Service Using PHP & jQuery'
author: Subin Siby
type: post
date: 2013-09-27T16:30:00+00:00
excerpt: 'I made a URL&nbsp;shortening service a year back using PHP&nbsp;&amp; jQuery. It was pretty simple. You should know the concept of URL&nbsp;Shortening. The concept is simple. When a user sends a request to short a URL, PHP&nbsp;will check if the URL&nb...'
url: /create-your-own-url-shortening-service-using-php-jquery
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
  - http://sag-3.blogspot.com/2013/09/create-own-url-shortening-service-with-php-jquery.html
dsq_thread_id:
  - 1953380469
  - 1953380469
  - 1953380469
  - 1953380469
  - 1953380469
  - 1953380469
syndication_item_hash:
  - 91da3632c9fa29922d7c0f84b9949162
  - 91da3632c9fa29922d7c0f84b9949162
  - 91da3632c9fa29922d7c0f84b9949162
  - 91da3632c9fa29922d7c0f84b9949162
  - 91da3632c9fa29922d7c0f84b9949162
  - 91da3632c9fa29922d7c0f84b9949162
categories:
  - HTML
  - JavaScript
  - jQuery
  - PHP
  - RegEx
tags:
  - error
  - Form
  - Google
  - header
  - String
  - URL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  I made a <b>URL</b>&nbsp;shortening service a year back using <b>PHP</b>&nbsp;& <b>jQuery</b>. It was pretty simple. You should know the concept of <b>URL</b>&nbsp;Shortening. The concept is simple. When a user sends a request to short a <b>URL</b>, <b>PHP</b>&nbsp;will check if the <b>URL</b>&nbsp;is valid. If it&#8217;s valid then <b>PHP</b>&nbsp;will make a string of <b>5</b>&nbsp;or <b>6</b>&nbsp;characters. A folder is created with the name of this string and an <b>index.php</b>&nbsp;file with <b>header</b>&nbsp;<b>function</b>&nbsp;code is created in this folder. This will make the folder redirect to the <b>URL</b>&nbsp;the user requested. Since it&#8217;s a folder no extensions will be seen ! So You can make up some pretty good <b>URL</b>&#8216;s.</p> 
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=az03vqeafry3e8o&class=4" >Download</a> <a class="demo" href="http://demos.subinsb.com/url-shortener/" >Demo</a>
  </div>
  
  <p>
    Here is the form for <b>URL</b>&nbsp;<b>Shortening</b>&nbsp;:
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <form id="subinsbshorter"><br />&nbsp; <center><br />&nbsp; &nbsp;<input type="text" id="url" name="url" autocomplete="off" placeholder="Your URL here&#8230;"/><br/><br />&nbsp; &nbsp;<input type="submit" value="Short URL !"/><br/><br />&nbsp; &nbsp;Type in a <b>URL</b> and click on <b>Short URL !</b> to start shorting.<br />&nbsp; </center><br />&nbsp; <div id="loader" style="display:none;"><br />&nbsp; &nbsp;<center><img src="../cdn/load.gif" /></center><br />&nbsp; </div><br />&nbsp; <div id="successDiv" style="display:none;"><br />&nbsp; &nbsp;<h2 style="color:green;">Shorting Successful</h2><br />&nbsp; &nbsp;Here is Your Shorted URL : <a href=""></a><br />&nbsp; </div><br />&nbsp; <div id="errorDiv" style="display:none;"><br />&nbsp; &nbsp;<h2 style="color:red;">Invalid URL</h2><br />&nbsp; </div><br />&nbsp;</form><br />&nbsp;<style><br />&nbsp; #url{font-family: &#8216;Droid Sans&#8217;, Arial;font-size: 15px;font-weight: normal;border-width: 0px;padding: 5px;color: #999999;width: 405px;outline-style: none; outline-width: 0px;<br />&nbsp;</style>
    </p>
  </blockquote>
  
  <p>
    We also added a <b>loader</b>&nbsp;div, <b>success</b>&nbsp;div and an <b>error</b>&nbsp;div in the <b><form></b>&nbsp;tag. The <b>jQuery</b>&nbsp;code plays a vital role in this process because we are not adding any <b>action</b>&nbsp;attributes in the form, so if <b>jQuery</b>&nbsp;fail to load then the conversion won&#8217;t work. Here is the <b>jQuery </b>code :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <script><br />$(document).ready(function(){<br />&nbsp;function isValidUrl(aUrl){var urlregex=new RegExp("^(http://|https://|ftp://){1}([0-9A-Za-z]+.)");return urlregex.test(aUrl);}<br />&nbsp;function ser(u){$("#errorDiv,#successDiv,#loader").hide();$("#errorDiv h2").text(u);$("#errorDiv").show();}<br />&nbsp;function slo(){$("#errorDiv,#successDiv,#loader").hide();$("#loader").show();}<br />&nbsp;function sss(v,s){$("#errorDiv,#successDiv,#loader").hide();$("#successDiv").show();$("#successDiv a").attr("href",v);$("#successDiv a").html("<blockquote>"+v+"</blockquote>");}<br />&nbsp;$("#subinsbshorter").on(&#8216;submit&#8217;,function(){<br />&nbsp; v=$(this).find("#url").val();<br />&nbsp; slo();<br />&nbsp; if(isValidUrl(v)){<br />&nbsp; &nbsp;$.post(&#8216;add.php&#8217;,$(this).serialize(),function(d){<br />&nbsp; &nbsp;d=JSON.parse(d);<br />&nbsp; &nbsp;if(d.stat==&#8217;1&#8242;){<br />&nbsp; &nbsp; sss(d.url,d.name);<br />&nbsp; &nbsp;}else{<br />&nbsp; &nbsp; ser("Failed To Short");<br />&nbsp; &nbsp;}<br />&nbsp; &nbsp;}).error(function(){ser("Failed To Short");});<br />&nbsp; }else{<br />&nbsp; &nbsp;ser("Invalid URL");<br />&nbsp; }<br />&nbsp; return false;<br />&nbsp;});<br />});<br /></script>
    </p>
  </blockquote>
  
  <p>
    Insert the above code anywhere you like. The <b>jQuery</b>&nbsp;will check if the <b>URL</b>&nbsp;is valid and will&nbsp;send a <b>POST</b>&nbsp;request to <b>add.php</b>&nbsp;when user submits the form. If the <b>URL</b>&nbsp;is not valid, then <b>jQuery</b>&nbsp;will show the <b>error </b>div.&nbsp;Here is the <b>add.php</b>&nbsp;file :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <?<br />header(&#8216;Content-Type: text/html&#8217;);<br />function rantext($length){$chars="abcdefghijklmnopqrstuvwxyzsubinsblogABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789&#8243;;$size=strlen($chars);for($i=0;$i<$length;$i++){$str.=$chars[rand(0,$size-1)];}return $str;}<br />$dir="/urls/";/*Directory to store URL&#8217;s. If main folder use "/" */<br />$url=$_POST[&#8216;url&#8217;];<br />if(!preg_match("/^(http://|https://|ftp://){1}([0-9A-Za-z]+.)/",$url)){<br />&nbsp;die(&#8216;{"stat":"0&#8243;}&#8217;);<br />}<br />$name = rantext(5);<br />if (file_exists($name)){<br />&nbsp;$name = rantext(6);<br />}else{<br />&nbsp;mkdir(&#8216;.&#8217;.$dir.$name);<br />&nbsp;$subinsbshort = fopen(&#8216;.&#8217;.$dir.$name.&#8217;/index.php&#8217;, &#8216;x&#8217;) or die(&#8216;{"stat":"0&#8243;}&#8217;);<br />}<br />if(fwrite($subinsbshort,'<?header("Location:&#8217;.$url.'");?>&#8217;)){<br />&nbsp;echo &#8216;{"stat":"1&#8243;,"url":"//demos.subinsb.com/url-shortener&#8217;.$dir.$name.'","name":"&#8216;.$name.'"}&#8217;;<br />}else{<br />&nbsp;die(&#8216;{"stat":"0&#8243;}&#8217;);<br />}<br />fclose($subinsbshort);<br />?>
    </p>
  </blockquote>
  
  <p>
    We also added the <b>Random String Function</b>&nbsp;to the <b>add.php</b>&nbsp;available from <a href="http://www.subinsb.com/2013/08/generating-random-text-in-php.html" >here</a>. The <b>Function</b>&nbsp;will generate a string of <b>5</b>&nbsp;characters. If the string created is already used, then the <b>script</b>&nbsp;will generate another string of <b>6</b>&nbsp;characters. Then the <b>add.php</b>&nbsp;will create a folder with the name of random string. The <b>add.php</b>&nbsp;returns a <b>JSON</b>&nbsp;data to <b>jQuery</b>. <b>jQuery </b>will process the data and if everything is <b>OK</b>, then <b>jQuery</b>&nbsp;will show a success message with the new shorted <b>URL</b>.<br />If you have any suggestions/problems/feedback say it out in the comments, I will help you.<br />And <b>Happy Birthday Google</b>.</div>