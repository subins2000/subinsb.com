---
title: My Short URL – a site for shortening URL’s
author: Subin Siby
type: post
date: 2013-01-25T17:40:00+00:00
excerpt: "My Short URL&nbsp;is my latest invention. A site for shortening URL's like goo.gl&nbsp;and&nbsp;bit.ly.You can short URL's on the site MSURL.TK.As other projects the site is also hosted by AppFog&nbsp;and the free domain is given by My.Dot.TK.The proje..."
url: /my-short-url-a-site-for-shortening-urls
authorsure_include_css:
  - 
  - 
  - 
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_item_hash:
  - 0ef14a4dc1e9fd85e32904b760f222fc
  - 0ef14a4dc1e9fd85e32904b760f222fc
  - 0ef14a4dc1e9fd85e32904b760f222fc
  - 0ef14a4dc1e9fd85e32904b760f222fc
  - 0ef14a4dc1e9fd85e32904b760f222fc
  - 0ef14a4dc1e9fd85e32904b760f222fc
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
  - http://sag-3.blogspot.com/2013/01/msurl-site-for-shortening-URL.html
dsq_thread_id:
  - 1961774979
  - 1961774979
  - 1961774979
  - 1961774979
  - 1961774979
  - 1961774979
categories:
  - PHP
tags:
  - AppFog
  - Bit.ly
  - Google
  - My Short URL
  - My.Dot.Tk
  - Subins
  - URL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>My Short URL</b>&nbsp;is my latest invention. A site for shortening URL&#8217;s like <b>goo.gl</b>&nbsp;and<b>&nbsp;bit.ly</b>.</p> 
  
  <div>
    </p> 
    
    <div>
      You can short URL&#8217;s on the site <a href="http://msurl.tk/" style="font-weight: bold;">MSURL.TK</a>.
    </div>
    
    <div>
    </div>
  </div>
  
  <div>
    As other projects the site is also hosted by <b><a href="http://appfog.com/">AppFog</a></b>&nbsp;and the free domain is given by <a href="http://my.dot.tk/" style="font-weight: bold;">My.Dot.TK</a>.
  </div>
  
  <div>
    The project was created under <b>30 minutes</b>&nbsp;and the lines of code is less that <b>64</b>.
  </div>
  
  <div>
    It&#8217;s written in&nbsp;<b>PHP</b>&nbsp;as you already know it.&nbsp;
  </div>
  
  <div>
    The shortened <b>URL</b>&nbsp;only has <b>13</b>&nbsp;characters (without <b>HTTP://</b>).
  </div>
  
  <div>
    The shortened <b>URL</b>&nbsp;will not have <b>.php</b>&nbsp;extension. I will tell you how to view page without file extension on the next tutorial.
  </div>
  
  <div>
  </div>
  
  <div>
    Enjoy Shortening URL !
  </div>
</div>