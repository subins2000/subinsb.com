---
title: 'Create Text Editor like Blogger Using HTML & jQuery'
author: Subin Siby
type: post
date: 2012-10-24T16:23:00+00:00
excerpt: 'You might have seen text editors where you can bold a text, italic a text etc... The perfect example of this the Blogger&nbsp;Post Text Editor.&nbsp;This is a simple tutorial that will create a text editor using HTML, Jquery &amp; JavaScript.Download D...'
url: /create-text-editor-using-html-jquery-like-in-blogger
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
  - http://sag-3.blogspot.com/2012/10/text-editor-using-javascript-jquery.html
dsq_thread_id:
  - 1959649134
  - 1959649134
  - 1959649134
  - 1959649134
  - 1959649134
  - 1959649134
syndication_item_hash:
  - 483c11c8947e25ad4e2a248bffa09273
  - 483c11c8947e25ad4e2a248bffa09273
  - 483c11c8947e25ad4e2a248bffa09273
  - 483c11c8947e25ad4e2a248bffa09273
  - 483c11c8947e25ad4e2a248bffa09273
  - 483c11c8947e25ad4e2a248bffa09273
categories:
  - Blogger
  - HTML
  - JavaScript
  - jQuery
tags:
  - URL

---
<div dir="ltr" style="text-align: left;">
  <p>
    <span style="font-family: inherit;">You might have seen text editors where you can bold a text, italic a text etc&#8230; The perfect example of this the <b>Blogger</b> Post <b>Text Editor</b>. </span>
  </p>
  
  <div>
    <span style="font-family: inherit;">This is a simple tutorial that will create a text editor using <b>HTML, Jquery & JavaScript.</b></span>
  </div>
  
  <div class="padlinks">
    <a class="download" href="http://demos.subinsb.com/down.php?id=7rdxkdd9wmmd383&class=5">Download</a> <a class="demo" href="http://demos.subinsb.com/text-editor-jquery/">Demo</a>
  </div>
  
  <div style="text-align: left;">
    <p>
      We have two files, <b>index.html</b> which contains the <b>HTML</b> code and <b>texteditor.js</b> which contains the <b>jQuery</b> code. You should include the <b>texteditor.js</b> in <b>index.html</b>.<b><span style="font-family: inherit; font-size: large;">index.html</span></b>
    </p>
  </div>
  
  <div style="text-align: left;">
    <pre class="prettyprint"><code>&lt;span style="font-family: inherit;">&lt;!DOCTYPE html&gt;&lt;/span>&lt;span style="font-family: inherit;">&lt;html&gt;&lt;/span>
&lt;span style="font-family: inherit;"> &lt;head&gt;&lt;/span>
&lt;span style="font-family: inherit;">  &lt;script src="http://code.jquery.com/jquery-1.8.0.min.js"&gt;&lt;/script&gt;&lt;/span>
&lt;span style="font-family: inherit;">  &lt;script src="texteditor.js"&gt;&lt;/script&gt;&lt;/span>
&lt;span style="font-family: inherit;"> &lt;/head&gt;&lt;/span>
&lt;span style="font-family: inherit;"> &lt;body&gt;&lt;/span>
&lt;span style="font-family: inherit;">  &lt;center style="margin-bottom:20px;"&gt;&lt;/span>
&lt;span style="font-family: inherit;">   &lt;div class="ze ie"&gt;&lt;/div&gt;&lt;/span>
&lt;span style="font-family: inherit;">   &lt;style&gt;&lt;/span>
&lt;span style="font-family: inherit;">   .font-bold.bold{font-weight:bold;}.italic{font-style:italic;}.selected{background-color: orange;}#openpb{margin:15px;}&lt;/span>
&lt;span style="font-family: inherit;">   &lt;/style&gt;&lt;/span>
&lt;span style="font-family: inherit;">   &lt;button type="button" class="g-button g-button-submit" id='stext'&gt;Text&lt;/button&gt;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span>
&lt;span style="font-family: inherit;">   &lt;button type="button" class="g-button g-button-submit" id='shtml'&gt;HTML&lt;/button&gt;&lt;/span>
&lt;span style="font-family: inherit;">   &lt;div id="controls" style="margin-bottom: 10px;"&gt;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;a id="bold" style="color:black;display: inline-block;" class="font-bold"&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;button type="button"&gt;B&lt;/button&gt;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;/a&gt;&nbsp;&nbsp;&nbsp;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;a id="italic" style="color:black !important;display: inline-block;"class="italic"&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;button type="button"&gt;I&lt;/button&gt;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;/a&gt;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;a id="link" class="link" style="display: inline-block;"&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;button type="button"&gt;Link&lt;/button&gt;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;/a&gt;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;select id="fonts" class="g-button"&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Normal"&gt;Normal&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Arial"&gt;Arial&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Comic Sans MS"&gt;Comic Sans MS&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Courier New"&gt;Courier New&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Monotype Corsiva"&gt;Monotype&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Tahoma New"&gt;Tahoma&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Times"&gt;Times&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Trebuchet New"&gt;Trebuchet&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">     &lt;option value="Ubuntu"&gt;Ubuntu&lt;/option&gt;&lt;/span>
&lt;span style="font-family: inherit;">    &lt;/select&gt;&lt;/span>
&lt;span style="font-family: inherit;">   &lt;/div&gt;&lt;/span>
&lt;span style="font-family: inherit;">   &lt;iframe frameborder="0" id="textEditor" style="width:500px; height:80px;border:2px solid #CCC;border-radius:20px;overflow:auto;"&gt;&lt;/iframe&gt;&lt;/span>
&lt;span style="font-family: inherit;">  &lt;/div&gt;&lt;/span>
&lt;span style="font-family: inherit;">  &lt;textarea name="text" id='text' style="border-radius:20px;overflow:auto;display:none;padding-left: 10px;" rows="6" cols="53"&gt;&lt;/textarea&gt;&lt;/span>
&lt;span style="font-family: inherit;"> &lt;/center&gt;&lt;/span>
&lt;span style="font-family: inherit;"> &lt;/body&gt;&lt;/span>
&lt;span style="font-family: inherit;">&lt;/html&gt;&lt;/span></code></pre>
    
    <p>
      <span style="font-family: inherit; font-size: large;"><b>texteditor.js</b></span>
    </p>
  </div>
  
  <pre class="prettyprint"><code>&lt;span style="font-family: inherit;">$(document).ready(function(){&lt;/span>
&lt;span style="font-family: inherit;"> document.getElementById('textEditor').contentWindow.document.designMode="on";&lt;/span>
&lt;span style="font-family: inherit;"> document.getElementById('textEditor').contentWindow.document.close();&lt;/span>
&lt;span style="font-family: inherit;"> var edit = document.getElementById("textEditor").contentWindow;&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;"> $("#bold").click(function(){&lt;/span>
&lt;span style="font-family: inherit;">  if($(this).hasClass("selected")){&lt;/span>
&lt;span style="font-family: inherit;">   $(this).removeClass("selected");&lt;/span>
&lt;span style="font-family: inherit;">  }else{&lt;/span>
&lt;span style="font-family: inherit;">   $(this).addClass("selected");&lt;/span>
&lt;span style="font-family: inherit;">  }&lt;/span>
&lt;span style="font-family: inherit;">  boldIt();&lt;/span>
&lt;span style="font-family: inherit;"> });&lt;/span>
&lt;span style="font-family: inherit;"> $("#italic").click(function(){&lt;/span>
&lt;span style="font-family: inherit;">  if($(this).hasClass("selected")){&lt;/span>
&lt;span style="font-family: inherit;">   $(this).removeClass("selected");&lt;/span>
&lt;span style="font-family: inherit;">  }else{&lt;/span>
&lt;span style="font-family: inherit;">   $(this).addClass("selected");&lt;/span>
&lt;span style="font-family: inherit;">  }&lt;/span>
&lt;span style="font-family: inherit;">  ItalicIt();&lt;/span>
&lt;span style="font-family: inherit;"> });&lt;/span>
&lt;span style="font-family: inherit;"> $("#fonts").on('change',function(){&lt;/span>
&lt;span style="font-family: inherit;">  changeFont($("#fonts").val());&lt;/span>
&lt;span style="font-family: inherit;"> });&lt;/span>
&lt;span style="font-family: inherit;"> $("#link").click(function(){&lt;/span>
&lt;span style="font-family: inherit;">  var urlp=prompt("What is the link:","http://");&lt;/span>
&lt;span style="font-family: inherit;">  url(urlp);&lt;/span>
&lt;span style="font-family: inherit;"> }); &lt;/span>
&lt;span style="font-family: inherit;"> $("#stext").click(function(){&lt;/span>
&lt;span style="font-family: inherit;">  $("#text").hide();&lt;/span>
&lt;span style="font-family: inherit;">  $("#textEditor").show();&lt;/span>
&lt;span style="font-family: inherit;">  $("#controls").show()&lt;/span>
&lt;span style="font-family: inherit;"> });&lt;/span>
&lt;span style="font-family: inherit;"> $("#shtml").on('click',function(){&lt;/span>
&lt;span style="font-family: inherit;">  $("#text").css("display","block");&lt;/span>
&lt;span style="font-family: inherit;">  $("#textEditor").hide();&lt;/span>
&lt;span style="font-family: inherit;">  $("#controls").hide();&lt;/span>
&lt;span style="font-family: inherit;"> });&lt;/span>
&lt;span style="font-family: inherit;">});&lt;/span>
&lt;span style="font-family: inherit;">function boldIt(){&lt;/span>
&lt;span style="font-family: inherit;"> var edit = document.getElementById("textEditor").contentWindow;&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;">  edit.document.execCommand("bold", false, "");&lt;/span>
&lt;span style="font-family: inherit;">  edit.focus();&lt;/span>
&lt;span style="font-family: inherit;">}&lt;/span>
&lt;span style="font-family: inherit;">function ItalicIt(){&lt;/span>
&lt;span style="font-family: inherit;"> var edit = document.getElementById("textEditor").contentWindow;&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;"> edit.document.execCommand("italic", false, "");&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;">}&lt;/span>
&lt;span style="font-family: inherit;">function changeFont(font){&lt;/span>
&lt;span style="font-family: inherit;"> var edit = document.getElementById("textEditor").contentWindow;&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;"> edit.document.execCommand("FontName", false, font);&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;">}&lt;/span>
&lt;span style="font-family: inherit;">function url(url){&lt;/span>
&lt;span style="font-family: inherit;"> var edit = document.getElementById("textEditor").contentWindow;&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;"> edit.document.execCommand("Createlink", false, url);&lt;/span>
&lt;span style="font-family: inherit;"> edit.focus();&lt;/span>
&lt;span style="font-family: inherit;">}&lt;/span>
&lt;span style="font-family: inherit;">setInterval(function(){&lt;/span>
&lt;span style="font-family: inherit;"> var gyt=$("#textEditor").contents().find("body").html().match(/@/g);&lt;/span>
&lt;span style="font-family: inherit;"> if($("#textEditor").contents().find("body").html().match(/@/g)&gt;=0){}else{&lt;/span>
&lt;span style="font-family: inherit;">  $("#text").val($("#textEditor").contents().find("body").html());&lt;/span>
&lt;span style="font-family: inherit;"> }&lt;/span>
&lt;span style="font-family: inherit;"> $("#text").val($("#textEditor").contents().find("body").html());&lt;/span>
&lt;span style="font-family: inherit;">},1000);&lt;/span></code></pre>
  
  <div style="text-align: left;">
    <span class="js-variable" style="font-family: inherit;">The user will type text in the <b>iframe</b>. The <b>HTML</b> of <b>iframe</b> will be added in the <b>textarea</b>. So when the user clicks <b>HTML</b> button he sees the <b>HTML</b> of <b>iframe</b>. You can pass the <b>textarea</b> value to your database if needed, but use <b>htmlspecialchars</b> when passing in <b>PHP</b>, because the user can inject <b>XSS</b> codes.</span>
  </div>
</div>