---
title: Login with Google OAuth without using Google PHP Library
author: Subin Siby
type: post
date: 2013-04-25T04:50:00+00:00
excerpt: "Google PHP Library is very hard to use and is very long. It wasted a lot of my time. So I decided to do it manually.I created a new library for Google OAuth in&nbsp;PHP.Download DemoIt is very simple to use and you don't have to worry about a thing.FAQ..."
url: /login-with-google-oauth-without-using-google-php-library
authorsure_include_css:
  - 
  - 
  - 
syndication_source:
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/04/login-with-google-oauth-without-using.html
  - http://sag-3.blogspot.com/2013/04/login-with-google-oauth-without-using.html
  - http://sag-3.blogspot.com/2013/04/login-with-google-oauth-without-using.html
  - http://sag-3.blogspot.com/2013/04/login-with-google-oauth-without-using.html
  - http://sag-3.blogspot.com/2013/04/login-with-google-oauth-without-using.html
  - http://sag-3.blogspot.com/2013/04/login-with-google-oauth-without-using.html
syndication_item_hash:
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
  - 7c70135a98f023bc362ac4a54d98f864
blogger_permalink:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
blogger_blog:
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
categories:
  - PHP
tags:
  - API
  - Google

---
<div dir="ltr" style="text-align: left;">
  <b>Google PHP</b> Library is very hard to use and is very long. It wasted a lot of my time. So I decided to do it manually.<br /> I created a new library for <b>Google OAuth</b> in <b>PHP</b>.</p> 
  
  <div class="padlinks">
    <a class="download" href="http://get-subins.hp.af.cm/view.php?id=341">Download</a> <a class="demo" href="http://demos.subinsb.com/google-php-lib-simple/">Demo</a>
  </div>
  
  <p>
    It is very simple to use and you don&#8217;t have to worry about a thing.
  </p>
  
  <h1>
    <b><span style="font-size: x-large;">FAQ</span></b>
  </h1>
  
  <div style="text-align: left;">
    <b><span style="font-size: large;">How to get a client Id and Client Secret ?</span></b>
  </div>
  
  <p>
    1) Go to <b><a href="https://code.google.com/apis/console">https://code.google.com/apis/console</a></b> and create a new project.<br /> 2) Fill up the form.<br /> 3) In the redirect URL&#8217;s type in the URL of the index.php in your site.<br /> 4) Add your site URL as trusted Origin.<br /> You will now get a client Id and a Client Secret Id. Type both of those in each <b>$client_id</b> and <b>$client_secret</b> variable of Configuration section in <b>index.php</b> file.<span style="font-size: large;"><br /> </span>
  </p>
  
  <h1 style="text-align: left;">
    <b><span style="font-size: large;">What should I type in $redirect_url variable ?</span></b>
  </h1>
  
  <p>
    Open the page index.php in your browser and you will be redirected to the Google Oauth site and when the user authorised your app they will reach the <b>redirect_url</b> you specified in index.php configuration section.
  </p>
  
  <h1 style="text-align: left;">
    <b><span style="font-size: large;">Where can I get the list of scopes ?</span></b>
  </h1>
  
  <p>
    I have made a blog post containing the list of scopes. View the post @ <a href="http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html">http://sag-3.blogspot.com/2013/04/list-google-oauth-scopes.html</a>
  </p>
</div>