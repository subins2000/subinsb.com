---
title: "Kuttanad Flood"
author: Subin Siby
type: post
draft: true
#date: 2018-09-09T01:47:19+05:30
date: 2019-05-04T12:00:00+05:30
url: /kuttanad-flood
---

My home state Kerala was affected with a flood in the month of August. Kerala is blessed with a monsoon every year. But this year, the rains were huge. On August 14, 15 & 16, rain fell continuously. This caused all rivers in Kerala to overflow and many dams were opened further escalating the water level.

Thirty-five out of the fifty-four dams within the state were opened for the first time in history. All five overflow gates of the Idukki Dam were opened at the same time, for the first time in 26 years. [Read More](https://en.wikipedia.org/wiki/2018_Kerala_floods).

## Kuttanad

[Kuttanad](https://en.wikipedia.org/wiki/Kuttanad) is a special region in the Kerala district of Alappuzha. Kuttanad is known for its beautiful backwaters and natural beauty. Kuttanad is a very large area having a population of more than **150,000**.

4 rivers join the [Vembanad Lake](https://en.wikipedia.org/wiki/Vembanad) here. Every monsoon, Kuttanad gets flooded. But this year was the worst. Kuttanad got flooded 3 times and the last one in this August was the **worst ever in history**.

Almost 97% of the people in Kuttanad were evacuated and moved to relief camps.

## Volunteering

After the flood, volunteers from all over India came to help Kuttanad. There's an organization called ["Canalpy"](https://www.canalpy.com/) whose aim is to reclaim the canals of Alappuzha town. Because of the flood, they took the handling of the rehabilitation process. Canalpy is an initiative between IIT-Bombay, KILA & Kerala government. My cousin brother [Nithin](https://instagram.com/troubled_soul) was a research assistant at Canalpy.

It was decided that on August 28, 29 & 30 a massive Kuttanad Rehabilitation task would be done. The news was spread through social media, newspapers. I came to know of it and decided to go.

My dad (appa) is actually a native of Kuttanadu. He lived there for more than 30 years before moving out. I've been to Kuttanadu, but never really knew the place or lived there.

Appa used to tell me those stories of living in Kuttanad, how it was hard, the frequent floods etc. So I was curious about it. So this volunteering was actually a way to discover my roots.

Appa didn't like the idea of me going. But since I had Nithin with me and my uncles there, he was okay. Besides, univeristy was giving grace marks for volunteers. That made both my parents willing to let me go (The things parents do for marks).

## Teams

Volunteers came from all over Kerala. There were two tasks, one was to help in the cleaning process and the other to take survey of the damages caused.

I ended up in the survey team. People were grouped and taught how to do the survey. Our task is to go to houses, ask for details, take photographs of the damages.

### Technical

The survery was done using [ODKCollect](https://opendatakit.org/). ODK basically has a questionnaire to fill out. ODK has GPS support, so each location was exactly tracked. The data can be collected offline and later when internet is available, it can be uploaded.

Our group had engineering students from different colleges in Kerala and some professionals.