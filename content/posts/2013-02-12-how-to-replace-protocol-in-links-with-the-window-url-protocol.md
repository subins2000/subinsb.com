---
title: How to replace protocol in links with the window URL protocol
author: Subin Siby
type: post
date: 2013-02-12T17:22:00+00:00
excerpt: 'The best way to this is by replacing every HTTP &amp; HTTPS&nbsp;in the HTML&nbsp;file&nbsp;with //. For Example:&lt;a href="http://subins.tk"&gt;Subins&lt;/a&gt;With :&lt;a href="//subins.tk"&gt;Subins&lt;/a&gt;If the location protocol is HTTPS &nbsp;...'
url: /how-to-replace-protocol-in-links-with-the-window-url-protocol
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
syndication_item_hash:
  - b7c07b95e4f47061b92c33030df8bd03
  - b7c07b95e4f47061b92c33030df8bd03
  - b7c07b95e4f47061b92c33030df8bd03
  - b7c07b95e4f47061b92c33030df8bd03
  - b7c07b95e4f47061b92c33030df8bd03
  - b7c07b95e4f47061b92c33030df8bd03
blogger_permalink:
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
  - http://sag-3.blogspot.com/2013/02/how-to-replace-protocol-in-links-with.html
categories:
  - HTML
tags:
  - HTTP
  - HTTPS
  - Protocol

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  The best way to this is by replacing every <b>HTTP </b>& <b>HTTPS</b>&nbsp;in the <b>HTML</b>&nbsp;<b>file</b>&nbsp;with <b>//</b>. For Example:</p> 
  
  <blockquote class="tr_bq">
    <p>
      <a href="http://subins.tk">Subins</a>
    </p>
  </blockquote>
  
  <p>
    With :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      <a href="//subins.tk">Subins</a>
    </p>
  </blockquote>
  
  <p>
    If the location protocol is <b>HTTPS </b>&nbsp;all links&#8217; protocol will be changed to<b>&nbsp;HTTPS</b>&nbsp;and&nbsp;If the location protocol is&nbsp;<b>HTTP&nbsp;</b>&nbsp;all links&#8217; protocol will be changed to<b>&nbsp;HTTP</b>.
  </p>
  
  <p>
    For a <b>mass replacement </b>of links USE <b><a href="http://adf.ly/J9Rdm" >Regexxer</a>.</b></div>