---
title: Minify JavaScript Using PHP
author: Subin Siby
type: post
date: 2014-06-07T05:34:46+00:00
url: /php-minify-js-code
authorsure_include_css:
  - 
  - 
  - 
categories:
  - JavaScript
  - PHP
tags:
  - Class
  - Compress
  - Function

---
Making **JavaScript** codes is difficult and long. In Web projects, we try our maximum to minify JavaScript and **CSS** to serve content better. There are many sites on the Web that helps to compress/minify JS code. There is even a PHP Software to do this. In this post, I&#8217;ll tell you how to use the PHP **jSqueeze** library to minify JavaScript code.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=b5wg3mjz3ba1/s/1xmre7m5wwys7lms6651&class=28" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/php/jsMin" target="_blank">Demo</a>
</div>

## Setting Up

<del>You can get the class code of <strong>JSqueeze</strong> from <a href="https://github.com/nicolas-grekas/JSqueeze" target="_blank">here</a>. Paste the code into a file called "min-js.php". We will include this file to compress our code.</del>

The author of **JSqueeze** has made it a Composer Package. You can install it by :

<pre class="prettyprint"><code>composer require patchwork/jsqueeze&lt;br /></code></pre>

## Compress Code

The **JSqueeze** class is under **Patchwork** namspace. We should first make the object :

<pre class="prettyprint"><code>&lt;?php
use Patchwork\JSqueeze;
$jSqueeze = new JSqueeze();&lt;br /></code></pre>

We will now add the JavaScript code into a variable called "$jsCode" :

<pre class="prettyprint"><code>$jsCode = "function lop(number){
  alert(number + 2);
}
lop(2);";</code></pre>

Or you can include the POST form data in to the variable :

<pre class="prettyprint"><code>$jsCode = $_POST["code"];</code></pre>

Now, we ask the class to compress the code by calling the **$JSqueeze->squeeze()** function and include it in the "$minified" variable :

<pre class="prettyprint"><code>$minified = $jSqueeze-&gt;squeeze($jsCode, true, false);</code></pre>

Now, when you echo the "$minified" variable, you will get the minified code which will be something like this :

<pre class="prettyprint"><code>;function lop(o){alert(o+2)};lop(2);</code></pre>

As you can see, it&#8217;s shorter than the original code. This is how compression works. It changed the variables used to shorter ones and used less semi colons.