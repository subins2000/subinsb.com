---
title: Get a work email on Blogger
author: Subin Siby
type: post
date: 2012-03-09T15:04:00+00:00
excerpt: 'You must have heard of work email.Do you want a work email for your blog without creating an another gmail account???If you want then follow these steps.1st step : Go to your Blogger -&gt; Settings -&gt; Mobile and email&nbsp;You will see a box like th...'
url: /get-a-work-email-on-blogger
authorsure_include_css:
  - 
  - 
  - 
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_item_hash:
  - c4c1c54989c1fa3b49dddf917819d7dd
  - c4c1c54989c1fa3b49dddf917819d7dd
  - c4c1c54989c1fa3b49dddf917819d7dd
  - c4c1c54989c1fa3b49dddf917819d7dd
  - c4c1c54989c1fa3b49dddf917819d7dd
  - c4c1c54989c1fa3b49dddf917819d7dd
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
  - http://sag-3.blogspot.com/2012/03/get-work-email-on-blogger.html
categories:
  - Blogger
tags:
  - Tutorials

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You must have heard of work email.<br />Do you want a work email for your blog without creating an another gmail account???<br />If you want then follow these steps.</p> 
  
  <p>
    1st step : Go to your <b>Blogger -> Settings -> Mobile and email&nbsp;</b><br /><b><br /></b>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <p>
    You will see a box like this<br /><a href="//1.bp.blogspot.com/-2dxOwrTBKJ8/T1oYqqwuC9I/AAAAAAAAAlM/ya8WdTW8k6I/s1600/Screenshot-6.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em; text-align: center;"><img border="0" src="//1.bp.blogspot.com/-2dxOwrTBKJ8/T1oYqqwuC9I/AAAAAAAAAlM/ya8WdTW8k6I/s1600/Screenshot-6.png" /></a><br /><b>Type the word you want on the text box.</b><br /><b>Then click on the radio button on the left side of "<span style="background-color: #fff9e7; color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: -webkit-auto;">Save emails as draft post</span><span style="background-color: #fff9e7; color: #222222; font-family: Arial, Helvetica, sans-serif; font-size: 13px; text-align: -webkit-auto;">&nbsp;"</span></b>
  </p>
  
  <div class="separator" style="clear: both; text-align: center;">
  </div>
  
  <div style="text-align: -webkit-auto;">
    <span style="color: #222222; font-family: Arial, Helvetica, sans-serif;">After this click on&nbsp;</span><a href="//4.bp.blogspot.com/-3D9C-9KMWDY/T1obA7vJUwI/AAAAAAAAAlU/mUDaGAM7XQw/s1600/Screenshot-7.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em; text-align: center;"><img border="0" src="//4.bp.blogspot.com/-3D9C-9KMWDY/T1obA7vJUwI/AAAAAAAAAlU/mUDaGAM7XQw/s1600/Screenshot-7.png" /></a>&nbsp;button
  </div>
  
  <div style="text-align: -webkit-auto;">
  </div>
  
  <div style="text-align: -webkit-auto;">
    Finished !!!!&nbsp;
  </div>
  
  <div style="text-align: -webkit-auto;">
    <b>All the mails sent to the mail id you created will be on the "drafts" option in "Posts" menu</b>
  </div>
</div>