---
title: 16 Years of Revolving Around The Sun
author: Subin Siby
type: post
date: 2016-01-21T02:52:55+00:00
url: /16-years-of-revolving-around-the-sun
categories:
  - Personal

---
So I revolved around the sun 16 years. Yes, I turned 16. It seems like I just turned 15 yesterday.

As age increases, life becomes harder. This direct proportionality is making me closer to the answer of life. This post is an analysis of myself of what I did and what I plan to do.

"Birthdays are a constant reminder of how I wasted time"

## Past 1 Year As A Student

I got full A+ in the 10th grade exam and is now in 11th grade as a C.S student. Got a whole lot of new friends.

There is this one friend who is passionate about C.S and crazy about I.T. He is <a href="http://vishnumc7.blogspot.in/" target="_blank">Vishnu M.C</a> &#8211; a blogger who does it via a 3 year old smartphone. It is hard to do blogging by phone. I have no idea how he manages it. He totally flunk studies, does work on blog.

I have a wish to do what he is doing now. But, as my parents say "You are nothing without a qualification". Damn this qualification thing. I just wanna work with Open Source.

Here are some pics from my current Higher Secondary days :<figure class="wp-caption aligncenter">

[<img class="" src="https://scontent.fdel1-1.fna.fbcdn.net/hphotos-xla1/t31.0-8/fr/cp0/e15/q65/12513843_937885886304750_9138160489355675971_o.jpg" alt="" width="2048" height="1578" />][1]<figcaption class="wp-caption-text">From Left : Rejohn, Navaneeth, Vishnu, Delvin, Me, Vikas, Mobin</figcaption></figure> <figure class="wp-caption aligncenter">[<img class="" src="https://scontent-hkg3-1.xx.fbcdn.net/hphotos-xta1/v/t1.0-9/fr/cp0/e15/q65/12088091_896293280464011_8881500342318265372_n.jpg?efg=eyJpIjoidCJ9&oh=597749eec82d73ef5c00ffac35e6bcb3&oe=573E96E5" alt="" width="720" height="960" />][2]<figcaption class="wp-caption-text">From Back Left : Akshay, Godson, Abin, Me, Rejohn and the one who is taking the selfie is Kailas</figcaption></figure> 

It is these pictures that would be make me nostalgic 10 years later.

## Past 1 Year As A Developer

I just released <a href="https://lobby.subinsb.com" target="_blank">Lobby</a> today. After 2 years of continuous development, it has reached a STABLE phase.

The amount of posts written on blog was so low. Due to the time wasting school, I don&#8217;t have time for fun things.

The amount of commits on GitHub has increased and I&#8217;m planning on keeping that long streak of commits :<figure class="wp-caption aligncenter">

<img class="" src="//imgur.com/Y2yV9Qtl.png" alt="" width="640" height="254" /><figcaption class="wp-caption-text">After January 2, 2016, commits have increased</figcaption></figure> 

I have a lot of hope on Lobby. It&#8217;s one of a kind and I hope you will help it become a success. 🙂

 [1]: https://scontent.fdel1-1.fna.fbcdn.net/hphotos-xla1/t31.0-8/fr/cp0/e15/q65/12513843_937885886304750_9138160489355675971_o.jpg
 [2]: https://scontent-hkg3-1.xx.fbcdn.net/hphotos-xta1/v/t1.0-9/fr/cp0/e15/q65/12088091_896293280464011_8881500342318265372_n.jpg?efg=eyJpIjoidCJ9&oh=597749eec82d73ef5c00ffac35e6bcb3&oe=573E96E5