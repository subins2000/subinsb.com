---
title: Make Headers Jump Links In WordPress Posts
author: Subin Siby
type: post
date: 2016-11-22T12:24:57+00:00
url: /make-wordpress-post-headers-jump-links
categories:
  - Content Management System
  - HTML
  - PHP
  - WordPress
tags:
  - Hack
  - Posts
  - RegEx
  - URL

---
You might have noticed that the <h2>, <h3> headers in my blog posts are links that can be used to jump to that section. These links are called **Jump Links**.

These **Jump Links** can be sent anywhere on web for reading that particular section. This is very useful for tutorials as when someone comments a problem, we can use the link to redirect that user to the section where the solution is situated.

<div class="padlinks">
  <a class="demo" href="#what-it-does">Demo</a>
</div>

I&#8217;ll show you how to automatically make all headers Jump Links in WordPress.

  * Go to **WordPress Admin** -> **Appearance** -> **Editor**. This will bring you to Theme Editor.
  * Choose "Theme Functions (functions.php)" file from the list on the right.

At the end of that file, add this :

<pre class="prettyprint"><code>/**
 * Add id attribute to &lt;h1&gt;, &lt;h2&gt;, &lt;h3&gt; tags
 * http://subinsb.com/make-wordpress-post-headers-jump-links
 */
function anchor_content_headings($content) {
  $content = preg_replace_callback("/\&lt;h([1|2|3])\&gt;(.*?)\&lt;\/h([1|2|3])\&gt;/", function ($matches) {
    $hTag = $matches[1];
    $title = $matches[2];
    $slug = sanitize_title_with_dashes($title);
    return '&lt;a href="#'. $slug .'"&gt;&lt;h'. $hTag .' id="' . $slug . '"&gt;' . $title . '&lt;/h'. $hTag .'&gt;&lt;/a&gt;';
  }, $content);
  return $content;
}
add_filter('the_content', 'anchor_content_headings');</code></pre>

## What It Does

This is the HTML of the headers that are normally outputted in your post :

<pre class="prettyprint"><code>&lt;h1&gt;Hello World&lt;/h1&gt;
&lt;h2&gt;Who ???&lt;/h2&gt;
&lt;h3&gt;It's Me&lt;/h3&gt;</code></pre>

The code you just added will make them into this :

<pre class="prettyprint"><code>&lt;a href="#hello-world"&gt;&lt;h1 id="hello-world"&gt;Hello World&lt;/h1&gt;&lt;/a&gt;
&lt;a href="#who"&gt;&lt;h2 id="who"&gt;Who ???&lt;/h2&gt;
&lt;a href="#its-me"&gt;&lt;h3 id="its-me"&gt;It's Me&lt;/h3&gt;
</code></pre>

What it means is that if you go to this URL :

<pre class="prettyprint"><code>http://myblog.com/my-post#hello-world
</code></pre>

You will be shown the section that starts from Hello World heading.

## Hacks

You can modify the script to also include **<h4>** and **<h5>** as well. You can do this by replacing the "preg\_replace\_callback" line with this :

<pre class="prettyprint"><code>$content = preg_replace_callback("/\&lt;h([1|2|3|4|5])\&gt;(.*?)\&lt;\/h([1|2|3|4|5])\&gt;/", function ($matches) {</code></pre>

You may want to add a prefix or suffix to the IDs of headers. You can do this by modifying **$slug** variable :

<pre class="prettyprint"><code>$slug = "section-" . sanitize_title_with_dashes($title);
</code></pre>

This will make the URLs :

<pre class="prettyprint"><code>http://myblog.com/my-post#section-hello-world</code></pre>