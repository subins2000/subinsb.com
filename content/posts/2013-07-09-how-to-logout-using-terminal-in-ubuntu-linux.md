---
title: How To Log Out Using Terminal in Ubuntu Linux
author: Subin Siby
type: post
date: 2013-07-08T22:00:00+00:00
excerpt: 'There is no specific command to logout&nbsp;in an ubuntu&nbsp;system. You can only do it with a help of multiple commands. The commands are ps and&nbsp;kill.First of all Open Terminal&nbsp;(CTRL&nbsp;+ ALT&nbsp;+ T) and maximize it (ALT&nbsp;+ F10)Do t...'
url: /how-to-logout-using-terminal-in-ubuntu-linux
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
  - http://sag-3.blogspot.com/2013/07/logout-using-terminal-ubuntu-linux.html
syndication_item_hash:
  - 2434eb4510922f9889c0514ab7373ac9
  - 2434eb4510922f9889c0514ab7373ac9
  - 2434eb4510922f9889c0514ab7373ac9
  - 2434eb4510922f9889c0514ab7373ac9
  - 2434eb4510922f9889c0514ab7373ac9
  - 2434eb4510922f9889c0514ab7373ac9
categories:
  - Linux
  - LXDE
  - Program
  - Ubuntu
  - Unity
  - XFCE
tags:
  - GNOME
  - Terminal

---
There is no specific command to **logout** in an **Ubuntu** system.


## Why ?

Here&#8217;s how it works. The desktop you&#8217;re seeing is a separate software. They are called **Desktop Environments** (DE). The default DE of **Ubuntu** is **Unity** from Ubuntu&#8217;s 11.04 versions. Before it was **GNOME**. There are many DEs for Ubuntu. Some of them are :

  * Openbox
  * XFCE
  * LXDE

and many more.. There are also separate **Login Managers** to provide the login screen while Ubuntu is loaded. When you log in, the Login Manager starts the default DE set up on your system and when this DE program is closed or terminated, it will go back to the Login Screen or Login Manager.

So, as to log out the user, you have to terminate the DE program. This is why there is no special command to log out.

## How ?

Now, let me show you how to terminate the DE program. It&#8217;s very easy, just like closing an application.

First, we need to find which DE **Ubuntu** is using on your system. You can find it from the small Ubuntu icon on your login screen just near Username field. Here are some other clues :

  * Unity &#8211; Stylish Sidebar with icons and the Dashboard menu appears when you press Super/Windows key
  * GNOME &#8211; A Footprint logo on desktop
  * XFCE &#8211; There&#8217;s a mice logo somewhere on your desktop
  * LXDE &#8211; Have a Windows like Panel at the bottom

And here are the program names of the above DE except for Unity :

  * GNOME &#8211; gnome-session
  * XFCE &#8211; xfce4-session
  * LXDE &#8211; lxsession

Open a Terminal (CTRL + ALT + T) and do one of the two methods shown below :

## Method 1

Do the following command :

<pre class="prettyprint"><code>ps aux | grep &lt;DE_program&gt;</code></pre>

Replace "<DE_program>" in the above command with the program name of DE.

You will get a list of processes running. Find the DE program name from the list. Get the process id of that process from column no 2. Suppose the id is **1728**. To logout you should **kill** the process. For that use the following command:

<pre class="prettyprint"><code>kill &lt;process_id&gt;</code></pre>

If the process id is **1728** the code will be :

<pre class="prettyprint"><code>kill 1728</code></pre>

What the above command does is that it terminates the DE program.

## Method 2

You can directly kill the DE program by mentioning it&#8217;s program name, like this :

<pre class="prettyprint"><code>pkill &lt;DE_program&gt;</code></pre>

Replace "<DE_program>" in the above command with the program name of DE and execute it. This will also kill the program just like the 1st method.

## Unity

Unity has some special consideration. The log out can be initiated without killing any program. Run the command :

<span style="color: #222222;"></span>

<pre class="prettyprint"><code>gnome-session-quit --no-prompt</code></pre>

<span style="color: #222222;"></span>

The above will work on Unity even though the command is for **GNOME**, because the program is included with every installation of Ubuntu.

 [1]: #why
 [2]: #how
 [3]: #method-1
 [4]: #method-2
 [5]: #unity
