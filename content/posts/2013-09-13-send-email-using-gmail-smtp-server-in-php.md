---
title: Send Email using GMail SMTP server in PHP
author: Subin Siby
type: post
date: 2013-09-13T04:42:00+00:00
excerpt: "This tutorial was suggested by SWDS. To send an e-mail using GMail's SMTP&nbsp;server, you need extra help. Download the latest version of&nbsp;Swift Mailer Package from here.This mailing class is simple, that's why I recommend you to use Swift Mailer...."
url: /send-email-using-gmail-smtp-server-in-php
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1973357605
  - 1973357605
  - 1973357605
  - 1973357605
  - 1973357605
  - 1973357605
blogger_permalink:
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
  - http://sag-3.blogspot.com/2013/09/gmail-smtp-mail-sending-php.html
syndication_item_hash:
  - 09696c3bad6950aeef90c3049d003126
  - 09696c3bad6950aeef90c3049d003126
  - 09696c3bad6950aeef90c3049d003126
  - 09696c3bad6950aeef90c3049d003126
  - 09696c3bad6950aeef90c3049d003126
  - 09696c3bad6950aeef90c3049d003126
categories:
  - PHP
tags:
  - Email
  - GMAIL
  - Google
  - SMTP
  - Suggestion

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div class="tr_bq">
    This tutorial was suggested by <a href="http://disqus.com/disqus_vTWBKfAyps/" >SWDS</a>. To send an e-mail using GMail&#8217;s <b>SMTP</b>&nbsp;server, you need extra help. Download the latest version of&nbsp;<b>Swift Mailer</b> Package from <a href="http://swiftmailer.org/" >here</a>.
  </div>
  
  <div class="tr_bq">
    This mailing class is simple, that&#8217;s why I recommend you to use <b>Swift Mailer</b>.<br />If you have enabled <a href="https://support.google.com/accounts/answer/185833?hl=en" >application specific password</a> feature on <b>Google Accounts</b>, you may have to generate a new application password and use that password for <b>SMTP</b>&nbsp;authentication. Otherwise the example won&#8217;t be able to login to your <b>google account</b>.</p> 
    
    <p>
      Let&#8217;s get started.
    </p>
  </div>
  
  <p>
    Create a file named <b>send.php </b>:
  </p>
  
  <blockquote>
    <p>
      ini_set("display_errors",1);<br />require_once &#8216;../path/to/lib/swift_required.php&#8217;;<br />// Configuration<br />$transport = Swift_SmtpTransport::newInstance(&#8216;ssl://smtp.gmail.com&#8217;, 465)<br />&nbsp; &nbsp; ->setUsername(&#8216;<span style="background-color: yellow;">username@gmail.com</span>&#8216;) // Your Gmail Username<br />&nbsp; &nbsp; ->setPassword(&#8216;<span style="background-color: yellow;">my_secure_gmail_password</span>&#8216;); // Your Gmail Password<br />$mailer = Swift_Mailer::newInstance($transport);<br />// Create a message<br />$message = Swift_Message::newInstance(&#8216;Wonderful Subject Here&#8217;)<br />&nbsp; &nbsp; ->setFrom(array(&#8216;<span style="background-color: yellow;">sender@example.com</span>&#8216; => &#8216;<span style="background-color: yellow;">My Name</span>&#8216;)) // can be $_POST[&#8217;email&#8217;] etc&#8230;<br />&nbsp; &nbsp; ->setTo(array(&#8216;<span style="background-color: yellow;">receiver@example.com</span>&#8216; => &#8216;<span style="background-color: yellow;">A guy</span>&#8216;)) // your email / multiple supported.<br />&nbsp; &nbsp; ->setBody(&#8216;Here is the <strong>message</strong> itself. It can be text or <h1>HTML</h1>.&#8217;, &#8216;text/html&#8217;);<br />if ($mailer->send($message)) {<br />&nbsp; &nbsp; echo &#8216;Mail sent successfully. Time to celebrate&#8217;;<br />} else {<br />&nbsp; &nbsp; echo &#8216;Some error occurred. Check your configuration&#8217;;<br />}
    </p>
  </blockquote>
  
  <p>
    The above example has configured with the following credentials :<span style="background-color: white;"></span>
  </p>
  
  <blockquote class="tr_bq">
    <p>
      Username &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: username@gmail.com<br />Password &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : my_secure_gmail_password<br />Send From &nbsp; &nbsp; &nbsp; &nbsp; : sender@example.com<br />Sender Name &nbsp; &nbsp; : My Name<br />Receiver &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : receiver@example.com<br />Receiver Name &nbsp; : A guy
    </p>
  </blockquote>
  
  <p>
    You should change the credentials to meet your conditions. If the mailing didn&#8217;t work, make sure the configuration is correct. If there&#8217;s any problems/suggestion/feedback just echo out in the comment box below.
  </p>
</div>