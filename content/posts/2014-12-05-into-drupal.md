---
title: Into The Drupal Community
author: Subin Siby
type: post
date: 2014-12-05T02:31:55+00:00
url: /into-drupal
categories:
  - WordPress
tags:
  - Blog
  - Drupal
  - Website

---
I have been using **WordPress** for 2 years and haven&#8217;t tried out other **CMS** platforms. I heard about <a href="https://www.drupal.org/" target="_blank"><strong>Drupal</strong></a> through the internet and via my **I.T** textbook of my school. But never got the time to try it out.

Now, I&#8217;m a participant of **Google Code In, 2014** and **Drupal** was one of the Open Source organization in it. I tried Wikimedia last year but it was hard, because it was really big for me to work on. This year, I chose **Drupal**, because that it is a **CMS** platform used by millions around the world and I thought if many other programmers can work with it, Why can&#8217;t I ? Since I already have experience with **WordPress**, it wouldn&#8217;t be hard to understand too.

So, I claimed a task and worked on making a patch of a Test for a module. What really got me thinking and exciting was that, how **awesome** testing is. You don&#8217;t get testing stuff on **WordPress** or any other **CMS** platforms that I use. A module can test itself to see whether it will work with the version of **Dupal**. How cool is that ?

All you have to do to make a test file is to make a class, add code into it and Test ! The test file behaves like what a user would do in a browser and **Drupal** will make that code into action &#8211; browsing like a user.


## Community is Big, really BIG

The best feature of **Drupal** is that it have a strong, able and <a href="https://www.drupal.org/community" target="_blank">big community</a>. If you have any problems with **Drupal** or one of it&#8217;s modules, you can ask it up in the issues page as well as in the <a href="http://drupal.org/irc" target="_blank"><strong>IRC</strong></a>. Since you have a live communication, solving the problems is very easy.

If I needed any help, the community is always there to help and there are many places where I could knock on to get help. It&#8217;s this community that makes **Drupal** what it is right now.

## Drupal.org

Though there are many good things about **Drupal** which I experienced in a single task, there are also some stuff that I think would need to be improved.

### Look & Feel

The homepage of [**Drupal**][4] is good. Though it would be nice to have a slideshow of the screenshots of websites created with **Drupal**. It would make a visitor going to the homepage thinking that "Wow, That {site} I go often was made with Drupal !". This will attract lots of web developers, especially newbies who are looking to learn a **CMS**.

<a href="https://www.drupal.org/" target="_blank"><strong>Drupal.org</strong></a> is a very fast site. Loading and performance is the best and I know it&#8217;s fast, because I have a slow internet connection. If any improvement is going to be made in the styling of the site, then the changes shouldn&#8217;t affect the speed of the site what is right now.

### Module Install

If you want to install <a href="https://www.drupal.org/project/project_module/categories" target="_blank">modules</a> on **Drupal**, you have to get the download file URL or upload he file by yourself. This is an annoying thing. Instead like **WordPress**, a page inside **Drupal**, where you can search and install with a single button is needed. See <a href="https://www.drupal.org/project/menu_import" target="_blank">this example</a> yourself.

### Documentation

The docs are too long and would take forever to completely read it. Instead, less words and more examples (tiny) will help to understand way better. Long docs should be moved into another section rather than as the main one. If you look into the docs, you can see what I mean.

Docs like <a href="https://www.drupal.org/documentation/modules/simpletest" target="_blank">this</a> are good. But long docs with crowded texts like <a href="https://www.drupal.org/documentation/install/modules-themes/modules-7" target="_blank">this</a> should be separated into different sections.

### Dashboard

By default, the admin dashboard of a **Drupal** site is empty with 3 blocks. Instead a default dashboard should be made and then allow the user to customize rather than forcing the user to create one himself like in WordPress.

### Database

If you look at the database of **Drupal**, you will be absolutely confused. In my database only, there are **77** tables where as in **WordPress**, there will be only **11**. I think this is because that **Drupal** is more advanced. But, for a developer to understand what happens in the background, he can&#8217;t. Because, it&#8217;s absolutely confusing and you won&#8217;t have any idea what is in the background. Check this out :

<img class="alignnone" src="https://www.drupal.org/files/Drupal8_UPsitesWeb_Schema_10-19-2013.png" alt="" width="2914" height="1364" />

## Conclusion

I would like to make a website myself with **Drupal** and see how it goes. Maybe, if it&#8217;s good then I&#8217;ll move this website from **WordPress** to **Drupal** itself. I learnt may about **Drupal** with a single task and what all could I learn if I do more tasks ? **Drupal is great**, **but it should be greater**.

 [1]: #community-is-big-really-big
 [2]: #drupalorg
 [3]: #look-feel
 [4]: https://www.drupal.org/
 [5]: #module-install
 [6]: #documentation
 [7]: #dashboard
 [8]: #database
 [9]: #conclusion
