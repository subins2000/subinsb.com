---
title: Implementing Login With Google + In PHP
author: Subin Siby
type: post
date: 2013-12-24T06:37:45+00:00
url: /implement-login-with-google-in-php
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - HTML
  - PHP
tags:
  - Google
  - HTTP
  - HTTPS
  - OAuth

---
In the last post, I introduced you to a **PHP OAuth Class** named **OAuth API**. In this post I&#8217;m going to tell you how to implement **Login With Google +** feature in **PHP** using the class I introduced. It&#8217;s very simple. I will tell the ways of implementing step by step. This tutorial includes placing a **Login with Google + **button on web site to authorization and finishes when we get user details. You should read <a title="Implementing OAuth System Using OAuth API Library In PHP" href="//subinsb.com/2013/12/oauth-api-library-php" target="_blank">this post</a> before starting this tutorial.

<div class="padlinks">
  <a class="demo" href="http://open.subinsb.com" target="_blank">DEMO</a>
</div>


# Appearance

<div>
  First of all, we will design our <strong>Login With Google + button</strong>. Then we will move on to the server side. We are going to design the button using <strong>HTML</strong>, <strong>CSS</strong> and a small icon.
</div>

Place the below **HTML** code on the place where you want to show the button :

> <a href="login\_with\_google.php" id="login\_with\_google">Login With Google +</a>

You should need a **Google +** small logo for making the button attractive. I have included the image below :

[<img class="aligncenter size-full wp-image-2143" alt="g+_icon" src="//lab.subinsb.com/projects/blog/uploads/2013/12/g+_icon.png" width="28" height="28" />][2]

Here is the **CSS** code that will decorate the button. I have provided the image **URL** as **https://lab.subinsb.com/projects/blog/uploads/2013/12/g+_icon.png**

> #login\_with\_google{
> 
> display: inline-block;
> 
> height: 43px;
> 
> margin: 0px;
> 
> padding: 0px 20px 0px 52px;
> 
> font-family: &#8216;Ubuntu&#8217;, sans-serif;
> 
> font-size: 18px;
> 
> font-weight: 400;
> 
> color: #fff;
> 
> line-height: 41px;
> 
> background:rgb(231, 38, 54) url([https://lab.subinsb.com/projects/blog/uploads/2013/12/g+_icon.png][2]) no-repeat 14px 8px scroll;
> 
> -webkit-border-radius: 4px;
> 
> -moz-border-radius: 4px;
> 
> border-radius: 4px;
> 
> text-decoration: none;
> 
> cursor:pointer;
> 
> }

The finished button will look something like below :

[<img class="aligncenter size-full wp-image-2144" alt="Login With Google + button" src="//lab.subinsb.com/projects/blog/uploads/2013/12/0069.png" width="238" height="52" />][3]

# Server Side

## login_with_google.php

This is the file that handles the authorization. You need the **oauth_client.php** which we mentioned in the <a title="Implementing OAuth System Using OAuth API Library In PHP" href="//subinsb.com/2013/12/oauth-api-library-php" target="_blank">previous post</a>. You need a **client id **and **client secret** which you get when you create an app @ <a href="http://code.google.com/apis/console" target="_blank">http://code.google.com/apis/console</a>. You have to mention the redirect URL in the app settings of **Google Console**.

Here is the complete code :

> <?php
  
> require(&#8216;http.php&#8217;);
  
> require(&#8216;oauth_client.php&#8217;);
  
> $client&nbsp;=&nbsp;new&nbsp;oauth\_client\_class;
  
> $client->server&nbsp;=&nbsp;&#8216;Google&#8217;;
  
> $client->debug&nbsp;=&nbsp;false;
  
> $client->debug_http&nbsp;=&nbsp;true;
  
> $client->redirect\_uri&nbsp;=&nbsp;&#8216;http://&#8217;.$\_SERVER[&#8216;HTTP\_HOST&#8217;].dirname(strtok($\_SERVER[&#8216;REQUEST\_URI&#8217;],&#8217;?&#8217;)).&#8217;/login\_with_google.php&#8217;;
  
> $client->client_id&nbsp;=&nbsp;";/\*Client&nbsp;ID\*/
  
> $client->client_secret&nbsp;=&nbsp;";/\*Client&nbsp;Secret\*/
  
> $client->scope&nbsp;=&nbsp;&#8216;https://www.googleapis.com/auth/userinfo.email&nbsp;https://www.googleapis.com/auth/userinfo.profile&nbsp;https://www.googleapis.com/auth/plus.me&#8217;;
  
> $application\_line&nbsp;=&nbsp;\\_\_LINE\_\_;
  
> if(strlen($client->client\_id)&nbsp;==&nbsp;0&nbsp;||&nbsp;strlen($client->client\_secret)&nbsp;==&nbsp;0){
  
> &nbsp;die(&#8216;Please&nbsp;go&nbsp;to&nbsp;Google&nbsp;APIs&nbsp;console&nbsp;page&nbsp;&#8216;.
  
> &nbsp;&nbsp;&#8216;http://code.google.com/apis/console&nbsp;in&nbsp;the&nbsp;API&nbsp;access&nbsp;tab,&nbsp;&#8216;.
  
> &nbsp;&nbsp;&#8216;create&nbsp;a&nbsp;new&nbsp;client&nbsp;ID,&nbsp;and&nbsp;in&nbsp;the&nbsp;line&nbsp;&#8216;.$application_line.
  
> &nbsp;&nbsp;&#8216;&nbsp;set&nbsp;the&nbsp;client\_id&nbsp;to&nbsp;Client&nbsp;ID&nbsp;and&nbsp;client\_secret&nbsp;with&nbsp;Client&nbsp;Secret.&nbsp;&#8216;.
  
> &nbsp;&nbsp;&#8216;The&nbsp;callback&nbsp;URL&nbsp;must&nbsp;be&nbsp;&#8216;.$client->redirect_uri.&#8217;&nbsp;but&nbsp;make&nbsp;sure&nbsp;&#8216;.
  
> &nbsp;&nbsp;&#8216;the&nbsp;domain&nbsp;is&nbsp;valid&nbsp;and&nbsp;can&nbsp;be&nbsp;resolved&nbsp;by&nbsp;a&nbsp;public&nbsp;DNS.&#8217;);
  
> }
  
> if(($success&nbsp;=&nbsp;$client->Initialize())){
  
> &nbsp;if(($success&nbsp;=&nbsp;$client->Process())){
  
> &nbsp;&nbsp;if(strlen($client->authorization_error)){
  
> &nbsp;&nbsp;&nbsp;$client->error&nbsp;=&nbsp;$client->authorization_error;
  
> &nbsp;&nbsp;&nbsp;$success&nbsp;=&nbsp;false;
  
> &nbsp;&nbsp;}elseif(strlen($client->access_token)){
  
> &nbsp;&nbsp;&nbsp;$success&nbsp;=&nbsp;$client->CallAPI(&#8216;https://www.googleapis.com/oauth2/v1/userinfo&#8217;,&nbsp;&#8216;GET&#8217;,&nbsp;array(),&nbsp;array(&#8216;FailOnAccessError&#8217;=>true),&nbsp;$user);
  
> &nbsp;&nbsp;&nbsp;if($success){
  
> &nbsp;&nbsp;&nbsp;&nbsp;$email=$user->email;
  
> &nbsp;&nbsp;&nbsp;&nbsp;$name=$user->name;
  
> &nbsp;&nbsp;&nbsp;&nbsp;$gender=$user->gender;
  
> &nbsp;&nbsp;&nbsp;&nbsp;$birthday=date(&#8216;d/m/Y&#8217;,&nbsp;strtotime($user->birthday));
  
> &nbsp;&nbsp;&nbsp;&nbsp;$image=$user->picture;
  
> &nbsp;&nbsp;&nbsp;}
  
> &nbsp;&nbsp;}
  
> &nbsp;}
  
> &nbsp;$success&nbsp;=&nbsp;$client->Finalize($success);
  
> }
  
> if($client->exit){
  
> &nbsp;die("Something&nbsp;Happened","<a&nbsp;href='".$client->redirect_uri."&#8216;>Try&nbsp;Again</a>");
  
> }
  
> if(!$success){
  
> ?>
  
> &nbsp;<!DOCTYPE&nbsp;HTML&nbsp;PUBLIC&nbsp;"-//W3C//DTD&nbsp;HTML&nbsp;4.01&nbsp;Transitional//EN">
  
> &nbsp;<html>
  
> &nbsp;&nbsp;<head>
  
> &nbsp;&nbsp;&nbsp;<title>Error</title>
  
> &nbsp;&nbsp;</head>
  
> &nbsp;&nbsp;<body>
  
> &nbsp;&nbsp;&nbsp;<h1>OAuth&nbsp;client&nbsp;error</h1>
  
> &nbsp;&nbsp;&nbsp;<pre>Error:&nbsp;<?php&nbsp;echo&nbsp;HtmlSpecialChars($client->error);&nbsp;?></pre>
  
> &nbsp;&nbsp;</body>
  
> &nbsp;</html>
  
> <?}?>

Don&#8217;t forget to replace **client_id** & **client_secret** values.

# How It Works

  1. When a user clicks **Login With Google +** button, the user will be redirected to the **login\_with\_google.php** page.
  2. The **PHP** file will redirect the user to **Google**&#8216;s authorization page with all the app details needed by **Google**.
  3. When the user authorizes the app, he/she will be redirected back to **login\_with\_google.php** page with a **GET **parameter named **code**.
  4. **OAuth API** library will request an **access token** to **Google** with the given **code**.
  5. When it receives the **access token**, It sends a request to **https://www.googleapis.com/oauth2/v1/userinfo** with the **access_token** and the **URL** will return the details of the user.

# Variables

> $email &#8211; The E-Mail of the user
> 
> $name &#8211; The Name of the user
> 
> $gender &#8211; The Gender of the user
> 
> $birthday &#8211; The Birthday of the user
> 
> $image &#8211; The Image URL of the user

Isn&#8217;t this tutorial simple ?

If you encountered any problems or have a suggestion, please say it on the comments. I will be happy to respond.

 [1]: #appearance
 [2]: //lab.subinsb.com/projects/blog/uploads/2013/12/g+_icon.png
 [3]: //lab.subinsb.com/projects/blog/uploads/2013/12/0069.png
 [4]: #server-side
 [5]: #login-with-googlephp
 [6]: #how-it-works
 [7]: #variables
