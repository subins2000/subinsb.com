---
title: Uploading Images Using AJAX In HTML5
author: Subin Siby
type: post
date: 2015-12-28T03:30:19+00:00
url: /upload-images-html5-ajax
categories:
  - HTML
  - HTML5
  - JavaScript
  - Program

---
In a post that was published some years ago, I explained how to [upload an image using AJAX][1]. But, it is an old trick when **HTML5** was not standardized.

But, now browsers have evolved and almost every person in the world has an **HTML5** supported browser. Besides, the old trick used a JS file which is about **15KB**. If we used **HTML5** mechanism instead, then you can save about **14KB**.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/ajax-image-upload/" target="_blank">Demo</a>
</div>

We use **FormData** object in **JS** to make the **multipart/form-data**, then the file object retrieved from the input element :

<pre class="prettyprint"><code>var data = new FormData();
var files = $("input[type=file]")[0].files;

// Append files infos (object)
$.each(files, function(i, file) {
    data.append('file-'+i, file);
});
$.ajax(</code></pre>

The code uses **jQuery**. If you want to use plain JS, then use this :

<pre class="prettyprint"><code>var data = new FormData();
var files = document.getElementById("inputfile").files;

// Append files infos (object)
for(i = 0;i &lt; files.length;i++){
    data.append('file-'+i, files[i]);
};
</code></pre>

Each file selected by the user is inserted into the FormData as "file-" followed by the index number of file. That is, if two files are selected, then the files is sent as "file-0" and "file-1".

Here is the server code in **PHP** where we access the uploaded files :

<pre class="prettyprint"><code>&lt;?php
function getMIMEType($filename){
  $finfo = new finfo;
  return $finfo-&gt;file($filename, FILEINFO_MIME_TYPE);
}
function rand_string($length) {
  $str="";
  $chars = "subinsblogabcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  $size = strlen($chars);
  for($i = 0;$i &lt; $length;$i++) {
    $str .= $chars[rand(0,$size-1)];
  }
  return $str;
}

$supported_files = array("image/jpg", "image/png", "image/gif", "image/bmp", "image/jpeg", "image/bmp");

if(count($_FILES) != 0 and $_SERVER['REQUEST_METHOD'] == "POST"){
  for($i = 0;$i &lt; count($_FILES);$i++){
    $name = $_FILES['file-' . $i]['name'];
    $size = $_FILES['file-' . $i]['size'];
    $tmp  = $_FILES['file-' . $i]['tmp_name'];
    $type = getMIMEType($tmp);
    
    echo "&lt;h3&gt;File # ". ($i+1) ."&lt;/h3&gt;";
    if(in_array($type, $supported_files)){
      /**
       * Max size is 1 MB = 1024 KB = 1024 * 1024 Bytes
       */
      if($size &lt; (1024 * 1024)){
        $upload_file_name = time() . "-" . rand_string(4);
        if(move_uploaded_file($tmp, __DIR__ . "/uploads/$upload_file_name")){
          echo "File Name : $name";
          echo "&lt;br/&gt;File Temporary Location : $tmp";
          echo "&lt;br/&gt;File Size : $size";
          echo "&lt;br/&gt;File Type : $type";
          echo "&lt;br/&gt;Image : &lt;img style='margin-left:10px;' src='uploads/$upload_file_name'&gt;";
        }else{
          echo "Uploading Failed.";
        }
      }else{
        echo "Files must have maximum size of 1 MB";
      }
    }else{
      echo "Invalid Image file format.";
    }
  }
}else{
  echo "Please select an image.";
  exit;
}
?&gt;</code></pre>

We use two functions, <a href="//subinsb.com/php-find-file-mime-type" target="_blank">getMIMEType()</a> and <a href="//subinsb.com/php-generate-random-string" target="_blank">rand_string()</a>. One for getting the uploaded file&#8217;s type and other for generating the uploaded file&#8217;s name.

By using the script above, one can upload **multiple** files. Status of each file is shown as different section with headings of each files.

 [1]: //subinsb.com/uploading-an-image-using-ajax-in-jquery-with-php