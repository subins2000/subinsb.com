---
title: India vs Australia
author: Subin Siby
type: post
date: 2011-12-28T04:36:00+00:00
excerpt: '2nd InningsAustralia : 72/4 (22.4 Over)Over 22.4&nbsp;Z Khan&nbsp;to&nbsp;MEK Hussey, no runs,&nbsp;gets forward and drags his cover drive towards mid on.'
url: /india-vs-australia
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
blogger_blog:
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
  - http://sag-3.blogspot.com
syndication_item_hash:
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
  - afd9a1b6ba5c0b39850bba59c46dc2f1
syndication_permalink:
  - http://sag-3.blogspot.com/2011/12/india-vs-australia_28.html
  - http://sag-3.blogspot.com/2011/12/india-vs-australia_28.html
  - http://sag-3.blogspot.com/2011/12/india-vs-australia_28.html
  - http://sag-3.blogspot.com/2011/12/india-vs-australia_28.html
  - http://sag-3.blogspot.com/2011/12/india-vs-australia_28.html
  - http://sag-3.blogspot.com/2011/12/india-vs-australia_28.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
  - "Subin's Blog"
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
categories:
  - Uncategorized
tags:
  - Uncategorized

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div style="text-align: center;">
    <span class="Apple-style-span" style="font-size: x-large;"><u>2nd Innings</u></span>
  </div>
  
  <div style="text-align: left;">
    Australia : 72/4 (22.4 Over)
  </div>
  
  <div style="text-align: left;">
    <span class="Apple-style-span" style="-webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: #bee9f8; color: #5c6467; font-family: Arial, Helvetica, sans-serif; font-size: x-small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 11px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;"><span class="Apple-style-span" style="color: #000099; font-weight: bold; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px;">Over 22.4</span>&nbsp;<span style="color: #0033cc; font-size: 11px; font-weight: bold; line-height: 11px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px;">Z Khan</span>&nbsp;to&nbsp;<span style="color: #339900; font-size: 11px; font-weight: bold; line-height: 11px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px;">MEK Hussey</span>, no runs<span style="font-size: 11px; font-weight: normal; line-height: 11px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px;"></span>,&nbsp;</span><span class="Apple-style-span" style="-webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: #bee9f8; color: #5c6467; font-family: Arial, Helvetica, sans-serif; font-size: x-small; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 11px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px;">gets forward and drags his cover drive towards mid on.</span>
  </div>
</div>