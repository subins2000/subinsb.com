---
title: Auto Login Without Form in phpMyAdmin
author: Subin Siby
type: post
date: 2014-08-11T03:30:57+00:00
url: /phpmyadmin-auto-login
authorsure_include_css:
  - 
  - 
  - 
categories:
  - MySQL
  - PHP
tags:
  - Array
  - Tutorials

---
If you&#8217;re using **phpMyAdmin **(PMA), you know that every time you use the application, you have to log in and when the cookie expires, you have to log in again. It&#8217;s an annoying thing to log in every time for a developer who needs to do work fast. There is a way to stop this annoying thing. It&#8217;s by disabling the login form and auto login when you visit **phpMyAdmin**.

As an application **PMA** also have a configuration file though it&#8217;s not defaultly created in PMA. It&#8217;s a **PHP** file where settings are entered into an Array.

You can find this configuration file with the name **config.inc.php** in the installation **PMA** directory. It&#8217;s possible that this file doesn&#8217;t exist in the directory. If it doesn&#8217;t, make a copy of the **config.sample.inc.php** file in the same directory with the name **config.inc.php**.

Open the **config.inc.php** file with your text editor. As you can see, the settings are added to an array contained in the variable **$cfg**. Many of the settings in it are not active because they are all commented. You can explore more settings from it.

Now, we should add new settings to disable the login by form and set the login automatically. For this, first we add 2 new items to the settings array :

<pre class="prettyprint"><code>$cfg['Servers'][$i]['user'] = '&lt;span style="color: rgb(255, 0, 0);">MySQLusername&lt;/span>';
$cfg['Servers'][$i]['password'] = '&lt;span style="color: rgb(255, 0, 0);">MySQLpassword&lt;/span>';</code></pre>

You should include the above code with the values changed in the **Authentication type** section of the file. It&#8217;s better if you keep the code as the first one in the **Authentication type** section.

Now, we should let PMA know that the login username and password is include in the configuration file and it should use it to login rather than displaying the login form. For this, you only have to change the value of the key "auth_type". By default, it will be "cookie" or something else. Here is how the new code with the value should be :

<pre class="prettyprint"><code>$cfg['Servers'][$i]['auth_type'] = 'config';</code></pre>

Save the file and when you visit PMA now, the login form won&#8217;t be seen and it will be automatically redirected to the Home page.

&nbsp;