---
title: Free Translate Using Google Without API
author: Subin Siby
type: post
date: 2015-04-20T04:30:58+00:00
url: /francium-translator
categories:
  - Francium
  - PHP
  - Program
tags:
  - Google

---
**Google Translate** is one of the best tools on the internet that translates text to another language. Currently it supports **90+** languages. Google has its **API** for translate, but its **not** **free** and you have to go through a lot of procedures. It&#8217;s boring, right ? If your app/website needs a translator, this post will help you.

As you know <a href="http://translate.google.com" target="_blank">Google Translate</a> is a web app and you can monitor the requests it sends to the server. If you look through the Developer Tools during a translation process, you will get the **URL** !

I did this and made a [Francium class][1] called "Translator" (of course it is). You can see the <a href="https://github.com/subins2000/francium-translator" target="_blank"><strong>GitHub</strong> project here</a>.

<div class="padlinks">
  <a class="download" href="https://github.com/subins2000/francium-translator/archive/master.zip" target="_blank">Download</a> <a class="demo" href="http://demos.subinsb.com/Francium/Translator/" target="_blank">Demo</a>
</div>

It is not a big project (< 300 lines) and it has only 1 function to be used by you.


# Requirements

  * <a href="http://php.net/manual/en/curl.installation.php" target="_blank">PHP cURL Extension</a>

# Configuration

One thing to note is that, we don&#8217;t mention language by its full name. But, with short forms. You can find these short forms in the class file itself. It is an array in <a href="https://github.com/subins2000/francium-translator/blob/master/class.translator.php#L52" target="_blank">`config`->`languages`</a>. Example :

There is only one sub config that you have to edit. It is \`config\`->\`default\`. What it contains is the default translation languages. Again, you have to use the short form of languages here. This is useful when you are going to use the translator for a single lang-to-lang translation.

# Translate

Now, let&#8217;s translate something. Here is the syntax of **\Fr\Translator::translate()** :

<pre class="prettyprint"><code>\Fr\Translator::translate($text, $translateTo, $translateFrom);</code></pre>

If the **$translateFrom** is null, the value "auto" is used which means that Google will detect the language of the text.

And if **$translateTo** is null, the default value from \`config\`->\`default\` is used.

## Examples

Translating "Hello" into Malayalam :

<pre class="prettyprint"><code>echo \Fr\Translator::translate("Hello", "ml", "en");
// Outputs "ഹലോ"</code></pre>

Translating "My House" to French :

<pre class="prettyprint"><code>echo \Fr\Translator::translate("Hello", "fr", "en");
// Outputs "Ma Maison"</code></pre>

Translating "Internet" to Arabic :

<pre class="prettyprint"><code>echo \Fr\Translator::translate("Hello", "ar", "en");
// Outputs "Ma Maison"</code></pre>

This Francium class supports error logging too. The most likely error you&#8217;re going to get will be of **cURL**&#8216;s.

For support, you can see [this page][6].

 [1]: //subinsb.com/the-francium-project "The Francium Project"
 [2]: #requirements
 [3]: #configuration
 [4]: #translate
 [5]: #examples
 [6]: //subinsb.com/ask/francium/translator
