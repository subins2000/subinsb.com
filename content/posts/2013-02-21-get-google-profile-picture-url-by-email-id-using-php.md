---
title: Get Google Profile Picture URL by email id using PHP
author: Subin Siby
type: post
date: 2013-02-21T05:46:00+00:00
excerpt: "This is a trick using Google Profile.First of all we are going to set a variable with a Google&nbsp;E-mail&nbsp;address&nbsp;(Gmail address). We are going to set it with my E-mail address$email='subins2000@gmail.com';&nbsp;Then we are going to split @ ..."
url: /get-google-profile-picture-url-by-email-id-using-php
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
  - http://sag-3.blogspot.com/2013/02/Get-Google-Profile-Picture-by-email-id-using-PHP.html
syndication_item_hash:
  - 9675db7fd290301577554f8f2fdadfad
  - 9675db7fd290301577554f8f2fdadfad
  - 9675db7fd290301577554f8f2fdadfad
  - 9675db7fd290301577554f8f2fdadfad
  - 9675db7fd290301577554f8f2fdadfad
  - 9675db7fd290301577554f8f2fdadfad
dsq_thread_id:
  - 1962170655
  - 1962170655
  - 1962170655
  - 1962170655
  - 1962170655
  - 1962170655
categories:
  - PHP
tags:
  - Google
  - Image
  - Profile
  - Trick
  - URL

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  This is a trick using <b>Google Profile</b>.<br />First of all we are going to set a variable with a <b>Google&nbsp;E-mail</b>&nbsp;address<b>&nbsp;</b>(<b>Gmail </b>address). We are going to set it with my E-mail address</p> 
  
  <blockquote class="tr_bq">
    <p>
      $email=&#8217;<span style="color: red;">subins2000</span>@gmail.com&#8217;;&nbsp;
    </p>
  </blockquote>
  
  <p>
    Then we are going to split <b>@</b> from&nbsp;the mail address.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      list($id, $domain) = split("@",$email);
    </p>
  </blockquote>
  
  <p>
    Next we are going to send a request to <b>Google Profiles</b>&nbsp;to fetch the <b>IMAGE</b>&nbsp;url.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      $headers = get_headers("https://profiles.google.com/s2/photos/profile/".$id, 1);
    </p>
  </blockquote>
  
  <p>
    The variable <b>$id</b>&nbsp;above is from the splitted part.<br />Finally we have to get the <b>URL</b>&nbsp;of the user&#8217;s profile picture.
  </p>
  
  <blockquote class="tr_bq">
    <p>
      $url = $headers[&#8216;Location&#8217;];
    </p>
  </blockquote>
  
  <p>
    We have now got the <b>Image</b>&nbsp;URL. The overall code will be :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      $email=$_GET[&#8216;gmail&#8217;];<br />list($id, $domain) = split("@",$email);<br />$headers = get_headers("https://profiles.google.com/s2/photos/profile/".$id, 1);<br />$url = $headers[&#8216;Location&#8217;];
    </p>
  </blockquote>
  
  <p>
    You could do whatever with the <b>IMAGE</b>&nbsp;URL.</div>