---
title: Configure WordPress To Not Use FTP To Do Stuff
author: Subin Siby
type: post
date: 2014-02-18T14:04:23+00:00
url: /wordpress-no-ftp
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Linux
  - PHP
  - WordPress
tags:
  - FTP
  - Permission

---
By default, if you want to install plugins or upgrade WordPress, you have to provide the **FTP** details. If you are running WordPress on localhost, you won&#8217;t have the FTP credentials. In this case you won&#8217;t be able to install plugins or upgrade WordPress. There is a way to configure WP to don&#8217;t use FTP, instead install directly using the **Read/Write** permission of folder.

If WordPress can&#8217;t install anything without FTP, it needs another method &#8211; direct install. WP will download the required files in a **ZIP** archive format, extract it to the plugins directory. But WP can&#8217;t just move/copy files to a directory of your WP root with out permissions. So you have to change the permissions of your WP root.

## Set Write Permission

WP root directory should have the **Write** permission. You can set the write permission by applying the **0777** permission to the directory. On **Linux** system, you can do this with the following command :

<pre class="prettyprint"><code>chmod 0777 /path/to/wordpress-installation</code></pre>

As you can see, you have to mention the WP installation directory location. By doing the above command, the WP folder&#8217;s permissions would have **Write**.

## Configure WP To Not Use FTP

The configuration file of WP is **wp-config.php**, so any settings we are going to set should be in the **wp-config.php** file. The "No FTP" setting should also be in this file.

Open your WP location. Open the **wp-config.php** file using a text editor. Add the following code at the end of the file :

<pre class="prettyprint"><code>/* No FTP : http://subinsb.com/wordpress-no-ftp */
define('FS_METHOD','direct');</code></pre>

Save the file and close the text editor.

Now, try installing a plugin or removing a plugin. WordPress won&#8217;t bother you anymore with FTP details form.