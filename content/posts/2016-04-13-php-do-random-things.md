---
title: Doing Random Things in PHP
author: Subin Siby
type: post
date: 2016-04-12T19:30:30+00:00
url: /php-do-random-things
categories:
  - PHP
  - Program
tags:
  - boolean
  - Function
  - Random

---
Suppose you want to display something randomly in a news feed, or pick random elements from an array, or anything random in general. Programming Languages are used to define the way how something must be done. Hence in a technical way, it can&#8217;t do things randomly on it&#8217;s own. Otherwise it would be an **AI** (Artificial Intelligence). Doing random things is not done at compiling, but in execution. In case of **PHP**, this doesn&#8217;t matter as it&#8217;s an **interpreter**.


## Note

In situations like generating random salts for password and other security related things, you **shouldn&#8217;t** use this. There are really better functions to do those like **random_int()**, **openssl\_random\_pseudo_bytes()**

## The Problem

The Random Function is useful in these scenarios :

  * <a href="//open.subinsb.com" target="_blank">Showing Extra Box (Ads, Friend Suggestions) between some "posts" randomly in a news feed of a social network.</a>
  * Show "Subscribe" box randomly to readers in blogs

There isn&#8217;t a definite function in **PHP** to do something at random. How about we make one ?

## The Solution aka random()

Meet the function **random()** which will return positive (In boolean terms, it&#8217;s called **TRUE**), and negative if it&#8217;s not the right time to do something.

<pre class="prettyprint"><code>/**
 * $occurence - integer
 * $range - integer
 */
function random($occurence = 3, $range = 100){
  return mt_rand(1, $range) % $occurence === 0;
}</code></pre>

By tweaking the **$occurence** parameter, you can increase/decrease the possibility ie returning **TRUE**. **$range** can be used to increase/decrease the scope of randomness. Example: By increasing both **$occurence** and **$range**, the possibility of getting a **TRUE **output **decreases**. It is inversely proportional. Note that :

> **$range** > **$occurence**

Which means, tweaking **$occurence** has greater effect on the randomness than **$range**. Yeah, it&#8217;s confusing. You can decrease this confusion by reading the **[How It Works ?][4]**

## How It Works

As you may know, there is already a function in **PHP** to get a random number ie **rand()**. We simply uses it to return **TRUE**/**FALSE** instead of numbers. So, how is it done ? By checking whether the **random number** is **divisible** by the integer in **$occurence**. The random number is made from :

> 1 &#8211; $range

^ I omitted zero as 0 can be divided by any number. As the **random number** is random (obviously!), it won&#8217;t be always divisible by **$occurence**. So, when it is divisible, you get a **TRUE** return value and when it&#8217;s not divisible you get a **FALSE** return value. This is why I earlier stated that :

> Tweaking **$occurence** has greater effect on the randomness than **$range**.

Increasing **$range** would just increase the list from where the random number is picked. This means that the altering of the randomness will be small. But, if you change **$occurence**, the whole scenario changes and the randomness dramatically changes. Here&#8217;s a <a href="https://gist.github.com/subins2000/40d8023cfaf2d0836c5e33a616a2e747" target="_blank">test case</a> running **random()** in a loop for **100** iterations  :

| $occurence | Number of Outcomes That Were TRUE |
| ---------- | --------------------------------- |
| 1          | 100                               |
| 2          | 38                                |
| 3          | 35                                |
| 4          | 18                                |
| 5          | 21                                |
| 6          | 9                                 |
| 7          | 14                                |

As you can see the **TRUE** outcomes didn&#8217;t decrease proportionally ie it&#8217;s **absolutely random**.

 [1]: #note
 [2]: #the-problem
 [3]: #the-solution-aka-random
 [4]: #article-how-it-works
 [5]: #how-it-works
