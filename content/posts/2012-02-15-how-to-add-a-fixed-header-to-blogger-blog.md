---
title: How to Add a Fixed Header to Blogger Blog
author: Subin Siby
type: post
date: 2012-02-15T15:44:00+00:00
excerpt: 'A fixed header is a header that is in a fixed position. It will be seen in your blog wherever you are in your blog. See an example here.To add this follow these steps1st Step : Go to Blogger -&gt; Template -&gt; Edit Html&nbsp;&nbsp; &nbsp; &nbsp; &nbs...'
url: /how-to-add-a-fixed-header-to-blogger-blog
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
  - http://sag-3.blogspot.com/2012/02/how-to-add-fixed-header-to-blogger.html
syndication_item_hash:
  - 76ec1a9c5536c1976bfced2d45669d6e
  - 76ec1a9c5536c1976bfced2d45669d6e
  - 76ec1a9c5536c1976bfced2d45669d6e
  - 76ec1a9c5536c1976bfced2d45669d6e
  - 76ec1a9c5536c1976bfced2d45669d6e
  - 76ec1a9c5536c1976bfced2d45669d6e
dsq_thread_id:
  - 1958084449
  - 1958084449
  - 1958084449
  - 1958084449
  - 1958084449
  - 1958084449
categories:
  - Blogger
tags:
  - Tutorials

---
A fixed header is a header that is in a fixed position. It will be seen in your blog wherever you are in your blog. See an example [here][1].
  
To add this follow these steps

1st Step : Go to **Blogger -> Template -> Edit Html<img class="alignright" src="//4.bp.blogspot.com/-qIrpm3u0MsA/TzvOcNDo9FI/AAAAAAAAAgQ/kG76N_6CE-o/s200/fb4.png" alt="" width="100" height="60" border="0" />**

Copy and pate these lines before the **</head>** tag :

    <style type="text/css">
    /* Subin's Blog - http://subinsb.com */
    .fixed-header{
     overflow: hidden;
     position: fixed;
     z-index: 999999;
     top: 0px;
     left: 0px;
     right: 0px;
     height: <span style="color: red;">76</span>px;
     background: <span style="color: red;">#EEE</span>;
    }</style>

2nd Step : Go to **Blogger -> Layout  **and click on <a style="font-family: 'Times New Roman'; line-height: normal; margin-left: 1em; margin-right: 1em; text-align: center;" href="https://2.bp.blogspot.com/-N_WqfH_GbMU/TzvQ2yjVUtI/AAAAAAAAAgc/wlk6pwY2ktY/s1600/fb6.png"><img src="https://2.bp.blogspot.com/-N_WqfH_GbMU/TzvQ2yjVUtI/AAAAAAAAAgc/wlk6pwY2ktY/s200/fb6.png" alt="" width="200" height="32" border="0" /></a> button.

Then Click on <a style="font-family: 'Times New Roman'; line-height: normal; margin-left: 1em; margin-right: 1em; text-align: center;" href="//1.bp.blogspot.com/-AsdUESbUyBY/TzvRPEkugmI/AAAAAAAAAgk/cPBSB6ld5Qw/s1600/fb7.png"><img src="//1.bp.blogspot.com/-AsdUESbUyBY/TzvRPEkugmI/AAAAAAAAAgk/cPBSB6ld5Qw/s200/fb7.png" alt="" width="200" height="46" border="0" /></a> button and type these in the content area

    <div class="fixed-header" >
    <center><h1>Type Your Blog Name Here</h1></center>
    </div>
    

After typing Click on "**SAVE**" button. That&#8217;s it. You should now see a fixed-header in your blog.

 [1]: http://subin-demos.blogspot.in/2012/04/blogger-fixed-header-demo.html