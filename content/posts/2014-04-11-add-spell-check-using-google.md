---
title: Add Spell Check With PHP Using Google
author: Subin Siby
type: post
date: 2014-04-11T14:22:05+00:00
url: /add-spell-check-using-google
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - Google
  - Hack
  - Trick

---
If you google something that has spelling mistakes, Google will automatically correct it for you or if the bot isn&#8217;t sure, it will give the **Did You Mean ?** clause. If you&#8217;re developing a search engine for your site, this spell check will come in handy. If the user had made a mistake and he was shown a "No Results Found" page, user will be annoyed. So, it&#8217;s better to have a spell check for your site.

Google until 2013 had the Spell Check service available to all developers by accessing a URL. Sadly, they discontinued and we were forced to look up on other methods. Bing supports Spell check and Yahoo too. But it&#8217;s not as good & perfect as Google.

Google Translate also has the **Did You Mean** functionality. So, I looked at the AJAX request and response and got the URL for getting the suggestions. It&#8217;s simple, we&#8217;ll send the request with the word to get suggestions. If there is one, we will display it as "Did you mean {word} ?". We&#8217;ll use **cURL** and **RegEx** for accomplishing our task.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/Francium/SpellCheck/" target="_blank">Demo</a><a class="download" href="https://github.com/subins2000/francium-spellcheck/archive/master.zip" target="_blank">Download</a>
</div>

Note that this trick or "hack" is not publicly made available by Google and I&#8217;m not responsible for the actions you make with this trick and thank you Google for your awesome products and requests like this.

Here is the function that we&#8217;ll use to check with Google about the word and what the Google Dictionary Bot thinks. If there isn&#8217;t any thing it&#8217;ll return **null** and if there is, it&#8217;ll return the word as corrected by Google.

It&#8217;s very necessary that your web server supports **cURL**.

# UPDATE

Spell Check is now under [The Francium Project][1] and it has become a static class.

# Usage

The usage of the function **\Fr\SC::check()** is very easy, you just pass the word to it and it will return the corrected word. It won&#8217;t return anything  if the word you gave is of more than the length **49**. So that&#8217;s the limit.

In this example, I give the wrong word "googel" and it will output "google".

<pre class="prettyprint"><code>$word = \Fr\SC::check("googel");
if($word == null){
 echo "No Suggestion";
}else{
 echo "Corrected Word - " . $word;
}</code></pre>

Here, I give a sentence of exactly the length **49** to the function and it&#8217;ll give the corrected output :

<pre class="prettyprint"><code>$word = \Fr\SC::check("soemthign's wrnog. If there's omething wqrong wit");
if($word == null){
 echo "No Suggestion";
}else{
 echo "Corrected Word - " . $word;
}</code></pre>

Ain&#8217;t that cool ? It is very strongly recommended that you include this trick on your search engine, because it will increase page visits and user&#8217;s experience with your site.

 [1]: //subinsb.com/the-francium-project