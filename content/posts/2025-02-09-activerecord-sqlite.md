---
title: Use ActiveRecord + SQLite without Rails
author: Subin Siby
type: post
date: 2025-02-09T00:00:00+05:30
lastmod: 2025-02-09T00:00:00+05:30
url: /ruby-activerecord-sqlite
aliases:
  - /civ
categories:
  - Tech
tags:
  - ruby
  - sqlite
---

Recently been using ActiveRecord + SQLite a LOT, so making this post for the basic template.

## Gemfile

```
source "https://rubygems.org"

gem "activerecord", "~> 8.0"
gem "pry"

gem "sqlite3", "~> 2.4"
```

## playground.rb

```ruby
require 'active_record'
require 'pry'
require 'sqlite3'

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: 'db.sqlite3'
)

class Place < ActiveRecord::Base
end

@first_run = !Place.table_exists?

# Create the table (migration-like setup)
if @first_run
  ActiveRecord::Schema.define do
    create_table :places do |t|
      t.string :name
      t.string :lat
      t.string :lon
      t.timestamps
    end
    add_index :places, :name, unique: true
  end
end

binding.pry
```
