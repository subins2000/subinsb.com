---
title: Add Invite Facebook Friends Feature In A Website
author: Subin Siby
type: post
date: 2014-02-01T07:22:55+00:00
url: /add-invite-facebook-friends-in-website
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Facebook
tags:
  - Tutorials

---
Please read this before continuing :

> Looks like Facebook is making restrictions into this. Note that, this tutorial is not officially supported by Facebook. You can say this is a trick or a hack.
> 
> So, no guarantee whether this will work now or in the future. This worked before, but it has been noted that Facebook is restricting this "hack".

There is an Invite feature for Facebook apps. Inviting feature helps your site&#8217;s users to invite their friends to your site. This increases the traffic and will make your site popular. All you should need is a Facebook App to implement this on your site.

<div class="padlinks">
  <a class="demo" href="http://open.subinsb.com/invite" target="_blank">Demo</a>
</div>


## Facebook App

Create a Facebook App. Instructions can be found <a href="//subinsb.com/2012/02/tutorial-how-to-create-a-facebook-app" target="_blank">here</a>. Go to the "Settings" page of your app :

Click on "Add Platform" button. Choose "App on Facebook" and "Website" after adding the "App on Facebook" platform :

[<img class="aligncenter size-medium wp-image-2377" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0085-300x183.png" alt="0085" width="300" height="183" />][2]

Here are the settings for "Website" platform :

[<img class="aligncenter size-medium wp-image-2378" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0086-300x105.png" alt="0086" width="300" height="105" />][3]

Add your website URL is "Site URL" and the mobile site URL if it have any.

Settings for "App on Facebook" platform :

[<img class="aligncenter size-medium wp-image-2380" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0087-300x138.png" alt="0087" width="300" height="138" />][4]

Add the **facebook_app.html** page to your site&#8217;s "Canvas URL" and "Secure Canvas URL" with the "?fb" parameter, because Facebook won&#8217;t allow you to add a page to URL without "?" character.

# Website Integration

## Invite Page

Create an invite page (invite.html). Add the following code inside "<body></body>" :

<pre class="prettyprint"><code>&lt;div id="facebook_invite"&gt;&lt;/div&gt;</code></pre>

Load the Facebook JS SDK inside #facebook_invite :

<pre class="prettyprint"><code>&lt;script src="http://connect.facebook.net/en_US/all.js"&gt;&lt;/script&gt;</code></pre>

Add the Invite Link with "FBInvite()" callback on click :

<pre class="prettyprint"><code>&lt;a href="#" onclick="FBInvite()"&gt;Invite Facebook Friends&lt;/a&gt;</code></pre>

Start the FB function :

<pre class="prettyprint"><code>&lt;script&gt;
 FB.init({
  appId:'app_id',
  cookie:true,
  status:true,
  xfbml:true
 });
&lt;/script&gt;</code></pre>

Replace "app_id" with the Application&#8217;s ID.

FBInvite() makes the call to Facebook for app request :

<pre class="prettyprint"><code>&lt;script&gt;
 function FBInvite(){
  FB.ui({
   method: 'apprequests',
   message: 'Invite your Facebook Friends'
  },function(response) {
   if (response) {
    alert('Successfully Invited');
   } else {
    alert('Failed To Invite');
   }
  });
 }
&lt;/script&gt;</code></pre>

Change the "message" parameter as you want. When user clicks the link, "FBInvite()" is called and the "Invite Friends" dialog appears :

<img class="aligncenter size-medium wp-image-2381" src="//lab.subinsb.com/projects/blog/uploads/2014/02/0088-300x287.png" alt="0088" width="300" height="287" />

The "Done" button should be clicked after clicking of "Invite" buttons near the friends&#8217; names.

## Facebook Canvas

We really don&#8217;t need a canvas, because we are inviting them to our site, not to our Facebook app. So we should make a redirection page as canvas. We mentioned the canvas URL as **facebook_app.html** when we created our app. So create the "facebook_app.html" on your site&#8217;s main directory and add the following contents :

<pre class="prettyprint"><code>&lt;html&gt;
 &lt;head&gt;&lt;/head&gt;
 &lt;body&gt;
  You are being redirected. If not &lt;a href="http://yoursite.com/" target="_top"&gt;click here&lt;/a&gt;.
  &lt;script&gt;window.top.location.href = "http://yoursite.com/";&lt;/script&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

Replace "yoursite.com" with your site&#8217;s URL. When the user goes to your Facebook App, the canvas is loaded with the "facebook_app.html" of your site which will redirect to your main site.

Sometimes Facebook will ask for Secure Canvas URL. In this case upload the HTML file to Services that provides HTML hosting and has a SSL certificate. Google Drive offers this feature.

Hope you liked it. If you have any problems feel free to comment. I will help you.

 [1]: #facebook-app
 [2]: //lab.subinsb.com/projects/blog/uploads/2014/02/0085.png
 [3]: //lab.subinsb.com/projects/blog/uploads/2014/02/0086.png
 [4]: //lab.subinsb.com/projects/blog/uploads/2014/02/0087.png
 [5]: #website-integration
 [6]: #invite-page
 [7]: #facebook-canvas
