---
title: How to get Facebook PHP SDK and install it
author: Subin Siby
type: post
date: 2013-05-10T13:59:00+00:00
excerpt: 'Download&nbsp;Facebook PHP SDK&nbsp;from here.Extract the folder src&nbsp;in the&nbsp;zip file to a folder named fbsdk in the home directory of your app.InstallationThere is no installation. You can run the functions in the file if you put this line at...'
url: /how-to-get-facebook-php-sdk-and-install-it
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - 68c36e3f21cf95ca58436ea9c9a82d69
  - 68c36e3f21cf95ca58436ea9c9a82d69
  - 68c36e3f21cf95ca58436ea9c9a82d69
  - 68c36e3f21cf95ca58436ea9c9a82d69
  - 68c36e3f21cf95ca58436ea9c9a82d69
  - 68c36e3f21cf95ca58436ea9c9a82d69
syndication_permalink:
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
  - http://sag-3.blogspot.com/2013/05/how-to-get-facebook-php-sdk-and-install.html
categories:
  - Facebook
  - PHP
tags:
  - Facebook Oauth
  - SDK

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  Download&nbsp;<b>Facebook PHP SDK</b>&nbsp;from <a href="http://get-subins.hp.af.cm/view.php?id=346" >here</a>.<br /><span style="color: red;">Extract the folder <b>src</b>&nbsp;in the&nbsp;zip file to a folder named <b>fbsdk </b>in the home directory of your app.</span></p> 
  
  <p>
    <b><span style="font-size: large;"><u>Installation</u></span></b>
  </p>
  
  <p>
    There is no installation. You can run the functions in the file if you put this line at top of each file you run the <b>SDK</b>&nbsp;functions :
  </p>
  
  <blockquote class="tr_bq">
    <p>
      include(&#8216;fbsdk/facebook.php&#8217;);
    </p>
  </blockquote>
  
  <p>
    <b>Facebook</b>&nbsp;<b>SDK</b>&nbsp;requires the following to work properly<b>&nbsp;:</b>
  </p>
  
  <ol style="text-align: left;">
    <li>
      <b>JSON PHP extension.</b>
    </li>
    <li>
      <b>CURL PHP extension.</b>
    </li>
  </ol>
</div>