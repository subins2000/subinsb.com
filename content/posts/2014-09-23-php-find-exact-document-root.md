---
title: Finding Exact Document Root in PHP
author: Subin Siby
type: post
date: 2014-09-23T03:30:55+00:00
url: /php-find-exact-document-root
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - File
  - Trick
  - Tutorials

---
You might know the **DOCUMENT_ROOT** value of the **$_SERVER** array. This value indicates the absolute location of the site directory where pages are served. In short, it&#8217;s the public folder of site as defined in the server&#8217;s configuration. Here&#8217;s a quick example :

If you have a site in **/home/me/site** and can access it by **http://site.dev**, then the **$\_SERVER[&#8216;DOCUMENT\_ROOT&#8217;]** is "/home/me/site".

If any page in the server, even if it is in many sub folders, the **DOCUMENT_ROOT** value will be the same.

This is totally depended on the site&#8217;s configuration. This value won&#8217;t be the same in your localhost server and on the server in the web.

In an other case, if you have a project or library that requires accessing files inside it which will be used in many server environments, accessing files by **$\_SERVER[&#8216;DOCUMENT\_ROOT&#8217;]** is not efficient and possible. In this case, we have to find another way to precisely calculate the library&#8217;s document path.

Any library will have a file that&#8217;s loaded first. As an example, we assume it as "load.php" in the "library" directory and this is the file which includes it :

<pre class="prettyprint"><code>&lt;?php
include "library/load.php";
?&gt;
</code></pre>

and in **load.php**, we include an other file which is in the library&#8217;s directory called "file2.php". Here is how we do it with **$\_SERVER[&#8216;DOCUMENT\_ROOT&#8217;]** :

<pre class="prettyprint"><code>&lt;?php
include $_SERVER["DOCUMENT_ROOT"] . "/library/file2.php";
?&gt;</code></pre>

But, we cannot be sure that the directory containing the library is named "library". So, we should calculate it in the correct way :

<pre class="prettyprint"><code>&lt;?php
include realpath( dirname(__FILE__) ) . "/file2.php";
?&gt;</code></pre>

The above will work in all server environments which run **PHP 5**. The above code can be shortened in another way :

<pre class="prettyprint"><code>&lt;?php
include realpath(__DIR__) . "/file2.php";
?&gt;</code></pre>

Both the ways is the same. Note that we are obtaining the absolute path of the "library" directory from the "load.php" file. So, we don&#8217;t need to add "library" before "/file2.php".

Be sure to use this when you are creating a library in **PHP** for more efficiency and perfect performance in various environments,