---
title: Set Up Free Outlook E-Mail Accounts on a Custom Domain
author: Subin Siby
type: post
date: 2014-01-24T17:12:07+00:00
url: /outlook-free-e-mail-accounts-on-custom-domains
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Microsoft
tags:
  - Domain
  - Email

---
A custom email account on your own domain is useful for various purposes. By getting an own domain email name makes you not to disclose your personal mail and use the work email for disclosing. Email account on a custom domain is costly these days. Google Apps increased the price of their plan, which makes it impossible for custom email addresses.

But, Microsoft Outlook is giving it away for free ! They will host the Email address for you free of cost. Only you have to do is, create a account on Microsoft Live. You will find the step by step instructions for setting up a custom email address on your domain.

I will prove it to you by disclosing the email address set up on this domain : **mail@subinsb.com**. To get a E-Mail address like mine you should have a Microsoft Account. If you don&#8217;t have one yet, <a href="https://signup.live.com/" target="_blank">signup for it</a>.


## Add Domain

Go to the <a href="https://domains.live.com/Signup/SignupDomain.aspx" target="_blank">Signup Domain Page</a>. The page will be like below :

[<img class="aligncenter size-medium wp-image-2288" alt="0074" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0074-300x157.png" width="300" height="157" />][2]

Type in the domain name and choose "Set up Outlook.com for my domain" option. Once filled, click Continue button.

You will be asked for assigning a domain administrator :

[<img class="aligncenter size-medium wp-image-2289" alt="0079" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0079-300x93.png" width="300" height="93" />][3]

Choose the 1st option since you already signed up for a Microsoft account. When you click "Continue", the Login page will be shown. Log in with your Microsoft account. You will be redirected back to the domains page :

[<img class="aligncenter size-medium wp-image-2291" alt="0075" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0075-300x221.png" width="300" height="221" />][4]

Make sure the details are correct. Then type in the captcha and click on "I Accept" button. If the captcha was correct, you will be redirected to your domain page.

## Verify Domain

[<img class="aligncenter size-medium wp-image-2292" alt="0076" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0076-300x228.png" width="300" height="228" />][6]

The domain page will ask you to add a **MX** record to your site. You should only add the first **MX** record shown. It&#8217;s not necessary to add all the other records. Remember to make the record priority at the highest number than other **MX** records.

Once you added the **MX** record, click on "Refresh" button. If verified, the page will reload. To check if it has been verified successfully, go to the D<a href="https://domains.live.com/manage/default.aspx" target="_blank">omain Dashboard</a>. If there is a green circle besides your domain, then it&#8217;s verified :

[<img class="aligncenter size-medium wp-image-2293" alt="0077" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0077-300x62.png" width="300" height="62" />][7]

# Add Account

Click on your domain where you want to add an E-Mail account. Click the "Add" button on the page where you are now. A Form will pop on your screen. Fill it according to your needs :

[<img class="aligncenter size-medium wp-image-2294" alt="0078" src="//lab.subinsb.com/projects/blog/uploads/2014/01/0078-300x215.png" width="300" height="215" />][9]

The "Account Name" is the username of the E-Mail address which is situated before the **@** character. If the Form is filled like in the above image, the E-Mail address will be :

<pre class="prettyprint"><code>contact@open.subinsb.com</code></pre>

Choose a Password. Note that the form you submit is going to be like creating another account on Microsoft. So be careful while filling the fields. Once you submit the form, a new account will be created. You now have an own E-Mail address on your domain. The E-Mail address will be :

<pre class="prettyprint"><code>Account_Name@domain.com</code></pre>

An example when I typed **mail** in "Account Name" for the domain **subinsb.com** :

<pre class="prettyprint"><code>mail@subinsb.com</code></pre>

So, there you go, you own E-Mail address. If you want to access your new account&#8217;s E-Mails, Log Out from the current user and login with the E-Mail address of your domain and with the password you typed in when you filled the form. If you checked the "Password Change On First Login" while submitting the form, then you will be asked to change your password. Once that is done, you will be able to access your account.

## Mail Software Configuration

If you want to add the account to your Mail Software like "**Evolution**", here is the server details of incoming E-Mails and Outgoing E-Mails.**
  
**

## Incoming E-Mail

<pre class="prettyprint"><code>Server Type : POP

Server : pop3.live.com

Use Authentication : Yes

Use Secure Connection : SSL</code></pre>

## Outgoing E-Mail

<pre class="prettyprint"><code>Server Type : SMTP

Server : smtp.live.com

Use Authentication : Yes

Use Secure Connection : SSL</code></pre>

If you have any problems, or have a suggestion please comment it.

 [1]: #add-domain
 [2]: //lab.subinsb.com/projects/blog/uploads/2014/01/0074.png
 [3]: //lab.subinsb.com/projects/blog/uploads/2014/01/0079.png
 [4]: //lab.subinsb.com/projects/blog/uploads/2014/01/0075.png
 [5]: #verify-domain
 [6]: //lab.subinsb.com/projects/blog/uploads/2014/01/0076.png
 [7]: //lab.subinsb.com/projects/blog/uploads/2014/01/0077.png
 [8]: #add-account
 [9]: //lab.subinsb.com/projects/blog/uploads/2014/01/0078.png
 [10]: #mail-software-configuration
 [11]: #incoming-e-mail
 [12]: #outgoing-e-mail
