---
title: How To Open/Show A Select Element Using jQuery
author: Subin Siby
type: post
date: 2013-11-30T06:22:01+00:00
url: /how-to-open-a-select-element-using-jquery
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - JavaScript
  - jQuery
tags:
  - Year

---
The default **select** elements looks ugly and if you want to create beautiful sites, its better to not use **select** element. But what if you really want to use **select** element ? The only way to accomplish this is to create a dynamic **select** box which functions just like the **HTML select** element. In this post I&#8217;m going to show you the way to create a fake **select** box that will open when clicked and when the user selects an option, the **select** box will automatically close.

Here is our **HTML** code :

<pre class="prettyprint"><code>&lt;button id="clickme"&gt;Click Me To Open Select Box&lt;/button&gt;
&lt;select id="choose" name="choose" style=display:none;""&gt;
  &lt;option value="burger"&gt;Burger&lt;/option&gt;
  &lt;option value="fries"&gt;French Fries&lt;/option&gt;
  &lt;option value="banana"&gt;Banana&lt;/option&gt;
&lt;/select&gt;</code></pre>

This is the **jQuery** code that will open the **#choose** select box when clicked on **#clickme** and will close if the user chooses an option :

<pre class="prettyprint"><code>$("#clickme").on("click",function(){
  var se=$("#choose");
  se.show();
  se[0].size=2;
});
$("#choose").on("click",function(){
  var se=$(this);
  se.hide();
});</code></pre>

This trick is used on **Open** in the post form** **: <a href="http://open.subinsb.com" target="_blank">http://open.subinsb.com</a>