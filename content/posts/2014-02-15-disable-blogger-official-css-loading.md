---
title: Disable Blogger Official CSS From Loading In Template
author: Subin Siby
type: post
date: 2014-02-15T16:45:20+00:00
url: /disable-blogger-official-css-loading
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Blogger
  - CSS
  - HTML

---
Blogger adds their **CSS** styles to every templates. These files&#8217; load time will make your site slow. So for a better user experience, Blogger CSS files should be removed. But there isn&#8217;t any Settings page for this. Blogger automatically add it&#8217;s style sheets. But you can disable the loading with a simple hack.


## Blogger Style Sheets

The Blogger CSS files are these :

<pre class="prettyprint"><code>https://www.blogger.com/static/v1/widgets/4219271310-widget_css_2_bundle.css
https://www.blogger.com/static/v1/widgets/1937454905-widget_css_bundle.css
https://www.blogger.com/static/v1/widgets/3160349130-widget_css_2_bundle.css
</code></pre>

When **Blogger** updates, the above URL&#8217;s will also change. Thus page load increases.

## Warning

The Blogger CSS files contain the styles of Blogger Native Widgets. If you are using a Blogger Made Template and disable the Style Sheet loading, your site will look ugly. Use this hack only if your blog is using a custom template.

## How ?

The CSS loading "<link>" tags will appear on the source code of your blog, but won&#8217;t be loaded by browsers, because it is wrapped in **HTML Comment** tag. Browsers ignore the HTML markup inside the comments. So, the Blogger CSS files won&#8217;t be loaded.

## Disable Loading In Template

Go to your **Blogger Blog -> Template -> Edit HTML**

Search for "<b:skin>" in the Template Code using your browser Find Tool (CTRL + F). Move the contents of the "<b:skin>" to "</b:skin>" element to a file. Now, replace the "<b:skin></b:skin>" code with the following :

<pre class="prettyprint"><code>&lt;style&gt;&lt;!--/*&lt;b:skin&gt;&lt;![CDATA[*/]]&gt;&lt;/b:skin&gt;</code></pre>

After replacing search for the "</head>" element. Before it add the following code :

<pre class="prettyprint"><code>&lt;style&gt;&lt;/style&gt;</code></pre>

Copy the code that we moved to a file and paste it after "<style>" in the above code.

Save the Template.

## Did It Work ?

Now, we should check if it worked on your blog. So, go to your blog address, right click on an empty space and choose the "View Page Source" option from the right click context menu Or use the CTRL + U keyboard shortcut.

Search for the following code :

<pre class="prettyprint"><code>&lt;style&gt;&lt;!---/*</code></pre>

If you found the code above and it is highlighted in black/gray color, then you have successfully disabled the Blogger CSS loading.

 [1]: #blogger-style-sheets
 [2]: #warning
 [3]: #how
 [4]: #disable-loading-in-template
 [5]: #did-it-work
