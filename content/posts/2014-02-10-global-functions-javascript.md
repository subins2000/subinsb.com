---
title: Create Global Functions In JavaScript
author: Subin Siby
type: post
date: 2014-02-10T02:35:48+00:00
url: /global-functions-javascript
authorsure_include_css:
  - 
  - 
  - 
categories:
  - JavaScript

---
Normally, a function defined would not be accessible to all places of a page. Say a function is mentioned using "function checkCookie()" on a external file loaded first on a page. This function cannot be called by the JavaScript file loaded last. So you have to make it a  Global function that can be called from anywhere on a page.

The "window" is a Global Object that has a lot of functions.

<pre class="prettyprint"><code>&gt; typeof window
&lt; "object"</code></pre>

This "window" object is accessible to all JavaScript code of a page, even if it&#8217;s an external file. So, if the "window" object is global, then the functions it contain will also be global. So, if we add a new function to the "window" object, it will be a global function. This is what we are going to do to make a Global function.

Here is a normal way that we use to define a function :

<pre class="prettyprint"><code>function checkCookie(){
 // do whatever you want here
}</code></pre>

As you already understood, this function wouldn&#8217;t be accessible to all JS codes. The error that produce when a function can&#8217;t be accessible is :

<pre class="prettyprint"><code>ReferenceError: checkCookie is not defined</code></pre>

To make the "checkCookie" function global, we are going to add the function to the "window" object :

<pre class="prettyprint"><code>window.checkCookie=function(){
 // do whatever you want here
};</code></pre>

Note the semi colon (";") at the closing of the function. This semi colon is required when defining a function on a object. As normal you can add parameters to the function.

Another Example :

<pre class="prettyprint"><code>function makeString(string, position){
 // do whatever you want here
}</code></pre>

can be made global by :

<pre class="prettyprint"><code>window.makeString=function(string, position){
 // do whatever you want here
};</code></pre>

Now, you can call the function directly or call it with window :

<pre class="prettyprint"><code>checkCookie()
window.checkCookie()

makeString("subinsb.com", 5)
window.makeString("subinsb.com", 5)</code></pre>

The output of the different ways to call functions will be the same. You should know that jQuery is also doing this to make "$" & "jQuery" functions global.