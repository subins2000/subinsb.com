---
title: How to show Disqus comment count in Blogger
author: Subin Siby
type: post
date: 2013-07-01T03:30:00+00:00
excerpt: "Many of sites have switched to Disqus&nbsp;comment system. It's fast, easy and simple to use.Many wanted to show the comment count on their homepages. For showing the comment count on your Blogger Blog&nbsp;you can just do a simple trick.Go to Blogger ..."
url: /how-to-show-disqus-comment-count-in-blogger
authorsure_include_css:
  - 
  - 
  - 
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
  - http://sag-3.blogspot.com/2013/07/how-to-show-disqus-comment-count-in.html
dsq_thread_id:
  - 1960985327
  - 1960985327
  - 1960985327
  - 1960985327
  - 1960985327
  - 1960985327
syndication_item_hash:
  - a830cfd082dd4dc34e25088328326fb8
  - a830cfd082dd4dc34e25088328326fb8
  - a830cfd082dd4dc34e25088328326fb8
  - a830cfd082dd4dc34e25088328326fb8
  - a830cfd082dd4dc34e25088328326fb8
  - a830cfd082dd4dc34e25088328326fb8
categories:
  - Blogger
  - Content Management System
  - HTML
  - JavaScript
tags:
  - Disqus

---
Many of sites have switched to **Disqus** comment system. It&#8217;s fast, easy and simple to use. Many wanted to show the comment count on their homepages. For showing the comment count on your **Blogger Blog **you can just do a simple trick.

Go to **Blogger **->** Template** -> **Edit HTML**.

Place this **Javascript** code above **</body>**.

<pre class="prettyprint"><code>&lt;script type="text/javascript"&gt;
  /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
  var disqus_shortname = '&lt;span style="background-color: yellow;">subinsblog&lt;/span>'; // required: replace example with your forum shortname
  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function () {
    var s = document.createElement('script'); s.async = true;
    s.type = 'text/javascript';
    s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
  }());
&lt;/script&gt;</code></pre>

Replace the <span style="background-color: yellow;">yellow background</span> text above to your **Disqus **shortname example: **subinsblog** from **subinsblog.disqus.com** is the **shortname**.

Now place the following code anywhere on the template where you want to show comment count :

<pre class="prettyprint"><code>&lt;a expr:href="data:post.url + &quot;#disqus_thread&quot;"&gt;0 Comments&lt;/a&gt;</code></pre>

You should place the above code anywhere inside **<div class=&#8217;post hentry&#8217;>** or else the code won&#8217;t work.