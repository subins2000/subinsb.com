---
title: How to Repair/Restore/Reinstall GRUB Using Ubuntu Live CD
author: Subin Siby
type: post
date: 2012-09-02T13:15:00+00:00
lastmod: 2021-02-18
excerpt: |
  <div dir="ltr" trbidi="on"><div><a href="//3.bp.blogspot.com/-omlah7Nh8Ys/Ub1enzdv8eI/AAAAAAAACu8/GQ6KhdJiOvk/s1600/0013.jpg" imageanchor="1"><img border="0" height="230" src="//3.bp.blogspot.com/-omlah7Nh8Ys/Ub1enzdv8eI/AAAAAAAACu8/GQ6KhdJiOvk/s320/0013.jpg" width="320"></a></div>If you have installed <b>Windows </b>after installing <b>Ubuntu </b>on your computer, then&nbsp;<b>Windows Boot Menu </b>will be the default boot menu&nbsp;on your computer. Since <b>Windows Boot Menu</b> won't detect&nbsp;<b>Ubuntu</b> you can't boot into <b>Ubuntu</b>. To fix this you need to install <b>GRUB </b>which is the boot menu of <b>Linux</b>. To install <b>GRUB</b>,<b>&nbsp;</b>boot in to your <b>Ubuntu CD </b>or a <b>Pendrive</b>&nbsp;which has <b>Ubuntu</b>. For that insert your <b>CD </b>or <b>Pendrive </b>to your computer. Restart your computer and press <b>F12&nbsp;</b>until&nbsp;a box appears. On that box you will see 2 or 3 items. If you inserted a <b>Ubuntu&nbsp;CD </b>then Select <b>CD-ROM</b>.<br><b>Note :- Boot menu might change according to the computer manufacture</b>.<br>After selecting you will enter in to <b>Ubuntu</b>&nbsp;CD.<br>You will get a screen now. Select <b>RUN Ubuntu without installing</b>. You will now boot in to <b>Ubuntu</b>.<br><br> See More : <a href="https://help.ubuntu.com/community/BootFromCD" target="_blank">Boot From CD</a><br><br> First, open the Terminal.<br><div>Mount the partition you Ubuntu Installation is on. If you are not sure which it is, launch gparted (included in the Live CD) and find out. It is Usually a EXT4 Partition. Replace the XY with the drive letter, and partition number. Example:&nbsp;sudo mount&nbsp;/dev/sda5&nbsp;/mnt. Then bind the directories, you need, like so:</div><blockquote>sudo mount&nbsp;/dev/sdXY&nbsp;/mnt</blockquote><blockquote>sudo mount --bind /dev /mnt/dev</blockquote><blockquote>sudo mount --bind /proc /mnt/proc</blockquote>&nbsp;Then we need to be the root of the installed Ubuntu. For that use <b>chroot</b>.<br><blockquote>sudo chroot /mnt</blockquote>Now install, check, and update grub. <b>This time you only need to add the partition letter (usually a) to replace X. Example</b>:grub-install&nbsp;/dev/sda,&nbsp;grub-install &ndash;recheck&nbsp;/dev/sda <br><blockquote>grub-install&nbsp;/dev/sdX</blockquote><blockquote>grub-install --recheck&nbsp;/dev/sdX&nbsp;</blockquote><blockquote>update-grub</blockquote>   <br><div>Now you can exit you mounted hard disk, and unmount.</div><blockquote>exit</blockquote><blockquote>sudo umount /mnt/dev</blockquote><blockquote>sudo umount /mnt/proc</blockquote><blockquote>sudo umount /mnt</blockquote><div><b>Shut down and turn your computer back on, and you will get the default Grub screen.</b><br><b>NOTE : Sometimes when you reboot you night not see Windows. To fix this do as below.</b><br><br><div>Boot in to <b>Ubuntu</b> and press <b>CTRL + ALT + T</b>.</div><div>A terminal window appears. Then paste the following command and press enter.</div><blockquote>sudo update-grub</blockquote><div></div><div>It will ask for your password. Type it and press enter.<br>Reboot your computer and you will see Windows now.</div><div>If you have any problems comment on this post. I will help you.</div></div><div></div></div>
url: /how-to-repairrestorereinstall-grub-2-using-a-ubuntu-live-cd
authorsure_include_css:
  -
  -
  -
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
  - http://sag-3.blogspot.com/2012/09/how-to-repairrestorereinstall-grub-2.html
syndication_item_hash:
  - d5ebb4c47160539eaeb28e9a1952c51d
  - d5ebb4c47160539eaeb28e9a1952c51d
  - d5ebb4c47160539eaeb28e9a1952c51d
  - d5ebb4c47160539eaeb28e9a1952c51d
  - d5ebb4c47160539eaeb28e9a1952c51d
  - d5ebb4c47160539eaeb28e9a1952c51d
dsq_thread_id:
  - 1960312454
  - 1960312454
  - 1960312454
  - 1960312454
  - 1960312454
  - 1960312454
categories:
  - Linux
  - Ubuntu
tags:
  - GRUB

---
GRUB is the default bootloader of Linux. It is installed on your Hard Disk, but it's not accessible as a drive. GRUB is installed on your Hard Disk when you install a Linux Operating System.

Without GRUB, you can't boot in to OSs. Windows, Mac and other OS have their own bootloader. Linux too have different bootloaders. GRUB, BURG are examples.

When you install Windows after you install Ubuntu, Windows replaces the GRUB in the Hard Disk to Windows' boot loader which won't identify Linux systems (Damn you Windows !). But, our GRUB detects every OS. So, it's better to have GRUB over Windows Boot Loader.

You have to re install GRUB after the following scenarios :

  * Changed Partitions of Hard Disk
  * Installed Windows after Ubuntu

GRUB comes with every Linux Distros. So, all you have to do is install GRUB from those distros. Luckily, Ubuntu can be booted from the Install Disk itself. Therefore you can install GRUB from the Lice CD.

## Boot From Live CD

Insert the CD or Pendrive that has Ubuntu in to your computer.<img class="alignright" src="//3.bp.blogspot.com/-omlah7Nh8Ys/Ub1enzdv8eI/AAAAAAAACu8/GQ6KhdJiOvk/s320/0013.jpg" alt="" width="320" height="230" border="0" />

Restart your computer and press **F12** repeatedly until the **BIOS Boot Menu** appears. You can see 2 or 3 items in the menu.

If you inserted a Ubuntu CD then Select CD-ROM or others accordingly.

Note : Boot menu might change according to the **BIOS** version.

After selecting you will enter in to the Ubuntu Installation Process.

After some time, Ubuntu Installation Window appears.

Select Run Ubuntu / Try Ubuntu from the options. You will now boot in to Ubuntu.

For more information about booting into Ubuntu from CD, see the official Ubuntu Community Page : [Boot From CD][1]

## Re Install GRUB

When you reach the Ubuntu Desktop, open the Terminal using `CTRL + ALT + T` key combination or by the Dash menu or on Old systems by `Applications -> Accessories -> Terminal`.

Mount the partition where your Ubuntu is installed. If you are not sure which it is, launch `gparted` (included in the Live CD) and find out. You can open gparted from the Terminal with the command :

```bash
gparted
```

Linux distros use **ext4** partition. Locate your installed Ubuntu partition (drive) from `gparted` and note the partition (drive) letter and number (example: `sda5`).

Now, mount the installed Ubuntu Drive. Replace `XY` with the drive letter, and partition number. Example: `sudo mount /dev/sda5 /mnt`

```bash
sudo mount /dev/sdXY /mnt
```

Then bind the directories we need. Copy the whole command (it's separate commands wrapped into one) below and run it :

```bash
sudo mount --bind /dev /mnt/dev && \
sudo mount --bind /dev/pts /mnt/dev/pts && \
sudo mount --bind /proc /mnt/proc && \
sudo mount --bind /sys /mnt/sys
```

If your system uses UEFI for booting, you may need to do additional steps as [mentioned here](https://askubuntu.com/a/831241/87199).

Then we need to be the super use of the installed Ubuntu. For that use **chroot**.

```bash
sudo chroot /mnt
```

Now install, check, and update grub. Replace `X` in the following command with only the drive letter (usually **a**). Example: `grub-install /dev/sda`

```bash
grub-install /dev/sdX && grub-install --recheck /dev/sdX && update-grub
```

GRUB is now installed in the MBR (Master Boot Recorder).

Now you can exit you mounted hard disk, and unmount everything we mounted

```bash
exit && sudo umount /mnt/dev && sudo umount /mnt/proc && sudo umount /mnt
```

Restart your computer and you will get the default GRUB Boot Screen.

## Can't See Windows

When you reboot after installing GRUB, you night not see Windows. To fix this do as below.

 Boot in to the installed <b>Ubuntu</b> (not live CD) and open Terminal. Then do the following command :

```bash
sudo update-grub
```

It will ask for your password. Type it and press enter. Reboot your computer and you will see Windows now.

If you have any problems, comment on this post. I will help you.

 [1]: https://help.ubuntu.com/community/BootFromCD
