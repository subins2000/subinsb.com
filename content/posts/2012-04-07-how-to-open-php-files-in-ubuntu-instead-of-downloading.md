---
title: How to open PHP files in Ubuntu instead of downloading.
author: Subin Siby
type: post
date: 2012-04-07T15:15:00+00:00
excerpt: "No one can open PHP files in web browsers without a server. Page types such as HTML, Javascript and CSS can be understand by the browsers. But browsers can't understand PHP. So the file will download.There are two types of file that will run : Client-s..."
url: /how-to-open-php-files-in-ubuntu-instead-of-downloading
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
syndication_item_hash:
  - 196a466d9fd296aaf5edf46fd453cf45
  - 196a466d9fd296aaf5edf46fd453cf45
  - 196a466d9fd296aaf5edf46fd453cf45
  - 196a466d9fd296aaf5edf46fd453cf45
  - 196a466d9fd296aaf5edf46fd453cf45
  - 196a466d9fd296aaf5edf46fd453cf45
blogger_permalink:
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
  - http://sag-3.blogspot.com/2012/04/how-to-open-php-files-in-ubuntu-instead.html
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
dsq_thread_id:
  - 1968413799
  - 1968413799
  - 1968413799
  - 1968413799
  - 1968413799
  - 1968413799
categories:
  - PHP
  - Ubuntu
tags:
  - Tutorials

---
<p dir="ltr" style="text-align: left;">
  No one can open <b>PHP</b> files in web browsers without a server. Page types such as <b>HTML</b>, <b>Javascript</b> and <b>CSS</b> can be understand by the browsers. But browsers can&#8217;t understand<b> PHP</b>. So the file will download.<span style="font-family: inherit;"><br /> There are two types of file that will run : <b>Client-side</b> and <b>Server-side</b></span><br /> <span style="font-family: inherit;"><br /> <b>Client Side </b>: The program runs in browser itself. The client can easily access the code. The browser can understand the language in which the client program is written.</span><br /> <span style="font-family: inherit;">examples : HTML, CSS, JavaScript, jQuery, AngularJS</span><br /> <span style="font-family: inherit;"><br /> <b>Server Side</b> : The program runs in server and the finished page is sent to the client&#8217;s browser. The returned page won&#8217;t contain any source code of the language ie the client won&#8217;t see the code in page source unlike in <b>Client</b> <b>Side</b>. The returned page won&#8217;t contain any server language at all.</span><br /> <span style="font-family: inherit;"><br /> So now you got the reason why the <b>PHP</b> file won&#8217;t open in web browsers. </span><span style="line-height: 1.5em;">You can only open </span><b style="line-height: 1.5em;">PHP</b><span style="line-height: 1.5em;"> file with a server that supports </span><b style="line-height: 1.5em;">PHP</b><span style="line-height: 1.5em;"> language. </span><span style="line-height: 1.5em;">So here&#8217;s how to install Apache server and configure the server in </span><b style="line-height: 1.5em;">Ubuntu</b>
</p>

<p dir="ltr" style="text-align: left;">
  <span style="font-family: inherit;"><br /> Open <b>Terminal [CTRL + ALT + T]</b></span>
</p>

<h1 dir="ltr" style="text-align: left;">
  <b>Install Apache</b>
</h1>

Type

<blockquote class="tr_bq">
  <p>
    sudo apt-get install apache2
  </p>
</blockquote>

in terminal. You will be asked for your password. Type your password and press enter key.

# Restart Server

Type

<pre class="prettyprint"><code>sudo /etc/init.d/apache2 restart</code></pre>

in terminal and press Enter Key.
  
To check whether you installed **Apache** correctly open your web browser and go to <http://localhost/>
  
If the browser doesn&#8217;t say "**This webpage is not available**", You installed Apache correctly.

# <span style="font-family: inherit;">Install PHP</span>

Type

<pre class="prettyprint"><code>sudo apt-get install libapache2-mod-php5</code></pre>

<span style="background-color: white;"><span style="text-align: -webkit-auto; white-space: pre-wrap;">and press Enter Key.</span></span>
  
<span style="background-color: white;"><span style="text-align: -webkit-auto; white-space: pre-wrap;">Enable this module by doing the command :</span></span>

<pre class="prettyprint"><code>sudo a2enmod php5</code></pre>

<span style="text-align: -webkit-auto; white-space: pre-wrap;">and relaunch Apache by entering the command:</span>

<pre class="prettyprint"><code>sudo service apache2 restart</code></pre>

To Open the **PHP** files now, copy the files and paste it on **/var/www** folder. The go to **http://localhost/file-name.php** to see your PHP file in action. <span style="background-color: white; text-align: -webkit-auto;"><span style="background-color: white; text-align: -webkit-auto;">See this <a href="http://sag-3.blogspot.in/2012/04/how-to-create-localhost-site-in-ubuntu.html">tutorial</a> to see how to create a localhost website.</span></span>