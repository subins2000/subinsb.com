---
title: Installing Wine 1.6 on Ubuntu 10.04
author: Subin Siby
type: post
date: 2013-07-28T03:30:00+00:00
excerpt: 'Wine 1.6&nbsp;is not latest version for&nbsp;Ubuntu 10.04, but you can install it by compiling the source code and it works perfectly. See demo here. To install Wine 1.6&nbsp;follow the instructions.Optional :Wine Team&nbsp;recommend you to uninstall p...'
url: /installing-wine-1-6-on-ubuntu-10-04
authorsure_include_css:
  - 
  - 
  - 
blogger_permalink:
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
  - http://sag-3.blogspot.com/2013/07/install-wine-1.6-ubuntu-10.04.html
syndication_item_hash:
  - f06388fa4bf1c84b1213a47d3d3a2475
  - f06388fa4bf1c84b1213a47d3d3a2475
  - f06388fa4bf1c84b1213a47d3d3a2475
  - f06388fa4bf1c84b1213a47d3d3a2475
  - f06388fa4bf1c84b1213a47d3d3a2475
  - f06388fa4bf1c84b1213a47d3d3a2475
dsq_thread_id:
  - 1961353705
  - 1961353705
  - 1961353705
  - 1961353705
  - 1961353705
  - 1961353705
categories:
  - Ubuntu
  - Wine
tags:
  - 10.04
  - Compile
  - Download

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <b>Wine 1.6</b>&nbsp;is not latest version for&nbsp;<b>Ubuntu 10.04</b>, but you can install it by compiling the source code and it works perfectly. See demo <a href="http://sag-3.blogspot.com/2013/07/nfs-most-wanted-running-on-wine-16.html" >here</a>. To install <b>Wine 1.6</b>&nbsp;follow the instructions.</p> 
  
  <h4 style="text-align: left;">
    <b><span style="font-size: large;"><u>Optional :</u></span></b>
  </h4>
  
  <div>
    <b>Wine Team</b>&nbsp;recommend you to uninstall previous <b>Wine </b>versions when installing a compiled version. But its optional. If the <b>Wine 1.6</b>&nbsp;fails you can uninstall <b>1.6</b>&nbsp;and use the old version.
  </div>
  
  <h4 style="text-align: left;">
    <b><span style="font-size: large;"><u>Steps :</u></span></b>
  </h4>
  
  <div>
    Download the <b>Wine</b>&nbsp;<b>1.6</b>&nbsp;<b>Source Code</b>&nbsp;from <a href="http://get-subins.hp.af.cm/view.php?id=351" >here</a>. Extract the <b>wine- 1.6</b>&nbsp;folder in the archive file to a place you desire. For Now, place it on your home directory.
  </div>
  
  <div>
    The <b>Wine-1.6</b>&nbsp;folder will look something like this:
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="https://2.bp.blogspot.com/-4fMRUQbuxxQ/UfPSpqVaOCI/AAAAAAAAC3I/3kdAmcLfTuU/s1600/0031.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="https://2.bp.blogspot.com/-4fMRUQbuxxQ/UfPSpqVaOCI/AAAAAAAAC3I/3kdAmcLfTuU/s1600/0031.png" /></a>
  </div>
  
  <div>
    Right click on blank space and click <b>Open In Terminal</b>.
  </div>
  
  <div>
    Do the following command:
  </div>
  
  <blockquote class="tr_bq">
    <p>
      sudo&nbsp;make install
    </p>
  </blockquote>
  
  <div>
    The program will automatically compile and install <b>Wine</b>. Also sometimes it might want some software to work and the software they want will be printed on terminal. Install it. If you need instructions <b>Google</b>&nbsp;It.
  </div>
  
  <div>
    It might take up to <b>30</b>&nbsp;minutes to compile and install or maybe more. After installation the <b>Wine</b>&nbsp;<b>Config</b>&nbsp; window will look like below:
  </div>
  
  <div class="separator" style="clear: both; text-align: center;">
    <a href="//4.bp.blogspot.com/-TFMu8BsvGp8/UfPUJF5lXKI/AAAAAAAAC3Y/q2OKPsrqZjA/s1600/0032.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" src="//4.bp.blogspot.com/-TFMu8BsvGp8/UfPUJF5lXKI/AAAAAAAAC3Y/q2OKPsrqZjA/s1600/0032.png" /></a>
  </div>
  
  <div>
    The styles of button are changed in the above window of <b>Wine</b>&nbsp;because <i>I have installed a theme.</i>
  </div>
</div>