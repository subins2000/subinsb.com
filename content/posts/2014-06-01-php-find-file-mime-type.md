---
title: Find MIME Type of File in PHP
author: Subin Siby
type: post
date: 2014-06-01T07:22:59+00:00
url: /php-find-file-mime-type
authorsure_include_css:
  - 
  - 
  - 
categories:
  - PHP
tags:
  - File

---
**MIME **type is needed to know what kind of file we&#8217;re looking at. **HTML** files have the MIME type **text/html** , for gif image, it&#8217;s **image/gif** and so on.

Since **PHP** is a dynamic content creator language, MIME type is a very important thing you should know about in PHP. In this small post, I&#8217;ll tell you how to find the MIME type of a local file in your server.

## Enable Fileinfo Extension

For this, we need to enable the **PHP** fileinfo extension in **php.ini**. Add this into your **php.ini** file it doesn&#8217;t already exist :

<pre class="prettyprint"><code>extension=php_fileinfo.dll</code></pre>

And restart the Web Server. The restarting is different on various web servers. If it&#8217;s Apache you can do this command :

<pre class="prettyprint"><code>sudo service apache2 restart</code></pre>

## Get the MIME Type

We will make a function to get the MIME type easily. We&#8217;ll call it "getMIMEType" :

<pre class="prettyprint"><code>function getMIMEType($filename){
 $finfo = finfo_open();
 $fileinfo = finfo_file($finfo, $filename, FILEINFO_MIME_TYPE);
 finfo_close($finfo);
 return $fileinfo;
}</code></pre>

Or you can make it easy by using the **finfo** object :

<pre class="prettyprint"><code>function getMIMEType($filename){
 $finfo = new finfo;
 return $finfo-&gt;file($filename, FILEINFO_MIME_TYPE);
}</code></pre>

But, I think the OO (Object Oriented) way will be slower than the other. It&#8217;s your choice, but they will both work in the same way.

And here&#8217;s how you can use it. Just pass the location of the file to it and it will return the MIME type :

<pre class="prettyprint"><code>echo getMIMEType("myfile.html");</code></pre>

In the above case, it will return :

<pre class="prettyprint"><code>text/html</code></pre>

Here are some more examples :

<pre class="prettyprint"><code>getMIMEType("myimage.jpeg"); // Returns image/jpeg
getMIMEType("myfile.js"); // Returns application/x-javascript
getMIMEType("myCss.css"); // Returns text/css</code></pre>

Use it as you wish.