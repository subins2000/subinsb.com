---
title: Some Plans
author: Subin Siby
type: post
date: 2015-08-25T08:51:56+00:00
url: /some-plans
categories:
  - Personal

---
It&#8217;s been a while since I have written on the blog and there are many reasons for it. I&#8217;m writing now because I&#8217;m on a vacation for 10 days because of Onam, a festival in Kerala.


# Reasons

  1. School
  2. Bored Life (These days, I&#8217;m feeling a repulsion)
  3. Busy

I&#8217;ll explain :

  1. Since I have passed the 10th grade and is in higher secondary education, the stuff to learn increased. Even the though the subjects to learn have reduced to 6, the size of the books increased.
  
    Though I love all the subject that I learn now (Physics, Chemistry, Computer Science, Maths, English, Malayalam), there is something that prevents me these days to study (Teenage shit ?).
  2. Yes, life has become boring. The fire I had 4 years ago (2011) has decreased gradually. Why ? I think it&#8217;s because of the tough 10th grade. Also, some teenage problem. There is something that controls me these days, a power which I don&#8217;t know. Perhaps this is experienced by everyone of my age (or do they ?).Back in 2011, if I had planned to do something, I had this fire in my mind to do it until I reach it. But, now sometimes I feel that fire, but after some time the repulsion force brings me down. I hope this repulsion force will go soon.
  3. I&#8217;m so busy these days. School, work and code. GOD ! It&#8217;s frustrating.

# Plans

I have new plans to achieve and I hope I can do it. I&#8217;m going to get back the fire I once had. I will.

## New Company

Me and some friends are planning to create a company in my locality. We have high hopes for it. It may even grow to be an international company.

The name hasn&#8217;t been decided. It will be decided after the domain is purchased.

I will tell more about it later.

## logSys

I have plans to improve **logSys** and add new features to it.

  1. ### 2 Step/Factor Authentication
    
    I&#8217;m planning to add a 2 Step Verification on login from a new browser. A secret token will be sent to the user to verify that the user is the original owner.
    
    This secret token will be sent by E-Mail or Mobile (your choice). So, anyone want this enhancement ?
    
    Do you want this, then see <a href="https://github.com/subins2000/logSys/issues/7" target="_blank">this</a> and comment on it.</li> 
    
      * ### logSys Advanced
        
        Many of the comments in logSys was the need to have an admin panel to manage users. Though you can do this with **phpMyAdmin**, it will be a nice feature for **logSys** to have this.
        
        I plan to make this by adding user roles. Each user would have special roles. Only certain users with role can access some pages and others. It&#8217;s better if all this is integrated into a new class. So, a new class is going to be created which will be a fork of logSys. It will be called "**logSys Advanced**". It will be Open Source just like logSys.</li> </ol> 
        
        What do you think ? Do you have any extra suggestions ? Please comment.

 [1]: #reasons
 [2]: #plans
 [3]: #new-company
 [4]: #logsys
 [5]: #2-stepfactor-authentication
 [6]: #logsys-advanced
