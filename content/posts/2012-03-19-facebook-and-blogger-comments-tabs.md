---
title: Add Facebook and Blogger Comments As Tabs In Blogger
author: Subin Siby
type: post
date: 2012-03-19T16:45:00+00:00
excerpt: 'Do you want to add facebook and blogger comments in tabs ? Well you are in the right place.To add this, follow these steps.See    Demo1st step : Go to&nbsp;Blogger &gt; Template &gt; Edit Html&nbsp;Click the box Expand Widget Templates&nbsp;Paste this ...'
url: /facebook-and-blogger-comments-tabs
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1967607556
  - 1967607556
  - 1967607556
  - 1967607556
  - 1967607556
  - 1967607556
syndication_item_hash:
  - 83589e6ff5714643b3cdf51088d5512c
  - 83589e6ff5714643b3cdf51088d5512c
  - 83589e6ff5714643b3cdf51088d5512c
  - 83589e6ff5714643b3cdf51088d5512c
  - 83589e6ff5714643b3cdf51088d5512c
  - 83589e6ff5714643b3cdf51088d5512c
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
  - http://sag-3.blogspot.com/2012/03/tutorial-add-facebook-and-blogger.html
categories:
  - Blogger
  - CSS
tags:
  - Tutorials

---
Having multiple comments box on your posts make more comments. The most popular of Comments are Facebook. Why not add Facebook comments with Blogger comments on your blog. This can be easily done if you follow this tutorial carefully.

<div class="padlinks">
  <a class="demo" href="http://subin-demos.blogspot.in/2012/04/facebook-and-blogger-comments-in-tabs.html" target="_blank">Demo</a>
</div>

Go to **Blogger -> Template -> Edit HTML**.
  
Paste the following code below "<head>" tag :

<pre class="prettyprint"><code>&lt;script src='http://connect.facebook.net/en_US/all.js#xfbml=1'&gt;&lt;/script&gt; 
&lt;script src='http://code.jquery.com/jquery-latest.js'/&gt;
&lt;script type='text/javascript'&gt;
 function commentToggle(selectTab) {
  $(&quot;.comments-tab&quot;).addClass(&quot;inactive-select-tab&quot;);
  $(selectTab).removeClass(&quot;inactive-select-tab&quot;);
  $(&quot;.comments-page&quot;).hide();
  $(selectTab + &quot;-page&quot;).show();
 }
&lt;/script&gt;</code></pre>

Search for **<html** and replace it with the following code :

<pre class="prettyprint"><code>&lt;html xmlns:fb='http://ogp.me/ns/fb#'
</code></pre>

Add the following HTML code above "</head>" :

<pre class="prettyprint"><code>&lt;style&gt;
/*--- Tabbed Facebook Comments By subinsb.com ----*/
.comments-page {
 background-color: #f2f2f2;
 width:450px;
}
#blogger-comments-page {
 padding: 0px 5px;
 display: none;
}
.comments-tab {
 float: left;
 padding: 5px;
 margin-right: 3px;
 cursor: pointer; 
 background-color: #f2f2f2;
}
.comments-tab-icon {
 height: 14px;
 display: inline-block;
 vertical-align: top;
 margin-right: 3px;
}
.comments-tab:hover {
 background-color: #eeeeee;
}
.inactive-select-tab{
 background-color: #d1d1d1;
}
&lt;/style&gt;</code></pre>

By using the Blogger shortcut key **CTRL + F **find this code :

<pre class="prettyprint"><code>&lt;div class='comments' id='comments'&gt;</code></pre>

**Note : If you found two codes like above, you have to paste the code shown below after the two codes you found.**

<div style="text-align: left;">
  <pre class="prettyprint"><code>&lt;center&gt;
 &lt;table&gt;
  &lt;tbody&gt;
   &lt;tr&gt;
    &lt;td&gt;
     &lt;div class='comments-tab' id='fb-comments' onclick='javascript:commentToggle(&quot;#fb-comments&quot;);' style='float:left;' title='Facebook Comments'&gt; 
      &lt;img class='comments-tab-icon' src='https://2.bp.blogspot.com/-rae4j6NaLkY/T1JrjUEIkkI/AAAAAAAAGBc/PzwMIo1g1Is/s400/fbcomment.png'/&gt;
      &lt;fb:comments-count expr:href='data:post.url'/&gt; Comments 
     &lt;/div&gt;
    &lt;/td&gt;
    &lt;td&gt;
     &lt;div class='comments-tab inactive-select-tab' id='blogger-comments' onclick='javascript:commentToggle(&apos;#blogger-comments&apos;);' title='Blogger Comments'&gt;
      &lt;img class='comments-tab-icon' src='http://www.blogger.com/img/icon_logo32.gif'/&gt;
      &lt;data:post.numComments/&gt; Blogger Comments
     &lt;/div&gt;
    &lt;/td&gt;
   &lt;/tr&gt;
  &lt;/tbody&gt;
 &lt;/table&gt;
 &lt;div class='comments-page' id='fb-comments-page'&gt;
  &lt;b:if cond='data:blog.pageType == &quot;item&quot;'&gt;
   &lt;div id='fb-root'&gt;&lt;/div&gt;
   &lt;script&gt;
   (function(d){
   var js, id = &#39;facebook-jssdk&#39;;
   if (d.getElementById(id)){return;}
   js = d.createElement(&#39;script&#39;);
   js.id = id;
   js.async = true;
   js.src = &quot;//connect.facebook.net/en_US/all.js#xfbml=1&quot;;
   d.getElementsByTagName(&#39;head&#39;)[0].appendChild(js);
   }(document));
   &lt;/script&gt;
   &lt;fb:comments colorscheme='light' expr:href='data:post.url' expr:title='data:post.title' expr:xid='data:post.id' width='450'/&gt;
  &lt;/b:if&gt;
 &lt;/div&gt;
 &lt;div class='comments comments-page' id='blogger-comments-page' style='display:none;'&gt;</code></pre>
  
  <p>
    Save the template. You will get an error. Blogger will automatically highlight a code (</b:includable>). Paste the following code above the code Blogger highlighted :
  </p>
  
  <pre class="prettyprint"><code>&lt;/center&gt;&lt;/div&gt;</code></pre>
  
  <p>
    Save the Template and check out a post on your blog. Every post on your blog will now have a Facebook & Blogger Comments tabbed. If you want help, post a comment.
  </p>
</div>