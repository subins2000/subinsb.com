---
title: "2018 Year Review 👏👏👏"
type: post
date: 2018-12-31
url: "/2018-year-review"
aliases:
    - /2018
categories:
    - personal
    - year review
---

***Year Review 👏👏👏***

And there goes another year. Been very inactive in this blog. Should have blogged of some of the major events, but couldn't. I'm all now posting stories on [Instagram](https://instagram.com/subins2000) and on my [channel in Telegram](https://t.me/SubinSiby). Here's a summing up of my year 2018. 

## Coding

I kinda tried doing a **#100DaysOfCode** challenge, but stopped at **73**. This is my longest streak ever on GitHub !

When I look back at my contribution wall, I can see what I've been doing my whole year. It's kind of like a journal and you don't have to write anything. It just explains itself !

![](//lab.subinsb.com/projects/blog/uploads/2018/12/github-contribution-wall-2018-explained.jpg)

### Projects

#### BeX

A book exchanging webapp specifically meant for colleges. I made this to learn Django & ReactJS.

[Source](https://github.com/subins2000/bex)

Created this in semester break to implement in the following semester, but it wasn't complete. Have to work on this and possibly implement it in my college soon !

#### OpenSalve

A disaster management software for volunteers and rescue teams. Created as part of hackathons.

[Source](https://github.com/subins2000/OpenSalve)

#### TorrentBro

Ceated [TorrentBro](https://github.com/subins2000/TorrentBro) in 2017. Added some more features to it I think. I don't remember much :D

## Hackathons

This year, I participated in my very first hackathon and went on to more hackathons !

Photos, experience and stuff I did there are all uploaded as stories in [**my Instagram profile**](https://instagram.com/subins2000) !

### HackForTomorrow

[HackForTomorrow](http://hackfortomorrow.excelmec.org/) is my first ever hackathon. Me and my college senior, [Geon George](https://geongeorge.com/) participated in it.  I didn't know what hackathons were. 

And we won 2nd ! Our project was [OpenSalve](https://github.com/subins2000/OpenSalve-prototype), a disaster management software.

### FISAT TechPlanet

The next hackathon was at [FISAT TechPlanet](http://iesummit.fisat.ac.in/). Our team consisted of 5, all from college. We won **1st** !

### Hardware Hackathon

I never got into hardware hackathon but I was curious. So I got into a hardware hackathon to see how things were. We didn't win anything but the experience was damn awesome !

### NASA SpaceApps

Level up 🆙. I travelled solo 300KMs to Technopark, Thiruvananthapuram to participate in [NASA's SpaceApps challenge](https://2018.spaceappschallenge.org/). This hackathon was 48 hours and is the best hackathon I've been to.

### IBeto

Winning 2nd position in [HackForTomorrow](#hackfortomorrow) got us into a direct entry to [IBeto](http://ibeto.excelmec.org/). I teamed up with 4 people who I met in HackForTomorrow event. We were able to make an Android app of OpenSalve and make the OpenSalve backend more better.

### Drishti

College of Engineering, Thiruvananthapuram conducts their annual tech fest called Drishti. I participated in MCA deaprtment's DCOD3.0 competition. The aim is to find the best techie. It had interesting various rounds.

There was a round to decrypt encrypted values which I found very interesting. I was able to solve it. It was in PHP. All I had to do was reverse engineer the encrypt function.

At the end, passing through 8 challenges, I won DCOD3.0 !

### LocalHackDay

The last hackathon I've been to is [Local Hack Day](https://localhackday.mlh.io/lhd-2018/events/563-asiet-hack-club). It was a general hackathon and what we made is a [site of information about Linux distributions](http://github.com/subins2000/lisa).

## Hackathons Are Great

I had this worry in my mind of going to hackathon thinking that I won't be able to do well in it and other people are all smarter than me and stuff. I wanted to know what hackathons are like. So I went for [HackForTomorrow](#hackfortomorrow) and enjoyed it !

There are all kinds of people at hackathons. Whether you're an absolute beginner or not, just go. Hackathons are the best place to learn. You're in a room full of vibrant people collaborating and learning together.

And it's not just for coders. A software has all kinds of people involved in it; coders, designers, testers, documentation people and so on. You can pick your path and be in a team.

Hackathons are all about learning, collaborating and sharing together. The experience I got from attending such events is tremendous. And I got to meet a lot of fun, interesting people. And I'm now doing projects with them !

## College

### FOSSers

I talked about [revamping the FOSS club of our college in an old post](//subinsb.com/18-years-revolving-around-sun/#activities).

Here's the update : It's going REALLY WELL ! We now have a good community of people. Got a website set up, social media accounts and did a lot of events ! I should make an entire blog post on FOSSers itself, cause there's lot to say ! (Into the wishlist it goes !).

[FOSSers Website](https://fossers.vidyaacademy.ac.in/)

[FOSSers Instagram](https://instagram.com/FOSSersVAST)

[FOSSers Telegram](https://t.me/FOSSersVAST)

### Life

Pretty rad ! I disliked college at first, but visiting other colleges and seeing the state, I became more happy with my college 😂. Students in my class are not that most-active, but are alright. Just need to push, set them ablaze to do stuff.

The CSE juniors who just came 5 months back are pretty awesome. Vibrant bunch of people.

I'm now more of an extrovert now. I got to know that **there are a lot of interesting, fun people** in my class and college itself. All you gotta do is find them which is why I'm trying to be friends with almost everybody ! I'm now a people person 😁🤗

Every person's interesting and I have something in common to talk to. I'm that middle intersection in a Venn Diagram 🤓

**Introvert -> extrovert 💯**

If I had time and interest, I would have written a lot more. Have an exam on `2019 January 4th`. Oh and BTW, I'm not focusing on academics much anymore.

I'm writing this all in a hurry and I've missed a lot. Perhaps another post soon 🤞

Peace out ✌️