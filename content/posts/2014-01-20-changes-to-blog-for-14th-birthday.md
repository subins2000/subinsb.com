---
title: Blog Changes For My 14th Birthday
author: Subin Siby
type: post
date: 2014-01-20T02:21:25+00:00
url: /changes-to-blog-for-14th-birthday
authorsure_include_css:
  - 
  - 
  - 
categories:
  - Personal
tags:
  - News

---
My birthday is here. So long to the age 13. I will turn 14 in 3 hours from the published time of this post. My blog has been changed as like my age. This blog will turn **3 years** on **January 30**. A lot of changes happened to this blog since the launch. I still have the memory of the moment when I created this blog with my brother and the help of my neighbor Simraj. I will always be thankful to Simraj bro for giving his time to help me create this blog. Let&#8217;s get to the point. I have made some huge changes to this blog (At Server).

# Removed HAproxy

You might be wondering why I removed haproxy, the leading load balancer software. The haproxy I installed is from Openshift. They use their haproxy for other purposes like connecting my app to other nodes which makes the site very slow and causes 503 downtimes most of the time. So I removed Haproxy.

# Moved Domain

The original domain (blog-subins.rhcloud.com) is moved to main-subins.rhcloud.com. I did this because Openshift doesn&#8217;t allow you to remove the web load balancer (haproxy) from an app after it&#8217;s created. Since I have to remove haproxy, I had to clone my app with out haproxy. This changed the domain.

# Updated Description

Since I&#8217;m 14 years old now, I had to change the description. There are many sites where I added 13 years mentions. I have to find all those sites and update it something else apart from the 14 year old mention. Because In 2015, I have to change it again.