---
title: Native JavaScript Equivalents of jQuery
author: Subin Siby
type: post
date: 2014-03-31T18:11:16+00:00
url: /jquery-to-javascript
authorsure_include_css:
  - 
  - 
  - 
categories:
  - JavaScript
  - jQuery
tags:
  - Function

---
If you want to do some little task on your website, you don&#8217;t need **jQuery**. But if you only did work with jQuery, you won&#8217;t know the JavaScript functions that do the same thing. I have ran in to this problem and found the real JavaScript functions in replacement of jQuery functions. I&#8217;m going to share it with you. This post will continue to expand when jQuery adds new functions.


## Selector

jQuery selects element using **Sizzle Selector**, a small library containing in the jQuery source file. In JavaScript you select elements using one of the following doing various type of selections :

<pre class="prettyprint"><code>document.getElementById
document.getElementsByClassName
document.getElementsByName
document.getElementsByTagName
document.getElementsByTagNameNS</code></pre>

jQuery element selecting is like mentioning an element in **CSS**. There is a native **JavaScript** function that selects elements like CSS selecting of an element. Here is an example of selecting a div element of id **firstElem** that has the **body** parent in both jQuery and JavaScript :**
  
**

<pre class="prettyprint"><code>$("body div#firstElem")</code></pre>

<pre class="prettyprint"><code>document.querySelector("body div#firstElem")</code></pre>

Both selects the same element. JavaScript will return only a single element if you select elements using **querySelector()**. You should use **querySelectorAll**, **getElementsByClassName** or **getElementsByTagName** for getting an array of elements.

## Functions

In the following examples, first is jQuery code and the second is the JavaScript code that does the same task.

### .text()

    $("#elem").text();

    document.getElementById("elem").innerText;

### .html()

<pre class="prettyprint"><code>$("#elem").html();</code></pre>

<pre class="prettyprint"><code>document.getElementById("elem").innerHTML;</code></pre>

### .each()

<pre class="prettyprint"><code>$("div").each(function(){
 console.log($(this).html());
});</code></pre>

<pre class="prettyprint"><code>nodes=document.getElementsByTagName("div");
for(i=0;i&lt;nodes.length;i++){
 console.log(nodes[i].innerHTML);
}</code></pre>

### .find()

<pre class="prettyprint"><code>$("#elem").find("#saves");</code></pre>

<pre class="prettyprint"><code>function checkChildren(nodes, elemId){
 for(i=0;i&lt;nodes.length;i++){
  if(nodes[i].id==elemId){
   return nodes[i];
  }else{
   return checkChildren(nodes[i].children, elemId);
  }
 }
}
nodes=document.getElementById("elem").children;
checkChildren(nodes, "saves");
</code></pre>

### .post()

<pre class="prettyprint"><code>$.post("http://subinsb.com/request.php", {"who" : "subin", "what" : "personalBelongings"}, function(data){
 console.log(data);
});</code></pre>

<pre class="prettyprint"><code>xhr=new XMLHttpRequest();
xhr.open("POST", "http://subinsb.com/request.php", true);
xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
xhr.send("who=subin&what=personalBelongings");
xhr.onreadystatechange=function(){
 if(xhr.readyState==4 && xhr.status==200){
  console.log(xhr.responseText);
 }
}</code></pre>

### .get()

<pre class="prettyprint"><code>$.get("http://subinsb.com/request.php", {"who" : "subin", "what" : "personalBelongings"}, function(data){
 console.log(data);
});</code></pre>

<pre class="prettyprint"><code>xhr=new XMLHttpRequest();
xhr.open("GET", "http://subinsb.com/request.php?who=subin&what=personalBelongings", true);
xhr.send();
xhr.onreadystatechange=function(){
 if(xhr.readyState==4 && xhr.status==200){
  console.log(xhr.responseText);
 }
}</code></pre>

### .load()

$("#loadTo").load("http://subinsb.com");

<pre class="prettyprint"><code>xhr=new XMLHttpRequest();
xhr.open("GET", "http://subinsb.com/request.php?who=subin&what=personalBelongings", true);
xhr.send();
xhr.onreadystatechange=function(){
 if(xhr.readyState==4 && xhr.status==200){
  document.getElementById("loadTo").innerHTML = xhr.responseText;
 }
}</code></pre>

### .hide()

    $("#elem").hide();

    document.getElementById("elem").style.display = "none";

### .show()

<pre class="prettyprint"><code>$("#elem").show();</code></pre>

<pre class="prettyprint"><code>document.getElementById("elem").style.display = "block";</code></pre>

### .toggle()

<pre class="prettyprint"><code>$("#elem").toggle();</code></pre>

<pre class="prettyprint"><code>document.getElementById("elem").style.display = document.getElementById("elem").style.display=="block" ? "none":"block";</code></pre>

### .remove()

<pre class="prettyprint"><code>$("#elem").remove();</code></pre>

<pre class="prettyprint"><code>document.getElementById("elem").remove();</code></pre>

### .append()

<pre class="prettyprint"><code>$("#elem").append("I'm a good guy.");</code></pre>

<pre class="prettyprint"><code>document.getElementById("elem").innerHTML += "I'm a good guy.";</code></pre>

## Conclusion

As you can see, jQuery codes are very small compared to the JavaScript codes. But JavaScript takes less time to execute rather than jQuery. You can use jQuery if you&#8217;re web page doesn&#8217;t have a lot of codes. If you&#8217;re web page has lot of codes in jQuery, then it&#8217;s better if you change the codes to native JavaScript to increase page performance. Also, when you need to accomplish a small task, use JavaScript.

More to come in the next part of this.

 [1]: #selector
 [2]: #functions
 [3]: #text
 [4]: #html
 [5]: #each
 [6]: #find
 [7]: #post
 [8]: #get
 [9]: #load
 [10]: #hide
 [11]: #show
 [12]: #toggle
 [13]: #remove
 [14]: #append
 [15]: #conclusion
