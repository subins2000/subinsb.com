---
title: 'New Theme : Light, Simple & Fast'
author: Subin Siby
type: post
date: 2014-03-31T06:21:55+00:00
url: /subins-blog-v1
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML5
  - Themes
  - WordPress
tags:
  - Browsers
  - Responsive

---
As you can see, I changed the theme of this blog with a new theme. If you have visited <a href="http://css-tricks.com" target="_blank">css-tricks.com</a>, you can see there are similarities to it&#8217;s theme and mine. I really liked Chris Coyer&#8217;s theme and I contacted him to lend me the theme. But, no response. So, I decided that I should create my own theme.

By WordPress Support forum, I found <a href="http://underscores.me/" target="_blank">_s site</a>. Underscores provide a starter theme for WordPress. It can be downloaded, make changes as we like. I downloaded it and made it in to this awesome theme. I&#8217;m calling this theme **Subin&#8217;s Blog V1**.<figure id="attachment_2675" class="wp-caption aligncenter">

[<img class="size-full wp-image-2675" alt="Version 1 of Subin's Blog Theme" src="//lab.subinsb.com/projects/blog/uploads/2014/03/0112.png" width="1367" height="654" />][1]<figcaption class="wp-caption-text">Version 1 of Subin&#8217;s Blog Theme</figcaption></figure> 

The Navigation menu is mixed with colors and it has a drop down with the same color as that of the parent navigation link.


# Features

## Responsive

The theme was created to be responsive to all screen sizes. I don&#8217;t have a smart phone, but when I resize the window it&#8217;s adapting to the screen size. Here are some screenshots of the theme in different screen resolutions.<figure id="attachment_2676" class="wp-caption aligncenter">

[<img class="size-full wp-image-2676" alt="Mobile Display" src="//lab.subinsb.com/projects/blog/uploads/2014/03/0113.png" width="283" height="487" />][4]<figcaption class="wp-caption-text">Mobile Display</figcaption></figure> <figure id="attachment_2677" class="wp-caption aligncenter">[<img class="size-full wp-image-2677" alt="On a 640x480 resolution" src="//lab.subinsb.com/projects/blog/uploads/2014/03/0114.png" width="640" height="481" />][5]<figcaption class="wp-caption-text">On a 640&#215;480 resolution</figcaption></figure> 

&nbsp;<figure id="attachment_2678" class="wp-caption aligncenter">

[<img class="size-full wp-image-2678" alt="On a 1024x768 resolution" src="//lab.subinsb.com/projects/blog/uploads/2014/03/0115.png" width="1026" height="764" />][6]<figcaption class="wp-caption-text">On a 1024&#215;768 resolution</figcaption></figure> 

## Light

The theme was created to be light. It has only **500** lines of **CSS** code which is served compressed in the blog. The whole theme is of size **85 KB** and most of the files in the theme is not used. So the actual size of theme is less than 85 KB.

The theme doesn&#8217;t cause any PHP errors as far as I tested. So, the error log is comparitevely small.

## No jQuery

By default, WordPress loads **jQuery** on every pages. But, on this theme I have disabled the loading of jQuery because no component of the theme needs jQuery. Even the navigation drop down feature is made possible by **CSS**. Widgets that uses jQuery was replaced with native JavaScript and due to this, page loads more fast.

## Fast

Pages using the theme loads comparitevely fast. According to the test ran using Pingdom, page loads **98 % faster than all tested websites**. You can see the full report <a href="http://tools.pingdom.com/fpt/#!/esX1M0/http://subinsb.com" target="_blank">here</a>.

The page load time is less because there is no loading of **jQuery**, **jQuery Migrate** and other silly JavaScript files. I&#8217;m really happy with this theme. But there are some bugs in there somewhere. When I think it&#8217;s all perfect, I may post the code on GitHub.

## Cross Browser Support

I have tested the theme on different browsers. Almost all of the browsers give positive result as I expected. Firefox 9.0 didn&#8217;t render the site properly probably because it doesn&#8217;t support **HTML 5**. I haven&#8217;t tested this on IE. But, why should I ? Who uses IE anymore ?

## Help

If you found any flaws or problem with the theme, please please report it to me via comments or by sending an email to mail@subinsb.com

Version 1 of Subin&#8217;s Blog Theme

 [1]: //lab.subinsb.com/projects/blog/uploads/2014/03/0112.png
 [2]: #features
 [3]: #responsive
 [4]: //lab.subinsb.com/projects/blog/uploads/2014/03/0113.png
 [5]: //lab.subinsb.com/projects/blog/uploads/2014/03/0114.png
 [6]: //lab.subinsb.com/projects/blog/uploads/2014/03/0115.png
 [7]: #light
 [8]: #no-jquery
 [9]: #fast
 [10]: #cross-browser-support
 [11]: #help
