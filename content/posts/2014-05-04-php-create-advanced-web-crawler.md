---
title: Create An Advanced Crawler In PHP
author: Subin Siby
type: post
date: 2014-05-04T17:06:47+00:00
url: /php-create-advanced-web-crawler
authorsure_include_css:
  - 
  - 
  - 
categories:
  - HTML
  - PHP
tags:
  - Crawler
  - HTTP
  - Tutorials

---
Crawlers are everywhere. They move on and on to many webpages each second. The most biggest of them are Google&#8217;s. It already crawled almost 90% of the web and is still crawling. If you&#8217;re like me and want to create a more advanced crawler with options and features, this post will help you.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?class=26" target="_blank">Download</a><a class="demo" href="http://demos.subinsb.com/php/advanced-crawler" target="_blank">Demo</a>
</div>

When I created my Search Engine test project, it needed an awesome crawler. I knew that developing one on my own will be hard especially when you have to follow **robots.txt** and other rules. So, I googled and found this <a href="http://phpcrawl.cuab.de/" target="_blank">great library</a> which is 10 years old !

I tested it and it was great. It had all the cool features. It follows **robots.txt**, it acts like a browser when visiting sites. So, the site owner won&#8217;t know what visited just then.


## Download & Install

You first have to download the library from the <a href="http://sourceforge.net/projects/phpcrawl/files/PHPCrawl/" target="_blank">projects&#8217; website</a>. The latest versions from **0.82** is recommended. After downloading, extract the main folder **PHPCrawl\_{version\_number}** to your site&#8217;s source root. Rename this folder to "PHPCrawl", so that when new version code are extracted, the folder name remains the same.

We&#8217;ll use the files in this extracted folder to create our crawler.

Here&#8217;s a Directory tree :

<pre class="prettyprint"><code>PHPCrawl
    ... Crawler Library Files Here
index.php</code></pre>

We will make our crawler in the **index.php** file.

## Customize Crawler

As I said before, we&#8217;ll write the code for the crawler in **index.php** file. You can type it on another file if you want.

We include the crawler library file first inside PHP Tags :

<pre class="prettyprint"><code>&lt;?
include("/PHPCrawl/libs/PHPCrawler.class.php");</code></pre>

Then, we extend the **PHPCrawl** class as to suit our needs. By using this code, we replace the default function that is called when a document is loaded by the crawler :

<pre class="prettyprint"><code>class SBCrawler extends PHPCrawler { 
 function handleDocumentInfo(PHPCrawlerDocumentInfo $p){ 
  $pageurl= $p-&gt;url;
  $status = $p-&gt;http_status_code;
  $source = $p-&gt;source;
  if($status==200 && $source!=""){
   // Page Successfully Got
   echo $pageurl."&lt;br/&gt;";
  }
 }
}</code></pre>

Note that in the above code, we check if the HTTP status code of the page is 200 which is **OK** and if the source (page HTML code) is not null. If the conditions are met, then the URL then crawled is printed.

If you want to manipulate or extract contents from the DOM, you can use PHP libraries like **Simple HTML DOM**. Here is an example of using Simple HTML DOM to get the contents of the "<title>" tag :

<pre class="prettyprint"><code>if($status == 200 && $source != ""){
 // Page Successfully Got
 $html = str_get_html($source );
 if(is_object($html)){
  $t = $html-&gt;find("title", 0);
  if($t){
   $title = $t-&gt;innertext;
  }
  echo $title." - ".$pageurl."&lt;br/&gt;";
  $html-&gt;clear(); 
  unset($html);
 }
}</code></pre>

Note that, you have to include the **Simple HTML DOM** library and you include the above code in the custom class **SBCrawler** we created before.

If you want to make customizations to the document info got from the crawler, see the full document info you can <a href="phpcrawl.cuab.de/classreferences/PHPCrawlerDocumentInfo/overview.html" target="_blank">get from it here</a> and it&#8217;s recommended that you put the code inside the status code checking we did on **SBCrawler** class.

## Create Crawler

Now, we make the function **crawl** which creates the class object and set the options for crawling :

<pre class="prettyprint"><code>function crawl($u){
 $C = new SBCrawler();
 $C-&gt;setURL($u);
 $C-&gt;addContentTypeReceiveRule("#text/html#");
 $C-&gt;addURLFilterRule("#(jpg|gif|png|pdf|jpeg|svg|css|js)$# i"); /* We don't want to crawl non HTML pages */
 $C-&gt;setTrafficLimit(2000 * 1024);
 $C-&gt;obeyRobotsTxt(true); /* Should We follow robots.txt */
 $C-&gt;go();
}</code></pre>

You can set <a href="http://phpcrawl.cuab.de/classreferences/PHPCrawler/overview.html" target="_blank">more options too</a>. Here are some of the main options that I think you&#8217;ll need :

<table class="table">
  <tr>
    <td>
      Code
    </td>
    
    <td>
      Description
    </td>
  </tr>
  
  <tr>
    <td>
      $C-><a href="http://phpcrawl.cuab.de/classreferences/PHPCrawler/method_detail_tpl_method_obeyRobotsTxt.htm" target="_blank">obeyRobotsTxt</a>(true)
    </td>
    
    <td>
      Whether the crawler should obey the robots.txt file of domain
    </td>
  </tr>
  
  <tr>
    <td>
      $C-><a href="http://phpcrawl.cuab.de/classreferences/PHPCrawler/method_detail_tpl_method_setFollowMode.htm" target="_blank">setFollowMod</a>e(2)
    </td>
    
    <td>
      The type of follow mode. Default 2 (crawls only URLs in the same domain). 0 for all domain URLs.
    </td>
  </tr>
  
  <tr>
    <td>
      $C-><a href="http://phpcrawl.cuab.de/classreferences/PHPCrawler/method_detail_tpl_method_setPageLimit.htm" target="_blank">setPageLimit</a>(0)
    </td>
    
    <td>
      How many pages should the crawler crawl. Default 0 (no limit)
    </td>
  </tr>
  
  <tr>
    <td>
      $C-><a href="http://phpcrawl.cuab.de/classreferences/PHPCrawler/method_detail_tpl_method_setUserAgentString.htm" target="_blank">setUserAgentString</a>("PHPCrawl")
    </td>
    
    <td>
      The User Agent String. Crawler visits site with this string so that the owner knows what visited.
    </td>
  </tr>
  
  <tr>
    <td>
      $C-><a href="http://phpcrawl.cuab.de/classreferences/PHPCrawler/method_detail_tpl_method_enableCookieHandling.htm" target="_blank">enableCookieHandling</a>(true)
    </td>
    
    <td>
      Whether the crawler should acts like a browser (storing cookies).
    </td>
  </tr>
</table>

The possibilities are endless. As you can see, you can make many customizations of how the crawler should be and act like.

## Start Crawling

It&#8217;s time to give a URL to **crawl()** and start the crawling processes. Here&#8217;s how you start crawling the site "http://subinsb.com" :

<pre class="prettyprint"><code>crawl("http://subinsb.com");</code></pre>

Crawling process can be time consuming and don&#8217;t worry that the script takes too much to load. If you want to display the results immediately after each crawl, use **flush()** after the **echo** in **SBCrawler** class. Here&#8217;s a sample :

<pre class="prettyprint"><code>if($status == 200 && $source != ""){
 // Page Successfully Got
 echo $pageurl."&lt;br/&gt;";
 flush();
}</code></pre>

You can see the demo of this **flush()** <a href="http://demos.subinsb.com/php/advanced-crawler/" target="_blank">here</a>.

PHPCrawl library will be updated in course of time and it&#8217;s features will rise and rise in time. Hope you succeeded through this tutorial and had a great result. If you had a problem with something, feel free to comment and I&#8217;ll be here to help you out. 😀

 [1]: #download-install
 [2]: #customize-crawler
 [3]: #create-crawler
 [4]: #start-crawling
