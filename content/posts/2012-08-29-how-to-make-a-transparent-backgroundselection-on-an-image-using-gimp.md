---
title: How to make a transparent background/selection on an image using GIMP
author: Subin Siby
type: post
date: 2012-08-29T13:10:00+00:00
excerpt: 'Open your image in GIMP.Select the area you want to make transparent.Select the appropriate selection tool from the Tool window or the Tools - Selection Tools menu on the Layer window.&nbsp;I usually use the magic wand/fuzzy select&nbsp;(Select contigu...'
url: /how-to-make-a-transparent-backgroundselection-on-an-image-using-gimp
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - d3d9e0654f286c6ca65c5256616d509b
  - d3d9e0654f286c6ca65c5256616d509b
  - d3d9e0654f286c6ca65c5256616d509b
  - d3d9e0654f286c6ca65c5256616d509b
  - d3d9e0654f286c6ca65c5256616d509b
  - d3d9e0654f286c6ca65c5256616d509b
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
blogger_permalink:
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
  - http://sag-3.blogspot.com/2012/08/how-to-make-transparent.html
categories:
  - Linux
  - Ubuntu
tags:
  - GIMP

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">Open your image in <b>GIMP</b>.</span><br /><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;"><br /></span>
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">Select the area you want to make transparent.</span><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">Select the appropriate selection tool from the Tool window or the <b>Tools &#8211; Selection Tools</b> menu on the <b>Layer window</b>.&nbsp;</span>
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">I usually use the magic wand/fuzzy select&nbsp;(Select contiguous region) tool or the Select regions by color tool.</span><br /><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;"><br /></span>
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">Click in the region you want selected.&nbsp; Use shift-click to add more regions/colors.</span>
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;">Tip: It&#8217;s easier if you zoom in (View &#8211; Zoom menu) to see finer details of&nbsp;exactly what you&#8217;re selecting.</span><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;"><br /></span><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;"><br /></span>
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">In the Layer window (the one showing your image), select <b>Layer &#8211; Transparency &#8211; Add Alpha Channel</b>.&nbsp; If this is blanked out then it&#8217;s already done.&nbsp; This makes sure your image can store transparency data.</span><br /><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">Select Edit &#8211; Clear.&nbsp; This makes the selection transparent.</span><br /><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">Save the file.&nbsp;</span><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;">&nbsp;</span>
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;"><br /></span>
  </div>
  
  <div style="text-align: left;">
    <span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: -webkit-auto;"><b>Note</b>: If you save it as a <b>PNG</b> file, be sure to select the &#8216;Save colour values from transparent pixels&#8217; option in the Save as <b>PNG</b> dialog box.</span>
  </div>
</div>