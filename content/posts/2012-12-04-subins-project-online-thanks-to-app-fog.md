---
title: Subins Project Online – Thanks to App Fog
author: Subin Siby
type: post
date: 2012-12-04T16:24:00+00:00
excerpt: "You might have know the project I have been working on. The Subins Project. I have spent a year on this project. My wish was to host this on a site. But I couldn't afford the hosting charges cause I'm 12 and my dad won't lend me money.I took a leave be..."
url: /subins-project-online-thanks-to-app-fog
authorsure_include_css:
  - 
  - 
  - 
syndication_item_hash:
  - e5bc3555d99dec8d0ee244add8a61be3
  - e5bc3555d99dec8d0ee244add8a61be3
  - e5bc3555d99dec8d0ee244add8a61be3
  - e5bc3555d99dec8d0ee244add8a61be3
  - e5bc3555d99dec8d0ee244add8a61be3
  - e5bc3555d99dec8d0ee244add8a61be3
syndication_permalink:
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
blogger_permalink:
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
  - http://sag-3.blogspot.com/2012/12/subins-project-online-thanks-to-app-fog.html
categories:
  - Linux
  - Personal
  - PHP
  - Ubuntu
tags:
  - AppFog
  - Subins

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  You might have know the project I have been working on. <b>The Subins Project</b>. I have spent a year on this project. My wish was to host this on a site. But I couldn&#8217;t afford the hosting charges cause I&#8217;m 12 and my dad won&#8217;t lend me money.</p> 
  
  <p>
    I took a leave because of fever. On that day I was browsing the internet and out of nowhere I saw a post about a site named <b><a href="http://appfog.com/?utm_source=subinsblog" >AppFog</a></b>. The post said that <b><a href="http://appfog.com/?utm_source=subinsblog" >AppFog</a></b>&nbsp;gives free hosting on their servers under their domain name. So quickly I went to <b><a href="http://appfog.com/?utm_source=subinsblog" >AppFog</a></b>&nbsp;and signed up. It was pretty easy. I created my site in the domain <a href="http://subins.hp.af.cm/?utm_source=subinsblog" style="font-weight: bold;" >subins.hp.af.cm</a>.
  </p>
  
  <p>
    I uploaded my project using a&nbsp;<b>RubyGem </b>named <b>AF</b>. I just need to install the <b>GEM</b>&nbsp;and use the command <b>af.</b>&nbsp;Since I uses <b>Linux</b>&nbsp;it was pretty easy. I Love <a href="http://appfog.com/?utm_source=subinsblog" >AppFog</a>.
  </p>
  
  <p>
    Then I created more sites associated with <b>Subins</b>. The services which have been uploaded and full working are the following :-
  </p>
  
  <ol style="text-align: left;">
    <li>
      <a href="http://subins.hp.af.cm/?utm_source=subinsblog" >Subins</a>
    </li>
    <li>
      <a href="http://accounts-subins.hp.af.cm/?utm_source=subinsblog" >Accounts</a>
    </li>
    <li>
      <a href="http://chat-subins.hp.af.cm/?utm_source=subinsblog" >Chat</a>
    </li>
    <li>
      <a href="http://fd-subins.hp.af.cm/?utm_source=subinsblog" >Friendshood</a>
    </li>
  </ol>
  
  <p>
    I am working on uploading more services I created but I don&#8217;t have time cause <b>Christmas Exam</b>&nbsp;is coming soon and mom won&#8217;t let me use the internet. <b>Exams </b>will start on 13 December and will end on 21 December. I will upload more services after the exams.
  </p>
  
  <p>
    BTW signup for <b>Subins </b>@ <a href="http://accounts-subins.hp.af.cm/signup.php" style="font-weight: bold;" >Subins Signup Page</a>.<br />Hope you like it 🙂</div>