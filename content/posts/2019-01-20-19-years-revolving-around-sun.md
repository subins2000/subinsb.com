---
title: 19 Years Of Revolving Around The Sun
author: Subin Siby
type: post
date: 2019-01-20T06:29:06+00:00
lastmod: 2019-03-11T06:00:06+00:00
url: /19-years-revolving-around-sun
aliases:
    - /19
categories:
  - Personal
tags:
  - birth

---

Writing this at [CUSAT MakeATon Hackathon](https://dhishna.org/make-a-ton/).

Part of the revolving around series :

* [18 years of revolving around the sun](/18)
* [17 years of revolving around the sun](/17)

```python
age = 18
age = age + 1
```

I have officially revolved around the sun 19 times. 19 years of existence. This is the beginning of my last teenage year. I feel old.

A tagline that I've been using these days is "A 21st century teen". Well that's gonna go off in a year.

## Coding

I participated in a lot of hackathons this year. Well, I'm writing this from one ! And my GitLab & GitHub profile is lit this year !

* Made some cool projects and learnt new stuff.
* Networked with a lot of people and made some cool new friends !

**I MET RICHARD STALLMAN** this year at Kochi Muziris Biennale ! One of the living greatest person on this planet. Got a really weird pic with him, cause he don't like taking photos :P

One quote he said during the talk was hilarious :

> Calling me the father of open source is like calling Ambedkar the father of BJP

I'm also trying to participate in Google Summer of Code this year. I have to work full time for it and I hope I can. I would really love to work in a large project and make a significant contribution to a FOSS project. I'm currently looking at KDE cause I use it everyday and they have really cool projects (Have I mentioned how much I love KDEConnect :P).

## College

The past 3rd semester, I was more outside of college than in. [More about my hackathon experiences](/2018). That's probably gonna affect my grades this semester. I seriously doubt I'll pass all the subjects last semester.

FOSSers is getting very active. We now have a [good website up and running](http://fossers.vidyaacademy.ac.in/). The community is growing and I'm very happy and excited to lead it. We're one of the most active FOSS clubs in our state now and I'm proud. I never thought that I had a leader in me. I always doubted that. But I'm now realising that I'm capable of being a leader :)

Discovered more friends ! I met a gal and a guy who are Pewdiepie fans. This is a first. Bros 👊. This gal was the first new person I met in first day of college. There was this orientation class and we were divided into groups and she was in my group. I made some lame jokes (`Subin v17.08` `lame-jokes-bug`) and yet she laughed to it :D. I found her laugh pretty cute. Never thought she was a Pewdiepie fan. In my place, it's pretty rare to find such people.

Well... after that, we didn't even speak or mind at all for over a year (MY FAULT. Another `Subin v17.08` bug). And now we've renewed that one-day friendship again. I absolutely regret not doing it earlier. Here's one quote to remember :

> At the end of the day, you'll regret the chances you didn't take

I've also made friends with some new cool people. What's interesting is that the friendship happened online through messages. We're in the same college yet we haven't talked directly 

I've said this many times and I'm saying it again :

> **I have a variety of interests and lots of stuff to talk about. I'm that middle intersection in a Venn Diagram 🤓**

I mentioned about the possibilty of getting a scholarship in the [last post](/18). I GOT IT ! So less burden on the money part :)

## Personal

My grandma died just a week ago. Every year for birthday, she would call me first in the morning and wish me. This year, there won't be a call :(

![The best picture I have with grandma. Took this placing the camera on a table and controlling it with an Android app](//lab.subinsb.com/projects/blog/uploads/2019/03/mummy.jpeg)

> The best picture I have with grandma. Took in June, 2018. `Took this placing the camera on a table and controlling it with an Android app`

I stayed along with her for the first time ever in last June during semester break. I'm glad I did that. Atleast I could spend some happy moments with her :)

I've never lost someone so dearest to me. Death is hard to comprehend.

I hope to do more stuff in the future.

> വെറുതെ ഇരുന്നിട്ട് കാര്യമില്ല

and meet new people...

> I am because we are ❤️