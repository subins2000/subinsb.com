---
title: Program to Replace localhost links with website url
author: Subin Siby
type: post
date: 2013-08-24T03:30:00+00:00
excerpt: 'If you are working on a project and wants to upload the project to your site, then you have to manually replace all the localhost links with the site url. I created a small Python&nbsp;program that will replace localhost links with your WWW&nbsp;site u...'
url: /program-to-replace-localhost-links-with-website-url
authorsure_include_css:
  - 
  - 
  - 
enclosure:
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
  - |
    |
        
        
        
syndication_source:
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
  - 'Subin&#039;s Blog'
syndication_source_uri:
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
  - http://sag-3.blogspot.com/
syndication_source_id:
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
  - tag:blogger.com,1999:blog-1163006449268681643
syndication_feed:
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
  - 'http://sag-3.blogspot.in/rss.xml?redirect=false&start-index=1&max-results=500'
syndication_feed_id:
  - 2
  - 2
  - 2
  - 2
  - 2
  - 2
syndication_permalink:
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
blogger_permalink:
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
  - http://sag-3.blogspot.com/2013/08/python-replace-localhost-links-with-website-url.html
syndication_item_hash:
  - 6fedd2e2c4fef237522fc840fbc45b4f
  - 6fedd2e2c4fef237522fc840fbc45b4f
  - 6fedd2e2c4fef237522fc840fbc45b4f
  - 6fedd2e2c4fef237522fc840fbc45b4f
  - 6fedd2e2c4fef237522fc840fbc45b4f
  - 6fedd2e2c4fef237522fc840fbc45b4f
categories:
  - Linux
  - Localhost
  - Python
tags:
  - Terminal
  - URL
  - WWW

---
<div dir="ltr" style="text-align: left;" trbidi="on">
  If you are working on a project and wants to upload the project to your site, then you have to manually replace all the localhost links with the site url. I created a small <b>Python</b>&nbsp;program that will replace localhost links with your <b>WWW</b>&nbsp;site url.<br />Here is the <b>Python</b>&nbsp;code :</p> 
  
  <blockquote class="tr_bq">
    <p>
      #!/usr/bin/python<br />import os<br />indir = &#8216;<span style="color: red;">/var/www/mysite</span>&#8216; #!Folder of your localhost site.<br />for root, dirs, filenames in os.walk(indir):<br />&nbsp;for f in filenames:<br />&nbsp; log = open(os.path.join(root, f), &#8216;r+&#8217;)<br />&nbsp; f=log.read() &nbsp; <br />&nbsp; n=f.replace("<span style="color: red;">http://localhost</span>", "<span style="color: red;">http://mysite.com</span>")<br />&nbsp; log.seek(0)<br />&nbsp; log.truncate()<br />&nbsp; log.write(n)<br />&nbsp; log.close()<br />print "Successfully Replaced."
    </p>
  </blockquote>
  
  <p>
    Change the location of your localhost site and the <b>localhost</b>&nbsp;link and <b>mysite</b> link to your site address&nbsp;above if you want to change it. Then save this file as <b>link-replace.py</b>. <a href="http://sag-3.blogspot.com/2013/07/making-file-executable-linux.html" >Make the file executable</a>&nbsp;and run it in <b>terminal</b>. The program will automatically replace the links and will print out "<b>Successfully Replaced</b>" if the program completed replacing links.</div>