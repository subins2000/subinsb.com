---
title: PHP Secure, Advanced Login System
author: Subin Siby
type: post
date: 2014-05-20T16:40:49+00:00
lastmod: 2018-09-04
url: /php-logsys
categories:
  - PHP
tags:
  - Hash
  - Log In
  - PDO
  - Secure
  - Tutorials

---
We all get new ideas every day. With the idea, we build more and more sites. Mostly, the sites would have a login system, right ? So, when we create new projects (sites) every time, we will have to create login systems for each one. I do the same thing and I&#8217;m tired of creating the same thing over and over again. So, I decided to create a login system that can be integrated to any **PHP** powered sites.

<div class="padlinks">
  <a class="download" href="http://demos.subinsb.com/down.php?id=nu98sx5w7y5l/s/6m5cmj3vznxubiou9zem&class=27" target="_blank" rel="noopener">Download</a><a class="demo" href="http://demos.subinsb.com/php/loginSystem" target="_blank" rel="noopener">Demo</a>
</div>

## Latest Version

The latest version is **1.0.1** released on February 11, 2018. <a href="https://github.com/subins2000/logSys/blob/master/CHANGELOG.md#09" target="_blank" rel="noopener">See changelog</a>.

### How To Upgrade

logSys can now be installed with composer :

> composer require francium/logsys

NOTE &#8211; If you&#8217;re upgrading from versions less than **0.6**, your users won&#8217;t be able to login with their password. They would have to reset it to a new one.

If you&#8217;re upgrading to **0.7** from **0.6**, your users can login normally like before.

## logSys Admin

**logSys** has an administrator app (Graphic Application) for managing users, statistics and more&#8230; See [this post][4].

## Features

I&#8217;m introducing you to the PHP class **logSys**. This class will help you to set up a login System on any PHP site. logSys have the following features :

  * Supports **MySQL**, **SQLite** and **PostgreSQL**

    logSys uses PDO for database connection
  * Lightweight

    Almost no dependenices &#8211; logSys does not require additional dependencies
  * Login & Registering
  * "Remember Me"
  * 2 Step Verification

    SMS or E-Mail
  * Secure

    Uses password_hash() for hashing passwords

    Protection from Brute Force attacks by disabling login attempts for fixed time after 5 failed login attempts

    Device Management &#8211; Log Out from a device remotely
  * "Forgot Password ?"
  * Custom fields for storing users&#8217; details
  * Easily get and update user details
  * Auto redirection based on the login status of user
  * Extra functions such as

    E-mail validation and random string generator

    Show time since user joined

Yes, it&#8217;s BIG ! The source code is more than **700** lines long and you customize it so much. Here&#8217;s a full guide on how to use **logSys**.

## Requirements

  * PHP 5.5 or later

    If you want to use it in an older PHP version, get the **password.php** file from [here][7] and include it before loading `LS.php` file.
  * MySQL server or PostgreSQL server or SQLite

## Download & Install

Use composer for installing logSys in your project.

<pre class="prettyprint"><code>composer require francium/logsys</code></pre>

Previously logSys could have been used with just a single class file. But as features got added, the dependencies also increased. Hence now it's only possible to install logSys with [composer](https://getcomposer.org/).

## Introduction

For this tutorial, we use the table as "users" (which is changable).

The user can either use E-Mail or username for logging in. An example database structure :

<pre class="prettyprint"><code style="text-align: center;">id | username | email | password | name | created | attempt
</code></pre>

The columns "id", "username" and "password" is absolutely required for the basic setup of logSys. Other columns are for optional features like "Login with E-Mail & Username", "Time Since User Joined" and "Brute Force Protection". You can also change the column names if you want.

You can easily set up the Database table by reading the "[Database Table Setup][11]" section of this tutorial.

Return values of logSys functions are booleans. So use **===** operator instead of **==**.

In this post, we assume that you use "custom configuration" and not the other method to configure. Therefore there will be no including "LS.php" file, but "config.php". Don&#8217;t know what I&#8217;m talking about ? <a title="The Francium Project" href="//subinsb.com/the-francium-project" target="_blank" rel="noopener">Read this</a>.

Make sure you read the next portion carefully and change the config values accordingly.

## Configuration

You have to configure logSys first to continue. You have two options to change configurations. See <a title="The Francium Project" href="//subinsb.com/the-francium-project#1-Custom-Config" target="_blank" rel="noopener">about it here</a> on the "configuration" section.

Here are some of the main options you can set in logSys :

### basic

<table class="table">
  <tr>
    <td>
      Option
    </td>

    <td>
      Description
    </td>

    <td>
      Example
    </td>
  </tr>

  <tr>
    <td>
      company
    </td>

    <td>
      Your site/company name. Used for including in E-Mails to be sent
    </td>

    <td>
      Subin&#8217;s Blog
    </td>
  </tr>

  <tr>
    <td>
      email
    </td>

    <td>
      Used for "From" header in E-Mails sent
    </td>

    <td>
      my_address@mystie.com
    </td>
  </tr>
</table>

### db

<table class="table">
  <tr>
    <td>
      type
    </td>

    <td>
      Database to use : mysql or sqlite or postgresql
    </td>

    <td>
      mysql
    </td>
  </tr>

  <tr>
    <td>
      sqlite_path
    </td>

    <td>
      Absolute path to SQLite database file
    </td>

    <td>
      /home/simsu/mydb.sqlite
    </td>
  </tr>

  <tr>
    <td>
      host
    </td>

    <td>
      The database Hostname.
    </td>

    <td>
      localhost
    </td>
  </tr>

  <tr>
    <td>
      port
    </td>

    <td>
      The database Port
    </td>

    <td>
      3306
    </td>
  </tr>

  <tr>
    <td>
      username
    </td>

    <td>
      The database username
    </td>

    <td>
      root
    </td>
  </tr>

  <tr>
    <td>
      password
    </td>

    <td>
      The database password for the username
    </td>

    <td>
      mypassword
    </td>
  </tr>

  <tr>
    <td>
      name
    </td>

    <td>
      The database name
    </td>

    <td>
      projects
    </td>
  </tr>

  <tr>
    <td>
      table
    </td>

    <td>
      The database table to store users&#8217; data
    </td>

    <td>
      users
    </td>
  </tr>

  <tr>
    <td>
      token_table
    </td>

    <td>
      The database table to store tokens used for "Forgot Password" etc.
    </td>

    <td>
      user_tokens
    </td>
  </tr>

  <tr>
    <td>
      <a href="#columns">columns</a>
    </td>

    <td>
      Array of column names to be used
    </td>

    <td>
      See <a href="#columns">below</a>
    </td>
  </tr>
</table>

### columns

You can change the column names used by logSys. Just pass the alternative column name as value to the array :

<pre class="prettyprint"><code>array(
    "id" =&gt; "user_id",
    "username" =&gt; "my_column_username",
    "password" =&gt; "col_password",
    "email" =&gt; "user_email",
    "attempt" =&gt; "brute_force_check"
)
</code></pre>

The above code will make logSys use the column "user_id" instead of "id" and others likewise.

### keys

<table class="table">
  <tr>
    <td>
      cookie
    </td>

    <td>
      A secure key that is used to encrypt cookie values
    </td>

    <td>
      sadauj3n(#ff)#%$
    </td>
  </tr>

  <tr>
    <td>
      salt
    </td>

    <td>
      The common password salt (site salt) that is used to hash all the users&#8217; passwords.<br /> This should not be changed after a user is created
    </td>

    <td>
      vdsjn3md%##$*5
    </td>
  </tr>
</table>

Make sure the secure keys for both hashing passwords and cookies are **different** and secure. Don&#8217;t let anyone know them.

If you change the **salt** value later, users won&#8217;t be able to login with their existing passwords.

If you change the **cookie** value later, all cookies will become invalid and user sessions will be terminated which will make them login again.

### features

<table class="table">
  <tr>
    <td>
      start_session
    </td>

    <td>
      Should the class call session_start();
    </td>

    <td>
      true (boolean)
    </td>
  </tr>

  <tr>
    <td>
      email_login
    </td>

    <td>
      Should the class allows logging in with both E-Mail & Username ?
    </td>

    <td>
      true (boolean)
    </td>
  </tr>

  <tr>
    <td>
      remember_me
    </td>

    <td>
      If value is provided to Fr\LS::login(), should user be automatically logged in even after the end of PHP session.
    </td>

    <td>
      true (boolean)
    </td>
  </tr>

  <tr>
    <td>
      auto_init
    </td>

    <td>
      Should Fr\LS::init() be ran automatically on the first logSys function called in any page ?
    </td>

    <td>
      false (boolean)
    </td>
  </tr>

  <tr>
    <td>
      block_brute_force
    </td>

    <td>
      Prevent Brute Forcing.<br /> By enabling this, logSys will deny login for the time mentioned in the config value "brute_force"->"time_limit" seconds after config value "brute_force"->"tries" incorrect login tries.
    </td>

    <td>
      true
    </td>
  </tr>
</table>

### brute_force

<table class="table">
  <tr>
    <td>
      tries
    </td>

    <td>
      Number of login tries alloted to users
    </td>

    <td>
      5
    </td>
  </tr>

  <tr>
    <td>
      time_limit
    </td>

    <td>
      The time IN SECONDS for which block from login action should be done after incorrect login attempts. Use <a href="http://www.easysurf.cc/utime.htm#m60s" target="_blank" rel="noopener">this tool</a> for converting minutes to seconds. Default : 5 minutes
    </td>

    <td>
      300
    </td>
  </tr>

  <tr>
    <td>
      max_tokens
    </td>

    <td>
      Maximum number of tokens the user can request.
    </td>

    <td>
      5
    </td>
  </tr>
</table>

### pages

<table class="table">
  <tr>
    <td>
      no_login
    </td>

    <td>
      An array containing the relative pathname of pages that doesn&#8217;t require logging in like the index page of site
    </td>

    <td>
      array("/")
    </td>
  </tr>

  <tr>
    <td>
      everyone
    </td>

    <td>
      An array containing the relative pathname of pages that can be accessed by logged in and logged out users
    </td>

    <td>
      array()
    </td>
  </tr>

  <tr>
    <td>
      login_page
    </td>

    <td>
      The relative pathname of login page
    </td>

    <td>
      /login.php
    </td>
  </tr>

  <tr>
    <td>
      home_page
    </td>

    <td>
      The relative pathname of home page that the class should redirect to after logging in
    </td>

    <td>
      /home.php
    </td>
  </tr>
</table>

It&#8217;s not necessary to include the login page and home page in "no_login" array, because logSys automatically adds it into that array.

### cookies

<table class="table">
  <tr>
    <td>
      expire
    </td>

    <td>
      The expire time of the 2 cookies created during login process. This value is used for making the time using strtotime() function. So, the value must be compatible with <a href="http://php.net/manual/en/function.strtotime.php" target="_blank" rel="noopener">strtotime()</a>
    </td>

    <td>
      +30 days
    </td>
  </tr>

  <tr>
    <td>
      path
    </td>

    <td>
      The `path` value of cookies.
    </td>

    <td>
      /
    </td>
  </tr>

  <tr>
    <td>
      domain
    </td>

    <td>
      The `domain` value of cookies. See <a href="http://php.net/manual/en/function.setcookie.php" target="_blank" rel="noopener">setcookie()</a>
    </td>

    <td>
      subinsb.com
    </td>
  </tr>
</table>

### two_step_login

<table class="table">
  <tr>
    <td>
      instruction
    </td>

    <td>
      Message to show before displaying "Enter Token" form.
    </td>

    <td>
      A token was sent to your mobile number as SMS. Please enter it below :
    </td>
  </tr>

  <tr>
    <td>
      send_callback
    </td>

    <td>
      Callback that sends the token to user
    </td>

    <td>
      function($LS, $userID, $token){}
    </td>
  </tr>

  <tr>
    <td>
      devices_table
    </td>

    <td>
      Table in database where users&#8217; device information are stored
    </td>

    <td>
      user_devices
    </td>
  </tr>

  <tr>
    <td>
      token_length
    </td>

    <td>
      The length of token that is generated
    </td>

    <td>
      4
    </td>
  </tr>

  <tr>
    <td>
      token_tries
    </td>

    <td>
      Maximum number of incorrect tries for Two Step Login token the user can do
    </td>

    <td>
      3
    </td>
  </tr>

  <tr>
    <td>
      numeric
    </td>

    <td>
      Whether the token generated should be numeric only
    </td>

    <td>
      true
    </td>
  </tr>

  <tr>
    <td>
      expire
    </td>

    <td>
      The expire duration of the device cookie.<br /> This value is used for making the time using strtotime() function. So, the value must be compatible with <a href="http://php.net/manual/en/function.strtotime.php" target="_blank" rel="noopener">strtotime()</a>
    </td>

    <td>
      +45 days
    </td>
  </tr>

  <tr>
    <td>
      first_check_only
    </td>

    <td>
      Should logSys checks if device is valid, everytime logSys is initiated ie everytime a page loads. If you want to check only the first time a user loads a page, then set the value to TRUE, else FALSE
    </td>

    <td>
      true
    </td>
  </tr>
</table>

### debug

You can make logSys write log files to debug.

<table class="table">
  <tr>
    <td>
      enable
    </td>

    <td>
      Should debugging be enabled
    </td>

    <td>
      false
    </td>
  </tr>

  <tr>
    <td>
      log_file
    </td>

    <td>
      The absolute path to log file
    </td>

    <td>
      /
    </td>
  </tr>
</table>

Make sure the log file is situated outside the public directory of server. Messages are appended to log files and not overwritten.

You can find a sample configuration file [here][22].

## Database Table Setup

### MySQL

You can find the **SQL** code to create the table **users** [here][25].

logSys remembers the table name through "db"->"table". By default it&#8217;s set to "users". You can add extra columns according to your choice. After all, you can ask many info from users.

**Note** : As you can see in the \`users\` table mentioned above as SQL, the username column has a limit of "10" characters. So, you won&#8217;t be able to register users with usernames more than 10 chars. So, increase the limit to allow more characters in username.

For storing the reset password tokens, you need to create an extra table called "user\_tokens". This table name can be changed with config -> db -> tokens\_table option.

You can find the SQL code for creating it [here][26].

The reset password token saving table should be called "resetTokens". It is **not changeable**. If you really want to change it, find the SQL queries in the class file that uses the "resetTokens" table and replace it.

### PostgreSQL

You can find the SQL code [here][28] to create the 3 tables.

### SQLite

The SQL code for creating the 3 tables is different for **SQL****ite**. You can find it [here][30].

## Timezone

Timezone is an important factor in any system that uses time in both database and server. In a login system it&#8217;s extensively used. So don&#8217;t screw it up.

I recommend using **UTC** as the timezone for all your applications. logSys by default doesn&#8217;t set a timezone. You should do it in your servers. See these pages to learn how to set it :

  * <a href="https://stackoverflow.com/a/7587637/1372424" target="_blank" rel="noopener">Set timezone to UTC in PHP</a>
  * <a href="https://stackoverflow.com/a/19075291/1372424" target="_blank" rel="noopener">Set timezone to UTC in MySQL</a>
  * <a href="https://stackoverflow.com/a/11443155/1372424" target="_blank" rel="noopener">Set timezone to UTC in PostgreSQL</a>

## Initialize

Redirection based on the login status is needed for a login system. You should call the **$LS->init();** at the start of every page on your site to redirect according to the login status. You can do this automatically without calling the function manually by setting `config` -> `features` -> `auto_init` to boolean TRUE.

Here is an example :

<pre class="prettyprint"><code>&lt;?php
require "config.php";
$LS-&gt;init();
?&gt;
&lt;html&gt;</code></pre>

and continue with your HTML code. When a user who is not logged in visits a page that is **not** in the `config` -> `pages` -> `no_login` array, the user gets redirected to the login page mentioned in `config` -> `pages` -> `login_page`.

If the user is logged in and is on a page mentioned in the `config` -> `pages` -> `no_login` array, he/she will be redirected to the `config` -> `pages` -> `home_page` URI.

All users who is logged in and not logged in can see the pages mentioned in `config` -> `pages` -> `everyone`. They won&#8217;t be redirected in those pages.

## Login Page

Now, we&#8217;ll set up a login page. All you have to do is make a form and call the **$LS->login()** function with details at starting of the page. Example :

<pre class="prettyprint"><code>&lt;html&gt;
 &lt;head&gt;
  &lt;title&gt;Log In&lt;/title&gt;
 &lt;/head&gt;
 &lt;body&gt;
  &lt;div class="content"&gt;
   &lt;form method="POST"&gt;
    &lt;label&gt;Username / E-Mail&lt;/label&gt;&lt;br/&gt;
    &lt;input name="login" type="text"/&gt;&lt;br/&gt;
    &lt;label&gt;Password&lt;/label&gt;&lt;br/&gt;
    &lt;input name="pass" type="password"/&gt;&lt;br/&gt;
    &lt;label&gt;
     &lt;input type="checkbox" name="remember_me"/&gt; Remember Me
    &lt;/label&gt;
    &lt;button name="act_login"&gt;Log In&lt;/button&gt;
   &lt;/form&gt;
  &lt;/div&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

Now, we process the submitted login data. Place this code at the top of the page before **<html>** :

<pre class="prettyprint"><code>&lt;?php
require "config.php";
$LS-&gt;init();
if(isset($_POST["action_login"])){
  $identification = $_POST['login'];
  $password = $_POST['password'];
  if($identification == "" || $password == ""){
    $msg = array("Error", "Username / Password Wrong !");
  }else{
    $login = $LS-&gt;login($identification, $password, isset($_POST['remember_me']));
    if($login === false){
      $msg = array("Error", "Username / Password Wrong !");
    }else if(is_array($login) && $login['status'] == "blocked"){
      $msg = array("Error", "Too many login attempts. You can attempt login after ". $login['minutes'] ." minutes (". $login['seconds'] ." seconds)");
    }
  }
}
?&gt;</code></pre>

The syntax for using the **$LS->login()** function is this :

<pre class="prettyprint"><code>boolean|array Fr\LS::login($username, $password, $remember_me, $cookies)</code></pre>

The **$username** parameter can be either the **E-Mail** (if `config` -> `features` -> `email_login` config value is boolean **TRUE**) or the username of user.

The **$remember_me** parameter (default FALSE) should be set to boolean **TRUE**, if the user needs to be remembered even after the end of the PHP session that is the user is automatically logged in after he/she visits the page again. But, to enable this, the config value `features` -> `remember_me` must be set to boolean **TRUE**.

The **$cookies** parameter (default TRUE) makes the decision whether cookies should be created or not. This is useful, when you have to check if a username and password is correct without creating any cookies and redirects. This too needs a boolean value.

User will be redirected after logging in if **$cookies** is set to TRUE which is the default value.

You can also login by passing the password value as **NULL** (Thanks <a href="https://disqus.com/home/user/adikedem/" target="_blank" rel="noopener">Adi Kedem</a>). This is useful when you have to login a user without knowing the password like in OAuth login process :

<pre class="prettyprint"><code>$LS-&gt;login($username, null)</code></pre>

But there is a **danger** to this &#8211; a user can enter just by entering the username and no password. To avoid this issue, a checking whether the password is blank is done in **login.php** page :

<pre class="prettyprint"><code>$user = $_POST['login'];
$pass = $_POST['pass'];
if($user == "" || $pass == ""){
    $msg = array("Error", "Username / Password Is Blank !");
}</code></pre>

If brute force check is enabled and if the account is blocked from incorrect login attempts, then an **array** is returned. Here is how the array look like :

<pre class="prettyprint"><code>array(
   "status" =&gt; "blocked",
   "minutes" =&gt; 5
   "seconds" =&gt; 300
)</code></pre>

Both minutes and seconds will be shown. Note that the minutes value is rounded. So, if it&#8217;s actually 4.10 seconds, the minutes value will still be "5".

If the user login is successful, a boolean **TRUE** is returned, otherwise a boolean **FALSE** unless the account is blocked for which an array is returned instead of a boolean value.

## Register / Create Account

Now, we move forward to the register page. We use **Fr\LS::register()** function for creating accounts. Here is the syntax :

<pre class="prettyprint"><code>boolean Fr\LS::register($username, $password, $extraValues);</code></pre>

The **$extraValues** variable is an array containing keys and values that are inserted with the username and password. Suppose, you made an extra column named "name" that is used for storing the user&#8217;s name. Here is how you make the **$extraValues** array :

<pre class="prettyprint"><code>array("name" =&gt; $personName)</code></pre>

Note that email value is not passed directly to the **register()** function. You should include it with **$extraValues **array and the whole array becomes :

<pre class="prettyprint"><code>array(
    "email" =&gt; $email,
    "name" =&gt; $name
)</code></pre>

You create the HTML form and pass the values got from the form to this function and it will take care of everything else. **Fr\LS::register()** returns "exists" if the username is already taken or if an account with the email given exists. Otherwise, if everything is successful, it returns boolean **TRUE**.

## Check If User Exists

There is an in-built function to check if there is an account with the username or email exist already. Here is the syntax :

<pre class="prettyprint"><code>boolean Fr\LS::userExists($username)</code></pre>

You can also pass e-mail as the value if `config` -> `features` -> `email_login` is set to TRUE.

### Check If User ID Exists

Instead of username, you can check if a user ID exist using `userIDExists()` :

<pre class="prettyprint"><code>boolean Fr\LS::userIDExists($userID);</code></pre>

Example :

<pre class="prettyprint"><code>if ($LS-&gt;userIDExists(1)) {
    // User with ID '1' exists
}</code></pre>

## Check If User is Logged In

You can check if user is logged in with the function **Fr\LS::isLoggedIn()** :

```
if($LS->isLoggedIn()){
  // User logged in
} else {
  // User not logged in
}
```

## Log Out

You just need to call **Fr\LS::logout()** for clearing the browser cookies and PHP session which means the user is logged out :

<pre class="prettyprint"><code>$LS-&gt;logout();</code></pre>

You don&#8217;t have to do anything else.

## Sending E-Mails

When any components of logSys needs to send emails, it calls the **Fr\LS::sendMail()** function with email address, subject and body in the corresponding order.

You can change the method used to send mails by adding a callback function to `config` -> `basic` -> `email_callback`. Example :

<pre class="prettyprint"><code>"email_callback" =&gt; function($LS, $email, $subject, $body){
  mail($email, $subject . " - My Company", $body);
}</code></pre>

Sometimes **mail()** function won&#8217;t work and email won&#8217;t be received by the users. In that case, try using an Email API like [Mailgun][40]. Then use that API in the `config` -> `basic` -> `email_callback` function.

## Forgot/Reset Password

Normally, almost every user forgets their password. logSys have a special function that takes care of everything for you. Just call **Fr\LS::forgotPassword()** at the place where you want to display the Forgot Password form :

<pre class="prettyprint"><code>&lt;?php
require "config.php";
?&gt;
&lt;html&gt;
 &lt;head&gt;&lt;/head&gt;
 &lt;body&gt;
  &lt;div class="content"&gt;
   &lt;?php
   $LS-&gt;forgotPassword();
   ?&gt;
  &lt;/div&gt;
 &lt;/body&gt;
&lt;/html&gt;</code></pre>

You may call **LS->init()** in the above page if you are sensitive about logged in users accessing the page. This function returns different strings according to the status of the resetting password process. Here are they :

<table style="height: 320px;" width="717">
  <tr>
    <td>
      Return String
    </td>

    <td>
      Description
    </td>
  </tr>

  <tr>
    <td>
      identityNotProvided
    </td>

    <td>
      Identity (email/username) is not provided
    </td>
  </tr>

  <tr>
    <td>
      userNotFound
    </td>

    <td>
      No user with given identity was found
    </td>
  </tr>

  <tr>
    <td>
      resetPasswordForm
    </td>

    <td>
      The Reset Password form is currently shown.
    </td>
  </tr>

  <tr>
    <td>
      invalidToken
    </td>

    <td>
      The token given for changing password is invalid/wrong.
    </td>
  </tr>

  <tr>
    <td>
      changePasswordForm
    </td>

    <td>
      The change Password Form is currently shown.
    </td>
  </tr>

  <tr>
    <td>
      fieldsLeftBlank
    </td>

    <td>
      The fields of change password form were left blank.
    </td>
  </tr>

  <tr>
    <td>
      passwordDontMatch
    </td>

    <td>
      The new password field and retype password field doesn&#8217;t match
    </td>
  </tr>

  <tr>
    <td>
      emailSent
    </td>

    <td>
      An email was sent to the account holder&#8217;s email address
    </td>
  </tr>

  <tr>
    <td>
      passwordChanged
    </td>

    <td>
      The Password was changed/resetted successfully
    </td>
  </tr>
</table>

## Change Password

<del>Just like forgot password, all you have to do is call <strong>$LS->changePassword()</strong> function to display the form and do the tasks needed for changing the password. As like before, logSys will take care of everything here too.</del>

Update : As of version 0.4, logSys doesn&#8217;t take care of everything. You have to make the form and pass the values to the **Fr\LS::changePassword()** function. Here is an example :

<pre class="prettyprint"><code>&lt;?php
require "config.php";
$LS-&gt;init();
?&gt;
&lt;!DOCTYPE html&gt;
&lt;html&gt;
  &lt;head&gt;
    &lt;title&gt;Change Password&lt;/title&gt;
  &lt;/head&gt;
  &lt;body&gt;
    &lt;?php
    if(isset($_POST['change_password'])){
      if(isset($_POST['current_password']) && $_POST['current_password'] != "" && isset($_POST['new_password']) && $_POST['new_password'] != "" && isset($_POST['retype_password']) && $_POST['retype_password'] != "" && isset($_POST['current_password']) && $_POST['current_password'] != ""){

        $curpass = $_POST['current_password'];
        $new_password = $_POST['new_password'];
        $retype_password = $_POST['retype_password'];

        if($new_password !== $retype_password){
          echo "&lt;p&gt;&lt;h2&gt;Passwords Doesn't match&lt;/h2&gt;&lt;p&gt;The passwords you entered didn't match. Try again.&lt;/p&gt;&lt;/p&gt;";
        }else if($LS-&gt;login($LS-&gt;getUser("username"), "", false, false) == false){
          echo "&lt;h2&gt;Current Password Wrong!&lt;/h2&gt;&lt;p&gt;The password you entered for your account is wrong.&lt;/p&gt;";
        }else{
          $change_password = $LS-&gt;changePassword($new_password);
          if($change_password === true){
            echo "&lt;h2&gt;Password Changed Successfully&lt;/h2&gt;";
          }
        }
      }else{
        echo "&lt;p&gt;&lt;h2&gt;Password Fields was blank&lt;/h2&gt;&lt;p&gt;Form fields were left blank&lt;/p&gt;&lt;/p&gt;";
      }
    }
    ?&gt;
    &lt;form action="&lt;?php echo Fr\LS::curPageURL();?&gt;" method='POST'&gt;
      &lt;label&gt;
        &lt;p&gt;Current Password&lt;/p&gt;
        &lt;input type='password' name='current_password' /&gt;
      &lt;/label&gt;
      &lt;label&gt;
        &lt;p&gt;New Password&lt;/p&gt;
        &lt;input type='password' name='new_password' /&gt;
      &lt;/label&gt;
      &lt;label&gt;
        &lt;p&gt;Retype New Password&lt;/p&gt;
        &lt;input type='password' name='retype_password' /&gt;
      &lt;/label&gt;
      &lt;button style="display: block;margin-top: 10px;" name='change_password' type='submit'&gt;Change Password&lt;/button&gt;
    &lt;/form&gt;
  &lt;/body&gt;
&lt;/html&gt;</code></pre>

Here is the syntax of the function :

<pre class="prettyprint"><code>boolean Fr\LS::changePassword($new_password, $userID);</code></pre>

You may optionally mention the user ID. If not, the currently logged in user is used. This function returns boolean TRUE if the password was changed.

You will now have to check whether the current password is right or not with **Fr\LS::login()** before changing the password.

<pre class="prettyprint"><code>$LS-&gt;login("username", "current_password", false, false)
</code></pre>

The 4th parameter should be set to FALSE, otherwise redirection will occur as like a normal login.

## Get User Details/Info

As I said in the introduction, you can add more columns to the table. This means that you have to get values from every columns. For this, I added an extra function to get all the fields of a particular row. To get the fields of current user, all you have to do is call **Fr\LS::getUser()**. Syntax :

<pre class="prettyprint"><code>string|array Fr\LS::getUser("column_name", $userID);</code></pre>

Note that, we are using **$userID** which is the **id** field of the row. If you use the column name as "*", you will get an array as the return value like this :

<pre class="prettyprint"><code>array(
  "id" =&gt; 1,
  "username" =&gt; "subins2000",
  "email" =&gt; "mail@subinsb.com",
  "password" =&gt; "asd4845ghnvbmvolfpsdpsa0ffkfoeww89d9d25f1f56",
  "password_salt" =&gt; "mv5r7(4565v"
)</code></pre>

More fields will be obtained once you add more columns to the table. If you need to get only a single field, you can use :

<pre class="prettyprint"><code>$LS-&gt;getUser("column_name");
</code></pre>

## Update User Details/Info

As a suggestion of adding this feature from <a href="http://disqus.com/kevinhamil/" target="_blank" rel="noopener">Kevin Hamil</a>, I have added a function to update the users&#8217; details. Syntax :

<pre class="prettyprint"><code>boolean Fr\LS::updateUser($values, $userID);</code></pre>

The variable **$values** is an array containing the information about updation of values. If you need to update the "name" field to "Vishal", you can make the array like this :

<pre class="prettyprint"><code>$values = array(
    "name" =&gt; "Vishal"
);</code></pre>

And the **$userID** variable contains the user&#8217;s ID. By default, the value of it is the currently logged in user. Here is an example of updating the current user&#8217;s information :

<pre class="prettyprint"><code>$LS-&gt;updateUser(array(
    "name"  =&gt; "Subin",
    "birth" =&gt; "20/01/2000"
));
</code></pre>

## Two Step Login

Two Step Login is a new feature added to logSys in version 0.5. Here is how it works :

  1. User logs in with his/her username-password
  2. If the device the user used to login is not authorized, then a form is shown asking the user to enter a code

    This code should be sent by your login system. You can choose the medium for sending : E-Mail/SMS

    You should implement a callback that will send the token.
  3. The form also has a "Remember Device" option if enabled wouldn&#8217;t show the form again upon further logins in the future.

    If the user gets the token right (note that it cannot be brute forced), then login is finished

    If not, then the user would have to login again starting from username-password form

First of all enable Two Step Login by setting the value of `config` -> `features` -> `two_step_login` to TRUE.

Here&#8217;s a skeleton of the process :

<pre class="prettyprint"><code>try {
    if (isset($_POST['login']) && isset($_POST['password'])) {
        /**
         * Try login
         */
        $LS-&gt;twoStepLogin($_POST['login'], $_POST['password'], isset($_POST['remember_me']));
    } else {
        /**
         * Handle Two Step Login
         */
        $LS-&gt;twoStepLogin();
    }
} catch (Fr\LS\TwoStepLogin $TSL) {
    if ($TSL-&gt;getStatus() === 'login_fail') {
        // Username/password wrong
    } elseif ($TSL-&gt;getStatus() === 'blocked') {
        $blockInfo = $TSL-&gt;getBlockInfo();
        // Account blocked
    } elseif ($TSL-&gt;getStatus() === 'enter_token_form' || $TSL-&gt;getStatus() === 'invalid_token') {
        // Wrong token
        $hideLoginForm = true;
    } elseif ($TSL-&gt;getStatus() === 'login_success') {
        // login success. If auto init is enabled, redirection will be automatically done
    } elseif ($TSL-&gt;isError()) {
        // Some other error
    }
}

if (!isset($hideLoginForm)) {
    // Show login form here
}</code></pre>

See a complete example <a href="https://github.com/subins2000/logSys/blob/dev/examples/two-step-login/login.php" target="_blank" rel="noopener">here</a>.

The login form is placed under an if condition so that it is not displayed when the "Enter Token" form of Two step Login is shown. Or you can separate the login form page and the Two Step Login process page.

Calling `twoStepLogin()` with no parameters will make it automatically handle the Two Step Login part of the login process. The username and password parameter should be passed only once when the user submits the login form. This is what you see in the `try`{} block.

The parameters are the same as `login()`:

`$LS->twoStepLogin($_POST['login'], $_POST['password'], isset($_POST['remember_me']));`

### Exception

Exceptions are thrown by `twoStepLogin()`for handling the Two Step Login process. There are two type of exceptions here :

  * Two Step Login process exception
  * Two Step Login error exception

Note that the first exception in the above is actually good and not an error. You can test whether it&#8217;s an error using `isError()`.

### isError()

Returns whether the exception is about an error. Example :

<pre class="prettyprint"><code>catch (Fr\LS\TwoStepLogin as $TSL) {
    var_dump($TSL-&gt;isError()); // A boolean value
}</code></pre>

### getOption()

For some statuses, there will be additional values associated with it. This can be obtained with this function. Example :

When the "Enter Token" form should be displayed, there are 3 values available. These are obtained like this :

<pre class="prettyprint"><code>$TSL-&gt;getOption('uid'); // User ID
$TSL-&gt;getOption('remember_me'); // Whether user checked 'Remember Me'
$TSL-&gt;getOption('tries_left'); // Number of tries left for entering token</code></pre>

### getStatus()

Returns the status code of exception. This includes both error status and the Two Step Login process status. Example :

<pre class="prettyprint"><code>catch (Fr\LS\TwoStepLogin as $TSL) {
    var_dump($TSL-&gt;getStatus()); // String
}</code></pre>

The following table shows the different status values returned by `Fr\LS\TwoStepLogin->getStatus()` :

<table class="table">

<tbody>

<tr>

<td>Type</td>

<td>Value</td>

<td>What It Means</td>

</tr>

<tr>

<td>process</td>

<td>enter_token_form</td>

<td>Show the enter token form. Read the section after this table.</td>

</tr>

<tr>

<td>process</td>

<td>login_success</td>

<td>The Two Step Login token was correct and the user can be logged in.

This exception can only be caught if 4th parameter to `twoStepLogin()` is FALSE i.e. no cookies should be set.

Because if cookies are set the user is redirected to the home page.

</td>

</tr>

<tr>

<td>error</td>

<td>login_fail</td>

<td>The username or password was incorrect. The Two Step Login process cannot be started.</td>

</tr>

<tr>

<td>error</td>

<td>blocked</td>

<td>The user is blocked. Read [this](#getblockstatus).</td>

</tr>

<tr>

<td>error</td>

<td>invalid_token</td>

<td>The Two Step Login token submitted was incorrect.</td>

</tr>

<tr>

<td>error</td>

<td>invalid_csrf_token</td>

<td>CSRF Security token failed. [Read this](#csrf-security).</td>

</tr>

</tbody>

</table>

##### enter_token_form

This status is returned to show the Two Step Login "Enter Token" form. This form should be made by you with the only condition that the token input field should have the name `two_step_login_token`:

```
<input type='text' name='two_step_login_token' />
```

This is how the form should look :

```
<form action="<?php echo Fr\LS::curPageURL(); ?>" method="POST">
    <p>A token was sent to your E-Mail address. Paste the token in the box below :</p>
    <input type="text" name="two_step_login_token" /><br/>
    <span>Remember this device ?</span>
    <input type="checkbox" name="two_step_login_remember_device" /><br/>
    <input type="hidden" name="two_step_login_uid" value="<?php echo $TSL->getOption('uid'); ?>" />
    <?php
    echo $LS->csrf('i');
    if ($TSL->getOption('remember_me')) {
    ?>
        <input type="hidden" name="two_step_login_remember_me" />
    <?php
    }
    ?>
    <button>Verify</button>
    <a onclick="window.location.reload();" href="#">Resend Token</a>
</form>
```

NOTE that the input fields’ **name** attribute in your form SHOULD BE THE SAME AS SHOWN ABOVE.

Here are the additional input fields used :

*   two_step_login_remember_device  
    Should the device be remembered. If it’s remembered, when the user logs in again later, Two Step Login process is skipped and user is logged in if he enters the correct username and password.
*   two_step_login_remember_me  
    If the user had chosen "Remember Me" checkbox in the previous step (log in with username/password), then this field must be present to remember the user.  
    Whether the user had chosen "Remember Me" checkbox in the previous step is determined by `$TSL->getOption('remember_me')`
*   two_step_login_uid  
    Stores the user’s ID who is now attempting to log in. If this value is tampered with, then the login process fails.

##### blocked

When the user is blocked, the exception status will be ‘blocked’. You can get the information about the block using `getBlockInfo()`. The data returned will be same as in the [normal login process](#login-page).

Once again, I request you to see the complete example [here](https://github.com/subins2000/logSys/blob/dev/examples/two-step-login/login.php) to understand better.

### Database

Now we must create a table in database that will store the devices of users :

#### MySQL

See [this](https://github.com/subins2000/logSys/blob/master/sql/mysql.sql)

#### PostgreSQL

See [this](https://github.com/subins2000/logSys/blob/master/sql/postgresql.sql)

#### SQLite

See [this](https://github.com/subins2000/logSys/blob/master/sql/sqlite.sql)

You can change the table name if you want to, but you must mention the new name in `config` -> `two_step_login` -> `devices_table`.

### Configure

We still haven’t added the callback that will send the token. Add a callback function in `config` -> `two_step_login` -> `send_callback` :

```
'send_callback' => function($LS, $userID, $token){
    // Send Token as email
    $email = Fr\LS::getUser("email", $userID);
    $subject = 'Verify Yourself - 2 Step Verification';
    $body = '<p>Someone tried to login to your account. If it was you, then use the following token to complete logging in : <blockquote>'. $token .'</blockquote>If it was <b>not you</b>, then ignore this email and please consider to change your account\'s password.</p>';
    mail($email, $subject, $body);
}
```

You can use any mechanism to send the token. If you have access to SMS API, then use it because it is much more secure than E-Mail.

#### token_length

You can also change the token’s length. Just change the integer value of `config` -> `two_step_login` -> `token_length`.

#### token_tries

You can set a limit on how many incorrect tries the user can make while entering the Two Step Login token. For this, change the integer value of `config` -> `two_step_login` -> `token_tries`.

#### numeric

By default, the randomly generated token will have alphanumeric characters. In case you’re using SMS mechanism, it will be simpler for user to type numericals. You can make logSys generate numeric tokens for Two Step Login by setting the value of `config` -> `two_step_login` -> `numeric` to TRUE.

#### expiry

If the user chooses the "Remember Device" option, then a cookie is created in that browser that recognises the authorised device. You can set the validity of this cookie by changing the value of `config` -> `two_step_login` -> `expiry`. The value is used in [strtotime()](http://php.net/manual/en/function.strtotime.php) function, so enter values that are valid for the function.

#### first_check_only

If a user did "Remember Device" and visit a page, logSys will check if the device cookie matches the device ID stored in database. If it does not, then the user is logged out. This is done so, because if the original user revokes a device, the session on that device must be logged out wherever it is. But, checking this every time a page loads can decrease performance. To avoid this, you can set the value of `config` -> `two_step_login` -> `first_check_only` to TRUE.

### getDeviceID()

The ID of the device currently logged in by the user. Actually it returns the device cookie value.

This only works if the user is logged in.

## CSRF Security

To protect against CSRF, a token system is implemented in logSys. For handling CSRF security `csrf()` function is used.

### Get Token As String

```
echo $LS->csrf('s');
```

### Check If CSRF Token Is Correct

When a form is submitted it is necessary to check if CSRF token is correct. For this simply call `csrf()` with no parameters :

```
if ($LS->csrf()){
    // All good
}
```

### Get CSRF Token As An Input Field

```
echo $LS->csrf('i');
```

This returns something like this :

```
<input type='hidden' name='csrf_token' value='w9cvK' />
```

## Extra Functions/Tools

Along with the main functions in logSys, some extra tools or functions are included.

### Time Since User Joined

If you would like to display to the user how much time he has been a member of the site, you have to do the following : Create a column named "created" in your users database table and add the created value in registration :

```
$LS->register($username, $password, array(
  "created" => date("Y-m-d H:i:s")
));
```

Now, you can use the built in **joinedSince()** function of logSys to display the time since joined :

```
echo $LS->joinedSince();
```

Some example outputs :

```
10 Seconds
2 Minutes
4 Hours
25 days
7 Weeks
15 Months
```

### Check if email is valid

Use **Fr\LS::validEmail()** function for checking if an email address is valid or not. Usage :

```
Fr\LS::validEmail("mail@subinsb.com")
```

### Current Page URL

Get the full URL of the current page. Usage :

```
echo Fr\LS::curPageURL()
```

### Generate Random String

As seen on [http://subinsb.com/php-generate-random-string](//subinsb.com/php-generate-random-string), Generates a unique string. Usage :

```
Fr\LS::randStr(20)
```

### Current Page Pathname

Get the path name of the current page. Usage :

```
echo Fr\LS::curPage()
```

Some sample outputs :

```
/
/myfolder/mysubfolder/mypage.php
```

### Redirect With HTTP Status

Redirects with the HTTP status such as 301, 302\. Usage :

```
Fr\LS::redirect("http://subinsb.com", 302)
```

That’s all the extra tools.

## Common Problems

I should have made this section long time ago. Here are some of the most common problems and the solution to them :

### Fatal error: Call to a member function prepare() on a non-object

This error happens because logSys couldn’t connect to the database. Either your server doesn’t have **PHP PDO Extension** or the database credentials given in `config` -> `db` is not correct.

So, install **PDO** extension and check if the database credentials given is correct.

### Redirect Loop / Can’t Access Pages

This is the most common problem and the solution is simple. Why this error happened is because that the relative path names put in the `config` -> `pages` -> `no_login` array is wrong or the `config` -> `pages`->`login_page` is wrong or `pages` -> `home_page` has an invalid value. Here are some valid path names :

```
/
/index.php
/mypage/myfile.php
/login.php
/home.php
```

But, these path names are **wrong** :

```
index.php
http://mysite.com/mypage/myfile.php
//mysite.com/login.php
mysite.com/home.php
```

An easy way to find out the relative pathname of a page is to output **$_SERVER[‘REQUEST_URI’]** in that page.

### session_start() – headers already sent

This is a common problem seen from the **0.4** version. It is because that the session is not started [session_start()] before the content is outputted.

logSys will start the session if `config` -> `features` -> `start_session` is set to **TRUE**. If you enable this, you must construct the logSys object before any output is made like this :

```
<?php
$LS = new Fr\LS;
?>
<html>
```

### Cookies Not Created

When the cookies are not created, user is not logged in after submitting the form. He/she won’t be redirected to the home page and will still see the login page.

This is probably because of faults in the configuration. Check the values of `config` -> `cookies` array. Try removing the `domain` value or `path` value. Try messing with the values of it.

Also, try keeping the values of `domain` and `path` blank. It might work.

### Cannot modify header information

You may have to [enable Output Buffering](//subinsb.com/php-output-buffering) to solve this problem. Or you should move all your logSys and redirection function calls at the top of the page before any output is made.

### User Roles

Setting User Roles is a feature asked by many. I have plans to implement it, but it will take time. If you want to do it manually, this is what I recommend :

*   Add a column named "role" containing user access level in DB table
*   In the page where you display stuff on your site, add checking if the user has the level to access the page or display information in it. Example :

```
if($LS->getUser("role") == "admin"){
  // Show Admin stuff
}else if($LS->getUser("role") == "editor"){
  // Show Editor Stuff
}else if($LS->getUser("role") == "contributor"){
  // Show Contributor Stuff
}
```

assuming that the user is logged in.

## Security

[Chet](//subinsb.com/php-logsys#comment-1777655700) has said that including sensitive credentials in a **PHP** file is not secure. I’m agreeing with him. But, a possible way to make it secure is not to include malicious/untrusted scripts in your server. See [this post](http://security.stackexchange.com/questions/35492/is-it-possible-for-a-hacker-to-download-a-php-file-without-executing-it-first) to see about how an attacker gets your configuration credentials such as database’s.

**file_get_contents()** is a function that can be exploited by an attacker to retrieve sensitive information from a server. Here is a [list of the exploitable PHP functions](http://stackoverflow.com/a/3697776/1372424).

If you’re going to report a problem, tell the version of logSys using, explain the problem clearly and put in some example codes.

This tutorial is completed. I will update logSys in the future if I can. After all, I’m a kid who is in 11th grade – a grade where studies should be taken seriously. Good Luck and I hope you found what you are looking for.

 [1]: #latest-version
 [2]: #how-to-upgrade
 [3]: #logsys-admin
 [4]: //subinsb.com/logsys-admin
 [5]: #features
 [6]: #requirements
 [7]: https://github.com/ircmaxell/password_compat/blob/master/lib/password.php
 [8]: #download-install
 [9]: https://github.com/subins2000/logSys/blob/master/src/LS.php
 [10]: #introduction
 [11]: #Database-Table-Setup
 [12]: #configuration
 [13]: #basic
 [14]: #db
 [15]: #keys
 [16]: #features-2
 [17]: #brute-force
 [18]: #pages
 [19]: #cookies
 [20]: #two-step-login
 [21]: #debug
 [22]: https://github.com/subins2000/logSys/blob/master/examples/basic/config.php
 [23]: #database-table-setup
 [24]: #mysql
 [25]: https://github.com/subins2000/logSys/blob/master/sql/mysql.sql#L8
 [26]: https://github.com/subins2000/logSys/blob/master/sql/mysql.sql#L23
 [27]: #postgresql
 [28]: https://github.com/subins2000/logSys/blob/master/sql/postgresql.sql
 [29]: #sqlite
 [30]: https://github.com/subins2000/logSys/blob/master/sql/sqlite.sql
 [31]: #timezone
 [32]: #initialize
 [33]: #login-page
 [34]: #register-create-account
 [35]: #check-if-user-exists
 [36]: #check-if-user-id-exists
 [37]: #check-if-user-is-logged-in
 [38]: #log-out
 [39]: #sending-e-mails
 [40]: http://subinsb.com/send-free-emails-using-mailgun-api-in-php
 [41]: #forgotreset-password
 [42]: #change-password
 [43]: #get-user-detailsinfo
 [44]: #update-user-detailsinfo
 [45]: #two-step-login-2
 [46]: #exception
