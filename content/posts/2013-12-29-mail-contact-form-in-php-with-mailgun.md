---
title: Create A Contact Form In PHP With Mailgun
author: Subin Siby
type: post
date: 2013-12-29T08:04:38+00:00
url: /mail-contact-form-in-php-with-mailgun
authorsure_include_css:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
authorsure_hide_author_box:
  - 
  - 
  - 
  - 
  - 
  - 
categories:
  - HTML
  - PHP
tags:
  - Email
  - IP

---
Contact forms make it easy for the visitors of your site to contact you. Contact forms are available in various **CM systems**, but if you are creating your own site, you have to create the contact form yourself. The messages thus sent by the users have to be stored in databases and you have to constantly check the database for new messages. To overcome this problem, it&#8217;s better to directly send the messages to your inbox.

In my <a title="Send Free E-Mails Using Mailgun API in PHP" href="//subinsb.com/send-free-emails-using-mailgun-api-in-php#comment-2546148234" target="_blank">previous post</a>, I gave the function **send_mail** which can be used to send free mails using the **Mailgun** **API**.

<div class="padlinks">
  <a class="demo" href="http://demos.subinsb.com/contact-form-mailgun" target="_blank">DEMO</a> <a class="download" href="http://demos.subinsb.com/down.php?id=j2vocxn1by3x&class=14" target="_blank">DOWNLOAD</a>
</div>

We can use that function in our contact form to send the messages given by the user directly to you. If you haven&#8217;t seen the **send_mail()** function yet, see <a title="Send Free E-Mails Using Mailgun API in PHP" href="//subinsb.com/2013/12/send-free-emails-using-mailgun-api-in-php" target="_blank">this post</a>. We will create a contact form which sends the messages directly to the site admin using **Mailgun API** and **PHP**. We should create our form first.

<pre class="prettyprint"><code>&lt;form action="contact.php" method="POST"&gt;
Name&lt;br/&gt;
&lt;input type="text" name="name" placeholder="Your Name" size="40"/&gt;&lt;br/&gt;
Message&lt;br/&gt;
&lt;textarea name="message" cols="40" rows="10"&gt;&lt;/textarea&gt;&lt;br/&gt;
&lt;input type="submit" value="Send Message" name="submit"/&gt;
&lt;/form&gt;</code></pre>

# contact.php

This is the page where the form data will be sent to. This file checks if all the fields are filled or not and if all are filled, it will send the email :

<pre class="prettyprint"><code>&lt;?php
if(isset($_POST['submit'])){
  $name = $_POST['name'];
  $msg = $_POST['message'];
  if($name != "" && $msg != ""){
    $ip_address = $_SERVER['REMOTE_ADDR'];
    send_mail("subins2000@gmail.com","New Message","The IP ($ip_address) has sent you a message : &lt;blockquote&gt;$msg&lt;/blockquote&gt;");
    echo "&lt;h2 style='color:green;'&gt;Your Message Has Been Sent&lt;/h2&gt;";
  }else{
    echo "&lt;h2 style='color:red;'&gt;Please Fill Up all the Fields&lt;/h2&gt;";
  }
}
?&gt;</code></pre>

The email sent will have the **IP address** of the user and the message of the user.