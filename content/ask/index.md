---
title: Ask
author: Subin Siby
type: page
date: 2013-11-09T15:51:16+00:00
authorsure_include_css:
  - 
  - 
  - 
dsq_thread_id:
  - 1799366118
  - 1799366118
  - 1799366118
  - 1799366118
  - 1799366118
  - 1799366118

---
If you have any questions / problems / suggestions you can ask / request it in the comments below

If you want to request a tutorial, you can request it too via the comments.

If you want to discuss about a topic or need help, please use this page. I will be glad to help you.

I will reply to your comments if I&#8217;m not in the following conditions :

  1. Have a school project
  2. Have got fever or other diseases
  3. Have a function to attend
  4. Internet Connection Expired
  5. Internet Not Working
  6. Dead

Please don&#8217;t paste codes on the comments. If you want to paste codes you can use <a href="http://pastebin.com" target="_blank"><strong>pastebin</strong></a>. If you want to directly contact me, send an email to  mail[@]subinsb.com

If you want to embed codes in the comment, use **&lt;blockquote&gt;** tag. Example :

<pre class="prettyprint"><code>&lt;blockquote&gt;&lt;? echo "Hello World"; ?&gt;&lt;/blockquote&gt;</code></pre>

You can also use "code" & "pre" tags for embedding codes. You can bold using "<b></b>" & can add a link using `<a href="></a>`.

And if you think, it&#8217;s vulnerable to XSS attack, think again.
