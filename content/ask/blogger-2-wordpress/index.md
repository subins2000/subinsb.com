---
title: Blogger 2 WordPress
author: Subin Siby
type: page
date: 2014-02-09T05:55:29+00:00
authorsure_include_css:
  - 
  - 
  - 

---
Do you have a question about Blogger 2 WordPress plugin ? Ask them here. If you need support, you can use the comment form below.

The Plugin can be downloaded from WordPress plugins page. Search For "Blogger 2 WordPress" in the "Add New" page of Plugins.

You can see more info about this plugin and how to migrate Blogger blog to WordPress in <a href="//subinsb.com/move-blog-from-blogger-to-wordpress" target="_blank">this post</a>.