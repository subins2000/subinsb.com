---
title: PHP siteCompressor
author: Subin Siby
type: page
date: 2014-06-15T15:12:28+00:00
authorsure_include_css:
  - 
  - 
  - 

---
This is the official support page of the **PHP Site Compressor** software termed as **siteCompressor**. You can ask your doubts about **siteCompressor** here. If you have a suggestion, you can submit them here. I&#8217;ll be here to help you.

You can see the tutorial on how to set up **siteCompressor** [here][1].

The official **GitHub** page of the software is <a href="https://github.com/subins2000/siteCompressor" target="_blank">here</a>. You can contribute to the project via GitHub.

By submitting means commenting. The "support forum" is the comments. If you want more help about how Asking works, see [this page][2].

 [1]: //subinsb.com/compress-php-website-html-css-js "Compress PHP Site - HTML, CSS & JS"
 [2]: //subinsb.com/ask "Ask"