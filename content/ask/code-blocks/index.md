---
title: Code Blocks
author: Subin Siby
type: page
date: 2014-02-21T16:29:47+00:00
authorsure_include_css:
  - 
  - 
  - 

---
**Code Blocks** is a **WordPress** plugin that adds a tool to post codes in post. Code Blocks can be configured. It also can be used to add **Custom CSS** to WordPress Visual Editor.

You can download the plugin from WordPress. Simply search for **Code Blocks** in WordPress Plugins Add New page or you can download the plugin from <a title="Download Code Blocks Directly" href="http://demos.subinsb.com/WordPress/code-blocks.zip" target="_blank">here</a>.

You can seek help using the comments on this page and the plugin author (me) will certainly help you for sure if I&#8217;m not in the conditions mentioned in <a title="Conditions Where I can't comment" href="//subinsb.com/ask" target="_blank">this page</a>.