---
title: Projects
author: Subin Siby
type: page
date: 2020-06-28T23:00:00+05:30
lastmod: 2023-09-23T23:00:00+05:30
layout: projects

categories:
  - android
  - desktop
  - web
  - malayalam

projects:
  - name: മwordle
    img: https://mwordle.subinsb.com/static/favicon.png
    desc: Wordle game in Manglish/Malayalam. Uses Varnam as input API.
    cat:
      - 2022
      - web
      - malayalam
    links:
      - text: Play Game
        url: https://mwordle.subinsb.com
      - text: Varnam
        url: https://varnamproject.com

  - name: കിട്ടും.com
    img: /projects/kittum.com.png
    desc: A malayalam domain for having some fun with URLs.
    cat:
      - 2022
      - web
      - malayalam
    links:
      - text: View
        url: https://ബിരിയാണി-എപ്പോ.കിട്ടും.com
      - text: GitHub
        url: https://github.com/subins2000/kittum.com

  - name: Varnam for macOS
    img: /projects/varnam-macos.png
    desc: Varnam input method for macOS. Type Malayalam and other Indian languages on macOS easily.
    cat:
      - 2021
      - desktop
      - malayalam
    links:
      - text: Install
        url: https://varnamproject.com
      - text: GitHub
        url: https://github.com/varnamproject/varnam-macos

  - name: Varnam for GNU/Linux
    img: https://varnamproject.com/_index/im.png
    desc: Varnam input method for GNU/Linux systems. Type Malayalam and other Indian languages on GNU/Linux systems easily.
    cat:
      - 2021
      - desktop
      - malayalam
    links:
      - text: Install
        url: https://varnamproject.com
      - text: GitHub
        url: https://github.com/varnamproject/govarnam-ibus

  - name: WebDrop
    img: https://raw.githubusercontent.com/subins2000/WebDrop/master/public/favicon.png
    desc: A web app to share files and messages peer to peer across different devices. Auto discovers devices in the same LAN. Also allows to directly send files over the internet.
    cat:
      - 2020
      - web
      - webrtc
      - webtorrent
    links:
      - text: Open App
        url: https://webdrop.space
      - text: Blog
        url: /webdrop

  - name: Vett, The Dots & Boxes Game
    img: https://raw.githubusercontent.com/subins2000/vett/master/public/static/favicon.png
    desc: Online multiplayer Dots & Boxes game. No installation needed, game works in the browser peer to peer with WebRTC & WebTorrent.
    cat:
      - 2020
      - web
      - webrtc
      - webtorrent
    links:
      - text: Play Game
        url: https://dots.subinsb.com
      - text: Blog
        url: /vett

  - name: Mayalam to Manglish
    img: https://raw.githubusercontent.com/subins2000/manglish/master/artwork/logo.svg

    # Image sizes: https://bulma.io/documentation/elements/image/
    imgsize:

    desc: A simple Android app to convert Malayalam text to Manglish. For people who understands Malayalam, but can't read the script.
    cat:
      - 2019
      - android
      - malayalam
    links:
      - text: Play Store
        url: https://play.google.com/store/apps/details?id=subins2000.manglish
      - text: F-Droid
        url: https://f-droid.org/packages/subins2000.manglish/
      - text: Blog
        url: /manglish

  - name: Indic-En
    img: https://raw.githubusercontent.com/subins2000/indicen/master/assets/favicon.png
    desc: This browser extension will convert Malayalam, Hindi & Kannada webpages to Manglish, Hinglish & Kanglish respectively. For people who understands the language, but can't read the script.
    cat:
      - 2019
      - web
      - malayalam
      - indiclang
    links:
      - text: Firefox
        url: https://addons.mozilla.org/en-US/firefox/addon/indicen/
      - text: Chrome
        url: https://chrome.google.com/webstore/detail/indic-en/fbbhbgpgjfjdncnealckbfdmieafpgon
      - text: Blog
        url: /indicen

  - name: Varnam Editor
    img: https://lab.subinsb.com/projects/blog/projects/varnam-editor.png
    desc: An offline editor to easily type Indian languages using transliteration. Easy install with Flatpak on Linux.
    cat:
      - 2019
      - desktop
      - malayalam
      - indiclang
    links:
      - text: Download
        url: /varnam
      - text: Blog
        url: /varnam

  - name: Profile Picture Framer
    img: https://lab.subinsb.com/projects/blog/projects/blasters-profile-pic-framer.png
    desc: Frame profile pictures easily with this webtool. Originally created during 2016 Indian Super League football tournament for Kerala Blasters fans. [Cat Image Source](https://commons.wikimedia.org/wiki/File:June_odd-eyed-cat_cropped.jpg)
    cat:
      - 2016
      - web
    links:
      - text: Try It
        url: https://demos.subinsb.com/isl-profile-pic/
      - text: Blog
        url: /create-profile-picture-framer-web-app/

  - name: Lobby
    img: https://lab.subinsb.com/projects/blog/projects/lobby.png
    desc: A localhost platform for web apps. Kinda similar to ElectronJS & WordPress. I made many apps and games inside it which exclusively worked only in Lobby. Worked on it for 3 years, now abandoned
    cat:
      - 2014
      - android
      - desktop
      - web
    links:
      - text: Archive
        url: https://web.archive.org/web/20170829084945/http://lobby.subinsb.com/
      - text: GitHub
        url: https://github.com/LobbyOS
      - text: Apps
        url: https://web.archive.org/web/20170821003313/http://lobby.subinsb.com/apps

  - name: logSys
    img:
    desc: An Open Source library to implement a login system to any PHP website. Framework independent. Still used by many sites. Open, Lobby used logSys.
    cat:
      - 2014
      - web
    links:
      - text: Blog
        url: /php-logsys
      - text: Source Code
        url: https://github.com/subins2000/logSys

  - name: Search Engine
    img:
    desc: An Open Source search engine made in PHP with its own crawler bot. Have indexed more than 150,000 URLs before the hosting provider OpenShift's free plan limit exceeded.
    cat:
      - 2014
      - web
    links:
      - text: Blog
        url: /search-engine-in-php-part-1/
      - text: Source Code
        url: https://github.com/subins2000/search

  - name: Demos
    img: https://lab.subinsb.com/projects/blog/projects/subins-lab.png
    desc: A site to host small small demos of the things I make.
    cat:
      - 2013
      - web
    links:
      - text: See Demos
        url: https://demos.subinsb.com/

  - name: Open
    img: https://raw.githubusercontent.com/subins2000/open/master/cdn/img/logo.png
    desc: A complete open source social network. Made it when I was 13. Used to have 5K+ users on [open.subinsb.com](https://open.subinsb.com) before the hosting provider OpenShift removed their free hosting plan.
    cat:
      - 2013
      - web
    links:
      - text: Archive
        url: https://web.archive.org/web/20170806115858/http://open.subinsb.com:80/
      - text: Source
        url: https://github.com/subins2000/open
      - text: Blogs
        url: /tags/open

  - name: Subins
    img: https://raw.githubusercontent.com/subins2000/subins/master/static/images/logo.png
    desc: My first coding project. It was a network of sites that had a search engine, social network, video host etc.
    cat:
      - 2011
      - web
    links:
      - text: Archive
        url: /the-subins-project/#what-s-left
      - text: Source
        url: https://github.com/subins2000/subins
      - text: Blogs
        url: /tags/subins
---
