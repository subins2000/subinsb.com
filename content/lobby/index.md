---
title: Lobby
author: Subin Siby
type: page
date: 2016-05-08T19:13:00+00:00

---
Lobby is a localhost Web OS. It can be attributed as a combination of **Android** and **WordPress**.

Lobby is a framework for building web apps and publishing it.