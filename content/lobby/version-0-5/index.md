---
title: Lobby Version 0.5
author: Subin Siby
type: page
date: 2016-03-04T18:30:26+00:00

---
Version 0.5 brings all this :

  * <span style="line-height: 1.5;">New UI</span>
  * <span style="line-height: 1.5;">Improved UX</span>
  * <span style="line-height: 1.5;">Added &#8216;Require&#8217; functionality for apps</span>
  * <span style="line-height: 1.5;">Fixed bugs</span>
  * <span style="line-height: 1.5;">etc.</span>

The update is available to everyone.

It is **HIGHLY RECOMMENDED** to update Lobby to 0.5