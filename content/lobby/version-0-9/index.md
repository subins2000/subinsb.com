---
title: Version 0.9 Berly
author: Subin Siby
type: page
date: 2016-07-23T08:46:02+00:00

---
I&#8217;m very excited to announce **Lobby 0.9 Berly**, the most **STABLE** version of Lobby.

[Changelog][1]

## Codename

The name "berly" was suggested by [Puneet Varma][2] and became the top runner in [the poll with 10 votes][3].

Berly has no special meaning and it just popped into the mind of Puneet.

&nbsp;

 [1]: https://github.com/LobbyOS/lobby/blob/dev/CHANGELOG.md#09
 [2]: https://www.facebook.com/puneethacker
 [3]: https://www.facebook.com/groups/LobbyOS/permalink/1753822351570020/