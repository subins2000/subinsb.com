---
title: Version 0.8 Arcadia
author: Subin Siby
type: page
date: 2016-06-25T07:18:25+00:00

---
I&#8217;m very excited to announce **Lobby 0.8 Arcadia**, the first **STABLE** version that comes with a codename.

[Changelog][1]

## Codename

The name "Arcadia" was suggested by Vishnu MC and became the top runner in [the poll with 2 votes][2].

From Wikipedia :

> Arcadia (Greek: Ἀρκαδία) refers to an ancient civilization of pastoralism and harmony with nature. The Greek province of the same name is derived from it; Arcadia has developed into a poetic byword for a noble and peaceful wilderness. Arcadia is a poetic place associated with bountiful natural splendor and harmony.[1] The &#8216;Garden&#8217; is often inhabited by shepherds. The place is also mentioned in Renaissance mythology.

 [1]: https://github.com/LobbyOS/lobby/blob/dev/CHANGELOG.md#08
 [2]: https://www.facebook.com/groups/LobbyOS/permalink/1752722431680012/