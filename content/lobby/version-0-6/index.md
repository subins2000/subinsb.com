---
title: Lobby Version 0.6
author: Subin Siby
type: page
date: 2016-05-08T19:50:44+00:00

---
Lobby **0.6** was developed in a month with these changes :

  * Bug fixes
  * App install is now a background process
  * Lobby class files is now included in composer
  * Composer packages have been updated
  * Integration with **PHP Cli Web Server** in both **Windows** and **Linux**

**Lobby 0.6** will be the first version to be used in the Lobby Standalone packages for **Windows** and **Linux**.

<a href="https://lobby.subinsb.com/download" target="_blank">Download Lobby Now</a>

<a href="https://lobby.subinsb.com/docs/update" target="_blank">How To Update Lobby</a>

## Quick Install Packages

The Lobby Debian Package has been discontinued. Instead, **<a href="http://lobby.subinsb.com/download#linux" target="_blank">Lobby Linux Standalone</a>** is introduced which supports all Linux distributions.

The Lobby **.msi** installer file for Windows has been discontinued too. There was no need of an installer in the first place. Instead, **<a href="https://lobby.subinsb.com/download#windows" target="_blank">Lobby Windows Standalone</a>** has been developed.