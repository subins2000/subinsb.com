---
title: Version 1.0 Cobra
author: Subin Siby
type: page
date: 2016-12-28T08:30:11+00:00

---
Lobby 1.0 Cobra has been released.

[Changelog][1]

## Codename

"Cobra" is the codename of this release. This was chosen by a [poll][2] conducted in the LobbyOS group in Facebook.

 [1]: https://github.com/LobbyOS/lobby/blob/dev/CHANGELOG.md#10
 [2]: https://www.facebook.com/groups/LobbyOS/permalink/1766051580347097/