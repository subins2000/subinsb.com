---
title: Lobby Version 0.4
author: Subin Siby
type: page
date: 2016-02-14T18:30:17+00:00

---
Version 0.4 brings all this :

  * <span style="line-height: 1.5;">Added FilePicker &#8211; choose files from system</span>
  * <span style="line-height: 1.5;">Timezone support</span>
  * <span style="line-height: 1.5;">Fixed bugs</span>
  * <span style="line-height: 1.5;">Made Lobby more faster</span>

The update will be available to everyone. Please note that it&#8217;s **better to reinstall the latest version if you&#8217;re using Lobby version < 0.3**