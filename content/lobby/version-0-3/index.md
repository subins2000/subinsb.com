---
title: Lobby Version 0.3
author: Subin Siby
type: page
date: 2015-11-10T18:30:12+00:00

---
Okay, we have some good news and bad news.

The good news is that Lobby 0.3 has undergone a lot of changes and heavy coding 

The bad news is that you can&#8217;t upgrade your existing Lobby to the new one. 

## Why ?

 The whole schematics of Lobby has changed. The base of Lobby has undergone drastic changes that a complete reinstallation would replace the older version. 

A standard upgrade can&#8217;t be relied on as there are a lot of changes in the new Lobby version