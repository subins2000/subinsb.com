---
title: Lobby Version 0.7
author: Subin Siby
type: page
date: 2016-06-12T12:48:52+00:00

---
I&#8217;m pleased to announce the release of Lobby **0.7**.

<div class="padlinks">
  <a class="download" href="https://lobby.subinsb.com/download">Download</a>
</div>

Here are the changes :

  * Rewritten Core
  * More Object Oriented
  * Most global variables, functions removed
  * Auto Timezone configuration
  * UI Changes
  * Bugs fixed

With Lobby 0.7, I hope to make a more stable version from which apps can be built on stably.

From now, on each major release will be named specifically.

Next release date : **25/06/2016**

## NOTE (IMPORTANT)

Since the core was rewritten, all apps that worked in versions **0.6** and older **won&#8217;t work in 0.7**. All apps has been updated to work with version **0.7**. So, **update apps before updating Lobby**.

> When you update apps, it won&#8217;t work with your Lobby version.
> 
> It will only work when your Lobby installation has been updated to version 0.7