---
title: Talks
author: Subin Siby
type: page
date: 2024-12-13T16:00:55+00:00
---

Whenever I feel like it, I give talks at conferences and meetups. I like public speaking especially when it's to showcase something that excites me.

### 2024

* ActiveRecord + SQLite on WASM! (Video coming soon)

  [RubyConf India](https://rubyconf.in/), Jaipur, India

* Simple analysis on place names of Kerala (Video coming soon)

  [State of The Map Kerala](https://wiki.openstreetmap.org/wiki/State_of_the_Map_Kerala_2024), Wayanad, India

* [Learnings from creating an input method for GNU/Linux from a product perspective](https://www.youtube.com/watch?v=lWoXsdDrPkk)

  [DebConf24](https://debconf24.debconf.org/), Busan, South Korea

  [_Mirror_](https://laotzu.ftp.acc.umu.se/pub/debian-meetings/2024/DebConf24/debconf24-386-learnings-from-creating-an-input-method-for-gnulinux-from-a-product-perspective.av1.webm)

* [Making a semantic dictionary from 2001 Pagemaker files using FOSS tools](https://hasgeek.com/fosscell/fossmeet24/sub/making-a-semantic-dictionary-from-2001-pagemaker-f-HjVpJCtN8VjSYm9WT2oxBt)

  [FOSSMeet'24](https://wiki.fosscell.org/index.php?title=FOSSMeet/24), NIT Calicut, India

### 2023

* [പയ്യനും ഫ്രീക്കനും പിന്നെ ഞാനും. "F¥p hn\[nbnXv !"](https://www.youtube.com/watch?v=iBM1OLYP8os)

  [Kochi FOSS Meetup](https://kochifoss.org), Kochi, India

* [Using Ruby as a config file syntax](https://www.youtube.com/watch?v=S-EadO1n38k) (Lightning talk)

  [RubyConf India](https://rubyconf.in/), Pune, India

* [How I made കിട്ടും.com](https://www.youtube.com/watch?v=DZrq6FkxW48)

  [Kochi FOSS Meetup](https://kochifoss.org), Kochi, India

  [_Mirror_](https://www.youtube.com/watch?v=EctlcR8zygg)

* [Using WebTorrent trackers for purposes other than torrents](https://www.youtube.com/watch?v=o8IWwuLaqTs)

  IndiaFOSS, Bengaluru, India

* [Open source beginner experience of contributing to WebTorrent and building apps with it](https://www.youtube.com/watch?v=XPm-i8smfNU)

  [FOSSMeet'23](https://wiki.fosscell.org/index.php?title=FOSSMEET#2023_(13th_Edition)), NIT Calicut, India

### 2021

* [GoVarnam, a new "Intelligent" Input Method for Indian Languages in Desktop & Mobile](https://www.youtube.com/watch?v=vewyVG6TfgA)

  [DebConf21](https://debconf21.debconf.org/talks/21-govarnam-a-new-intelligent-input-method-for-indian-languages-in-desktop-mobile/), Online

* [ടി.വി പരസ്യഘാതകൻ, Television AdBlocker](https://www.youtube.com/watch?v=oBD7iA6zmgk)

  [MiniDebConf India 2021](https://in2021.mini.debconf.org/talks/13-/), Online

### 2020

* [State of writing Malayalam on GNU/Linux : ഗ്നു/ലിനക്സിലെ മലയാളമെഴുത്തു്‌](https://www.youtube.com/watch?v=Fy0r-pHE6Us)

  [DebConf20](https://debconf20.debconf.org/talks/77-state-of-writing-malayalam-on-gnulinux/), Online

<!-- ### 2019

* [‌Selfie a Day project](https://www.youtube.com/watch?v=zIWdH1cV88s)

  [PyCon India](), Chennai, India -->

<style>
  article li {
    margin-bottom: 10px;
  }
</style>