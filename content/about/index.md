---
title: About
author: Subin Siby
type: page
date: 2013-12-21T05:33:08+00:00
lastmod: 2019-12-29T23:00:00+05:30
---

```php
$cur_year = date("Y"); // PHP. This returns the current year.
$age = $cur_year - 2000; // Forget the month and the date
```

## Hi ! I'm Subin

Hi all,

I am Subin Siby, a self-taught programmer who loves to make & tinker with things. I was born in the year <strong>2000</strong> (And that's how I got the username). I am currently $age years old and lives in Kerala, India. **Malayalam (മലയാളം)** is my native language. I learnt English from school and by watching a LOT of tv shows and movies.

I started programming in **2011** when I was **11** years old. I love FOSS because of the collaborative nature and the freedom to hack the software to our needs. I support Free Software, Open Knowledge & Sharing.

I'm a very curious person. I try to learn new things and love doing interesting stuff, like the one time (2018) where I tracked rainfall for a week and [made a chart](https://codepen.io/subins2000/pen/WgvNgM) with ChartJS.

I love reading history, going through many Wikipedia pages, reddit and all things internet :D

### Profiles

You can find me on sites with the username `subins2000` or `SubinSiby`.

- [GitHub](https://github.com/subins2000)
  <br>Has most of my FOSS projects. Plus contributions
- [GitLab](https://gitlab.com/subins2000)
  <br>Has my relatively new FOSS projects. Plus contributions
- [Wikipedia](https://meta.wikimedia.org/wiki/User:Subins2000)
  <br>I contribute to Wikipedia and its sister-projects
- [OpenStreetMap](https://www.openstreetmap.org/user/subins2000/)
  <br>I sometimes map, upload traces to OSM
- [Telegram](https://t.me/SubinSiby)
  <br>I share interesting things I find on the web & sometimes my original content in this Telegram channel
- [Mastodon : @subins2000@aana.site](https://aana.site/@subins2000)
  <br>Toots in [മലയാളം](/tags/malayalam) or English or both :P
- [Twitter](https://twitter.com/SubinSiby)
  <br>My Mastodon & Twitter is bridged.
- [LinkedIn](https://www.linkedin.com/in/subinsiby/)
  <br>I don't use it much
- [Instagram](https://www.instagram.com/subins2000)
  <br>Is a reminder of the places I've travelled. I post interesting stuff as stories too.

If you're a curious person like me, you'll like my stories on Instagram, my toots and things in my Telegram channel.

### Contact

See [/contact](/contact).

## Projects

I've made a lot of free & open source software which you can find in this blog and in my [GitHub](https://github.com/subins2000) and [GitLab](https://gitlab.com/subins2000) profiles.

See [/projects](/projects).

### Legacy

I've made more projects that I don't maintain anymore. You can find them on my GitHub or GitLab.

- [Lobby](/lobby/) : A framework/platform for running web apps in localhost
- [Open](/tags/open/) : An Open Source social network in PHP

## About this blog

- When I encounter a problem during programming or while making something and find a solution, I post it in this blog.
- When I participate in tech events, I share about the experience here
- When I feel like writing something, I share it here
- Some stories [like this one](/myre)

I started this blog when I was **11** years old. This blog shows how I've grown as a person. I cringe myself seeing my old blog posts 😬, but I don't delete em. It's history and nostalgic 😁🌟.

A lot of people were kind enough to [donate money](/donate) to support my projects. You see, as a middle class kid growing up in a rural part of India, these donations, feedback and appreciation were a huge boost for me growing up. Besides, seeing the donations, my parents were quite surprised and that helped in their support towards me.

### Subscribe

There used to be email subscriptions before. But it's too hard to manage. Instead better follow my Telegram channel or Mastodon or Twitter.

## Stats

Google Analytics was used at first for pageviews count. Then, StatCounter was used. Then again, it was switched back to GA.

Since 2019 July, I've been using a [custom hit counter](https://github.com/defuse/phpcount). The pageviews from this is shown in the header of posts. The count was reset to zero at the beginning of 2020, so that a better analysis is obtained. This custom hit counter doesn't track users.

Since the stats measurement tool was changed between years, it was hard to make an overall total. But I [did it](https://gist.github.com/subins2000/7b81088787cda65efb8694b8a2406fa2) with some effort.

Stats till `2020-01-01` :

- Users: `2,400,000`+
- Pageviews: `4,266,068`+

I wonder how many people around the world I've made an influence on, even in a small way. Feels good :)

I encourage everyone to start a blog, write stuff. When you face a problem, fixes it, and there's no docs on it anywhere, blog about it. It might help someone else, maybe even after 10, 20 years :)

Your writings may influence others, perhaps someone who came across your blog while wandering through the internet. Hey, I still remember some things I read in random blogs I came across when roaming around the world wide web and that have influenced me, yes ;)

## Blog's History

### 2011 - 2013

This blog was first created as "Science And Games" (SAG) in Blogger.com. My neighbour and good friend Simrajettan (Simraj KS) introduced me to blogging. He was a physics school teacher at the time. He has a [blog named "scintillla"](http://simmash.blogspot.com) from which I drew inspiration.

Someone had already took `sag.blogspot.com`, so I registered with [sag-3.blogspot.com](http://sag-3.blogspot.com). I took the number 3 because it was me, my brother Simrin Siby and Simraj KS who were at the time I thought would contribute to the blog.

The initial posts were about some tutorials, Wikipedia copy-paste articles and stuff. Some of these posts were deleted later. You can see the [entire post list here](https://gitlab.com/subins2000/subinsb.com/tree/075097114ab1d3d66b13659de1343b77cad2d995/content/posts).

[Archive.org snapshots of the site sag-3.blogspot.in](https://web.archive.org/web/20120101000000*/http://sag-3.blogspot.in)

### 2013

In September 2013, after constant begging to my parents, appa (dad) finally gave me his debit card to buy a domain. Online transactions weren't common at the time and appa was suspicious of me using it. Thanks to GoDaddy's $0.99 domain offer, it was cheap to buy it.

> $0.99 != 70 Indian Rupees

I chose `subinsb.com`. It stands as an abbreviation of `SUBIN'S Blog`. It can also stand for `SUBIN SiBy` :P

I mapped my blogspot domain to the new one and old post links were all automatically redirected. The blog used `www.subinsb.com`.

[Archive.org snapshots of the site subinsb.com](https://web.archive.org/web/20130101000000*/http://subinsb.com)

### 2013 - 2017

In November of 2013, the blog was moved from **Blogger to WordPress**. The hosting was provided for free by RedHat's OpenShift platform which had a special free tier.

The redirection of old links of Blogger to WordPress was done possible with a [small WordPress plugin I made](//subinsb.com/move-blog-from-blogger-to-wordpress/).

I made a [custom theme called Brenton](https://github.com/subins2000/brenton-wordpress) for the blog.

Meanwhile, the site [demos.subinsb.com](http://demos.subinsb.com) was started to showcase examples of tutorials in the blog. The entire transition process is also mentioned there.

One huge influence of my blog and work were [Srinivas Tamada's 9lessons.info](http://9lessons.info).

[Archive.org snapshots of the site demos.subinsb.com](https://web.archive.org/web/20130101000000*/http://demos.subinsb.com)

### 2017 - now

RedHat discontinued OpenShift platform in favor of OpenShift 3 which had no free tier plans. So now I was troubled with another "movement".

Looking for a free alternative and low maintenance, I found [Hugo](https://gohugo.io/). I made a [custom theme called Brenton](//github.com/subins2000/brenton-hugo) which was a near reimplemtation of the old WordPress theme.

The blog is hosted for [free thanks to GitLab ❤️](https://gitlab.com/subins2000/subinsb.com).

## Stories

These stories will be completed later

### The Internet

Appa (dad) was the one who introduced me to internet. It was in the year 2008 on a GPRS enabled phone. The goal was to download games. How expensive internet and small games were back then !

The Samsung flip-phone we had supported Java games. To download new ones, there's a store called GameJAR or something. GPRS internet was billed on bandwidth usage from prepaid balance (100KB was 1Rs IIRC). When me & my brother get bored of games, we would reluctantly ask Appa to get us new ones ☺️. Since the whole procedure was expensive, we only get to do it once or twice an year. We had to patiently wait for Appa to do it for us. Ahh, the power he had over us then 😂

We bought a computer in 2010. In 2011, we got a `156Kbps` Tata Docomo mobile internet connection (USB). I used this internet till 2016. It was using this poor connection that I learnt programming,

### How I chose the username subins2000

### How I started programming

### Linux

The first GNU/Linux distro I got to interact with was Fedora around 2007/2008-ish. It was my uncle's computer and when we visit during vacation time, we'd play games on it. Fedora was the way how our uncle prevented us from using the Windows OS in it, so that we don't mess things up. I remember playing [Nibbles](https://wiki.gnome.org/Apps/Nibbles) very often.

I got seriously introduced to GNU/Linux from school where the Information Technology textbooks were all in GNU/Linux software. Kerala schools have a custom Ubuntu based distro, called [IT@School Linux](https://kite.kerala.gov.in). The shift from Windows XP to Ubuntu happened in textbooks when I entered 5th grade (2009).

When we were able to buy a computer (2010), we made sure that the IT@School distro came with the computer. I explored Ubuntu further, because I liked tinkering with it, the terminal, installation, software and all. At one point, I formatted the entire system which my dad absolutely didn't like :P

I still have that IT@School Ubuntu 10.04 CD with me. Should frame it and put up on the wall someday !

Kerala, that has a population of 30 million still continues to use & promote FOSS in school. I often see kids making cool stuff with tools they get. Kids in school these days get to play with Raspberyy Pi & Arduino, wish I was in school now !

## Copyright Notice

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />All content of this site is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
